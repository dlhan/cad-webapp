﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Safer.model
{
    public partial class Code
    {
        public int CodeId { get; set; }
        public string GroupCode { get; set; }
        public string Code1 { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Discontinued { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedDate { get; set; }
        public string Locale { get; set; }
    }
}
