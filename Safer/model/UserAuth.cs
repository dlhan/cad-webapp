﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Safer.model
{
    public partial class UserAuth
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string AuthId { get; set; }
        public string IsApproved { get; set; }
    }
}
