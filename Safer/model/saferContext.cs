﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace Safer.model
{
    public partial class saferContext : DbContext
    {
        public saferContext()
        {
        }

        public saferContext(DbContextOptions<saferContext> options)
            : base(options)
        {
        }

        public virtual DbSet<AuthInfo> AuthInfos { get; set; }
        public virtual DbSet<Code> Codes { get; set; }
        public virtual DbSet<CodeGroup> CodeGroups { get; set; }
        public virtual DbSet<IsWard> IsWards { get; set; }
        public virtual DbSet<Notification> Notifications { get; set; }
        public virtual DbSet<Person> People { get; set; }
        public virtual DbSet<PersonEducation> PersonEducations { get; set; }
        public virtual DbSet<PersonEducationPromotion> PersonEducationPromotions { get; set; }
        public virtual DbSet<PersonFireTrain> PersonFireTrains { get; set; }
        public virtual DbSet<PersonLicense> PersonLicenses { get; set; }
        public virtual DbSet<SmCd> SmCds { get; set; }
        public virtual DbSet<SmCdgroup> SmCdgroups { get; set; }
        public virtual DbSet<SmCdgroupcd> SmCdgroupcds { get; set; }
        public virtual DbSet<UserAuth> UserAuths { get; set; }
        public virtual DbSet<UserPreference> UserPreferences { get; set; }
        public virtual DbSet<ViewInfo> ViewInfos { get; set; }
        public virtual DbSet<ViewPrivilege> ViewPrivileges { get; set; }
        public virtual DbSet<Ward> Wards { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AuthInfo>(entity =>
            {
                entity.HasKey(e => e.AuthId)
                    .HasName("PRIMARY");

                entity.ToTable("auth_info");

                entity.Property(e => e.AuthId)
                    .HasMaxLength(20)
                    .HasColumnName("auth_id");

                entity.Property(e => e.Description)
                    .HasMaxLength(100)
                    .HasColumnName("description")
                    .HasDefaultValueSql("'NULL'");
            });

            modelBuilder.Entity<Code>(entity =>
            {
                entity.ToTable("code");

                entity.HasIndex(e => e.GroupCode, "code_FK");

                entity.Property(e => e.CodeId)
                    .HasColumnType("int(11)")
                    .HasColumnName("code_id");

                entity.Property(e => e.Code1)
                    .HasMaxLength(100)
                    .HasColumnName("code")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.CreatedDate)
                    .HasMaxLength(20)
                    .HasColumnName("created_date")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Description)
                    .HasMaxLength(100)
                    .HasColumnName("description")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Discontinued)
                    .HasMaxLength(1)
                    .HasColumnName("discontinued")
                    .HasDefaultValueSql("'NULL'")
                    .IsFixedLength(true);

                entity.Property(e => e.GroupCode)
                    .IsRequired()
                    .HasMaxLength(20)
                    .HasColumnName("group_code");

                entity.Property(e => e.Locale)
                    .HasMaxLength(2)
                    .HasColumnName("locale")
                    .HasDefaultValueSql("'NULL'")
                    .IsFixedLength(true);

                entity.Property(e => e.ModifiedDate)
                    .HasMaxLength(20)
                    .HasColumnName("modified_date")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Name)
                    .HasMaxLength(100)
                    .HasColumnName("name")
                    .HasDefaultValueSql("'NULL'");
            });

            modelBuilder.Entity<CodeGroup>(entity =>
            {
                entity.ToTable("code_group");

                entity.Property(e => e.CodeGroupId)
                    .HasColumnType("int(11)")
                    .HasColumnName("code_group_id");

                entity.Property(e => e.CreatedDate)
                    .HasMaxLength(20)
                    .HasColumnName("created_date")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Description)
                    .HasMaxLength(100)
                    .HasColumnName("description")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Discontinued)
                    .HasMaxLength(1)
                    .HasColumnName("discontinued")
                    .HasDefaultValueSql("'NULL'")
                    .IsFixedLength(true);

                entity.Property(e => e.GroupCode)
                    .IsRequired()
                    .HasMaxLength(20)
                    .HasColumnName("group_code");

                entity.Property(e => e.Level)
                    .HasColumnType("int(11)")
                    .HasColumnName("level")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Locale)
                    .HasMaxLength(2)
                    .HasColumnName("locale")
                    .HasDefaultValueSql("'NULL'")
                    .IsFixedLength(true);

                entity.Property(e => e.ModifiedDate)
                    .HasMaxLength(20)
                    .HasColumnName("modified_date")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Name)
                    .HasMaxLength(100)
                    .HasColumnName("name")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.ParentGroupCode)
                    .HasMaxLength(20)
                    .HasColumnName("parent_group_code")
                    .HasDefaultValueSql("'NULL'");
            });

            modelBuilder.Entity<IsWard>(entity =>
            {
                entity.HasKey(e => e.WardId)
                    .HasName("PRIMARY");

                entity.ToTable("is_ward");

                entity.Property(e => e.WardId)
                    .HasMaxLength(8)
                    .HasColumnName("ward_id");

                entity.Property(e => e.Address)
                    .HasMaxLength(100)
                    .HasColumnName("address")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.AutoListYn)
                    .HasMaxLength(1)
                    .HasColumnName("auto_list_yn")
                    .HasDefaultValueSql("'NULL'")
                    .IsFixedLength(true);

                entity.Property(e => e.BrdUsingYn)
                    .HasMaxLength(1)
                    .HasColumnName("brd_using_yn")
                    .HasDefaultValueSql("'NULL'")
                    .IsFixedLength(true);

                entity.Property(e => e.DayTel)
                    .HasMaxLength(20)
                    .HasColumnName("day_tel")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.DspGroupYn)
                    .HasMaxLength(1)
                    .HasColumnName("dsp_group_yn")
                    .HasDefaultValueSql("'NULL'")
                    .IsFixedLength(true);

                entity.Property(e => e.ExtTel)
                    .HasMaxLength(10)
                    .HasColumnName("ext_tel")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.FireServiceId)
                    .HasMaxLength(8)
                    .HasColumnName("fire_service_id")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.LchgDtime)
                    .HasColumnType("date")
                    .HasColumnName("lchg_dtime")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.LchgUserId)
                    .HasMaxLength(20)
                    .HasColumnName("lchg_user_id")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.NickName)
                    .HasMaxLength(50)
                    .HasColumnName("nick_name")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.NightTel)
                    .HasMaxLength(20)
                    .HasColumnName("night_tel")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Remark)
                    .HasMaxLength(300)
                    .HasColumnName("remark")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.TypeClsCd)
                    .HasColumnType("int(11)")
                    .HasColumnName("type_cls_cd")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.UpwardId)
                    .HasMaxLength(8)
                    .HasColumnName("upward_id")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.UseYn)
                    .HasMaxLength(1)
                    .HasColumnName("use_yn")
                    .HasDefaultValueSql("'''1'''")
                    .IsFixedLength(true);

                entity.Property(e => e.WardName)
                    .HasMaxLength(50)
                    .HasColumnName("ward_name")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.WardOrder)
                    .HasMaxLength(100)
                    .HasColumnName("ward_order")
                    .HasDefaultValueSql("'''9999'''");

                entity.Property(e => e.WardcloseDate)
                    .HasColumnType("date")
                    .HasColumnName("wardclose_date")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.WardopenDate)
                    .HasColumnType("date")
                    .HasColumnName("wardopen_date")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.WardopenYn)
                    .HasMaxLength(1)
                    .HasColumnName("wardopen_yn")
                    .HasDefaultValueSql("'NULL'")
                    .IsFixedLength(true);

                entity.Property(e => e.WorkTypeCd)
                    .HasColumnType("int(11)")
                    .HasColumnName("work_type_cd")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.X)
                    .HasMaxLength(20)
                    .HasColumnName("x")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Y)
                    .HasMaxLength(20)
                    .HasColumnName("y")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.ZipCode)
                    .HasMaxLength(10)
                    .HasColumnName("zip_code")
                    .HasDefaultValueSql("'NULL'");
            });

            modelBuilder.Entity<Notification>(entity =>
            {
                entity.ToTable("notification");

                entity.Property(e => e.NotificationId)
                    .HasColumnType("int(11)")
                    .HasColumnName("notification_id");

                entity.Property(e => e.CompletedDate)
                    .HasMaxLength(20)
                    .HasColumnName("completed_date")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.CreatedDate)
                    .HasMaxLength(20)
                    .HasColumnName("created_date")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.NotificationType)
                    .HasMaxLength(1)
                    .HasColumnName("notification_type")
                    .HasDefaultValueSql("'NULL'")
                    .IsFixedLength(true);

                entity.Property(e => e.RelatedId)
                    .HasColumnType("int(11)")
                    .HasColumnName("related_id")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.State)
                    .HasMaxLength(1)
                    .HasColumnName("state")
                    .HasDefaultValueSql("'NULL'")
                    .IsFixedLength(true);

                entity.Property(e => e.UserId)
                    .HasMaxLength(50)
                    .HasColumnName("user_id")
                    .HasDefaultValueSql("'NULL'");
            });

            modelBuilder.Entity<Person>(entity =>
            {
                entity.ToTable("person");

                entity.Property(e => e.PersonId)
                    .HasColumnType("int(11)")
                    .HasColumnName("person_id");

                entity.Property(e => e.BirthDay)
                    .HasColumnType("date")
                    .HasColumnName("birth_day")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.CreatedDate)
                    .HasMaxLength(20)
                    .HasColumnName("created_date")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Email)
                    .HasMaxLength(50)
                    .HasColumnName("email")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.EnrollDate)
                    .HasColumnType("date")
                    .HasColumnName("enroll_date")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.EtcAddress)
                    .HasMaxLength(200)
                    .HasColumnName("etc_address")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.ExtNum)
                    .HasMaxLength(100)
                    .HasColumnName("ext_num")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.FaxNum)
                    .HasMaxLength(20)
                    .HasColumnName("fax_num")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.FirstName)
                    .HasMaxLength(50)
                    .HasColumnName("first_name")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Gender)
                    .HasMaxLength(1)
                    .HasColumnName("gender")
                    .HasDefaultValueSql("'NULL'")
                    .IsFixedLength(true);

                entity.Property(e => e.HomeTel)
                    .HasMaxLength(20)
                    .HasColumnName("home_tel")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.IsApproved)
                    .HasMaxLength(1)
                    .HasColumnName("is_approved")
                    .HasDefaultValueSql("'''0'''")
                    .IsFixedLength(true);

                entity.Property(e => e.IsSystemAdministrator)
                    .HasMaxLength(1)
                    .HasColumnName("is_system_administrator")
                    .HasDefaultValueSql("'NULL'")
                    .IsFixedLength(true);

                entity.Property(e => e.IsUse)
                    .HasMaxLength(1)
                    .HasColumnName("is_use")
                    .HasDefaultValueSql("'NULL'")
                    .IsFixedLength(true);

                entity.Property(e => e.JobCode)
                    .HasMaxLength(100)
                    .HasColumnName("job_code")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.LastName)
                    .HasMaxLength(50)
                    .HasColumnName("last_name")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.LatestUpdateDate)
                    .HasMaxLength(20)
                    .HasColumnName("latest_update_date")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.MobileNum)
                    .HasMaxLength(20)
                    .HasColumnName("mobile_num")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.NotUseReason)
                    .HasMaxLength(100)
                    .HasColumnName("not_use_reason")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Password)
                    .HasMaxLength(100)
                    .HasColumnName("password")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.PositionCode)
                    .HasMaxLength(100)
                    .HasColumnName("position_code")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.PostalCode)
                    .HasColumnType("int(11)")
                    .HasColumnName("postal_code")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.RankCode)
                    .HasMaxLength(100)
                    .HasColumnName("rank_code")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.UserId)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("user_id");

                entity.Property(e => e.WardId)
                    .HasColumnType("int(11)")
                    .HasColumnName("ward_id")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.WorkTel)
                    .HasMaxLength(20)
                    .HasColumnName("work_tel")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.WorkTypeCode)
                    .HasMaxLength(100)
                    .HasColumnName("work_type_code")
                    .HasDefaultValueSql("'NULL'");
            });

            modelBuilder.Entity<PersonEducation>(entity =>
            {
                entity.ToTable("person_education");

                entity.HasIndex(e => e.PersonId, "person_education_FK");

                entity.Property(e => e.PersonEducationId)
                    .HasColumnType("int(11)")
                    .HasColumnName("person_education_id");

                entity.Property(e => e.AcademyName)
                    .HasMaxLength(100)
                    .HasColumnName("academy_name")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.AcademyType)
                    .HasMaxLength(100)
                    .HasColumnName("academy_type")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Degree)
                    .HasMaxLength(100)
                    .HasColumnName("degree")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.EntranceDate)
                    .HasColumnType("date")
                    .HasColumnName("entrance_date")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.GraducationDate)
                    .HasColumnType("date")
                    .HasColumnName("graducation_date")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Major)
                    .HasMaxLength(50)
                    .HasColumnName("major")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.PersonId)
                    .HasColumnType("int(11)")
                    .HasColumnName("person_id");

                entity.HasOne(d => d.Person)
                    .WithMany(p => p.PersonEducations)
                    .HasForeignKey(d => d.PersonId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("person_education_FK");
            });

            modelBuilder.Entity<PersonEducationPromotion>(entity =>
            {
                entity.ToTable("person_education_promotion");

                entity.HasIndex(e => e.PersonId, "person_education_promotion_FK");

                entity.Property(e => e.PersonEducationPromotionId)
                    .HasColumnType("int(11)")
                    .HasColumnName("person_education_promotion_id");

                entity.Property(e => e.CompleteCode)
                    .HasMaxLength(100)
                    .HasColumnName("completeCode")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.CourseName)
                    .HasMaxLength(100)
                    .HasColumnName("course_name")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.EducationClsCode)
                    .HasMaxLength(100)
                    .HasColumnName("education_cls_code")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.EndDate)
                    .HasColumnType("date")
                    .HasColumnName("end_date")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.InstitutionName)
                    .HasMaxLength(100)
                    .HasColumnName("institution_name")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.PersonId)
                    .HasColumnType("int(11)")
                    .HasColumnName("person_id");

                entity.Property(e => e.Score)
                    .HasMaxLength(10)
                    .HasColumnName("score")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.StartDate)
                    .HasColumnType("date")
                    .HasColumnName("start_date")
                    .HasDefaultValueSql("'NULL'");

                entity.HasOne(d => d.Person)
                    .WithMany(p => p.PersonEducationPromotions)
                    .HasForeignKey(d => d.PersonId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("person_education_promotion_FK");
            });

            modelBuilder.Entity<PersonFireTrain>(entity =>
            {
                entity.ToTable("person_fire_train");

                entity.HasIndex(e => e.PersonId, "person_fire_train_FK");

                entity.Property(e => e.PersonFireTrainId)
                    .HasColumnType("int(11)")
                    .HasColumnName("person_fire_train_id");

                entity.Property(e => e.EndDate)
                    .HasColumnType("date")
                    .HasColumnName("end_date")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.FireTrainName)
                    .HasMaxLength(100)
                    .HasColumnName("fire_train_name")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Host)
                    .HasMaxLength(100)
                    .HasColumnName("host")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.OrganizationName)
                    .HasMaxLength(100)
                    .HasColumnName("organization_name")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Perf)
                    .HasMaxLength(100)
                    .HasColumnName("perf")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.PersonId)
                    .HasColumnType("int(11)")
                    .HasColumnName("person_id")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.StartDate)
                    .HasColumnType("date")
                    .HasColumnName("start_date")
                    .HasDefaultValueSql("'NULL'");

                entity.HasOne(d => d.Person)
                    .WithMany(p => p.PersonFireTrains)
                    .HasForeignKey(d => d.PersonId)
                    .HasConstraintName("person_fire_train_FK");
            });

            modelBuilder.Entity<PersonLicense>(entity =>
            {
                entity.ToTable("person_license");

                entity.HasIndex(e => e.PersonId, "person_license_FK");

                entity.Property(e => e.PersonLicenseId)
                    .HasColumnType("int(11)")
                    .HasColumnName("person_license_id");

                entity.Property(e => e.AcquisitionDate)
                    .HasColumnType("date")
                    .HasColumnName("acquisition_date")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.LicenseName)
                    .HasMaxLength(100)
                    .HasColumnName("license_name")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.LicenseNumber)
                    .HasMaxLength(100)
                    .HasColumnName("license_number")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.PersonId)
                    .HasColumnType("int(11)")
                    .HasColumnName("person_id");

                entity.HasOne(d => d.Person)
                    .WithMany(p => p.PersonLicenses)
                    .HasForeignKey(d => d.PersonId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("person_license_FK");
            });

            modelBuilder.Entity<SmCd>(entity =>
            {
                entity.HasKey(e => new { e.Cd, e.Locale })
                    .HasName("PRIMARY");

                entity.ToTable("sm_cd");

                entity.Property(e => e.Cd)
                    .HasColumnType("int(11)")
                    .HasColumnName("cd");

                entity.Property(e => e.Locale)
                    .HasMaxLength(2)
                    .HasColumnName("locale");

                entity.Property(e => e.CdDesc)
                    .HasMaxLength(300)
                    .HasColumnName("cd_desc")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.CdName)
                    .HasMaxLength(100)
                    .HasColumnName("cd_name")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.UseYn)
                    .HasMaxLength(1)
                    .HasColumnName("use_yn")
                    .HasDefaultValueSql("'''1'''")
                    .IsFixedLength(true);
            });

            modelBuilder.Entity<SmCdgroup>(entity =>
            {
                entity.HasKey(e => new { e.CdGrp, e.Locale })
                    .HasName("PRIMARY");

                entity.ToTable("sm_cdgroup");

                entity.Property(e => e.CdGrp)
                    .HasMaxLength(20)
                    .HasColumnName("cd_grp");

                entity.Property(e => e.Locale)
                    .HasMaxLength(2)
                    .HasColumnName("locale");

                entity.Property(e => e.CdGrpDesc)
                    .HasMaxLength(1000)
                    .HasColumnName("cd_grp_desc")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.CdGrpName)
                    .HasMaxLength(100)
                    .HasColumnName("cd_grp_name")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.CdGrpStep)
                    .HasColumnType("decimal(10,0)")
                    .HasColumnName("cd_grp_step")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.IsLeaf)
                    .HasMaxLength(1)
                    .HasColumnName("is_leaf")
                    .HasDefaultValueSql("'NULL'")
                    .IsFixedLength(true);

                entity.Property(e => e.SortOrder)
                    .HasColumnType("decimal(10,0)")
                    .HasColumnName("sort_order")
                    .HasDefaultValueSql("'1'");

                entity.Property(e => e.UpperCdGrp)
                    .HasMaxLength(20)
                    .HasColumnName("upper_cd_grp")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.UseYn)
                    .HasMaxLength(1)
                    .HasColumnName("use_yn")
                    .HasDefaultValueSql("'''1'''")
                    .IsFixedLength(true);
            });

            modelBuilder.Entity<SmCdgroupcd>(entity =>
            {
                entity.HasKey(e => new { e.CdGrp, e.Cd })
                    .HasName("PRIMARY");

                entity.ToTable("sm_cdgroupcd");

                entity.Property(e => e.CdGrp)
                    .HasMaxLength(20)
                    .HasColumnName("cd_grp");

                entity.Property(e => e.Cd)
                    .HasColumnType("int(11)")
                    .HasColumnName("cd");

                entity.Property(e => e.SortOrder)
                    .HasColumnType("decimal(10,0)")
                    .HasColumnName("sort_order")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.UseYn)
                    .HasMaxLength(100)
                    .HasColumnName("use_yn")
                    .HasDefaultValueSql("'''1'''");
            });

            modelBuilder.Entity<UserAuth>(entity =>
            {
                entity.ToTable("user_auth");

                entity.HasIndex(e => e.AuthId, "user_auth_FK");

                entity.HasIndex(e => e.UserId, "user_auth_FK_1");

                entity.Property(e => e.Id)
                    .HasColumnType("int(11)")
                    .HasColumnName("id");

                entity.Property(e => e.AuthId)
                    .IsRequired()
                    .HasMaxLength(20)
                    .HasColumnName("auth_id");

                entity.Property(e => e.IsApproved)
                    .HasMaxLength(1)
                    .HasColumnName("is_approved")
                    .HasDefaultValueSql("'NULL'")
                    .IsFixedLength(true);

                entity.Property(e => e.UserId)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("user_id");
            });

            modelBuilder.Entity<UserPreference>(entity =>
            {
                entity.ToTable("user_preference");

                entity.Property(e => e.UserPreferenceId)
                    .HasColumnType("int(11)")
                    .HasColumnName("user_preference_id");

                entity.Property(e => e.Color)
                    .HasMaxLength(20)
                    .HasColumnName("color")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Language)
                    .HasMaxLength(2)
                    .HasColumnName("language")
                    .HasDefaultValueSql("'NULL'")
                    .IsFixedLength(true);

                entity.Property(e => e.TableDense)
                    .HasMaxLength(1)
                    .HasColumnName("table_dense")
                    .HasDefaultValueSql("'NULL'")
                    .IsFixedLength(true);

                entity.Property(e => e.TableRows)
                    .HasColumnType("int(11)")
                    .HasColumnName("table_rows")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Theme)
                    .HasMaxLength(10)
                    .HasColumnName("theme")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.UserId)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("user_id");
            });

            modelBuilder.Entity<ViewInfo>(entity =>
            {
                entity.HasKey(e => e.ViewId)
                    .HasName("PRIMARY");

                entity.ToTable("view_info");

                entity.Property(e => e.ViewId)
                    .HasMaxLength(20)
                    .HasColumnName("view_id");

                entity.Property(e => e.ResourceId)
                    .HasMaxLength(100)
                    .HasColumnName("resource_id")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.SystemId)
                    .HasMaxLength(10)
                    .HasColumnName("system_id")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.ViewName)
                    .HasMaxLength(100)
                    .HasColumnName("view_name")
                    .HasDefaultValueSql("'NULL'");
            });

            modelBuilder.Entity<ViewPrivilege>(entity =>
            {
                entity.ToTable("view_privilege");

                entity.HasIndex(e => e.ViewId, "view_privilege_FK");

                entity.HasIndex(e => e.AuthId, "view_privilege_FK_1");

                entity.Property(e => e.Id)
                    .HasColumnType("int(11)")
                    .HasColumnName("id");

                entity.Property(e => e.AuthId)
                    .HasMaxLength(20)
                    .HasColumnName("auth_id")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Privilege)
                    .HasColumnType("int(11)")
                    .HasColumnName("privilege")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.ViewId)
                    .HasMaxLength(20)
                    .HasColumnName("view_id")
                    .HasDefaultValueSql("'NULL'");

                entity.HasOne(d => d.Auth)
                    .WithMany(p => p.ViewPrivileges)
                    .HasForeignKey(d => d.AuthId)
                    .HasConstraintName("view_privilege_FK_1");

                entity.HasOne(d => d.View)
                    .WithMany(p => p.ViewPrivileges)
                    .HasForeignKey(d => d.ViewId)
                    .HasConstraintName("view_privilege_FK");
            });

            modelBuilder.Entity<Ward>(entity =>
            {
                entity.ToTable("ward");

                entity.Property(e => e.WardId)
                    .HasColumnType("int(11)")
                    .HasColumnName("ward_id");

                entity.Property(e => e.AbbrName)
                    .HasMaxLength(50)
                    .HasColumnName("abbr_name")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.CloseDate)
                    .HasColumnType("date")
                    .HasColumnName("close_date")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.EtcAddress)
                    .HasMaxLength(200)
                    .HasColumnName("etc_address")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.ExtNumber)
                    .HasMaxLength(10)
                    .HasColumnName("ext_number")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.FaxNumber)
                    .HasMaxLength(20)
                    .HasColumnName("fax_number")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.IsBrodcastingUse)
                    .HasMaxLength(1)
                    .HasColumnName("is_brodcasting_use")
                    .HasDefaultValueSql("'NULL'")
                    .IsFixedLength(true);

                entity.Property(e => e.IsUse)
                    .HasMaxLength(1)
                    .HasColumnName("is_use")
                    .HasDefaultValueSql("'NULL'")
                    .IsFixedLength(true);

                entity.Property(e => e.OpenDate)
                    .HasColumnType("date")
                    .HasColumnName("open_date")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.PhoneNumber)
                    .HasMaxLength(20)
                    .HasColumnName("phone_number")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.PostalCode)
                    .HasColumnType("int(11)")
                    .HasColumnName("postal_code")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Remark)
                    .HasMaxLength(500)
                    .HasColumnName("remark")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.TypeCode)
                    .HasMaxLength(20)
                    .HasColumnName("type_code")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.UpperWardId)
                    .HasColumnType("int(11)")
                    .HasColumnName("upper_ward_id")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.WardName)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("ward_name");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
