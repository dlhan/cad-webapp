﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Safer.model
{
    public partial class Notification
    {
        public int NotificationId { get; set; }
        public string NotificationType { get; set; }
        public int? RelatedId { get; set; }
        public string State { get; set; }
        public string CreatedDate { get; set; }
        public string CompletedDate { get; set; }
        public string UserId { get; set; }
    }
}
