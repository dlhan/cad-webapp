﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Safer.model
{
    public partial class AuthInfo
    {
        public AuthInfo()
        {
            ViewPrivileges = new HashSet<ViewPrivilege>();
        }

        public string AuthId { get; set; }
        public string Description { get; set; }

        public virtual ICollection<ViewPrivilege> ViewPrivileges { get; set; }
    }
}
