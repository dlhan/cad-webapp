﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Safer.model
{
    public partial class Person
    {
        public Person()
        {
            PersonEducationPromotions = new HashSet<PersonEducationPromotion>();
            PersonEducations = new HashSet<PersonEducation>();
            PersonFireTrains = new HashSet<PersonFireTrain>();
            PersonLicenses = new HashSet<PersonLicense>();
        }

        public int PersonId { get; set; }
        public string UserId { get; set; }
        public string Password { get; set; }
        public DateTime? EnrollDate { get; set; }
        public string LatestUpdateDate { get; set; }
        public string IsUse { get; set; }
        public string IsSystemAdministrator { get; set; }
        public string NotUseReason { get; set; }
        public int? PostalCode { get; set; }
        public string EtcAddress { get; set; }
        public int? WardId { get; set; }
        public string PositionCode { get; set; }
        public string RankCode { get; set; }
        public string JobCode { get; set; }
        public string WorkTypeCode { get; set; }
        public string HomeTel { get; set; }
        public string MobileNum { get; set; }
        public string WorkTel { get; set; }
        public string ExtNum { get; set; }
        public string FaxNum { get; set; }
        public string Email { get; set; }
        public string CreatedDate { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string IsApproved { get; set; }
        public string Gender { get; set; }
        public DateTime? BirthDay { get; set; }

        public virtual ICollection<PersonEducationPromotion> PersonEducationPromotions { get; set; }
        public virtual ICollection<PersonEducation> PersonEducations { get; set; }
        public virtual ICollection<PersonFireTrain> PersonFireTrains { get; set; }
        public virtual ICollection<PersonLicense> PersonLicenses { get; set; }
    }
}
