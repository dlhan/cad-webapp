﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Safer.model
{
    public partial class IsWard
    {
        public string WardId { get; set; }
        public string UpwardId { get; set; }
        public string FireServiceId { get; set; }
        public string UseYn { get; set; }
        public string WardName { get; set; }
        public int? TypeClsCd { get; set; }
        public string NickName { get; set; }
        public string ZipCode { get; set; }
        public string Address { get; set; }
        public int? WorkTypeCd { get; set; }
        public string DspGroupYn { get; set; }
        public string AutoListYn { get; set; }
        public string DayTel { get; set; }
        public string NightTel { get; set; }
        public string ExtTel { get; set; }
        public DateTime? WardopenDate { get; set; }
        public DateTime? WardcloseDate { get; set; }
        public string WardopenYn { get; set; }
        public string BrdUsingYn { get; set; }
        public string Remark { get; set; }
        public DateTime? LchgDtime { get; set; }
        public string LchgUserId { get; set; }
        public string WardOrder { get; set; }
        public string X { get; set; }
        public string Y { get; set; }
    }
}
