﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Safer.model
{
    public partial class PersonLicense
    {
        public int PersonLicenseId { get; set; }
        public DateTime? AcquisitionDate { get; set; }
        public string LicenseNumber { get; set; }
        public string LicenseName { get; set; }
        public int PersonId { get; set; }

        public virtual Person Person { get; set; }
    }
}
