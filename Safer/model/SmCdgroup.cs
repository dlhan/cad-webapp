﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Safer.model
{
    public partial class SmCdgroup
    {
        public string CdGrp { get; set; }
        public string UpperCdGrp { get; set; }
        public decimal? CdGrpStep { get; set; }
        public string CdGrpName { get; set; }
        public string UseYn { get; set; }
        public string CdGrpDesc { get; set; }
        public decimal? SortOrder { get; set; }
        public string Locale { get; set; }
        public string IsLeaf { get; set; }
    }
}
