﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Safer.model
{
    public partial class ViewPrivilege
    {
        public int Id { get; set; }
        public string AuthId { get; set; }
        public string ViewId { get; set; }
        public int? Privilege { get; set; }

        public virtual AuthInfo Auth { get; set; }
        public virtual ViewInfo View { get; set; }
    }
}
