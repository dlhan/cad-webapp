﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Safer.model
{
    public partial class CodeGroup
    {
        public int CodeGroupId { get; set; }
        public string GroupCode { get; set; }
        public string ParentGroupCode { get; set; }
        public int? Level { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Discontinued { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedDate { get; set; }
        public string Locale { get; set; }
    }
}
