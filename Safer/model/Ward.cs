﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Safer.model
{
    public partial class Ward
    {
        public int WardId { get; set; }
        public int? UpperWardId { get; set; }
        public string IsUse { get; set; }
        public string WardName { get; set; }
        public string TypeCode { get; set; }
        public string AbbrName { get; set; }
        public int? PostalCode { get; set; }
        public string PhoneNumber { get; set; }
        public string ExtNumber { get; set; }
        public DateTime? OpenDate { get; set; }
        public DateTime? CloseDate { get; set; }
        public string IsBrodcastingUse { get; set; }
        public string FaxNumber { get; set; }
        public string Remark { get; set; }
        public string EtcAddress { get; set; }
    }
}
