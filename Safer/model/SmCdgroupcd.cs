﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Safer.model
{
    public partial class SmCdgroupcd
    {
        public string CdGrp { get; set; }
        public int Cd { get; set; }
        public decimal? SortOrder { get; set; }
        public string UseYn { get; set; }
    }
}
