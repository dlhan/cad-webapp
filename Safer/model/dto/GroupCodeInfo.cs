using System;
using System.Collections.Generic;

namespace Safer.model.dto 
{
    public class GroupCodeInfo
    {
        public string GroupCodeTitle { get; set; }
        public virtual IEnumerable<SmCdgroup> rows { get; set; } 
    }
}