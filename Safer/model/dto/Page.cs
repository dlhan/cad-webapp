using System;
using System.Collections.Generic;

namespace Safer.model.dto 
{
    public class Page
    {
        public int RowsPerPage { get; set; }
        public int PageNumber { get; set; }
        public int TotalCount { get;set; }
    }
}