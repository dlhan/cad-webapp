using System;
using System.Collections.Generic;
using Safer.model;

namespace Safer.model.dto 
{
    public class SearchWard
    {
        public Page page { get; set; }
        public int? UpperWardId { get; set; }
        public int? WardId { get; set; }
        public string TypeCode { get; set; }
        public string Locale { get; set; }
    }

    public class SearchWardResult
    {
        public Page page { get; set; }
        public virtual IEnumerable<SearchWardInfo> Rows { get; set; } 
    }

    public class SearchWardInfo
    {
        public int WardId { get; set; }
        public string UpperWardName { get; set; }
        public string WardName { get; set; }
        public string TypeCode { get; set; }
        public string AbbrName { get; set; }
        public string PhoneNumber { get; set; }
        public string Address { get; set; }
        public string IsBrodcastingUse { get; set; }
    }
}