using System;
using System.Collections.Generic;

namespace Safer.model.dto 
{
    public class UserInfo 
    {
        public CallResult result { get; set; }
        public Person userInfo { get; set; }
        public UserPreference userPreference { get; set; }
        public virtual IEnumerable<UserAuth> userAuth { get; set; } 
    }

    public class UserAuth
    {
        public string ViewId { get; set; }
        public int? Privilege { get; set; }
    }
}