using System;
using System.Collections.Generic;

namespace Safer.model.dto 
{
    public class LogIn
    {
        public string LoginId { get; set; }
        public string Password { get; set; }
    }
}