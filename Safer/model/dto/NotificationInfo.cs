using System;
using System.Collections.Generic;
using Safer.model;

namespace Safer.model.dto 
{
    public class NotificationInfo
    {
        public int NotificationId { get; set; }
        public string NotificationType { get; set; }
        public int? RelatedId { get; set; }
        public string State { get; set; }
        public string CreatedDate { get; set; }
        public string UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string WardName { get; set; }
    }
}