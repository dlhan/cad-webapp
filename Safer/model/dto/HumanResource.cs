using System;
using System.Collections.Generic;
using Safer.model;

namespace Safer.model.dto 
{
    public class SearchPerson
    {
        public Page page { get; set; }
        public int? WardId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserId { get; set; }
        public string PositionCode { get; set; }
        public string RankCode { get; set; }
        public string JobCode { get; set; }
        public string WorkTypeCode { get; set; }
        public string Locale { get; set; }
    }

    public class SearchPersonResult
    {
        public Page page { get; set; }
        public virtual IEnumerable<PersonInfo> Rows { get; set; } 
    }

    public class PersonInfo
    {
        public int PersonId { get; set; }
        public string UserId { get; set; }
        public string Password { get; set; }
        public DateTime? EnrollDate { get; set; }
        public string LatestUpdateDate { get; set; }
        public string IsUse { get; set; }
        public string IsSystemAdministrator { get; set; }
        public string NotUseReason { get; set; }
        public int? PostalCode { get; set; }
        public string EtcAddress { get; set; }
        public int? UpperWardId { get; set; }
        public string UpperWardName { get; set; }
        public int? WardId { get; set; }
        public string WardName { get; set; }
        public string PositionCode { get; set; }
        public string Position { get; set; }
        public string RankCode { get; set; }
        public string Rank { get; set; }
        public string JobCode { get; set; }
        public string Job { get; set; }
        public string WorkTypeCode { get; set; }
        public string WorkType { get; set; }
        public string HomeTel { get; set; }
        public string MobileNum { get; set; }
        public string WorkTel { get; set; }
        public string ExtNum { get; set; }
        public string FaxNum { get; set; }
        public string Email { get; set; }
        public string CreatedDate { get; set; }
        public string PersonName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string IsApproved { get; set; }
        public string Gender { get; set; }
        public DateTime? BirthDay { get; set; }
    }

    public class PersonEducationInfo
    {
        public int PersonEducationId { get; set; }
        public int PersonId { get; set; }
        public DateTime? EntranceDate { get; set; }
        public DateTime? GraducationDate { get; set; }
        public string AcademyName { get; set; }
        public string Major { get; set; }
        public string Degree { get; set; }
        public string DegreeName { get; set; }
        public string AcademyType { get; set; }
        public string AcademyTypeName { get; set; }
    }

    public class PersonEducationPromotionInfo
    {
        public int PersonEducationPromotionId { get; set; }
        public int PersonId { get; set; }
        public string CourseName { get; set; }
        public string InstitutionName { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string EducationClsCode { get; set; }
        public string EducationClsCodeName { get; set; }
        public string CompleteCode { get; set; }
        public string CompleteCodeName { get; set; }
        public string Score { get; set; }
    }
}