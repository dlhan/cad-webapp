﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Safer.model
{
    public partial class PersonEducation
    {
        public int PersonEducationId { get; set; }
        public int PersonId { get; set; }
        public DateTime? EntranceDate { get; set; }
        public DateTime? GraducationDate { get; set; }
        public string AcademyName { get; set; }
        public string Major { get; set; }
        public string Degree { get; set; }
        public string AcademyType { get; set; }

        public virtual Person Person { get; set; }
    }
}
