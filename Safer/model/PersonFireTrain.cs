﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Safer.model
{
    public partial class PersonFireTrain
    {
        public int PersonFireTrainId { get; set; }
        public int? PersonId { get; set; }
        public string FireTrainName { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Host { get; set; }
        public string Perf { get; set; }
        public string OrganizationName { get; set; }

        public virtual Person Person { get; set; }
    }
}
