﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Safer.model
{
    public partial class UserPreference
    {
        public int UserPreferenceId { get; set; }
        public string UserId { get; set; }
        public string Language { get; set; }
        public string Theme { get; set; }
        public int? TableRows { get; set; }
        public string TableDense { get; set; }
        public string Color { get; set; }
    }
}
