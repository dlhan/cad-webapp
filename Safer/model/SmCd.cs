﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Safer.model
{
    public partial class SmCd
    {
        public int Cd { get; set; }
        public string CdName { get; set; }
        public string UseYn { get; set; }
        public string CdDesc { get; set; }
        public string Locale { get; set; }
    }
}
