﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Safer.model
{
    public partial class ViewInfo
    {
        public ViewInfo()
        {
            ViewPrivileges = new HashSet<ViewPrivilege>();
        }

        public string ViewId { get; set; }
        public string ViewName { get; set; }
        public string ResourceId { get; set; }
        public string SystemId { get; set; }

        public virtual ICollection<ViewPrivilege> ViewPrivileges { get; set; }
    }
}
