﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Safer.model
{
    public partial class PersonEducationPromotion
    {
        public int PersonEducationPromotionId { get; set; }
        public int PersonId { get; set; }
        public string CourseName { get; set; }
        public string InstitutionName { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string EducationClsCode { get; set; }
        public string CompleteCode { get; set; }
        public string Score { get; set; }

        public virtual Person Person { get; set; }
    }
}
