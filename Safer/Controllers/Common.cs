using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Safer.model;
using Safer.model.dto;

namespace Safer.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CommonController : ControllerBase
    {
        private readonly saferContext _context;

        public CommonController(saferContext context)
        {
            _context = context;
        }

        [HttpGet]
        [Route("GetGroupCode/{GroupCode}/{locale}")]
        public IEnumerable<CodeGroup> GetGroupCode(string GroupCode, string locale)
        {
            var db = _context.CodeGroups.Where(p => p.ParentGroupCode == GroupCode).Where(p => p.Locale == locale);
            return db;
        }

        [HttpGet]
        [Route("GetCodes/{GroupCode}/{locale}")]
        public IEnumerable<Code> GetCodes(string GroupCode, string locale)
        {
            var db = _context.Codes.Where(p => p.GroupCode == GroupCode).Where(p => p.Locale == locale);
            return db;
        }

        [HttpGet]
        [Route("GetNotification")]
        public IEnumerable<NotificationInfo> GetNotification()
        {
            var db = from notifications in _context.Notifications.Where(p => p.State == "0")
                     join people in _context.People on notifications.UserId equals people.UserId
                     join wardlist in _context.Wards on people.WardId equals wardlist.WardId into JoinData
                     from m in JoinData.DefaultIfEmpty()
                       select new NotificationInfo {
                           NotificationId  = notifications.NotificationId,
                           NotificationType = notifications.NotificationType,
                           RelatedId = notifications.RelatedId,
                           State = notifications.State,
                           CreatedDate = notifications.CreatedDate,
                           UserId = notifications.UserId,
                           FirstName = people.FirstName,
                           LastName  = people.LastName,
                           WardName = (m == null ? String.Empty : m.WardName)
                       };

            return db.ToList();
        }

        [HttpPost("ApproveNotification")]
        public ActionResult<CallResult> ApproveNotification([FromBody] IList<int> Ids )
        {
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                _context.Notifications.Where(p => Ids.Contains(p.NotificationId)).ToList()
                .ForEach(item => {
                    switch (item.NotificationType)
                    {
                        case "1" :
                            _context.People.SingleOrDefault(f => f.PersonId == item.RelatedId).IsApproved = "1";
                            _context.People.SingleOrDefault(f => f.PersonId == item.RelatedId).IsUse = "1";
                            _context.SaveChanges();
                            break;
                        case "2" :
                            _context.UserAuths.SingleOrDefault(f => f.Id == item.RelatedId).IsApproved = "1";
                            _context.SaveChanges();
                            break;
                    }

                    _context.Notifications.SingleOrDefault(f => f.NotificationId == item.NotificationId).CompletedDate = DateTime.Now.ToString();
                    _context.Notifications.SingleOrDefault(f => f.NotificationId == item.NotificationId).State = "1"; //0:Not approved, 1:Approved, 2:Rejected
                    _context.SaveChanges();
                });
                result.result.IsSuccess = true;
                result.result.Message = "approved";
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }

            return CreatedAtAction(nameof(ApproveNotification), result);
        }

        [HttpPost("RejectNotification")]
        public ActionResult<CallResult> RejectNotification([FromBody] IList<int> Ids )
        {
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                _context.Notifications.Where(p => Ids.Contains(p.NotificationId)).ToList()
                .ForEach(item => {
                    switch (item.NotificationType)
                    {
                        case "1" :
                            _context.People.SingleOrDefault(f => f.PersonId == item.RelatedId).IsApproved = "0";
                            _context.SaveChanges();
                            break;
                        case "2" :
                            _context.UserAuths.SingleOrDefault(f => f.Id == item.RelatedId).IsApproved = "0";
                            _context.SaveChanges();
                            break;
                    }

                    _context.Notifications.SingleOrDefault(f => f.NotificationId == item.NotificationId).CompletedDate = DateTime.Now.ToString();
                    _context.Notifications.SingleOrDefault(f => f.NotificationId == item.NotificationId).State = "2"; //0:Not approved, 1:Approved, 2:Rejected
                    _context.SaveChanges();
                });
                result.result.IsSuccess = true;
                result.result.Message = "rejected";
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }

            return CreatedAtAction(nameof(RejectNotification), result);
        }
    }
}