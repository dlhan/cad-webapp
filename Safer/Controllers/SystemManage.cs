using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Safer.model;
using Safer.model.dto;

namespace Safer.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SystemManageController : ControllerBase
    {
        private readonly saferContext _context;

        public SystemManageController(saferContext context)
        {
            _context = context;
        }

        [HttpGet]
        [Route("GetNextGroupCode/{groupCode}/{locale}")]
        public ActionResult<GroupCodeInfo> GetNextGroupCode(string groupCode, string locale)
        {
            GroupCodeInfo info = new GroupCodeInfo();
            var GropCode = _context.SmCdgroups.Where(p => p.CdGrp == groupCode).Where(p => p.Locale == locale).Select(s => new {s.CdGrpName}).FirstOrDefault();
            var db = from smGroups in _context.SmCdgroups.Where(p => p.UpperCdGrp == groupCode).Where(p => p.Locale == locale).Where(p => p.UseYn == "1").OrderBy(o => o.SortOrder)
                select new SmCdgroup {
                    CdGrp = smGroups.CdGrp,
                    CdGrpName = smGroups.CdGrpName,
                    CdGrpDesc = smGroups.CdGrpDesc,
                    IsLeaf = String.IsNullOrEmpty(smGroups.IsLeaf) ? "0" : smGroups.IsLeaf
                };
            info.GroupCodeTitle = GropCode.CdGrpName.ToString();
            info.rows = db;
            return info;
        }

        [HttpGet]
        [Route("GetCodesForGroupCode/{groupCode}/{locale}")]
        public IEnumerable<SmCd> GetCodesForGroupCode(string groupCode, string locale)
        {
            var db = from cdGroupMatch in _context.SmCdgroupcds.Where(w => w.CdGrp == groupCode).OrderBy(o => o.SortOrder)
                     join cd in _context.SmCds.Where(w => w.Locale == locale).Where(w => w.UseYn == "1") on cdGroupMatch.Cd equals cd.Cd
                     select new SmCd {
                       Cd = cd.Cd,
                       CdName = cd.CdName,
                       CdDesc = cd.CdDesc
                     };
            
            return db.ToList();
        }
    }
}