using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Safer.model;
using Safer.model.dto;

namespace Safer.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WardController : ControllerBase
    {
        private readonly saferContext _context;

        public WardController(saferContext context)
        {
            _context = context;
        }

        [HttpGet]
        [Route("GetWardInfo/{wardId}")]
        public Ward GetWardInfo(int wardId)
        {
            Ward wardInfo = new Ward();
            wardInfo = _context.Wards.Where(p => p.WardId == wardId).First();
            return wardInfo;
        }

        [HttpGet]
        [Route("GetUpperWardList")]
        public IEnumerable<Ward> GetUpperWardList()
        {
            var db = _context.Wards.Where(w => w.TypeCode.Equals("1") || w.TypeCode.Equals("2")).OrderBy(o => o.WardName);
            return db;
        }

        [HttpGet]
        [Route("GetWardListForUpperOffice/{upperWardId}")]
        public IEnumerable<Ward> GetWardListForUpperOffice(int upperWardId)
        {
            var db = _context.Wards.Where(p => p.UpperWardId == upperWardId).OrderBy(o => o.WardName);
            return db;
        }

        [HttpGet]
        [Route("GetWardListForUpperOffice")]
        public IEnumerable<Ward> GetWardListForUpperOffice()
        {
            var db = _context.Wards.Where(p => p.UpperWardId == null);
            return db;
        }

        [HttpPost]
        [Route("WardList")]
        public ActionResult<SearchWardResult> WardList([FromBody] SearchWard dto)
        {
            int? UpperWardId = dto.UpperWardId;
            int? WardId = dto.WardId;
            string TypeCode = dto.TypeCode;
            int RowsPerPage = dto.page.RowsPerPage;
            int PageNumber = dto.page.PageNumber;
            int TotalCount = 0;
            
            SearchWardResult wardResult = new SearchWardResult();
                        
            var predicate = PredicateBuilder.True<Ward>();

            if (dto.UpperWardId != null)
            {
                predicate = predicate.And(p => p.UpperWardId.Equals(dto.UpperWardId));
            }

            if (dto.WardId != null)
            {
                predicate = predicate.And(p => p.WardId.Equals(dto.WardId));
            }

            if (dto.TypeCode != null)
            {
                predicate = predicate.And(p => p.TypeCode.Equals(dto.TypeCode));
            }

            var WardList = from wardList in _context.Wards.Where(predicate).OrderBy(p => p.WardName).OrderBy(p => p.WardId).Skip(RowsPerPage * (PageNumber - 1)).Take(RowsPerPage)
                           join codes in _context.Codes.Where(w => w.Locale == dto.Locale) on wardList.TypeCode equals codes.Code1 
                           join uppwardList in _context.Wards on wardList.UpperWardId equals uppwardList.WardId  into JoinData
                           from m in JoinData.DefaultIfEmpty()
                             select new SearchWardInfo {
                                WardId = wardList.WardId,
                                UpperWardName = m.WardName,
                                TypeCode = codes.Name,
                                WardName = wardList.WardName,
                                AbbrName = wardList.AbbrName,
                                PhoneNumber = wardList.PhoneNumber,
                                Address = wardList.EtcAddress,
                                IsBrodcastingUse = wardList.IsBrodcastingUse 
                             };
            
            TotalCount = _context.Wards.Where(predicate).Count();



            wardResult.page = new Page();
            wardResult.page.PageNumber = dto.page.PageNumber;
            wardResult.page.RowsPerPage = dto.page.RowsPerPage;
            wardResult.page.TotalCount = TotalCount;
            wardResult.Rows = WardList;

            return CreatedAtAction(nameof(WardList), wardResult);
        }

        [HttpPost]
        [Route("InsertWard")]
        public ActionResult<JobResultWithId> InsertWard([FromBody] Ward dto)
        {
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                _context.Wards.Add(dto);
                _context.SaveChanges();
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_insert";
                result.ID = dto.WardId;
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }
            
    
            return CreatedAtAction(nameof(InsertWard), result);
        }

        [HttpPost]
        [Route("UpdateWard")]
        public ActionResult<JobResultWithId> UpdateWard([FromBody] Ward dto)
        {
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                _context.Wards.Update(dto);
                _context.SaveChanges();
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_update";
                result.ID = dto.WardId;
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }
            
    
            return CreatedAtAction(nameof(UpdateWard), result);
        }

        [HttpPost("DeleteWard")]
        public ActionResult<CallResult> DeleteWard([FromBody] IList<int> Ids )
        {
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                var wardData = _context.Wards.Where(p => Ids.Contains(p.WardId)).ToList();
                _context.Wards.RemoveRange(wardData);
                _context.SaveChanges();
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_delete";
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }

            return CreatedAtAction(nameof(DeleteWard), result);
        }

        [HttpPost("GetWardsForType")]
        public IEnumerable<IsWard> GetWardsForType([FromBody] IList<int?> type_cls_cds )
        {
            var db = _context.IsWards.Where(p => type_cls_cds.Contains(p.TypeClsCd));
            return db.ToList();
        }
    }
}