using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Safer.model;
using Safer.model.dto;

namespace Safer.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class HumanResourceController : ControllerBase
    {
        private readonly saferContext _context;

        public HumanResourceController(saferContext context)
        {
            _context = context;
        }

        [HttpPost]
        [Route("GetPersonList")]
        public ActionResult<SearchPersonResult> WardList([FromBody] SearchPerson dto)
        {
            int? WardId = dto.WardId;
            string FirstName = dto.FirstName;
            string LastName = dto.LastName;
            string UserId = dto.UserId;
            string PositionCode = dto.PositionCode;
            string RankCode = dto.RankCode;
            string JobCode = dto.JobCode;
            string WorkTypeCode = dto.WorkTypeCode;
            int RowsPerPage = dto.page.RowsPerPage;
            int PageNumber = dto.page.PageNumber;
            int TotalCount = 0;
            
            SearchPersonResult searchResult = new SearchPersonResult();
                        
            var predicate = PredicateBuilder.True<Person>();

            if (WardId != null)
            {
                predicate = predicate.And(p => p.WardId.Equals(WardId));
            }

            if (FirstName != null)
            {
                predicate = predicate.And(p => p.FirstName.Contains(FirstName));
            }

            if (LastName != null)
            {
                predicate = predicate.And(p => p.LastName.Contains(LastName));
            }

            if (UserId != null)
            {
                predicate = predicate.And(p => p.UserId.Contains(UserId));
            }

            if (PositionCode != null)
            {
                predicate = predicate.And(p => p.PositionCode.Equals(PositionCode));
            }

            if (RankCode != null)
            {
                predicate = predicate.And(p => p.RankCode.Equals(RankCode));
            }

            if (JobCode != null)
            {
                predicate = predicate.And(p => p.JobCode.Equals(JobCode));
            }

            if (WorkTypeCode != null)
            {
                predicate = predicate.And(p => p.WorkTypeCode.Equals(WorkTypeCode));
            }

            var db = from personList in _context.People.Where(predicate).OrderBy(p => p.FirstName).OrderBy(p => p.PersonId).Skip(RowsPerPage * (PageNumber - 1)).Take(RowsPerPage)
                        join ward in _context.Wards on personList.WardId equals ward.WardId into wardJoin from w1 in wardJoin.DefaultIfEmpty()
                        join position in _context.Codes.Where(w => w.Locale == dto.Locale) on personList.PositionCode equals position.Code1 into positionJoin from m1 in positionJoin.DefaultIfEmpty()
                        join rank in _context.Codes.Where(w => w.Locale == dto.Locale) on personList.RankCode equals rank.Code1 into rankJoin from m2 in rankJoin.DefaultIfEmpty()
                        join job in _context.Codes.Where(w => w.Locale == dto.Locale) on personList.JobCode equals job.Code1 into jobJoin from m3 in jobJoin.DefaultIfEmpty()
                        join workType in _context.Codes.Where(w => w.Locale == dto.Locale) on personList.WorkTypeCode equals workType.Code1 into workTypeJoin from m4 in workTypeJoin.DefaultIfEmpty()
                        join upward in _context.Wards on w1.UpperWardId equals upward.WardId  into JoinData
                        from m in JoinData.DefaultIfEmpty()
                             select new PersonInfo {
                                PersonId = personList.PersonId,
                                UserId = personList.UserId,
                                EnrollDate = personList.EnrollDate,
                                LatestUpdateDate = personList.LatestUpdateDate,
                                IsUse = personList.IsUse,
                                IsSystemAdministrator = personList.IsSystemAdministrator,
                                NotUseReason = personList.NotUseReason,
                                PostalCode = personList.PostalCode,
                                EtcAddress = personList.EtcAddress,
                                UpperWardName = m.WardName,
                                WardName = w1.WardName,
                                Position = m1.Name,
                                Rank = m2.Name,
                                Job = m3.Name,
                                WorkType = m4.Name,
                                HomeTel = personList.HomeTel,
                                MobileNum = personList.MobileNum,
                                WorkTel = personList.WorkTel,
                                ExtNum = personList.ExtNum,
                                FaxNum = personList.FaxNum,
                                Email = personList.Email,
                                PersonName = String.Format("{0} {1}", personList.FirstName, personList.LastName),
                                Gender = personList.Gender,
                                BirthDay = personList.BirthDay
                             };
            
            TotalCount = _context.People.Where(predicate).Count();

            searchResult.page = new Page();
            searchResult.page.PageNumber = dto.page.PageNumber;
            searchResult.page.RowsPerPage = dto.page.RowsPerPage;
            searchResult.page.TotalCount = TotalCount;
            searchResult.Rows = db;

            return CreatedAtAction(nameof(WardList), searchResult);
        }


        [HttpGet]
        [Route("GetFireWorker/{personId}")]
        public PersonInfo GetFireWorker(int personId)
        {
            Person fireWorker = new Person();
            fireWorker = _context.People.Where(p => p.PersonId == personId).First();
            var db = from person in _context.People.Where(p => p.PersonId == personId)
                     join ward in _context.Wards on person.WardId equals ward.WardId into joinData from w in joinData.DefaultIfEmpty()
                        select new PersonInfo {
                                PersonId = person.PersonId,
                                UserId = person.UserId,
                                EnrollDate = person.EnrollDate,
                                IsUse = person.IsUse,
                                IsSystemAdministrator = person.IsSystemAdministrator,
                                NotUseReason = person.NotUseReason,
                                PostalCode = person.PostalCode,
                                EtcAddress = person.EtcAddress,
                                UpperWardId = w.UpperWardId,
                                WardId = w.WardId,
                                PositionCode = person.PositionCode,
                                RankCode = person.RankCode,
                                JobCode = person.JobCode,
                                WorkTypeCode = person.WorkTypeCode,
                                HomeTel = person.HomeTel,
                                MobileNum = person.MobileNum,
                                WorkTel = person.WorkTel,
                                ExtNum = person.ExtNum,
                                FaxNum = person.FaxNum,
                                Email = person.Email,
                                FirstName = person.FirstName,
                                LastName = person.LastName,
                                Gender = person.Gender,
                                BirthDay = person.BirthDay
                             };
            return db.FirstOrDefault();
        }

        [HttpGet]
        [Route("CheckLoginIdDuplicated/{loginId}")]
        public bool CheckLoginIdDuplicated(string loginId)
        {
            bool result = false;
            var db = _context.People.Where(p => p.UserId == loginId).ToList();

            if (db.Count > 0) {
                result = true;
            } 
            return result;
        }
        
        [HttpPost]
        [Route("InsertFireWorker")]
        public ActionResult<JobResultWithId> InsertFireWorker([FromBody] Person dto)
        {
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                var data = _context.People.Where(f => f.UserId == dto.UserId).ToList();

                if (data.Count > 0) {
                    result.result.IsSuccess = false;
                    result.result.Message = "msg_id_duplicated";
                }
                else 
                {
                    _context.People.Add(dto);
                    _context.SaveChanges();

                    Notification notification = new Notification();
                    notification.UserId = dto.UserId;
                    notification.NotificationType = "1";
                    notification.RelatedId = dto.PersonId;
                    notification.CreatedDate = DateTime.Now.ToString();
                    notification.State = "0";

                    _context.Notifications.Add(notification);
                    _context.SaveChanges();

                    result.result.IsSuccess = true;
                    result.result.Message = "msg_success_to_insert";
                    result.ID = dto.PersonId;
                }
                
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }
            
    
            return CreatedAtAction(nameof(InsertFireWorker), result);
        }

        [HttpPost]
        [Route("UpdateFireWorker")]
        public ActionResult<JobResultWithId> UpdateFireWorker([FromBody] Person dto)
        {
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                _context.People.Update(dto);
                _context.SaveChanges();
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_update";
                result.ID = dto.PersonId;
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }
            
    
            return CreatedAtAction(nameof(UpdateFireWorker), result);
        }

        [HttpPost("DeleteFireWorker")]
        public ActionResult<CallResult> DeleteFireWorker([FromBody] IList<int> Ids )
        {
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                var FireWorkers = _context.People.Where(p => Ids.Contains(p.PersonId)).ToList();
                _context.People.RemoveRange(FireWorkers);
                _context.SaveChanges();
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_delete";
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }

            return CreatedAtAction(nameof(DeleteFireWorker), result);
        }

        [HttpGet]
        [Route("GetEducationInfo/{PersonEducationId}")]
        public PersonEducation GetEducationInfo(int PersonEducationId)
        {
            PersonEducation educationInfo = new PersonEducation();
            educationInfo = _context.PersonEducations.Where(p => p.PersonEducationId == PersonEducationId).First();
            return educationInfo;
        }

        [HttpGet]
        [Route("GetEducationList/{personId}/{locale}")]
        public IEnumerable<PersonEducationInfo> GetEducationList(int personId, string locale)
        {
            var db = from educationList in _context.PersonEducations.Where(p => p.PersonId == personId).OrderBy(o => o.PersonEducationId)
                     join a in _context.Codes.Where(w => w.Locale == locale) on educationList.AcademyType equals a.Code1  into AJoin from c1 in AJoin.DefaultIfEmpty()
                     join d in _context.Codes.Where(w => w.Locale == locale) on educationList.Degree equals d.Code1  into DJoin from c2 in DJoin.DefaultIfEmpty()
                      select new PersonEducationInfo {
                          PersonEducationId = educationList.PersonEducationId,
                          PersonId = educationList.PersonId,
                          EntranceDate = educationList.EntranceDate,
                          GraducationDate = educationList.GraducationDate,
                          AcademyType = educationList.AcademyType,
                          AcademyTypeName = c1.Name,    
                          AcademyName = educationList.AcademyName,
                          Major = educationList.Major,
                          Degree = educationList.Degree,
                          DegreeName = c2.Name
                      }; 
            return db;
        }
        
        [HttpPost]
        [Route("InsertEducationInfo")]
        public ActionResult<JobResultWithId> InsertEducationInfo([FromBody] PersonEducation dto)
        {
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                _context.PersonEducations.Add(dto);
                _context.SaveChanges();
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_insert";
                result.ID = dto.PersonEducationId;
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }
            
    
            return CreatedAtAction(nameof(InsertEducationInfo), result);
        }

        [HttpPost]
        [Route("UpdateEducationInfo")]
        public ActionResult<JobResultWithId> UpdateEducationInfo([FromBody] PersonEducation dto)
        {
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                _context.PersonEducations.Update(dto);
                _context.SaveChanges();
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_update";
                result.ID = dto.PersonEducationId;
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }
               
            return CreatedAtAction(nameof(UpdateEducationInfo), result);
        }

        [HttpPost("DeleteEducationInfo")]
        public ActionResult<CallResult> DeleteEducationInfo([FromBody] IList<int> Ids )
        {
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                var educations = _context.PersonEducations.Where(p => Ids.Contains(p.PersonEducationId)).ToList();
                _context.PersonEducations.RemoveRange(educations);
                _context.SaveChanges();
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_delete";
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }

            return CreatedAtAction(nameof(DeleteEducationInfo), result);
        }
        
        [HttpGet]
        [Route("GetLicenseInfo/{PersonLicenseId}")]
        public PersonLicense GetLicenseInfo(int PersonLicenseId)
        {
            PersonLicense licenseInfo = new PersonLicense();
            licenseInfo = _context.PersonLicenses.Where(p => p.PersonLicenseId == PersonLicenseId).First();
            return licenseInfo;
        }

        [HttpGet]
        [Route("GetLicenseList/{personId}")]
        public IEnumerable<PersonLicense> GetLicenseList(int personId)
        {
            var db = _context.PersonLicenses.Where(p => p.PersonId == personId);
            return db.ToList();
        }
        
        [HttpPost]
        [Route("InsertLicenseInfo")]
        public ActionResult<JobResultWithId> InsertLicenseInfo([FromBody] PersonLicense dto)
        {
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                _context.PersonLicenses.Add(dto);
                _context.SaveChanges();
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_insert";
                result.ID = dto.PersonLicenseId;
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }
            
    
            return CreatedAtAction(nameof(InsertLicenseInfo), result);
        }

        [HttpPost]
        [Route("UpdateLicenseInfo")]
        public ActionResult<JobResultWithId> UpdateLicenseInfo([FromBody] PersonLicense dto)
        {
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                _context.PersonLicenses.Update(dto);
                _context.SaveChanges();
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_update";
                result.ID = dto.PersonLicenseId;
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }
            
    
            return CreatedAtAction(nameof(UpdateLicenseInfo), result);
        }

        [HttpPost("DeleteLicenseInfo")]
        public ActionResult<CallResult> DeleteLicenseInfo([FromBody] IList<int> Ids )
        {
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                var licenses = _context.PersonLicenses.Where(p => Ids.Contains(p.PersonLicenseId)).ToList();
                _context.PersonLicenses.RemoveRange(licenses);
                _context.SaveChanges();
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_delete";
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }

            return CreatedAtAction(nameof(DeleteLicenseInfo), result);
        }

        [HttpGet]
        [Route("GetEducationPromotionInfo/{PersonEducationPromotionId}")]
        public PersonEducationPromotion GetEducationPromotionInfo(int PersonEducationPromotionId)
        {
            PersonEducationPromotion educatoinPromotionInfo = new PersonEducationPromotion();
            educatoinPromotionInfo = _context.PersonEducationPromotions.Where(p => p.PersonEducationPromotionId == PersonEducationPromotionId).First();
            return educatoinPromotionInfo;
        }

        [HttpGet]
        [Route("GetEducationPromotionList/{personId}/{locale}")]
        public IEnumerable<PersonEducationPromotionInfo> GetEducationPromotionList(int personId, string locale)
        {
            var db = from educatoinPromotionList in _context.PersonEducationPromotions.Where(p => p.PersonId == personId).OrderBy(o => o.PersonEducationPromotionId)
                     join a in _context.Codes.Where(w => w.Locale == locale) on educatoinPromotionList.EducationClsCode equals a.Code1  into AJoin from c1 in AJoin.DefaultIfEmpty()
                     join d in _context.Codes.Where(w => w.Locale == locale) on educatoinPromotionList.CourseName equals d.Code1  into DJoin from c2 in DJoin.DefaultIfEmpty()
                      select new PersonEducationPromotionInfo {
                          PersonEducationPromotionId = educatoinPromotionList.PersonEducationPromotionId,
                          PersonId = educatoinPromotionList.PersonId,
                          CourseName = educatoinPromotionList.CourseName,
                          InstitutionName = educatoinPromotionList.InstitutionName,
                          StartDate = educatoinPromotionList.StartDate,
                          EndDate = educatoinPromotionList.EndDate,    
                          EducationClsCode = educatoinPromotionList.EducationClsCode,
                          EducationClsCodeName = c1.Name,
                          CompleteCode = educatoinPromotionList.CompleteCode,
                          CompleteCodeName = c2.Name,
                          Score = educatoinPromotionList.Score,
                      }; 

            return db.ToList();
        }
        
        [HttpPost]
        [Route("InsertEducationPromotionInfo")]
        public ActionResult<JobResultWithId> InsertEducationPromotionInfo([FromBody] PersonEducationPromotion dto)
        {
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                _context.PersonEducationPromotions.Add(dto);
                _context.SaveChanges();
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_insert";
                result.ID = dto.PersonEducationPromotionId;
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }
            
    
            return CreatedAtAction(nameof(InsertEducationPromotionInfo), result);
        }

        [HttpPost]
        [Route("UpdateEducationPromotionInfo")]
        public ActionResult<JobResultWithId> UpdateEducationPromotionInfo([FromBody] PersonEducationPromotion dto)
        {
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                _context.PersonEducationPromotions.Update(dto);
                _context.SaveChanges();
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_update";
                result.ID = dto.PersonEducationPromotionId;
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }
            
    
            return CreatedAtAction(nameof(UpdateEducationPromotionInfo), result);
        }

        [HttpPost("DeleteEducationPromotionInfo")]
        public ActionResult<CallResult> DeleteEducationPromotionInfo([FromBody] IList<int> Ids )
        {
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                var educationPromotions = _context.PersonEducationPromotions.Where(p => Ids.Contains(p.PersonEducationPromotionId)).ToList();
                _context.PersonEducationPromotions.RemoveRange(educationPromotions);
                _context.SaveChanges();
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_delete";
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }

            return CreatedAtAction(nameof(DeleteEducationPromotionInfo), result);
        }

        [HttpGet]
        [Route("GetFireTrainInfo/{PersonFireTrainId}")]
        public PersonFireTrain GetFireTrainInfo(int PersonFireTrainId)
        {
            PersonFireTrain fireTrainInfo = new PersonFireTrain();
            fireTrainInfo = _context.PersonFireTrains.Where(p => p.PersonFireTrainId == PersonFireTrainId).First();
            return fireTrainInfo;
        }

        [HttpGet]
        [Route("GetFireTrainList/{personId}")]
        public IEnumerable<PersonFireTrain> GetFireTrainList(int personId)
        {
            var db = _context.PersonFireTrains.Where(p => p.PersonId == personId);
            return db.ToList();
        }
        
        [HttpPost]
        [Route("InsertFireTrainInfo")]
        public ActionResult<JobResultWithId> InsertFireTrainInfo([FromBody] PersonFireTrain dto)
        {
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                _context.PersonFireTrains.Add(dto);
                _context.SaveChanges();
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_insert";
                result.ID = dto.PersonFireTrainId;
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }
            
    
            return CreatedAtAction(nameof(InsertFireTrainInfo), result);
        }

        [HttpPost]
        [Route("UpdateFireTrainInfo")]
        public ActionResult<JobResultWithId> UpdateFireTrainInfo([FromBody] PersonFireTrain dto)
        {
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                _context.PersonFireTrains.Update(dto);
                _context.SaveChanges();
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_update";
                result.ID = dto.PersonFireTrainId;
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }
                
            return CreatedAtAction(nameof(UpdateFireTrainInfo), result);
        }

        [HttpPost("DeleteFireTrainInfo")]
        public ActionResult<CallResult> DeleteFireTrainInfo([FromBody] IList<int> Ids )
        {
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                var fireTrains = _context.PersonFireTrains.Where(p => Ids.Contains(p.PersonFireTrainId)).ToList();
                _context.PersonFireTrains.RemoveRange(fireTrains);
                _context.SaveChanges();
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_delete";
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }

            return CreatedAtAction(nameof(DeleteFireTrainInfo), result);
        }
    }
}