using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Safer.model;
using Safer.model.dto;

namespace Safer.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserController : ControllerBase
    {
        private readonly saferContext _context;

        public UserController(saferContext context)
        {
            _context = context;
        }
        
        [HttpPost]
        [Route("LogIn")]
        public ActionResult<UserInfo> LogIn([FromBody] LogIn dto)
        {
            string userId = dto.LoginId;
            string Password = dto.Password;
            UserInfo userInfo = new UserInfo();
            CallResult result = new CallResult();
  
            var data = _context.People.Where(f => f.UserId == userId && f.Password == Password).Where(f => f.IsApproved == "1").ToList();

            if (data.Count > 0)
            {
                var UserPreference = _context.UserPreferences.Where(f => f.UserId == userId).FirstOrDefault();

                result.IsSuccess = true;
                result.Message = "Succee to Login.";
                userInfo.userInfo = data.First();
                userInfo.userPreference = UserPreference;
                userInfo.result = result;

                userInfo.userAuth =_context.UserAuths.Where(f => f.UserId == userId).Where(f => f.IsApproved == "1")
                               .Join(
                                     _context.ViewPrivileges,
                                         auth => auth.AuthId,
                                         view => view.AuthId,
                                         (auth,view) => new {
                                                                viewId = view.ViewId, privilige = view.Privilege
                                                            }
                                ).GroupBy(g => new {
                                    g.viewId
                                }).Select(s => new Safer.model.dto.UserAuth{
                                    ViewId = s.Key.viewId,
                                    Privilege = s.Max(e => e.privilige)
                                }).ToList();
            }
            else
            {
                result.IsSuccess = false;
                result.Message = "Fail to Login.";
                userInfo.result = result;
            }
            
            return CreatedAtAction(nameof(LogIn), userInfo);
        }
    }
}