using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Safer.model;
using Safer.model.dto;

namespace Safer.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserPreferenceController : ControllerBase
    {
        private readonly saferContext _context;

        public UserPreferenceController(saferContext context)
        {
            _context = context;
        }

        [HttpGet]
        [Route("GetUserPreference/{userId}")]
        public ActionResult<UserPreference> GetUserPreference(string userId)
        {
            return _context.UserPreferences.Where(p => p.UserId == userId).FirstOrDefault();
        }

        [HttpPost]
        [Route("UpsertUserPreference")]
        public ActionResult<JobResultWithId> UpsertUserPreference([FromBody] UserPreference dto)
        {
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                if (dto.UserPreferenceId == null) 
                {
                    _context.UserPreferences.Add(dto);
                    _context.SaveChanges();
                    result.result.IsSuccess = true;
                    result.result.Message = "msg_success_to_update";
                    result.ID = dto.UserPreferenceId;
                }
                else
                {
                    _context.UserPreferences.Update(dto);
                    _context.SaveChanges();
                    result.result.IsSuccess = true;
                    result.result.Message = "msg_success_to_update";
                    result.ID = dto.UserPreferenceId;
                }
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }
            
            return CreatedAtAction(nameof(UpsertUserPreference), result);
        }
    }
}