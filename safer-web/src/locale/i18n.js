import i18n from "i18next";
import detector from "i18next-browser-languagedetector";
import { initReactI18next } from "react-i18next";

import translationEn from './translation.en';
import translationKo from './translation.kr';

// the translations
const resources = {
  en: {
    translation: translationEn
  },
  kr: {
    translation: translationKo
  }
};

i18n
  .use(detector)
  .use(initReactI18next) 
  .init({
    resources,
    fallbackLng: ['kr', 'en'],
    debug: true,
    keySeparator: false, 
    interpolation: {
      escapeValue: false 
    },
    detection: { order: ['path', 'navigator'] }
  });

export default i18n;