import Dictionary from '../common/Dictionary';

const dictionary = new Dictionary();
class ServiceList {
    login = {
                "url" : "/user/login",
                "method" : dictionary.POST,
                "body" : {
                            "loginId":dictionary.STRING,
                            "password":dictionary.STRING
                        },
                "response" : {
                    "result": {
                        "isSuccess": dictionary.BOOLEAN,
                        "message": dictionary.STRING
                    },
                    "userInfo": {
                        "personId": dictionary.NUMERIC,
                        "userId": dictionary.STRING,
                        "wardId": dictionary.NUMERIC,
                        "email": dictionary.STRING
                    },
                    "userPreference": {
                        "userPreferenceId":dictionary.NUMERIC,
                        "language": dictionary.STRING,
                        "color": dictionary.STRING,
                        "theme": dictionary.STRING,
                        "tableRows": dictionary.NUMERIC,
                        "tableDense": dictionary.BOOLEAN
                    },
                    "userAuth": []
                }
            };
    getWardList = {
                "url" : "/ward/wardlist",
                "method" : dictionary.POST,
                "body" : {
                            "page" : {
                                        "rowsPerPage" : dictionary.NUMERIC,
                                        "pageNumber" : dictionary.NUMERIC
                                    },
                            "upperWardId" : dictionary.NUMERIC,
                            "wardId" : dictionary.NUMERIC,
                            "typeCode" : dictionary.STRING,
                            "locale" : dictionary.STRING
                        },
                "response" : {
                            "page" : {
                                        "rowsPerPage" : dictionary.NUMERIC,
                                        "pageNumber" : dictionary.NUMERIC,
                                        "totalCount" : dictionary.NUMERIC
                                    },
                            "rows" : {
                                "wardId": dictionary.NUMERIC,
                                "upperWardName": dictionary.STRING,
                                "wardName": dictionary.STRING,
                                "typeCode": dictionary.STRING,
                                "abbrName": dictionary.STRING,
                                "phoneNumber": dictionary.STRING,
                                "address": dictionary.STRING,
                                "isBrodcastingUse":dictionary.BOOLEANUseOrNot
                            }
                }
            };
    getUpperwardList = {
                "url" : "/ward/getupperwardlist",
                "method" : dictionary.GET,
                "body" : [], //Ward ids
                "response" : {
                    "wardId": dictionary.NUMERIC,
                    "wardName": dictionary.STRING
                }
            };
    GetWardListForUpperOffice = {
                "url" : "/ward/getwardlistforupperoffice/{id}",
                "method" : dictionary.GET,
                "body" : [], //Ward ids
                "response" : {
                    "wardId": dictionary.NUMERIC,
                    "wardName": dictionary.STRING
                }
            };
    getWardInfo = {
                "url" : "/ward/getwardinfo/{id}",
                "method" : dictionary.GET,
                "body" : {},
                "response" : {
                                "wardId": dictionary.NUMERIC,
                                "typeCode": dictionary.NUMERIC,
                                "wardName": dictionary.STRING,
                                "upperWardId": dictionary.NUMERIC,
                                "abbrName": dictionary.STRING,
                                "isBrodcastingUse": dictionary.BOOLEAN,
                                "isUse": dictionary.BOOLEAN,
                                "extNumber": dictionary.STRING,
                                "phoneNumber": dictionary.STRING,
                                "faxNumber": dictionary.STRING,
                                "postalCode": dictionary.NUMERIC,
                                "etcAddress": dictionary.STRING,
                                "openDate": dictionary.DATE,
                                "closeDate": dictionary.DATE,
                                "remark": dictionary.STRING,
                            }
            };
    insertWard = {
                "url" : "/ward/insertward",
                "method" : dictionary.POST,
                "body" : {
                            "wardId": dictionary.NUMERIC,
                            "typeCode": dictionary.NUMERIC,
                            "wardName": dictionary.STRING,
                            "upperWardId": dictionary.NUMERIC,
                            "abbrName": dictionary.STRING,
                            "isBrodcastingUse": dictionary.BOOLEAN,
                            "isUse": dictionary.BOOLEAN,
                            "extNumber": dictionary.STRING,
                            "phoneNumber": dictionary.STRING,
                            "faxNumber": dictionary.STRING,
                            "postalCode": dictionary.NUMERIC,
                            "etcAddress": dictionary.STRING,
                            "openDate": dictionary.DATE,
                            "closeDate": dictionary.DATE,
                            "remark": dictionary.STRING,
                        },
                "response" : {
                    "result": {
                        "isSuccess": dictionary.BOOLEAN,
                        "message": dictionary.STRING,
                        "ID":dictionary.NUMERIC
                    }
                }
            };
    updateWard = {
                "url" : "/ward/updateward",
                "method" : dictionary.POST,
                "body" : {
                            "wardId": dictionary.NUMERIC,
                            "typeCode": dictionary.NUMERIC,
                            "wardName": dictionary.STRING,
                            "upperWardId": dictionary.NUMERIC,
                            "abbrName": dictionary.STRING,
                            "isBrodcastingUse": dictionary.BOOLEAN,
                            "isUse": dictionary.BOOLEAN,
                            "extNumber": dictionary.STRING,
                            "phoneNumber": dictionary.STRING,
                            "faxNumber": dictionary.STRING,
                            "postalCode": dictionary.NUMERIC,
                            "etcAddress": dictionary.STRING,
                            "openDate": dictionary.DATE,
                            "closeDate": dictionary.DATE,
                            "remark": dictionary.STRING,
                        },
                "response" : {
                    "result": {
                        "isSuccess": dictionary.BOOLEAN,
                        "message": dictionary.STRING,
                        "ID":dictionary.NUMERIC
                    }
                }
            };
    deleteWard = {
                "url" : "/ward/deleteward",
                "method" : dictionary.POST,
                "body" : [], //Ward ids
                "response" : {
                    "result": {
                        "isSuccess": dictionary.BOOLEAN,
                        "message": dictionary.STRING
                    }
                }
            };
    getGroupCodes = {
                "url" : "/common/getgroupcode/{parentGroupCode}/{locale}",
                "method" : dictionary.GET,
                "body" : [], 
                "response" : {
                    "groupCode": dictionary.STRING,
                    "level": dictionary.NUMERIC,
                    "name": dictionary.STRING
                }
            };
    getCodesForGroupCode = {
                "url" : "/common/getcodes/{groupCode}/{locale}",
                "method" : dictionary.GET,
                "body" : [], 
                "response" : {
                    "code1": dictionary.STRING,
                    "name": dictionary.STRING,
                    "description": dictionary.STRING
                }
            };
    getUserPreference = {
                "url" : "/userpreference/getuserpreference/{userId}",
                "method" : dictionary.GET,
                "body" : [], 
                "response" : {
                    "userPreferenceId": dictionary.NUMERIC,
                    "userId": dictionary.STRING,
                    "language": dictionary.STRING,
                    "theme": dictionary.STRING,
                    "tableRows": dictionary.NUMERIC,
                    "tableDense": dictionary.BOOLEAN,
                    "color": dictionary.STRING
                }
            };
    upsertUserPreference = {
                "url" : "/userpreference/upsertuserpreference",
                "method" : dictionary.POST,
                "body" : {
                            "userPreferenceId":dictionary.NUMERIC,
                            "userId": dictionary.STRING,
                            "language": dictionary.STRING,
                            "color": dictionary.STRING,
                            "theme": dictionary.STRING,
                            "tableRows": dictionary.INT,
                            "tableDense": dictionary.BOOLEAN
                        },
                "response" : {
                            "result": {
                                "isSuccess": dictionary.BOOLEAN,
                                "message": dictionary.STRING,
                                "ID":dictionary.NUMERIC
                            }
                }
            };
    getFireWorkerList = {
                "url" : "/humanresource/getfireworkers",
                "method" : dictionary.POST,
                "body" : {
                            "page" : {
                                        "rowsPerPage" : dictionary.NUMERIC,
                                        "pageNumber" : dictionary.NUMERIC
                                    },
                            "upperWardId" : dictionary.NUMERIC,
                            "wardId" : dictionary.NUMERIC,
                            "personName" : dictionary.STRING,
                            "positionCode" : dictionary.NUMERIC,
                            "userId" : dictionary.STRING
                        },
                "response" : {
                            "page" : {
                                        "rowsPerPage" : dictionary.NUMERIC,
                                        "pageNumber" : dictionary.NUMERIC,
                                        "totalCount" : dictionary.NUMERIC
                                    },
                            "rows" : {
                                "personId": dictionary.NUMERIC,
                                "upperWardName": dictionary.STRING,
                                "wardName": dictionary.STRING,
                                "position": dictionary.STRING,
                                "rank": dictionary.STRING,
                                "job": dictionary.STRING,
                                "personName": dictionary.STRING,
                                "userId":dictionary.STRING,
                            }
                }
            };
    getFireWorker = {
        "url" : "/humanresource/getfireworker/{personId}",
        "method" : dictionary.GET,
        "body" : [], 
        "response" : {
            "personId": dictionary.NUMERIC,
            "userId": dictionary.STRING,
            "gender": dictionary.STRING,
            "birthDay": dictionary.DATE,
            "enrollDate": dictionary.DATE,
            "isUse": dictionary.BOOLEAN,
            "isSystemAdministrator":dictionary.BOOLEAN,
            "notUseReason": dictionary.STRING,
            "postalCode": dictionary.NUMERIC,
            "etcAddress": dictionary.STRING,
            "upperWardId":dictionary.NUMERIC,
            "wardId":dictionary.NUMERIC,
            "positionCode":dictionary.NUMERIC,
            "rankCode":dictionary.NUMERIC,
            "jobCode":dictionary.NUMERIC,
            "workTypeCode":dictionary.NUMERIC,
            "homeTel":dictionary.STRING,
            "mobileNum":dictionary.STRING,
            "workTel":dictionary.STRING,
            "extNum":dictionary.STRING,
            "faxNum":dictionary.STRING,
            "email":dictionary.STRING,
            "firstName":dictionary.STRING,
            "lastName":dictionary.STRING
            }
        };
    insertFireWorker = {
            "url" : "/humanresource/insertfireworker",
            "method" : dictionary.POST,
            "body" : {
                "personId": dictionary.NUMERIC,
                "userId": dictionary.STRING,
                "password": dictionary.STRING,
                "gender": dictionary.STRING,
                "birthDay": dictionary.DATE,
                "enrollDate": dictionary.DATE,
                "isSystemAdministrator":dictionary.BOOLEAN,
                "isUse":dictionary.BOOLEAN,
                "postalCode": dictionary.NUMERIC,
                "etcAddress": dictionary.STRING,
                "wardId":dictionary.NUMERIC,
                "positionCode":dictionary.NUMERIC,
                "rankCode":dictionary.NUMERIC,
                "jobCode":dictionary.NUMERIC,
                "workTypeCode":dictionary.NUMERIC,
                "homeTel":dictionary.STRING,
                "mobileNum":dictionary.STRING,
                "workTel":dictionary.STRING,
                "extNum":dictionary.STRING,
                "faxNum":dictionary.STRING,
                "email":dictionary.STRING,
                "firstName":dictionary.STRING,
                "lastName":dictionary.STRING,
                "isApproved":dictionary.BOOLEAN
                },
            "response" : {
                "result": {
                    "isSuccess": dictionary.BOOLEAN,
                    "message": dictionary.STRING,
                    "ID":dictionary.NUMERIC
                }
            }
        };
    updateFireWorker = {
            "url" : "/humanresource/updatefireworker",
            "method" : dictionary.POST,
            "body" : {
                    "personId": dictionary.NUMERIC,
                    "gender": dictionary.STRING,
                    "birthDay": dictionary.DATE,
                    "enrollDate": dictionary.DATE,
                    "isUse": dictionary.BOOLEAN,
                    "isSystemAdministrator":dictionary.BOOLEAN,
                    "notUseReason": dictionary.STRING,
                    "postalCode": dictionary.NUMERIC,
                    "etcAddress": dictionary.STRING,
                    "wardId":dictionary.NUMERIC,
                    "positionCode":dictionary.NUMERIC,
                    "rankCode":dictionary.NUMERIC,
                    "jobCode":dictionary.NUMERIC,
                    "workTypeCode":dictionary.NUMERIC,
                    "homeTel":dictionary.STRING,
                    "mobileNum":dictionary.STRING,
                    "workTel":dictionary.STRING,
                    "extNum":dictionary.STRING,
                    "faxNum":dictionary.STRING,
                    "email":dictionary.STRING,
                    "firstName":dictionary.STRING,
                    "lastName":dictionary.STRING
                    },
            "response" : {
                "result": {
                    "isSuccess": dictionary.BOOLEAN,
                    "message": dictionary.STRING,
                    "ID":dictionary.NUMERIC
                }
            }
        };
    deleteFireWorker = {
            "url" : "/humanresource/deletefireworker",
            "method" : dictionary.POST,
            "body" : [], //Ward ids
            "response" : {
                "result": {
                    "isSuccess": dictionary.BOOLEAN,
                    "message": dictionary.STRING
                }
            }
        };
    
    getCheckLoginIdDuplicated = {
            "url" : "/humanresource/checkloginidduplicated/{loginId}",
            "method" : dictionary.GET,
            "body" : [], 
            "response" : dictionary.BOOLEAN
        };
            
    getNotificationList = {
            "url" : "/common/getnotification",
            "method" : dictionary.GET,
            "body" : [], 
            "response" : {
                "notificationId": dictionary.NUMERIC,
                "notificationType": dictionary.STRING,
                "relatedId": dictionary.NUMERIC,
                "state": dictionary.STRING,
                "createdDate": dictionary.STRING,
                "userId": dictionary.STRING,
                "firstName": dictionary.STRING,
                "lastName": dictionary.STRING,
                "wardName": dictionary.STRING
            }
        };
    approveNotification = {
            "url" : "/common/approvenotification",
            "method" : dictionary.POST,
            "body" : [], 
            "response" : {
                "result": {
                    "isSuccess": dictionary.BOOLEAN,
                    "message": dictionary.STRING
                }
            }
        };
    rejectNotification = {
            "url" : "/common/rejectnotification",
            "method" : dictionary.POST,
            "body" : [], 
            "response" : {
                "result": {
                    "isSuccess": dictionary.BOOLEAN,
                    "message": dictionary.STRING
                }
            }
        };
    getPersonList = {
            "url" : "/humanresource/getpersonlist",
            "method" : dictionary.POST,
            "body" : {
                        "page" : {
                                    "rowsPerPage" : dictionary.NUMERIC,
                                    "pageNumber" : dictionary.NUMERIC
                                },
                        "wardId" : dictionary.NUMERIC,
                        "firstName" : dictionary.STRING,
                        "lastName" : dictionary.STRING,
                        "userId" : dictionary.STRING,
                        "positionCode" : dictionary.NUMERIC,
                        "rankCode" : dictionary.NUMERIC,
                        "jobCode" : dictionary.NUMERIC,
                        "workTypeCode" : dictionary.NUMERIC,
                        "locale" : dictionary.STRING
                    },
            "response" : {
                        "page" : {
                                    "rowsPerPage" : dictionary.NUMERIC,
                                    "pageNumber" : dictionary.NUMERIC,
                                    "totalCount" : dictionary.NUMERIC
                                },
                        "rows" : {
                            "personId": dictionary.NUMERIC,
                            "upperWardName": dictionary.STRING,
                            "wardName": dictionary.STRING,
                            "personName": dictionary.STRING,
                            "userId": dictionary.STRING,
                            "workTel": dictionary.STRING,
                            "mobileNum": dictionary.STRING,
                            "position": dictionary.STRING,
                            "rank": dictionary.STRING,
                            "job": dictionary.STRING,
                            "workType": dictionary.STRING
                        }
            }
        };
    getPersonEducationList = {
            "url" : "/humanresource/geteducationlist/{personId}/{locale}",
            "method" : dictionary.GET,
            "body" : {},
            "response" : {
                        "rows" : {
                            "personEducationId": dictionary.NUMERIC,
                            "personId": dictionary.NUMERIC,
                            "entranceDate": dictionary.DATE,
                            "graducationDate": dictionary.DATE,
                            "academyName": dictionary.STRING,
                            "major": dictionary.STRING,
                            "degreeName":dictionary.STRING,
                            "academyTypeName": dictionary.STRING
                        }
            }
        };
    getPersonEducationInfo = {
            "url" : "/humanresource/geteducationinfo/{personEducationId}",
            "method" : dictionary.GET,
            "body" : [], 
            "response" : {
                "personEducationId": dictionary.NUMERIC,
                "personId": dictionary.NUMERIC,
                "entranceDate": dictionary.DATE,
                "graducationDate": dictionary.DATE,
                "academyName": dictionary.STRING,
                "major": dictionary.STRING,
                "degree":dictionary.STRING,
                "academyType": dictionary.STRING
                }
            };
    insertPersonEducation = {
            "url" : "/humanresource/inserteducationinfo",
            "method" : dictionary.POST,
            "body" : {
                "personEducationId": dictionary.NUMERIC,
                "personId": dictionary.NUMERIC,
                "entranceDate": dictionary.DATE,
                "graducationDate": dictionary.DATE,
                "academyName": dictionary.STRING,
                "major": dictionary.STRING,
                "degree":dictionary.STRING,
                "academyType": dictionary.STRING
                    },
            "response" : {
                "result": {
                    "isSuccess": dictionary.BOOLEAN,
                    "message": dictionary.STRING,
                    "ID":dictionary.NUMERIC
                }
            }
    };
    updatePersonEducation = {
        "url" : "/humanresource/updateeducationinfo",
        "method" : dictionary.POST,
        "body" : {
            "personEducationId": dictionary.NUMERIC,
            "personId": dictionary.NUMERIC,
            "entranceDate": dictionary.DATE,
            "graducationDate": dictionary.DATE,
            "academyName": dictionary.STRING,
            "major": dictionary.STRING,
            "degree":dictionary.STRING,
            "academyType": dictionary.STRING
                },
        "response" : {
            "result": {
                "isSuccess": dictionary.BOOLEAN,
                "message": dictionary.STRING,
                "ID":dictionary.NUMERIC
            }
        }
    };
    deletePersonEducation = {
        "url" : "/humanresource/deleteeducationinfo",
        "method" : dictionary.POST,
        "body" : [], //personEducation ids
        "response" : {
            "result": {
                "isSuccess": dictionary.BOOLEAN,
                "message": dictionary.STRING
            }
        }
    };
    getPersonLicenseList = {
        "url" : "/humanresource/getlicenselist/{personId}",
        "method" : dictionary.GET,
        "body" : {},
        "response" : {
                    "rows" : {
                        "personLicenseId": dictionary.NUMERIC,
                        "personId": dictionary.NUMERIC,
                        "licenseNumber": dictionary.STRING,
                        "licenseName": dictionary.STRING,
                        "acquisitionDate": dictionary.DATE
                    }
        }
    };
    getPersonLicenseInfo = {
        "url" : "/humanresource/getlicenseinfo/{personLicenseId}",
        "method" : dictionary.GET,
        "body" : [], 
        "response" : {
            "personLicenseId": dictionary.NUMERIC,
            "personId": dictionary.NUMERIC,
            "licenseNumber": dictionary.STRING,
            "licenseName": dictionary.STRING,
            "acquisitionDate": dictionary.DATE
            }
        };
    insertPersonLicense = {
        "url" : "/humanresource/insertlicenseinfo",
        "method" : dictionary.POST,
        "body" : {
            "personLicenseId": dictionary.NUMERIC,
            "personId": dictionary.NUMERIC,
            "licenseNumber": dictionary.STRING,
            "licenseName": dictionary.STRING,
            "acquisitionDate": dictionary.DATE
        },
        "response" : {
            "result": {
                "isSuccess": dictionary.BOOLEAN,
                "message": dictionary.STRING,
                "ID":dictionary.NUMERIC
            }
        }
    };
    updatePersonLicense = {
        "url" : "/humanresource/updatelicenseinfo",
        "method" : dictionary.POST,
        "body" : {
            "personLicenseId": dictionary.NUMERIC,
            "personId": dictionary.NUMERIC,
            "licenseNumber": dictionary.STRING,
            "licenseName": dictionary.STRING,
            "acquisitionDate": dictionary.DATE
        },
        "response" : {
            "result": {
                "isSuccess": dictionary.BOOLEAN,
                "message": dictionary.STRING,
                "ID":dictionary.NUMERIC
            }
        }
    };
    deletePersonLicense = {
        "url" : "/humanresource/deletelicenseinfo",
        "method" : dictionary.POST,
        "body" : [], //personLicense ids
        "response" : {
            "result": {
                "isSuccess": dictionary.BOOLEAN,
                "message": dictionary.STRING
            }
        }
    };
    getPersonLearningList = {
        "url" : "/humanresource/Geteducationpromotionlist/{personId}/{locale}",
        "method" : dictionary.GET,
        "body" : {},
        "response" : {
                    "rows" : {
                        "personEducationPromotionId": dictionary.NUMERIC,
						"personId": dictionary.NUMERIC,
						"courseName": dictionary.STRING,
						"institutionName": dictionary.STRING,
						"startDate": dictionary.DATE,
						"endDate": dictionary.DATE,
						"educationClsCode": dictionary.STRING,
						"educationClsCodeName": dictionary.STRING,
						"completeCode": dictionary.STRING,
						"completeCodeName": dictionary.STRING,
						"score": dictionary.STRING
                    }
        }
    };
    getPersonLearningInfo = {
        "url" : "/humanresource/geteducationpromotioninfo/{personEducationPromotionId}",
        "method" : dictionary.GET,
        "body" : [], 
        "response" : {
            "personEducationPromotionId": dictionary.NUMERIC,
            "personId": dictionary.NUMERIC,
            "courseName": dictionary.STRING,
            "institutionName": dictionary.STRING,
            "startDate": dictionary.DATE,
            "endDate": dictionary.DATE,
            "educationClsCode": dictionary.STRING,
            "completeCode": dictionary.STRING,
            "score": dictionary.STRING
            }
        };
    insertPersonLearningInfo = {
        "url" : "/humanresource/Inserteducationpromotioninfo",
        "method" : dictionary.POST,
        "body" : {
            "personEducationPromotionId": dictionary.NUMERIC,
            "personId": dictionary.NUMERIC,
            "courseName": dictionary.STRING,
            "institutionName": dictionary.STRING,
            "startDate": dictionary.DATE,
            "endDate": dictionary.DATE,
            "educationClsCode": dictionary.STRING,
            "completeCode": dictionary.STRING,
            "score": dictionary.STRING
        },
        "response" : {
            "result": {
                "isSuccess": dictionary.BOOLEAN,
                "message": dictionary.STRING,
                "ID":dictionary.NUMERIC
            }
        }
    };
    updatePersonLearningInfo = {
        "url" : "/humanresource/updateeducationpromotioninfo",
        "method" : dictionary.POST,
        "body" : {
            "personEducationPromotionId": dictionary.NUMERIC,
            "personId": dictionary.NUMERIC,
            "courseName": dictionary.STRING,
            "institutionName": dictionary.STRING,
            "startDate": dictionary.DATE,
            "endDate": dictionary.DATE,
            "educationClsCode": dictionary.STRING,
            "completeCode": dictionary.STRING,
            "score": dictionary.STRING
        },
        "response" : {
            "result": {
                "isSuccess": dictionary.BOOLEAN,
                "message": dictionary.STRING,
                "ID":dictionary.NUMERIC
            }
        }
    };
    deletePersonLearningInfo = {
        "url" : "/humanresource/deleteeducationpromotioninfo",
        "method" : dictionary.POST,
        "body" : [], //personEducationPromotion ids
        "response" : {
            "result": {
                "isSuccess": dictionary.BOOLEAN,
                "message": dictionary.STRING
            }
        }
    };
    getPersonFireTrainList = {
        "url" : "/humanresource/getfiretrainlist/{personId}",
        "method" : dictionary.GET,
        "body" : {},
        "response" : {
                    "rows" : {
                        "personFireTrainId": dictionary.NUMERIC,
						"personId": dictionary.NUMERIC,
						"fireTrainName": dictionary.STRING,
						"startDate": dictionary.DATE,
						"endDate": dictionary.DATE,
						"organizationName": dictionary.STRING,
                        "host": dictionary.STRING,
						"perf": dictionary.STRING
                    }
        }
    };
    getPersonFireTrainInfo = {
        "url" : "/humanresource/getfiretraininfo/{personFireTrainId}",
        "method" : dictionary.GET,
        "body" : [], 
        "response" : {
            "personFireTrainId": dictionary.NUMERIC,
			"personId": dictionary.NUMERIC,
			"fireTrainName": dictionary.STRING,
			"startDate": dictionary.DATE,
			"endDate": dictionary.DATE,
            "organizationName": dictionary.STRING,
			"host": dictionary.STRING,
			"perf": dictionary.STRING
            }
        };
    insertPersonFireTrainInfo = {
        "url" : "/humanresource/insertfiretrainInfo",
        "method" : dictionary.POST,
        "body" : {
            "personFireTrainId": dictionary.NUMERIC,
			"personId": dictionary.NUMERIC,
			"fireTrainName": dictionary.STRING,
			"startDate": dictionary.DATE,
			"endDate": dictionary.DATE,
            "organizationName": dictionary.STRING,
			"host": dictionary.STRING,
			"perf": dictionary.STRING
        },
        "response" : {
            "result": {
                "isSuccess": dictionary.BOOLEAN,
                "message": dictionary.STRING,
                "ID":dictionary.NUMERIC
            }
        }
    };
    updatePersonFireTrainInfo = {
        "url" : "/humanresource/updatefiretraininfo",
        "method" : dictionary.POST,
        "body" : {
            "personFireTrainId": dictionary.NUMERIC,
			"personId": dictionary.NUMERIC,
			"fireTrainName": dictionary.STRING,
			"startDate": dictionary.DATE,
			"endDate": dictionary.DATE,
            "organizationName": dictionary.STRING,
			"host": dictionary.STRING,
			"perf": dictionary.STRING
        },
        "response" : {
            "result": {
                "isSuccess": dictionary.BOOLEAN,
                "message": dictionary.STRING,
                "ID":dictionary.NUMERIC
            }
        }
    };
    deletePersonFireTrainInfo = {
        "url" : "/humanresource/deletefiretraininfo",
        "method" : dictionary.POST,
        "body" : [], //personEducationPromotion ids
        "response" : {
            "result": {
                "isSuccess": dictionary.BOOLEAN,
                "message": dictionary.STRING
            }
        }
    };
    getNextGroupCodes = {
        "url" : "/systemmanage/getnextgroupcode/{groupCode}/{locale}",
        "method" : dictionary.GET,
        "body" : {},
        "response" : {
            "groupCodeTitle":dictionary.STRING,
            "rows" : {
                "cdGrp": dictionary.STRING,
			    "cdGrpName": dictionary.STRING,
			    "cdGrpDesc": dictionary.STRING,
			    "isLeaf": dictionary.BOOLEAN
            }
            
        }
    };
    getCodesForGroupCode = {
        "url" : "/systemmanage/getcodesforgroupcode/{groupCode}/{locale}",
        "method" : dictionary.GET,
        "body" : {},
        "response" : {
            "cd": dictionary.NUMERIC,
			"cdName": dictionary.STRING,
			"cdDesc": dictionary.STRING
        }
    };
}

export default ServiceList;