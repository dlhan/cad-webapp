import axios from 'axios';
import Dictionary from '../common/Dictionary';
import { SetBody, StringUrlFormatFromJson } from '../common/Utility';

const dictionary = new Dictionary();

function RunService(service, body, skipCheck = false) {
    switch (service.method) {
        case dictionary.POST :
            var requestBody = body;
            if (!skipCheck) {
                requestBody = SetBody(service.body, body);
            }
            
            return axios.post(service.url, requestBody);
        case dictionary.GET :
            var url = service.url;
            if (body) {
                url = StringUrlFormatFromJson(service.url, body);
            }
            
            return axios.get(url);
        case dictionary.DELETE :
            return axios.delete(service.url, body);
        default :
            return;
    }
}

export { RunService };

