import React from 'react';
import { FormStyle } from '../style/Style';
import TextField from '@material-ui/core/TextField';

const useStyles = FormStyle;

export default function CustomizedTextField(props) {
  const { id, label, className ,onChange,type ,disabled, value, required, multiline, fullWidth, rows, variant, defaultValue,max,min,helperText,error } = props;
  const classes = useStyles();

  let textType = type ? type : "text";

  return (
    <React.Fragment>
      <TextField
        id={id}
        name={id}
        type={textType}
        label={label}
        className={className ? className : classes.textField}
        multiline={multiline}
        required={required}
        rows={rows}
        variant={variant}
        onChange={onChange}
        defaultValue={defaultValue}
        disabled={disabled}
        value={value}
        fullWidth={fullWidth}
        max={max}
        min={min}
        InputLabelProps={{
          shrink: true,
        }}
        helperText={helperText}
        error={error}
      />
    </React.Fragment>
  );
}
