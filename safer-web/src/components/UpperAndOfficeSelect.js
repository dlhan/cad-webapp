import React, { useState, useEffect } from 'react';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import NativeSelect from '@material-ui/core/NativeSelect';
import { GetText } from '../common/BundleManager'
import clsx from 'clsx';
import { FormStyle } from '../style/Style';
import serviceList from '../service/ServiceList';
import { RunService } from '../service/RestApi';
import { ResetForResponseData } from '../common/Utility';

const useStyles = FormStyle;

export default function UpperAndOfficeSelect({upperWardOnly, upperWardId, wardid, callEvent, className, selectedUpperWardValue, selectedWardValue, disabled, checkDataToRefresh}) {
  const classes = useStyles();
  const s = new serviceList();

  const [upperWards, setUpperWards] = useState([]);
  const [subWards, setSubWards] = useState([]);
  let pUpwardId = upperWardId ? upperWardId : "upperWardId";
  let pWardId = wardid ? wardid : "wardId";

  const handleChange = (event) => {
    const name = event.target.name;
    callEvent(event);
    if (name === pUpwardId) {
      getSubOffices(event.target.value);
    } 
  };

  const getSubOffices = (upperWardId) => {
    RunService(s.GetWardListForUpperOffice, {id:upperWardId})
          .then(response => {
            setSubWards(ResetForResponseData(s.GetWardListForUpperOffice.response, response.data));
          })
          .catch(error => {
            console.log(error);
          })
  }

  useEffect(() => {
    async function getUpperWardList() {
      RunService(s.getUpperwardList, {})
          .then(response => {
            setUpperWards(ResetForResponseData(s.getUpperwardList.response, response.data));
          })
          .catch(error => {
            console.log(error);
          })
    }
    getUpperWardList()
  }, [checkDataToRefresh]);

  useEffect(() => {
    if (selectedUpperWardValue) getSubOffices(selectedUpperWardValue);
  }, [selectedUpperWardValue]);

  return (
    <React.Fragment>
      <FormControl className={className ? className : classes.textField}>
        <InputLabel shrink>
          {GetText("upper_ward_name")}
        </InputLabel>
        <NativeSelect
          onChange={handleChange}
          disabled={disabled}
          value={selectedUpperWardValue}
          name={pUpwardId}
          id={pUpwardId}
        >
          <option value="" key=""></option>
            {upperWards.map((ward) => (
            <option
             value={ward.wardId}
             key={ward.wardId}
            >
              {ward.wardName}
            </option>
          ))}
        </NativeSelect>
      </FormControl>
      {!upperWardOnly && <FormControl className={className ? className : classes.textField}>
        <InputLabel shrink>
          {GetText("ward_name")}
        </InputLabel>
        <NativeSelect
          onChange={handleChange}
          disabled={disabled}
          name={pWardId}
          id={pWardId}
          value={selectedWardValue}
        >
          <option value=""></option>
            {subWards.map((ward) => (
            <option
             value={ward.wardId}
             key={ward.wardId}
            >
              {ward.wardName}
            </option>
          ))}
        </NativeSelect>
      </FormControl>} 
    </React.Fragment>
  );
}
