import React from 'react';
import { BrowserRouter as Router, Route, Redirect } from 'react-router-dom';
import SignIn from '../views/SignIn';
import MainPage from '../views/MainPage';

export default function Routers() {
    return (
        <Router>
            <main>
                <Route exact path="/" component={SignIn} />
                <Route path="/mainpage" component={MainPage} />
                <Route component={() => <Redirect to="/" />} />
            </main>
        </Router>
    )
} 