import React, { useState, useEffect } from 'react';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import NativeSelect from '@material-ui/core/NativeSelect';
import clsx from 'clsx';
import { FormStyle } from '../style/Style';
import serviceList from '../service/ServiceList';
import { RunService } from '../service/RestApi';
import { ResetForResponseData } from '../common/Utility';
import { useUserContext } from '../common/UserContext';

const useStyles = FormStyle;

export default function CodeComboBox(props) {
  const {parentGroupCode, groupCode, className, parentLabel, childLabel, parentComboId, childComboId, callEvent, parentClass, codeClass, childOnly, selectedParentCode, selectedCode, disabled} = props;
  const classes = useStyles();
  const s = new serviceList();
  const userContext = useUserContext();

  const [groupCodes, setGroupCodes] = useState([]);
  const [codes, setCodes] = useState([]);

  const handleChange = (event) => {
    const name = event.target.name;
    if (name === parentComboId) {
      getCodeList(event.target.value);
    } else {
      callEvent({code:event.target.value});
    }
  };

  const getCodeList = (g) => {
    RunService(s.getCodesForGroupCode, {groupCode:g, locale: userContext.preference.language})
      .then(response => {
        setCodes(ResetForResponseData(s.getCodesForGroupCode.response, response.data));
      })
      .catch(error => {
        console.log(error);
      })
  }

  useEffect(() => {
    async function getGroupCodeList() {
      RunService(s.getGroupCodes, {parentGroupCode:parentGroupCode, locale: userContext.preference.language})
      .then(response => {
        setGroupCodes(ResetForResponseData(s.getGroupCodes.response, response.data));
      })
      .catch(error => {
        console.log(error);
      })
    }
    if (!childOnly && parentGroupCode) getGroupCodeList()
  }, []);

  useEffect(() => {
    if (childOnly && groupCode) getCodeList(groupCode);
  }, []);
  
  return (
    <React.Fragment>
      {!childOnly && <FormControl className={className ? className : classes.textField}>
        <InputLabel shrink>
          {parentLabel}
        </InputLabel>
        <NativeSelect
          onChange={handleChange}
          value={selectedParentCode}
          inputProps={{ 
            name: parentComboId,
            id: parentComboId,
            'disabled' : disabled ? true : false}}
        >
          <option value="" key=""></option>
            {groupCodes.map((g) => (
            <option
             value={g.groupCode}
             key={g.groupCode}
            >
              {g.name}
            </option>
          ))}
        </NativeSelect>
      </FormControl>}
      <FormControl className={className ? className : classes.textField}>
        <InputLabel shrink>
          {childLabel}
        </InputLabel>
        <NativeSelect
          onChange={callEvent}
          disabled={disabled}
          inputProps={{
            name: childComboId,
            id: childComboId,
            'disabled' : disabled ? true : false
          }}
          value={selectedCode}
        >
          <option value=""></option>
          {codes.map((c) => (
            <option
             value={c.code1}
             key={c.code1}
            >
              {c.name}
            </option>
          ))}
        </NativeSelect>
      </FormControl>
    </React.Fragment>
  );
}
