import React from 'react';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import { makeStyles } from '@material-ui/core/styles';
import Config from '../common/Config';

  function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }

  const useStyles = makeStyles((theme) => ({
    root: {
      width: '100%',
      '& > * + *': {
        marginTop: theme.spacing(2),
      },
    },
  }));

  export default function CustomizedSnackbar({isOpen, severity, message, closeHandler}) {
    const classes = useStyles();
    const config = new Config();
  
    return (
      <div className={classes.root}>
        <Snackbar id="snackbar" open={isOpen} autoHideDuration={config.SnackbarCloseTime} onClose={closeHandler}>
          <Alert onClose={closeHandler} severity={severity}>
            {message}
          </Alert>
        </Snackbar >
    </div>
    );
  }