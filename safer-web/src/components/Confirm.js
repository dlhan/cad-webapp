import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { GetText } from '../common/BundleManager'
import Dictionary from '../common/Dictionary';
import ErrorIcon from '@material-ui/icons/Error';
import WarningIcon from '@material-ui/icons/Warning';
import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline';
import { useUserContext } from '../common/UserContext';
import Grid from '@material-ui/core/Grid';

const useStyles = makeStyles((theme) => ({
    icon: {
      paddingTop:theme.spacing(0.5),
      paddingRight:theme.spacing(0.5)
    },
    contentWidth: {
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(1),
      width: '55ch',
    },
  }));

function IconType(MessageType) {
  var dictionary = new Dictionary();
  const classes = useStyles();
  const userContext = useUserContext();

  switch (MessageType) {
      case dictionary.ERROR:
        return (
            <Grid container spacing={2}>
                  <Grid item xs={12}>
                    <Grid container>
                      <Grid key={0} item className={classes.icon}>
                        <ErrorIcon color="error" />
                      </Grid>
                      <Grid key={1} item>
                        {GetText("error")}   
                      </Grid>  
                    </Grid>
                  </Grid>
                </Grid>
        )
      case dictionary.WARNING:
        return (
                <Grid container spacing={2}>
                  <Grid item xs={12}>
                    <Grid container>
                      <Grid key={0} item className={classes.icon}>
                        <WarningIcon color={userContext.preference.color} />
                      </Grid>
                      <Grid key={1} item>
                        {GetText("warning")}   
                      </Grid>  
                    </Grid>
                  </Grid>
                </Grid>
        )
      case dictionary.INFORMATION:
        return (
                <Grid container spacing={2}>
                  <Grid item xs={12}>
                    <Grid container>
                      <Grid key={0} item className={classes.icon}>
                        <ErrorOutlineIcon color={userContext.preference.color} />
                      </Grid>
                      <Grid key={1} item>
                       {GetText("information")}   
                      </Grid>  
                    </Grid>
                  </Grid>
                </Grid>
          )
      default:
        return (
            <Grid container spacing={2}>
              <Grid item xs={12}>
                <Grid container>
                  <Grid key={0} item className={classes.icon}>
                    <WarningIcon />
                  </Grid>
                  <Grid key={1} item>
                    {GetText("warning")}   
                  </Grid>  
                </Grid>
              </Grid>
            </Grid>
          )
  }
}

export default function Confirm({MessageType, MessageId, open ,handleClose}) {
  const userContext = useUserContext();
  const classes = useStyles();

  return (
    <div>
      <Dialog
        open={open}
        id="confirm_dialog"
      >
        <DialogTitle id="alert-dialog-title">
            {IconType(MessageType)}
        </DialogTitle>
        <DialogContent className={classes.contentWidth}>
          <DialogContentText id="alert-dialog-description">
            {GetText(MessageId)}
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={() => handleClose(true)} color={userContext.preference.color}>
            {GetText("yes")}
          </Button>
          <Button onClick={() => handleClose(false)} color={userContext.preference.color}>
            {GetText("no")}
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
