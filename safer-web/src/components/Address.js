import React from 'react';
import { GetText } from '../common/BundleManager'
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import CustomizedTextField from './TextField';
import Input from '@material-ui/core/Input';
import { FormStyle } from '../style/Style';

const useStyles = FormStyle;

export default function Address({addressId, etcAddressid, changeEvent, disabled, postalCodeValue,etcAddressValue}) {
  const classes = useStyles();

  return (
    <React.Fragment>
      <FormControl className={classes.textFieldAddress}>
          <InputLabel shrink>{GetText("address")}</InputLabel>
          <Input
            id={addressId}
            name={addressId}
            type='text'
            onChange={changeEvent}
            disabled = {disabled}
            value={postalCodeValue}
            endAdornment={
                <InputAdornment position="end">
                  <IconButton aria-label="Address">
                    <AddCircleOutlineIcon />
                  </IconButton>
                </InputAdornment>
            }
          />
        </FormControl>  
        <CustomizedTextField
          label={GetText("etc_address")}
          id={etcAddressid}
          name={etcAddressid}
          className={classes.textFieldAddressEtc}
          onChange={changeEvent}
          disabled = {disabled}
          value={etcAddressValue}
        />   
    </React.Fragment>
  );
}