import React from 'react';
import { emphasize, withStyles } from '@material-ui/core/styles';
import Breadcrumbs from '@material-ui/core/Breadcrumbs';
import Chip from '@material-ui/core/Chip';
import HomeIcon from '@material-ui/icons/Home';
import { GetText } from '../common/BundleManager';

const StyledBreadcrumb = withStyles((theme) => ({
  root: {
    backgroundColor: 'transparent',
    height: theme.spacing(3),
    color: theme.palette.grey[800],
    fontWeight: theme.typography.fontWeightRegular,
    '&:hover, &:focus': {
      backgroundColor: theme.palette.grey[300],
    },
    '&:active': {
      boxShadow: theme.shadows[1],
      backgroundColor: emphasize(theme.palette.grey[300], 0.12),
    },
    paddingBottom: theme.spacing(1)
  },
}))(Chip); // TypeScript only: need a type cast here because https://github.com/Microsoft/TypeScript/issues/26591

export default function CustomizedBreadcrumb({viewInfo}) {
  let locations = [];
  if (viewInfo.locations) {
    locations = viewInfo.locations;
  }
  
  return (
    <Breadcrumbs aria-label="breadcrumb">
      <StyledBreadcrumb
        component="a"
        label={GetText("menu_home")}
        key={0}
        icon={<HomeIcon fontSize="small" />}
      />
      {
        locations.map((location, index) => {
          return (
            <StyledBreadcrumb label={location} key={index + 1} />
          );
        })
      }
    </Breadcrumbs>
  );
}
