import React, { useState, useEffect } from 'react';
import { GetText } from '../common/BundleManager';
import serviceList from '../service/ServiceList';
import { RunService } from '../service/RestApi';
import Dictionary from '../common/Dictionary';
import { CheckResultFromService, ResetForResponseData } from '../common/Utility';
import Grid from '@material-ui/core/Grid';
import StickyHeadTable from '../components/Table';
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import ActionPanel from '../components/ActionPanel';
import { FormStyle } from '../style/Style';
import { useUserContext } from '../common/UserContext';
import CustomizedSnackbar from '../components/Snackbar';
import CircularProgress from '@material-ui/core/CircularProgress';
import Confirm from '../components/Confirm';
import ViewInfo from '../common/ViewInfo';

const useStyles = FormStyle;
const columns = [
    { id: 'notificationId', numeric: false, disablePadding: false, label: '', align: 'center', minWidth: 0 },
    { id: 'relatedId', numeric: false, disablePadding: false, label: '', align: 'center', minWidth: 0 },
    { id: 'notificationType', numeric: false, disablePadding: false, label: 'notification_type', align: 'center', minWidth: 200 },
    { id: 'state', numeric: false, disablePadding: false, label: 'state', align: 'center', minWidth: 130 },
    { id: 'userId', numeric: false, disablePadding: false, label: 'user_id', align: 'center', minWidth: 120 },
    { id: 'name', numeric: false, disablePadding: false, label: 'user_name', align: 'center', minWidth: 200 },
    { id: 'wardName', numeric: false, disablePadding: false, label: 'ward_name', align: 'center', minWidth: 150 },
    { id: 'createdDate', numeric: false, disablePadding: false, label: 'notification_date', align: 'center', minWidth: 170 },
  ];

let dataSelected = [];
export default function WardDetail(props) {
  const classes = useStyles();
  var dictionary = new Dictionary();
  const userContext = useUserContext();
  const s = new serviceList();
  var viewInfo = new ViewInfo();
  var view = viewInfo.notification_list;
  const [rows, setRows] = useState([]);
  let [loading, setLoading] = useState(false);
  const [operateType, setOperateType] = useState();
  
/////////////Setting for Snackbar////////////////////////////////
  let [snackbar, setSnackbar] = useState({openSnackbar:false, severity:dictionary.MessageType.Info, message:''});
  let { openSnackbar, severity, message } = snackbar;
    
  const onCloseSnackbar = () => {
    setSnackbar(prev => ({
      ...prev,
      openSnackbar:false,
      severity:dictionary.MessageType.Info,
      message:''
    }));
  }
/////////////////////////////////////////////

//////Confrim event
  const [confirmMessage, setConfirmMessage] = React.useState({
                                                      isConfirmOpen : false,
                                                      messageType : "",
                                                      messageId : ""
                                                });
  let { isConfirmOpen,messageType,messageId } = confirmMessage;
  
  const handleConfirmDialog = (answer) => {
    setConfirmMessage({
      ...confirmMessage,
      isConfirmOpen:false
    });
  
    if (answer){
      executeServiceCall();
    }
  }
  /////////////////////////////////////////////
  
  const onConfirm = (evt) => {
    if (evt === 'approve') {
        setOperateType('approve');
        setConfirmMessage({ isConfirmOpen: true, messageType: dictionary.WARNING, messageId : "msg_will_you_approve" });
    } else {
        setOperateType('reject');
        setConfirmMessage({ isConfirmOpen: true, messageType: dictionary.WARNING, messageId : "msg_will_you_reject" });
    }   
  }

  const checkBoxClickEvent = (data) => {
    dataSelected = data;
  }

  const executeServiceCall = () => {
    let service;
    if (operateType === 'approve') {
      service = s.approveNotification;
    } else {
      service = s.rejectNotification;
    }

    setLoading(true);
    RunService(service, dataSelected)
    .then(response => {
        setLoading(false);
        var result = CheckResultFromService(response.data);
        if (result.isSuccess) {
          getNotification();
          props.callFromDialog({refresh:true});
          setSnackbar(prev => ({
            ...prev,
            openSnackbar:true,
            severity:dictionary.MessageType.Success,
            message:GetText(result.message)
          }));
        } else {
          setSnackbar(prev => ({
            ...prev,
            openSnackbar:true,
            severity:dictionary.MessageType.Error,
            message:GetText(result.message)
          }));
        }

    })
    .catch(error => {
      setSnackbar(prev => ({
        ...prev,
        openSnackbar:true,
        severity:dictionary.MessageType.Error,
        message:GetText("msg_normal_error_message")
      }));
      setLoading(false);
    })  
  }

  const handleMakeRows = (data) => {
    let rowData = [];
    let item ={};

    data.forEach((row) => {
        item ={};
        item["notificationId"] = row.notificationId;
        item["relatedId"] = row.relatedId;
        item["notificationType"] = changeNotificationTypeToText(row.notificationType);
        item["state"] = changeStateToText(row.state);
        item["userId"] = row.userId;
        item["name"] = row.lastName + ' ' + row.firstName;
        item["wardName"] = row.wardName;
        item["createdDate"] = row.createdDate;
        item["rowId"] = row.notificationId;
        rowData.push(item);
    })

    setRows(rowData);
  }

  const changeNotificationTypeToText = (data) => {
    switch (data) {
        case "1" :
            return GetText("create_account");
        case "2" :
            return GetText("request_auth");
        default :
            return data;
    }
  }

  const changeStateToText = (data) => {
    switch (data) {
        case "0" :
            return GetText("before_approved");
        case "1" :
            return GetText("Approved");
        case "2" :
            return GetText("Rejected");
        default :
            return data;
    }
  }

  const getNotification = () => {
    setLoading(true);
    RunService(s.getNotificationList)
        .then(response => {
          handleMakeRows(response.data);
          setLoading(false);
        })
        .catch(error => {
          setSnackbar(prev => ({
            ...prev,
            openSnackbar:true,
            severity:dictionary.MessageType.Error,
            message:GetText("msg_normal_error_message")
          }));
          setLoading(false);
        })
  }

/////////////////////Get data////////////////////////////////////////////
  useEffect(() => {
    getNotification();
  }, []);
  /////////////////////////////////////////////////////////////////

  return (
    <React.Fragment>
      <Paper className={classes.control}>
        <StickyHeadTable 
              tableId={"notificationTable"} 
              columns={columns} 
              rows={rows} 
              SupportCheckBox={view.table.allowCheckBox} 
              SupportPaging={view.table.allowPage}
              checkBoxClickEvent={checkBoxClickEvent}
              heightGap={190}
        />
      </Paper>   
      
      <Box p={1}></Box>
      <Divider style={{'width':'100%','position': 'absolute', 'left': 0}} />
      <ActionPanel>
        <Button 
          color={userContext.preference.color}
          onClick={() => onConfirm('approve')}
        >
          { GetText("approve") }
        </Button>
        <Button 
          color={userContext.preference.color}
          onClick={() => onConfirm('reject')}
        >
          { GetText("reject") }
        </Button>
        {loading && <CircularProgress size={24} className={classes.buttonProgress} />}
      </ActionPanel>
      <CustomizedSnackbar isOpen={openSnackbar} severity={severity} message={message} closeHandler={onCloseSnackbar} />
      <Confirm open={isConfirmOpen} MessageType={messageType} MessageId={messageId} handleClose={handleConfirmDialog} />
    </React.Fragment>
  );
}