import React from 'react';
import CustomizedBreadcrumb from '../components/BreadCrumb';
import ViewInfo from '../common/ViewInfo';

export default function Home() {
  var viewInfo = new ViewInfo();

  return (
    <React.Fragment>
      <CustomizedBreadcrumb viewInfo={viewInfo.home} />
    </React.Fragment>
  );
}
