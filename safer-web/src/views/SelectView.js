import React from 'react';
import Home from './Home';
import WardList from './is/WardList';
import WardDetail from './is/WardDetail';
import BuildingList from './is/BuildingList';
import BuildingDetail from './is/BuildingDetail';
import UserPreference from './UserPreference';
import PersonList from './hr/PersonList';
import PersonDetail from './hr/PersonDetail';
import PersonBasicInfo from './hr/PersonBasicInfo';
import PersonEducationHistory from './hr/PersonEducationHistory';
import PersonEducationDetail from './hr/PersonEducationDetail';
import PersonLicencePossessed from './hr/PersonLicencePossessed';
import PersonLicencePossessedDetail from './hr/PersonLicencePossessedDetail';
import PersonLearningAchivement from './hr/PersonLearningAchivement';
import PersonLearningAchivementDetail from './hr/PersonLearningAchivementDetail';
import PersonFireTrainAchivementDetail from './hr/PersonFireTrainAchivementDetail';
import CodeTreeView from './sm/CodeTreeView';
import Notification from './Notification';
import CreateAccount from './CreateAccount';
import Dictionary from '../common/Dictionary';

export default function SelectView(viewInfo, callFromDialog=null, sendInfo=null, handleDialogClose) {
  let dictionary = new Dictionary();

  switch (viewInfo.viewName) {
      case dictionary.View_WardList:
        return <WardList callFromDialog={callFromDialog} sendInfo={sendInfo} handleDialogClose={handleDialogClose} />;
      case dictionary.View_WardDetail:
        return <WardDetail callFromDialog={callFromDialog} sendInfo={sendInfo} handleDialogClose={handleDialogClose} />;
      case dictionary.View_BuildingList:
        return <BuildingList callFromDialog={callFromDialog} sendInfo={sendInfo} handleDialogClose={handleDialogClose} />;
      case dictionary.View_BuildingDetail:
        return <BuildingDetail callFromDialog={callFromDialog} sendInfo={sendInfo} handleDialogClose={handleDialogClose} />;
      case dictionary.View_UserPreference:
        return <UserPreference callFromDialog={callFromDialog} sendInfo={sendInfo} handleDialogClose={handleDialogClose} />;
      case dictionary.View_PersonList:
        return <PersonList callFromDialog={callFromDialog} sendInfo={sendInfo} handleDialogClose={handleDialogClose} />;
      case dictionary.View_PersonDetail:
        return <PersonDetail callFromDialog={callFromDialog} sendInfo={sendInfo} handleDialogClose={handleDialogClose} />;
      case dictionary.View_PersonBasicInfo:
        return <PersonBasicInfo callFromDialog={callFromDialog} sendInfo={sendInfo} handleDialogClose={handleDialogClose} />;
      case dictionary.View_PersonEducationHistory:
        return <PersonEducationHistory callFromDialog={callFromDialog} sendInfo={sendInfo} handleDialogClose={handleDialogClose} />;
      case dictionary.View_PersonEducationDetail:
        return <PersonEducationDetail callFromDialog={callFromDialog} sendInfo={sendInfo} handleDialogClose={handleDialogClose} />;
      case dictionary.View_PersonLicenseList:
        return <PersonLicencePossessed callFromDialog={callFromDialog} sendInfo={sendInfo} handleDialogClose={handleDialogClose} />;
      case dictionary.View_PersonLicenseDetail:
        return <PersonLicencePossessedDetail callFromDialog={callFromDialog} sendInfo={sendInfo} handleDialogClose={handleDialogClose} />;
      case dictionary.View_PersonLearningList:
        return <PersonLearningAchivement callFromDialog={callFromDialog} handleDialogClose={handleDialogClose} />;
      case dictionary.View_PersonLearningDetail:
        return <PersonLearningAchivementDetail callFromDialog={callFromDialog} sendInfo={sendInfo} handleDialogClose={handleDialogClose} />;
      case dictionary.View_PersonFireTrainDetail:
        return <PersonFireTrainAchivementDetail callFromDialog={callFromDialog} sendInfo={sendInfo} handleDialogClose={handleDialogClose} />;
      case dictionary.View_CreateAccount:
        return <CreateAccount callFromDialog={callFromDialog} handleDialogClose={handleDialogClose} />;
      case dictionary.View_Notification:
        return <Notification callFromDialog={callFromDialog} handleDialogClose={handleDialogClose} />;
      case dictionary.View_GroupCodeTreeView:
        return <CodeTreeView callFromDialog={callFromDialog} sendInfo={sendInfo} />;
      default:
        return <Home />;
  }
}