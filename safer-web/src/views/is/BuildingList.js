import React, { useState } from 'react';
import { GetText } from '../../common/BundleManager'
import serviceList from '../../service/ServiceList';
import CircularProgress from '@material-ui/core/CircularProgress';
import CustomizedSnackbar from '../../components/Snackbar';
import Dictionary from '../../common/Dictionary';
import { RunService } from '../../service/RestApi';
import { SetSearchBody, ResetForResponseData } from '../../common/Utility';
import StickyHeadTable from '../../components/Table';
import CustomizedBreadcrumb from '../../components/BreadCrumb';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import InputAdornment from '@material-ui/core/InputAdornment';
import clsx from 'clsx';
import IconButton from '@material-ui/core/IconButton';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import SearchIcon from '@material-ui/icons/Search';
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';
import Fab from '@material-ui/core/Fab';
import Input from '@material-ui/core/Input';
import CustomizedDialog from '../../components/Dialog';
import { FormStyle } from '../../style/Style';
import ViewInfo from '../../common/ViewInfo';
import { useUserContext } from '../../common/UserContext';
import Confirm from '../../components/Confirm';
import UpperAndOfficeSelect from '../../components/UpperAndOfficeSelect';

const useStyles = FormStyle;

const columns = [
  { id: 'buildingId', numeric: false, disablePadding: false, label: '', align: 'center' },
  { id: 'buildingManageNumber', numeric: false, disablePadding: false, label: GetText('building_manage_number'), align: 'center' },
  { id: 'buildingType', numeric: false, disablePadding: false, label: GetText('building_type'), align: 'center' },
  { id: 'jurisOffice', numeric: false, disablePadding: false, label: GetText('juris_office'), align: 'center' },
  { id: 'jurisCenter', numeric: false, disablePadding: false, label: GetText('juris_center'), align: 'center' },
  { id: 'buildingName', numeric: false, disablePadding: false, label: GetText('building_name'), align: 'center' },
  { id: 'mainUsed', numeric: false, disablePadding: false, label: GetText('building_main_used'), align: 'center' },  
  { id: 'address', numeric: false, disablePadding: false, label: GetText('address'), align: 'center' }  
];

export default function BuildingList(props) {
  const classes = useStyles();
  var s = new serviceList();
  var dictionary = new Dictionary();
  var viewInfo = new ViewInfo();
  const userContext = useUserContext();

  let [loading, setLoading] = useState(false);

////////Setting for Table//////////////////////////////////////////
  const onSelectRow = (rowId) => {
    handleDialogOpen(dictionary.READ, rowId)
  }

  const [page, setPage] = React.useState(0);
  const [totalDataCount, setTotalDataCount] = React.useState(0);
  const [rows, setRows] = useState([]);
  const onPageChange = (event, newPage) => {
    setPage(newPage);
    getData(newPage, false);
  }
///////////////////////////////////////////////////////////////////

//////The parf for search ////////////////////////////////
  let [searchItems, setSearchItems] = useState({
                                       upperWardId:''
                                     , wardId:''
                                     , typeCode:''
                                     });
  let { upperWardId, wardId, typeCode  } = searchItems;

  const onChange = e => {
    let {name, value} = e.target;

     setSearchItems({
       ...searchItems,
       [name]:value
     });
  }
///////////////////////////////////////////////////////////////////

  const getData = (pageNumber, isSearchMode) => {
    setLoading(true);
    let body = SetSearchBody(s.getWardList, searchItems, userContext.preference.tableRowsCount, pageNumber);

    RunService(s.getWardList, body, true)
        .then(response => {
          setLoading(false);
          setTotalDataCount(response.data.page.totalCount);

          setRows(ResetForResponseData(s.getWardList.response.rows,response.data.rows,"wardId"));

          if (isSearchMode) {
            setSnackbar(prev => ({
              ...prev,
              openSnackbar:true,
              severity:dictionary.MessageType.Success,
              message:GetText("msg_data_refreshed")
            }));
          }
        })
        .catch(error => {
          setLoading(false);
          setSnackbar(prev => ({
            ...prev,
            openSnackbar:true,
            severity:dictionary.MessageType.Error,
            message:GetText("msg_normal_error_message")
          }));
        })
  }

  const onSearch = () => {
    setPage(0);
    getData(1, true);
  }

//Callback from the combo for upward and ward.
  const onChangeWardCombo = (e) => {
    setSearchItems({
      ...searchItems,
      [Object.keys(e)[0]]:e[Object.keys(e)[0]]
    });
  }

//////Dialog event
  const [isDialogOpen, setIsDialogOpen] = useState(false);
  const [isModified, setIsModified] = useState(false);
  
  const callFromDialog = (p) => {
    setIsModified(p.modified);

    if (p.refresh) {
      onSearch();
    }
  }

  const [sendIfo, setSendInfo] = useState({operateType:'', id:''});
  const { operateType, id } = sendIfo;

  const handleDialogClose = (forceNoMessage) => {
    let forceClose = false;

    if (typeof forceNoMessage === 'boolean') forceClose = forceNoMessage;

    if (isModified && !forceClose) {
      setConfirmMessage({
        isConfirmOpen: true,
        messageType: dictionary.WARNING,
        messageId: "msg_confirm_close_dialog" 
      });
    } else {
      setIsDialogOpen(false);
    }
  };
  
  const handleDialogOpen = (operateType, id) => {
    setSendInfo({operateType:operateType, id:id});
    setIsDialogOpen(true);
  };
////////////////////////////////////////////////////////

//////Confrim event in closing the dialog///////////////
  const [confirmMessage, setConfirmMessage] = useState({
    isConfirmOpen : false,
    messageType : "",
    messageId : ""
  });
  let { isConfirmOpen,messageType,messageId } = confirmMessage;

  const handleConfirmDialog = (answer) => {
    setConfirmMessage({
      ...confirmMessage,
      isConfirmOpen:false
    });

    if (answer){
      setIsDialogOpen(false);
    }
  }
////////////////////////////////////////////////////////

/////////////Setting for Snackbar////////////////////////////////
  let [snackbar, setSnackbar] = useState({openSnackbar:false, severity:'', message:''});
  let { openSnackbar, severity, message } = snackbar;
    
  const onCloseSnackbar = () => {
    setSnackbar(prev => ({
      ...prev,
      openSnackbar:false,
      severity:'',
      message:''
    }));
  }
////////////////////////////////////////////////////////
 
  return (
    <React.Fragment>
      <CustomizedBreadcrumb viewInfo={viewInfo.ward_list} />
      <Grid container spacing={1}>
        <Grid item xs={12}>
          <Paper className={classes.control}>
            <div className={classes.paper} id="search_panel">
              <UpperAndOfficeSelect callEvent={onChangeWardCombo} checkDataToRefresh={rows}/>
              <FormControl className={clsx(classes.margin, classes.textField)}>
                <InputLabel shrink>{GetText("building_type")}</InputLabel>
                <Input
                  id="typeCode"
                  name="typeCode"
                  type='text'
                  onChange={onChange}
                  endAdornment={
                    <InputAdornment position="end">
                      <IconButton aria-label="toggle password visibility">
                        <AddCircleOutlineIcon />
                        </IconButton>
                      </InputAdornment>
                    }
                />
              </FormControl>
              <div className={classes.grow} />
              <div className={classes.sectionSearch}>
                <Button
                  variant="contained"
                  color={userContext.preference.color}
                  size="large"
                  onClick={onSearch}
                  className={classes.button}
                  startIcon={<SearchIcon />}
                >
                  {GetText("search")}
                </Button>
              </div>
            </div>
          </Paper>
        </Grid>
        <Grid item xs={12}>
          <Paper className={classes.control}>
            <StickyHeadTable 
              tableId={"mainTable"} 
              onSelectRow={onSelectRow} 
              columns={columns} 
              rows={rows} 
              SupportCheckBox={false} 
              page={page} 
              totalDataCount={totalDataCount}
              onPageChange={onPageChange} 
            />
          </Paper>
        </Grid>
        <Grid item xs={12}>
          <div className={classes.centerLocate}>
            <Fab color={userContext.preference.color} aria-label="add">
              <AddIcon onClick={() => handleDialogOpen(dictionary.NEW)} />
            </Fab>
          </div>
        </Grid>
      </Grid>
      {loading && <CircularProgress size={24} className={classes.buttonProgress} />}
      <CustomizedDialog isDialogOpen={isDialogOpen} sendInfo={sendIfo} viewInfo={viewInfo.ward_Detail} handleDialogClose={handleDialogClose} callFromDialog={callFromDialog} />
      <Confirm open={isConfirmOpen} MessageType={messageType} MessageId={messageId} handleClose={handleConfirmDialog} />
      <CustomizedSnackbar isOpen={openSnackbar} severity={severity} message={message} closeHandler={onCloseSnackbar} />
     </React.Fragment>
  );
}