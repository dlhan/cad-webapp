import React, { useState, useEffect } from 'react';
import { GetText } from '../../common/BundleManager';
import serviceList from '../../service/ServiceList';
import { RunService } from '../../service/RestApi';
import Dictionary from '../../common/Dictionary';
import { CheckResultFromService, ResetForResponseData } from '../../common/Utility';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import Address from '../../components/Address';
import ActionPanel from '../../components/ActionPanel';
import { FormStyle } from '../../style/Style';
import CustomizedSwitch from '../../components/Switch';
import { useUserContext } from '../../common/UserContext';
import CustomizedSnackbar from '../../components/Snackbar';
import CircularProgress from '@material-ui/core/CircularProgress';
import ViewInfo from '../../common/ViewInfo';
import { useForm } from "react-hook-form";
import Confirm from '../../components/Confirm';
import UpperAndOfficeSelect from '../../components/UpperAndOfficeSelect';
import CustomizedTextField from '../../components/TextField';
import TextFieldWithIcon from '../../components/TextFieldWithIcon';
import FindInPageIcon from '@material-ui/icons/FindInPage';
import CustomizedDialog from '../../components/Dialog';

const useStyles = FormStyle;
let originalInputs = {};
let isModified = false;

export default function WardDetail(props) {
  const classes = useStyles();
  var dictionary = new Dictionary();
  const userContext = useUserContext();
  const s = new serviceList();
  var viewInfo = new ViewInfo();

  const [operateType, setOperateType] = useState(props.sendInfo.operateType);
  let [inputs, setInputs] = useState({wardId:''
                                    , typeCode:''
                                    , typeCodeName:''
                                    , wardName:''
                                    , upperWardId:''
                                    , abbrName:''
                                    , isBrodcastingUse:false
                                    , isUse: true
                                    , extNumber:''
                                    , phoneNumber:''
                                    , faxNumber:''
                                    , postalCode:''
                                    , etcAddress:''
                                    , openDate:''
                                    , closeDate:''
                                    , remark:''
                                    });
  let { wardId, typeCode, typeCodeName, wardName, upperWardId, abbrName, isBrodcastingUse, isUse,extNumber,phoneNumber,faxNumber,postalCode,etcAddress,openDate,closeDate,remark } = inputs;
  let [loading, setLoading] = useState(false);
  let [disabled, setDisabled] = useState(false);
  let [isUpdateMode, setIsUpdateMode] = useState(false);
  
//////////Upward select event/////////////////////////////////////////
  const callEvent = (e) => {
    setInputs({
      ...inputs,
      upperWardId:e[Object.keys(e)[0]]
    });
  }
///////////////////////////////////////////////////
  
/////////Setting for validate the inputs in the form.
  const { handleSubmit } = useForm();
////////////////////////////////////////////////////

  const onChange = e => {
    let {name, value} = e.target;
    
    if (name === "isBrodcastingUse") { value = !inputs.isBrodcastingUse};
    if (name === "isUse") { value = !inputs.isUse};
    isModified = true;
    setInputs({
      ...inputs,
      [name]:value
    });
  }

/////////////Setting for Snackbar////////////////////////////////
  let [snackbar, setSnackbar] = useState({openSnackbar:false, severity:'', message:''});
  let { openSnackbar, severity, message } = snackbar;
    
  const onCloseSnackbar = () => {
    setSnackbar(prev => ({
      ...prev,
      openSnackbar:false,
      severity:'',
      message:''
    }));
  }
/////////////////////////////////////////////

//////Confrim event
  const [confirmMessage, setConfirmMessage] = React.useState({
                                                      isConfirmOpen : false,
                                                      messageType : "",
                                                      messageId : ""
                                                });
  let { isConfirmOpen,messageType,messageId } = confirmMessage;
  
  const handleConfirmDialog = (answer) => {
    setConfirmMessage({
      ...confirmMessage,
      isConfirmOpen:false
    });
  
    if (answer){
      onSubmit();
    }
  }
  /////////////////////////////////////////////
  
  ////Button operate
  
  const onChangeUpdateState = () => {
    setIsUpdateMode(!isUpdateMode);
    if (!isUpdateMode) {
      onUpdateMode();
    } else {
      onReadMode(wardId);
    }
  }

  const onReadMode = () => {
    setDisabled(true);
    setOperateType("");
    setIsUpdateMode(false);
    props.callFromDialog({modified: false});
  }

  const onNewMode = () => {
    setDisabled(false);
    setOperateType(dictionary.NEW);
    setInputs({wardId:''
            , typeCode:''
            , typeCodeName:''
            , wardName:''
            , upperWardId:''
            , abbrName:''
            , isBrodcastingUse:false
            , isUse: true
            , extNumber:''
            , phoneNumber:''
            , faxNumber:''
            , postalCode:''
            , etcAddress:''
            , openDate:''
            , closeDate:''
            , remark:''
            });
    props.callFromDialog({modified: false});
    originalInputs = {wardId:''
                    , typeCode:''
                    , typeCodeName:''
                    , wardName:''
                    , upperWardId:''
                    , abbrName:''
                    , isBrodcastingUse:false
                    , isUse: true
                    , extNumber:''
                    , phoneNumber:''
                    , faxNumber:''
                    , postalCode:''
                    , etcAddress:''
                    , openDate:''
                    , closeDate:''
                    , remark:''};
    isModified = false;
  }

  const onUpdateMode = () => {
    setDisabled(false);
    setOperateType(dictionary.UPDATE);
    props.callFromDialog({modified: false});
  }

  const onConfirm = () => {
    if (operateType === dictionary.NEW) {
      setConfirmMessage({ isConfirmOpen: true, messageType: dictionary.WARNING, messageId : "msg_will_you_insert" });
    } else if (operateType === dictionary.UPDATE) {
      setConfirmMessage({ isConfirmOpen: true, messageType: dictionary.WARNING, messageId : "msg_will_you_update" });
    } else {
      onNewMode();
    }
  }

  const onDelete = () => {
    setOperateType(dictionary.DELETE);
    setConfirmMessage({ isConfirmOpen: true, messageType: dictionary.WARNING, messageId : "msg_will_you_delete" });
  }

  const openGroupCodeTreeView = () => {
    setIsDialogOpen(true);
  }

/////////////////Check whether inputs are modified or not. If modified, it sends the state to parent//////////////////// 
  useEffect(() => {
    let Modified =  (JSON.stringify(inputs) === JSON.stringify(originalInputs)? false : true);
    
    if (operateType !== dictionary.READ) props.callFromDialog({modified: Modified && isModified});
  }, [inputs]);
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//////Dialog event
const [isDialogOpen, setIsDialogOpen] = useState(false);

const handleDialogClose = () => {
  setIsDialogOpen(false);
};

const callFromDialog = (p) => {
  setInputs({
    ...inputs,
    typeCode:p.cd,
    typeCodeName:p.cdName
  });
  setIsDialogOpen(false);
}

const sendIfo = {groupCode:'211'};
////////////////////////////////////////////////////////

/////////////////////Get data////////////////////////////////////////////
  useEffect(() => {
    let completed = false; 
    
    async function getInfo(id) {
      let body = {"id": id};
      setLoading(true);
      RunService(s.getWardInfo, body)
          .then(response => {
            if (!completed) { 
              let reponseData = ResetForResponseData(s.getWardInfo.response, response.data);
              setInputs(reponseData);
              originalInputs = reponseData;
            }
            completed= true;
            onReadMode();
            setLoading(false);
          })
          .catch(error => {
            setSnackbar(prev => ({
              ...prev,
              openSnackbar:true,
              severity:dictionary.MessageType.Error,
              message:GetText("msg_normal_error_message")
            }));
          })
    }

    if (operateType === dictionary.READ && props.sendInfo.id) getInfo(props.sendInfo.id);
    
    return () => {
      completed = true;
    };
  }, []);
  /////////////////////////////////////////////////////////////////

  ///////////////////// Insert or Update///////////////////////////
  const onSubmit = () => {
    if (operateType === "") {
      onNewMode();
      return;
    }

    setLoading(true);
    let service;
    let body;

    switch(operateType) {
      case dictionary.NEW :
        service = s.insertWard;
        body = inputs;
        break;
      case dictionary.UPDATE :
        service = s.updateWard;
        body = inputs;
        break;
      case dictionary.DELETE :
        service = s.deleteWard;
        body = [inputs.wardId]; 
        break;
      default :
        return;
    }
  
    RunService(service, body)
        .then(response => {
          setLoading(false);
          var result = CheckResultFromService(response.data);
          if (result.isSuccess) {
            setSnackbar(prev => ({
              ...prev,
              openSnackbar:true,
              severity:dictionary.MessageType.Success,
              message:GetText(result.message)
            }));
            
            setInputs({
              ...inputs,
              wardId:result.id
            });
            onReadMode();
            
            if (operateType === dictionary.DELETE) {
              props.handleDialogClose(true);
            } else {
              props.callFromDialog({modified: false, refresh:true});
            }

          } else {
            setSnackbar(prev => ({
              ...prev,
              openSnackbar:true,
              severity:dictionary.MessageType.Error,
              message:GetText(result.message)
            }));
          }
        })
        .catch(error => {
          setLoading(false);
          setSnackbar(prev => ({
            ...prev,
            openSnackbar:true,
            severity:dictionary.MessageType.Error,
            message:GetText("msg_normal_error_message")
          }));
        })
  } 
  /////////////////////////////////////////////////////////////////

  return (
    <React.Fragment>
      <form onSubmit={handleSubmit(onConfirm)}>
      <Paper className={classes.control}>
        <Grid container spacing={1}>
          <Grid item xs={12}>
            <div className={classes.paper}>
            <TextFieldWithIcon 
                id="typeCodeName"
                label={GetText("type")}
                onChange={onChange}
                disabled={disabled}
                value={typeCodeName}
                iconEvent={openGroupCodeTreeView}
                icon={<FindInPageIcon />}
                required={true}
                readOnly={true}
              />
              <CustomizedTextField
                label={GetText("ward_name")}
                id="wardName"
                onChange={onChange}
                disabled={disabled}
                required={true}
                value={wardName}
              />
              <UpperAndOfficeSelect 
                upperWardOnly={true} 
                callEvent={callEvent} 
                selectedUpperWardValue={upperWardId}
                disabled={disabled}
              /> 
              <CustomizedTextField
                label={GetText("abbreviation")}
                id="abbrName"
                name="abbrName"
                onChange={onChange}
                disabled={disabled}
                value={abbrName}
              />          
            </div>
          </Grid>
          <Grid item xs={12}>
            <div className={classes.paper}>
              <CustomizedTextField
                label={GetText("tel_num")}
                id="phoneNumber"
                onChange={onChange}
                disabled={disabled}
                value={phoneNumber}
              />
              <CustomizedTextField
                label={GetText("fax")}
                id="faxNumber"
                onChange={onChange}
                disabled={disabled}
                value={faxNumber}
              />          
              <CustomizedTextField
                label={GetText("ext_num")}
                id="extNumber"
                onChange={onChange}
                disabled={disabled}
                value={extNumber}
              />
            </div>
          </Grid>
          <Grid item xs={12}>
            <div className={classes.paper}>
              <Address 
                addressId={'postalCode'} 
                etcAddressid={'etcAddress'} 
                changeEvent={onChange}
                disabled={disabled}
                postalCodeValue={postalCode}
                etcAddressValue={etcAddress}
              />
            </div>
          </Grid>
          <Grid item xs={12}>
            <div className={classes.paper}>
              <CustomizedSwitch 
                id='isBrodcastingUse'
                name='isBrodcastingUse' 
                checked={isBrodcastingUse}
                onChange={onChange}
                disabled={disabled}
                value={isBrodcastingUse}
                label={GetText("is_use_broadcast")} 
              />
              <CustomizedSwitch 
                id='isUse'
                name='isUse' 
                checked={isUse}
                onChange={onChange}
                disabled={disabled}
                value={isUse}
                label={GetText("ward_opened")} 
              />
              <CustomizedTextField
                id="openDate"
                label={GetText("ward_open_date")}
                type="date"
                onChange={onChange}
                disabled={disabled}
                value={openDate}
              />
              <CustomizedTextField
                id="closeDate"
                label={GetText("ward_close_date")}
                type="date"
                onChange={onChange}
                disabled={disabled}
                value={closeDate}
              />
            </div>
          </Grid>
          <Grid item xs={12}>
            <CustomizedTextField
              id="remark"
              label={GetText("remark")}
              className={classes.textFieldFullLength}
              multiline={true}
              rows={4}
              variant="outlined"
              onChange={onChange}
              disabled={disabled}
              value={remark}
            />
          </Grid>
        </Grid>
      </Paper>   
      
      <Box p={1}></Box>
      <Divider style={{'width':'100%','position': 'absolute', 'left': 0}} />
      <ActionPanel>
        <Button 
          disabled={loading}
          color={userContext.preference.color}
          type="submit"
        >
          { operateType !== "" ? GetText("save") : GetText("new") }
        </Button>
        { 
          inputs.wardId && operateType !== dictionary.NEW &&
            <Button 
              disabled={loading}
              color={userContext.preference.color}
              name="update"
              onClick={onChangeUpdateState}
            >
              { isUpdateMode ? GetText("cancel") : GetText("update") }
            </Button>
        }
        
        {
          inputs.wardId && 
            <Button 
              disabled={loading}
              color={userContext.preference.color}
              name="delete"
              onClick={onDelete}
          >
              {GetText("delete")}
            </Button> 
        }
        
        {loading && <CircularProgress size={24} className={classes.buttonProgress} />}
      </ActionPanel>
      </form>
      <CustomizedSnackbar isOpen={openSnackbar} severity={severity} message={message} closeHandler={onCloseSnackbar} />
      <Confirm open={isConfirmOpen} MessageType={messageType} MessageId={messageId} handleClose={handleConfirmDialog} />
      <CustomizedDialog id={'groupCodeViewDialog'} isDialogOpen={isDialogOpen} sendInfo={sendIfo} viewInfo={viewInfo.groupCode_tree_view} callFromDialog={callFromDialog} handleDialogClose={handleDialogClose} />
    </React.Fragment>
  );
}