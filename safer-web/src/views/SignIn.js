import React, { useState } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import { GetText } from '../common/BundleManager';
import serviceList from '../service/ServiceList';
import CircularProgress from '@material-ui/core/CircularProgress';
import { green } from '@material-ui/core/colors';
import CustomizedSnackbar from '../components/Snackbar';
import ViewInfo from '../common/ViewInfo';
import Dictionary from '../common/Dictionary';
import { RunService } from '../service/RestApi';
import { CheckResultFromService, ConvertResponseToForm } from '../common/Utility';
import { useUserContext, useUserDispatch } from '../common/UserContext';
import CustomizedDialog from '../components/Dialog';
import Confirm from '../components/Confirm';

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  buttonProgress: {
    color: green[500],
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -12,
    marginLeft: -12,
  }
}));

export default function SignIn(props) {
    var s = new serviceList();
    var dictionary = new Dictionary();
    var viewInfo = new ViewInfo();
    var view = viewInfo.create_account;

    let [inputs, setInputs] = useState({loginId:'',password:''});
    let { loginId, password } = inputs;

    let [checkInputs, setCheckInputs] = useState({
                                                  helpTextLoginId:''
                                                , helpTextPassword:''
                                                , errorLoginId:false
                                                , errorPassword:false
                                                });
    
    let [loading, setLoading] = useState(false);
    const classes = useStyles();
    const userContext = useUserContext();

/////////////Setting for Snackbar////////////////////////////////
    let [snackbar, setSnackbar] = useState({openSnackbar:false, severity:'', message:''});
    let { openSnackbar, severity, message } = snackbar;
  
    const onCloseSnackbar = () => {
      setSnackbar(prev => ({
        ...prev,
        openSnackbar:false,
        severity:'',
        message:''
      }));
    }
////////////////////////////////////////////////////////////////

//////Dialog event
const [isDialogOpen, setIsDialogOpen] = useState(false);
const [isModified, setIsModified] = useState(false);

const callFromDialog = (p) => {
  setIsModified(p.modified);
}

const handleDialogClose = (forceNoMessage) => {
  let forceClose = false;

  if (typeof forceNoMessage === 'boolean') forceClose = forceNoMessage;

  if (isModified && !forceClose) {
    setConfirmMessage({
      isConfirmOpen: true,
      messageType: dictionary.WARNING,
      messageId: "msg_confirm_close_dialog" 
    });
  } else {
    setIsDialogOpen(false);
  }
};

const handleDialogOpen = (operateType, id) => {
  setIsDialogOpen(true);
};
////////////////////////////////////////////////////////

//////Confrim event in closing the dialog///////////////
const [confirmMessage, setConfirmMessage] = useState({
  isConfirmOpen : false,
  messageType : "",
  messageId : ""
});
let { isConfirmOpen,messageType,messageId } = confirmMessage;

const handleConfirmDialog = (answer) => {
  setConfirmMessage({
    ...confirmMessage,
    isConfirmOpen:false
  });

  if (answer){
    setIsDialogOpen(false);
  }
}
////////////////////////////////////////////////////////

const userContextDispatch = useUserDispatch();
  
const onChange = e => {
  const {name, value} = e.target;
  
  setInputs({
    ...inputs,
    [name]:value
  });
}
    
const onLogin = (e) => {
  if (!loginId && !password) {
    setCheckInputs({ helpTextLoginId: "Please fill out."
                    ,helpTextPassword : "Please fill out."
                    ,errorLoginId:true
                    ,errorPassword:true
                  });
  } else if (!loginId) {
    setCheckInputs({ helpTextLoginId: "Please fill out."
                    ,helpTextPassword : ""
                    ,errorLoginId:true
                    ,errorPassword:false
                  });
  } else {
    setCheckInputs({ helpTextLoginId: ""
                    ,helpTextPassword : "Please fill out."
                    ,errorLoginId:false
                    ,errorPassword:true
                  });
  }

  setLoading(true);
  RunService(s.login, inputs)
          .then(response => {
            setLoading(false);
            var result = CheckResultFromService(response.data);
            if (result.isSuccess) {
              let responseData = ConvertResponseToForm(s.login, response.data);
              userContextDispatch({type: dictionary.SET_USER_INFO,data: responseData});

              setSnackbar(prev => ({
                ...prev,
                openSnackbar:true,
                severity:dictionary.MessageType.Success,
                message:GetText(result.message)
              }));
              props.history.push('/mainpage');
            } else {
              setSnackbar(prev => ({
                ...prev,
                openSnackbar:true,
                severity:dictionary.MessageType.Error,
                message:GetText(result.message)
              }));
            }
          })
          .catch(error => {
            setSnackbar(prev => ({
              ...prev,
              openSnackbar:true,
              severity:dictionary.MessageType.Error,
              message:error
            }));
          })
} 
 
  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          {GetText('sign_in')}
        </Typography>
        <form className={classes.form} >
          <TextField
            id="loginId"
            variant="outlined"
            margin="normal"
            required
            fullWidth
            label={GetText('user_id')}
            name="loginId"
            autoComplete="loginId"
            autoFocus
            value={loginId}
            onChange={onChange}
            helperText={checkInputs.helpTextLoginId}
            error={checkInputs.errorLoginId}
          />
          <TextField
            id="password"
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label={GetText('password')}
            type="password"
            autoComplete="current-password"
            value={password}
            onChange={onChange}
            helperText={checkInputs.helpTextPassword}
            error={checkInputs.errorPassword}
          />
          <Button 
              disabled={loading}
              color={userContext.preference.color}
              name="createAccount"
              onClick={handleDialogOpen}
            >
              { GetText("create_account") }
            </Button>
          <Button
            fullWidth={true}
            variant="contained"
            color={userContext.preference.color}
            size="large"
            className={classes.submit}
            disabled={loading}
            onClick={onLogin}
          >
            {GetText("sign_in")}
          </Button>
          
          {loading && <CircularProgress size={24} className={classes.buttonProgress} />}
          <CustomizedDialog isDialogOpen={isDialogOpen} viewInfo={viewInfo.create_account} handleDialogClose={handleDialogClose} callFromDialog={callFromDialog} />
          <Confirm open={isConfirmOpen} MessageType={messageType} MessageId={messageId} handleClose={handleConfirmDialog} />
          <CustomizedSnackbar isOpen={openSnackbar} severity={severity} message={message} closeHandler={onCloseSnackbar} />
        </form>
      </div>
    </Container>
  );
}