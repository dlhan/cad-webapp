import React, { useState, useEffect } from 'react';
import { GetText } from '../../common/BundleManager';
import serviceList from '../../service/ServiceList';
import { RunService } from '../../service/RestApi';
import Dictionary from '../../common/Dictionary';
import { CheckResultFromService, ResetForResponseData, CheckMenuAuth } from '../../common/Utility';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import FormLabel from '@material-ui/core/FormLabel';
import Address from '../../components/Address';
import ActionPanel from '../../components/ActionPanel';
import { FormStyle } from '../../style/Style';
import CustomizedSwitch from '../../components/Switch';
import { useUserContext } from '../../common/UserContext';
import CustomizedSnackbar from '../../components/Snackbar';
import CircularProgress from '@material-ui/core/CircularProgress';
import { useForm } from "react-hook-form";
import Confirm from '../../components/Confirm';
import UpperAndOfficeSelect from '../../components/UpperAndOfficeSelect';
import CustomizedTextField from '../../components/TextField';
import CodeComboBox from '../../components/CodeComboBox';
import YoutubeSearchedForIcon from '@material-ui/icons/YoutubeSearchedFor';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import CustomizedRadioGroup from '../../components/RadioGroup';
import TextFieldWithIcon from '../../components/TextFieldWithIcon';

const useStyles = FormStyle;
let originalInputs = {};
let isModified = false;

export default function PersonBasicInfo(props) {
  const classes = useStyles();
  var dictionary = new Dictionary();
  const userContext = useUserContext();
  const s = new serviceList();
  const [operateType, setOperateType] = useState(props.sendInfo.operateType);
  const mode = props.sendInfo.mode;
  let [inputs, setInputs] = useState({personId:props.personId
                                    , firstName:''
                                    , lastName:''
                                    , userId:''
                                    , password:''
                                    , birthDay:''
                                    , gender: 'F'
                                    , enrollDate:''
                                    , isUse:''
                                    , isSystemAdministrator:false
                                    , notUseReason: ''
                                    , postalCode:''
                                    , etcAddress:''
                                    , upperWardId:''
                                    , wardId:''
                                    , positionCode:''
                                    , rankCode:''
                                    , jobCode:''
                                    , workTypeCode:''
                                    , homeTel:''
                                    , mobileNum:''
                                    , workTel:''
                                    , extNum:''
                                    , faxNum:''
                                    , email:''
                                    , isApproved:props.sendInfo.mode === 'admin' ? true : ""
                                    });
  let { personId, firstName, lastName,userId,password,birthDay,gender,enrollDate,isUse,isSystemAdministrator,notUseReason,postalCode,etcAddress,wardId,upperWardId,positionCode,rankCode,jobCode,workTypeCode,homeTel,mobileNum,workTel,extNum,faxNum,email,isApproved } = inputs;
  let [loading, setLoading] = useState(false);
  let [disabled, setDisabled] = useState(false);
  let [isUpdateMode, setIsUpdateMode] = useState(false);
  let [showPassword, setShowPassword] = useState(false);
  let [checkLoginId, setCheckLoginId] = useState({
    loginIdChecked: false,
    helpText: '',
    error: false
  });
  const hasUserCreateAuth = CheckMenuAuth(["CA001"]);
  
/////////Setting for validate the inputs in the form.
  const { handleSubmit } = useForm();
////////////////////////////////////////////////////

  const onChange = e => {
    let {name, value} = e.target;
    
    if (name === "isUse") { value = !inputs.isUse};
    if (name === "isSystemAdministrator") { value = !inputs.isSystemAdministrator};
    if (name === 'userId' && !checkLoginId.loginIdChecked) {
      setCheckLoginId({...checkLoginId, helpText:GetText("msg_check_loginid")});
    }
    isModified = true;
    setInputs({
      ...inputs,
      [name]:value
    });
  }

/////////////Setting for Snackbar////////////////////////////////
  let [snackbar, setSnackbar] = useState({openSnackbar:false, severity:'', message:''});
  let { openSnackbar, severity, message } = snackbar;
    
  const onCloseSnackbar = () => {
    setSnackbar(prev => ({
      ...prev,
      openSnackbar:false,
      severity:'',
      message:''
    }));
  }
/////////////////////////////////////////////

//////Confrim event
  const [confirmMessage, setConfirmMessage] = React.useState({
                                                      isConfirmOpen : false,
                                                      messageType : "",
                                                      messageId : ""
                                                });
  let { isConfirmOpen,messageType,messageId } = confirmMessage;
  
  const handleConfirmDialog = (answer) => {
    setConfirmMessage({
      ...confirmMessage,
      isConfirmOpen:false
    });
  
    if (answer){
      onSubmit();
    }
  }
  /////////////////////////////////////////////
  
  ////Button operate
  
  const onChangeUpdateState = () => {
    setIsUpdateMode(!isUpdateMode);
    if (!isUpdateMode) {
      onUpdateMode();
    } else {
      onReadMode(personId);
    }
  }

  const onReadMode = (id) => {
    setDisabled(true);
    setOperateType("");
    setIsUpdateMode(false);
    setCheckLoginId({...checkLoginId, loginIdChecked:true});
    props.callFromDialog({id:id, operateType:dictionary.READ, modified: false});
  }

  const onNewMode = () => {
    setDisabled(false);
    setOperateType(dictionary.NEW);
    setInputs({ personId:''
              , firstName:''
              , lastName:''
              , userId:''
              , password:''
              , birthDay:''
              , gender: 'F'
              , enrollDate:''
              , isUse:''
              , isSystemAdministrator:false
              , notUseReason: ''
              , postalCode:''
              , etcAddress:''
              , upperWardId:''
              , wardId:''
              , positionCode:''
              , rankCode:''
              , jobCode:''
              , workTypeCode:''
              , homeTel:''
              , mobileNum:''
              , workTel:''
              , extNum:''
              , faxNum:''
              , email:''
              , isApproved:props.sendInfo.mode === 'admin' ? true : ""
            });
    props.callFromDialog({id:'', operateType:dictionary.NEW, modified: false});
    originalInputs = {personId:''
                    , firstName:''
                    , lastName:''
                    , userId:''
                    , password:''
                    , birthDay:''
                    , gender: 'F'
                    , enrollDate:''
                    , isUse:''
                    , isSystemAdministrator:false
                    , notUseReason: ''
                    , postalCode:''
                    , etcAddress:''
                    , upperWardId:''
                    , wardId:''
                    , positionCode:''
                    , rankCode:''
                    , jobCode:''
                    , workTypeCode:''
                    , homeTel:''
                    , mobileNum:''
                    , workTel:''
                    , extNum:''
                    , faxNum:''
                    , email:''
                    , isApproved:props.sendInfo.mode === 'admin' ? true : ""
                  };
    isModified = false;
    setCheckLoginId({...checkLoginId, loginIdChecked:false});
  }

  const onUpdateMode = () => {
    setDisabled(false);
    setOperateType(dictionary.UPDATE);
    props.callFromDialog({id:personId, operateType:dictionary.UPDATE, modified: false});
  }

  const onConfirm = () => {
    if (operateType === dictionary.NEW || operateType === dictionary.UPDATE) {
      if (!checkLoginId.loginIdChecked) {
        setSnackbar(prev => ({
          ...prev,
          openSnackbar:true,
          severity:dictionary.MessageType.Error,
          message:GetText("msg_check_loginid")
        }));
  
        return;
      }
    }

    if (operateType === dictionary.NEW) {
      setConfirmMessage({ isConfirmOpen: true, messageType: dictionary.WARNING, messageId : "msg_will_you_insert" });
    } else if (operateType === dictionary.UPDATE) {
      setConfirmMessage({ isConfirmOpen: true, messageType: dictionary.WARNING, messageId : "msg_will_you_update" });
    } else if (hasUserCreateAuth) {
      onNewMode();
    }
  }

  const onDelete = () => {
    setOperateType(dictionary.DELETE);
    setConfirmMessage({ isConfirmOpen: true, messageType: dictionary.WARNING, messageId : "msg_will_you_delete" });
  }

/////////////////Check whether inputs are modified or not. If modified, it sends the state to parent//////////////////// 
  useEffect(() => {
    let Modified =  (JSON.stringify(inputs) === JSON.stringify(originalInputs)? false : true);
    
    if (operateType !== dictionary.READ) props.callFromDialog({id:personId, modified: Modified && isModified});
  }, [inputs]);
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////Get data////////////////////////////////////////////
  useEffect(() => {
    async function getInfo(id) {
      let body = {"personId": id};
      setLoading(true);
      RunService(s.getFireWorker, body)
          .then(response => {
            let reponseData = ResetForResponseData(s.getFireWorker.response, response.data);
            setInputs(reponseData);
            originalInputs = reponseData;
            onReadMode(id);
            setLoading(false);
          })
          .catch(error => {
            setSnackbar(prev => ({
              ...prev,
              openSnackbar:true,
              severity:dictionary.MessageType.Error,
              message:GetText("msg_normal_error_message")
            }));
          })
    }

    if (operateType === dictionary.READ && props.sendInfo.id) getInfo(props.sendInfo.id);
  }, []);
  /////////////////////////////////////////////////////////////////

  ///////////////////// Insert or Update///////////////////////////
  const onSubmit = () => {
    if (operateType === "") {
      onNewMode();
      return;
    }

    setLoading(true);
    let service;
    let body;

    switch(operateType) {
      case dictionary.NEW :
        service = s.insertFireWorker;
        body = inputs;
        break;
      case dictionary.UPDATE :
        service = s.updateFireWorker;
        body = inputs;
        break;
      case dictionary.DELETE :
        service = s.deleteFireWorker;
        body = [inputs.personId]; 
        break;
      default :
        return;
    }
  
    RunService(service, body)
        .then(response => {
          setLoading(false);
          var result = CheckResultFromService(response.data);
          if (result.isSuccess) {
            setSnackbar(prev => ({
              ...prev,
              openSnackbar:true,
              severity:dictionary.MessageType.Success,
              message:GetText(result.message)
            }));
            
            setInputs({
              ...inputs,
              personId:result.id
            });
            onReadMode(result.id);
            
            if (operateType === dictionary.DELETE) {
              props.handleDialogClose(true);
            } else {
              props.callFromDialog({id:result.id, modified: false, refresh:true});
            }
          } else {
            setSnackbar(prev => ({
              ...prev,
              openSnackbar:true,
              severity:dictionary.MessageType.Error,
              message:GetText(result.message)
            }));
          }
        })
        .catch(error => {
          setLoading(false);
          setSnackbar(prev => ({
            ...prev,
            openSnackbar:true,
            severity:dictionary.MessageType.Error,
            message:GetText("msg_normal_error_message")
          }));
        })
  } 
  /////////////////////////////////////////////////////////////////

  const checkLoginDuplicated = () => {
    setLoading(true);
    var body = {loginId: userId};
    RunService(s.getCheckLoginIdDuplicated, body)
          .then(response => {
            if (response.data) {//true is duplicated, false:it is unique.
              setCheckLoginId({loginIdChecked:false, helpText:GetText("msg_id_duplicated"), error:true});
            } else {
              setCheckLoginId({loginIdChecked:true, helpText:GetText("msg_id_available"), error:false});
            }
            setLoading(false);
          })
          .catch(error => {
            setSnackbar(prev => ({
              ...prev,
              openSnackbar:true,
              severity:dictionary.MessageType.Error,
              message:GetText("msg_normal_error_message")
            }));
            setLoading(false);
          })
  }

  const onPasswordIconEvent = () => {
    setShowPassword(!showPassword);
  }

  return (
    <React.Fragment>
      <form onSubmit={handleSubmit(onConfirm)}>
      <div className={classes.paper}>
        <FormLabel component="legend">{GetText("basic_information")}</FormLabel>
      </div>
      <Paper className={classes.control}>
        <Grid container spacing={1}>
          <Grid item xs={12}>
            <div className={classes.paper}>
            <CustomizedTextField
                id="firstName"
                label={GetText("first_name")}
                onChange={onChange}
                disabled={disabled}
                value={firstName}
                required={true}
                className={classes.textFieldS}
              />     
              <CustomizedTextField
                id="lastName"
                label={GetText("last_name")}
                onChange={onChange}
                disabled={disabled}
                value={lastName}
                required={true}
                className={classes.textFieldS}
              />  
              <CustomizedTextField
                id="birthDay"
                label={GetText("birth_day")}
                type="date"
                onChange={onChange}
                disabled={disabled}
                value={birthDay}
                className={classes.textFieldS}
              />
              <CustomizedRadioGroup 
                id="gender" 
                label={GetText("gender")}
                defaultValue={gender}
                value={gender}
                radios={[{label:GetText("female"), value:"F"},{label:GetText("male"), value:"M"}]} 
                onChange={onChange}
                disabled={disabled}
                className={classes.textFieldS}
              />
              {mode === 'admin' && 
                <CustomizedTextField
                  id="enrollDate"
                  label={GetText("enroll_date")}
                  type="date"
                  onChange={onChange}
                  disabled={disabled}
                  value={enrollDate}
                  className={classes.textFieldS}
                />
              }
            </div>
          </Grid>
          <Grid item xs={12}>
            <div className={classes.paper}>
            <TextFieldWithIcon 
                id="userId"
                label={GetText("user_id")}
                onChange={onChange}
                disabled={disabled || operateType === dictionary.UPDATE}
                value={userId}
                iconEvent={checkLoginDuplicated}
                icon={operateType === dictionary.NEW ? <YoutubeSearchedForIcon /> : ""}
                helpText={checkLoginId.helpText}
                error={checkLoginId.error}
                required={true}
                className={classes.textFieldS}
              />
              {operateType === dictionary.NEW &&
                <TextFieldWithIcon 
                  id="password"
                  label={GetText("password")}
                  onChange={onChange}
                  type={showPassword ? 'text' : 'password'}
                  disabled={disabled}
                  value={password}
                  iconEvent={onPasswordIconEvent}
                  icon={showPassword ? <Visibility /> : <VisibilityOff />}
                  required={true}
                  className={classes.textFieldS}
                />
              }

              <CustomizedSwitch 
                id='isSystemAdministrator'
                name='isSystemAdministrator' 
                checked={isSystemAdministrator}
                onChange={onChange}
                value={isSystemAdministrator}
                label={GetText("is_ward_administrator")} 
                disabled={disabled}
              />  
              {mode === 'admin' && 
                <CustomizedSwitch 
                  id='isUse'
                  name='isUse' 
                  checked={isUse}
                  onChange={onChange}
                  value={isUse}
                  label={GetText("is_use")} 
                  disabled={disabled}
                />  
              }
              {mode === 'admin' && 
                <CustomizedTextField
                  label={GetText("not_use_reason")}
                  id="notUseReason"
                  onChange={onChange}
                  disabled={disabled}
                  value={notUseReason}
                />
              }
            </div>
          </Grid>
          <Grid item xs={12}>
            <div className={classes.paper}>
              <Address 
                addressId={'postalCode'} 
                etcAddressid={'etcAddress'} 
                changeEvent={onChange}
                disabled={disabled}
                postalCodeValue={postalCode}
                etcAddressValue={etcAddress}
              />
            </div>
          </Grid>
        </Grid>
      </Paper>   
      <div className={classes.paper}>
        <FormLabel component="legend">{GetText("basic_information")}</FormLabel>
      </div>
      <Paper className={classes.control}>
        <Grid container spacing={1}>
          <Grid item xs={12}>
            <div className={classes.paper}>
              <UpperAndOfficeSelect 
                callEvent={onChange} 
                selectedUpperWardValue={upperWardId}
                selectedWardValue={wardId}
                disabled={disabled}
                className={classes.textFieldS}
              /> 
              <CodeComboBox
                  groupCode={'P030'}
                  selectedCode={positionCode}
                  childLabel={GetText("position")}
                  childComboId={'positionCode'}
                  callEvent={onChange}
                  childOnly={true}
                  disabled={disabled}
                  className={classes.textFieldS}
              />
              <CodeComboBox
                  groupCode={'P031'}
                  selectedCode={rankCode}
                  childLabel={GetText("job_class")}
                  childComboId={'rankCode'}
                  callEvent={onChange}
                  childOnly={true}
                  disabled={disabled}
                  className={classes.textFieldS}
              /> 
              <CodeComboBox
                  groupCode={'P029'}
                  selectedCode={jobCode}
                  childLabel={GetText("job")}
                  childComboId={'jobCode'}
                  callEvent={onChange}
                  childOnly={true}
                  disabled={disabled}
                  className={classes.textFieldS}
              /> 
              <CodeComboBox
                  groupCode={'P028'}
                  selectedCode={workTypeCode}
                  childLabel={GetText("wark_cls")}
                  childComboId={'workTypeCode'}
                  callEvent={onChange}
                  childOnly={true}
                  disabled={disabled}
                  className={classes.textFieldS}
              /> 
            </div>
          </Grid>
        </Grid>
      </Paper>   
      <div className={classes.paper}>
        <FormLabel component="legend">{GetText("contact")}</FormLabel>
      </div>
      <Paper className={classes.control}>
        <Grid container spacing={1}>
          <Grid item xs={12}>
            <div className={classes.paper}>
              <CustomizedTextField
                label={GetText("home_tel")}
                id="homeTel"
                onChange={onChange}
                disabled={disabled}
                value={homeTel}
                className={classes.textFieldS}
              />
              <CustomizedTextField
                label={GetText("mobile_number")}
                id="mobileNum"
                onChange={onChange}
                disabled={disabled}
                value={mobileNum}
                className={classes.textFieldS}
              />
              <CustomizedTextField
                label={GetText("office_tel")}
                id="workTel"
                onChange={onChange}
                disabled={disabled}
                value={workTel}
                className={classes.textFieldS}
              />
              <CustomizedTextField
                label={GetText("ext_num")}
                id="extNum"
                onChange={onChange}
                disabled={disabled}
                value={extNum}
                className={classes.textFieldS}
              />
              <CustomizedTextField
                label={GetText("fax")}
                id="faxNum"
                onChange={onChange}
                disabled={disabled}
                value={faxNum}
                className={classes.textFieldS}
              />
              <CustomizedTextField
                label={GetText("email")}
                id="email"
                onChange={onChange}
                disabled={disabled}
                value={email}
                className={classes.textFieldS}
              />
            </div>
          </Grid>
        </Grid>
      </Paper> 
      <Box p={1}></Box>
      <Divider style={{'width':'100%','position': 'absolute', 'left': 0}} />
      <ActionPanel>
        <Button 
          disabled={loading}
          color={userContext.preference.color}
          type="submit"
        >
          { operateType !== "" ? GetText("save") : hasUserCreateAuth ? GetText("new") : "" }
        </Button>
        { 
          inputs.personId && operateType !== dictionary.NEW && hasUserCreateAuth &&
            <Button 
              disabled={loading}
              color={userContext.preference.color}
              name="update"
              onClick={onChangeUpdateState}
            >
              { isUpdateMode ? GetText("cancel") : GetText("update") }
            </Button>
        }
        
        {
          inputs.personId && hasUserCreateAuth &&
            <Button 
              disabled={loading}
              color={userContext.preference.color}
              name="delete"
              onClick={onDelete}
          >
              {GetText("delete")}
            </Button> 
        }
        
        {loading && <CircularProgress size={24} className={classes.buttonProgress} />}
      </ActionPanel>
      </form>
      <CustomizedSnackbar isOpen={openSnackbar} severity={severity} message={message} closeHandler={onCloseSnackbar} />
      <Confirm open={isConfirmOpen} MessageType={messageType} MessageId={messageId} handleClose={handleConfirmDialog} />
    </React.Fragment>
  );
}