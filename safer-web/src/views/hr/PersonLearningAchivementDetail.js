import React, { useState, useEffect } from 'react';
import { GetText } from '../../common/BundleManager';
import serviceList from '../../service/ServiceList';
import { RunService } from '../../service/RestApi';
import Dictionary from '../../common/Dictionary';
import { CheckResultFromService, ResetForResponseData } from '../../common/Utility';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import ActionPanel from '../../components/ActionPanel';
import { FormStyle } from '../../style/Style';
import { useUserContext } from '../../common/UserContext';
import CustomizedSnackbar from '../../components/Snackbar';
import CircularProgress from '@material-ui/core/CircularProgress';
import { useForm } from "react-hook-form";
import Confirm from '../../components/Confirm';
import CustomizedTextField from '../../components/TextField';
import CodeComboBox from '../../components/CodeComboBox';

const useStyles = FormStyle;
let originalInputs = {};
let isModified = false;

export default function PersonLearningAchivementDetail(props) {
  const classes = useStyles();
  var dictionary = new Dictionary();
  const userContext = useUserContext();
  const s = new serviceList();

  const [operateType, setOperateType] = useState(props.sendInfo.operateType);
  let [inputs, setInputs] = useState({personEducationPromotionId:props.sendInfo.id
                                    , personId: props.sendInfo.personId
                                    , courseName:''
                                    , institutionName:''
                                    , startDate:''
                                    , endDate:''
                                    , educationClsCode:''
                                    , completeCode:''
                                    , score:''
                                    });
  let { personEducationPromotionId, personId, courseName, institutionName, startDate,endDate,educationClsCode,completeCode,score } = inputs;
  let [loading, setLoading] = useState(false);
  let [disabled, setDisabled] = useState(false);
  let [isUpdateMode, setIsUpdateMode] = useState(false);
 
/////////Setting for validate the inputs in the form.
  const { handleSubmit } = useForm();
////////////////////////////////////////////////////

  const onChange = e => {
    let {name, value} = e.target;

    isModified = true;
    setInputs({
      ...inputs,
      [name]:value
    });
  }

/////////////Setting for Snackbar////////////////////////////////
  let [snackbar, setSnackbar] = useState({openSnackbar:false, severity:'', message:''});
  let { openSnackbar, severity, message } = snackbar;
    
  const onCloseSnackbar = () => {
    setSnackbar(prev => ({
      ...prev,
      openSnackbar:false,
      severity:'',
      message:''
    }));
  }
/////////////////////////////////////////////

//////Confrim event
  const [confirmMessage, setConfirmMessage] = React.useState({
                                                      isConfirmOpen : false,
                                                      messageType : "",
                                                      messageId : ""
                                                });
  let { isConfirmOpen,messageType,messageId } = confirmMessage;
  
  const handleConfirmDialog = (answer) => {
    setConfirmMessage({
      ...confirmMessage,
      isConfirmOpen:false
    });
  
    if (answer){
      onSubmit();
    }
  }
  /////////////////////////////////////////////
  
  ////Button operate
  
  const onChangeUpdateState = () => {
    setIsUpdateMode(!isUpdateMode);
    if (!isUpdateMode) {
      onUpdateMode();
    } else {
      onReadMode(personEducationPromotionId);
    }
  }

  const onReadMode = () => {
    setDisabled(true);
    setOperateType("");
    setIsUpdateMode(false);
    props.callFromDialog({modified: false});
  }

  const onNewMode = () => {
    setDisabled(false);
    setOperateType(dictionary.NEW);
    setInputs({personEducationPromotionId:''
            , personId: props.sendInfo.personId
            , courseName:''
            , institutionName:''
            , startDate:''
            , endDate:''
            , educationClsCode:''
            , completeCode:''
            , score:''
            });
    props.callFromDialog({modified: false});
    originalInputs = {personEducationPromotionId:''
        , personId: props.sendInfo.personId
        , courseName:''
        , institutionName:''
        , startDate:''
        , endDate:''
        , educationClsCode:''
        , completeCode:''
        , score:''
        };
    isModified = false;
  }

  const onUpdateMode = () => {
    setDisabled(false);
    setOperateType(dictionary.UPDATE);
    props.callFromDialog({modified: false});
  }

  const onConfirm = () => {
    if (operateType === dictionary.NEW) {
      setConfirmMessage({ isConfirmOpen: true, messageType: dictionary.WARNING, messageId : "msg_will_you_insert" });
    } else if (operateType === dictionary.UPDATE) {
      setConfirmMessage({ isConfirmOpen: true, messageType: dictionary.WARNING, messageId : "msg_will_you_update" });
    } else {
      onNewMode();
    }
  }

  const onDelete = () => {
    setOperateType(dictionary.DELETE);
    setConfirmMessage({ isConfirmOpen: true, messageType: dictionary.WARNING, messageId : "msg_will_you_delete" });
  }

/////////////////Check whether inputs are modified or not. If modified, it sends the state to parent//////////////////// 
  useEffect(() => {
    let Modified =  (JSON.stringify(inputs) === JSON.stringify(originalInputs)? false : true);
    
    if (operateType !== dictionary.READ) props.callFromDialog({modified: Modified && isModified});
  }, [inputs]);
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////Get data////////////////////////////////////////////
  useEffect(() => {
    let completed = false; 
    
    async function getInfo(id) {
      let body = {"personEducationPromotionId": id};
      setLoading(true);
      RunService(s.getPersonLearningInfo, body)
          .then(response => {
            if (!completed) { 
              let reponseData = ResetForResponseData(s.getPersonLearningInfo.response, response.data);
              setInputs(reponseData);
              originalInputs = reponseData;
            }
            completed= true;
            onReadMode();
            setLoading(false);
          })
          .catch(error => {
            setSnackbar(prev => ({
              ...prev,
              openSnackbar:true,
              severity:dictionary.MessageType.Error,
              message:GetText("msg_normal_error_message")
            }));
          })
    }

    if (operateType === dictionary.READ && props.sendInfo.id) getInfo(props.sendInfo.id);
    
    return () => {
      completed = true;
    };
  }, []);
  /////////////////////////////////////////////////////////////////

  ///////////////////// Insert or Update///////////////////////////
  const onSubmit = () => {
    if (operateType === "") {
      onNewMode();
      return;
    }

    setLoading(true);
    let service;
    let body;

    switch(operateType) {
      case dictionary.NEW :
        service = s.insertPersonLearningInfo;
        body = inputs;
        break;
      case dictionary.UPDATE :
        service = s.updatePersonLearningInfo;
        body = inputs;
        break;
      case dictionary.DELETE :
        service = s.deletePersonLearningInfo;
        body = [inputs.personEducationPromotionId]; 
        break;
      default :
        return;
    }
  
    RunService(service, body)
        .then(response => {
          setLoading(false);
          var result = CheckResultFromService(response.data);
          if (result.isSuccess) {
            setSnackbar(prev => ({
              ...prev,
              openSnackbar:true,
              severity:dictionary.MessageType.Success,
              message:GetText(result.message)
            }));
            
            setInputs({
              ...inputs,
              personEducationPromotionId:result.id
            });
            onReadMode();
            
            if (operateType === dictionary.DELETE) {
              props.callFromDialog({modified: false, refresh:true});
              props.handleDialogClose(true);
            } else {
              props.callFromDialog({modified: false, refresh:true});
            }

          } else {
            setSnackbar(prev => ({
              ...prev,
              openSnackbar:true,
              severity:dictionary.MessageType.Error,
              message:GetText(result.message)
            }));
          }
        })
        .catch(error => {
          setLoading(false);
          setSnackbar(prev => ({
            ...prev,
            openSnackbar:true,
            severity:dictionary.MessageType.Error,
            message:GetText("msg_normal_error_message")
          }));
        })
  } 
  /////////////////////////////////////////////////////////////////

  return (
    <React.Fragment>
      <form onSubmit={handleSubmit(onConfirm)}>
      <Paper className={classes.control}>
        <Grid container spacing={1}>
          <Grid item xs={12}>
            <div className={classes.paper}>
              <CustomizedTextField
                label={GetText("course_name")}
                id="courseName"
                className={classes.textField25}
                onChange={onChange}
                disabled={disabled}
                value={courseName}
              />    
              <CustomizedTextField
                label={GetText("institution_name")}
                id="institutionName"
                className={classes.textField25}
                onChange={onChange}
                disabled={disabled}
                value={institutionName}
              />    
            </div>
          </Grid>
          <Grid item xs={12}>
            <div className={classes.paper}>
              <CustomizedTextField
                id="startDate"
                label={GetText("start_date")}
                type="date"
                className={classes.textField25}
                onChange={onChange}
                disabled={disabled}
                value={startDate}
              />
              <CustomizedTextField
                id="endDate"
                label={GetText("end_date")}
                type="date"
                className={classes.textField25}
                onChange={onChange}
                disabled={disabled}
                value={endDate}
              />
            </div>
          </Grid>
          <Grid item xs={12}>
            <div className={classes.paper}>
              <CodeComboBox
                  groupCode={'P034'}
                  selectedCode={educationClsCode}
                  childLabel={GetText("education_classification")}
                  className={classes.textField25}
                  childComboId={'educationClsCode'}
                  callEvent={onChange}
                  childOnly={true}
                  disabled={disabled}
              />
              <CodeComboBox
                  groupCode={'P035'}
                  selectedCode={completeCode}
                  childLabel={GetText("education_completed")}
                  className={classes.textField25}
                  childComboId={'completeCode'}
                  callEvent={onChange}
                  childOnly={true}
                  disabled={disabled}
              />
            </div>
          </Grid>
        </Grid>
      </Paper>   
      
      <Box p={1}></Box>
      <Divider style={{'width':'100%','position': 'absolute', 'left': 0}} />
      <ActionPanel>
        <Button 
          disabled={loading}
          color={userContext.preference.color}
          type="submit"
        >
          { operateType !== "" ? GetText("save") : GetText("new") }
        </Button>
        { 
          inputs.personEducationPromotionId && operateType !== dictionary.NEW &&
            <Button 
              disabled={loading}
              color={userContext.preference.color}
              name="update"
              onClick={onChangeUpdateState}
            >
              { isUpdateMode ? GetText("cancel") : GetText("update") }
            </Button>
        }
        
        {
          inputs.personEducationPromotionId && 
            <Button 
              disabled={loading}
              color={userContext.preference.color}
              name="delete"
              onClick={onDelete}
          >
              {GetText("delete")}
            </Button> 
        }
        
        {loading && <CircularProgress size={24} className={classes.buttonProgress} />}
      </ActionPanel>
      </form>
      <CustomizedSnackbar isOpen={openSnackbar} severity={severity} message={message} closeHandler={onCloseSnackbar} />
      <Confirm open={isConfirmOpen} MessageType={messageType} MessageId={messageId} handleClose={handleConfirmDialog} />
    </React.Fragment>
  );
}