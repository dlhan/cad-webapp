import React, { useState } from 'react';
import { GetText } from '../../common/BundleManager'
import serviceList from '../../service/ServiceList';
import CircularProgress from '@material-ui/core/CircularProgress';
import CustomizedSnackbar from '../../components/Snackbar';
import Dictionary from '../../common/Dictionary';
import { RunService } from '../../service/RestApi';
import { SetSearchBody, ResetForResponseData, CheckMenuAuth } from '../../common/Utility';
import StickyHeadTable from '../../components/Table';
import CustomizedBreadcrumb from '../../components/BreadCrumb';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import SearchIcon from '@material-ui/icons/Search';
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';
import Fab from '@material-ui/core/Fab';
import CustomizedDialog from '../../components/Dialog';
import { FormStyle } from '../../style/Style';
import ViewInfo from '../../common/ViewInfo';
import { useUserContext } from '../../common/UserContext';
import Confirm from '../../components/Confirm';
import UpperAndOfficeSelect from '../../components/UpperAndOfficeSelect';
import CodeComboBox from '../../components/CodeComboBox';
import AccordionSearchBox from '../../components/AccordionSearchBox';
import CustomizedTextField from '../../components/TextField';

const useStyles = FormStyle;

const columns = [
  { id: 'personId', numeric: false, disablePadding: false, label: '', align: 'center', minWidth: 0 },
  { id: 'upperWardName', numeric: false, disablePadding: false, label: 'upper_ward_name', align: 'center', minWidth: 150 },
  { id: 'wardName', numeric: false, disablePadding: false, label: 'ward_name', align: 'center', minWidth: 150 },
  { id: 'personName', numeric: false, disablePadding: false, label: 'user_name', align: 'center', minWidth: 200 },
  { id: 'userId', numeric: false, disablePadding: false, label: 'user_id', align: 'center', minWidth: 120 },  
  { id: 'position', numeric: false, disablePadding: false, label: 'position', align: 'center', minWidth: 220 },
  { id: 'rank', numeric: false, disablePadding: false, label: 'job_class', align: 'center', minWidth: 150 },
  { id: 'job', numeric: false, disablePadding: false, label: 'job', align: 'center', minWidth: 150 },
  { id: 'workType', numeric: false, disablePadding: false, label: 'wark_cls', align: 'center', minWidth: 200 }
];

const SearchPanel = (props) => {
  const classes = useStyles();
  const userContext = useUserContext();
  const { onChangeWardCombo, rows, onChange, onSearch } = props;

  return (
    <div className={classes.paper} id="search_panel">
      <Grid container spacing={1}>
        <Grid item xs={12}>
          <div className={classes.paper}>
          <UpperAndOfficeSelect className={classes.textField25} callEvent={onChangeWardCombo} checkDataToRefresh={rows}/>
          <CustomizedTextField
            label={GetText("first_name")}
            className={classes.textField25}
            id="firstName"
            onChange={onChange}
          />
          <CustomizedTextField
            label={GetText("last_name")}
            className={classes.textField25}
            id="lastName"
            onChange={onChange}
          />
          <CustomizedTextField
            label={GetText("user_id")}
            className={classes.textField25}
            id="userId"
            onChange={onChange}
          />
          
          </div>
        </Grid>
        <Grid item xs={12}>
          <div className={classes.paper}>
          <CodeComboBox
              groupCode={'P030'}
              className={classes.textField25}
              childLabel={GetText("job_position")}
              childComboId={'positionCode'}
              callEvent={onChange}
              childOnly={true}
            />
            <CodeComboBox
              groupCode={'P031'}
              className={classes.textField25}
              childLabel={GetText("job_class")}
              childComboId={'rankCode'}
              callEvent={onChange}
              childOnly={true}
            /> 
            <CodeComboBox
              groupCode={'P029'}
              className={classes.textField25}
              childLabel={GetText("job")}
              childComboId={'jobCode'}
              callEvent={onChange}
              childOnly={true}
            /> 
            <CodeComboBox
              groupCode={'P028'}
              className={classes.textField25}
              childLabel={GetText("wark_cls")}
              childComboId={'workTypeCode'}
              callEvent={onChange}
              childOnly={true}
            /> 
          
            <div className={classes.grow} />
            <div className={classes.sectionSearch}>
            <Button
              variant="contained"
              color={userContext.preference.color}
              onClick={onSearch}
              className={classes.button}
              startIcon={<SearchIcon />}
            >
              {GetText("search")}
            </Button>
            </div>
          </div>
        </Grid>
      </Grid>
    </div>
  );
};

export default function PersonList(props) {
  const classes = useStyles();
  var s = new serviceList();
  var dictionary = new Dictionary();
  var viewInfo = new ViewInfo();
  var view = viewInfo.person_list;
  const userContext = useUserContext();

  let [loading, setLoading] = useState(false);
  const hasUserCreateAuth = CheckMenuAuth(["CA001"]);

////////Setting for Table//////////////////////////////////////////
  const onSelectRow = (rowId) => {
    handleDialogOpen(dictionary.READ, rowId)
  }

  const [page, setPage] = React.useState(1);
  const [totalDataCount, setTotalDataCount] = React.useState(0);
  const [rows, setRows] = useState([]);
  const [expanded, setExpanded] = useState(true);
  const [tableResize, setTableResize] = useState();
  const onPageChange = (event, newPage) => {
    setPage(newPage);
    getData(newPage, false);
  }
///////////////////////////////////////////////////////////////////

//////The parf for search ////////////////////////////////
  let [searchItems, setSearchItems] = useState({
                                       wardId:''
                                     , firstName:''
                                     , lastName:''
                                     , userId:''
                                     , positionCode:''
                                     , rankCode:''
                                     , jobCode:''
                                     , workTypeCode:''
                                     , locale : userContext.preference.language
                                     });
  let { wardId, firstName, lastName,userId,positionCode,rankCode, jobCode, workTypeCode,locale } = searchItems;

  const onChange = e => {
    let {name, value} = e.target;

     setSearchItems({
       ...searchItems,
       [name]:value
     });
  }
///////////////////////////////////////////////////////////////////

  const getData = (pageNumber, isSearchMode) => {
    setLoading(true);
    let body = SetSearchBody(s.getPersonList, searchItems, pageNumber + 1, userContext);

    RunService(s.getPersonList, body, true)
        .then(response => {
          setLoading(false);
          setTotalDataCount(response.data.page.totalCount);

          setRows(ResetForResponseData(s.getPersonList.response.rows,response.data.rows,"personId"));

          if (isSearchMode) {
            setSnackbar(prev => ({
              ...prev,
              openSnackbar:true,
              severity:dictionary.MessageType.Success,
              message:GetText("msg_data_refreshed")
            }));
          }
        })
        .catch(error => {
          setLoading(false);
          setSnackbar(prev => ({
            ...prev,
            openSnackbar:true,
            severity:dictionary.MessageType.Error,
            message:GetText("msg_normal_error_message")
          }));
        })
  }

  const onSearch = () => {
    let pageNumber = 0;
    setPage(pageNumber);
    getData(pageNumber, true);
  }

//Callback from the combo for upward and ward.
  const onChangeWardCombo = (e) => {
    setSearchItems({
      ...searchItems,
      [Object.keys(e)[0]]:e[Object.keys(e)[0]]
    });
  }

//////Dialog event
  const [isDialogOpen, setIsDialogOpen] = useState(false);
  const [isModified, setIsModified] = useState(false);
  
  const callFromDialog = (p) => {
    setIsModified(p.modified);
    if (p.operateType) {
      setSendInfo({...sendInfo, operateType:p.operateType, id:p.id, tabDisabled : p.operateType == dictionary.NEW ? true : false});
    }
    
    if (p.refresh) {
      onSearch();
    }
  }

  const [sendInfo, setSendInfo] = useState({operateType:'', id:'', tabDisabled:false, mode : dictionary.ADMIN});
  const { operateType, id ,tabDisabled, mode } = sendInfo;

  const handleDialogClose = (forceNoMessage) => {
    let forceClose = false;

    if (typeof forceNoMessage === 'boolean') forceClose = forceNoMessage;

    if (isModified && !forceClose) {
      setConfirmMessage({
        isConfirmOpen: true,
        messageType: dictionary.WARNING,
        messageId: "msg_confirm_close_dialog" 
      });
    } else {
      setIsDialogOpen(false);
    }
  };
  
  const handleDialogOpen = (operateType, id) => {
    setSendInfo({...sendInfo, operateType:operateType, id:id, tabDisabled : operateType == dictionary.NEW ? true : false});
    setIsDialogOpen(true);
  };
////////////////////////////////////////////////////////

//////Confrim event in closing the dialog///////////////
  const [confirmMessage, setConfirmMessage] = useState({
    isConfirmOpen : false,
    messageType : "",
    messageId : ""
  });
  let { isConfirmOpen,messageType,messageId } = confirmMessage;

  const handleConfirmDialog = (answer) => {
    setConfirmMessage({
      ...confirmMessage,
      isConfirmOpen:false
    });

    if (answer){
      setIsDialogOpen(false);
    }
  }
////////////////////////////////////////////////////////

/////////////Setting for Snackbar////////////////////////////////
  let [snackbar, setSnackbar] = useState({openSnackbar:false, severity:'', message:''});
  let { openSnackbar, severity, message } = snackbar;
    
  const onCloseSnackbar = () => {
    setSnackbar(prev => ({
      ...prev,
      openSnackbar:false,
      severity:'',
      message:''
    }));
  }
////////////////////////////////////////////////////////

////Accordion //////////////////////////////////////////
  const accordionId = "accordionId";
  const onChangeAccordion = () => {
    setExpanded(!expanded);
    setTimeout(function() {
      onTableResize(true);
    }, 300);
  }

//Resize the height of table whenever acoordioon is resized.
  const onTableResize = (e) => {
    setTableResize(document.getElementById(accordionId).clientHeight)
  }
 
  return (
    <React.Fragment>
      <CustomizedBreadcrumb viewInfo={view} />
      <Grid container spacing={1}>
        <Grid item xs={12}>
          <Paper className={classes.control}>
            <AccordionSearchBox  
              expanded={expanded}  
              searchPanel={<SearchPanel 
                              onChangeWardCombo={onChangeWardCombo} 
                              rows={rows} 
                              onChange={onChange} 
                              onSearch={onSearch} 
                          />}
              onChange={onChangeAccordion}
              id={accordionId}
            />
          </Paper>
        </Grid>
        <Grid item xs={12}>
          <Paper className={classes.control}>
            <StickyHeadTable 
              tableId={"mainTable"} 
              onSelectRow={onSelectRow} 
              columns={columns} 
              rows={rows} 
              SupportCheckBox={view.table.allowCheckBox} 
              SupportPaging={view.table.allowPage}
              page={page} 
              totalDataCount={totalDataCount}
              onPageChange={onPageChange} 
              resize={tableResize}
            />
          </Paper>
        </Grid>
        {hasUserCreateAuth && <Grid item xs={12}>
            <div className={classes.centerLocate}>
              <Fab color={userContext.preference.color} aria-label="add">
                <AddIcon onClick={() => handleDialogOpen(dictionary.NEW)} />
              </Fab>
            </div>
          </Grid>
        }
        
      </Grid>
      {loading && <CircularProgress size={24} className={classes.buttonProgress} />}
      <CustomizedDialog isDialogOpen={isDialogOpen} sendInfo={sendInfo} viewInfo={viewInfo.person_detail} handleDialogClose={handleDialogClose} callFromDialog={callFromDialog} />
      <Confirm open={isConfirmOpen} MessageType={messageType} MessageId={messageId} handleClose={handleConfirmDialog} />
      <CustomizedSnackbar isOpen={openSnackbar} severity={severity} message={message} closeHandler={onCloseSnackbar} />
     </React.Fragment>
  );
}