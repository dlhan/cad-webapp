import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import { GetText } from '../../common/BundleManager';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import Divider from '@material-ui/core/Divider';
import Dictionary from '../../common/Dictionary';
import PersonBasicInfo from './PersonBasicInfo';
import PersonEducationHistory from './PersonEducationHistory';
import PersonLicencePossessed from './PersonLicencePossessed';
import PersonLearningAchivement from './PersonLearningAchivement';
import PersonFireTrainAchivement from './PersonFireTrainAchivement';

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function PersonDetail(props) {
  const classes = useStyles();
  const [value, setValue] = useState(0);
  const [personId, setPersonId] = useState(props.sendInfo.id);
    var dictionary = new Dictionary();
    
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const callFromBasicInfo = (e) => {
    props.callFromDialog(e);

    if (e.operateType === dictionary.NEW) {
      setPersonId(""); 
    } else if (e.id) {
      setPersonId(e.id); 
    }
  }

  const callFromEducationHistory = (e) => {
    props.callFromDialog(e);
  }

  const callFromLicensePossessed = (e) => {
    props.callFromDialog(e);
  }

  const callFromLearningAchivement = (e) => {
    props.callFromDialog(e);
  }

  const callFromFireTrainAchivement = (e) => {
    props.callFromDialog(e);
  }

  return (
    <div className={classes.root}>
      <Tabs value={value} onChange={handleChange}>
          <Tab label={GetText("basic_information")} {...a11yProps(0)} />
          <Tab label={GetText("education_history")} {...a11yProps(1)} disabled={props.sendInfo.tabDisabled} />
          <Tab label={GetText("possessed_licenses")} {...a11yProps(2)} disabled={props.sendInfo.tabDisabled} />
          <Tab label={GetText("learning_achievement")} {...a11yProps(3)} disabled={props.sendInfo.tabDisabled} />
          <Tab label={GetText("fire_train_achievement")} {...a11yProps(4)} disabled={props.sendInfo.tabDisabled} />
        </Tabs>
      <TabPanel value={value} index={0}>
        <Divider />
        <PersonBasicInfo callFromDialog={callFromBasicInfo} sendInfo={props.sendInfo} personId={personId} handleDialogClose={props.handleDialogClose} />
      </TabPanel>
      <TabPanel value={value} index={1}>
        <Divider />
        <PersonEducationHistory callFromDialog={callFromEducationHistory} sendInfo={props.sendInfo} personId={personId} handleDialogClose={props.handleDialogClose} />
      </TabPanel>
      <TabPanel value={value} index={2}>
        <Divider />
        <PersonLicencePossessed callFromDialog={callFromLicensePossessed} sendInfo={props.sendInfo} personId={personId} handleDialogClose={props.handleDialogClose} />
      </TabPanel>
      <TabPanel value={value} index={3}>
        <Divider />
        <PersonLearningAchivement callFromDialog={callFromLearningAchivement} sendInfo={props.sendInfo} personId={personId} handleDialogClose={props.handleDialogClose} />
      </TabPanel>
      <TabPanel value={value} index={4}>
        <Divider />
        <PersonFireTrainAchivement callFromDialog={callFromFireTrainAchivement} sendInfo={props.sendInfo} personId={personId} handleDialogClose={props.handleDialogClose} />
      </TabPanel>
    </div>
  );
}
