import Dictionary from '../common/Dictionary';
import { GetText } from '../common/BundleManager';

const dictionary = new Dictionary();
class ViewInfo {
    home = {
        "viewName" : dictionary.View_Home,
        "locations" : [], //This is used for breadcrumbs.
        "viewType" : "",
        "viewTitle" : {"normal":GetText("menu_home"), "new" : "","update":""},
        "subTextOnDialogTitle" : "",
        "dialogWidthType" : {"maxWidth": "md"}, //Pick one in "xs","sm","sl","md","lg" and "xl". Getting larger from right side.
        "table" : {
            "allowPage" : true,
            "allowCheckBox" : false
        }
    };
    ward_list = {
        "viewName" : dictionary.View_WardList,
        "locations" : [GetText("menu_fire_service"),GetText("menu_fire_dept_mgr")], //This is used for breadcrumbs.
        "viewType" : dictionary.LIST,
        "viewTitle" : {"normal":GetText("menu_fire_dept_mgr"), "new" : GetText("menu_fire_dept_mgr"),"update":GetText("menu_fire_dept_mgr")},
        "subTextOnDialogTitle" : "",
        "dialogWidthType" : {"maxWidth": "md"}, //Pick one in "xs","sm","sl","md","lg" and "xl". Getting larger from right side.
        "table" : {
            "allowPage" : true,
            "allowCheckBox" : false
        }
    };
    ward_Detail = {
        "viewName" : dictionary.View_WardDetail,
        "locations" : [GetText("menu_fire_service"),GetText("menu_fire_dept_mgr")], //This is used for breadcrumbs.
        "viewType" : dictionary.DETAIL,
        "viewTitle" : {"normal":GetText("menu_vehicle_mgr"), "new" : GetText("menu_vehicle_mgr"),"update":GetText("menu_vehicle_mgr")},
        "subTextOnDialogTitle" : "",
        "dialogWidthType" : {"maxWidth": "md"}, //Pick one in "xs","sm","sl","md","lg" and "xl". Getting larger from right side.
        "table" : {
            "allowPage" : true,
            "allowCheckBox" : false
        }
    };
    building_list = {
        "viewName" : dictionary.View_BuildingList,
        "locations" : [GetText("menu_information_support"),GetText("menu_building_search")], //This is used for breadcrumbs.
        "viewType" : dictionary.LIST,
        "viewTitle" : {"normal":GetText("menu_building_search"), "new" : GetText("menu_building_search"),"update":GetText("menu_building_search")},
        "subTextOnDialogTitle" : "",
        "dialogWidthType" : {"maxWidth": "md"}, //Pick one in "xs","sm","sl","md","lg" and "xl". Getting larger from right side.
        "table" : {
            "allowPage" : true,
            "allowCheckBox" : false
        }
    };
    building_Detail = {
        "viewName" : dictionary.View_BuildingDetail,
        "locations" : [GetText("menu_fire_service"),GetText("menu_fire_dept_mgr")], //This is used for breadcrumbs.
        "viewType" : dictionary.DETAIL,
        "viewTitle" : {"normal":GetText("menu_vehicle_mgr"), "new" : GetText("menu_vehicle_mgr"),"update":GetText("menu_vehicle_mgr")},
        "subTextOnDialogTitle" : "",
        "dialogWidthType" : {"maxWidth": "md"}, //Pick one in "xs","sm","sl","md","lg" and "xl". Getting larger from right side.
        "table" : {
            "allowPage" : true,
            "allowCheckBox" : false
        }
    };
    user_preference = {
        "viewName" : dictionary.View_UserPreference,
        "locations" : "", //This is used for breadcrumbs.
        "viewType" : dictionary.DETAIL,
        "viewTitle" : {"normal":GetText("user_preference"), "new" : GetText("user_preference"),"update":GetText("user_preference")},
        "subTextOnDialogTitle" : "",
        "dialogWidthType" : {"maxWidth": "sm"}, //Pick one in "xs","sm","sl","md","lg" and "xl". Getting larger from right side.
        "table" : {}
    };
    person_list = {
        "viewName" : dictionary.View_PersonList,
        "locations" : [GetText("menu_fire_service"),GetText("menu_hr_mgr")], //This is used for breadcrumbs.
        "viewType" : dictionary.LIST,
        "viewTitle" : {"normal":GetText("menu_hr_mgr"), "new" : GetText("menu_hr_mgr"),"update":GetText("menu_hr_mgr")},
        "subTextOnDialogTitle" : "",
        "dialogWidthType" : {"maxWidth": "md"}, //Pick one in "xs","sm","sl","md","lg" and "xl". Getting larger from right side.
        "table" : {
            "allowPage" : true,
            "allowCheckBox" : false
        }
    };
    person_detail = {
        "viewName" : dictionary.View_PersonDetail,
        "locations" : [GetText("menu_fire_service"),GetText("menu_hr_mgr")], //This is used for breadcrumbs.
        "viewType" : dictionary.DETAIL,
        "viewTitle" : {"normal":GetText("menu_hr_mgr"), "new" : GetText("menu_hr_mgr"),"update":GetText("menu_hr_mgr")},
        "subTextOnDialogTitle" : "",
        "dialogWidthType" : {"maxWidth": "lg"}, //Pick one in "xs","sm","sl","md","lg" and "xl". Getting larger from right side.
        "table" : {}
    };
    person_basic_info = {
        "viewName" : dictionary.View_PersonBasicInfo,
        "locations" : [GetText("menu_fire_service"),GetText("menu_hr_mgr")], //This is used for breadcrumbs.
        "viewType" : dictionary.DETAIL,
        "viewTitle" : {"normal":GetText("fire_worker_basic_info"), "new" : GetText("fire_worker_basic_info"),"update":GetText("fire_worker_basic_info")},
        "subTextOnDialogTitle" : "",
        "dialogWidthType" : {"maxWidth": "lg"}, //Pick one in "xs","sm","sl","md","lg" and "xl". Getting larger from right side.
        "table" : {}
    };
    create_account = {
        "viewName" : dictionary.View_CreateAccount,
        "locations" : [], //This is used for breadcrumbs.
        "viewType" : dictionary.DETAIL,
        "viewTitle" : {"normal":GetText("create_account"), "new" : GetText("create_account"),"update":GetText("create_account")},
        "subTextOnDialogTitle" : "",
        "dialogWidthType" : {"maxWidth": "lg"}, //Pick one in "xs","sm","sl","md","lg" and "xl". Getting larger from right side.
        "table" : {}
    };
    notification_list = {
        "viewName" : dictionary.View_Notification,
        "locations" : [], //This is used for breadcrumbs.
        "viewType" : dictionary.LIST,
        "viewTitle" : {"normal":GetText("notification_list"), "new" : GetText("notification_list"),"update":GetText("notification_list")},
        "subTextOnDialogTitle" : "",
        "dialogWidthType" : {"maxWidth": "lg"}, //Pick one in "xs","sm","sl","md","lg" and "xl". Getting larger from right side.
        "table" : {
            "allowPage" : false,
            "allowCheckBox" : true
        }
    };
    person_education_history = {
        "viewName" : dictionary.View_PersonEducationHistory,
        "locations" : [GetText("menu_fire_service"),GetText("menu_hr_mgr")], //This is used for breadcrumbs.
        "viewType" : dictionary.LIST,
        "viewTitle" : {"normal":GetText("education_history"), "new" : GetText("education_history"),"update":GetText("education_history")},
        "subTextOnDialogTitle" : "",
        "dialogWidthType" : {"maxWidth": "md"}, //Pick one in "xs","sm","sl","md","lg" and "xl". Getting larger from right side.
        "table" : {
            "allowPage" : false,
            "allowCheckBox" : false
        }
    };
    person_education_detail = {
        "viewName" : dictionary.View_PersonEducationDetail,
        "locations" : [GetText("menu_fire_service"),GetText("menu_hr_mgr")], //This is used for breadcrumbs.
        "viewType" : dictionary.DETAIL,
        "viewTitle" : {"normal":GetText("education_history"), "new" : GetText("education_history"),"update":GetText("education_history")},
        "subTextOnDialogTitle" : "",
        "dialogWidthType" : {"maxWidth": "sm"}, //Pick one in "xs","sm","sl","md","lg" and "xl". Getting larger from right side.
        "table" : {}
    };
    person_license_list = {
        "viewName" : dictionary.View_PersonLicenseList,
        "locations" : [GetText("menu_fire_service"),GetText("menu_hr_mgr")], //This is used for breadcrumbs.
        "viewType" : dictionary.LIST,
        "viewTitle" : {"normal":GetText("possessed_licenses"), "new" : GetText("possessed_licenses"),"update":GetText("possessed_licenses")},
        "subTextOnDialogTitle" : "",
        "dialogWidthType" : {"maxWidth": "md"}, //Pick one in "xs","sm","sl","md","lg" and "xl". Getting larger from right side.
        "table" : {
            "allowPage" : false,
            "allowCheckBox" : false
        }
    };
    person_license_detail = {
        "viewName" : dictionary.View_PersonLicenseDetail,
        "locations" : [GetText("menu_fire_service"),GetText("menu_hr_mgr")], //This is used for breadcrumbs.
        "viewType" : dictionary.DETAIL,
        "viewTitle" : {"normal":GetText("possessed_licenses"), "new" : GetText("possessed_licenses"),"update":GetText("possessed_licenses")},
        "subTextOnDialogTitle" : "",
        "dialogWidthType" : {"maxWidth": "md"}, //Pick one in "xs","sm","sl","md","lg" and "xl". Getting larger from right side.
        "table" : {}
    };
    person_learning_list = {
        "viewName" : dictionary.View_PersonLearningList,
        "locations" : [GetText("menu_fire_service"),GetText("menu_hr_mgr")], //This is used for breadcrumbs.
        "viewType" : dictionary.LIST,
        "viewTitle" : {"normal":GetText("learning_achievement"), "new" : GetText("learning_achievement"),"update":GetText("learning_achievement")},
        "subTextOnDialogTitle" : "",
        "dialogWidthType" : {"maxWidth": "md"}, //Pick one in "xs","sm","sl","md","lg" and "xl". Getting larger from right side.
        "table" : {
            "allowPage" : false,
            "allowCheckBox" : false
        }
    };
    person_learning_detail = {
        "viewName" : dictionary.View_PersonLearningDetail,
        "locations" : [GetText("menu_fire_service"),GetText("menu_hr_mgr")], //This is used for breadcrumbs.
        "viewType" : dictionary.DETAIL,
        "viewTitle" : {"normal":GetText("learning_achievement"), "new" : GetText("learning_achievement"),"update":GetText("learning_achievement")},
        "subTextOnDialogTitle" : "",
        "dialogWidthType" : {"maxWidth": "md"}, //Pick one in "xs","sm","sl","md","lg" and "xl". Getting larger from right side.
        "table" : {}
    };
    person_fireTrain_list = {
        "viewName" : dictionary.View_PersonFireTrainList,
        "locations" : [GetText("menu_fire_service"),GetText("menu_hr_mgr")], //This is used for breadcrumbs.
        "viewType" : dictionary.LIST,
        "viewTitle" : {"normal":GetText("fire_train_achievement"), "new" : GetText("fire_train_achievement"),"update":GetText("fire_train_achievement")},
        "subTextOnDialogTitle" : "",
        "dialogWidthType" : {"maxWidth": "md"}, //Pick one in "xs","sm","sl","md","lg" and "xl". Getting larger from right side.
        "table" : {
            "allowPage" : false,
            "allowCheckBox" : false
        }
    };
    person_fireTrain_detail = {
        "viewName" : dictionary.View_PersonFireTrainDetail,
        "locations" : [GetText("menu_fire_service"),GetText("menu_hr_mgr")], //This is used for breadcrumbs.
        "viewType" : dictionary.DETAIL,
        "viewTitle" : {"normal":GetText("fire_train_achievement"), "new" : GetText("fire_train_achievement"),"update":GetText("fire_train_achievement")},
        "subTextOnDialogTitle" : "",
        "dialogWidthType" : {"maxWidth": "md"}, //Pick one in "xs","sm","sl","md","lg" and "xl". Getting larger from right side.
        "table" : {}
    };
    groupCode_tree_view = {
        "viewName" : dictionary.View_GroupCodeTreeView,
        "locations" : [GetText("menu_system_manager"),GetText("group_code_tree")], //This is used for breadcrumbs.
        "viewType" : dictionary.DETAIL,
        "viewTitle" : {"normal":GetText("select_code"), "new" : GetText("select_code"),"update":GetText("select_code")},
        "subTextOnDialogTitle" : GetText("subtext_groupcodetree"),
        "dialogWidthType" : {"maxWidth": "sl"}, //Pick one in "xs","sm","sl","md","lg" and "xl". Getting larger from right side.
        "table" : {}
    };
}

export default ViewInfo;