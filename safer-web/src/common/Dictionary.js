class Dictionary {
    POST = "post";
    GET = "get";
    PUT = "put";
    DELETE = "delete";
    INT = "integer";
    NUMERIC = "numeric";
    STRING = "string";
    BOOLEAN = "bool";
    BOOLEANYN = "bool_yes_no";
    BOOLEANOnOff = "bool_on_off";
    BOOLEANUseOrNot = "bool_use_or_not";
    DATE = "date";
    DATETIME = "datetime";
    TIME = "time";
    LOADING = "loading";
    SUCCESS = "success";
    ERROR = "error";
    RESULT = "result";
    FAIL = "fail";
    MessageType = {
        "Error":"error",
        "Warning":"warning",
        "Info":"info",
        "Success":"success"
    };
    ADD_AUTH = "ADD_AUTH";
    CHANGE_LANG = "CHANGE_LANG";
    CHANGE_THEME = "CHANGE_THEME";
    SET_USER_INFO = "SET_USER_INFO";
    CHANGE_USER_PREFERENCE = "CHANGE_USER_PREFERENCE";
    View_Home = "Home";
    View_WardList = "WardList";
    View_WardDetail = "WardDetail";
    View_WardDetail = "WardDetail";
    View_BuildingList = "BuildingList";
    View_BuildingDetail = "BuildingDetail";
    View_UserPreference = "UserPreference";
    View_PersonList = "PersonList";
    View_PersonDetail = "PersonDetail";
    View_PersonBasicInfo = "PersonBasicInfo";
    View_PersonEducationHistory = "PersonEducationHistory";
    View_PersonEducationDetail = "PersonEducationDetail";
    View_PersonLicenseList = "PersonLicenseList";
    View_PersonLicenseDetail = "PersonLicenseDetail";
    View_CreateAccount = "CreateAccount";
    View_Notification = "Notification";
    View_PersonLearningList = "PersonLearningList";
    View_PersonLearningDetail = "PersonLearningDetail";
    View_PersonFireTrainList = "PersonFireTrainList";
    View_PersonFireTrainDetail = "PersonFireTrainDetail";
    View_GroupCodeTreeView = "GroupCodeTreeView";
    LIST = "list";
    DETAIL = "detail";
    WARNING = "warning";
    ERROR = "error";
    INFORMATION = "information";
    NEW = "new";
    UPDATE = "update";
    READ = "read";
    ID = "id";
    ADMIN = "admin";
}

export default Dictionary;