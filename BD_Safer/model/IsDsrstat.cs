﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BD_Safer.model
{
    public partial class IsDsrstat
    {
        public string WorkDtime { get; set; }
        public int SituSeq { get; set; }
        public string MakeUserId { get; set; }
        public string ConfUserId { get; set; }
        public string JurisWardId { get; set; }
        public string WorksituDesc { get; set; }
        public int? WorksituCd { get; set; }
        public string LchgDtime { get; set; }
        public string LchgUserId { get; set; }
        public string CreatedDate { get; set; }

        public virtual HrUser ConfUser { get; set; }
        public virtual IsWard JurisWard { get; set; }
        public virtual HrUser MakeUser { get; set; }
    }
}
