﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BD_Safer.model
{
    public partial class HrUserEdu
    {
        public int UserEducationSeq { get; set; }
        public string UserId { get; set; }
        public DateTime? EntranceDate { get; set; }
        public DateTime? GraducationDate { get; set; }
        public string AcademyName { get; set; }
        public string Major { get; set; }
        public int? DegreeCd { get; set; }
        public int? AcademyTypeCd { get; set; }

        public virtual HrUser User { get; set; }
    }
}
