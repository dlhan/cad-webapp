﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BD_Safer.model
{
    public partial class SmCdMaster
    {
        public int Cd { get; set; }
        public string CdDesc { get; set; }
        public string UseYn { get; set; }
    }
}
