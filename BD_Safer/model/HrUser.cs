﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BD_Safer.model
{
    public partial class HrUser
    {
        public HrUser()
        {
            HrEduPromotions = new HashSet<HrEduPromotion>();
            HrPersonalEvalEvaluatorUsers = new HashSet<HrPersonalEval>();
            HrPersonalEvalUsers = new HashSet<HrPersonalEval>();
            HrUserEdus = new HashSet<HrUserEdu>();
            HrUserFireTrains = new HashSet<HrUserFireTrain>();
            HrUserLicenses = new HashSet<HrUserLicense>();
            IsDsrstatConfUsers = new HashSet<IsDsrstat>();
            IsDsrstatMakeUsers = new HashSet<IsDsrstat>();
            SmNotifications = new HashSet<SmNotification>();
            SmUserloghists = new HashSet<SmUserloghist>();
            SmUserprivs = new HashSet<SmUserpriv>();
        }

        public string UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CitizenNo { get; set; }
        public string WardId { get; set; }
        public int? ClassCd { get; set; }
        public int? TitleCd { get; set; }
        public int? WorkCd { get; set; }
        public int? WorkSectionCd { get; set; }
        public string UserPw { get; set; }
        public DateTime? PwChgDate { get; set; }
        public string RegDate { get; set; }
        public string UseYn { get; set; }
        public string SysmgrYn { get; set; }
        public string ZipCode { get; set; }
        public string Address { get; set; }
        public string HomeTel { get; set; }
        public string OfficeTel { get; set; }
        public string Cell { get; set; }
        public string FaxNo { get; set; }
        public string ExtTel { get; set; }
        public string Email { get; set; }
        public DateTime? BirthDate { get; set; }
        public string Gender { get; set; }
        public string LchgDtime { get; set; }
        public string LchgUserId { get; set; }
        public int UserSeq { get; set; }

        public virtual IsWard Ward { get; set; }
        public virtual ICollection<HrEduPromotion> HrEduPromotions { get; set; }
        public virtual ICollection<HrPersonalEval> HrPersonalEvalEvaluatorUsers { get; set; }
        public virtual ICollection<HrPersonalEval> HrPersonalEvalUsers { get; set; }
        public virtual ICollection<HrUserEdu> HrUserEdus { get; set; }
        public virtual ICollection<HrUserFireTrain> HrUserFireTrains { get; set; }
        public virtual ICollection<HrUserLicense> HrUserLicenses { get; set; }
        public virtual ICollection<IsDsrstat> IsDsrstatConfUsers { get; set; }
        public virtual ICollection<IsDsrstat> IsDsrstatMakeUsers { get; set; }
        public virtual ICollection<SmNotification> SmNotifications { get; set; }
        public virtual ICollection<SmUserloghist> SmUserloghists { get; set; }
        public virtual ICollection<SmUserpriv> SmUserprivs { get; set; }
    }
}
