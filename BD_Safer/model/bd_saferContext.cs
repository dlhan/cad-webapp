﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace BD_Safer.model
{
    public partial class bd_saferContext : DbContext
    {
        public bd_saferContext()
        {
        }

        public bd_saferContext(DbContextOptions<bd_saferContext> options)
            : base(options)
        {
        }

        public virtual DbSet<CmDistrict> CmDistricts { get; set; }
        public virtual DbSet<CmDivision> CmDivisions { get; set; }
        public virtual DbSet<CmIndex> CmIndices { get; set; }
        public virtual DbSet<Code> Codes { get; set; }
        public virtual DbSet<CtDsprule> CtDsprules { get; set; }
        public virtual DbSet<HrEduPromotion> HrEduPromotions { get; set; }
        public virtual DbSet<HrPersonalEval> HrPersonalEvals { get; set; }
        public virtual DbSet<HrUser> HrUsers { get; set; }
        public virtual DbSet<HrUserEdu> HrUserEdus { get; set; }
        public virtual DbSet<HrUserFireTrain> HrUserFireTrains { get; set; }
        public virtual DbSet<HrUserLicense> HrUserLicenses { get; set; }
        public virtual DbSet<IsBldg> IsBldgs { get; set; }
        public virtual DbSet<IsDgrmgr> IsDgrmgrs { get; set; }
        public virtual DbSet<IsDsrstat> IsDsrstats { get; set; }
        public virtual DbSet<IsFmgr> IsFmgrs { get; set; }
        public virtual DbSet<IsHosp> IsHosps { get; set; }
        public virtual DbSet<IsHyd> IsHyds { get; set; }
        public virtual DbSet<IsJurisward> IsJuriswards { get; set; }
        public virtual DbSet<IsObj> IsObjs { get; set; }
        public virtual DbSet<IsObjdrw> IsObjdrws { get; set; }
        public virtual DbSet<IsObjfireequip> IsObjfireequips { get; set; }
        public virtual DbSet<IsObjhist> IsObjhists { get; set; }
        public virtual DbSet<IsObjpic> IsObjpics { get; set; }
        public virtual DbSet<IsObjplan> IsObjplans { get; set; }
        public virtual DbSet<IsObjrel> IsObjrels { get; set; }
        public virtual DbSet<IsOrg> IsOrgs { get; set; }
        public virtual DbSet<IsOrgfire> IsOrgfires { get; set; }
        public virtual DbSet<IsOrgman> IsOrgmen { get; set; }
        public virtual DbSet<IsStory> IsStories { get; set; }
        public virtual DbSet<IsStorySpecUsed> IsStorySpecUseds { get; set; }
        public virtual DbSet<IsTypeorg> IsTypeorgs { get; set; }
        public virtual DbSet<IsVolun> IsVoluns { get; set; }
        public virtual DbSet<IsVolunDsp> IsVolunDsps { get; set; }
        public virtual DbSet<IsVolunEdu> IsVolunEdus { get; set; }
        public virtual DbSet<IsWard> IsWards { get; set; }
        public virtual DbSet<IsWardcar> IsWardcars { get; set; }
        public virtual DbSet<IsWardcarhist> IsWardcarhists { get; set; }
        public virtual DbSet<SmCdMaster> SmCdMasters { get; set; }
        public virtual DbSet<SmCdResource> SmCdResources { get; set; }
        public virtual DbSet<SmCdgroupMaster> SmCdgroupMasters { get; set; }
        public virtual DbSet<SmCdgroupResource> SmCdgroupResources { get; set; }
        public virtual DbSet<SmCdgroupcd> SmCdgroupcds { get; set; }
        public virtual DbSet<SmMenu> SmMenus { get; set; }
        public virtual DbSet<SmMenupriv> SmMenuprivs { get; set; }
        public virtual DbSet<SmNotification> SmNotifications { get; set; }
        public virtual DbSet<SmPriv> SmPrivs { get; set; }
        public virtual DbSet<SmSysenv> SmSysenvs { get; set; }
        public virtual DbSet<SmUserPreference> SmUserPreferences { get; set; }
        public virtual DbSet<SmUserloghist> SmUserloghists { get; set; }
        public virtual DbSet<SmUserpriv> SmUserprivs { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<CmDistrict>(entity =>
            {
                entity.HasKey(e => e.DistrictId)
                    .HasName("PRIMARY");

                entity.ToTable("cm_districts");

                entity.HasIndex(e => e.DivisionCode, "cm_districts_FK");

                entity.Property(e => e.DistrictId)
                    .HasColumnType("int(11)")
                    .HasColumnName("district_id");

                entity.Property(e => e.DistrictName)
                    .IsRequired()
                    .HasMaxLength(30)
                    .HasColumnName("district_name");

                entity.Property(e => e.DivisionCode)
                    .HasMaxLength(1)
                    .HasColumnName("division_code")
                    .HasDefaultValueSql("'NULL'")
                    .IsFixedLength(true);

                entity.HasOne(d => d.DivisionCodeNavigation)
                    .WithMany(p => p.CmDistricts)
                    .HasForeignKey(d => d.DivisionCode)
                    .HasConstraintName("cm_districts_FK");
            });

            modelBuilder.Entity<CmDivision>(entity =>
            {
                entity.HasKey(e => e.Code)
                    .HasName("PRIMARY");

                entity.ToTable("cm_divisions");

                entity.Property(e => e.Code)
                    .HasMaxLength(1)
                    .HasColumnName("code")
                    .IsFixedLength(true);

                entity.Property(e => e.DivisionName)
                    .HasMaxLength(20)
                    .HasColumnName("division_name")
                    .HasDefaultValueSql("'NULL'");
            });

            modelBuilder.Entity<CmIndex>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("cm_index");

                entity.Property(e => e.Id)
                    .HasColumnType("int(11)")
                    .HasColumnName("id");

                entity.Property(e => e.OrganName)
                    .HasMaxLength(100)
                    .HasColumnName("organ_name")
                    .HasDefaultValueSql("'NULL'");
            });

            modelBuilder.Entity<Code>(entity =>
            {
                entity.HasNoKey();

                entity.ToTable("code");

                entity.Property(e => e.Cd)
                    .HasColumnType("int(11)")
                    .HasColumnName("cd")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Code1)
                    .HasColumnType("int(11)")
                    .HasColumnName("code")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.En)
                    .HasMaxLength(100)
                    .HasColumnName("en")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.Kr)
                    .HasMaxLength(100)
                    .HasColumnName("kr")
                    .HasDefaultValueSql("'NULL'");
            });

            modelBuilder.Entity<CtDsprule>(entity =>
            {
                entity.HasKey(e => new { e.DsrKndCd, e.DsrClsCd, e.DsrSizeCd, e.CarCdGrp })
                    .HasName("PRIMARY");

                entity.ToTable("ct_dsprule");

                entity.HasComment("유형별출동대편성지침");

                entity.Property(e => e.DsrKndCd)
                    .HasColumnType("int(11)")
                    .HasColumnName("dsr_knd_cd")
                    .HasComment("긴급구조종별코드");

                entity.Property(e => e.DsrClsCd)
                    .HasColumnType("int(11)")
                    .HasColumnName("dsr_cls_cd")
                    .HasComment("긴급구조분류코드");

                entity.Property(e => e.DsrSizeCd)
                    .HasColumnType("int(11)")
                    .HasColumnName("dsr_size_cd")
                    .HasComment("긴급구조규모코드");

                entity.Property(e => e.CarCdGrp)
                    .HasColumnType("int(11)")
                    .HasColumnName("car_cd_grp")
                    .HasComment("차종코드그룹");

                entity.Property(e => e.CarCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("car_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("차량수");

                entity.Property(e => e.LchgDtime)
                    .HasMaxLength(20)
                    .HasColumnName("lchg_dtime")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("수정일시");

                entity.Property(e => e.LchgUserId)
                    .HasMaxLength(20)
                    .HasColumnName("lchg_user_id")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("수정자ID");
            });

            modelBuilder.Entity<HrEduPromotion>(entity =>
            {
                entity.HasKey(e => new { e.EducationPromotionSeq, e.UserId })
                    .HasName("PRIMARY");

                entity.ToTable("hr_edu_promotion");

                entity.HasIndex(e => e.UserId, "FK_hr_edu_promotion_user_id");

                entity.Property(e => e.EducationPromotionSeq)
                    .HasColumnType("int(11)")
                    .ValueGeneratedOnAdd()
                    .HasColumnName("education_promotion_seq")
                    .HasComment("일련번호");

                entity.Property(e => e.UserId)
                    .HasMaxLength(20)
                    .HasColumnName("user_id")
                    .HasComment("사용자 id");

                entity.Property(e => e.CompleteCd)
                    .HasColumnType("int(11)")
                    .HasColumnName("complete_cd")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("교육이수코드");

                entity.Property(e => e.CourseName)
                    .HasMaxLength(100)
                    .HasColumnName("course_name")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("과정명");

                entity.Property(e => e.EducationClsCd)
                    .HasColumnType("int(11)")
                    .HasColumnName("education_cls_cd")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("교육구분코드");

                entity.Property(e => e.EndDate)
                    .HasColumnType("date")
                    .HasColumnName("end_date")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("종료일");

                entity.Property(e => e.InstitutionName)
                    .HasMaxLength(100)
                    .HasColumnName("institution_name")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("기관명");

                entity.Property(e => e.Score)
                    .HasMaxLength(10)
                    .HasColumnName("score")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("성적");

                entity.Property(e => e.StartDate)
                    .HasColumnType("date")
                    .HasColumnName("start_date")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("시작일");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.HrEduPromotions)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_hr_edu_promotion_user_id");
            });

            modelBuilder.Entity<HrPersonalEval>(entity =>
            {
                entity.HasKey(e => new { e.EvalSeq, e.EvalYear, e.UserId })
                    .HasName("PRIMARY");

                entity.ToTable("hr_personal_eval");

                entity.HasIndex(e => e.EvaluatorUserId, "FK_hr_personal_eval_evaluator_user_id");

                entity.HasIndex(e => e.UserId, "FK_hr_personal_eval_user_id");

                entity.Property(e => e.EvalSeq)
                    .HasColumnType("int(11)")
                    .ValueGeneratedOnAdd()
                    .HasColumnName("eval_seq")
                    .HasComment("일련번호");

                entity.Property(e => e.EvalYear)
                    .HasColumnType("int(4)")
                    .HasColumnName("eval_year")
                    .HasComment("평가년도");

                entity.Property(e => e.UserId)
                    .HasMaxLength(20)
                    .HasColumnName("user_id")
                    .HasComment("사용자 id");

                entity.Property(e => e.DiligenceScore)
                    .HasMaxLength(10)
                    .HasColumnName("diligence_score")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("성실성 점수");

                entity.Property(e => e.EvalDate)
                    .HasMaxLength(20)
                    .HasColumnName("eval_date")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("평가일시");

                entity.Property(e => e.EvaluatorUserId)
                    .IsRequired()
                    .HasMaxLength(20)
                    .HasColumnName("evaluator_user_id")
                    .HasComment("평가자 id");

                entity.Property(e => e.PerfAbilityScore)
                    .HasMaxLength(10)
                    .HasColumnName("perf_ability_score")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("직무수행능력 점수");

                entity.Property(e => e.RespScore)
                    .HasMaxLength(10)
                    .HasColumnName("resp_score")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("책임감 점수");

                entity.HasOne(d => d.EvaluatorUser)
                    .WithMany(p => p.HrPersonalEvalEvaluatorUsers)
                    .HasForeignKey(d => d.EvaluatorUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_hr_personal_eval_evaluator_user_id");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.HrPersonalEvalUsers)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_hr_personal_eval_user_id");
            });

            modelBuilder.Entity<HrUser>(entity =>
            {
                entity.HasKey(e => e.UserId)
                    .HasName("PRIMARY");

                entity.ToTable("hr_user");

                entity.HasIndex(e => e.WardId, "FK_hr_user_ward_id");

                entity.Property(e => e.UserId)
                    .HasMaxLength(20)
                    .HasColumnName("user_id")
                    .HasComment("사용자ID");

                entity.Property(e => e.Address)
                    .HasMaxLength(100)
                    .HasColumnName("address")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("주소");

                entity.Property(e => e.BirthDate)
                    .HasColumnType("date")
                    .HasColumnName("birth_date")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("생년월일");

                entity.Property(e => e.Cell)
                    .HasMaxLength(20)
                    .HasColumnName("cell")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("이동전화번호");

                entity.Property(e => e.CitizenNo)
                    .HasMaxLength(20)
                    .HasColumnName("citizen_no")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("주민번호");

                entity.Property(e => e.ClassCd)
                    .HasColumnType("int(11)")
                    .HasColumnName("class_cd")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("계급코드");

                entity.Property(e => e.Email)
                    .HasMaxLength(50)
                    .HasColumnName("email")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("이메일");

                entity.Property(e => e.ExtTel)
                    .HasMaxLength(10)
                    .HasColumnName("ext_tel")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("내선번호");

                entity.Property(e => e.FaxNo)
                    .HasMaxLength(20)
                    .HasColumnName("fax_no")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("팩스번호");

                entity.Property(e => e.FirstName)
                    .HasMaxLength(50)
                    .HasColumnName("first_name")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("First_name");

                entity.Property(e => e.Gender)
                    .HasMaxLength(1)
                    .HasColumnName("gender")
                    .HasDefaultValueSql("'NULL'")
                    .IsFixedLength(true)
                    .HasComment("성별");

                entity.Property(e => e.HomeTel)
                    .HasMaxLength(20)
                    .HasColumnName("home_tel")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("집전화번호");

                entity.Property(e => e.LastName)
                    .HasMaxLength(50)
                    .HasColumnName("last_name")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("Last name");

                entity.Property(e => e.LchgDtime)
                    .HasMaxLength(20)
                    .HasColumnName("lchg_dtime")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("수정일시");

                entity.Property(e => e.LchgUserId)
                    .HasMaxLength(20)
                    .HasColumnName("lchg_user_id")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("수정자ID");

                entity.Property(e => e.OfficeTel)
                    .HasMaxLength(20)
                    .HasColumnName("office_tel")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("회사전화번호");

                entity.Property(e => e.PwChgDate)
                    .HasColumnType("date")
                    .HasColumnName("pw_chg_date")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("비밀번호변경일자");

                entity.Property(e => e.RegDate)
                    .HasMaxLength(20)
                    .HasColumnName("reg_date")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("등록일자");

                entity.Property(e => e.SysmgrYn)
                    .HasMaxLength(1)
                    .HasColumnName("sysmgr_yn")
                    .HasDefaultValueSql("'NULL'")
                    .IsFixedLength(true)
                    .HasComment("시스템관리자여부");

                entity.Property(e => e.TitleCd)
                    .HasColumnType("int(11)")
                    .HasColumnName("title_cd")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("직위코드");

                entity.Property(e => e.UseYn)
                    .HasMaxLength(1)
                    .HasColumnName("use_yn")
                    .HasDefaultValueSql("'''1'''")
                    .IsFixedLength(true)
                    .HasComment("사용여부");

                entity.Property(e => e.UserPw)
                    .HasMaxLength(40)
                    .HasColumnName("user_pw")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("비밀번호");

                entity.Property(e => e.UserSeq)
                    .HasColumnType("int(11)")
                    .HasColumnName("user_seq")
                    .HasComment("사용자 일렬번호");

                entity.Property(e => e.WardId)
                    .HasMaxLength(9)
                    .HasColumnName("ward_id")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("서·센터ID");

                entity.Property(e => e.WorkCd)
                    .HasColumnType("int(11)")
                    .HasColumnName("work_cd")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("직류코드");

                entity.Property(e => e.WorkSectionCd)
                    .HasColumnType("int(11)")
                    .HasColumnName("work_section_cd")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("근무구분코드");

                entity.Property(e => e.ZipCode)
                    .HasMaxLength(10)
                    .HasColumnName("zip_code")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("우편번호");

                entity.HasOne(d => d.Ward)
                    .WithMany(p => p.HrUsers)
                    .HasForeignKey(d => d.WardId)
                    .HasConstraintName("FK_hr_user_ward_id");
            });

            modelBuilder.Entity<HrUserEdu>(entity =>
            {
                entity.HasKey(e => new { e.UserEducationSeq, e.UserId })
                    .HasName("PRIMARY");

                entity.ToTable("hr_user_edu");

                entity.HasIndex(e => e.UserId, "FK_hr_user_edu_user_id");

                entity.Property(e => e.UserEducationSeq)
                    .HasColumnType("int(11)")
                    .ValueGeneratedOnAdd()
                    .HasColumnName("user_education_seq")
                    .HasComment("일련번호");

                entity.Property(e => e.UserId)
                    .HasMaxLength(20)
                    .HasColumnName("user_id")
                    .HasComment("사용자 id");

                entity.Property(e => e.AcademyName)
                    .HasMaxLength(50)
                    .HasColumnName("academy_name")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("학교명");

                entity.Property(e => e.AcademyTypeCd)
                    .HasColumnType("int(11)")
                    .HasColumnName("academy_type_cd")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("학교구분코드");

                entity.Property(e => e.DegreeCd)
                    .HasColumnType("int(11)")
                    .HasColumnName("degree_cd")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("학위");

                entity.Property(e => e.EntranceDate)
                    .HasColumnType("date")
                    .HasColumnName("entrance_date")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("입학일");

                entity.Property(e => e.GraducationDate)
                    .HasColumnType("date")
                    .HasColumnName("graducation_date")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("졸업일");

                entity.Property(e => e.Major)
                    .HasMaxLength(50)
                    .HasColumnName("major")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("전공");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.HrUserEdus)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_hr_user_edu_user_id");
            });

            modelBuilder.Entity<HrUserFireTrain>(entity =>
            {
                entity.HasKey(e => new { e.FireTrainSeq, e.UserId })
                    .HasName("PRIMARY");

                entity.ToTable("hr_user_fire_train");

                entity.HasIndex(e => e.UserId, "FK_hr_user_fire_train_user_id");

                entity.Property(e => e.FireTrainSeq)
                    .HasColumnType("int(11)")
                    .ValueGeneratedOnAdd()
                    .HasColumnName("fire_train_seq")
                    .HasComment("일련번호");

                entity.Property(e => e.UserId)
                    .HasMaxLength(20)
                    .HasColumnName("user_id")
                    .HasComment("사용자 id");

                entity.Property(e => e.EndDate)
                    .HasColumnType("date")
                    .HasColumnName("end_date")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("종료일");

                entity.Property(e => e.Host)
                    .HasMaxLength(50)
                    .HasColumnName("host")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("주최");

                entity.Property(e => e.OrgName)
                    .HasMaxLength(50)
                    .HasColumnName("org_name")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("기관명");

                entity.Property(e => e.Perf)
                    .HasMaxLength(100)
                    .HasColumnName("perf")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("실적");

                entity.Property(e => e.StartDate)
                    .HasColumnType("date")
                    .HasColumnName("start_date")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("시작일");

                entity.Property(e => e.TrainName)
                    .HasMaxLength(100)
                    .HasColumnName("train_name")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("소방훈련명");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.HrUserFireTrains)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_hr_user_fire_train_user_id");
            });

            modelBuilder.Entity<HrUserLicense>(entity =>
            {
                entity.HasKey(e => new { e.LicenseSeq, e.UserId })
                    .HasName("PRIMARY");

                entity.ToTable("hr_user_license");

                entity.HasIndex(e => e.UserId, "FK_hr_user_license_user_id");

                entity.Property(e => e.LicenseSeq)
                    .HasColumnType("int(11)")
                    .ValueGeneratedOnAdd()
                    .HasColumnName("license_seq")
                    .HasComment("일련번호");

                entity.Property(e => e.UserId)
                    .HasMaxLength(20)
                    .HasColumnName("user_id")
                    .HasComment("사용자 id");

                entity.Property(e => e.AcqDate)
                    .HasColumnType("date")
                    .HasColumnName("acq_date")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("취득일");

                entity.Property(e => e.LicenseName)
                    .HasMaxLength(50)
                    .HasColumnName("license_name")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("자격증명");

                entity.Property(e => e.LicenseNum)
                    .HasMaxLength(50)
                    .HasColumnName("license_num")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("자격증번호");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.HrUserLicenses)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_hr_user_license_user_id");
            });

            modelBuilder.Entity<IsBldg>(entity =>
            {
                entity.HasKey(e => new { e.BldgSeq, e.ObjId })
                    .HasName("PRIMARY");

                entity.ToTable("is_bldg");

                entity.HasComment("대상물동별내역");

                entity.HasIndex(e => e.ObjId, "FK_is_bldg_obj_id");

                entity.Property(e => e.BldgSeq)
                    .HasColumnType("int(11)")
                    .ValueGeneratedOnAdd()
                    .HasColumnName("bldg_seq")
                    .HasComment("동일련번호");

                entity.Property(e => e.ObjId)
                    .HasColumnType("int(20)")
                    .HasColumnName("obj_id")
                    .HasComment("대상물ID");

                entity.Property(e => e.BldgHeight)
                    .HasColumnType("decimal(7,2)")
                    .HasColumnName("bldg_height")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("건물동높이");

                entity.Property(e => e.BldgMainUseCd)
                    .HasColumnType("int(11)")
                    .HasColumnName("bldg_main_use_cd")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("건물동주용도코드");

                entity.Property(e => e.BldgName)
                    .HasMaxLength(50)
                    .HasColumnName("bldg_name")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("동명");

                entity.Property(e => e.BldgSubUseCd)
                    .HasColumnType("int(11)")
                    .HasColumnName("bldg_sub_use_cd")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("건물동부용도코드");

                entity.Property(e => e.BstoryCnt)
                    .HasColumnType("int(3)")
                    .HasColumnName("bstory_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("지하층수");

                entity.Property(e => e.ComStairCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("com_stair_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("일반계단수");

                entity.Property(e => e.ElvtrCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("elvtr_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("승강기수");

                entity.Property(e => e.EmgliftCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("emglift_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("비상승강기수");

                entity.Property(e => e.EntrpsCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("entrps_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("업체수");

                entity.Property(e => e.EscStairCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("esc_stair_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("피난계단수");

                entity.Property(e => e.EscalCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("escal_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("에스카레이터수");

                entity.Property(e => e.ExitCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("exit_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("비상구수");

                entity.Property(e => e.FloorArea)
                    .HasColumnType("decimal(10,2)")
                    .HasColumnName("floor_area")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("바닥면적");

                entity.Property(e => e.HomeCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("home_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("세대수");

                entity.Property(e => e.HouseCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("house_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("가구수");

                entity.Property(e => e.InclineCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("incline_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("경사로수");

                entity.Property(e => e.InhbtntCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("inhbtnt_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("주민수");

                entity.Property(e => e.LotArea)
                    .HasColumnType("decimal(10,2)")
                    .HasColumnName("lot_area")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("부지면적");

                entity.Property(e => e.OutStairCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("out_stair_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("옥외계단수");

                entity.Property(e => e.RemoveDate)
                    .HasColumnType("date")
                    .HasColumnName("remove_date")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("철거날짜");

                entity.Property(e => e.RemoveYn)
                    .HasMaxLength(1)
                    .HasColumnName("remove_yn")
                    .HasDefaultValueSql("'NULL'")
                    .IsFixedLength(true)
                    .HasComment("철거여부");

                entity.Property(e => e.RootYn)
                    .HasMaxLength(1)
                    .HasColumnName("root_yn")
                    .HasDefaultValueSql("'''0'''")
                    .IsFixedLength(true)
                    .HasComment("옥상유무");

                entity.Property(e => e.RpsnBldgYn)
                    .HasMaxLength(1)
                    .HasColumnName("rpsn_bldg_yn")
                    .HasDefaultValueSql("'''1'''")
                    .IsFixedLength(true)
                    .HasComment("대표건물동여부");

                entity.Property(e => e.SescStairCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("sesc_stair_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("특별피난계단수");

                entity.Property(e => e.Struct1)
                    .HasColumnType("int(11)")
                    .HasColumnName("struct_1")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("건물구조식코드");

                entity.Property(e => e.Struct2)
                    .HasColumnType("int(11)")
                    .HasColumnName("struct_2")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("건물구조조코드");

                entity.Property(e => e.Struct3)
                    .HasColumnType("int(11)")
                    .HasColumnName("struct_3")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("건물구조즙코드");

                entity.Property(e => e.TotArea)
                    .HasColumnType("decimal(10,2)")
                    .HasColumnName("tot_area")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("연면적");

                entity.Property(e => e.UseConfmDate)
                    .HasColumnType("date")
                    .HasColumnName("use_confm_date")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("사용승인날짜");

                entity.Property(e => e.UseYn)
                    .HasMaxLength(1)
                    .HasColumnName("use_yn")
                    .HasDefaultValueSql("'''1'''")
                    .IsFixedLength(true)
                    .HasComment("사용여부");

                entity.Property(e => e.UstoryCnt)
                    .HasColumnType("int(3)")
                    .HasColumnName("ustory_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("지상층수");

                entity.HasOne(d => d.Obj)
                    .WithMany(p => p.IsBldgs)
                    .HasForeignKey(d => d.ObjId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_is_bldg_obj_id");
            });

            modelBuilder.Entity<IsDgrmgr>(entity =>
            {
                entity.HasKey(e => new { e.DgrmgrSeq, e.ObjId })
                    .HasName("PRIMARY");

                entity.ToTable("is_dgrmgr");

                entity.HasComment("안전관리자");

                entity.HasIndex(e => e.ObjId, "FK_is_dgrmgr_obj_id");

                entity.Property(e => e.DgrmgrSeq)
                    .HasColumnType("int(11)")
                    .ValueGeneratedOnAdd()
                    .HasColumnName("dgrmgr_seq")
                    .HasComment("안전관리자일련번호");

                entity.Property(e => e.ObjId)
                    .HasColumnType("int(20)")
                    .HasColumnName("obj_id")
                    .HasComment("대상물ID");

                entity.Property(e => e.Address)
                    .HasMaxLength(100)
                    .HasColumnName("address")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("주소");

                entity.Property(e => e.FirstName)
                    .HasMaxLength(50)
                    .HasColumnName("first_name")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("안전관리자명");

                entity.Property(e => e.LastName)
                    .HasMaxLength(50)
                    .HasColumnName("last_name")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.TelNum)
                    .HasMaxLength(20)
                    .HasColumnName("tel_num")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("전화번호");

                entity.Property(e => e.UseYn)
                    .HasMaxLength(1)
                    .HasColumnName("use_yn")
                    .HasDefaultValueSql("'''1'''")
                    .IsFixedLength(true)
                    .HasComment("사용여부");

                entity.Property(e => e.ZipCode)
                    .HasMaxLength(10)
                    .HasColumnName("zip_code")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("우편번호");

                entity.HasOne(d => d.Obj)
                    .WithMany(p => p.IsDgrmgrs)
                    .HasForeignKey(d => d.ObjId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_is_dgrmgr_obj_id");
            });

            modelBuilder.Entity<IsDsrstat>(entity =>
            {
                entity.HasKey(e => new { e.WorkDtime, e.SituSeq, e.MakeUserId })
                    .HasName("PRIMARY");

                entity.ToTable("is_dsrstat");

                entity.HasComment("근무상황");

                entity.HasIndex(e => e.ConfUserId, "FK_is_dsrstat_conf_user_id");

                entity.HasIndex(e => e.JurisWardId, "FK_is_dsrstat_juris_ward_id");

                entity.HasIndex(e => e.MakeUserId, "FK_is_dsrstat_make_user_id");

                entity.Property(e => e.WorkDtime)
                    .HasMaxLength(20)
                    .HasColumnName("work_dtime")
                    .HasComment("근무일시");

                entity.Property(e => e.SituSeq)
                    .HasColumnType("int(11)")
                    .HasColumnName("situ_seq")
                    .HasComment("상황일련번호");

                entity.Property(e => e.MakeUserId)
                    .HasMaxLength(20)
                    .HasColumnName("make_user_id")
                    .HasComment("작성자ID");

                entity.Property(e => e.ConfUserId)
                    .HasMaxLength(20)
                    .HasColumnName("conf_user_id")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("확인자ID");

                entity.Property(e => e.CreatedDate)
                    .HasMaxLength(20)
                    .HasColumnName("created_date")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("등록일자");

                entity.Property(e => e.JurisWardId)
                    .HasMaxLength(9)
                    .HasColumnName("juris_ward_id")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("관할서소ID");

                entity.Property(e => e.LchgDtime)
                    .HasMaxLength(20)
                    .HasColumnName("lchg_dtime")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("수정일시");

                entity.Property(e => e.LchgUserId)
                    .HasMaxLength(20)
                    .HasColumnName("lchg_user_id")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("수정자ID");

                entity.Property(e => e.WorksituCd)
                    .HasColumnType("int(11)")
                    .HasColumnName("worksitu_cd")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("근무종별코드");

                entity.Property(e => e.WorksituDesc)
                    .HasMaxLength(200)
                    .HasColumnName("worksitu_desc")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("근무상황내용");

                entity.HasOne(d => d.ConfUser)
                    .WithMany(p => p.IsDsrstatConfUsers)
                    .HasForeignKey(d => d.ConfUserId)
                    .HasConstraintName("FK_is_dsrstat_conf_user_id");

                entity.HasOne(d => d.JurisWard)
                    .WithMany(p => p.IsDsrstats)
                    .HasForeignKey(d => d.JurisWardId)
                    .HasConstraintName("FK_is_dsrstat_juris_ward_id");

                entity.HasOne(d => d.MakeUser)
                    .WithMany(p => p.IsDsrstatMakeUsers)
                    .HasForeignKey(d => d.MakeUserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_is_dsrstat_make_user_id");
            });

            modelBuilder.Entity<IsFmgr>(entity =>
            {
                entity.HasKey(e => new { e.FmgrSeq, e.ObjId })
                    .HasName("PRIMARY");

                entity.ToTable("is_fmgr");

                entity.HasComment("방화관리자");

                entity.HasIndex(e => e.ObjId, "FK_is_fmgr_obj_id");

                entity.Property(e => e.FmgrSeq)
                    .HasColumnType("int(11)")
                    .ValueGeneratedOnAdd()
                    .HasColumnName("fmgr_seq")
                    .HasComment("방화관리자일련번호");

                entity.Property(e => e.ObjId)
                    .HasColumnType("int(20)")
                    .HasColumnName("obj_id")
                    .HasComment("대상물ID");

                entity.Property(e => e.Address)
                    .HasMaxLength(100)
                    .HasColumnName("address")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("주소");

                entity.Property(e => e.AssignDate)
                    .HasColumnType("date")
                    .HasColumnName("assign_date")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("선임일자");

                entity.Property(e => e.Birthday)
                    .HasColumnType("date")
                    .HasColumnName("birthday")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("방화관리자생년월일");

                entity.Property(e => e.CitizenNo)
                    .HasMaxLength(20)
                    .HasColumnName("citizen_no")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("주민번호");

                entity.Property(e => e.DismissDate)
                    .HasColumnType("date")
                    .HasColumnName("dismiss_date")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("해임일자");

                entity.Property(e => e.DismissRemark)
                    .HasMaxLength(200)
                    .HasColumnName("dismiss_remark")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("해임사유");

                entity.Property(e => e.FirstName)
                    .HasMaxLength(50)
                    .HasColumnName("first_name")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("방화관리자명");

                entity.Property(e => e.LastName)
                    .HasMaxLength(50)
                    .HasColumnName("last_name")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.MgeAgencyYn)
                    .HasMaxLength(1)
                    .HasColumnName("mge_agency_yn")
                    .HasDefaultValueSql("'NULL'")
                    .IsFixedLength(true);

                entity.Property(e => e.PubFmngYn)
                    .HasMaxLength(1)
                    .HasColumnName("pub_fmng_yn")
                    .HasDefaultValueSql("'NULL'")
                    .IsFixedLength(true)
                    .HasComment("공동방화관리여부");

                entity.Property(e => e.TelNum)
                    .HasMaxLength(20)
                    .HasColumnName("tel_num")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("전화번호");

                entity.Property(e => e.Title)
                    .HasMaxLength(50)
                    .HasColumnName("title")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("직위");

                entity.Property(e => e.UseYn)
                    .HasMaxLength(1)
                    .HasColumnName("use_yn")
                    .HasDefaultValueSql("'''1'''")
                    .IsFixedLength(true)
                    .HasComment("사용여부");

                entity.Property(e => e.ZipCode)
                    .HasMaxLength(10)
                    .HasColumnName("zip_code")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("우편번호");

                entity.HasOne(d => d.Obj)
                    .WithMany(p => p.IsFmgrs)
                    .HasForeignKey(d => d.ObjId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_is_fmgr_obj_id");
            });

            modelBuilder.Entity<IsHosp>(entity =>
            {
                entity.HasKey(e => e.HospId)
                    .HasName("PRIMARY");

                entity.ToTable("is_hosp");

                entity.HasComment("병원");

                entity.HasIndex(e => e.WardId, "is_hosp_FK");

                entity.Property(e => e.HospId)
                    .HasColumnType("int(11)")
                    .HasColumnName("hosp_id")
                    .HasComment("병원ID");

                entity.Property(e => e.Address)
                    .HasMaxLength(100)
                    .HasColumnName("address")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("주소");

                entity.Property(e => e.AmbulCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("ambul_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("구급차수");

                entity.Property(e => e.DayTel)
                    .HasMaxLength(20)
                    .HasColumnName("day_tel")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("주간전화번호");

                entity.Property(e => e.DoctorCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("doctor_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("의사수");

                entity.Property(e => e.ErMedicalYn)
                    .HasMaxLength(1)
                    .HasColumnName("er_medical_yn")
                    .HasDefaultValueSql("'NULL'")
                    .IsFixedLength(true)
                    .HasComment("지정응급의료기관/센터여부");

                entity.Property(e => e.HospClsCd)
                    .HasColumnType("int(11)")
                    .HasColumnName("hosp_cls_cd")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("병원구분코드");

                entity.Property(e => e.HospName)
                    .HasMaxLength(50)
                    .HasColumnName("hosp_name")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("병원명");

                entity.Property(e => e.LchgDtime)
                    .HasMaxLength(20)
                    .HasColumnName("lchg_dtime")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("수정일시");

                entity.Property(e => e.LchgUserId)
                    .HasMaxLength(20)
                    .HasColumnName("lchg_user_id")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("수정자ID");

                entity.Property(e => e.MainTreat)
                    .HasMaxLength(250)
                    .HasColumnName("main_treat")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("주진료과목");

                entity.Property(e => e.NightTel)
                    .HasMaxLength(20)
                    .HasColumnName("night_tel")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("야간전화번호");

                entity.Property(e => e.OnDutyYn)
                    .HasMaxLength(1)
                    .HasColumnName("on_duty_yn")
                    .HasDefaultValueSql("'NULL'")
                    .IsFixedLength(true)
                    .HasComment("당직근무여부");

                entity.Property(e => e.Remark)
                    .HasMaxLength(300)
                    .HasColumnName("remark")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("비고");

                entity.Property(e => e.RemoveYn)
                    .HasMaxLength(1)
                    .HasColumnName("remove_yn")
                    .HasDefaultValueSql("'NULL'")
                    .IsFixedLength(true)
                    .HasComment("철거여부");

                entity.Property(e => e.SickbedCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("sickbed_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("병상수");

                entity.Property(e => e.UndgrdYn)
                    .HasMaxLength(1)
                    .HasColumnName("undgrd_yn")
                    .HasDefaultValueSql("'''0'''")
                    .IsFixedLength(true)
                    .HasComment("지하 여부");

                entity.Property(e => e.UseYn)
                    .HasMaxLength(1)
                    .HasColumnName("use_yn")
                    .HasDefaultValueSql("'NULL'")
                    .IsFixedLength(true)
                    .HasComment("사용여부");

                entity.Property(e => e.WardId)
                    .HasMaxLength(9)
                    .HasColumnName("ward_id")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("서·센터ID");

                entity.Property(e => e.ZipCode)
                    .HasMaxLength(10)
                    .HasColumnName("zip_code")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("우편번호");
            });

            modelBuilder.Entity<IsHyd>(entity =>
            {
                entity.HasKey(e => e.HydId)
                    .HasName("PRIMARY");

                entity.ToTable("is_hyd");

                entity.HasComment("소방용수");

                entity.HasIndex(e => e.WardId, "FK_is_hyd_ward_id");

                entity.Property(e => e.HydId)
                    .HasColumnType("int(11)")
                    .HasColumnName("hyd_id")
                    .HasComment("소방용수ID");

                entity.Property(e => e.Address)
                    .HasMaxLength(100)
                    .HasColumnName("address")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("주소");

                entity.Property(e => e.AdjBldg)
                    .HasMaxLength(100)
                    .HasColumnName("adj_bldg")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("인근건물");

                entity.Property(e => e.FormCd)
                    .HasColumnType("int(11)")
                    .HasColumnName("form_cd")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("형식코드");

                entity.Property(e => e.HydWtrNo)
                    .HasMaxLength(100)
                    .HasColumnName("hyd_wtr_no")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("소방용수수리번호");

                entity.Property(e => e.HydkndCd)
                    .HasColumnType("int(11)")
                    .HasColumnName("hydknd_cd")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("소방용수시설종별코드");

                entity.Property(e => e.LchgDtime)
                    .HasMaxLength(20)
                    .HasColumnName("lchg_dtime")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("수정일시");

                entity.Property(e => e.LchgUserId)
                    .HasMaxLength(20)
                    .HasColumnName("lchg_user_id")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("수정자ID");

                entity.Property(e => e.Remark)
                    .HasMaxLength(300)
                    .HasColumnName("remark")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("비고");

                entity.Property(e => e.RemoveDate)
                    .HasColumnType("date")
                    .HasColumnName("remove_date")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("철거일자");

                entity.Property(e => e.SignpostYn)
                    .HasMaxLength(1)
                    .HasColumnName("signpost_yn")
                    .HasDefaultValueSql("'NULL'")
                    .IsFixedLength(true)
                    .HasComment("표지판설치여부");

                entity.Property(e => e.UseYn)
                    .HasMaxLength(1)
                    .HasColumnName("use_yn")
                    .HasDefaultValueSql("'NULL'")
                    .IsFixedLength(true)
                    .HasComment("사용여부");

                entity.Property(e => e.WardId)
                    .HasMaxLength(9)
                    .HasColumnName("ward_id")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("서·센터ID");

                entity.Property(e => e.X)
                    .HasMaxLength(20)
                    .HasColumnName("x")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("소방용수X좌표");

                entity.Property(e => e.Y)
                    .HasMaxLength(20)
                    .HasColumnName("y")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("소방용수Y좌표");

                entity.Property(e => e.ZipCode)
                    .HasMaxLength(10)
                    .HasColumnName("zip_code")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("우편번호");

                entity.HasOne(d => d.Ward)
                    .WithMany(p => p.IsHyds)
                    .HasForeignKey(d => d.WardId)
                    .HasConstraintName("FK_is_hyd_ward_id");
            });

            modelBuilder.Entity<IsJurisward>(entity =>
            {
                entity.HasKey(e => new { e.WardId, e.ZipCode })
                    .HasName("PRIMARY");

                entity.ToTable("is_jurisward");

                entity.HasComment("서.센터 관할 정보");

                entity.Property(e => e.WardId)
                    .HasMaxLength(9)
                    .HasColumnName("ward_id")
                    .HasComment("서·센터ID");

                entity.Property(e => e.ZipCode)
                    .HasMaxLength(10)
                    .HasColumnName("zip_code")
                    .HasComment("관할우편번호");

                entity.Property(e => e.BasicJurisYn)
                    .HasMaxLength(1)
                    .HasColumnName("basic_juris_yn")
                    .HasDefaultValueSql("'NULL'")
                    .IsFixedLength(true)
                    .HasComment("기본관할여부");

                entity.Property(e => e.JurisDivideYn)
                    .HasMaxLength(1)
                    .HasColumnName("juris_divide_yn")
                    .HasDefaultValueSql("'NULL'")
                    .IsFixedLength(true)
                    .HasComment("관할분할여부");

                entity.HasOne(d => d.Ward)
                    .WithMany(p => p.IsJuriswards)
                    .HasForeignKey(d => d.WardId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_juris_ward_id");
            });

            modelBuilder.Entity<IsObj>(entity =>
            {
                entity.HasKey(e => e.ObjId)
                    .HasName("PRIMARY");

                entity.ToTable("is_obj");

                entity.HasComment("대상물");

                entity.HasIndex(e => e.WardId, "FK_is_obj_ward_id");

                entity.Property(e => e.ObjId)
                    .HasColumnType("int(20)")
                    .HasColumnName("obj_id")
                    .HasComment("대상물ID");

                entity.Property(e => e.Address)
                    .HasMaxLength(100)
                    .HasColumnName("address")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("address");

                entity.Property(e => e.AdgInfo)
                    .HasMaxLength(1000)
                    .HasColumnName("adg_info")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("인근정보");

                entity.Property(e => e.BldgNm)
                    .HasMaxLength(200)
                    .HasColumnName("bldg_nm")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("건물명");

                entity.Property(e => e.ConstName)
                    .HasMaxLength(50)
                    .HasColumnName("const_name")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("대상물명");

                entity.Property(e => e.DayTel)
                    .HasMaxLength(20)
                    .HasColumnName("day_tel")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("주간전화번호");

                entity.Property(e => e.InstitClsCd)
                    .HasColumnType("int(11)")
                    .HasColumnName("instit_cls_cd")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("공공기관분류코드");

                entity.Property(e => e.InstitYn)
                    .HasMaxLength(1)
                    .HasColumnName("instit_yn")
                    .HasDefaultValueSql("'NULL'")
                    .IsFixedLength(true)
                    .HasComment("공공기관여부");

                entity.Property(e => e.NightTel)
                    .HasMaxLength(20)
                    .HasColumnName("night_tel")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("야간전화번호");

                entity.Property(e => e.ObjCd)
                    .HasColumnType("int(11)")
                    .HasColumnName("obj_cd")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("대상물구분코드");

                entity.Property(e => e.ObjManNum)
                    .HasMaxLength(20)
                    .HasColumnName("obj_man_num")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("대상물관리번호");

                entity.Property(e => e.ObjStdCd)
                    .HasColumnType("int(11)")
                    .HasColumnName("obj_std_cd")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("대상물기준코드");

                entity.Property(e => e.PrevdstTel)
                    .HasMaxLength(20)
                    .HasColumnName("prevdst_tel")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("방재실전화번호");

                entity.Property(e => e.PubfaclYn)
                    .HasMaxLength(1)
                    .HasColumnName("pubfacl_yn")
                    .HasDefaultValueSql("'NULL'")
                    .IsFixedLength(true)
                    .HasComment("다중이용업여부");

                entity.Property(e => e.RpsnFirmName)
                    .HasMaxLength(50)
                    .HasColumnName("rpsn_firm_name")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("대표상호명");

                entity.Property(e => e.SelfobjYn)
                    .HasMaxLength(1)
                    .HasColumnName("selfobj_yn")
                    .HasDefaultValueSql("'NULL'")
                    .IsFixedLength(true)
                    .HasComment("자체점검대상여부");

                entity.Property(e => e.SubUseDesc)
                    .HasMaxLength(100)
                    .HasColumnName("sub_use_desc")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("부용도내역");

                entity.Property(e => e.TunnelClsCd)
                    .HasColumnType("int(11)")
                    .HasColumnName("tunnel_cls_cd")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("터널구분코드");

                entity.Property(e => e.TunnelLength)
                    .HasColumnType("decimal(5,1)")
                    .HasColumnName("tunnel_length")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("터널길이");

                entity.Property(e => e.UseConfmDate)
                    .HasColumnType("date")
                    .HasColumnName("use_confm_date")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("사용승인날짜");

                entity.Property(e => e.UseYn)
                    .HasMaxLength(1)
                    .HasColumnName("use_yn")
                    .HasDefaultValueSql("'''1'''")
                    .IsFixedLength(true)
                    .HasComment("사용여부");

                entity.Property(e => e.UsedCd)
                    .HasColumnType("int(11)")
                    .HasColumnName("used_cd")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("주용도코드");

                entity.Property(e => e.WardId)
                    .HasMaxLength(9)
                    .HasColumnName("ward_id")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("서·센터ID");

                entity.Property(e => e.WeakobjCd)
                    .HasColumnType("int(11)")
                    .HasColumnName("weakobj_cd")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("대행화재취약대상코드");

                entity.Property(e => e.WeakobjYn)
                    .HasMaxLength(1)
                    .HasColumnName("weakobj_yn")
                    .HasDefaultValueSql("'NULL'")
                    .IsFixedLength(true)
                    .HasComment("대형화재취약대상여부");

                entity.Property(e => e.X)
                    .HasMaxLength(20)
                    .HasColumnName("x")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("x");

                entity.Property(e => e.Y)
                    .HasMaxLength(20)
                    .HasColumnName("y")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("y");

                entity.Property(e => e.ZipCode)
                    .HasMaxLength(10)
                    .HasColumnName("zip_code")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("zip code");

                entity.HasOne(d => d.Ward)
                    .WithMany(p => p.IsObjs)
                    .HasForeignKey(d => d.WardId)
                    .HasConstraintName("FK_is_obj_ward_id");
            });

            modelBuilder.Entity<IsObjdrw>(entity =>
            {
                entity.HasKey(e => new { e.DrwSeq, e.ObjId })
                    .HasName("PRIMARY");

                entity.ToTable("is_objdrw");

                entity.HasComment("대상물도면정보");

                entity.HasIndex(e => e.ObjId, "FK_is_objdrw_obj_id");

                entity.Property(e => e.DrwSeq)
                    .HasColumnType("int(11)")
                    .ValueGeneratedOnAdd()
                    .HasColumnName("drw_seq")
                    .HasComment("도면일련번호");

                entity.Property(e => e.ObjId)
                    .HasColumnType("int(20)")
                    .HasColumnName("obj_id")
                    .HasComment("대상물ID");

                entity.Property(e => e.CreatedDate)
                    .HasMaxLength(20)
                    .HasColumnName("created_date")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.FileName)
                    .HasMaxLength(100)
                    .HasColumnName("file_name")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("파일명");

                entity.Property(e => e.FilePath)
                    .HasMaxLength(100)
                    .HasColumnName("file_path")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("파일경로");

                entity.Property(e => e.Remark)
                    .HasMaxLength(1000)
                    .HasColumnName("remark")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("비고");

                entity.Property(e => e.UseYn)
                    .HasMaxLength(1)
                    .HasColumnName("use_yn")
                    .HasDefaultValueSql("'''1'''")
                    .IsFixedLength(true)
                    .HasComment("사용여부");

                entity.HasOne(d => d.Obj)
                    .WithMany(p => p.IsObjdrws)
                    .HasForeignKey(d => d.ObjId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_is_objdrw_obj_id");
            });

            modelBuilder.Entity<IsObjfireequip>(entity =>
            {
                entity.HasKey(e => new { e.ObjId, e.BldgSeq, e.StorySeq })
                    .HasName("PRIMARY");

                entity.ToTable("is_objfireequip");

                entity.HasComment("대상물소방시설");

                entity.HasIndex(e => new { e.StorySeq, e.ObjId, e.BldgSeq }, "FK_is_objfireequip");

                entity.Property(e => e.ObjId)
                    .HasColumnType("int(20)")
                    .HasColumnName("obj_id")
                    .HasComment("대상물ID");

                entity.Property(e => e.BldgSeq)
                    .HasColumnType("int(11)")
                    .HasColumnName("bldg_seq")
                    .HasComment("동일련번호");

                entity.Property(e => e.StorySeq)
                    .HasColumnType("int(11)")
                    .HasColumnName("story_seq")
                    .HasComment("층번호");

                entity.Property(e => e.AutoFireFindCircuitCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("auto_fire_find_circuit_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("자동 화재 탐지_회로");

                entity.Property(e => e.AutoFireFindSensCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("auto_fire_find_sens_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("자동 화재 탐지_감지기");

                entity.Property(e => e.AutoFireNewsfCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("auto_fire_newsf_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("자동 화재 속보");

                entity.Property(e => e.CarbonDioxAvCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("carbon_diox_av_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("이산화탄소AV");

                entity.Property(e => e.CarbonDioxHCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("carbon_diox_h_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("이산화탄소H");

                entity.Property(e => e.CarssetteCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("carssette_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("카세트");

                entity.Property(e => e.CurtainCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("curtain_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("커튼");

                entity.Property(e => e.DescSlowDeviceCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("desc_slow_device_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("완강기");

                entity.Property(e => e.EmgBrodcCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("emg_brodc_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("비상 방송");

                entity.Property(e => e.EmgLightCnt)
                    .HasColumnType("int(5)")
                    .HasColumnName("emg_light_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("비상조명등");

                entity.Property(e => e.EmgPlugCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("emg_plug_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("비상콘센트");

                entity.Property(e => e.EmgWaringCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("emg_waring_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("비상 정보");

                entity.Property(e => e.ExtingCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("exting_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("소화기");

                entity.Property(e => e.ExtingPumpCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("exting_pump_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("동력소방펌프");

                entity.Property(e => e.ExtingWaterCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("exting_water_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("소화수조");

                entity.Property(e => e.GasLkgeWaringCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("gas_lkge_waring_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("가스 누설 경보기");

                entity.Property(e => e.HalogenCompHCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("halogen_comp_h_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("할로겐화합물H");

                entity.Property(e => e.HarlogenCompAvCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("harlogen_comp_av_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("할로겐화합물AV");

                entity.Property(e => e.HydEquipEtcCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("hyd_equip_etc_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("소방 용수 설비 기타");

                entity.Property(e => e.InExtingCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("in_exting_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("옥내소화전");

                entity.Property(e => e.InduceLightCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("induce_light_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("유도등");

                entity.Property(e => e.InduceSignpostCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("induce_signpost_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("유도 표지");

                entity.Property(e => e.LadderCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("ladder_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("피난사다리");

                entity.Property(e => e.LkgeWaringCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("lkge_waring_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("누전 경보기");

                entity.Property(e => e.LwtrCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("lwtr_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("저수조");

                entity.Property(e => e.MeasureEquipCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("measure_equip_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("피난교");

                entity.Property(e => e.MeasureRopeCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("measure_rope_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("피난밧줄");

                entity.Property(e => e.OutExtingCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("out_exting_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("옥외소화전");

                entity.Property(e => e.PoExtingAvCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("po_exting_av_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("포소화AV");

                entity.Property(e => e.PoExtingHCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("po_exting_h_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("포 소화_H");

                entity.Property(e => e.PowderExtingAvCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("powder_exting_av_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("분말소화AV");

                entity.Property(e => e.PowderExtingHCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("powder_exting_h_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("분말소화H");

                entity.Property(e => e.RescueCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("rescue_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("구조대");

                entity.Property(e => e.RescueEquipCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("rescue_equip_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("인명구조기구");

                entity.Property(e => e.ResistEtcCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("resist_etc_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("방염 기타");

                entity.Property(e => e.ResmokeCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("resmoke_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("제연");

                entity.Property(e => e.SafeMatCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("safe_mat_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("공기안전매트");

                entity.Property(e => e.SimplctyExtingCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("simplcty_exting_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("간이 소화 용구");

                entity.Property(e => e.SlideCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("slide_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("미끄림대");

                entity.Property(e => e.SprayExtingAvCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("spray_exting_av_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("물분모소화AV");

                entity.Property(e => e.SprayExtingHCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("spray_exting_h_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("물분무소화H");

                entity.Property(e => e.SprinklerAvCnt)
                    .HasColumnType("int(5)")
                    .HasColumnName("sprinkler_av_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("스프링클러AV");

                entity.Property(e => e.SprinklerHCnt)
                    .HasColumnType("int(5)")
                    .HasColumnName("sprinkler_h_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("스프링클러H");

                entity.Property(e => e.UseYn)
                    .HasMaxLength(1)
                    .HasColumnName("use_yn")
                    .HasDefaultValueSql("'''1'''")
                    .IsFixedLength(true)
                    .HasComment("사용 여부");

                entity.Property(e => e.WaterExtingCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("water_exting_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("상수도소화용수");

                entity.Property(e => e.WaterSpringklingCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("water_springkling_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("연결살수");

                entity.Property(e => e.WirelessCommCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("wireless_comm_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("무선통신보조");

                entity.Property(e => e.WtrpipeCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("wtrpipe_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("연결송수관");

                entity.HasOne(d => d.IsStory)
                    .WithMany(p => p.IsObjfireequips)
                    .HasForeignKey(d => new { d.StorySeq, d.ObjId, d.BldgSeq })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_is_objfireequip");
            });

            modelBuilder.Entity<IsObjhist>(entity =>
            {
                entity.HasKey(e => new { e.ObjhistSeq, e.ObjId, e.BldgSeq })
                    .HasName("PRIMARY");

                entity.ToTable("is_objhist");

                entity.HasComment("대상물건축연혁");

                entity.HasIndex(e => new { e.BldgSeq, e.ObjId }, "FK_is_objhist");

                entity.Property(e => e.ObjhistSeq)
                    .HasColumnType("int(11)")
                    .ValueGeneratedOnAdd()
                    .HasColumnName("objhist_seq")
                    .HasComment("건축연혁일련번호");

                entity.Property(e => e.ObjId)
                    .HasColumnType("int(20)")
                    .HasColumnName("obj_id")
                    .HasComment("대상물ID");

                entity.Property(e => e.BldgSeq)
                    .HasColumnType("int(11)")
                    .HasColumnName("bldg_seq")
                    .HasComment("동일련번호");

                entity.Property(e => e.BstoryCnt)
                    .HasColumnType("int(3)")
                    .HasColumnName("bstory_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("지하층수");

                entity.Property(e => e.ChgDesc)
                    .HasMaxLength(1000)
                    .HasColumnName("chg_desc")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("변경내용");

                entity.Property(e => e.ConstUse)
                    .HasMaxLength(500)
                    .HasColumnName("const_use")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("건축물용도");

                entity.Property(e => e.Etc)
                    .HasMaxLength(300)
                    .HasColumnName("etc")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("기타");

                entity.Property(e => e.FinishDate)
                    .HasColumnType("date")
                    .HasColumnName("finish_date")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("준공일자");

                entity.Property(e => e.FloorArea)
                    .HasColumnType("decimal(10,2)")
                    .HasColumnName("floor_area")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("바닥면적");

                entity.Property(e => e.GrantDate)
                    .HasColumnType("date")
                    .HasColumnName("grant_date")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("허가일자");

                entity.Property(e => e.HistTypeCd)
                    .HasColumnType("int(11)")
                    .HasColumnName("hist_type_cd")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("연혁종별코드");

                entity.Property(e => e.PreuseYn)
                    .HasMaxLength(1)
                    .HasColumnName("preuse_yn")
                    .HasDefaultValueSql("'''0'''")
                    .IsFixedLength(true)
                    .HasComment("가사용여부");

                entity.Property(e => e.Struct1)
                    .HasColumnType("int(11)")
                    .HasColumnName("struct_1")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("건물구조식코드");

                entity.Property(e => e.Struct2)
                    .HasColumnType("int(11)")
                    .HasColumnName("struct_2")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("건물구조조코드");

                entity.Property(e => e.Struct3)
                    .HasColumnType("int(11)")
                    .HasColumnName("struct_3")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("건물구조즙코드");

                entity.Property(e => e.TotArea)
                    .HasColumnType("decimal(10,2)")
                    .HasColumnName("tot_area")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("연면적");

                entity.Property(e => e.UseYn)
                    .HasMaxLength(1)
                    .HasColumnName("use_yn")
                    .HasDefaultValueSql("'''1'''")
                    .IsFixedLength(true)
                    .HasComment("사용여부");

                entity.Property(e => e.UstoryCnt)
                    .HasColumnType("int(3)")
                    .HasColumnName("ustory_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("지상층수");

                entity.HasOne(d => d.IsBldg)
                    .WithMany(p => p.IsObjhists)
                    .HasForeignKey(d => new { d.BldgSeq, d.ObjId })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_is_objhist");
            });

            modelBuilder.Entity<IsObjpic>(entity =>
            {
                entity.HasKey(e => new { e.ObjPhotoSeq, e.ObjId })
                    .HasName("PRIMARY");

                entity.ToTable("is_objpic");

                entity.HasComment("경방계획도사진");

                entity.HasIndex(e => e.ObjId, "FK_is_objpic_obj_id");

                entity.Property(e => e.ObjPhotoSeq)
                    .HasColumnType("int(11)")
                    .ValueGeneratedOnAdd()
                    .HasColumnName("obj_photo_seq")
                    .HasComment("경방계획도사진일련번호");

                entity.Property(e => e.ObjId)
                    .HasColumnType("int(20)")
                    .HasColumnName("obj_id")
                    .HasComment("대상물ID");

                entity.Property(e => e.FileName)
                    .HasMaxLength(100)
                    .HasColumnName("file_name")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("파일명");

                entity.Property(e => e.FilePath)
                    .HasMaxLength(100)
                    .HasColumnName("file_path")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("파일경로");

                entity.Property(e => e.ObjPhotoTypeCd)
                    .HasColumnType("int(11)")
                    .HasColumnName("obj_photo_type_cd")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("경방계획도사진구분코드");

                entity.Property(e => e.Remark)
                    .HasMaxLength(1000)
                    .HasColumnName("remark")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("비고");

                entity.Property(e => e.UseYn)
                    .HasMaxLength(1)
                    .HasColumnName("use_yn")
                    .HasDefaultValueSql("'''1'''")
                    .IsFixedLength(true)
                    .HasComment("사용여부");

                entity.HasOne(d => d.Obj)
                    .WithMany(p => p.IsObjpics)
                    .HasForeignKey(d => d.ObjId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_is_objpic_obj_id");
            });

            modelBuilder.Entity<IsObjplan>(entity =>
            {
                entity.HasKey(e => e.ObjId)
                    .HasName("PRIMARY");

                entity.ToTable("is_objplan");

                entity.HasComment("경방계획");

                entity.Property(e => e.ObjId)
                    .HasColumnType("int(11)")
                    .HasColumnName("obj_id")
                    .HasComment("대상물ID");

                entity.Property(e => e.EnvDesc)
                    .HasMaxLength(1000)
                    .HasColumnName("env_desc")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("주위환경");

                entity.Property(e => e.EscPlace)
                    .HasMaxLength(1000)
                    .HasColumnName("esc_place")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("대피장소");

                entity.Property(e => e.EtcRemark)
                    .HasMaxLength(2000)
                    .HasColumnName("etc_remark")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("특기사항");

                entity.Property(e => e.ExpandRsn)
                    .HasMaxLength(1000)
                    .HasColumnName("expand_rsn")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("화재취약및연소확대요인");

                entity.Property(e => e.FmngIssue)
                    .HasMaxLength(1000)
                    .HasColumnName("fmng_issue")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("방화관리문제점");

                entity.Property(e => e.MvestExptAmt)
                    .HasColumnType("decimal(12,0)")
                    .HasColumnName("mvest_expt_amt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("동산피해예상액(원)");

                entity.Property(e => e.RealestExptAmt)
                    .HasColumnType("decimal(12,0)")
                    .HasColumnName("realest_expt_amt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("부동상피해예상액(원)");

                entity.Property(e => e.RescEscCplan)
                    .HasMaxLength(1000)
                    .HasColumnName("resc_esc_cplan")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("인명구조또는피난대책");

                entity.Property(e => e.SelfvolunOrg)
                    .HasMaxLength(1000)
                    .HasColumnName("selfvolun_org")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("자위소방재조직상황");

                entity.Property(e => e.TotAmt)
                    .HasColumnType("decimal(12,0)")
                    .HasColumnName("tot_amt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("총액(원)");

                entity.Property(e => e.TrainingGuideDesc)
                    .HasMaxLength(1000)
                    .HasColumnName("training_guide_desc")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("소방훈련지도개요");

                entity.Property(e => e.UseYn)
                    .HasMaxLength(1)
                    .HasColumnName("use_yn")
                    .HasDefaultValueSql("'''1'''")
                    .IsFixedLength(true)
                    .HasComment("사용여부");

                entity.Property(e => e.Ward1Dist)
                    .HasColumnType("decimal(8,1)")
                    .HasColumnName("ward1_dist")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("본서와의거리(km)");

                entity.Property(e => e.Ward2Dist)
                    .HasColumnType("decimal(8,1)")
                    .HasColumnName("ward2_dist")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("파출소와의거리(km)");

                entity.HasOne(d => d.Obj)
                    .WithOne(p => p.IsObjplan)
                    .HasForeignKey<IsObjplan>(d => d.ObjId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("is_objplan_FK");
            });

            modelBuilder.Entity<IsObjrel>(entity =>
            {
                entity.HasKey(e => new { e.RelSeq, e.ObjId })
                    .HasName("PRIMARY");

                entity.ToTable("is_objrel");

                entity.HasComment("대상물관계자내역");

                entity.HasIndex(e => e.ObjId, "FK_is_objrel_obj_id");

                entity.Property(e => e.RelSeq)
                    .HasColumnType("int(11)")
                    .ValueGeneratedOnAdd()
                    .HasColumnName("rel_seq")
                    .HasComment("관계자일련번호");

                entity.Property(e => e.ObjId)
                    .HasColumnType("int(20)")
                    .HasColumnName("obj_id")
                    .HasComment("대상물ID");

                entity.Property(e => e.Address)
                    .HasMaxLength(100)
                    .HasColumnName("address")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("주소");

                entity.Property(e => e.Birthday)
                    .HasColumnType("date")
                    .HasColumnName("birthday")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("관계자생년월일");

                entity.Property(e => e.CitizenNo)
                    .HasMaxLength(20)
                    .HasColumnName("citizen_no")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("주민번호");

                entity.Property(e => e.FirstName)
                    .HasMaxLength(50)
                    .HasColumnName("first_name")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("관계자명");

                entity.Property(e => e.JobDesc)
                    .HasMaxLength(50)
                    .HasColumnName("job_desc")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("직업");

                entity.Property(e => e.LastName)
                    .HasMaxLength(50)
                    .HasColumnName("last_name")
                    .HasDefaultValueSql("'NULL'");

                entity.Property(e => e.RelCd)
                    .HasColumnType("int(11)")
                    .HasColumnName("rel_cd")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("관계자구분코드");

                entity.Property(e => e.TelNum)
                    .HasMaxLength(20)
                    .HasColumnName("tel_num")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("전화번호");

                entity.Property(e => e.UseYn)
                    .HasMaxLength(1)
                    .HasColumnName("use_yn")
                    .HasDefaultValueSql("'''1'''")
                    .IsFixedLength(true)
                    .HasComment("사용여부");

                entity.Property(e => e.ZipCode)
                    .HasMaxLength(10)
                    .HasColumnName("zip_code")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("우편번호");

                entity.HasOne(d => d.Obj)
                    .WithMany(p => p.IsObjrels)
                    .HasForeignKey(d => d.ObjId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_is_objrel_obj_id");
            });

            modelBuilder.Entity<IsOrg>(entity =>
            {
                entity.HasKey(e => e.OrgId)
                    .HasName("PRIMARY");

                entity.ToTable("is_org");

                entity.HasComment("유관기관 테이블");

                entity.HasIndex(e => e.WardId, "FK_is_org_ward_id");

                entity.Property(e => e.OrgId)
                    .HasColumnType("int(11)")
                    .HasColumnName("org_id")
                    .HasComment("유관기관 ID");

                entity.Property(e => e.Address)
                    .HasMaxLength(100)
                    .HasColumnName("address")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("주소");

                entity.Property(e => e.AgreeYn)
                    .HasMaxLength(1)
                    .HasColumnName("agree_yn")
                    .HasDefaultValueSql("'NULL'")
                    .IsFixedLength(true)
                    .HasComment("승인여부");

                entity.Property(e => e.Cell)
                    .HasMaxLength(20)
                    .HasColumnName("cell")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("이동전화번호");

                entity.Property(e => e.CellOwnerName)
                    .HasMaxLength(20)
                    .HasColumnName("cell_owner_name")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("이동전화소유자명");

                entity.Property(e => e.DayFax)
                    .HasMaxLength(20)
                    .HasColumnName("day_fax")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("주간팩스번호");

                entity.Property(e => e.DayTel)
                    .HasMaxLength(20)
                    .HasColumnName("day_tel")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("주간전화번호");

                entity.Property(e => e.DsrOrgYn)
                    .HasMaxLength(1)
                    .HasColumnName("dsr_org_yn")
                    .HasDefaultValueSql("'NULL'")
                    .IsFixedLength(true)
                    .HasComment("재난조직여부");

                entity.Property(e => e.DsrSprtOrgYn)
                    .HasMaxLength(1)
                    .HasColumnName("dsr_sprt_org_yn")
                    .HasDefaultValueSql("'NULL'")
                    .IsFixedLength(true)
                    .HasComment("재난지원여부");

                entity.Property(e => e.Etc)
                    .HasMaxLength(100)
                    .HasColumnName("etc")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("기타내용");

                entity.Property(e => e.LchgDtime)
                    .HasMaxLength(20)
                    .HasColumnName("lchg_dtime")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("변경일시");

                entity.Property(e => e.LchgUserId)
                    .HasMaxLength(20)
                    .HasColumnName("lchg_user_id")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("변경자ID");

                entity.Property(e => e.NightFax)
                    .HasMaxLength(20)
                    .HasColumnName("night_fax")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("야간팩스번호");

                entity.Property(e => e.NightTel)
                    .HasMaxLength(20)
                    .HasColumnName("night_tel")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("야간전화번호");

                entity.Property(e => e.OrgClsCd)
                    .HasColumnType("int(11)")
                    .HasColumnName("org_cls_cd")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("조직분류코드");

                entity.Property(e => e.OrgName)
                    .HasMaxLength(50)
                    .HasColumnName("org_name")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("유관기관 명");

                entity.Property(e => e.Remark)
                    .HasMaxLength(300)
                    .HasColumnName("remark")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("비고");

                entity.Property(e => e.TouchPadYn)
                    .HasMaxLength(1)
                    .HasColumnName("touch_pad_yn")
                    .HasDefaultValueSql("'NULL'")
                    .IsFixedLength(true)
                    .HasComment("터치패드여부");

                entity.Property(e => e.UndgrdFg)
                    .HasMaxLength(1)
                    .HasColumnName("undgrd_fg")
                    .HasDefaultValueSql("'NULL'")
                    .IsFixedLength(true)
                    .HasComment("지하여부");

                entity.Property(e => e.UpOrgName)
                    .HasMaxLength(50)
                    .HasColumnName("up_org_name")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("상위조직명");

                entity.Property(e => e.UseYn)
                    .HasMaxLength(1)
                    .HasColumnName("use_yn")
                    .HasDefaultValueSql("'''1'''")
                    .IsFixedLength(true)
                    .HasComment("사용여부");

                entity.Property(e => e.WardId)
                    .IsRequired()
                    .HasMaxLength(9)
                    .HasColumnName("ward_id")
                    .HasComment("서센터ID");

                entity.Property(e => e.ZipCode)
                    .HasMaxLength(10)
                    .HasColumnName("zip_code")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("우편번호");

                entity.HasOne(d => d.Ward)
                    .WithMany(p => p.IsOrgs)
                    .HasForeignKey(d => d.WardId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_is_org_ward_id");
            });

            modelBuilder.Entity<IsOrgfire>(entity =>
            {
                entity.HasKey(e => new { e.OrgId, e.FightClsCd })
                    .HasName("PRIMARY");

                entity.ToTable("is_orgfire");

                entity.HasComment("유관기관동원소방력");

                entity.Property(e => e.OrgId)
                    .HasColumnType("int(11)")
                    .HasColumnName("org_id")
                    .HasComment("유관기관 ID");

                entity.Property(e => e.FightClsCd)
                    .HasColumnType("int(11)")
                    .HasColumnName("fight_cls_cd")
                    .HasComment("동원장비코드");

                entity.Property(e => e.LchgDtime)
                    .HasMaxLength(20)
                    .HasColumnName("lchg_dtime")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("수정일시");

                entity.Property(e => e.LchgUserId)
                    .HasMaxLength(20)
                    .HasColumnName("lchg_user_id")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("수정자ID");

                entity.Property(e => e.MobFightCnt)
                    .HasColumnType("int(4)")
                    .HasColumnName("mob_fight_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("동원소방력수");

                entity.Property(e => e.UseYn)
                    .HasMaxLength(1)
                    .HasColumnName("use_yn")
                    .HasDefaultValueSql("'NULL'")
                    .IsFixedLength(true)
                    .HasComment("사용여부");

                entity.HasOne(d => d.Org)
                    .WithMany(p => p.IsOrgfires)
                    .HasForeignKey(d => d.OrgId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_is_orgfire_org_id");
            });

            modelBuilder.Entity<IsOrgman>(entity =>
            {
                entity.HasKey(e => new { e.OrgId, e.SkillLicnCd })
                    .HasName("PRIMARY");

                entity.ToTable("is_orgman");

                entity.HasComment("유관기관인력");

                entity.Property(e => e.OrgId)
                    .HasColumnType("int(11)")
                    .HasColumnName("org_id")
                    .HasComment("유관기관 ID");

                entity.Property(e => e.SkillLicnCd)
                    .HasColumnType("int(11)")
                    .HasColumnName("skill_licn_cd")
                    .HasComment("인력코드");

                entity.Property(e => e.LchgDtime)
                    .HasMaxLength(20)
                    .HasColumnName("lchg_dtime")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("수정일시");

                entity.Property(e => e.LchgUserId)
                    .HasMaxLength(20)
                    .HasColumnName("lchg_user_id")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("수정자ID");

                entity.Property(e => e.MobManCnt)
                    .HasColumnType("int(5)")
                    .HasColumnName("mob_man_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("동원인원수");

                entity.Property(e => e.UseYn)
                    .HasMaxLength(1)
                    .HasColumnName("use_yn")
                    .HasDefaultValueSql("'NULL'")
                    .IsFixedLength(true)
                    .HasComment("사용여부");

                entity.HasOne(d => d.Org)
                    .WithMany(p => p.IsOrgmen)
                    .HasForeignKey(d => d.OrgId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_is_orgman_org_id");
            });

            modelBuilder.Entity<IsStory>(entity =>
            {
                entity.HasKey(e => new { e.StorySeq, e.ObjId, e.BldgSeq })
                    .HasName("PRIMARY");

                entity.ToTable("is_story");

                entity.HasComment("대상물층별내역");

                entity.HasIndex(e => new { e.BldgSeq, e.ObjId }, "FK_is_story");

                entity.Property(e => e.StorySeq)
                    .HasColumnType("int(11)")
                    .ValueGeneratedOnAdd()
                    .HasColumnName("story_seq")
                    .HasComment("층일련번호");

                entity.Property(e => e.ObjId)
                    .HasColumnType("int(20)")
                    .HasColumnName("obj_id")
                    .HasComment("대상물ID");

                entity.Property(e => e.BldgSeq)
                    .HasColumnType("int(11)")
                    .HasColumnName("bldg_seq")
                    .HasComment("동일련번호");

                entity.Property(e => e.AccomManCnt)
                    .HasColumnType("int(5)")
                    .HasColumnName("accom_man_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("수용인원수");

                entity.Property(e => e.Deco)
                    .HasMaxLength(100)
                    .HasColumnName("deco")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("내장재");

                entity.Property(e => e.Etc)
                    .HasMaxLength(1000)
                    .HasColumnName("etc")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("기타");

                entity.Property(e => e.FcontCnt)
                    .HasColumnType("int(5)")
                    .HasColumnName("fcont_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("방화구획수");

                entity.Property(e => e.ResidentManCnt)
                    .HasColumnType("int(5)")
                    .HasColumnName("resident_man_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("상주인원수");

                entity.Property(e => e.StoryArea)
                    .HasColumnType("decimal(10,2)")
                    .HasColumnName("story_area")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("층면적(m2)");

                entity.Property(e => e.StoryNo)
                    .HasMaxLength(12)
                    .HasColumnName("story_no")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("층번호");

                entity.Property(e => e.UseYn)
                    .HasMaxLength(1)
                    .HasColumnName("use_yn")
                    .HasDefaultValueSql("'''1'''")
                    .IsFixedLength(true)
                    .HasComment("사용여부");

                entity.HasOne(d => d.IsBldg)
                    .WithMany(p => p.IsStories)
                    .HasForeignKey(d => new { d.BldgSeq, d.ObjId })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_is_story");
            });

            modelBuilder.Entity<IsStorySpecUsed>(entity =>
            {
                entity.HasKey(e => new { e.SpecUseSeq, e.ObjId, e.BldgSeq, e.StorySeq })
                    .HasName("PRIMARY");

                entity.ToTable("is_story_spec_used");

                entity.HasComment("층특수용도현황(특수용도)");

                entity.HasIndex(e => new { e.BldgSeq, e.ObjId }, "FK_is_story_spec_used");

                entity.HasIndex(e => new { e.StorySeq, e.ObjId, e.BldgSeq }, "is_story_spec_used_FK");

                entity.Property(e => e.SpecUseSeq)
                    .HasColumnType("int(11)")
                    .ValueGeneratedOnAdd()
                    .HasColumnName("spec_use_seq")
                    .HasComment("특수 용도 일련번호");

                entity.Property(e => e.ObjId)
                    .HasColumnType("int(11)")
                    .HasColumnName("obj_id")
                    .HasComment("대상물ID");

                entity.Property(e => e.BldgSeq)
                    .HasColumnType("int(11)")
                    .HasColumnName("bldg_seq")
                    .HasComment("동일련번호");

                entity.Property(e => e.StorySeq)
                    .HasColumnType("int(11)")
                    .HasColumnName("story_seq")
                    .HasComment("층일련번호");

                entity.Property(e => e.Area)
                    .HasColumnType("decimal(10,2)")
                    .HasColumnName("area")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("면적");

                entity.Property(e => e.Remark)
                    .HasMaxLength(1000)
                    .HasColumnName("remark")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("비고");

                entity.Property(e => e.SpecUseClsCd)
                    .HasColumnType("int(11)")
                    .HasColumnName("spec_use_cls_cd")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("특수 용도 구분 코드");

                entity.Property(e => e.UseYn)
                    .HasMaxLength(1)
                    .HasColumnName("use_yn")
                    .HasDefaultValueSql("'''1'''")
                    .IsFixedLength(true)
                    .HasComment("사용 여부");

                entity.HasOne(d => d.IsBldg)
                    .WithMany(p => p.IsStorySpecUseds)
                    .HasForeignKey(d => new { d.BldgSeq, d.ObjId })
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_is_story_spec_used");
            });

            modelBuilder.Entity<IsTypeorg>(entity =>
            {
                entity.HasKey(e => new { e.OrgId, e.EmgTypeCd })
                    .HasName("PRIMARY");

                entity.ToTable("is_typeorg");

                entity.HasComment("긴급구조유형별유관기관");

                entity.Property(e => e.OrgId)
                    .HasColumnType("int(11)")
                    .HasColumnName("org_id")
                    .HasComment("유관기관ID");

                entity.Property(e => e.EmgTypeCd)
                    .HasColumnType("int(11)")
                    .HasColumnName("emg_type_cd")
                    .HasComment("긴급구조유형코드");

                entity.Property(e => e.LchgDtime)
                    .HasMaxLength(20)
                    .HasColumnName("lchg_dtime")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("수정일시");

                entity.Property(e => e.LchgUserId)
                    .HasMaxLength(20)
                    .HasColumnName("lchg_user_id")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("수정자ID");

                entity.Property(e => e.UseYn)
                    .HasMaxLength(1)
                    .HasColumnName("use_yn")
                    .HasDefaultValueSql("'''1'''")
                    .IsFixedLength(true)
                    .HasComment("사용여부");

                entity.HasOne(d => d.Org)
                    .WithMany(p => p.IsTypeorgs)
                    .HasForeignKey(d => d.OrgId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_is_typeorg_org_id");
            });

            modelBuilder.Entity<IsVolun>(entity =>
            {
                entity.HasKey(e => e.VolunId)
                    .HasName("PRIMARY");

                entity.ToTable("is_volun");

                entity.HasComment("의용소방대원");

                entity.HasIndex(e => e.WardId, "FK_is_volun_ward_id");

                entity.Property(e => e.VolunId)
                    .HasColumnType("int(11)")
                    .HasColumnName("volun_id")
                    .HasComment("의용소방대원ID");

                entity.Property(e => e.Address)
                    .HasMaxLength(100)
                    .HasColumnName("address")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("주소");

                entity.Property(e => e.BirthDate)
                    .HasColumnType("date")
                    .HasColumnName("birth_date")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("생년월일");

                entity.Property(e => e.Cell)
                    .HasMaxLength(20)
                    .HasColumnName("cell")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("이동전화번호");

                entity.Property(e => e.ClassDate)
                    .HasColumnType("date")
                    .HasColumnName("class_date")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("현계급임용일자");

                entity.Property(e => e.ContactTel)
                    .HasMaxLength(20)
                    .HasColumnName("contact_tel")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("연락전화번호");

                entity.Property(e => e.EduCompDesc)
                    .HasMaxLength(200)
                    .HasColumnName("edu_comp_desc")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("교육이수내역");

                entity.Property(e => e.EnlistDate)
                    .HasColumnType("date")
                    .HasColumnName("enlist_date")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("입대일자");

                entity.Property(e => e.HoldLicn)
                    .HasMaxLength(200)
                    .HasColumnName("hold_licn")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("보유자격증");

                entity.Property(e => e.LchgDtime)
                    .HasMaxLength(20)
                    .HasColumnName("lchg_dtime")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("수정일시");

                entity.Property(e => e.LchgUserId)
                    .HasMaxLength(20)
                    .HasColumnName("lchg_user_id")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("수정자ID");

                entity.Property(e => e.PostName)
                    .HasMaxLength(50)
                    .HasColumnName("post_name")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("소속대명");

                entity.Property(e => e.PostpartName)
                    .HasMaxLength(50)
                    .HasColumnName("postpart_name")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("소속반명");

                entity.Property(e => e.Remark)
                    .HasMaxLength(300)
                    .HasColumnName("remark")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("비고");

                entity.Property(e => e.TitleCd)
                    .HasColumnType("int(11)")
                    .HasColumnName("title_cd")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("직위코드");

                entity.Property(e => e.UseYn)
                    .HasMaxLength(1)
                    .HasColumnName("use_yn")
                    .HasDefaultValueSql("'''1'''")
                    .IsFixedLength(true)
                    .HasComment("사용여부");

                entity.Property(e => e.VolunmanName)
                    .HasMaxLength(100)
                    .HasColumnName("volunman_name")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("의용소방대원성명");

                entity.Property(e => e.WardId)
                    .IsRequired()
                    .HasMaxLength(9)
                    .HasColumnName("ward_id")
                    .HasComment("서·센터ID");

                entity.Property(e => e.ZipCode)
                    .HasMaxLength(10)
                    .HasColumnName("zip_code")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("우편번호");

                entity.HasOne(d => d.Ward)
                    .WithMany(p => p.IsVoluns)
                    .HasForeignKey(d => d.WardId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_is_volun_ward_id");
            });

            modelBuilder.Entity<IsVolunDsp>(entity =>
            {
                entity.HasKey(e => new { e.VolunId, e.DsrSeq })
                    .HasName("PRIMARY");

                entity.ToTable("is_volun_dsp");

                entity.HasComment("의용소방대 출동정보");

                entity.Property(e => e.VolunId)
                    .HasColumnType("int(11)")
                    .HasColumnName("volun_id")
                    .HasComment("의용소방대원ID");

                entity.Property(e => e.DsrSeq)
                    .HasMaxLength(12)
                    .HasColumnName("dsr_seq")
                    .HasComment("긴급구조번호");

                entity.Property(e => e.DspEdate)
                    .HasColumnType("date")
                    .HasColumnName("dsp_edate")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("완료시간");

                entity.Property(e => e.DspSdate)
                    .HasColumnType("date")
                    .HasColumnName("dsp_sdate")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("출동시간");

                entity.Property(e => e.Remark)
                    .HasMaxLength(200)
                    .HasColumnName("remark")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("비고");

                entity.HasOne(d => d.Volun)
                    .WithMany(p => p.IsVolunDsps)
                    .HasForeignKey(d => d.VolunId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_is_volun_dsp_volun_id");
            });

            modelBuilder.Entity<IsVolunEdu>(entity =>
            {
                entity.HasKey(e => new { e.VolunId, e.VolunEduSeq })
                    .HasName("PRIMARY");

                entity.ToTable("is_volun_edu");

                entity.HasComment("의용소방대원 교육정보");

                entity.Property(e => e.VolunId)
                    .HasColumnType("int(11)")
                    .HasColumnName("volun_id")
                    .HasComment("의용소방대원ID");

                entity.Property(e => e.VolunEduSeq)
                    .HasColumnType("int(11)")
                    .HasColumnName("volun_edu_seq")
                    .HasComment("Seq");

                entity.Property(e => e.CompleteCd)
                    .HasColumnType("int(11)")
                    .HasColumnName("complete_cd")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("이수코드");

                entity.Property(e => e.CourseName)
                    .HasMaxLength(100)
                    .HasColumnName("course_name")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("과정명");

                entity.Property(e => e.EndDate)
                    .HasColumnType("date")
                    .HasColumnName("end_date")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("과정 종료일");

                entity.Property(e => e.InstitutionName)
                    .HasMaxLength(100)
                    .HasColumnName("institution_name")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("기관명");

                entity.Property(e => e.Remark)
                    .HasMaxLength(200)
                    .HasColumnName("remark")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("비고");

                entity.Property(e => e.Score)
                    .HasMaxLength(10)
                    .HasColumnName("score")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("성적");

                entity.Property(e => e.StartDate)
                    .HasColumnType("date")
                    .HasColumnName("start_date")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("과정 시작일");

                entity.HasOne(d => d.Volun)
                    .WithMany(p => p.IsVolunEdus)
                    .HasForeignKey(d => d.VolunId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_is_volun_edu_volun_id");
            });

            modelBuilder.Entity<IsWard>(entity =>
            {
                entity.HasKey(e => e.WardId)
                    .HasName("PRIMARY");

                entity.ToTable("is_ward");

                entity.HasComment("서/센터");

                entity.Property(e => e.WardId)
                    .HasMaxLength(9)
                    .HasColumnName("ward_id")
                    .HasComment("서·센터ID");

                entity.Property(e => e.Address)
                    .HasMaxLength(100)
                    .HasColumnName("address")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("주소");

                entity.Property(e => e.AutoListYn)
                    .HasMaxLength(1)
                    .HasColumnName("auto_list_yn")
                    .HasDefaultValueSql("'NULL'")
                    .IsFixedLength(true)
                    .HasComment("자동편성여부");

                entity.Property(e => e.BrdUsingYn)
                    .HasMaxLength(1)
                    .HasColumnName("brd_using_yn")
                    .HasDefaultValueSql("'NULL'")
                    .IsFixedLength(true)
                    .HasComment("방송사용중여부");

                entity.Property(e => e.DayFax)
                    .HasMaxLength(20)
                    .HasColumnName("day_fax")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("주간팩스번호");

                entity.Property(e => e.DayTel)
                    .HasMaxLength(20)
                    .HasColumnName("day_tel")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("주간전화번호");

                entity.Property(e => e.DspGroupYn)
                    .HasMaxLength(1)
                    .HasColumnName("dsp_group_yn")
                    .HasDefaultValueSql("'NULL'")
                    .IsFixedLength(true)
                    .HasComment("출동대편성여부");

                entity.Property(e => e.ExtTel)
                    .HasMaxLength(10)
                    .HasColumnName("ext_tel")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("내선번호");

                entity.Property(e => e.FireOfficeId)
                    .HasMaxLength(9)
                    .HasColumnName("fire_office_id")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("상위소방서ID");

                entity.Property(e => e.LchgDtime)
                    .HasMaxLength(20)
                    .HasColumnName("lchg_dtime")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("수정일시");

                entity.Property(e => e.LchgUserId)
                    .HasMaxLength(20)
                    .HasColumnName("lchg_user_id")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("수정자ID");

                entity.Property(e => e.NickName)
                    .HasMaxLength(50)
                    .HasColumnName("nick_name")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("약어명");

                entity.Property(e => e.NightFax)
                    .HasMaxLength(20)
                    .HasColumnName("night_fax")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("야간팩스번호");

                entity.Property(e => e.NightTel)
                    .HasMaxLength(20)
                    .HasColumnName("night_tel")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("야간전화번호");

                entity.Property(e => e.Remark)
                    .HasMaxLength(300)
                    .HasColumnName("remark")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("비고");

                entity.Property(e => e.TypeClsCd)
                    .HasColumnType("int(11)")
                    .HasColumnName("type_cls_cd")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("유형구분코드");

                entity.Property(e => e.UpwardId)
                    .HasMaxLength(9)
                    .HasColumnName("upward_id")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("상위서·센터ID");

                entity.Property(e => e.UseYn)
                    .HasMaxLength(1)
                    .HasColumnName("use_yn")
                    .HasDefaultValueSql("'''1'''")
                    .IsFixedLength(true)
                    .HasComment("사용여부");

                entity.Property(e => e.WardName)
                    .HasMaxLength(50)
                    .HasColumnName("ward_name")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("서소명");

                entity.Property(e => e.WardOrder)
                    .HasColumnType("int(4)")
                    .HasColumnName("ward_order")
                    .HasDefaultValueSql("'9999'")
                    .HasComment("서센터정렬순서");

                entity.Property(e => e.WardcloseDate)
                    .HasColumnType("date")
                    .HasColumnName("wardclose_date")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("폐서(소)일자");

                entity.Property(e => e.WardopenDate)
                    .HasColumnType("date")
                    .HasColumnName("wardopen_date")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("개서(소)일자");

                entity.Property(e => e.WardopenYn)
                    .HasMaxLength(1)
                    .HasColumnName("wardopen_yn")
                    .HasDefaultValueSql("'NULL'")
                    .IsFixedLength(true)
                    .HasComment("개소여부");

                entity.Property(e => e.X)
                    .HasMaxLength(20)
                    .HasColumnName("x")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("x");

                entity.Property(e => e.Y)
                    .HasMaxLength(20)
                    .HasColumnName("y")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("y");

                entity.Property(e => e.ZipCode)
                    .HasMaxLength(10)
                    .HasColumnName("zip_code")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("우편번호");
            });

            modelBuilder.Entity<IsWardcar>(entity =>
            {
                entity.HasKey(e => new { e.CarId, e.WardId })
                    .HasName("PRIMARY");

                entity.ToTable("is_wardcar");

                entity.HasComment("서·센터차량");

                entity.HasIndex(e => e.WardId, "FK_is_wardcar_ward_id");

                entity.Property(e => e.CarId)
                    .HasColumnType("int(11)")
                    .HasColumnName("car_id")
                    .HasComment("차량호수");

                entity.Property(e => e.WardId)
                    .HasMaxLength(9)
                    .HasColumnName("ward_id")
                    .HasComment("서·센터ID");

                entity.Property(e => e.BuyDate)
                    .HasColumnType("date")
                    .HasColumnName("buy_date")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("구입일자");

                entity.Property(e => e.CarCd)
                    .HasColumnType("int(11)")
                    .HasColumnName("car_cd")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("차종코드");

                entity.Property(e => e.CarGrpCd)
                    .HasColumnType("int(11)")
                    .HasColumnName("car_grp_cd")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("차량그룹코드");

                entity.Property(e => e.CarKind)
                    .HasMaxLength(50)
                    .HasColumnName("car_kind")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("기종");

                entity.Property(e => e.CarMobile)
                    .HasMaxLength(20)
                    .HasColumnName("car_mobile")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("차량휴대전화번호");

                entity.Property(e => e.CarName)
                    .HasMaxLength(50)
                    .HasColumnName("car_name")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("차량명");

                entity.Property(e => e.CarNo)
                    .HasMaxLength(15)
                    .HasColumnName("car_no")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("차량번호");

                entity.Property(e => e.CarStatMemo)
                    .HasMaxLength(200)
                    .HasColumnName("car_stat_memo")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("차량상태메모");

                entity.Property(e => e.CarstatCd)
                    .HasColumnType("int(11)")
                    .HasColumnName("carstat_cd")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("차량상태코드");

                entity.Property(e => e.DspAutoYn)
                    .HasMaxLength(1)
                    .HasColumnName("dsp_auto_yn")
                    .HasDefaultValueSql("'''1'''")
                    .IsFixedLength(true)
                    .HasComment("출동대편성여부");

                entity.Property(e => e.DspDsrSeq)
                    .HasMaxLength(12)
                    .HasColumnName("dsp_dsr_seq")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("최종출동긴급구조번호");

                entity.Property(e => e.DspOrder)
                    .HasColumnType("int(2)")
                    .HasColumnName("dsp_order")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("출동우선순위");

                entity.Property(e => e.EtcInfo)
                    .HasMaxLength(100)
                    .HasColumnName("etc_info")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("기타정보");

                entity.Property(e => e.LchgDtime)
                    .HasMaxLength(20)
                    .HasColumnName("lchg_dtime")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("수정일시");

                entity.Property(e => e.LchgUserId)
                    .HasMaxLength(20)
                    .HasColumnName("lchg_user_id")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("수정자ID");

                entity.Property(e => e.LdspDtime)
                    .HasMaxLength(20)
                    .HasColumnName("ldsp_dtime")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("최종출동시각");

                entity.Property(e => e.LretDtime)
                    .HasMaxLength(20)
                    .HasColumnName("lret_dtime")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("최종귀소시각");

                entity.Property(e => e.Manufacturer)
                    .HasMaxLength(50)
                    .HasColumnName("manufacturer")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("제작사");

                entity.Property(e => e.PlatoonCls)
                    .HasMaxLength(1)
                    .HasColumnName("platoon_cls")
                    .HasDefaultValueSql("'NULL'")
                    .IsFixedLength(true)
                    .HasComment("소대구분");

                entity.Property(e => e.RadioCallsign)
                    .HasMaxLength(20)
                    .HasColumnName("radio_callsign")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("무선호출명칭");

                entity.Property(e => e.RideManCnt)
                    .HasColumnType("int(5)")
                    .HasColumnName("ride_man_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("탑승인원");

                entity.Property(e => e.UseYn)
                    .HasMaxLength(1)
                    .HasColumnName("use_yn")
                    .HasDefaultValueSql("'''1'''")
                    .IsFixedLength(true)
                    .HasComment("사용여부");

                entity.HasOne(d => d.Ward)
                    .WithMany(p => p.IsWardcars)
                    .HasForeignKey(d => d.WardId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_is_wardcar_ward_id");
            });

            modelBuilder.Entity<IsWardcarhist>(entity =>
            {
                entity.HasKey(e => new { e.CarId, e.LchgDtime })
                    .HasName("PRIMARY");

                entity.ToTable("is_wardcarhist");

                entity.HasComment("서·센터차량 History");

                entity.HasIndex(e => e.WardId, "FK_is_wardcarhist_ward_id");

                entity.Property(e => e.CarId)
                    .HasColumnType("int(11)")
                    .HasColumnName("car_id")
                    .HasComment("차량호수");

                entity.Property(e => e.LchgDtime)
                    .HasMaxLength(20)
                    .HasColumnName("lchg_dtime")
                    .HasComment("수정일시");

                entity.Property(e => e.BuyDate)
                    .HasColumnType("date")
                    .HasColumnName("buy_date")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("구입일자");

                entity.Property(e => e.CarCd)
                    .HasColumnType("int(11)")
                    .HasColumnName("car_cd")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("차종코드");

                entity.Property(e => e.CarGrpCd)
                    .HasColumnType("int(11)")
                    .HasColumnName("car_grp_cd")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("차량그룹코드");

                entity.Property(e => e.CarKind)
                    .HasMaxLength(50)
                    .HasColumnName("car_kind")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("기종");

                entity.Property(e => e.CarMobile)
                    .HasMaxLength(20)
                    .HasColumnName("car_mobile")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("차량휴대전화번호");

                entity.Property(e => e.CarName)
                    .HasMaxLength(50)
                    .HasColumnName("car_name")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("차량명");

                entity.Property(e => e.CarNo)
                    .HasMaxLength(15)
                    .HasColumnName("car_no")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("차량번호");

                entity.Property(e => e.CarStatMemo)
                    .HasMaxLength(200)
                    .HasColumnName("car_stat_memo")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("차량상태메모");

                entity.Property(e => e.CarstatCd)
                    .HasColumnType("int(11)")
                    .HasColumnName("carstat_cd")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("차량상태코드");

                entity.Property(e => e.DspAutoYn)
                    .HasMaxLength(1)
                    .HasColumnName("dsp_auto_yn")
                    .HasDefaultValueSql("'''1'''")
                    .IsFixedLength(true)
                    .HasComment("출동대편성여부");

                entity.Property(e => e.DspDsrSeq)
                    .HasMaxLength(12)
                    .HasColumnName("dsp_dsr_seq")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("최종출동긴급구조번호");

                entity.Property(e => e.DspOrder)
                    .HasColumnType("int(2)")
                    .HasColumnName("dsp_order")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("출동우선순위");

                entity.Property(e => e.EtcInfo)
                    .HasMaxLength(100)
                    .HasColumnName("etc_info")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("기타정보");

                entity.Property(e => e.LchgUserId)
                    .HasMaxLength(20)
                    .HasColumnName("lchg_user_id")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("수정자ID");

                entity.Property(e => e.LdspDtime)
                    .HasMaxLength(20)
                    .HasColumnName("ldsp_dtime")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("최종출동시각");

                entity.Property(e => e.LretDtime)
                    .HasMaxLength(20)
                    .HasColumnName("lret_dtime")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("최종귀소시각");

                entity.Property(e => e.Manufacturer)
                    .HasMaxLength(50)
                    .HasColumnName("manufacturer")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("제작사");

                entity.Property(e => e.PlatoonCls)
                    .HasMaxLength(1)
                    .HasColumnName("platoon_cls")
                    .HasDefaultValueSql("'NULL'")
                    .IsFixedLength(true)
                    .HasComment("소대구분");

                entity.Property(e => e.RadioCallsign)
                    .HasMaxLength(20)
                    .HasColumnName("radio_callsign")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("무선호출명칭");

                entity.Property(e => e.RideManCnt)
                    .HasColumnType("int(5)")
                    .HasColumnName("ride_man_cnt")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("탑승인원");

                entity.Property(e => e.UseYn)
                    .HasMaxLength(1)
                    .HasColumnName("use_yn")
                    .HasDefaultValueSql("'''1'''")
                    .IsFixedLength(true)
                    .HasComment("사용여부");

                entity.Property(e => e.WardId)
                    .HasMaxLength(9)
                    .HasColumnName("ward_id")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("서·센터ID");

                entity.HasOne(d => d.Ward)
                    .WithMany(p => p.IsWardcarhists)
                    .HasForeignKey(d => d.WardId)
                    .HasConstraintName("FK_is_wardcarhist_ward_id");
            });

            modelBuilder.Entity<SmCdMaster>(entity =>
            {
                entity.HasKey(e => e.Cd)
                    .HasName("PRIMARY");

                entity.ToTable("sm_cd_master");

                entity.HasComment("코드 Master");

                entity.Property(e => e.Cd)
                    .HasColumnType("int(11)")
                    .HasColumnName("cd")
                    .HasComment("코드");

                entity.Property(e => e.CdDesc)
                    .HasMaxLength(300)
                    .HasColumnName("cd_desc")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("코드설명");

                entity.Property(e => e.UseYn)
                    .HasMaxLength(1)
                    .HasColumnName("use_yn")
                    .HasDefaultValueSql("'''1'''")
                    .IsFixedLength(true)
                    .HasComment("사용여부");
            });

            modelBuilder.Entity<SmCdResource>(entity =>
            {
                entity.HasKey(e => new { e.Cd, e.Locale })
                    .HasName("PRIMARY");

                entity.ToTable("sm_cd_resource");

                entity.Property(e => e.Cd)
                    .HasColumnType("int(11)")
                    .HasColumnName("cd")
                    .HasComment("코드");

                entity.Property(e => e.Locale)
                    .HasMaxLength(2)
                    .HasColumnName("locale")
                    .HasComment("국가코드");

                entity.Property(e => e.CdDesc)
                    .HasMaxLength(300)
                    .HasColumnName("cd_desc")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("코드설명");

                entity.Property(e => e.CdName)
                    .HasMaxLength(100)
                    .HasColumnName("cd_name")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("코드명");
            });

            modelBuilder.Entity<SmCdgroupMaster>(entity =>
            {
                entity.HasKey(e => e.CdGrp)
                    .HasName("PRIMARY");

                entity.ToTable("sm_cdgroup_master");

                entity.HasComment("그룹코드 master");

                entity.Property(e => e.CdGrp)
                    .HasMaxLength(20)
                    .HasColumnName("cd_grp")
                    .HasComment("코드그룹");

                entity.Property(e => e.CdGrpDesc)
                    .HasMaxLength(100)
                    .HasColumnName("cd_grp_desc")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("코드그룹설명");

                entity.Property(e => e.CdGrpStep)
                    .HasColumnType("int(11)")
                    .HasColumnName("cd_grp_step")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("코드그룹단계");

                entity.Property(e => e.LeafYn)
                    .HasMaxLength(1)
                    .HasColumnName("leaf_yn")
                    .HasDefaultValueSql("'NULL'")
                    .IsFixedLength(true)
                    .HasComment("leaf 여부");

                entity.Property(e => e.SortOrder)
                    .HasColumnType("int(11)")
                    .HasColumnName("sort_order")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("정렬순서");

                entity.Property(e => e.UpperCdGrp)
                    .HasMaxLength(20)
                    .HasColumnName("upper_cd_grp")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("상위코드그룹");

                entity.Property(e => e.UseYn)
                    .HasMaxLength(1)
                    .HasColumnName("use_yn")
                    .HasDefaultValueSql("'''1'''")
                    .IsFixedLength(true)
                    .HasComment("사용여부");
            });

            modelBuilder.Entity<SmCdgroupResource>(entity =>
            {
                entity.HasKey(e => new { e.CdGrp, e.Locale })
                    .HasName("PRIMARY");

                entity.ToTable("sm_cdgroup_resource");

                entity.Property(e => e.CdGrp)
                    .HasMaxLength(20)
                    .HasColumnName("cd_grp")
                    .HasComment("코드그룹");

                entity.Property(e => e.Locale)
                    .HasMaxLength(2)
                    .HasColumnName("locale")
                    .HasComment("국가코드");

                entity.Property(e => e.CdGrpDesc)
                    .HasMaxLength(1000)
                    .HasColumnName("cd_grp_desc")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("코드그룹설명");

                entity.Property(e => e.CdGrpName)
                    .HasMaxLength(100)
                    .HasColumnName("cd_grp_name")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("코드그룹명");
            });

            modelBuilder.Entity<SmCdgroupcd>(entity =>
            {
                entity.HasKey(e => new { e.CdGrp, e.Cd })
                    .HasName("PRIMARY");

                entity.ToTable("sm_cdgroupcd");

                entity.HasComment("코드그룹매핑");

                entity.Property(e => e.CdGrp)
                    .HasMaxLength(20)
                    .HasColumnName("cd_grp")
                    .HasComment("코드그룹");

                entity.Property(e => e.Cd)
                    .HasColumnType("int(11)")
                    .HasColumnName("cd")
                    .HasComment("코드");

                entity.Property(e => e.SortOrder)
                    .HasColumnType("int(11)")
                    .HasColumnName("sort_order")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("정렬순서");

                entity.Property(e => e.UseYn)
                    .HasMaxLength(1)
                    .HasColumnName("use_yn")
                    .HasDefaultValueSql("'''1'''")
                    .IsFixedLength(true)
                    .HasComment("사용여부");
            });

            modelBuilder.Entity<SmMenu>(entity =>
            {
                entity.HasKey(e => e.MenuId)
                    .HasName("PRIMARY");

                entity.ToTable("sm_menu");

                entity.HasComment("메뉴");

                entity.Property(e => e.MenuId)
                    .HasMaxLength(5)
                    .HasColumnName("menu_id")
                    .HasComment("메뉴 id");

                entity.Property(e => e.MenuName)
                    .HasMaxLength(50)
                    .HasColumnName("menu_name")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("메뉴명");

                entity.Property(e => e.NouseRsn)
                    .HasMaxLength(100)
                    .HasColumnName("nouse_rsn")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("미사용이유");

                entity.Property(e => e.UseYn)
                    .HasMaxLength(1)
                    .HasColumnName("use_yn")
                    .HasDefaultValueSql("'''1'''")
                    .IsFixedLength(true)
                    .HasComment("사용여부");
            });

            modelBuilder.Entity<SmMenupriv>(entity =>
            {
                entity.HasKey(e => new { e.MenuPrivSeq, e.PrivId, e.MenuId })
                    .HasName("PRIMARY");

                entity.ToTable("sm_menupriv");

                entity.HasComment("권한/메뉴 매핑 및 권한 수준 설정");

                entity.HasIndex(e => e.MenuId, "sm_menu_priv_FK");

                entity.HasIndex(e => e.PrivId, "sm_menu_priv_FK_1");

                entity.Property(e => e.MenuPrivSeq)
                    .HasColumnType("int(11)")
                    .HasColumnName("menu_priv_seq")
                    .HasComment("일렬번호");

                entity.Property(e => e.PrivId)
                    .HasMaxLength(5)
                    .HasColumnName("priv_id")
                    .HasComment("권한 id");

                entity.Property(e => e.MenuId)
                    .HasMaxLength(5)
                    .HasColumnName("menu_id")
                    .HasComment("메뉴 id");

                entity.Property(e => e.LchgDtime)
                    .HasMaxLength(20)
                    .HasColumnName("lchg_dtime")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("수정일시");

                entity.Property(e => e.LchgUserId)
                    .HasMaxLength(20)
                    .HasColumnName("lchg_user_id")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("수정자ID");

                entity.Property(e => e.Level)
                    .HasColumnType("int(1)")
                    .HasColumnName("level")
                    .HasComment("권한수준(1:Read only, 2:Read & Write)");
            });

            modelBuilder.Entity<SmNotification>(entity =>
            {
                entity.HasKey(e => e.NotificationId)
                    .HasName("PRIMARY");

                entity.ToTable("sm_notification");

                entity.HasComment("승인요청");

                entity.HasIndex(e => e.UserId, "FK_sm_notification_user_id");

                entity.Property(e => e.NotificationId)
                    .HasColumnType("int(11)")
                    .HasColumnName("notification_id")
                    .HasComment("ID");

                entity.Property(e => e.CompletedDate)
                    .HasMaxLength(20)
                    .HasColumnName("completed_date")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("완료일");

                entity.Property(e => e.CompletedYn)
                    .HasMaxLength(1)
                    .HasColumnName("completed_yn")
                    .HasDefaultValueSql("'''0'''")
                    .IsFixedLength(true)
                    .HasComment("완료여부");

                entity.Property(e => e.CreatedDate)
                    .HasMaxLength(20)
                    .HasColumnName("created_date")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("생성일");

                entity.Property(e => e.Note)
                    .HasMaxLength(200)
                    .HasColumnName("note")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("메모");

                entity.Property(e => e.NotificationTypeCd)
                    .HasColumnType("int(11)")
                    .HasColumnName("notification_type_cd")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("통보유형");

                entity.Property(e => e.RelatedSeq)
                    .HasColumnType("int(11)")
                    .HasColumnName("related_seq")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("승인요청 항목 일렬번호");

                entity.Property(e => e.StateCd)
                    .HasColumnType("int(11)")
                    .HasColumnName("state_cd")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("상태");

                entity.Property(e => e.UserId)
                    .IsRequired()
                    .HasMaxLength(20)
                    .HasColumnName("user_id")
                    .HasComment("사용자id");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.SmNotifications)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_sm_notification_user_id");
            });

            modelBuilder.Entity<SmPriv>(entity =>
            {
                entity.HasKey(e => e.PrivId)
                    .HasName("PRIMARY");

                entity.ToTable("sm_priv");

                entity.HasComment("권한");

                entity.Property(e => e.PrivId)
                    .HasMaxLength(5)
                    .HasColumnName("priv_id")
                    .HasComment("권한 id");

                entity.Property(e => e.LchgDtime)
                    .HasMaxLength(20)
                    .HasColumnName("lchg_dtime")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("수정일시");

                entity.Property(e => e.LchgUserId)
                    .HasMaxLength(20)
                    .HasColumnName("lchg_user_id")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("수정자ID");

                entity.Property(e => e.PrivName)
                    .HasMaxLength(50)
                    .HasColumnName("priv_name")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("권한명");
            });

            modelBuilder.Entity<SmSysenv>(entity =>
            {
                entity.ToTable("sm_sysenv");

                entity.HasComment("시스템환경설정");

                entity.Property(e => e.Id)
                    .HasMaxLength(3)
                    .HasColumnName("id")
                    .IsFixedLength(true)
                    .HasComment("ID");

                entity.Property(e => e.Desc)
                    .HasMaxLength(100)
                    .HasColumnName("desc")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("설명");

                entity.Property(e => e.LchgDtime)
                    .HasMaxLength(20)
                    .HasColumnName("lchg_dtime")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("수정자ID");

                entity.Property(e => e.LchgUserId)
                    .HasMaxLength(20)
                    .HasColumnName("lchg_user_id")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("수정일시");

                entity.Property(e => e.Name)
                    .HasMaxLength(30)
                    .HasColumnName("name")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("설정명");

                entity.Property(e => e.RegDate)
                    .HasMaxLength(20)
                    .HasColumnName("reg_date")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("등록일");

                entity.Property(e => e.UseYn)
                    .HasMaxLength(1)
                    .HasColumnName("use_yn")
                    .HasDefaultValueSql("'''1'''")
                    .IsFixedLength(true)
                    .HasComment("사용여부");

                entity.Property(e => e.Values)
                    .HasColumnType("longtext")
                    .HasColumnName("values")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("설정값");
            });

            modelBuilder.Entity<SmUserPreference>(entity =>
            {
                entity.HasKey(e => e.UserId)
                    .HasName("PRIMARY");

                entity.ToTable("sm_user_preference");

                entity.Property(e => e.UserId)
                    .HasMaxLength(20)
                    .HasColumnName("user_id")
                    .HasComment("사용자 id");

                entity.Property(e => e.Color)
                    .HasMaxLength(20)
                    .HasColumnName("color")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("Color");

                entity.Property(e => e.Language)
                    .HasMaxLength(2)
                    .HasColumnName("language")
                    .HasDefaultValueSql("'NULL'")
                    .IsFixedLength(true)
                    .HasComment("언어");

                entity.Property(e => e.TableDense)
                    .HasMaxLength(1)
                    .HasColumnName("table_dense")
                    .HasDefaultValueSql("'NULL'")
                    .IsFixedLength(true)
                    .HasComment("테이블 Row 간격 좁게 세팅");

                entity.Property(e => e.TableRows)
                    .HasColumnType("int(11)")
                    .HasColumnName("table_rows")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("테이블 Row 수");

                entity.Property(e => e.Theme)
                    .HasMaxLength(20)
                    .HasColumnName("theme")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("테마");
            });

            modelBuilder.Entity<SmUserloghist>(entity =>
            {
                entity.HasKey(e => e.LogSeq)
                    .HasName("PRIMARY");

                entity.ToTable("sm_userloghist");

                entity.HasComment("사용자로그인아웃이력");

                entity.HasIndex(e => e.UserId, "FK_sm_userloghist_user_id");

                entity.Property(e => e.LogSeq)
                    .HasColumnType("int(11)")
                    .HasColumnName("log_seq")
                    .HasComment("로그일련번호");

                entity.Property(e => e.LogClsCd)
                    .HasColumnType("int(11)")
                    .HasColumnName("log_cls_cd")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("로그인/아웃 코드");

                entity.Property(e => e.LogDtime)
                    .HasColumnType("date")
                    .HasColumnName("log_dtime")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("로그인/아웃 시간");

                entity.Property(e => e.UserId)
                    .HasMaxLength(20)
                    .HasColumnName("user_id")
                    .HasDefaultValueSql("'NULL'")
                    .HasComment("사용자 id");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.SmUserloghists)
                    .HasForeignKey(d => d.UserId)
                    .HasConstraintName("FK_sm_userloghist_user_id");
            });

            modelBuilder.Entity<SmUserpriv>(entity =>
            {
                entity.HasKey(e => new { e.UserprivSeq, e.UserId, e.PrivId })
                    .HasName("PRIMARY");

                entity.ToTable("sm_userpriv");

                entity.HasComment("사용자별 권한");

                entity.HasIndex(e => e.PrivId, "FK_sm_userpriv_priv_id");

                entity.HasIndex(e => e.UserId, "FK_sm_userpriv_user_id");

                entity.Property(e => e.UserprivSeq)
                    .HasColumnType("int(11)")
                    .HasColumnName("userpriv_seq")
                    .HasComment("일련번호");

                entity.Property(e => e.UserId)
                    .HasMaxLength(20)
                    .HasColumnName("user_id")
                    .HasComment("사용자 id");

                entity.Property(e => e.PrivId)
                    .HasMaxLength(5)
                    .HasColumnName("priv_id")
                    .HasComment("권한 id");

                entity.Property(e => e.ApprovedYn)
                    .HasMaxLength(1)
                    .HasColumnName("approved_yn")
                    .HasDefaultValueSql("'''0'''")
                    .IsFixedLength(true)
                    .HasComment("승인여부");

                entity.HasOne(d => d.Priv)
                    .WithMany(p => p.SmUserprivs)
                    .HasForeignKey(d => d.PrivId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_sm_userpriv_priv_id");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.SmUserprivs)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_sm_userpriv_user_id");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
