﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BD_Safer.model
{
    public partial class IsVolunDsp
    {
        public int VolunId { get; set; }
        public string DsrSeq { get; set; }
        public DateTime? DspSdate { get; set; }
        public DateTime? DspEdate { get; set; }
        public string Remark { get; set; }

        public virtual IsVolun Volun { get; set; }
    }
}
