﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BD_Safer.model
{
    public partial class IsObj
    {
        public IsObj()
        {
            IsBldgs = new HashSet<IsBldg>();
            IsDgrmgrs = new HashSet<IsDgrmgr>();
            IsFmgrs = new HashSet<IsFmgr>();
            IsObjdrws = new HashSet<IsObjdrw>();
            IsObjpics = new HashSet<IsObjpic>();
            IsObjrels = new HashSet<IsObjrel>();
        }

        public int ObjId { get; set; }
        public string WardId { get; set; }
        public string ObjManNum { get; set; }
        public int? UsedCd { get; set; }
        public int? ObjCd { get; set; }
        public string ConstName { get; set; }
        public string RpsnFirmName { get; set; }
        public string ZipCode { get; set; }
        public string Address { get; set; }
        public string UseYn { get; set; }
        public int? ObjStdCd { get; set; }
        public string BldgNm { get; set; }
        public string X { get; set; }
        public string Y { get; set; }
        public string WeakobjYn { get; set; }
        public string SelfobjYn { get; set; }
        public string AdgInfo { get; set; }
        public string DayTel { get; set; }
        public string NightTel { get; set; }
        public string PrevdstTel { get; set; }
        public DateTime? UseConfmDate { get; set; }
        public string InstitYn { get; set; }
        public int? InstitClsCd { get; set; }
        public string PubfaclYn { get; set; }
        public int? WeakobjCd { get; set; }
        public string SubUseDesc { get; set; }
        public int? TunnelClsCd { get; set; }
        public decimal? TunnelLength { get; set; }

        public virtual IsWard Ward { get; set; }
        public virtual IsObjplan IsObjplan { get; set; }
        public virtual ICollection<IsBldg> IsBldgs { get; set; }
        public virtual ICollection<IsDgrmgr> IsDgrmgrs { get; set; }
        public virtual ICollection<IsFmgr> IsFmgrs { get; set; }
        public virtual ICollection<IsObjdrw> IsObjdrws { get; set; }
        public virtual ICollection<IsObjpic> IsObjpics { get; set; }
        public virtual ICollection<IsObjrel> IsObjrels { get; set; }
    }
}
