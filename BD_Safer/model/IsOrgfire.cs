﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BD_Safer.model
{
    public partial class IsOrgfire
    {
        public int OrgId { get; set; }
        public int FightClsCd { get; set; }
        public int? MobFightCnt { get; set; }
        public string LchgDtime { get; set; }
        public string LchgUserId { get; set; }
        public string UseYn { get; set; }

        public virtual IsOrg Org { get; set; }
    }
}
