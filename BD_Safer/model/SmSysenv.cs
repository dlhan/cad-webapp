﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BD_Safer.model
{
    public partial class SmSysenv
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Desc { get; set; }
        public string Values { get; set; }
        public string RegDate { get; set; }
        public string LchgUserId { get; set; }
        public string LchgDtime { get; set; }
        public string UseYn { get; set; }
    }
}
