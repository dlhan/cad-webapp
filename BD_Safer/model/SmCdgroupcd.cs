﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BD_Safer.model
{
    public partial class SmCdgroupcd
    {
        public string CdGrp { get; set; }
        public int Cd { get; set; }
        public int? SortOrder { get; set; }
        public string UseYn { get; set; }
    }
}
