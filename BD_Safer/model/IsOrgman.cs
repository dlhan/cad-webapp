﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BD_Safer.model
{
    public partial class IsOrgman
    {
        public int OrgId { get; set; }
        public int SkillLicnCd { get; set; }
        public int? MobManCnt { get; set; }
        public string LchgDtime { get; set; }
        public string LchgUserId { get; set; }
        public string UseYn { get; set; }

        public virtual IsOrg Org { get; set; }
    }
}
