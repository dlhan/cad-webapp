﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BD_Safer.model
{
    public partial class SmCdgroupMaster
    {
        public string CdGrp { get; set; }
        public string UpperCdGrp { get; set; }
        public int? CdGrpStep { get; set; }
        public string CdGrpDesc { get; set; }
        public string UseYn { get; set; }
        public int? SortOrder { get; set; }
        public string LeafYn { get; set; }
    }
}
