﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BD_Safer.model
{
    public partial class SmCdgroupResource
    {
        public string CdGrp { get; set; }
        public string CdGrpName { get; set; }
        public string CdGrpDesc { get; set; }
        public string Locale { get; set; }
    }
}
