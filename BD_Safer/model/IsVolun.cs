﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BD_Safer.model
{
    public partial class IsVolun
    {
        public IsVolun()
        {
            IsVolunDsps = new HashSet<IsVolunDsp>();
            IsVolunEdus = new HashSet<IsVolunEdu>();
        }

        public int VolunId { get; set; }
        public string WardId { get; set; }
        public string VolunmanName { get; set; }
        public string PostName { get; set; }
        public string PostpartName { get; set; }
        public int? TitleCd { get; set; }
        public string ZipCode { get; set; }
        public string Address { get; set; }
        public DateTime? EnlistDate { get; set; }
        public DateTime? BirthDate { get; set; }
        public DateTime? ClassDate { get; set; }
        public string ContactTel { get; set; }
        public string Cell { get; set; }
        public string EduCompDesc { get; set; }
        public string HoldLicn { get; set; }
        public string Remark { get; set; }
        public string LchgDtime { get; set; }
        public string LchgUserId { get; set; }
        public string UseYn { get; set; }

        public virtual IsWard Ward { get; set; }
        public virtual ICollection<IsVolunDsp> IsVolunDsps { get; set; }
        public virtual ICollection<IsVolunEdu> IsVolunEdus { get; set; }
    }
}
