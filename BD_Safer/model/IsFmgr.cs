﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BD_Safer.model
{
    public partial class IsFmgr
    {
        public int FmgrSeq { get; set; }
        public int ObjId { get; set; }
        public string FirstName { get; set; }
        public DateTime? Birthday { get; set; }
        public string ZipCode { get; set; }
        public string Address { get; set; }
        public string TelNum { get; set; }
        public DateTime? AssignDate { get; set; }
        public DateTime? DismissDate { get; set; }
        public string UseYn { get; set; }
        public string CitizenNo { get; set; }
        public string PubFmngYn { get; set; }
        public string DismissRemark { get; set; }
        public string Title { get; set; }
        public string MgeAgencyYn { get; set; }
        public string LastName { get; set; }

        public virtual IsObj Obj { get; set; }
    }
}
