﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BD_Safer.model
{
    public partial class SmNotification
    {
        public int NotificationId { get; set; }
        public int? NotificationTypeCd { get; set; }
        public int? RelatedSeq { get; set; }
        public int? StateCd { get; set; }
        public string CreatedDate { get; set; }
        public string CompletedDate { get; set; }
        public string CompletedYn { get; set; }
        public string UserId { get; set; }
        public string Note { get; set; }

        public virtual HrUser User { get; set; }
    }
}
