﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BD_Safer.model
{
    public partial class HrUserFireTrain
    {
        public int FireTrainSeq { get; set; }
        public string UserId { get; set; }
        public string TrainName { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string Host { get; set; }
        public string Perf { get; set; }
        public string OrgName { get; set; }

        public virtual HrUser User { get; set; }
    }
}
