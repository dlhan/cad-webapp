﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BD_Safer.model
{
    public partial class IsBldg
    {
        public IsBldg()
        {
            IsObjhists = new HashSet<IsObjhist>();
            IsStories = new HashSet<IsStory>();
            IsStorySpecUseds = new HashSet<IsStorySpecUsed>();
        }

        public int BldgSeq { get; set; }
        public int ObjId { get; set; }
        public string BldgName { get; set; }
        public int? Struct1 { get; set; }
        public int? Struct2 { get; set; }
        public int? Struct3 { get; set; }
        public int? UstoryCnt { get; set; }
        public int? BstoryCnt { get; set; }
        public decimal? FloorArea { get; set; }
        public decimal? TotArea { get; set; }
        public int? SescStairCnt { get; set; }
        public decimal? LotArea { get; set; }
        public int? EscStairCnt { get; set; }
        public int? ComStairCnt { get; set; }
        public int? OutStairCnt { get; set; }
        public int? InclineCnt { get; set; }
        public int? EmgliftCnt { get; set; }
        public int? EscalCnt { get; set; }
        public int? ExitCnt { get; set; }
        public string RootYn { get; set; }
        public int? HomeCnt { get; set; }
        public string UseYn { get; set; }
        public string RpsnBldgYn { get; set; }
        public int? HouseCnt { get; set; }
        public int? EntrpsCnt { get; set; }
        public int? InhbtntCnt { get; set; }
        public decimal? BldgHeight { get; set; }
        public int? ElvtrCnt { get; set; }
        public DateTime? RemoveDate { get; set; }
        public string RemoveYn { get; set; }
        public int? BldgMainUseCd { get; set; }
        public int? BldgSubUseCd { get; set; }
        public DateTime? UseConfmDate { get; set; }

        public virtual IsObj Obj { get; set; }
        public virtual ICollection<IsObjhist> IsObjhists { get; set; }
        public virtual ICollection<IsStory> IsStories { get; set; }
        public virtual ICollection<IsStorySpecUsed> IsStorySpecUseds { get; set; }
    }
}
