﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BD_Safer.model
{
    public partial class SmCdResource
    {
        public int Cd { get; set; }
        public string CdName { get; set; }
        public string CdDesc { get; set; }
        public string Locale { get; set; }
    }
}
