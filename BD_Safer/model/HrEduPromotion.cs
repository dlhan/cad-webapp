﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BD_Safer.model
{
    public partial class HrEduPromotion
    {
        public int EducationPromotionSeq { get; set; }
        public string UserId { get; set; }
        public string CourseName { get; set; }
        public string InstitutionName { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? EducationClsCd { get; set; }
        public int? CompleteCd { get; set; }
        public string Score { get; set; }

        public virtual HrUser User { get; set; }
    }
}
