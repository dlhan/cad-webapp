﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BD_Safer.model
{
    public partial class IsJurisward
    {
        public string WardId { get; set; }
        public string ZipCode { get; set; }
        public string JurisDivideYn { get; set; }
        public string BasicJurisYn { get; set; }

        public virtual IsWard Ward { get; set; }
    }
}
