﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BD_Safer.model
{
    public partial class SmMenu
    {
        public string MenuId { get; set; }
        public string MenuName { get; set; }
        public string UseYn { get; set; }
        public string NouseRsn { get; set; }
    }
}
