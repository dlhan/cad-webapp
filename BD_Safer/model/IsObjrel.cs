﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BD_Safer.model
{
    public partial class IsObjrel
    {
        public int ObjId { get; set; }
        public int RelSeq { get; set; }
        public int? RelCd { get; set; }
        public string FirstName { get; set; }
        public DateTime? Birthday { get; set; }
        public string ZipCode { get; set; }
        public string Address { get; set; }
        public string TelNum { get; set; }
        public string UseYn { get; set; }
        public string JobDesc { get; set; }
        public string CitizenNo { get; set; }
        public string LastName { get; set; }

        public virtual IsObj Obj { get; set; }
    }
}
