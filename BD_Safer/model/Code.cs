﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BD_Safer.model
{
    public partial class Code
    {
        public int? Cd { get; set; }
        public string Kr { get; set; }
        public string En { get; set; }
        public int? Code1 { get; set; }
    }
}
