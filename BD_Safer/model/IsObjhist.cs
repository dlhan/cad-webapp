﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BD_Safer.model
{
    public partial class IsObjhist
    {
        public int ObjhistSeq { get; set; }
        public int ObjId { get; set; }
        public int BldgSeq { get; set; }
        public int? HistTypeCd { get; set; }
        public string ConstUse { get; set; }
        public int? Struct1 { get; set; }
        public int? Struct2 { get; set; }
        public int? Struct3 { get; set; }
        public decimal? FloorArea { get; set; }
        public decimal? TotArea { get; set; }
        public int? UstoryCnt { get; set; }
        public int? BstoryCnt { get; set; }
        public DateTime? GrantDate { get; set; }
        public string PreuseYn { get; set; }
        public DateTime? FinishDate { get; set; }
        public string Etc { get; set; }
        public string UseYn { get; set; }
        public string ChgDesc { get; set; }

        public virtual IsBldg IsBldg { get; set; }
    }
}
