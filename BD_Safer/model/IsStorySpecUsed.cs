﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BD_Safer.model
{
    public partial class IsStorySpecUsed
    {
        public int ObjId { get; set; }
        public int BldgSeq { get; set; }
        public int StorySeq { get; set; }
        public int SpecUseSeq { get; set; }
        public int? SpecUseClsCd { get; set; }
        public decimal? Area { get; set; }
        public string Remark { get; set; }
        public string UseYn { get; set; }

        public virtual IsBldg IsBldg { get; set; }
    }
}
