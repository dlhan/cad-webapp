﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BD_Safer.model
{
    public partial class IsOrg
    {
        public IsOrg()
        {
            IsOrgfires = new HashSet<IsOrgfire>();
            IsOrgmen = new HashSet<IsOrgman>();
            IsTypeorgs = new HashSet<IsTypeorg>();
        }

        public int OrgId { get; set; }
        public string WardId { get; set; }
        public string OrgName { get; set; }
        public int? OrgClsCd { get; set; }
        public string ZipCode { get; set; }
        public string Address { get; set; }
        public string DayTel { get; set; }
        public string NightTel { get; set; }
        public string DayFax { get; set; }
        public string NightFax { get; set; }
        public string UpOrgName { get; set; }
        public string AgreeYn { get; set; }
        public string DsrSprtOrgYn { get; set; }
        public string DsrOrgYn { get; set; }
        public string TouchPadYn { get; set; }
        public string Remark { get; set; }
        public string Etc { get; set; }
        public string LchgDtime { get; set; }
        public string LchgUserId { get; set; }
        public string UseYn { get; set; }
        public string Cell { get; set; }
        public string CellOwnerName { get; set; }
        public string UndgrdFg { get; set; }

        public virtual IsWard Ward { get; set; }
        public virtual ICollection<IsOrgfire> IsOrgfires { get; set; }
        public virtual ICollection<IsOrgman> IsOrgmen { get; set; }
        public virtual ICollection<IsTypeorg> IsTypeorgs { get; set; }
    }
}
