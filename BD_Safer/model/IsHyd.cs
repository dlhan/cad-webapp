﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BD_Safer.model
{
    public partial class IsHyd
    {
        public int HydId { get; set; }
        public string WardId { get; set; }
        public int? FormCd { get; set; }
        public string HydWtrNo { get; set; }
        public string ZipCode { get; set; }
        public string Address { get; set; }
        public int? HydkndCd { get; set; }
        public string SignpostYn { get; set; }
        public string Remark { get; set; }
        public string AdjBldg { get; set; }
        public DateTime? RemoveDate { get; set; }
        public string LchgDtime { get; set; }
        public string LchgUserId { get; set; }
        public string UseYn { get; set; }
        public string X { get; set; }
        public string Y { get; set; }

        public virtual IsWard Ward { get; set; }
    }
}
