using System;
using System.Collections.Generic;
using BD_Safer.model;

namespace BD_Safer.model.dto 
{
    public class SearchWard
    {
        public Page page { get; set; }
        public string UpwardId { get; set; }
        public string WardId { get; set; }
        public int? TypeClsCd { get; set; }
    }

    public class SearchWardResult
    {
        public Page page { get; set; }
        public virtual IEnumerable<SearchWardInfo> Rows { get; set; } 
    }

    public class SearchWardInfo
    {
        public string WardId { get; set; }
        public string UpwardName { get; set; }
        public string WardName { get; set; }
        public string TypeCode { get; set; }
        public string NickName { get; set; }
        public string DayTel { get; set; }
        public string NightTel { get; set; }
        public string Address { get; set; }
        public string BrdUsingYn { get; set; }
    }

    public class WardInfo
    {
        public string WardId { get; set; }
        public string UpwardId { get; set; }
        public string FireOfficeId { get; set; }
        public string WardName { get; set; }
        public int? TypeClsCd { get; set; }
        public string TypeClsName { get; set; }
        public string NickName { get; set; }
        public string ZipCode { get; set; }
        public string Address { get; set; }
        public string DspGroupYn { get; set; }
        public string AutoListYn { get; set; }
        public string DayTel { get; set; }
        public string NightTel { get; set; }
        public string ExtTel { get; set; }
        public DateTime? WardopenDate { get; set; }
        public DateTime? WardcloseDate { get; set; }
        public string WardopenYn { get; set; }
        public string BrdUsingYn { get; set; }
        public string Remark { get; set; }
        public string LchgDtime { get; set; }
        public string LchgUserId { get; set; }
        public int? WardOrder { get; set; }
        public string X { get; set; }
        public string Y { get; set; }
        public string DayFax { get; set; }
        public string NightFax { get; set; }
        public string UseYn { get; set; }
    }

    public class SearchObject
    {
        public Page page { get; set; }
        public string WardId { get; set; }
        public string ConstName { get; set; }
        public string ObjManNum { get; set; }
        public int? ObjCd { get; set; }
        public int? UsedCd { get; set; }
        public string Address { get; set; }
    }

    public class SearchObjectResult
    {
        public Page page { get; set; }
        public virtual IEnumerable<SearchObjectInfo> Rows { get; set; } 
    }

    public class SearchObjectInfo
    {
        public int ObjId { get; set; }
        public string UpwardName { get; set; }
        public string WardName { get; set; }
        public string ObjManNum { get; set; }
        public string ConstName { get; set; }
        public string BldgNm { get; set; }
        public string Address { get; set; }
        public string ObjCdName { get; set; }
        public string UsedCdName { get; set; }
    }
    public class ObjectInfo
    {
        public int ObjId { get; set; }
        public string UpwardId { get; set; }
        public string WardId { get; set; }
        public string ObjManNum { get; set; }
        public int? UsedCd { get; set; }
        public int? ObjCd { get; set; }
        public string ConstName { get; set; }
        public string RpsnFirmName { get; set; }
        public string ZipCode { get; set; }
        public string Address { get; set; }
        public string UseYn { get; set; }
        public int? ObjStdCd { get; set; }
        public string BldgNm { get; set; }
        public string X { get; set; }
        public string Y { get; set; }
        public string WeakobjYn { get; set; }
        public string SelfobjYn { get; set; }
        public string AdgInfo { get; set; }
        public string DayTel { get; set; }
        public string NightTel { get; set; }
        public string PrevdstTel { get; set; }
        public DateTime? UseConfmDate { get; set; }
        public string InstitYn { get; set; }
        public int? InstitClsCd { get; set; }
        public string PubfaclYn { get; set; }
        public int? WeakobjCd { get; set; }
        public string SubUseDesc { get; set; }
        public int? TunnelClsCd { get; set; }
        public decimal? TunnelLength { get; set; }
    }

    public class BuildingList
    {
        public int BldgSeq { get; set; }
        public int ObjId { get; set; }
        public string BldgName { get; set; }
        public string Struct1 { get; set; }
        public string Struct2 { get; set; }
        public string Struct3 { get; set; }
        public string MainUse { get; set; }
        public string SubUse { get; set; }
        public int? UstoryCnt { get; set; }
        public int? BstoryCnt { get; set; }
        public decimal? LotArea { get; set; }
        public decimal? FloorArea { get; set; }
        public decimal? TotArea { get; set; }

    }

    public class ObjectElement
    {
        public int ObjId { get; set; }
        public int Seq { get; set; }
    }

    public class StoryPrimaryElement
    {
        public int ObjId { get; set; }
        public int BldgSeq { get; set; }
        public int StorySeq { get; set; }
    }

    public class StorySpecUsedList
    {
        public int ObjId { get; set; }
        public int BldgSeq { get; set; }
        public int StorySeq { get; set; }
        public int SpecUseSeq { get; set; }
        public string StoryNo { get; set; }
        public string SpecUseClsCdName { get; set; }
        public decimal? Area { get; set; }
        public string Remark { get; set; }

    }

    public class StorySpecUsedInfo
    {
        public int ObjId { get; set; }
        public int BldgSeq { get; set; }
        public int StorySeq { get; set; }
        public int SpecUseSeq { get; set; }
        public int? SpecUseClsCd { get; set; }
        public string SpecUseClsCdName { get; set; }
        public decimal? Area { get; set; }
        public string Remark { get; set; }
        public string UseYn { get; set; }
    }

    public class StorySpecUsedElement
    {
        public int ObjId { get; set; }
        public int BldgSeq { get; set; }
        public int StorySeq { get; set; }
        public int SpecUseSeq { get; set; }
    }

    public class SearchFireEquipment
    {
        public int ObjId { get; set; }
        public int? BldgSeq { get; set; }
        public int? StorySeq { get; set; }
    }

    public class FireEquipList
    {
        public int ObjId { get; set; }
        public int BldgSeq { get; set; }
        public int StorySeq { get; set; }
        public string StoryNo { get; set; }
        public string BldgName { get; set; }
        public int? InExtingCnt { get; set; }
        public int? OutExtingCnt { get; set; }
        public int? ExtingPumpCnt { get; set; }
        public int? SprinklerHCnt { get; set; }
        public int? SprinklerAvCnt { get; set; }
        public int? SprayExtingHCnt { get; set; }
        public int? SprayExtingAvCnt { get; set; }
        public int? PoExtingHCnt { get; set; }
        public int? PoExtingAvCnt { get; set; }
        public int? CarbonDioxHCnt { get; set; }
        public int? CarbonDioxAvCnt { get; set; }
        public int? HalogenCompHCnt { get; set; }
        public int? HarlogenCompAvCnt { get; set; }
        public int? PowderExtingHCnt { get; set; }
        public int? PowderExtingAvCnt { get; set; }
        public int? SlideCnt { get; set; }
        public int? LadderCnt { get; set; }
        public int? RescueCnt { get; set; }
        public int? DescSlowDeviceCnt { get; set; }
        public int? MeasureEquipCnt { get; set; }
        public int? MeasureRopeCnt { get; set; }
        public int? SafeMatCnt { get; set; }
        public int? RescueEquipCnt { get; set; }
        public int? EmgLightCnt { get; set; }
        public int? WaterExtingCnt { get; set; }
        public int? ExtingWaterCnt { get; set; }
        public int? LwtrCnt { get; set; }
        public int? WtrpipeCnt { get; set; }
        public int? WaterSpringklingCnt { get; set; }
        public int? EmgPlugCnt { get; set; }
        public int? WirelessCommCnt { get; set; }
        public string UseYn { get; set; }
        public int? ExtingCnt { get; set; }
        public int? SimplctyExtingCnt { get; set; }
        public int? EmgWaringCnt { get; set; }
        public int? EmgBrodcCnt { get; set; }
        public int? LkgeWaringCnt { get; set; }
        public int? AutoFireFindSensCnt { get; set; }
        public int? AutoFireFindCircuitCnt { get; set; }
        public int? AutoFireNewsfCnt { get; set; }
        public int? GasLkgeWaringCnt { get; set; }
        public int? InduceLightCnt { get; set; }
        public int? InduceSignpostCnt { get; set; }
        public int? HydEquipEtcCnt { get; set; }
        public int? ResmokeCnt { get; set; }
        public int? CurtainCnt { get; set; }
        public int? CarssetteCnt { get; set; }
        public int? ResistEtcCnt { get; set; }
    }

    public class ObjectHistoryList
    {
        public int ObjhistSeq { get; set; }
        public int ObjId { get; set; }
        public string BldgName { get; set; }
        public string HistTypeCdName { get; set; }
        public string ConstUse { get; set; }
        public string Struct1Name { get; set; }
        public string Struct2Name { get; set; }
        public string Struct3Name { get; set; }
    }

    public class FireManagerList
    {
        public int FmgrSeq { get; set; }
        public int ObjId { get; set; }
        public string Name { get; set; }
        public DateTime? Birthday { get; set; }
        public string Title { get; set; }
        public string Address { get; set; }
        public string TelNum { get; set; }
        public string PubFmngYn { get; set; }
        public string MgeAgencyYn { get; set; }
    }

    public class SafeManagerList
    {
        public int DgrmgrSeq { get; set; }
        public int ObjId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string TelNum { get; set; }
    }

    public class ManagerList
    {
        public int ObjId { get; set; }
        public int RelSeq { get; set; }
        public string RelCdName { get; set; }
        public string Name { get; set; }
        public DateTime? Birthday { get; set; }
        public string Address { get; set; }
        public string TelNum { get; set; }
    }

    public class BuildingHistoryPrimaryElement
    {
        public int ObjId { get; set; }
        public int ObjhistSeq { get; set; }
    }

    public class FireManagerPrimaryElement
    {
        public int ObjId { get; set; }
        public int FmgrSeq { get; set; }
    }
    public class SafeManagerPrimaryElement
    {
        public int ObjId { get; set; }
        public int DgrmgrSeq { get; set; }
    }

    public class ManagerPrimaryElement
    {
        public int ObjId { get; set; }
        public int RelSeq { get; set; }
    }
}