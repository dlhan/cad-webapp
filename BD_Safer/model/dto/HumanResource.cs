using System;
using System.Collections.Generic;
using BD_Safer.model;

namespace BD_Safer.model.dto 
{
    public class SearchUser
    {
        public Page page { get; set; }
        public string WardId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string UserId { get; set; }
        public int TitleCd { get; set; }
        public int ClassCd { get; set; }
        public int WorkCd { get; set; }
        public int WorkSectionCd { get; set; }
    }

    public class SearchUserResult
    {
        public Page page { get; set; }
        public virtual IEnumerable<UserListInfo> Rows { get; set; } 
    }

    public class UserListInfo
    {
        public string UserId { get; set; }
        public string Name { get; set; }
        public string UpwardName { get; set; }
        public string WardName { get; set; }
        public string ClassCdName { get; set; }
        public string TitleCdName { get; set; }
        public string WorkCdName { get; set; }
        public string WorkSectionCdName { get; set; }
        public string HomeTel { get; set; }
        public string OfficeTel { get; set; }
        public string Cell { get; set; }
        public string FaxNo { get; set; }
        public string ExtTel { get; set; }
        public string Email { get; set; }
    }
    public class UserDetail
    {
        public int UserSeq { get; set; }
        public string UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CitizenNo { get; set; }
        public string UpwardId { get; set; }
        public string WardId { get; set; }
        public int? ClassCd { get; set; }
        public int? TitleCd { get; set; }
        public int? WorkCd { get; set; }
        public int? WorkSectionCd { get; set; }
                
        public string SysmgrYn { get; set; }
        public string ZipCode { get; set; }
        public string Address { get; set; }
        public string HomeTel { get; set; }
        public string OfficeTel { get; set; }
        public string Cell { get; set; }
        public string FaxNo { get; set; }
        public string ExtTel { get; set; }
        public string Email { get; set; }
        public DateTime? BirthDate { get; set; }
        public string Gender { get; set; }
    }

    public class UserEducationListInfo
    {
        public int UserEducationSeq { get; set; }
        public string AcademyName { get; set; }
        public string Major { get; set; }
        public string DegreeCdName { get; set; }
        public string AcademyTypeCdName { get; set; }
        
        public DateTime? EntranceDate { get; set; }
        public DateTime? GraducationDate { get; set; }
        
    }

    public class UserEducationPromotionListInfo
    {
        public int EducationPromotionSeq { get; set; }
        public string CourseName { get; set; }
        public string InstitutionName { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public string EducationClsCdName { get; set; }
        public string CompleteCdName { get; set; }
        public string Score { get; set; }
    }
}

