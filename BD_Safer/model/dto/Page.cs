using System;
using System.Collections.Generic;

namespace BD_Safer.model.dto 
{
    public class Page
    {
        public int RowsPerPage { get; set; }
        public int PageNumber { get; set; }
        public int TotalCount { get;set; }
    }
}