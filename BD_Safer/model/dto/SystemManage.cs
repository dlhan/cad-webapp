using System;
using System.Collections.Generic;
using BD_Safer.model;

namespace BD_Safer.model.dto.SystemManage
{
    public class LogIn
    {
        public string UserId { get; set; }
        public string UserPw { get; set; }
    }
    public class NotificationInfo
    {
        public int NotificationId { get; set; }
        public string NotificationType { get; set; }
        public int? RelatedSeq { get; set; }
        public string State { get; set; }
        public string CreatedDate { get; set; }
        public string UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string WardName { get; set; }
    }

    public class GroupCodeInfo
    {
        public string GroupCodeTitle { get; set; }
        public virtual IEnumerable<GroupCodes> rows { get; set; } 
    }

    public class GroupCodes
    {
        public string CdGrp { get; set; }
        public string CdGrpName { get; set; }
        public string CdGrpDesc { get; set; }
        public string LeafYn { get; set; }
    }
}