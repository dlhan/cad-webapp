using System;
using System.Collections.Generic;

namespace BD_Safer.model.dto 
{
    public class UserInfo 
    {
        public CallResult result { get; set; }
        public HrUser userInfo { get; set; }
        public SmUserPreference userPreference { get; set; }
        public virtual IEnumerable<UserAuth> userAuth { get; set; } 
    }

    public class UserAuth
    {
        public string MenuId { get; set; }
        public int? Privilege { get; set; }
    }
}