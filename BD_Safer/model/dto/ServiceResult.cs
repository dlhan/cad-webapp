using System;
using System.Collections.Generic;

namespace BD_Safer.model.dto 
{
    public class CallResult
    {
        public bool IsSuccess { get; set; }
        public string Message { get; set; }
    }

    public class JobResultWithId
    {
        public CallResult result { get; set; }
        public string ID { get; set; }
    }
}