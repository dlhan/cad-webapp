﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BD_Safer.model
{
    public partial class IsObjfireequip
    {
        public int ObjId { get; set; }
        public int BldgSeq { get; set; }
        public int StorySeq { get; set; }
        public int? InExtingCnt { get; set; }
        public int? OutExtingCnt { get; set; }
        public int? ExtingPumpCnt { get; set; }
        public int? SprinklerHCnt { get; set; }
        public int? SprinklerAvCnt { get; set; }
        public int? SprayExtingHCnt { get; set; }
        public int? SprayExtingAvCnt { get; set; }
        public int? PoExtingHCnt { get; set; }
        public int? PoExtingAvCnt { get; set; }
        public int? CarbonDioxHCnt { get; set; }
        public int? CarbonDioxAvCnt { get; set; }
        public int? HalogenCompHCnt { get; set; }
        public int? HarlogenCompAvCnt { get; set; }
        public int? PowderExtingHCnt { get; set; }
        public int? PowderExtingAvCnt { get; set; }
        public int? SlideCnt { get; set; }
        public int? LadderCnt { get; set; }
        public int? RescueCnt { get; set; }
        public int? DescSlowDeviceCnt { get; set; }
        public int? MeasureEquipCnt { get; set; }
        public int? MeasureRopeCnt { get; set; }
        public int? SafeMatCnt { get; set; }
        public int? RescueEquipCnt { get; set; }
        public int? EmgLightCnt { get; set; }
        public int? WaterExtingCnt { get; set; }
        public int? ExtingWaterCnt { get; set; }
        public int? LwtrCnt { get; set; }
        public int? WtrpipeCnt { get; set; }
        public int? WaterSpringklingCnt { get; set; }
        public int? EmgPlugCnt { get; set; }
        public int? WirelessCommCnt { get; set; }
        public string UseYn { get; set; }
        public int? ExtingCnt { get; set; }
        public int? SimplctyExtingCnt { get; set; }
        public int? EmgWaringCnt { get; set; }
        public int? EmgBrodcCnt { get; set; }
        public int? LkgeWaringCnt { get; set; }
        public int? AutoFireFindSensCnt { get; set; }
        public int? AutoFireFindCircuitCnt { get; set; }
        public int? AutoFireNewsfCnt { get; set; }
        public int? GasLkgeWaringCnt { get; set; }
        public int? InduceLightCnt { get; set; }
        public int? InduceSignpostCnt { get; set; }
        public int? HydEquipEtcCnt { get; set; }
        public int? ResmokeCnt { get; set; }
        public int? CurtainCnt { get; set; }
        public int? CarssetteCnt { get; set; }
        public int? ResistEtcCnt { get; set; }

        public virtual IsStory IsStory { get; set; }
    }
}
