﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BD_Safer.model
{
    public partial class SmUserloghist
    {
        public int LogSeq { get; set; }
        public string UserId { get; set; }
        public DateTime? LogDtime { get; set; }
        public int? LogClsCd { get; set; }

        public virtual HrUser User { get; set; }
    }
}
