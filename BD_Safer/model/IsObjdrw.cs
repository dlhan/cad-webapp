﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BD_Safer.model
{
    public partial class IsObjdrw
    {
        public int ObjId { get; set; }
        public int DrwSeq { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string Remark { get; set; }
        public string UseYn { get; set; }
        public string CreatedDate { get; set; }

        public virtual IsObj Obj { get; set; }
    }
}
