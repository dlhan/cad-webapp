﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BD_Safer.model
{
    public partial class SmMenupriv
    {
        public int MenuPrivSeq { get; set; }
        public string PrivId { get; set; }
        public string MenuId { get; set; }
        public int Level { get; set; }
        public string LchgDtime { get; set; }
        public string LchgUserId { get; set; }
    }
}
