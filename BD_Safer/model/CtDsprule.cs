﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BD_Safer.model
{
    public partial class CtDsprule
    {
        public int DsrKndCd { get; set; }
        public int DsrClsCd { get; set; }
        public int DsrSizeCd { get; set; }
        public int CarCdGrp { get; set; }
        public int? CarCnt { get; set; }
        public string LchgDtime { get; set; }
        public string LchgUserId { get; set; }
    }
}
