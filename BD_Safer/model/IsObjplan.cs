﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BD_Safer.model
{
    public partial class IsObjplan
    {
        public int ObjId { get; set; }
        public string EnvDesc { get; set; }
        public string ExpandRsn { get; set; }
        public string RescEscCplan { get; set; }
        public string FmngIssue { get; set; }
        public string SelfvolunOrg { get; set; }
        public decimal? RealestExptAmt { get; set; }
        public decimal? MvestExptAmt { get; set; }
        public decimal? TotAmt { get; set; }
        public decimal? Ward1Dist { get; set; }
        public decimal? Ward2Dist { get; set; }
        public string EscPlace { get; set; }
        public string TrainingGuideDesc { get; set; }
        public string EtcRemark { get; set; }
        public string UseYn { get; set; }

        public virtual IsObj Obj { get; set; }
    }
}
