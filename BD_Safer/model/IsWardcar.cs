﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BD_Safer.model
{
    public partial class IsWardcar
    {
        public int CarId { get; set; }
        public string WardId { get; set; }
        public string CarNo { get; set; }
        public int? CarCd { get; set; }
        public int? CarstatCd { get; set; }
        public string DspAutoYn { get; set; }
        public string CarName { get; set; }
        public int? CarGrpCd { get; set; }
        public string Manufacturer { get; set; }
        public string CarKind { get; set; }
        public int? RideManCnt { get; set; }
        public DateTime? BuyDate { get; set; }
        public string LdspDtime { get; set; }
        public string LretDtime { get; set; }
        public string LchgDtime { get; set; }
        public string LchgUserId { get; set; }
        public string UseYn { get; set; }
        public string EtcInfo { get; set; }
        public string CarStatMemo { get; set; }
        public int? DspOrder { get; set; }
        public string RadioCallsign { get; set; }
        public string CarMobile { get; set; }
        public string DspDsrSeq { get; set; }
        public string PlatoonCls { get; set; }

        public virtual IsWard Ward { get; set; }
    }
}
