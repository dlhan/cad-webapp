﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BD_Safer.model
{
    public partial class CmDivision
    {
        public CmDivision()
        {
            CmDistricts = new HashSet<CmDistrict>();
        }

        public string Code { get; set; }
        public string DivisionName { get; set; }

        public virtual ICollection<CmDistrict> CmDistricts { get; set; }
    }
}
