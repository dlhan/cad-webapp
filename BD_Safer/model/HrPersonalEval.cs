﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BD_Safer.model
{
    public partial class HrPersonalEval
    {
        public int EvalSeq { get; set; }
        public int EvalYear { get; set; }
        public string UserId { get; set; }
        public string EvaluatorUserId { get; set; }
        public string EvalDate { get; set; }
        public string PerfAbilityScore { get; set; }
        public string RespScore { get; set; }
        public string DiligenceScore { get; set; }

        public virtual HrUser EvaluatorUser { get; set; }
        public virtual HrUser User { get; set; }
    }
}
