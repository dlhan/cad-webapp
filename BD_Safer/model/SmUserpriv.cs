﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BD_Safer.model
{
    public partial class SmUserpriv
    {
        public int UserprivSeq { get; set; }
        public string UserId { get; set; }
        public string PrivId { get; set; }
        public string ApprovedYn { get; set; }

        public virtual SmPriv Priv { get; set; }
        public virtual HrUser User { get; set; }
    }
}
