﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BD_Safer.model
{
    public partial class IsVolunEdu
    {
        public int VolunId { get; set; }
        public int VolunEduSeq { get; set; }
        public string CourseName { get; set; }
        public string InstitutionName { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? CompleteCd { get; set; }
        public string Score { get; set; }
        public string Remark { get; set; }

        public virtual IsVolun Volun { get; set; }
    }
}
