﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BD_Safer.model
{
    public partial class CmIndex
    {
        public int Id { get; set; }
        public string OrganName { get; set; }
    }
}
