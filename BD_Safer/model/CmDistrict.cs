﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BD_Safer.model
{
    public partial class CmDistrict
    {
        public int DistrictId { get; set; }
        public string DistrictName { get; set; }
        public string DivisionCode { get; set; }

        public virtual CmDivision DivisionCodeNavigation { get; set; }
    }
}
