﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BD_Safer.model
{
    public partial class IsHosp
    {
        public int HospId { get; set; }
        public string HospName { get; set; }
        public int? HospClsCd { get; set; }
        public string ErMedicalYn { get; set; }
        public string MainTreat { get; set; }
        public int? SickbedCnt { get; set; }
        public int? DoctorCnt { get; set; }
        public int? AmbulCnt { get; set; }
        public string ZipCode { get; set; }
        public string Address { get; set; }
        public string DayTel { get; set; }
        public string NightTel { get; set; }
        public string WardId { get; set; }
        public string RemoveYn { get; set; }
        public string Remark { get; set; }
        public string OnDutyYn { get; set; }
        public string LchgDtime { get; set; }
        public string LchgUserId { get; set; }
        public string UseYn { get; set; }
        public string UndgrdYn { get; set; }
    }
}
