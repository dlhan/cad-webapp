﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BD_Safer.model
{
    public partial class SmPriv
    {
        public SmPriv()
        {
            SmUserprivs = new HashSet<SmUserpriv>();
        }

        public string PrivId { get; set; }
        public string PrivName { get; set; }
        public string LchgDtime { get; set; }
        public string LchgUserId { get; set; }

        public virtual ICollection<SmUserpriv> SmUserprivs { get; set; }
    }
}
