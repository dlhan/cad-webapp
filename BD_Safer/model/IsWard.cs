﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BD_Safer.model
{
    public partial class IsWard
    {
        public IsWard()
        {
            HrUsers = new HashSet<HrUser>();
            IsDsrstats = new HashSet<IsDsrstat>();
            IsHyds = new HashSet<IsHyd>();
            IsJuriswards = new HashSet<IsJurisward>();
            IsObjs = new HashSet<IsObj>();
            IsOrgs = new HashSet<IsOrg>();
            IsVoluns = new HashSet<IsVolun>();
            IsWardcarhists = new HashSet<IsWardcarhist>();
            IsWardcars = new HashSet<IsWardcar>();
        }

        public string WardId { get; set; }
        public string UpwardId { get; set; }
        public string FireOfficeId { get; set; }
        public string WardName { get; set; }
        public int? TypeClsCd { get; set; }
        public string NickName { get; set; }
        public string ZipCode { get; set; }
        public string Address { get; set; }
        public string DspGroupYn { get; set; }
        public string AutoListYn { get; set; }
        public string DayTel { get; set; }
        public string NightTel { get; set; }
        public string ExtTel { get; set; }
        public DateTime? WardopenDate { get; set; }
        public DateTime? WardcloseDate { get; set; }
        public string WardopenYn { get; set; }
        public string BrdUsingYn { get; set; }
        public string Remark { get; set; }
        public string LchgDtime { get; set; }
        public string LchgUserId { get; set; }
        public int? WardOrder { get; set; }
        public string X { get; set; }
        public string Y { get; set; }
        public string DayFax { get; set; }
        public string NightFax { get; set; }
        public string UseYn { get; set; }

        public virtual ICollection<HrUser> HrUsers { get; set; }
        public virtual ICollection<IsDsrstat> IsDsrstats { get; set; }
        public virtual ICollection<IsHyd> IsHyds { get; set; }
        public virtual ICollection<IsJurisward> IsJuriswards { get; set; }
        public virtual ICollection<IsObj> IsObjs { get; set; }
        public virtual ICollection<IsOrg> IsOrgs { get; set; }
        public virtual ICollection<IsVolun> IsVoluns { get; set; }
        public virtual ICollection<IsWardcarhist> IsWardcarhists { get; set; }
        public virtual ICollection<IsWardcar> IsWardcars { get; set; }
    }
}
