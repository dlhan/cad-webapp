﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BD_Safer.model
{
    public partial class IsStory
    {
        public IsStory()
        {
            IsObjfireequips = new HashSet<IsObjfireequip>();
        }

        public int ObjId { get; set; }
        public int BldgSeq { get; set; }
        public string StoryNo { get; set; }
        public decimal? StoryArea { get; set; }
        public int? FcontCnt { get; set; }
        public string Deco { get; set; }
        public int? ResidentManCnt { get; set; }
        public int? AccomManCnt { get; set; }
        public string Etc { get; set; }
        public string UseYn { get; set; }
        public int StorySeq { get; set; }

        public virtual IsBldg IsBldg { get; set; }
        public virtual ICollection<IsObjfireequip> IsObjfireequips { get; set; }
    }
}
