﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BD_Safer.model
{
    public partial class IsObjpic
    {
        public int ObjPhotoSeq { get; set; }
        public int ObjId { get; set; }
        public int? ObjPhotoTypeCd { get; set; }
        public string Remark { get; set; }
        public string FileName { get; set; }
        public string FilePath { get; set; }
        public string UseYn { get; set; }

        public virtual IsObj Obj { get; set; }
    }
}
