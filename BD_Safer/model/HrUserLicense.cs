﻿using System;
using System.Collections.Generic;

#nullable disable

namespace BD_Safer.model
{
    public partial class HrUserLicense
    {
        public int LicenseSeq { get; set; }
        public string UserId { get; set; }
        public string LicenseNum { get; set; }
        public string LicenseName { get; set; }
        public DateTime? AcqDate { get; set; }

        public virtual HrUser User { get; set; }
    }
}
