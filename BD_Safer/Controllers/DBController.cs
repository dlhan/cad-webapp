using System;
using System.Data;
using MySql;
using MySql.Data;
using MySql.Data.MySqlClient;
using Microsoft.Extensions.Configuration;

namespace Safer
{
    public class DBController
    {
        private string _connectString = "";
        public DBController()
        {
            var config = new ConfigurationBuilder().SetBasePath(AppDomain.CurrentDomain.BaseDirectory).AddJsonFile("appsettings.json").Build();
            _connectString = config.GetConnectionString("DBConnecString");
        }

        private DataTable GetData(string sql)
        {
            DataTable dt = new DataTable();

            using (MySqlConnection conn = new MySqlConnection(_connectString))
            {
                conn.Open();
                MySqlDataAdapter da = new MySqlDataAdapter(sql, conn);
                DataSet ds = new DataSet();
                try
                {
                    ds.Reset();
                    da.Fill(ds);
                    dt = ds.Tables[0];        
                }
                catch
                {

                }
                finally
                {
                    ds.Dispose();
                    conn.Close();
                }
            }

            return dt;
        }

        
        
    }
}
