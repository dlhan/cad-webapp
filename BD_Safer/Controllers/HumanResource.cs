using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using BD_Safer.model;
using BD_Safer.model.dto;
using BD_Safer.model.dto.SystemManage;

namespace BD_Safer.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class HumanResourceController : ControllerBase
    {
        private readonly bd_saferContext _context;
        private readonly CommonController _controller;

        public HumanResourceController(bd_saferContext context)
        {
            _context = context;
            _controller = new CommonController(_context);
        }
        
        [HttpGet]
        [Route("CheckLoginIdDuplicated/{loginId}")]
        public bool CheckLoginIdDuplicated(string loginId)
        {
            bool result = false;
            var db = _context.HrUsers.Where(p => p.UserId == loginId).ToList();

            if (db.Count > 0) {
                result = true;
            } 
            return result;
        }

        [HttpPost]
        [Route("GetUsers/{locale}")]
        public ActionResult<SearchUserResult> GetUsers(string locale, [FromBody] SearchUser dto)
        {
            string WardId = dto.WardId;
            string FirstName = dto.FirstName;
            string LastName = dto.LastName;
            string UserId = dto.UserId;
            int? TitleCd = dto.TitleCd;
            int? ClassCd = dto.ClassCd;
            int? WorkCd = dto.WorkCd;
            int? WorkSectionCd = dto.WorkSectionCd;
            int RowsPerPage = dto.page.RowsPerPage;
            int PageNumber = dto.page.PageNumber;
            int TotalCount = 0;
            
            SearchUserResult searchResult = new SearchUserResult();
                        
            var predicate = PredicateBuilder.True<HrUser>();

            if (WardId != null)
            {
                predicate = predicate.And(p => p.WardId.Contains(WardId));
            }

            if (FirstName != null)
            {
                predicate = predicate.And(p => p.FirstName.Contains(FirstName));
            }

            if (LastName != null)
            {
                predicate = predicate.And(p => p.LastName.Contains(LastName));
            }

            if (UserId != null)
            {
                predicate = predicate.And(p => p.UserId.Contains(UserId));
            }

            if (TitleCd != 0)
            {
                predicate = predicate.And(p => p.TitleCd.Equals(TitleCd));
            }

            if (ClassCd != 0)
            {
                predicate = predicate.And(p => p.ClassCd.Equals(ClassCd));
            }

            if (WorkCd != 0)
            {
                predicate = predicate.And(p => p.WorkCd.Equals(WorkCd));
            }

            if (WorkSectionCd != 0)
            {
                predicate = predicate.And(p => p.WorkSectionCd.Equals(WorkSectionCd));
            }

            var db = from userList in _context.HrUsers.Where(predicate).OrderBy(p => p.FirstName).OrderBy(p => p.FirstName).OrderBy(p => p.LchgDtime).Skip(RowsPerPage * (PageNumber - 1)).Take(RowsPerPage)
                        join ward in _context.IsWards on userList.WardId equals ward.WardId into wardJoin from w1 in wardJoin.DefaultIfEmpty()
                        join upWard in _context.IsWards on w1.UpwardId equals upWard.WardId into upWardJoin from w2 in upWardJoin.DefaultIfEmpty()
                        join titleMaster in _context.SmCdMasters on userList.TitleCd equals titleMaster.Cd into joinC1 from c1 in joinC1.DefaultIfEmpty()
                        join titleResource in _context.SmCdResources.Where(w => w.Locale == locale) on c1.Cd equals titleResource.Cd into JoinCodeData1 from r1 in JoinCodeData1.DefaultIfEmpty()
                        join classMaster in _context.SmCdMasters on userList.ClassCd equals classMaster.Cd into joinC2 from c2 in joinC2.DefaultIfEmpty()
                        join classResource in _context.SmCdResources.Where(w => w.Locale == locale) on c2.Cd equals classResource.Cd into JoinCodeData2 from r2 in JoinCodeData2.DefaultIfEmpty()
                        join workMaster in _context.SmCdMasters on userList.WorkCd equals workMaster.Cd into joinC3 from c3 in joinC3.DefaultIfEmpty()
                        join workResource in _context.SmCdResources.Where(w => w.Locale == locale) on c3.Cd equals workResource.Cd into JoinCodeData3 from r3 in JoinCodeData3.DefaultIfEmpty()
                        join workSectionMaster in _context.SmCdMasters on userList.WorkSectionCd equals workSectionMaster.Cd into joinC4 from c4 in joinC4.DefaultIfEmpty()
                        join workSectionResource in _context.SmCdResources.Where(w => w.Locale == locale) on c4.Cd equals workSectionResource.Cd into JoinCodeData4 from r4 in JoinCodeData4.DefaultIfEmpty()
                            select new UserListInfo {
                                UpwardName = w2.WardName,
                                WardName = w1.WardName,
                                Name = String.Format("{0} {1}", userList.FirstName, userList.LastName),
                                UserId = userList.UserId,
                                TitleCdName = string.IsNullOrEmpty(r1.CdName) ? c1.CdDesc : r1.CdName,
                                ClassCdName = string.IsNullOrEmpty(r2.CdName) ? c2.CdDesc : r2.CdName,
                                WorkCdName = string.IsNullOrEmpty(r3.CdName) ? c3.CdDesc : r3.CdName,
                                WorkSectionCdName = string.IsNullOrEmpty(r4.CdName) ? c4.CdDesc : r4.CdName,
                                HomeTel = userList.HomeTel,
                                OfficeTel = userList.OfficeTel,
                                Cell = userList.Cell,
                                FaxNo = userList.FaxNo,
                                ExtTel = userList.ExtTel,
                                Email = userList.Email
                             };
            
            TotalCount = _context.HrUsers.Where(predicate).Count();

            searchResult.page = new Page();
            searchResult.page.PageNumber = dto.page.PageNumber;
            searchResult.page.RowsPerPage = dto.page.RowsPerPage;
            searchResult.page.TotalCount = TotalCount;
            searchResult.Rows = db;

            return CreatedAtAction(nameof(GetUsers), searchResult);
        }


        [HttpGet]
        [Route("GetUserInfo/{userId}")]
        public UserDetail GetUserInfo(string userId)
        {
            var db = from user in _context.HrUsers.Where(p => p.UserId == userId)
                     join ward in _context.IsWards on user.WardId equals ward.WardId into joinData from w in joinData.DefaultIfEmpty()
                        select new UserDetail {
                                UserSeq = user.UserSeq, 
                                UserId = user.UserId,
                                FirstName = user.FirstName,
                                LastName = user.LastName,
                                CitizenNo = user.CitizenNo,
                                UpwardId = w.UpwardId,
                                WardId = user.WardId,
                                ClassCd = user.ClassCd,
                                TitleCd = user.TitleCd,
                                WorkCd = user.WorkCd,
                                WorkSectionCd = user.WorkSectionCd, 
                                SysmgrYn = user.SysmgrYn,
                                ZipCode = user.ZipCode,
                                Address = user.Address,
                                HomeTel = user.HomeTel,
                                OfficeTel = user.OfficeTel,
                                Cell = user.Cell,
                                FaxNo = user.FaxNo,
                                ExtTel = user.ExtTel,
                                Email = user.Email,
                                BirthDate = user.BirthDate,
                                Gender = user.Gender
                             };
            return db.FirstOrDefault();
        }
        
        [HttpPost]
        [Route("InsertUser/{loginId}/{adminMode}")]
        public ActionResult<JobResultWithId> InsertUser(string loginId,int adminMode, [FromBody] HrUser dto) //adminMode-0:Normal mode ,1:Admin mode
        {
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();
            
            try
            {
                dto.UserSeq = _controller.MakeSeq(CommonController.TableType.SmUser);
                var data = _context.HrUsers.Where(f => f.UserId == dto.UserId).ToList();
                               
                if (data.Count > 0) {
                    result.result.IsSuccess = false;
                    result.result.Message = "msg_id_duplicated";
                }
                else 
                {
                    if (adminMode == 1) 
                    {
                        dto.UseYn = "1";
                        dto.LchgUserId = loginId;
                        dto.LchgDtime = _controller.MakeDateTimeFormat(DateTime.Now);
                    }
                    dto.RegDate = _controller.MakeDateTimeFormat(DateTime.Now);
                    _context.HrUsers.Add(dto);
                    _context.SaveChanges();

                    SmNotification notification = new SmNotification();
                    notification.UserId = dto.UserId;
                    notification.NotificationTypeCd = 15;
                    notification.RelatedSeq = dto.UserSeq;
                    notification.StateCd = 14;
                    notification.CreatedDate = _controller.MakeDateTimeFormat(DateTime.Now);
                    notification.CompletedYn = "0";
                    
                    _context.SmNotifications.Add(notification);
                    _context.SaveChanges();

                    result.result.IsSuccess = true;
                    result.result.Message = "msg_success_to_insert";
                    result.ID = dto.UserSeq.ToString();
                }
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }
            
    
            return CreatedAtAction(nameof(InsertUser), result);
        }

        [HttpPost]
        [Route("UpdateUser/{loginId}")]
        public ActionResult<JobResultWithId> UpdateUser(string loginId, [FromBody] HrUser dto)
        {
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                dto.LchgUserId = loginId;
                dto.LchgDtime = _controller.MakeDateTimeFormat(DateTime.Now);
                _context.HrUsers.Update(dto);
                _context.SaveChanges();
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_update";
                result.ID = dto.UserSeq.ToString();
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }
            
    
            return CreatedAtAction(nameof(UpdateUser), result);
        }

        [HttpPost("DeleteUser/{userId}")]
        public ActionResult<CallResult> DeleteUser(string userId, [FromBody] IList<string> Ids )
        {
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                _context.HrUsers.Where(p => Ids.Contains(p.UserId)).ToList()
                .ForEach(item => {
                    _context.HrUsers.SingleOrDefault(f => f.UserId == item.UserId).LchgUserId = userId;
                    _context.HrUsers.SingleOrDefault(f => f.UserId == item.UserId).LchgDtime = _controller.MakeDateTimeFormat(DateTime.Now);
                    _context.HrUsers.SingleOrDefault(f => f.UserId == item.UserId).UseYn = "0";
                    _context.SaveChanges();
                });
                
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_delete";
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }

            return CreatedAtAction(nameof(DeleteUser), result);
        }

        [HttpGet]
        [Route("GetUserEducationList/{userId}/{locale}")]
        public IEnumerable<UserEducationListInfo> GetUserEducationList(string userId, string locale)
        {
            var db = from educationList in _context.HrUserEdus.Where(p => p.UserId == userId).OrderBy(o => o.UserEducationSeq)
                        join degreeMaster in _context.SmCdMasters on educationList.DegreeCd equals degreeMaster.Cd into joinC1 from c1 in joinC1.DefaultIfEmpty()
                        join degreeResource in _context.SmCdResources.Where(w => w.Locale == locale) on c1.Cd equals degreeResource.Cd into JoinCodeData1 from r1 in JoinCodeData1.DefaultIfEmpty()
                        join academyTypeMaster in _context.SmCdMasters on educationList.AcademyTypeCd equals academyTypeMaster.Cd into joinC2 from c2 in joinC2.DefaultIfEmpty()
                        join academyTypeResource in _context.SmCdResources.Where(w => w.Locale == locale) on c2.Cd equals academyTypeResource.Cd into JoinCodeData2 from r2 in JoinCodeData2.DefaultIfEmpty()
                      select new UserEducationListInfo {
                          UserEducationSeq = educationList.UserEducationSeq,
                          AcademyName = educationList.AcademyName,
                          Major = educationList.Major,
                          DegreeCdName = string.IsNullOrEmpty(r1.CdName) ? c1.CdDesc : r1.CdName,
                          AcademyTypeCdName = string.IsNullOrEmpty(r2.CdName) ? c2.CdDesc : r2.CdName,
                          EntranceDate = educationList.EntranceDate,
                          GraducationDate = educationList.GraducationDate
                      }; 
            return db;
        }

        [HttpGet]
        [Route("GetEducationInfo/{userEducationSeq}")]
        public HrUserEdu GetEducationInfo(int UserEducationSeq)
        {
            HrUserEdu educationInfo = new HrUserEdu();
            educationInfo = _context.HrUserEdus.Where(p => p.UserEducationSeq == UserEducationSeq).First();
            return educationInfo;
        }

        [HttpPost]
        [Route("InsertEducationInfo")]
        public ActionResult<JobResultWithId> InsertEducationInfo([FromBody] HrUserEdu dto)
        {
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                _context.HrUserEdus.Add(dto);
                _context.SaveChanges();
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_insert";
                result.ID = dto.UserEducationSeq.ToString();
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }
            
    
            return CreatedAtAction(nameof(InsertEducationInfo), result);
        }

        [HttpPost]
        [Route("UpdateEducationInfo")]
        public ActionResult<JobResultWithId> UpdateEducationInfo([FromBody] HrUserEdu dto)
        {
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                _context.HrUserEdus.Update(dto);
                _context.SaveChanges();
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_update";
                result.ID = dto.UserEducationSeq.ToString();
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }
               
            return CreatedAtAction(nameof(UpdateEducationInfo), result);
        }

        [HttpPost("DeleteEducationInfo")]
        public ActionResult<CallResult> DeleteEducationInfo([FromBody] IList<int> Ids )
        {
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                var educations = _context.HrUserEdus.Where(p => Ids.Contains(p.UserEducationSeq)).ToList();
                _context.HrUserEdus.RemoveRange(educations);
                _context.SaveChanges();
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_delete";
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }

            return CreatedAtAction(nameof(DeleteEducationInfo), result);
        }

        [HttpGet]
        [Route("GetLicenseInfo/{licenseSeq}")]
        public HrUserLicense GetLicenseInfo(int LicenseSeq)
        {
            HrUserLicense licenseInfo = new HrUserLicense();
            licenseInfo = _context.HrUserLicenses.Where(p => p.LicenseSeq == LicenseSeq).First();
            return licenseInfo;
        }

        [HttpGet]
        [Route("GetLicenseList/{userId}")]
        public IEnumerable<HrUserLicense> GetLicenseList(string userId)
        {
            var db = _context.HrUserLicenses.Where(p => p.UserId == userId);
            return db.ToList();
        }

        [HttpPost]
        [Route("InsertLicenseInfo")]
        public ActionResult<JobResultWithId> InsertLicenseInfo([FromBody] HrUserLicense dto)
        {
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                _context.HrUserLicenses.Add(dto);
                _context.SaveChanges();
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_insert";
                result.ID = dto.LicenseSeq.ToString();
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }
            
    
            return CreatedAtAction(nameof(InsertLicenseInfo), result);
        }

        [HttpPost]
        [Route("UpdateLicenseInfo")]
        public ActionResult<JobResultWithId> UpdateLicenseInfo([FromBody] HrUserLicense dto)
        {
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                _context.HrUserLicenses.Update(dto);
                _context.SaveChanges();
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_update";
                result.ID = dto.LicenseSeq.ToString();
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }
            
    
            return CreatedAtAction(nameof(UpdateLicenseInfo), result);
        }

        [HttpPost("DeleteLicenseInfo")]
        public ActionResult<CallResult> DeleteLicenseInfo([FromBody] IList<int> Ids )
        {
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                var licenses = _context.HrUserLicenses.Where(p => Ids.Contains(p.LicenseSeq)).ToList();
                _context.HrUserLicenses.RemoveRange(licenses);
                _context.SaveChanges();
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_delete";
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }

            return CreatedAtAction(nameof(DeleteLicenseInfo), result);
        }

        [HttpGet]
        [Route("GetEducationPromotionInfo/{educationPromotionSeq}")]
        public HrEduPromotion GetEducationPromotionInfo(int EducationPromotionSeq)
        {
            HrEduPromotion educatoinPromotionInfo = new HrEduPromotion();
            educatoinPromotionInfo = _context.HrEduPromotions.Where(p => p.EducationPromotionSeq == EducationPromotionSeq).First();
            return educatoinPromotionInfo;
        }

        [HttpGet]
        [Route("GetEducationPromotionList/{userId}/{locale}")]
        public IEnumerable<UserEducationPromotionListInfo> GetEducationPromotionList(string userId, string locale)
        {
            var db = from educatoinPromotionList in _context.HrEduPromotions.Where(p => p.UserId == userId).OrderBy(o => o.EducationPromotionSeq)
                        join eduClsMaster in _context.SmCdMasters on educatoinPromotionList.EducationClsCd equals eduClsMaster.Cd into joinC1 from c1 in joinC1.DefaultIfEmpty()
                        join eduClsResource in _context.SmCdResources.Where(w => w.Locale == locale) on c1.Cd equals eduClsResource.Cd into JoinCodeData1 from r1 in JoinCodeData1.DefaultIfEmpty()
                        join completeMaster in _context.SmCdMasters on educatoinPromotionList.CompleteCd equals completeMaster.Cd into joinC2 from c2 in joinC2.DefaultIfEmpty()
                        join completeResource in _context.SmCdResources.Where(w => w.Locale == locale) on c2.Cd equals completeResource.Cd into JoinCodeData2 from r2 in JoinCodeData2.DefaultIfEmpty()
                      select new UserEducationPromotionListInfo {
                         EducationPromotionSeq = educatoinPromotionList.EducationPromotionSeq,
                         CourseName = educatoinPromotionList.CourseName,
                         InstitutionName = educatoinPromotionList.InstitutionName,
                         StartDate = educatoinPromotionList.StartDate,
                         EndDate = educatoinPromotionList.EndDate,
                         EducationClsCdName = string.IsNullOrEmpty(r1.CdName) ? c1.CdDesc : r1.CdName,
                         CompleteCdName = string.IsNullOrEmpty(r2.CdName) ? c2.CdDesc : r2.CdName,
                         Score = educatoinPromotionList.Score
                      }; 

            return db.ToList();
        }

        [HttpPost]
        [Route("InsertEducationPromotionInfo")]
        public ActionResult<JobResultWithId> InsertEducationPromotionInfo([FromBody] HrEduPromotion dto)
        {
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                _context.HrEduPromotions.Add(dto);
                _context.SaveChanges();
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_insert";
                result.ID = dto.EducationPromotionSeq.ToString();
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }
            
    
            return CreatedAtAction(nameof(InsertEducationPromotionInfo), result);
        }

        [HttpPost]
        [Route("UpdateEducationPromotionInfo")]
        public ActionResult<JobResultWithId> UpdateEducationPromotionInfo([FromBody] HrEduPromotion dto)
        {
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                _context.HrEduPromotions.Update(dto);
                _context.SaveChanges();
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_update";
                result.ID = dto.EducationPromotionSeq.ToString();
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }
            
    
            return CreatedAtAction(nameof(UpdateEducationPromotionInfo), result);
        }

        [HttpPost("DeleteEducationPromotionInfo")]
        public ActionResult<CallResult> DeleteEducationPromotionInfo([FromBody] IList<int> Ids )
        {
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                var educationPromotions = _context.HrEduPromotions.Where(p => Ids.Contains(p.EducationPromotionSeq)).ToList();
                _context.HrEduPromotions.RemoveRange(educationPromotions);
                _context.SaveChanges();
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_delete";
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }

            return CreatedAtAction(nameof(DeleteEducationPromotionInfo), result);
        }

        [HttpGet]
        [Route("GetFireTrainInfo/{fireTrainSeq}")]
        public HrUserFireTrain GetFireTrainInfo(int FireTrainSeq)
        {
            HrUserFireTrain fireTrainInfo = new HrUserFireTrain();
            fireTrainInfo = _context.HrUserFireTrains.Where(p => p.FireTrainSeq == FireTrainSeq).First();
            return fireTrainInfo;
        }

        [HttpGet]
        [Route("GetFireTrainList/{userId}")]
        public IEnumerable<HrUserFireTrain> GetFireTrainList(string userId)
        {
            var db = _context.HrUserFireTrains.Where(p => p.UserId == userId);
            return db.ToList();
        }

        [HttpPost]
        [Route("InsertFireTrainInfo")]
        public ActionResult<JobResultWithId> InsertFireTrainInfo([FromBody] HrUserFireTrain dto)
        {
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                _context.HrUserFireTrains.Add(dto);
                _context.SaveChanges();
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_insert";
                result.ID = dto.FireTrainSeq.ToString();
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }
            
    
            return CreatedAtAction(nameof(InsertFireTrainInfo), result);
        }

        [HttpPost]
        [Route("UpdateFireTrainInfo")]
        public ActionResult<JobResultWithId> UpdateFireTrainInfo([FromBody] HrUserFireTrain dto)
        {
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                _context.HrUserFireTrains.Update(dto);
                _context.SaveChanges();
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_update";
                result.ID = dto.FireTrainSeq.ToString();
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }
                
            return CreatedAtAction(nameof(UpdateFireTrainInfo), result);
        }

        [HttpPost("DeleteFireTrainInfo")]
        public ActionResult<CallResult> DeleteFireTrainInfo([FromBody] IList<int> Ids )
        {
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                var fireTrains = _context.HrUserFireTrains.Where(p => Ids.Contains(p.FireTrainSeq)).ToList();
                _context.HrUserFireTrains.RemoveRange(fireTrains);
                _context.SaveChanges();
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_delete";
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }

            return CreatedAtAction(nameof(DeleteFireTrainInfo), result);
        }
    }
}