using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using BD_Safer.model;
using BD_Safer.model.dto;
using Newtonsoft.Json.Linq;

namespace BD_Safer.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ObjectController : ControllerBase
    {
        private readonly bd_saferContext _context;

        public ObjectController(bd_saferContext context)
        {
            _context = context;
        }

        [HttpGet]
        [Route("GetObjectInfo/{objId}")]
        public ObjectInfo GetObjectInfo(int objId)
        {
            var objectInfo = from objList in _context.IsObjs.Where(w => w.ObjId == objId)
                            join wardList in _context.IsWards on objList.WardId equals wardList.WardId
                            select new ObjectInfo {    
                               ObjId = objList.ObjId,
                               UpwardId = wardList.UpwardId,
                               WardId = objList.WardId,
                               ObjManNum = objList.ObjManNum,
                               UsedCd = objList.UsedCd,
                               ObjCd = objList.ObjCd,
                               ConstName = objList.ConstName,
                               RpsnFirmName = objList.RpsnFirmName,
                               ZipCode = objList.ZipCode,
                               Address = objList.Address,
                               UseYn = objList.UseYn,
                               ObjStdCd = objList.ObjStdCd,
                               BldgNm = objList.BldgNm,
                               X = objList.X,
                               Y = objList.Y,
                               WeakobjYn = objList.WeakobjYn,
                               SelfobjYn = objList.SelfobjYn,
                               AdgInfo = objList.AdgInfo,
                               DayTel = objList.DayTel,
                               NightTel = objList.NightTel,
                               PrevdstTel = objList.PrevdstTel,
                               UseConfmDate = objList.UseConfmDate,
                               InstitYn = objList.InstitYn,
                               InstitClsCd = objList.InstitClsCd,
                               PubfaclYn = objList.PubfaclYn,
                               WeakobjCd = objList.WeakobjCd,
                               SubUseDesc = objList.SubUseDesc,
                               TunnelClsCd = objList.TunnelClsCd,
                               TunnelLength = objList.TunnelLength
                           };
             return objectInfo.First();
        }

        [HttpPost]
        [Route("ObjectList/{locale}")]
        public ActionResult<SearchObjectResult> ObjectList(string locale, [FromBody] SearchObject dto)
        {
            string WardId = dto.WardId;
            string ConstName = dto.ConstName;
            int? ObjCd = dto.ObjCd;
            string ObjManNum = dto.ObjManNum;
            int? UsedCd = dto.UsedCd;
            string Address = dto.Address;
            int RowsPerPage = dto.page.RowsPerPage;
            int PageNumber = dto.page.PageNumber;
            int TotalCount = 0;
            
            SearchObjectResult objResult = new SearchObjectResult();
                        
            var predicate = PredicateBuilder.True<IsObj>();

            if (ConstName != null)
            {
                predicate = predicate.And(p => p.ConstName.StartsWith(ConstName));
            }

            if (ObjManNum != null)
            {
                predicate = predicate.And(p => p.ObjManNum.StartsWith(ObjManNum));
            }

            if (Address != null)
            {
                predicate = predicate.And(p => p.Address.StartsWith(Address));
            }

            if (WardId != null)
            {
                predicate = predicate.And(p => p.WardId.Equals(WardId));
            }

            if (ObjCd != null)
            {
                predicate = predicate.And(p => p.ObjCd.Equals(ObjCd));
            }

            if (UsedCd != null)
            {
                predicate = predicate.And(p => p.UsedCd.Equals(UsedCd));
            }

            var OjbectList = from objList in _context.IsObjs.Where(predicate).Where(w => w.UseYn=="1").OrderBy(p => p.ConstName).OrderBy(p => p.ObjId).Skip(RowsPerPage * (PageNumber - 1)).Take(RowsPerPage)
                            join wardList in _context.IsWards on objList.WardId equals wardList.WardId
                            join upWardList in _context.IsWards on wardList.UpwardId equals upWardList.WardId into joinWard from w in joinWard.DefaultIfEmpty()
                           join objCdMaster in _context.SmCdMasters on objList.ObjCd equals objCdMaster.Cd into joinC1 from c1 in joinC1.DefaultIfEmpty()
                           join objCdResource in _context.SmCdResources.Where(w => w.Locale == locale) on c1.Cd equals objCdResource.Cd into JoinCodeData1 from r1 in JoinCodeData1.DefaultIfEmpty()
                           join usedCdMaster in _context.SmCdMasters on objList.UsedCd equals usedCdMaster.Cd into joinC2 from c2 in joinC2.DefaultIfEmpty()
                           join usedCdResource in _context.SmCdResources.Where(w => w.Locale == locale) on c2.Cd equals usedCdResource.Cd into JoinCodeData2 from r2 in JoinCodeData2.DefaultIfEmpty()
                             select new SearchObjectInfo {
                                ObjId = objList.ObjId,
                                UpwardName = w.WardName,
                                WardName = wardList.WardName,
                                ObjManNum = objList.ObjManNum,
                                ConstName = objList.ConstName,
                                BldgNm = objList.BldgNm,
                                Address = objList.Address,
                                ObjCdName = string.IsNullOrEmpty(r1.CdName) ? c1.CdDesc : r1.CdName,
                                UsedCdName = string.IsNullOrEmpty(r2.CdName) ? c2.CdDesc : r2.CdName,
                             };
            
            TotalCount = _context.IsObjs.Where(predicate).Count();
            objResult.page = new Page();
            objResult.page.PageNumber = dto.page.PageNumber;
            objResult.page.RowsPerPage = dto.page.RowsPerPage;
            objResult.page.TotalCount = TotalCount;
            objResult.Rows = OjbectList;

            return CreatedAtAction(nameof(ObjectList), objResult);
        }

        [HttpPost]
        [Route("InsertObject/{userId}")]
        public ActionResult<JobResultWithId> InsertObject(string userId, [FromBody] IsObj dto)
        {
            CommonController controller = new CommonController(_context);
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                string Json = _context.SmSysenvs.Where(w => w.Id == "E01").FirstOrDefault().Values;
                int DivisionCode = (int)JObject.Parse(Json)["code"];
                
                dto.ObjId = controller.MakeSeq(CommonController.TableType.ISObj);
                _context.IsObjs.Add(dto);
                _context.SaveChanges();
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_insert";
                result.ID = dto.ObjId.ToString();
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }
            
    
            return CreatedAtAction(nameof(InsertObject), result);
        }

        [HttpPost]
        [Route("UpdateObject/{userId}")]
        public ActionResult<JobResultWithId> UpdateObject(string userId, [FromBody] IsObj dto)
        {
            CommonController controller = new CommonController(_context);
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                _context.IsObjs.Update(dto);
                _context.SaveChanges();
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_update";
                result.ID = dto.ObjId.ToString();
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }
            
    
            return CreatedAtAction(nameof(UpdateObject), result);
        }

        [HttpPost("DeleteObject")]
        public ActionResult<CallResult> DeleteObject([FromBody] IList<int> Ids )
        {
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                _context.IsObjs.Where(p => Ids.Contains(p.ObjId)).ToList()
                .ForEach(item => {
                    _context.IsObjs.SingleOrDefault(f => f.ObjId == item.ObjId).UseYn = "0";
                    _context.SaveChanges();
                });
                                
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_delete";
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }

            return CreatedAtAction(nameof(DeleteObject), result);
        }

        [HttpGet]
        [Route("GetBuildingInfo/{objId}/{bldgSeq}")]
        public IsBldg GetBuildingInfo(int objId, int bldgSeq)
        {
            var buildingInfo = _context.IsBldgs.Where(w => w.ObjId == objId).Where(w => w.BldgSeq == bldgSeq).First();
            return buildingInfo;
        }

        [HttpGet]
        [Route("BuildingList/{objId}/{locale}")]
        public IEnumerable<BuildingList> BuildingList(int objId, string locale)
        {
            var buildingList = from bldgList in _context.IsBldgs.Where(w => w.ObjId == objId).Where(w => w.UseYn == "1").OrderBy(p => p.BldgName).OrderBy(p => p.BldgSeq)
                            join struct1Master in _context.SmCdMasters on bldgList.Struct1 equals struct1Master.Cd into joinC1 from c1 in joinC1.DefaultIfEmpty()
                            join struct1Resource in _context.SmCdResources.Where(w => w.Locale == locale) on c1.Cd equals struct1Resource.Cd into JoinCodeData1 from r1 in JoinCodeData1.DefaultIfEmpty()
                            join struct2Master in _context.SmCdMasters on bldgList.Struct2 equals struct2Master.Cd into joinC2 from c2 in joinC2.DefaultIfEmpty()
                            join struct2Resource in _context.SmCdResources.Where(w => w.Locale == locale) on c2.Cd equals struct2Resource.Cd into JoinCodeData2 from r2 in JoinCodeData2.DefaultIfEmpty()
                            join struct3Master in _context.SmCdMasters on bldgList.Struct1 equals struct3Master.Cd into joinC3 from c3 in joinC3.DefaultIfEmpty()
                            join struct3Resource in _context.SmCdResources.Where(w => w.Locale == locale) on c3.Cd equals struct3Resource.Cd into JoinCodeData3 from r3 in JoinCodeData3.DefaultIfEmpty()
                            join mainUsedCdMaster in _context.SmCdMasters on bldgList.Struct1 equals mainUsedCdMaster.Cd into joinC4 from c4 in joinC4.DefaultIfEmpty()
                            join mainUsedCdResource in _context.SmCdResources.Where(w => w.Locale == locale) on c4.Cd equals mainUsedCdResource.Cd into JoinCodeData4 from r4 in JoinCodeData4.DefaultIfEmpty()
                            join subUsedCdMaster in _context.SmCdMasters on bldgList.Struct1 equals subUsedCdMaster.Cd into joinC5 from c5 in joinC5.DefaultIfEmpty()
                            join subUsedCdResource in _context.SmCdResources.Where(w => w.Locale == locale) on c5.Cd equals subUsedCdResource.Cd into JoinCodeData5 from r5 in JoinCodeData5.DefaultIfEmpty()
                              select new BuildingList {
                                BldgSeq = bldgList.BldgSeq,
                                ObjId = bldgList.ObjId,
                                BldgName = bldgList.BldgName,
                                Struct1 = string.IsNullOrEmpty(r1.CdName) ? c1.CdDesc : r1.CdName,
                                Struct2 = string.IsNullOrEmpty(r2.CdName) ? c2.CdDesc : r2.CdName,
                                Struct3 = string.IsNullOrEmpty(r3.CdName) ? c3.CdDesc : r3.CdName,
                                MainUse = string.IsNullOrEmpty(r4.CdName) ? c4.CdDesc : r4.CdName,
                                SubUse  = string.IsNullOrEmpty(r5.CdName) ? c5.CdDesc : r5.CdName,
                                UstoryCnt = bldgList.UstoryCnt,
                                BstoryCnt = bldgList.BstoryCnt,
                                LotArea = bldgList.LotArea,
                                FloorArea = bldgList.FloorArea,
                                TotArea = bldgList.TotArea
                             };
            
            return buildingList.ToList();
        }

        [HttpPost]
        [Route("InsertBuilding/{userId}")]
        public ActionResult<JobResultWithId> InsertBuilding(string userId, [FromBody] IsBldg dto)
        {
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                dto.UseYn="1";
                _context.IsBldgs.Add(dto);
                _context.SaveChanges();
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_insert";
                result.ID = dto.BldgSeq.ToString();
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }
            
    
            return CreatedAtAction(nameof(InsertBuilding), result);
        }

        [HttpPost]
        [Route("UpdateBuilding/{userId}")]
        public ActionResult<JobResultWithId> UpdateBuilding(string userId, [FromBody] IsBldg dto)
        {
            CommonController controller = new CommonController(_context);
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                _context.IsBldgs.Update(dto);
                _context.SaveChanges();
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_update";
                result.ID = dto.BldgSeq.ToString();
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }
            
    
            return CreatedAtAction(nameof(UpdateBuilding), result);
        }

        [HttpPost("DeleteBuilding")]
        public ActionResult<CallResult> DeleteBuilding([FromBody] IList<ObjectElement> Ids )
        {
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                if (Ids.Count > 0)
                {
                    foreach (ObjectElement e in Ids)
                    {
                        _context.IsBldgs.Where(w => w.ObjId == e.ObjId).Where(w => w.BldgSeq == e.Seq).ToList()
                        .ForEach(item => {
                            _context.IsBldgs.SingleOrDefault(f => f.ObjId == item.ObjId && f.BldgSeq == item.BldgSeq).UseYn = "0";
                            _context.SaveChanges();
                        });
                    }
                }
                
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_delete";
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }

            return CreatedAtAction(nameof(DeleteBuilding), result);
        }

        [HttpGet]
        [Route("GetStoryInfo/{objId}/{bldgSeq}/{storySeq}")]
        public IsStory GetStoryInfo(int objId, int bldgSeq, int storySeq)
        {
            var storyInfo = _context.IsStories.Where(w => w.ObjId == objId).Where(w => w.BldgSeq == bldgSeq).Where(w => w.StorySeq == storySeq).First();
            return storyInfo;
        }

        [HttpGet]
        [Route("GetStoryList/{objId}/{bldgSeq}")]
        public IEnumerable<IsStory> GetStoryList(int objId, int bldgSeq)
        {
            var storyList = _context.IsStories.Where(w => w.ObjId == objId).Where(w => w.BldgSeq == bldgSeq).Where(w => w.UseYn == "1");
            return storyList.ToList();
        }

        [HttpPost]
        [Route("InsertStory/{userId}")]
        public ActionResult<JobResultWithId> InsertStory(string userId, [FromBody] IsStory dto)
        {
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                dto.UseYn = "1";
                _context.IsStories.Add(dto);
                _context.SaveChanges();
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_insert";
                result.ID = dto.StorySeq.ToString();
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }
            
    
            return CreatedAtAction(nameof(InsertStory), result);
        }

        [HttpPost]
        [Route("UpdateStory/{userId}")]
        public ActionResult<JobResultWithId> UpdateStory(string userId, [FromBody] IsStory dto)
        {
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                _context.IsStories.Update(dto);
                _context.SaveChanges();
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_update";
                result.ID = dto.StorySeq.ToString();
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }
            
    
            return CreatedAtAction(nameof(UpdateStory), result);
        }

        [HttpPost("DeleteStory")]
        public ActionResult<CallResult> DeleteStory([FromBody] IList<StoryPrimaryElement> Ids )
        {
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                if (Ids.Count > 0)
                {
                    foreach (StoryPrimaryElement e in Ids)
                    {
                        _context.IsStories.Where(w => w.ObjId == e.ObjId).Where(w => w.BldgSeq == e.BldgSeq).Where(w => w.StorySeq == e.StorySeq).ToList()
                        .ForEach(item => {
                            _context.IsStories.SingleOrDefault(f => f.ObjId == item.ObjId && f.BldgSeq == item.BldgSeq && f.StorySeq == item.StorySeq).UseYn = "0";
                            _context.SaveChanges();
                        });
                    }
                }
                
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_delete";
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }

            return CreatedAtAction(nameof(DeleteStory), result);
        }

        [HttpGet]
        [Route("GetStorySpecUsedInfo/{objId}/{bldgSeq}/{storySeq}/{specUseSeq}/{locale}")]
        public StorySpecUsedInfo GetStorySpecUsedInfo(int objId, int bldgSeq, int storySeq, int specUseSeq, string locale)
        {
            var storySpecUsedInfo = from storySpecUsed in _context.IsStorySpecUseds.Where(w => w.ObjId == objId).Where(w => w.BldgSeq == bldgSeq).Where(w => w.StorySeq == storySeq).Where(w => w.SpecUseSeq == specUseSeq)
                                    join specUseCd in _context.SmCdMasters on storySpecUsed.SpecUseClsCd equals specUseCd.Cd into joinC1 from c1 in joinC1.DefaultIfEmpty()
                                    join specUsedCdResource in _context.SmCdResources.Where(w => w.Locale == locale) on c1.Cd equals specUsedCdResource.Cd into JoinCodeData1 from r1 in JoinCodeData1.DefaultIfEmpty()
                                    select new StorySpecUsedInfo{
                                        ObjId = storySpecUsed.ObjId,
                                        BldgSeq = storySpecUsed.BldgSeq,
                                        StorySeq = storySpecUsed.StorySeq,
                                        SpecUseSeq = storySpecUsed.SpecUseSeq,
                                        SpecUseClsCd = storySpecUsed.SpecUseClsCd,
                                        SpecUseClsCdName = string.IsNullOrEmpty(r1.CdName) ? c1.CdDesc : r1.CdName,
                                        Area = storySpecUsed.Area,
                                        Remark = storySpecUsed.Remark,
                                        UseYn = storySpecUsed.UseYn
                                    };
            
            return storySpecUsedInfo.First();
        }

        [HttpGet]
        [Route("GetStorySpecUsedList/{objId}/{bldgSeq}/{storySeq}/{locale}")]
        public IEnumerable<StorySpecUsedList> GetStorySpecUsedList(int objId, int bldgSeq, int storySeq, string locale)
        {
            var storySpecUsedList = from storySpecUsed in _context.IsStorySpecUseds.Where(w => w.ObjId == objId).Where(w => w.BldgSeq == bldgSeq).Where(w => w.StorySeq == storySeq).Where(w => w.UseYn == "1").OrderBy(p => p.SpecUseSeq)
                            from storyList in _context.IsStories.Where(storyList => storySpecUsed.ObjId == storyList.ObjId)
                                                                .Where(storyList => storySpecUsed.BldgSeq == storyList.BldgSeq)
                                                                .Where(storyList => storySpecUsed.StorySeq == storyList.StorySeq)
                            join struct1Master in _context.SmCdMasters on storySpecUsed.SpecUseClsCd equals struct1Master.Cd into joinC1 from c1 in joinC1.DefaultIfEmpty()
                            join struct1Resource in _context.SmCdResources.Where(w => w.Locale == locale) on c1.Cd equals struct1Resource.Cd into JoinCodeData1 from r1 in JoinCodeData1.DefaultIfEmpty()
                            select new StorySpecUsedList {
                                ObjId = storySpecUsed.ObjId,
                                BldgSeq = storySpecUsed.BldgSeq,
                                StorySeq  = storySpecUsed.StorySeq,
                                SpecUseSeq = storySpecUsed.SpecUseSeq,
                                StoryNo = storyList.StoryNo,
                                SpecUseClsCdName = string.IsNullOrEmpty(r1.CdName) ? c1.CdDesc : r1.CdName,
                                Area = storySpecUsed.Area
                            };
                        
            return storySpecUsedList.ToList();
        }

        [HttpPost]
        [Route("InsertStorySpecUsed/{userId}")]
        public ActionResult<JobResultWithId> InsertStorySpecUsed(string userId, [FromBody] IsStorySpecUsed dto)
        {
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                dto.UseYn = "1";
                _context.IsStorySpecUseds.Add(dto);
                _context.SaveChanges();
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_insert";
                result.ID = dto.StorySeq.ToString();
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }
            
    
            return CreatedAtAction(nameof(InsertStorySpecUsed), result);
        }

        [HttpPost]
        [Route("UpdateStorySpecUsed/{userId}")]
        public ActionResult<JobResultWithId> UpdateStorySpecUsed(string userId, [FromBody] IsStorySpecUsed dto)
        {
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                _context.IsStorySpecUseds.Update(dto);
                _context.SaveChanges();
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_update";
                result.ID = dto.StorySeq.ToString();
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }
            
    
            return CreatedAtAction(nameof(UpdateStorySpecUsed), result);
        }

        [HttpPost("DeleteStorySpecUsed")]
        public ActionResult<CallResult> DeleteStorySpecUsed([FromBody] IList<StorySpecUsedElement> Ids )
        {
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                if (Ids.Count > 0)
                {
                    foreach (StorySpecUsedElement e in Ids)
                    {
                        _context.IsStorySpecUseds.Where(w => w.ObjId == e.ObjId).Where(w => w.BldgSeq == e.BldgSeq).Where(w => w.StorySeq == e.StorySeq).Where(w => w.SpecUseSeq == e.SpecUseSeq).ToList()
                        .ForEach(item => {
                            _context.IsStorySpecUseds.SingleOrDefault(f => f.ObjId == item.ObjId && f.BldgSeq == item.BldgSeq && f.StorySeq == item.StorySeq && f.SpecUseSeq == item.SpecUseSeq).UseYn = "0";
                            _context.SaveChanges();
                        });
                    }
                }
                
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_delete";
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }

            return CreatedAtAction(nameof(DeleteStorySpecUsed), result);
        }

        [HttpPost]
        [Route("GetFireEquipList")]
        public IEnumerable<FireEquipList> GetFireEquipList([FromBody] SearchFireEquipment dto)
        {
            var predicate = PredicateBuilder.True<IsObjfireequip>();

            if (dto.BldgSeq != null)
            {
                predicate = predicate.And(p => p.BldgSeq.Equals(dto.BldgSeq));
            }

            if (dto.StorySeq != null)
            {
                predicate = predicate.And(p => p.StorySeq.Equals(dto.StorySeq));
            }

            var FireEquipList = from fireEquiplist in _context.IsObjfireequips.Where(w => w.ObjId == dto.ObjId).Where(predicate).Where(w => w.UseYn == "1").OrderBy(p => p.BldgSeq).OrderBy(p => p.StorySeq)
                                 join buildingInfo in _context.IsBldgs on fireEquiplist.BldgSeq equals buildingInfo.BldgSeq into JoinBuildingData from m1 in JoinBuildingData.DefaultIfEmpty()
                                 join storyInfo in _context.IsStories on fireEquiplist.StorySeq equals storyInfo.StorySeq into JoinStoryData from m2 in JoinStoryData.DefaultIfEmpty()
                             select new FireEquipList {
                                ObjId = fireEquiplist.ObjId,
                                BldgSeq = fireEquiplist.BldgSeq, 
                                StorySeq = fireEquiplist.StorySeq,
                                BldgName = m1.BldgName,
                                StoryNo = m2.StoryNo,
                                InExtingCnt = fireEquiplist.InExtingCnt,
                                OutExtingCnt = fireEquiplist.OutExtingCnt,
                                ExtingPumpCnt = fireEquiplist.ExtingPumpCnt,
                                SprinklerHCnt = fireEquiplist.SprinklerHCnt,
                                SprinklerAvCnt = fireEquiplist.SprinklerAvCnt,
                                SprayExtingHCnt = fireEquiplist.SprayExtingHCnt,
                                SprayExtingAvCnt = fireEquiplist.SprayExtingAvCnt,
                                PoExtingHCnt = fireEquiplist.PoExtingHCnt,
                                PoExtingAvCnt = fireEquiplist.PoExtingAvCnt,
                                CarbonDioxHCnt = fireEquiplist.CarbonDioxHCnt,
                                CarbonDioxAvCnt = fireEquiplist.CarbonDioxAvCnt,
                                HalogenCompHCnt = fireEquiplist.HalogenCompHCnt,
                                HarlogenCompAvCnt = fireEquiplist.HarlogenCompAvCnt,
                                PowderExtingHCnt = fireEquiplist.PowderExtingHCnt,
                                PowderExtingAvCnt = fireEquiplist.PowderExtingAvCnt,
                                SlideCnt = fireEquiplist.SlideCnt,
                                LadderCnt = fireEquiplist.LadderCnt,
                                RescueCnt = fireEquiplist.RescueCnt,
                                DescSlowDeviceCnt = fireEquiplist.DescSlowDeviceCnt,
                                MeasureEquipCnt = fireEquiplist.MeasureEquipCnt,
                                MeasureRopeCnt = fireEquiplist.MeasureRopeCnt,
                                SafeMatCnt = fireEquiplist.SafeMatCnt,
                                RescueEquipCnt = fireEquiplist.RescueEquipCnt,
                                EmgLightCnt = fireEquiplist.EmgLightCnt,
                                WaterExtingCnt = fireEquiplist.WaterExtingCnt,
                                ExtingWaterCnt = fireEquiplist.ExtingWaterCnt,
                                LwtrCnt = fireEquiplist.LwtrCnt,
                                WtrpipeCnt = fireEquiplist.WtrpipeCnt,
                                WaterSpringklingCnt = fireEquiplist.WaterSpringklingCnt,
                                EmgPlugCnt = fireEquiplist.EmgPlugCnt,
                                WirelessCommCnt = fireEquiplist.WirelessCommCnt,
                                ExtingCnt = fireEquiplist.ExtingCnt,
                                SimplctyExtingCnt = fireEquiplist.SimplctyExtingCnt,
                                EmgWaringCnt = fireEquiplist.EmgWaringCnt,
                                EmgBrodcCnt = fireEquiplist.EmgBrodcCnt,
                                LkgeWaringCnt = fireEquiplist.LkgeWaringCnt,
                                AutoFireFindSensCnt = fireEquiplist.AutoFireFindSensCnt,
                                AutoFireFindCircuitCnt = fireEquiplist.AutoFireFindCircuitCnt,
                                AutoFireNewsfCnt = fireEquiplist.AutoFireNewsfCnt,
                                GasLkgeWaringCnt = fireEquiplist.GasLkgeWaringCnt,
                                InduceLightCnt = fireEquiplist.InduceLightCnt,
                                InduceSignpostCnt = fireEquiplist.InduceSignpostCnt,
                                HydEquipEtcCnt = fireEquiplist.HydEquipEtcCnt,
                                ResmokeCnt = fireEquiplist.ResmokeCnt,
                                CurtainCnt = fireEquiplist.CurtainCnt,
                                CarssetteCnt = fireEquiplist.CarssetteCnt,
                                ResistEtcCnt = fireEquiplist.ResistEtcCnt 
                            };
            
            return FireEquipList.ToList();
        }

        [HttpGet]
        [Route("GetFireEquipInfo/{objId}/{bldgSeq}/{storySeq}")]
        public IsObjfireequip GetFireEquipInfo(int objId, int bldgSeq, int storySeq)
        {
            var fireEquipInfo = _context.IsObjfireequips.Where(w => w.ObjId == objId).Where(w => w.BldgSeq == bldgSeq).Where(w => w.StorySeq == storySeq).First();
            return fireEquipInfo;
        }

        [HttpPost]
        [Route("InsertFireEquip/{userId}")]
        public ActionResult<JobResultWithId> InsertFireQuip(string userId, [FromBody] IsObjfireequip dto)
        {
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                dto.UseYn="1";
                _context.IsObjfireequips.Add(dto);
                _context.SaveChanges();
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_insert";
                result.ID = dto.StorySeq.ToString();
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }
            
    
            return CreatedAtAction(nameof(InsertFireQuip), result);
        }

        [HttpPost]
        [Route("UpdateFireEquip/{userId}")]
        public ActionResult<JobResultWithId> UpdateFireEquip(string userId, [FromBody] IsObjfireequip dto)
        {
            CommonController controller = new CommonController(_context);
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                _context.IsObjfireequips.Update(dto);
                _context.SaveChanges();
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_update";
                result.ID = dto.StorySeq.ToString();
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }
                
            return CreatedAtAction(nameof(UpdateFireEquip), result);
        }

        [HttpPost("DeleteFireEquip/{userId}")]
        public ActionResult<CallResult> DeleteFireEquip([FromBody] IList<StoryPrimaryElement> Ids )
        {
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                if (Ids.Count > 0)
                {
                    foreach (StoryPrimaryElement e in Ids)
                    {
                        _context.IsObjfireequips.Where(w => w.ObjId == e.ObjId).Where(w => w.BldgSeq == e.BldgSeq).Where(w => w.StorySeq == e.StorySeq).ToList()
                        .ForEach(item => {
                            _context.IsObjfireequips.SingleOrDefault(f => f.ObjId == item.ObjId && f.BldgSeq == item.BldgSeq && f.StorySeq == item.StorySeq).UseYn = "0";
                            _context.SaveChanges();
                        });
                    }
                }
                
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_delete";
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }

            return CreatedAtAction(nameof(DeleteFireEquip), result);
        }

        [HttpGet]
        [Route("GetObjPlan/{objId}")]
        public IsObjplan GetObjPlan(int objId)
        {
            int cnt = _context.IsObjplans.Where(w => w.ObjId == objId).Where(w => w.UseYn == "1").Count();
            if (cnt > 0) 
            {
                return _context.IsObjplans.Where(w => w.ObjId == objId).Where(w => w.UseYn == "1").First();
            }
            else 
            {
                return null;
            }
        }

        [HttpPost]
        [Route("InsertObjPlan/{userId}")]
        public ActionResult<JobResultWithId> InsertObjPlan(string userId, [FromBody] IsObjplan dto)
        {
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                dto.UseYn="1";
                _context.IsObjplans.Add(dto);
                _context.SaveChanges();
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_insert";
                result.ID = dto.ObjId.ToString();
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }
            
    
            return CreatedAtAction(nameof(InsertObjPlan), result);
        }

        [HttpPost]
        [Route("UpdateObjPlan/{userId}")]
        public ActionResult<JobResultWithId> UpdateObjPlan(string userId, [FromBody] IsObjplan dto)
        {
            CommonController controller = new CommonController(_context);
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                _context.IsObjplans.Update(dto);
                _context.SaveChanges();
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_update";
                result.ID = dto.ObjId.ToString();
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }
                
            return CreatedAtAction(nameof(UpdateObjPlan), result);
        }

        [HttpPost("DeleteObjPlan/{userId}")]
        public ActionResult<CallResult> DeleteObjPlan([FromBody] IList<int> Ids )
        {
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                _context.IsObjplans.Where(p => Ids.Contains(p.ObjId)).ToList()
                .ForEach(item => {
                    _context.IsObjplans.SingleOrDefault(f => f.ObjId == item.ObjId).UseYn = "0";
                    _context.SaveChanges();
                });

                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_delete";
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }

            return CreatedAtAction(nameof(DeleteObjPlan), result);
        }

        [HttpGet]
        [Route("GetBuildingHistoryList/{objId}/{locale}")]
        public IEnumerable<ObjectHistoryList> GetBuildingHistoryList(int objId, string locale)
        {
            var buildingHistoryList = from buildinghist in _context.IsObjhists.Where(w => w.ObjId == objId).Where(w => w.UseYn == "1").OrderBy(p => p.ObjhistSeq)
                            join bldgList in _context.IsBldgs on buildinghist.BldgSeq equals bldgList.BldgSeq
                            join histTypeMaster in _context.SmCdMasters on buildinghist.HistTypeCd equals histTypeMaster.Cd into joinC1 from c1 in joinC1.DefaultIfEmpty()
                            join histTypeResource in _context.SmCdResources.Where(w => w.Locale == locale) on c1.Cd equals histTypeResource.Cd into JoinCodeData1 from r1 in JoinCodeData1.DefaultIfEmpty()
                            join struct1Master in _context.SmCdMasters on buildinghist.Struct1 equals struct1Master.Cd into joinC2 from c2 in joinC2.DefaultIfEmpty()
                            join struct1Resource in _context.SmCdResources.Where(w => w.Locale == locale) on c2.Cd equals struct1Resource.Cd into JoinCodeData2 from r2 in JoinCodeData2.DefaultIfEmpty()
                            join struct2Master in _context.SmCdMasters on buildinghist.Struct2 equals struct2Master.Cd into joinC3 from c3 in joinC3.DefaultIfEmpty()
                            join struct2Resource in _context.SmCdResources.Where(w => w.Locale == locale) on c3.Cd equals struct2Resource.Cd into JoinCodeData3 from r3 in JoinCodeData3.DefaultIfEmpty()
                            join struct3Master in _context.SmCdMasters on buildinghist.Struct3 equals struct3Master.Cd into joinC4 from c4 in joinC4.DefaultIfEmpty()
                            join struct3Resource in _context.SmCdResources.Where(w => w.Locale == locale) on c4.Cd equals struct3Resource.Cd into JoinCodeData4 from r4 in JoinCodeData4.DefaultIfEmpty()
                            select new ObjectHistoryList {
                                ObjId = buildinghist.ObjId,
                                ObjhistSeq = buildinghist.ObjhistSeq,
                                BldgName = bldgList.BldgName,
                                HistTypeCdName = string.IsNullOrEmpty(r1.CdName) ? c1.CdDesc : r1.CdName,
                                ConstUse = buildinghist.ConstUse,
                                Struct1Name = string.IsNullOrEmpty(r2.CdName) ? c2.CdDesc : r2.CdName,
                                Struct2Name = string.IsNullOrEmpty(r3.CdName) ? c3.CdDesc : r3.CdName,
                                Struct3Name = string.IsNullOrEmpty(r4.CdName) ? c4.CdDesc : r4.CdName
                            };
                        
            return buildingHistoryList.ToList();
        }

        [HttpGet]
        [Route("GetBuildingHistoryInfo/{objId}/{objhistSeq}")]
        public IsObjhist GetBuildingHistoryInfo(int objId, int objhistSeq)
        {
            var buildingHistoryInfo = _context.IsObjhists.Where(w => w.ObjId == objId).Where(w => w.ObjhistSeq == objhistSeq).First();
            
            return buildingHistoryInfo;
        }

        [HttpPost]
        [Route("InsertBuildingHistory/{userId}")]
        public ActionResult<JobResultWithId> InsertBuildingHistory(string userId, [FromBody] IsObjhist dto)
        {
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                dto.UseYn="1";
                _context.IsObjhists.Add(dto);
                _context.SaveChanges();
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_insert";
                result.ID = dto.ObjhistSeq.ToString();
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }
            
    
            return CreatedAtAction(nameof(InsertBuildingHistory), result);
        }

        [HttpPost]
        [Route("UpdateBuildingHistory/{userId}")]
        public ActionResult<JobResultWithId> UpdateBuildingHistory(string userId, [FromBody] IsObjhist dto)
        {
            CommonController controller = new CommonController(_context);
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                _context.IsObjhists.Update(dto);
                _context.SaveChanges();
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_update";
                result.ID = dto.ObjhistSeq.ToString();
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }
                
            return CreatedAtAction(nameof(UpdateBuildingHistory), result);
        }

        [HttpPost("DeleteBuildingHistory/{userId}")]
        public ActionResult<CallResult> DeleteBuildingHistory([FromBody] IList<BuildingHistoryPrimaryElement> Ids )
        {
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                if (Ids.Count > 0)
                {
                    foreach (BuildingHistoryPrimaryElement e in Ids)
                    {
                        _context.IsObjhists.Where(w => w.ObjId == e.ObjId).Where(w => w.ObjhistSeq == e.ObjhistSeq).ToList()
                        .ForEach(item => {
                            _context.IsObjhists.SingleOrDefault(f => f.ObjId == item.ObjId && f.ObjhistSeq == item.ObjhistSeq).UseYn = "0";
                            _context.SaveChanges();
                        });
                    }
                }
                
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_delete";
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }

            return CreatedAtAction(nameof(DeleteBuildingHistory), result);
        }

        [HttpGet]
        [Route("GetFireManagerList/{objId}")]
        public IEnumerable<FireManagerList> GetFireManagerList(int objId)
        {
            var fireManagerList = from fireManagers in _context.IsFmgrs.Where(w => w.ObjId == objId).Where(w => w.UseYn == "1").OrderBy(o => o.FirstName).OrderBy(o => o.LastName)
                                    select new FireManagerList {
                                        FmgrSeq = fireManagers.FmgrSeq,
                                        ObjId = fireManagers.ObjId,
                                        Name = String.Format("{0} {1}", fireManagers.FirstName, fireManagers.LastName),
                                        Birthday = fireManagers.Birthday,
                                        Title = fireManagers.Title,
                                        Address = fireManagers.Address,
                                        TelNum = fireManagers.TelNum,
                                        PubFmngYn = fireManagers.PubFmngYn,
                                        MgeAgencyYn = fireManagers.MgeAgencyYn
                                    };

            return fireManagerList.ToList();
        }

        [HttpGet]
        [Route("GetFireManagerInfo/{objId}/{fmgrSeq}")]
        public IsFmgr GetFireManagerInfo(int objId, int fmgrSeq)
        {
            var fireManagerInfo = _context.IsFmgrs.Where(w => w.ObjId == objId).Where(w => w.FmgrSeq == fmgrSeq).First();
            
            return fireManagerInfo;
        }

        [HttpPost]
        [Route("InsertFireManager")]
        public ActionResult<JobResultWithId> InsertFireManager([FromBody] IsFmgr dto)
        {
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                dto.UseYn="1";
                _context.IsFmgrs.Add(dto);
                _context.SaveChanges();
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_insert";
                result.ID = dto.FmgrSeq.ToString();
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }
            
    
            return CreatedAtAction(nameof(InsertFireManager), result);
        }

        [HttpPost]
        [Route("UpdateFireManager")]
        public ActionResult<JobResultWithId> UpdateFireManager([FromBody] IsFmgr dto)
        {
            CommonController controller = new CommonController(_context);
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                _context.IsFmgrs.Update(dto);
                _context.SaveChanges();
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_update";
                result.ID = dto.FmgrSeq.ToString();
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }
                
            return CreatedAtAction(nameof(UpdateFireManager), result);
        }

        [HttpPost("DeleteFireManager")]
        public ActionResult<CallResult> DeleteFireManager([FromBody] IList<FireManagerPrimaryElement> Ids )
        {
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                if (Ids.Count > 0)
                {
                    foreach (FireManagerPrimaryElement e in Ids)
                    {
                        _context.IsFmgrs.Where(w => w.ObjId == e.ObjId).Where(w => w.FmgrSeq == e.FmgrSeq).ToList()
                        .ForEach(item => {
                            _context.IsFmgrs.SingleOrDefault(f => f.ObjId == item.ObjId && f.FmgrSeq == item.FmgrSeq).UseYn = "0";
                            _context.SaveChanges();
                        });
                    }
                }
                
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_delete";
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }

            return CreatedAtAction(nameof(DeleteFireManager), result);
        }

        [HttpGet]
        [Route("GetSafeManagerList/{objId}")]
        public IEnumerable<SafeManagerList> GetSafeManagerList(int objId)
        {
            var safeManagerList = from safeManagers in _context.IsDgrmgrs.Where(w => w.ObjId == objId).Where(w => w.UseYn == "1").OrderBy(o => o.FirstName).OrderBy(o => o.LastName)
                                    select new SafeManagerList {
                                        DgrmgrSeq = safeManagers.DgrmgrSeq,
                                        ObjId = safeManagers.ObjId,
                                        Name = String.Format("{0} {1}", safeManagers.FirstName, safeManagers.LastName),
                                        Address = safeManagers.Address,
                                        TelNum = safeManagers.TelNum
                                    };

            return safeManagerList.ToList();
        }

        [HttpGet]
        [Route("GetSafeManagerInfo/{objId}/{dgrmgrSeq}")]
        public IsDgrmgr GetSafeManagerInfo(int objId, int dgrmgrSeq)
        {
            var safeManagerInfo = _context.IsDgrmgrs.Where(w => w.ObjId == objId).Where(w => w.DgrmgrSeq == dgrmgrSeq).First();
            
            return safeManagerInfo;
        }

        [HttpPost]
        [Route("InsertSafeManager")]
        public ActionResult<JobResultWithId> InsertSafeManager([FromBody] IsDgrmgr dto)
        {
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                dto.UseYn="1";
                _context.IsDgrmgrs.Add(dto);
                _context.SaveChanges();
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_insert";
                result.ID = dto.DgrmgrSeq.ToString();
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }
            
    
            return CreatedAtAction(nameof(InsertSafeManager), result);
        }

        [HttpPost]
        [Route("UpdateSafeManager")]
        public ActionResult<JobResultWithId> UpdateSafeManager([FromBody] IsDgrmgr dto)
        {
            CommonController controller = new CommonController(_context);
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                _context.IsDgrmgrs.Update(dto);
                _context.SaveChanges();
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_update";
                result.ID = dto.DgrmgrSeq.ToString();
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }
                
            return CreatedAtAction(nameof(UpdateSafeManager), result);
        }

        [HttpPost("DeleteSafeManager")]
        public ActionResult<CallResult> DeleteSafeManager([FromBody] IList<SafeManagerPrimaryElement> Ids )
        {
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                if (Ids.Count > 0)
                {
                    foreach (SafeManagerPrimaryElement e in Ids)
                    {
                        _context.IsDgrmgrs.Where(w => w.ObjId == e.ObjId).Where(w => w.DgrmgrSeq == e.DgrmgrSeq).ToList()
                        .ForEach(item => {
                            _context.IsDgrmgrs.SingleOrDefault(f => f.ObjId == item.ObjId && f.DgrmgrSeq == item.DgrmgrSeq).UseYn = "0";
                            _context.SaveChanges();
                        });
                    }
                }
                
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_delete";
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }

            return CreatedAtAction(nameof(DeleteSafeManager), result);
        }

        [HttpGet]
        [Route("GetManagerList/{objId}/{locale}")]
        public IEnumerable<ManagerList> GetManagerList(int objId, string locale)
        {
            var managerList = from managers in _context.IsObjrels.Where(w => w.ObjId == objId).Where(w => w.UseYn == "1").OrderBy(o => o.FirstName).OrderBy(o => o.LastName)
                                join relCdMaster in _context.SmCdMasters on managers.RelCd equals relCdMaster.Cd into joinC1 from c1 in joinC1.DefaultIfEmpty()
                                join relCdResource in _context.SmCdResources.Where(w => w.Locale == locale) on c1.Cd equals relCdResource.Cd into JoinCodeData1 from r1 in JoinCodeData1.DefaultIfEmpty()
                                  select new ManagerList {
                                    RelSeq = managers.RelSeq,
                                    ObjId = managers.ObjId,
                                    Name = String.Format("{0} {1}", managers.FirstName, managers.LastName),
                                    Birthday = managers.Birthday,
                                    RelCdName = string.IsNullOrEmpty(r1.CdName) ? c1.CdDesc : r1.CdName,
                                    Address = managers.Address,
                                    TelNum = managers.TelNum
                                  };

            return managerList.ToList();
        }

        [HttpGet]
        [Route("GetManagerInfo/{objId}/{relSeq}")]
        public IsObjrel GetManagerInfo(int objId, int relSeq)
        {
            var managerInfo = _context.IsObjrels.Where(w => w.ObjId == objId).Where(w => w.RelSeq == relSeq).First();
            
            return managerInfo;
        }

        [HttpPost]
        [Route("InsertManager")]
        public ActionResult<JobResultWithId> InsertManager([FromBody] IsObjrel dto)
        {
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                dto.UseYn="1";
                _context.IsObjrels.Add(dto);
                _context.SaveChanges();
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_insert";
                result.ID = dto.RelSeq.ToString();
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }
            
    
            return CreatedAtAction(nameof(InsertManager), result);
        }

        [HttpPost]
        [Route("UpdateManager")]
        public ActionResult<JobResultWithId> UpdateManager([FromBody] IsObjrel dto)
        {
            CommonController controller = new CommonController(_context);
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                _context.IsObjrels.Update(dto);
                _context.SaveChanges();
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_update";
                result.ID = dto.RelSeq.ToString();
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }
                
            return CreatedAtAction(nameof(UpdateManager), result);
        }

        [HttpPost("DeleteManager")]
        public ActionResult<CallResult> DeleteManager([FromBody] IList<ManagerPrimaryElement> Ids )
        {
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                if (Ids.Count > 0)
                {
                    foreach (ManagerPrimaryElement e in Ids)
                    {
                        _context.IsObjrels.Where(w => w.ObjId == e.ObjId).Where(w => w.RelSeq == e.RelSeq).ToList()
                        .ForEach(item => {
                            _context.IsObjrels.SingleOrDefault(f => f.ObjId == item.ObjId && f.RelSeq == item.RelSeq).UseYn = "0";
                            _context.SaveChanges();
                        });
                    }
                }
                
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_delete";
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }

            return CreatedAtAction(nameof(DeleteManager), result);
        }
    }
}