using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using BD_Safer.model;
using Newtonsoft.Json.Linq;

namespace BD_Safer.Controllers
{
    public class CommonController: ControllerBase
    {
        private readonly bd_saferContext _context;

        public CommonController(bd_saferContext context)
        {
            _context = context;
        }

        public enum TableType
        {
            SmUser,
            ISObj,
            SmNotification
        }
    
        public int MakeSeq(TableType tableType)
        {
            string json = _context.SmSysenvs.Where(w => w.Id == "E01").FirstOrDefault().Values;
            int SeqNumber = 1;

            switch (tableType)
            {
                case TableType.ISObj :
                    if (_context.IsObjs.Count() > 0)
                    {
                        SeqNumber = _context.IsObjs.Max(p => p.ObjId) + 1;
                    }
                    else 
                    {
                        SeqNumber = ((int)JObject.Parse(json)["code"] * 10000000) + 1;
                    }
                    break;
                case TableType.SmUser :
                    if (_context.HrUsers.Count() > 0)
                    {
                        SeqNumber = _context.HrUsers.Max(p => p.UserSeq) + 1;
                    }
                    else 
                    {
                        SeqNumber = ((int)JObject.Parse(json)["code"] * 100000) + 1;
                    }
                    
                    break;
                case TableType.SmNotification :
                    if (_context.SmNotifications.Count() > 0)
                    {
                        SeqNumber = _context.SmNotifications.Max(p => p.NotificationId) + 1;
                    }
                    else 
                    {
                        SeqNumber = ((int)JObject.Parse(json)["code"] * 100000000) + 1;
                    }
                    break;
            }
            
            return SeqNumber;
        }

        public string MakeDateTimeFormat(DateTime dateTime)
        {
            return dateTime.ToString("MM/dd/yyyy HH:mm:ss");
        }
    }    
}
