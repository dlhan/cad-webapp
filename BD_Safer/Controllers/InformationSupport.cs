using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using BD_Safer.model;
using BD_Safer.model.dto;
using Newtonsoft.Json.Linq;

namespace BD_Safer.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class InformationSupportController : ControllerBase
    {
        private readonly bd_saferContext _context;

        public InformationSupportController(bd_saferContext context)
        {
            _context = context;
        }

        [HttpGet]
        [Route("GetWardInfo/{wardId}/{locale}")]
        public WardInfo GetWardInfo(string wardId, string locale)
        {
            var wardInfo = from wardList in _context.IsWards.Where(w => w.WardId == wardId)
                            join masterCd in _context.SmCdMasters on wardList.TypeClsCd equals masterCd.Cd
                            join resourceCd in _context.SmCdResources.Where(w => w.Locale == locale) on masterCd.Cd equals resourceCd.Cd into JoinCodeData from m1 in JoinCodeData.DefaultIfEmpty()
                           select new WardInfo {    
                               WardId = wardList.WardId, 
                               UpwardId  = wardList.UpwardId, 
                               FireOfficeId = wardList.FireOfficeId, 
                               WardName = wardList.WardName, 
                               TypeClsCd = wardList.TypeClsCd, 
                               TypeClsName = string.IsNullOrEmpty(m1.CdName) ? masterCd.CdDesc : m1.CdName, 
                               NickName = wardList.NickName, 
                               ZipCode = wardList.ZipCode, 
                               Address = wardList.Address, 
                               DspGroupYn = wardList.DspGroupYn, 
                               AutoListYn = wardList.AutoListYn, 
                               DayTel = wardList.DayTel, 
                               NightTel = wardList.NightTel, 
                               ExtTel = wardList.ExtTel, 
                               WardopenDate = wardList.WardopenDate, 
                               WardcloseDate = wardList.WardcloseDate, 
                               WardopenYn = wardList.WardopenYn, 
                               BrdUsingYn = wardList.BrdUsingYn, 
                               Remark = wardList.Remark, 
                               WardOrder = wardList.WardOrder, 
                               X = wardList.X, 
                               Y = wardList.Y, 
                               DayFax = wardList.DayFax, 
                               NightFax = wardList.NightFax, 
                               UseYn = wardList.UseYn
                           };
             return wardInfo.First();
        }

        [HttpGet]
        [Route("GetUpperWardListByTypeClsCd/{typeClsCd}")]
        public IEnumerable<IsWard> GetUpperWardListByTypeClsCd(int typeClsCd)
        {
            IEnumerable<IsWard> db = null;

            if (typeClsCd == 2)
            {
                 db = _context.IsWards.Where(w => w.TypeClsCd == 1).OrderBy(o => o.WardOrder);
            }
            else if (typeClsCd == 3 || typeClsCd == 4 || typeClsCd == 5 || typeClsCd == 6)
            {
                 db = _context.IsWards.Where(w => w.TypeClsCd == 2).OrderBy(o => o.WardOrder);
            }
            else if (typeClsCd == 8)
            {
                db = _context.IsWards.Where(w => w.TypeClsCd == 1).OrderBy(o => o.WardOrder);
            }
            else if (typeClsCd == 9)
            {
                db = _context.IsWards.Where(w => w.TypeClsCd == 2).OrderBy(o => o.WardOrder);
            }
            else if (typeClsCd == 10)
            {
                int?[] FireOfficeCds = new int?[]{3,4,5,6};
                db = _context.IsWards.Where(w => FireOfficeCds.Contains(w.TypeClsCd)).OrderBy(o => o.WardOrder);
            }

            return db.ToList();
        }

        [HttpGet]
        [Route("GetWardListForUpperOffice/{upwardId}")]
        public IEnumerable<IsWard> GetWardListForUpperOffice(string upwardId)
        {
            var db = _context.IsWards.Where(p => p.UpwardId == upwardId).OrderBy(o => o.WardOrder);
            return db;
        }

        [HttpPost]
        [Route("GetUpperwardList")]
        public IEnumerable<IsWard> GetUpperwardList([FromBody] IList<int?> codes)
        {
            var db = _context.IsWards.Where(p => codes.Contains(p.TypeClsCd)).OrderBy(o => o.WardId).OrderBy(o => o.WardOrder);
            return db;
        }

        [HttpGet]
        [Route("GetWardListForTypeClsCd/{typeClsCd}")]
        public IEnumerable<IsWard> GetWardListForTypeClsCd(int typeClsCd)
        {
            var db = _context.IsWards.Where(p => p.TypeClsCd == typeClsCd).OrderBy(o => o.WardOrder);
            return db;
        }

        [HttpGet]
        [Route("GetWardListForUpperOffice")]
        public IEnumerable<IsWard> GetWardListForUpperOffice()
        {
            var db = _context.IsWards.Where(p => p.UpwardId == null);
            return db;
        }

        [HttpPost]
        [Route("WardList/{locale}")]
        public ActionResult<SearchWardResult> WardList(string locale, [FromBody] SearchWard dto)
        {
            string UpperWardId = dto.UpwardId;
            string WardId = dto.WardId;
            int? TypeCode = dto.TypeClsCd;
            int RowsPerPage = dto.page.RowsPerPage;
            int PageNumber = dto.page.PageNumber;
            int TotalCount = 0;
            
            SearchWardResult wardResult = new SearchWardResult();
                        
            var predicate = PredicateBuilder.True<IsWard>();

            if (UpperWardId != null)
            {
                predicate = predicate.And(p => p.UpwardId.Equals(UpperWardId));
            }

            if (WardId != null)
            {
                predicate = predicate.And(p => p.WardId.Equals(WardId));
            }

            if (TypeCode != null)
            {
                predicate = predicate.And(p => p.TypeClsCd.Equals(TypeCode));
            }

            var WardList = from wardList in _context.IsWards.Where(predicate).Where(w => w.UseYn == "1").OrderBy(p => p.WardName).OrderBy(p => p.WardId).Skip(RowsPerPage * (PageNumber - 1)).Take(RowsPerPage)
                           join masterCd in _context.SmCdMasters on wardList.TypeClsCd equals masterCd.Cd
                           join resourceCd in _context.SmCdResources.Where(w => w.Locale == locale) on masterCd.Cd equals resourceCd.Cd into JoinCodeData from m1 in JoinCodeData.DefaultIfEmpty()
                           join uppwardList in _context.IsWards on wardList.UpwardId equals uppwardList.WardId  into JoinData
                           from m2 in JoinData.DefaultIfEmpty()
                             select new SearchWardInfo {
                                WardId = wardList.WardId,
                                UpwardName = m2.WardName,
                                TypeCode = string.IsNullOrEmpty(m1.CdName) ? masterCd.CdDesc : m1.CdName,
                                WardName = wardList.WardName,
                                NickName = wardList.NickName,
                                DayTel = wardList.DayTel,
                                NightTel = wardList.NightTel,
                                Address = wardList.Address,
                                BrdUsingYn = wardList.BrdUsingYn 
                             };
            
            TotalCount = _context.IsWards.Where(predicate).Where(w => w.UseYn == "1").Count();
            wardResult.page = new Page();
            wardResult.page.PageNumber = dto.page.PageNumber;
            wardResult.page.RowsPerPage = dto.page.RowsPerPage;
            wardResult.page.TotalCount = TotalCount;
            wardResult.Rows = WardList;

            return CreatedAtAction(nameof(WardList), wardResult);
        }

        [HttpPost]
        [Route("InsertWard/{userId}")]
        public ActionResult<JobResultWithId> InsertWard(string userId, [FromBody] IsWard dto)
        {
            CommonController controller = new CommonController(_context);
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                string Json = _context.SmSysenvs.Where(w => w.Id == "E01").FirstOrDefault().Values;
                int DivisionCode = (int)JObject.Parse(Json)["code"];
                int MidCount = 0;
                int WardId = 0;
                string FireOfficeId = "";

                if (dto.TypeClsCd == 2) //Division HQ
                {
                    MidCount = _context.IsWards.Where(w => w.TypeClsCd == 2).Where(w => w.WardId != null && w.WardId.Substring(0,2) == DivisionCode.ToString()).Count() + 1;
                    WardId = DivisionCode * 10000000 + MidCount * 1000000;
                    FireOfficeId = WardId.ToString();
                }
                else if (dto.TypeClsCd == 3 || dto.TypeClsCd == 4 || dto.TypeClsCd == 5 || dto.TypeClsCd == 6)
                {
                    int?[] FireOfficeCds = new int?[]{3,4,5,6};
                    var MaxWard = _context.IsWards.Where(w => FireOfficeCds.Contains(w.TypeClsCd)).Where(w => w.WardId != null && w.WardId.Substring(0,3) == dto.UpwardId.Substring(0,3)).Max(m => m.WardId);
                    if (MaxWard == null)
                    {
                        MidCount = 1;
                    }
                    else
                    {
                        MidCount = int.Parse(MaxWard.Substring(3,3)) + 1;
                    }
                    WardId = int.Parse(dto.UpwardId.Substring(0,3)) * 1000000 + MidCount * 1000;
                    FireOfficeId = WardId.ToString();
                }
                else
                {
                    int?[] DepartmentCds = new int?[]{9,10};
                    var MaxWard = _context.IsWards.Where(w => DepartmentCds.Contains(w.TypeClsCd)).Where(w => w.WardId != null && w.WardId.Substring(0,6) ==  dto.UpwardId.Substring(0,6)).Max(m => m.WardId);

                    if (MaxWard == null)
                    {
                        MidCount = 1;
                    }
                    else
                    {
                        MidCount = int.Parse(MaxWard.Substring(6,3)) + 1;
                    }

                    WardId = int.Parse(dto.UpwardId.Substring(0,6)) * 1000 + MidCount;
                    FireOfficeId = dto.UpwardId;
                }

                dto.WardId = WardId.ToString();
                dto.FireOfficeId = FireOfficeId;
                dto.LchgUserId = userId;
                dto.LchgDtime = controller.MakeDateTimeFormat(DateTime.Now);
                dto.UseYn = "1";
                _context.IsWards.Add(dto);
                _context.SaveChanges();
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_insert";
                result.ID = dto.WardId;
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }
            
    
            return CreatedAtAction(nameof(InsertWard), result);
        }

        [HttpPost]
        [Route("UpdateWard/{userId}")]
        public ActionResult<JobResultWithId> UpdateWard(string userId, [FromBody] IsWard dto)
        {
            CommonController controller = new CommonController(_context);
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                dto.LchgUserId = userId;
                dto.LchgDtime = controller.MakeDateTimeFormat(DateTime.Now);
                _context.IsWards.Update(dto);
                _context.SaveChanges();
                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_update";
                result.ID = dto.WardId;
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }
            
    
            return CreatedAtAction(nameof(UpdateWard), result);
        }

        [HttpPost("DeleteWard")]
        public ActionResult<CallResult> DeleteWard([FromBody] IList<string> Ids )
        {
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                _context.IsWards.Where(p => Ids.Contains(p.WardId)).ToList()
                .ForEach(item => {
                    _context.IsWards.SingleOrDefault(f => f.WardId == item.WardId).UseYn = "0";
                    _context.SaveChanges();
                });

                result.result.IsSuccess = true;
                result.result.Message = "msg_success_to_delete";
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }

            return CreatedAtAction(nameof(DeleteWard), result);
        }

        [HttpPost("GetWardsForType")]
        public IEnumerable<IsWard> GetWardsForType([FromBody] IList<int?> type_cls_cds )
        {
            var db = _context.IsWards.Where(p => type_cls_cds.Contains(p.TypeClsCd));
            return db.ToList();
        }
    }
}