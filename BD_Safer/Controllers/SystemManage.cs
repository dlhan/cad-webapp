using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using BD_Safer.model;
using BD_Safer.model.dto;
using BD_Safer.model.dto.SystemManage;

namespace BD_Safer.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SystemManageController : ControllerBase
    {
        private readonly bd_saferContext _context;

        public SystemManageController(bd_saferContext context)
        {
            _context = context;
        }

        [Route("LogIn")]
        public ActionResult<UserInfo> LogIn([FromBody] LogIn dto)
        {
            string userId = dto.UserId;
            string Password = dto.UserPw;
            UserInfo userInfo = new UserInfo();
            CallResult result = new CallResult();
  
            var data = _context.HrUsers.Where(f => f.UserId == userId && f.UserPw == Password).Where(f => f.UseYn == "1").ToList();

            if (data.Count > 0)
            {
                var UserPreference = _context.SmUserPreferences.Where(f => f.UserId == userId).FirstOrDefault();

                result.IsSuccess = true;
                result.Message = "Succee to Login.";
                userInfo.userInfo = data.First();
                userInfo.userPreference = UserPreference;
                userInfo.result = result;

                userInfo.userAuth =_context.SmUserprivs.Where(f => f.UserId == userId).Where(f => f.ApprovedYn == "1")
                               .Join(
                                     _context.SmMenuprivs,
                                         auth => auth.PrivId,
                                         menu => menu.PrivId,
                                         (auth,menu) => new {
                                                                menuId = menu.MenuId, privilige = menu.Level
                                                            }
                                ).GroupBy(g => new {
                                    g.menuId
                                }).Select(s => new BD_Safer.model.dto.UserAuth{
                                    MenuId = s.Key.menuId,
                                    Privilege = s.Max(e => e.privilige)
                                }).ToList();
            }
            else
            {
                result.IsSuccess = false;
                result.Message = "Fail to Login.";
                userInfo.result = result;
            }
            
            return CreatedAtAction(nameof(LogIn), userInfo);
        }

        [HttpGet]
        [Route("GetNextGroupCode/{groupCode}/{locale}")]
        public ActionResult<GroupCodeInfo> GetNextGroupCode(string groupCode, string locale)
        {
            GroupCodeInfo info = new GroupCodeInfo();
            var GropCode = from groupMaster in _context.SmCdgroupMasters.Where(w => w.CdGrp == groupCode)
                            join groupResource in _context.SmCdgroupResources.Where(w => w.Locale == locale) on groupMaster.CdGrp equals groupResource.CdGrp into JoinData
                            from e in JoinData.DefaultIfEmpty()
                            select new SmCdgroupResource {
                                CdGrpName = string.IsNullOrEmpty(e.CdGrpName) ? groupMaster.CdGrpDesc : e.CdGrpName
                            };
            
            string Grpname = GropCode.FirstOrDefault().CdGrpName;
            
            var db = from groupMaster in _context.SmCdgroupMasters.Where(w => w.UpperCdGrp == groupCode).Where(w => w.UseYn == "1").OrderBy(o => o.SortOrder)
                      join groupResource in _context.SmCdgroupResources.Where(w => w.Locale == locale) on groupMaster.CdGrp equals groupResource.CdGrp into JoinData
                      from e in JoinData.DefaultIfEmpty()
                      select new GroupCodes {
                        CdGrp = groupMaster.CdGrp,
                        CdGrpName = string.IsNullOrEmpty(e.CdGrpName) ? groupMaster.CdGrpDesc : e.CdGrpName,
                        CdGrpDesc = string.IsNullOrEmpty(e.CdGrpDesc) ? groupMaster.CdGrpDesc : e.CdGrpDesc,
                        LeafYn = groupMaster.LeafYn
                      };
            info.GroupCodeTitle = Grpname;
            info.rows = db;
            return info;
        }

        [HttpGet]
        [Route("GetCodesForGroupCode/{groupCode}/{locale}")]
        public IEnumerable<SmCdResource> GetCodesForGroupCode(string groupCode, string locale)
        {
            var db = from cdGroupMatch in _context.SmCdgroupcds.Where(w => w.CdGrp == groupCode).OrderBy(o => o.SortOrder)
                     join masterCd in _context.SmCdMasters.Where(w => w.UseYn == "1") on cdGroupMatch.Cd equals masterCd.Cd
                     join resourceCd in _context.SmCdResources.Where(w => w.Locale == locale) on masterCd.Cd equals resourceCd.Cd into JoinData
                     from e in JoinData.DefaultIfEmpty()
                     select new SmCdResource {
                       Cd = masterCd.Cd,
                       CdName = string.IsNullOrEmpty(e.CdName) ? masterCd.CdDesc : e.CdName,
                       CdDesc = string.IsNullOrEmpty(e.CdDesc) ? masterCd.CdDesc : e.CdDesc
                     };
            
            return db.ToList();
        }

         [HttpGet]
        [Route("GetNotification/{locale}")]
        public IEnumerable<NotificationInfo> GetNotification(string locale)
        {
            var db = from notifications in _context.SmNotifications.Where(p => p.CompletedYn == "0")
                     join people in _context.HrUsers on notifications.UserId equals people.UserId
                     join nofiticatonTypeCd in _context.SmCdMasters on notifications.NotificationTypeCd equals nofiticatonTypeCd.Cd
                     join cdResource1 in _context.SmCdResources.Where(w => w.Locale == locale) on nofiticatonTypeCd.Cd equals cdResource1.Cd into joinCd1 from c1 in joinCd1.DefaultIfEmpty()
                     join stateCd in _context.SmCdMasters on notifications.StateCd equals stateCd.Cd
                     join cdResource2 in _context.SmCdResources.Where(w => w.Locale == locale) on stateCd.Cd equals cdResource2.Cd into joinCd2 from c2 in joinCd2.DefaultIfEmpty()
                     join wardlist in _context.IsWards on people.WardId equals wardlist.WardId into JoinData
                     from m in JoinData.DefaultIfEmpty()
                       select new NotificationInfo {
                           NotificationId  = notifications.NotificationId,
                           NotificationType = string.IsNullOrEmpty(c1.CdName) ? nofiticatonTypeCd.CdDesc : c1.CdName,
                           State = string.IsNullOrEmpty(c2.CdName) ? stateCd.CdDesc : c2.CdName,
                           CreatedDate = notifications.CreatedDate,
                           UserId = notifications.UserId,
                           FirstName = people.FirstName,
                           LastName  = people.LastName,
                           WardName = (m == null ? String.Empty : m.WardName)
                       };

            return db.ToList();
        }

        [HttpPost("ApproveNotification")]
        public ActionResult<CallResult> ApproveNotification([FromBody] IList<int> Ids )
        {
            CommonController controller = new CommonController(_context);
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                _context.SmNotifications.Where(p => Ids.Contains(p.NotificationId)).ToList()
                .ForEach(item => {
                    switch (item.NotificationTypeCd)
                    {
                        case 15 :
                            _context.HrUsers.SingleOrDefault(f => f.UserSeq == item.RelatedSeq).UseYn = "1";
                            _context.SaveChanges();
                            break;
                        case 16 :
                            //_context.UserAuths.SingleOrDefault(f => f.Id == item.RelatedId).IsApproved = "1";
                            //_context.SaveChanges();
                            break;
                    }

                    _context.SmNotifications.SingleOrDefault(f => f.NotificationId == item.NotificationId).CompletedDate = controller.MakeDateTimeFormat(DateTime.Now);
                    _context.SmNotifications.SingleOrDefault(f => f.NotificationId == item.NotificationId).CompletedYn = "1"; //0:Not approved, 1:Approved, 2:Rejected
                    _context.SmNotifications.SingleOrDefault(f => f.NotificationId == item.NotificationId).StateCd = 119; //0:Not approved, 1:Approved, 2:Rejected
                    _context.SaveChanges();
                });
                result.result.IsSuccess = true;
                result.result.Message = "approved";
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }

            return CreatedAtAction(nameof(ApproveNotification), result);
        }

        [HttpPost("RejectNotification")]
        public ActionResult<CallResult> RejectNotification([FromBody] IList<int> Ids )
        {
            CommonController controller = new CommonController(_context);
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                _context.SmNotifications.Where(p => Ids.Contains(p.NotificationId)).ToList()
                .ForEach(item => {
                    switch (item.NotificationTypeCd)
                    {
                        case 15 :
                            _context.HrUsers.SingleOrDefault(f => f.UserSeq == item.RelatedSeq).UseYn = "0";
                            _context.SaveChanges();
                            break;
                        case 16 :
                            _context.SmUserprivs.SingleOrDefault(f => f.UserprivSeq == item.RelatedSeq).ApprovedYn = "2"; //0:Not approved, 1:Approved, 2:Rejected
                            _context.SaveChanges();
                            break;
                    }

                    _context.SmNotifications.SingleOrDefault(f => f.NotificationId == item.NotificationId).CompletedDate = controller.MakeDateTimeFormat(DateTime.Now);
                    _context.SmNotifications.SingleOrDefault(f => f.NotificationId == item.NotificationId).CompletedYn = "2"; //0:Not approved, 1:Approved, 2:Rejected
                    _context.SmNotifications.SingleOrDefault(f => f.NotificationId == item.NotificationId).StateCd = 17;
                    _context.SaveChanges();
                });
                result.result.IsSuccess = true;
                result.result.Message = "rejected";
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }

            return CreatedAtAction(nameof(RejectNotification), result);
        }
    }
}