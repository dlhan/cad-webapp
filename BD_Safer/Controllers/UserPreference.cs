using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using BD_Safer.model;
using BD_Safer.model.dto;

namespace Safer.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class UserPreferenceController : ControllerBase
    {
        private readonly bd_saferContext _context;

        public UserPreferenceController(bd_saferContext context)
        {
            _context = context;
        }

        [HttpGet]
        [Route("GetUserPreference/{userId}")]
        public ActionResult<SmUserPreference> GetUserPreference(string userId)
        {
            return _context.SmUserPreferences.Where(p => p.UserId == userId).FirstOrDefault();
        }

        [HttpPost]
        [Route("UpsertUserPreference")]
        public ActionResult<JobResultWithId> UpsertUserPreference([FromBody] SmUserPreference dto)
        {
            JobResultWithId result = new JobResultWithId();
            result.result = new CallResult();

            try
            {
                int cnt = _context.SmUserPreferences.Where(w => w.UserId == dto.UserId).Count();
                
                if (cnt > 0) {
                    _context.SmUserPreferences.Update(dto);
                    _context.SaveChangesAsync();
                    result.result.IsSuccess = true;
                    result.result.Message = "msg_success_to_update";
                    result.ID = "1";
                }
                else
                {
                    _context.SmUserPreferences.Add(dto);
                    _context.SaveChanges();
                    result.result.IsSuccess = true;
                    result.result.Message = "msg_success_to_update";
                    result.ID = "1";
                }
            }
            catch(Exception ex)
            {
                result.result.IsSuccess = false;
                result.result.Message = ex.Message;
            }
            
            return CreatedAtAction(nameof(UpsertUserPreference), result);
        }
    }
}