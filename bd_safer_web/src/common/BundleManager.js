import i18n from '../locale/i18n.js';

export function GetText(key, params) {
    var result = i18n.t(key)

        if(params) {
            if (Array.isArray(params)) {
                params.array.forEach(function(element,index, param){
                    var search = "{" + index + "}"
                    result = result.replace(search, param)
                });
            }
        }

    return result
}

export function ChangeLanguage(lang) {
    i18n.changeLanguage(lang);
}