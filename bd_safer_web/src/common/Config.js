class Config {
    SnackbarCloseTime = 5000;
    ReadWriteAuthCode = 11;
    ReadOnlyAuthCode = 12;
}

export default Config;