import React, { createContext, useReducer, useContext } from 'react';
import { ChangeLanguage } from '../common/BundleManager'
import Dictionary from '../common/Dictionary';

const dictionary = new Dictionary();

const userContext = {
    userInfo : {
      userSeq:null,
      userId:null,
      wardId:null,
      email:null,
      isAdmin:false
    },
    preference : {
      language: "en",
      theme: null,
      dateFormat:"MM/dd/yyyy",
      color:"primary",
      tableRowsCount: 20,
      tableIsDense:true
    },
    auth:[],
};

const setUserInfo = data => {
  userContext.userInfo.userSeq = data.userInfo.userSeq;
  userContext.userInfo.userId = data.userInfo.userId;
  userContext.userInfo.wardId = data.userInfo.wardId;
  userContext.userInfo.email = data.userInfo.email;
  userContext.userInfo.isAdmin = data.userInfo.sysmgrYn;
  
  if (data.userPreference) {
    userContext.preference.language = data.userPreference.language !== "" ? data.userPreference.language : "en";
    userContext.preference.theme = data.userPreference.theme !== "" ? data.userPreference.theme : "";
    userContext.preference.color = data.userPreference.color !== "" ? data.userPreference.color : "primary";
    userContext.preference.tableIsDense = data.userPreference.tableDense !== "" ? data.userPreference.tableDense : true;
    userContext.preference.tableRowsCount = data.userPreference.tableRows !== "" ? data.userPreference.tableRows : 20;
  }

  ChangeLanguage(data.userPreference.language);

  data.userAuth.map(auth => 
    userContext.auth.push(auth)
  );
   
  return userContext;
}

const changeLang = data => {
  userContext.preference.language = data;
  ChangeLanguage(data);
  return userContext;
}

const changeUserPreference = data => {
  changeLang(data.language);
  userContext.preference.theme = data.theme;
  userContext.preference.color = data.color;
  userContext.preference.tableRowsCount = data.tableRows;
  userContext.preference.tableIsDense = data.tableDense;
  return userContext;
}


function userReducer(state, action) {
  switch (action.type) {
    case dictionary.CHANGE_LANG :
      return changeLang(action.data); 
    case dictionary.SET_USER_INFO :
      return setUserInfo(action.data);
    case dictionary.CHANGE_USER_PREFERENCE :
      return changeUserPreference(action.data);
    default:
      throw new Error(`Unhandled action type: ${action.type}`);
  }
}

const UserStateContext = createContext(null);
const UserDispatchContext = createContext(null);

export function UserContextProvider({ children }) {
  const [state, dispatch] = useReducer(userReducer, userContext);
  return (
    <UserStateContext.Provider value={state}>
      <UserDispatchContext.Provider value={dispatch}>
        {children}
      </UserDispatchContext.Provider>
    </UserStateContext.Provider>
  );
}

export function useUserContext() {
  const context = useContext(UserStateContext);
  if (!context) {
    throw new Error('Cannot find UserContextProvider');
  }
  return context;
}

export function useUserDispatch() {
  const context = useContext(UserDispatchContext);
  if (!context) {
    throw new Error('Cannot find UserContextProvider');
  }
  return context;
}