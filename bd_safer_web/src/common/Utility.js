import { useUserContext } from '../common/UserContext';
import Dictionary from './Dictionary';
import { GetText } from '../common/BundleManager';

const dictionary = new Dictionary();

export function StringUrlFormatFromJson(str, json) {
  Object.keys(json).forEach(key => {
    var reg = "{" + key + "}";
      str = str.replace(reg, json[key])
  })
        
  return str
}

export function ConvertResponseToForm(serviceInfo, responseData) {
  let formedData = {};
  let serviceFormat = serviceInfo.response;
  if (!responseData && typeof responseData !== 'object') return;

  Object.keys(serviceFormat).forEach(key => {
    Object.keys(responseData).forEach(res => {
      if (key.toLowerCase() === res.toLowerCase()) {
        if (Array.isArray(serviceFormat[key])) {
          formedData[key] = responseData[res];
        } else if ( typeof serviceFormat[key] === 'object') { 
          formedData[key] = {};
          ConvertResponseObjectToForm(serviceFormat[key], responseData[res], formedData[key]);
        } else {
          formedData[key] = ConvertValueForType(serviceFormat[key],responseData[res]);
        }
      }
    });
  });

  return formedData;
}

function ConvertResponseObjectToForm(source, response, finalData) {
  Object.keys(source).forEach(key => {
    if (response) {
      for (let res of Object.keys(response)) {
        if (key.toLowerCase() === res.toLowerCase()) {
          if (typeof source[key] === 'object') {
            finalData[key] = {};
            if (response[res] !== null) {
              ConvertResponseObjectToForm(source[key], response[res], finalData[key]);
            }
            
          } else {
            finalData[key] = ConvertValueForType(source[key],response[res]);
          }
          continue;
        }
      }
    } else {
      finalData[key] = "";
    }
    
  });
}

export function ConvertDataToRows(serviceInfo, responseData, keyForRowId) {
  let rows = [];
  let row = {};
  let requiredResponse = serviceInfo.response.rows;

  if (Array.isArray(responseData)) {
    responseData.forEach(item=>{
      row = CompareJsonData(requiredResponse, item);
      row["rowId"] = row[keyForRowId];
      rows.push(row);
    })
  } else {
    row = CompareJsonData(requiredResponse, responseData);
    row["rowId"] = row[keyForRowId];
    rows.push(row);
  }

  return rows;
}

function ConvertValueForType(type, value) {

  switch(type) {
    case dictionary.BOOLEANYN :
      return value === "1" ? GetText("yes") : GetText("no");
    case dictionary.BOOLEANOnOff :
      return value === "1" ? GetText("on") : GetText("off");
    case dictionary.BOOLEAN :
      return value === "1" ? true : false;
    case dictionary.BOOLEANUseOrNot :
      return value === "1" ? GetText("use") : GetText("no_use");
    case dictionary.DATE :
      let date;
      let dateFormat = value;
      if (value) {
        try {
          date = new Date(value);
          dateFormat = date.getFullYear() +"-"+ makeTowDigit(date.getMonth() + 1) + "-" + makeTowDigit(date.getDate());
        } catch {}
      }
      return dateFormat;
    default :
      return value;
  }
}

function makeTowDigit(value) {
  var returnVal = value;
  if (value < 10) {
    returnVal = "0" + value;
  } 
  return returnVal;
}

export function SetSearchBody(serviceInfo, searchInputs, pageNumber, userContext) {
  var serviceFormat = serviceInfo.body;
  if (!serviceFormat && Object.keys(serviceFormat).length === 0) return;
  var requestBody = {};
  
  var page = serviceFormat.page;
  page.rowsPerPage = userContext.preference.tableRowsCount;
  page.pageNumber = pageNumber;

  requestBody["page"] = page;

  for (let key of Object.keys(serviceFormat)) {
    if (key === "locale") {
      requestBody[key] = userContext.preference.language;
      continue;
    }
    Object.keys(searchInputs).forEach(inputKey => {
      if (key.toLowerCase() === inputKey.toLowerCase()) {
        if (searchInputs[inputKey] !== '') {
          requestBody[key] = searchInputs[inputKey];
        }
      }
    });   
  }

  return requestBody;
}

export function SetBody(serviceBody, body) {
  let requestBody;

  if (Object.keys(serviceBody).length > 0) {
    requestBody = ValidateBodyForService(serviceBody, body);
  } else {
    requestBody = body;
  }

  return requestBody;
}

export function CheckResultFromService(json) {
  var result = {isSuccess:false, message:"", id:""};

  if (typeof json === 'object') {
    Object.keys(json).forEach(key => {
      if (key.toLowerCase() === dictionary.RESULT) {
        result.isSuccess = json[dictionary.RESULT].isSuccess;
          result.message = json[dictionary.RESULT].message;
      } else if (key.toLowerCase() === dictionary.ID) {
        result.id = json[dictionary.ID];
      }
    });
  }

  return result;
}

//rowId can be string or array. If it is array, the value will be "arr1_arr2_...".
export function ResetForResponseData(requiredResponse, responseData, rowId) {
    let row = {};

    if (!requiredResponse) return responseData;

    let requestBody;
    
    if (Array.isArray(responseData)) {
      requestBody = [];
      responseData.forEach(item=>{
        row = CompareJsonData(requiredResponse, item);
        if (rowId) row["rowId"] = makeRowId(row, rowId);
        requestBody.push(row);
      })
    } else {
      requestBody = {};
      row = CompareJsonData(requiredResponse, responseData);
      if (rowId) row["rowId"] = row[rowId];
      requestBody = row;
    }

    return requestBody;
}

function makeRowId(row, rowId) {
  var rowValue = "";
  if (Array.isArray(rowId)) {
    rowId.forEach(arr => {
      rowValue += row[arr] + "_";
    })
    rowValue = rowValue !=="" ? rowValue.substring(0, rowValue.lastIndexOf('_')) : "";
  } else {
    rowValue = row[rowId];
  }

  return rowValue;
}

function CompareJsonData(source, item) {
  let isChecked = false;
  let body = {};
  let value;

  Object.keys(source).forEach(key => {
    if (item) {
      Object.keys(item).forEach(dataKey => {
        if (key.toLowerCase() === dataKey.toLowerCase()) {
          body[key] = ConvertValueForType(source[key], item[dataKey]);
          isChecked = true;
        }
      });     
    }
    
    if (!isChecked) {
      body[key] = "";
      isChecked = false;
    } 
  });    

  return body;
}

export function CheckMenuAuth(menuIDs) {
    let hasAuth = false;
    const userContext = useUserContext();
    
    if (userContext.auth.length > 0) {
      for (let auth of userContext.auth) {
        if (typeof menuIDs === 'object') {
          for (let id of menuIDs) {
            if (id.toLowerCase() === auth.menuId.toLowerCase()) {
              hasAuth = true;
              return hasAuth;
            }
          }
        } else {
          if (menuIDs.toLowerCase() === auth.menuId.toLowerCase()) {
            hasAuth = true;
            return hasAuth;
          }
        }
      }
    }
    return hasAuth;
}

function ValidateBodyForService(serviceBody, body) {
    let requestBody = {};

    for (let key in body){
        if (body[key] !== null && typeof body[key] === 'object') {
            requestBody[key] = {};
            CheckEmptyAndBoolean(requestBody[key], key, serviceBody[key], body[key]);
        } else {
            RemoveEmptyAndChangeBoolean(requestBody, key, body, serviceBody[key]);
        }
    }

    return requestBody;
}

function RemoveEmptyAndChangeBoolean(requestBody, element, body, itemType) {
    switch (itemType) {
        case dictionary.BOOLEAN:
            return requestBody[element] = body[element] ? "1" : "0";
        default:
            return body[element] ? (requestBody[element] = body[element]) : "";
    }
}

function CheckEmptyAndBoolean(requestBody, key, serviceBody, body) {
    if (typeof body === 'object') {
        for (let subKey in body) {
            if (typeof body[subKey] === 'object') {
                requestBody[subKey] = {};
                CheckEmptyAndBoolean(requestBody[subKey], subKey, serviceBody[subKey], body[subKey]);
            } else {
                RemoveEmptyAndChangeBoolean(requestBody, subKey, body, serviceBody[subKey]);
            }
        }
    } else {
        RemoveEmptyAndChangeBoolean(requestBody, key, serviceBody, body[key]);
    }
}

export function GetNumbers(inputValue, allowDot, belowDotLength) {
  var numbers="";
    for (var i=0;i<inputValue.length;i++){
      if (!isNaN(inputValue[i])) {
        numbers += inputValue[i] + "";
      } else if (inputValue[i] === ".") {
        if (allowDot) {
          if (numbers.indexOf('.') === -1) {
            numbers += inputValue[i] + "";
          }
        }
      }
    }

    if (allowDot) {
      if (belowDotLength && !isNaN(belowDotLength))  {
        var dotIndex = numbers.indexOf('.');
        if (dotIndex > -1 && (numbers.length - dotIndex - 1 > Number(belowDotLength))) {
          numbers = numbers.substring(0,dotIndex + Number(belowDotLength) + 1);
        }
      }
    }

    return numbers;
}

export function ConvertItemsToComboType(valueName,lableName,items) {
  var arr=[];
  var valueLable={};
  if (!Array.isArray(items)) return;
  items.forEach(item=>{
    valueLable={};
    valueLable["value"] = item[valueName];
    valueLable["label"] = item[lableName];
    arr.push(valueLable);
  })

  return arr;
}

