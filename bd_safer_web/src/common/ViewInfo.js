import Dictionary from '../common/Dictionary';
import { GetText } from '../common/BundleManager';

const dictionary = new Dictionary();
class ViewInfo {
    home = {
        "viewName" : dictionary.View_Home,
        "locations" : [], //This is used for breadcrumbs.
        "viewType" : "",
        "viewTitle" : {"normal":GetText("menu_home"), "new" : "","update":""},
        "subTextOnDialogTitle" : "",
        "dialogWidthType" : {"maxWidth": "md"}, //Pick one in "xs","sm","md","lg" and "xl". Getting larger from right side.
        "table" : {
            "allowPage" : true,
            "allowCheckBox" : false
        }
    };
    wardList = {
        "viewName" : dictionary.View_WardList,
        "locations" : [GetText("menu_fire_service"),GetText("menu_fire_dept_mgr")], //This is used for breadcrumbs.
        "viewType" : dictionary.LIST,
        "viewTitle" : {"normal":GetText("menu_fire_dept_mgr"), "new" : GetText("menu_fire_dept_mgr"),"update":GetText("menu_fire_dept_mgr")},
        "subTextOnDialogTitle" : "",
        "dialogWidthType" : {"maxWidth": "md"}, //Pick one in "xs","sm","md","lg" and "xl". Getting larger from right side.
        "table" : {
            "allowPage" : true,
            "allowCheckBox" : false
        }
    };
    wardDetail = {
        "viewName" : dictionary.View_WardDetail,
        "locations" : [GetText("menu_fire_service"),GetText("menu_fire_dept_mgr")], //This is used for breadcrumbs.
        "viewType" : dictionary.DETAIL,
        "viewTitle" : {"normal":GetText("menu_fire_dept_mgr"), "new" : GetText("menu_fire_dept_mgr"),"update":GetText("menu_fire_dept_mgr")},
        "subTextOnDialogTitle" : "",
        "dialogWidthType" : {"maxWidth": "lg"}, //Pick one in "xs","sm","md","lg" and "xl". Getting larger from right side.
        "table" : {
            "allowPage" : true,
            "allowCheckBox" : false
        }
    };
    fireObjectList = {
        "viewName" : dictionary.View_FireObjectList,
        "locations" : [GetText("menu_information_support"),GetText("menu_search_fire_object")], //This is used for breadcrumbs.
        "viewType" : dictionary.LIST,
        "viewTitle" : {"normal":GetText("menu_search_fire_object"), "new" : GetText("menu_search_fire_object"),"update":GetText("menu_search_fire_object")},
        "subTextOnDialogTitle" : "",
        "dialogWidthType" : {"maxWidth": "lg"}, //Pick one in "xs","sm","md","lg" and "xl". Getting larger from right side.
        "table" : {
            "allowPage" : true,
            "allowCheckBox" : false
        }
    };
    fireObjectInfo = {
        "viewName" : dictionary.View_FireObjectInfo,
        "locations" : [GetText("menu_information_support"),GetText("menu_fire_object_info")], //This is used for breadcrumbs.
        "viewType" : dictionary.DETAIL,
        "viewTitle" : {"normal":GetText("menu_fire_object_info"), "new" : GetText("menu_fire_object_info"),"update":GetText("menu_fire_object_info")},
        "subTextOnDialogTitle" : "",
        "dialogWidthType" : {"maxWidth": "xl"}, //Pick one in "xs","sm","md","lg" and "xl". Getting larger from right side.
        "table" : {}
    };
    fireObjectDetail = {
        "viewName" : dictionary.View_FireObjectDetail,
        "locations" : [GetText("menu_information_support"),GetText("menu_fire_object_info")], //This is used for breadcrumbs.
        "viewType" : dictionary.DETAIL,
        "viewTitle" : {"normal":GetText("menu_fire_object_info"), "new" : GetText("menu_fire_object_info"),"update":GetText("menu_fire_object_info")},
        "subTextOnDialogTitle" : "",
        "dialogWidthType" : {"maxWidth": "xl"}, //Pick one in "xs","sm","md","lg" and "xl". Getting larger from right side.
        "table" : {}
    };
    buildingList = {
        "viewName" : dictionary.View_BuildingInfoList,
        "locations" : [GetText("menu_information_support"),GetText("menu_fire_object_info"),GetText("menu_building_info")], //This is used for breadcrumbs.
        "viewType" : dictionary.LIST,
        "viewTitle" : {"normal":GetText("menu_building_info"), "new" : GetText("menu_building_info"),"update":GetText("menu_building_info")},
        "subTextOnDialogTitle" : "",
        "dialogWidthType" : {"maxWidth": "xl"}, //Pick one in "xs","sm","md","lg" and "xl". Getting larger from right side.
        "table" : {
            "allowPage" : false,
            "allowCheckBox" : false
        }
    };
    buildingDetail = {
        "viewName" : dictionary.View_BuildingInfoDetail,
        "locations" : [GetText("menu_information_support"),GetText("menu_fire_object_info"),GetText("menu_building_info")], //This is used for breadcrumbs.
        "viewType" : dictionary.DETAIL,
        "viewTitle" : {"normal":GetText("menu_building_info"), "new" : GetText("menu_building_info"),"update":GetText("menu_building_info")},
        "subTextOnDialogTitle" : "",
        "dialogWidthType" : {"maxWidth": "lg"}, //Pick one in "xs","sm","md","lg" and "xl". Getting larger from right side.
        "table" : {}
    };
    storyList = {
        "viewName" : dictionary.View_BuildingStoryInfoList,
        "locations" : [GetText("menu_information_support"),GetText("menu_fire_object_info"),GetText("menu_story_info")], //This is used for breadcrumbs.
        "viewType" : dictionary.LIST,
        "viewTitle" : {"normal":GetText("menu_story_info"), "new" : GetText("menu_story_info"),"update":GetText("menu_story_info")},
        "subTextOnDialogTitle" : "",
        "dialogWidthType" : {"maxWidth": "xl"}, //Pick one in "xs","sm","md","lg" and "xl". Getting larger from right side.
        "table" : {
            "allowPage" : false,
            "allowCheckBox" : false
        }
    };
    storyDetail = {
        "viewName" : dictionary.View_BuildingStoryInfoDetail,
        "locations" : [GetText("menu_information_support"),GetText("menu_fire_object_info"),GetText("menu_story_info")], //This is used for breadcrumbs.
        "viewType" : dictionary.DETAIL,
        "viewTitle" : {"normal":GetText("menu_story_info"), "new" : GetText("menu_story_info"),"update":GetText("menu_story_info")},
        "subTextOnDialogTitle" : "",
        "dialogWidthType" : {"maxWidth": "md"}, //Pick one in "xs","sm","md","lg" and "xl". Getting larger from right side.
        "table" : {}
    };
    
    storySpecUsedDetail = {
        "viewName" : dictionary.View_BuildingStorySpecUsedInfoDetail,
        "locations" : [GetText("menu_information_support"),GetText("menu_fire_object_info"),GetText("menu_story_special_use_info")], //This is used for breadcrumbs.
        "viewType" : dictionary.DETAIL,
        "viewTitle" : {"normal":GetText("menu_story_special_use_info"), "new" : GetText("menu_story_special_use_info"),"update":GetText("menu_story_special_use_info")},
        "subTextOnDialogTitle" : "",
        "dialogWidthType" : {"maxWidth": "sm"}, //Pick one in "xs","sm","md","lg" and "xl". Getting larger from right side.
        "table" : {}
    };

    fireEquipmentList = {
        "viewName" : dictionary.View_FireEquipmentStateList,
        "locations" : [GetText("menu_information_support"),GetText("menu_fire_object_info"),GetText("menu_fire_equipment_info")], //This is used for breadcrumbs.
        "viewType" : dictionary.LIST,
        "viewTitle" : {"normal":GetText("menu_fire_equipment_info"), "new" : GetText("menu_fire_equipment_info"),"update":GetText("menu_fire_equipment_info")},
        "subTextOnDialogTitle" : "",
        "dialogWidthType" : {"maxWidth": "xl"}, //Pick one in "xs","sm","md","lg" and "xl". Getting larger from right side.
        "table" : {
            "allowPage" : false,
            "allowCheckBox" : false
        }
    };
    fireEquipmentDetail = {
        "viewName" : dictionary.View_FireEquipmentStateDetail,
        "locations" : [GetText("menu_information_support"),GetText("menu_fire_object_info"),GetText("menu_fire_equipment_info")], //This is used for breadcrumbs.
        "viewType" : dictionary.DETAIL,
        "viewTitle" : {"normal":GetText("menu_fire_equipment_info"), "new" : GetText("menu_fire_equipment_info"),"update":GetText("menu_fire_equipment_info")},
        "subTextOnDialogTitle" : "",
        "dialogWidthType" : {"maxWidth": "xl"}, //Pick one in "xs","sm","md","lg" and "xl". Getting larger from right side.
        "table" : {}
    };
    emergencyRescuePlan = {
        "viewName" : dictionary.View_EmergencyRescuePlan,
        "locations" : [GetText("menu_information_support"),GetText("menu_fire_object_info"),GetText("menu_emergency_plan_info")], //This is used for breadcrumbs.
        "viewType" : dictionary.DETAIL,
        "viewTitle" : {"normal":GetText("menu_emergency_plan_info"), "new" : GetText("menu_emergency_plan_info"),"update":GetText("menu_emergency_plan_info")},
        "subTextOnDialogTitle" : "",
        "dialogWidthType" : {"maxWidth": "xl"}, //Pick one in "xs","sm","md","lg" and "xl". Getting larger from right side.
        "table" : {}
    };
    emegencyRescuePictureList = {
        "viewName" : dictionary.View_EmergencyRescuePictureList,
        "locations" : [GetText("menu_information_support"),GetText("menu_fire_object_info"),GetText("menu_emergency_picture")], //This is used for breadcrumbs.
        "viewType" : dictionary.LIST,
        "viewTitle" : {"normal":GetText("menu_emergency_picture"), "new" : GetText("menu_emergency_picture"),"update":GetText("menu_emergency_picture")},
        "subTextOnDialogTitle" : "",
        "dialogWidthType" : {"maxWidth": "xl"}, //Pick one in "xs","sm","md","lg" and "xl". Getting larger from right side.
        "table" : {
            "allowPage" : false,
            "allowCheckBox" : false
        }
    };
    emegencyRescuePictureDetail = {
        "viewName" : dictionary.View_EmergencyRescuePictureDetail,
        "locations" : [GetText("menu_information_support"),GetText("menu_fire_object_info"),GetText("menu_emergency_picture")], //This is used for breadcrumbs.
        "viewType" : dictionary.DETAIL,
        "viewTitle" : {"normal":GetText("menu_emergency_picture"), "new" : GetText("menu_emergency_picture"),"update":GetText("menu_emergency_picture")},
        "subTextOnDialogTitle" : "",
        "dialogWidthType" : {"maxWidth": "xl"}, //Pick one in "xs","sm","md","lg" and "xl". Getting larger from right side.
        "table" : {}
    };
    buildingHistoryList = {
        "viewName" : dictionary.View_BuildingHistoryList,
        "locations" : [GetText("menu_information_support"),GetText("menu_fire_object_info"),GetText("menu_building_history")], //This is used for breadcrumbs.
        "viewType" : dictionary.LIST,
        "viewTitle" : {"normal":GetText("menu_building_history"), "new" : GetText("menu_building_history"),"update":GetText("menu_building_history")},
        "subTextOnDialogTitle" : "",
        "dialogWidthType" : {"maxWidth": "xl"}, //Pick one in "xs","sm","md","lg" and "xl". Getting larger from right side.
        "table" : {
            "allowPage" : false,
            "allowCheckBox" : false
        }
    };
    buildingHistoryDetail = {
        "viewName" : dictionary.View_BuildingHistoryDetail,
        "locations" : [GetText("menu_information_support"),GetText("menu_fire_object_info"),GetText("menu_building_history")], //This is used for breadcrumbs.
        "viewType" : dictionary.DETAIL,
        "viewTitle" : {"normal":GetText("menu_building_history"), "new" : GetText("menu_building_history"),"update":GetText("menu_building_history")},
        "subTextOnDialogTitle" : "",
        "dialogWidthType" : {"maxWidth": "lg"}, //Pick one in "xs","sm","md","lg" and "xl". Getting larger from right side.
        "table" : {}
    };
    arsonManagerList = {
        "viewName" : dictionary.View_ArsonManagerList,
        "locations" : [GetText("menu_information_support"),GetText("menu_fire_object_info"),GetText("menu_arson_manager")], //This is used for breadcrumbs.
        "viewType" : dictionary.LIST,
        "viewTitle" : {"normal":GetText("menu_arson_manager"), "new" : GetText("menu_arson_manager"),"update":GetText("menu_arson_manager")},
        "subTextOnDialogTitle" : "",
        "dialogWidthType" : {"maxWidth": "xl"}, //Pick one in "xs","sm","md","lg" and "xl". Getting larger from right side.
        "table" : {
            "allowPage" : false,
            "allowCheckBox" : false
        }
    };
    arsonManagerDetail = {
        "viewName" : dictionary.View_ArsonManagerDetail,
        "locations" : [GetText("menu_information_support"),GetText("menu_fire_object_info"),GetText("menu_arson_manager")], //This is used for breadcrumbs.
        "viewType" : dictionary.DETAIL,
        "viewTitle" : {"normal":GetText("menu_arson_manager"), "new" : GetText("menu_arson_manager"),"update":GetText("menu_arson_manager")},
        "subTextOnDialogTitle" : "",
        "dialogWidthType" : {"maxWidth": "md"}, //Pick one in "xs","sm","md","lg" and "xl". Getting larger from right side.
        "table" : {}
    };
    safetyManagerList = {
        "viewName" : dictionary.View_SafetyManagerList,
        "locations" : [GetText("menu_information_support"),GetText("menu_fire_object_info"),GetText("menu_safety_manager")], //This is used for breadcrumbs.
        "viewType" : dictionary.LIST,
        "viewTitle" : {"normal":GetText("menu_safety_manager"), "new" : GetText("menu_safety_manager"),"update":GetText("menu_safety_manager")},
        "subTextOnDialogTitle" : "",
        "dialogWidthType" : {"maxWidth": "xl"}, //Pick one in "xs","sm","md","lg" and "xl". Getting larger from right side.
        "table" : {
            "allowPage" : false,
            "allowCheckBox" : false
        }
    };
    safetyManagerDetail = {
        "viewName" : dictionary.View_SafetyManagerDetail,
        "locations" : [GetText("menu_information_support"),GetText("menu_fire_object_info"),GetText("menu_safety_manager")], //This is used for breadcrumbs.
        "viewType" : dictionary.DETAIL,
        "viewTitle" : {"normal":GetText("menu_safety_manager"), "new" : GetText("menu_safety_manager"),"update":GetText("menu_safety_manager")},
        "subTextOnDialogTitle" : "",
        "dialogWidthType" : {"maxWidth": "md"}, //Pick one in "xs","sm","md","lg" and "xl". Getting larger from right side.
        "table" : {}
    };
    buildingManagerList = {
        "viewName" : dictionary.View_BuildingManagerList,
        "locations" : [GetText("menu_information_support"),GetText("menu_fire_object_info"),GetText("menu_building_manager")], //This is used for breadcrumbs.
        "viewType" : dictionary.LIST,
        "viewTitle" : {"normal":GetText("menu_building_manager"), "new" : GetText("menu_building_manager"),"update":GetText("menu_building_manager")},
        "subTextOnDialogTitle" : "",
        "dialogWidthType" : {"maxWidth": "xl"}, //Pick one in "xs","sm","md","lg" and "xl". Getting larger from right side.
        "table" : {
            "allowPage" : false,
            "allowCheckBox" : false
        }
    };
    buildingManagerDetail = {
        "viewName" : dictionary.View_BuildingManagerDetail,
        "locations" : [GetText("menu_information_support"),GetText("menu_fire_object_info"),GetText("menu_building_manager")], //This is used for breadcrumbs.
        "viewType" : dictionary.DETAIL,
        "viewTitle" : {"normal":GetText("menu_building_manager"), "new" : GetText("menu_building_manager"),"update":GetText("menu_building_manager")},
        "subTextOnDialogTitle" : "",
        "dialogWidthType" : {"maxWidth": "md"}, //Pick one in "xs","sm","md","lg" and "xl". Getting larger from right side.
        "table" : {}
    };
    userPreference = {
        "viewName" : dictionary.View_UserPreference,
        "locations" : "", //This is used for breadcrumbs.
        "viewType" : dictionary.DETAIL,
        "viewTitle" : {"normal":GetText("user_preference"), "new" : GetText("user_preference"),"update":GetText("user_preference")},
        "subTextOnDialogTitle" : "",
        "dialogWidthType" : {"maxWidth": "sm"}, //Pick one in "xs","sm","md","lg" and "xl". Getting larger from right side.
        "table" : {}
    };
    userList = {
        "viewName" : dictionary.View_UserList,
        "locations" : [GetText("menu_fire_service"),GetText("menu_hr_mgr")], //This is used for breadcrumbs.
        "viewType" : dictionary.LIST,
        "viewTitle" : {"normal":GetText("menu_hr_mgr"), "new" : GetText("menu_hr_mgr"),"update":GetText("menu_hr_mgr")},
        "subTextOnDialogTitle" : "",
        "dialogWidthType" : {"maxWidth": "md"}, //Pick one in "xs","sm","md","lg" and "xl". Getting larger from right side.
        "table" : {
            "allowPage" : true,
            "allowCheckBox" : false
        }
    };
    userInfo = {
        "viewName" : dictionary.View_UserInfo,
        "locations" : [GetText("menu_fire_service"),GetText("menu_hr_mgr")], //This is used for breadcrumbs.
        "viewType" : dictionary.DETAIL,
        "viewTitle" : {"normal":GetText("menu_hr_mgr"), "new" : GetText("menu_hr_mgr"),"update":GetText("menu_hr_mgr")},
        "subTextOnDialogTitle" : "",
        "dialogWidthType" : {"maxWidth": "lg"}, //Pick one in "xs","sm","md","lg" and "xl". Getting larger from right side.
        "table" : {}
    };
    userBasicInfo = {
        "viewName" : dictionary.View_UserBasicInfo,
        "locations" : [GetText("menu_fire_service"),GetText("menu_hr_mgr")], //This is used for breadcrumbs.
        "viewType" : dictionary.DETAIL,
        "viewTitle" : {"normal":GetText("fire_worker_basic_info"), "new" : GetText("fire_worker_basic_info"),"update":GetText("fire_worker_basic_info")},
        "subTextOnDialogTitle" : "",
        "dialogWidthType" : {"maxWidth": "lg"}, //Pick one in "xs","sm","md","lg" and "xl". Getting larger from right side.
        "table" : {}
    };
    createAccount = {
        "viewName" : dictionary.View_CreateAccount,
        "locations" : [], //This is used for breadcrumbs.
        "viewType" : dictionary.DETAIL,
        "viewTitle" : {"normal":GetText("create_account"), "new" : GetText("create_account"),"update":GetText("create_account")},
        "subTextOnDialogTitle" : "",
        "dialogWidthType" : {"maxWidth": "lg"}, //Pick one in "xs","sm","md","lg" and "xl". Getting larger from right side.
        "table" : {}
    };
    notificationList = {
        "viewName" : dictionary.View_Notification,
        "locations" : [], //This is used for breadcrumbs.
        "viewType" : dictionary.LIST,
        "viewTitle" : {"normal":GetText("notification_list"), "new" : GetText("notification_list"),"update":GetText("notification_list")},
        "subTextOnDialogTitle" : "",
        "dialogWidthType" : {"maxWidth": "lg"}, //Pick one in "xs","sm","md","lg" and "xl". Getting larger from right side.
        "table" : {
            "allowPage" : false,
            "allowCheckBox" : true
        }
    };
    userEducationHistory = {
        "viewName" : dictionary.View_UserEducationHistory,
        "locations" : [GetText("menu_fire_service"),GetText("menu_hr_mgr")], //This is used for breadcrumbs.
        "viewType" : dictionary.LIST,
        "viewTitle" : {"normal":GetText("education_history"), "new" : GetText("education_history"),"update":GetText("education_history")},
        "subTextOnDialogTitle" : "",
        "dialogWidthType" : {"maxWidth": "md"}, //Pick one in "xs","sm","md","lg" and "xl". Getting larger from right side.
        "table" : {
            "allowPage" : false,
            "allowCheckBox" : false
        }
    };
    userEducationDetail = {
        "viewName" : dictionary.View_UserEducationDetail,
        "locations" : [GetText("menu_fire_service"),GetText("menu_hr_mgr")], //This is used for breadcrumbs.
        "viewType" : dictionary.DETAIL,
        "viewTitle" : {"normal":GetText("education_history"), "new" : GetText("education_history"),"update":GetText("education_history")},
        "subTextOnDialogTitle" : "",
        "dialogWidthType" : {"maxWidth": "sm"}, //Pick one in "xs","sm","md","lg" and "xl". Getting larger from right side.
        "table" : {}
    };
    userLicenseList = {
        "viewName" : dictionary.View_UserLicenseList,
        "locations" : [GetText("menu_fire_service"),GetText("menu_hr_mgr")], //This is used for breadcrumbs.
        "viewType" : dictionary.LIST,
        "viewTitle" : {"normal":GetText("possessed_licenses"), "new" : GetText("possessed_licenses"),"update":GetText("possessed_licenses")},
        "subTextOnDialogTitle" : "",
        "dialogWidthType" : {"maxWidth": "md"}, //Pick one in "xs","sm","md","lg" and "xl". Getting larger from right side.
        "table" : {
            "allowPage" : false,
            "allowCheckBox" : false
        }
    };
    userLicenseDetail = {
        "viewName" : dictionary.View_UserLicenseDetail,
        "locations" : [GetText("menu_fire_service"),GetText("menu_hr_mgr")], //This is used for breadcrumbs.
        "viewType" : dictionary.DETAIL,
        "viewTitle" : {"normal":GetText("possessed_licenses"), "new" : GetText("possessed_licenses"),"update":GetText("possessed_licenses")},
        "subTextOnDialogTitle" : "",
        "dialogWidthType" : {"maxWidth": "md"}, //Pick one in "xs","sm","md","lg" and "xl". Getting larger from right side.
        "table" : {}
    };
    userLearningList = {
        "viewName" : dictionary.View_UserLearningList,
        "locations" : [GetText("menu_fire_service"),GetText("menu_hr_mgr")], //This is used for breadcrumbs.
        "viewType" : dictionary.LIST,
        "viewTitle" : {"normal":GetText("learning_achievement"), "new" : GetText("learning_achievement"),"update":GetText("learning_achievement")},
        "subTextOnDialogTitle" : "",
        "dialogWidthType" : {"maxWidth": "md"}, //Pick one in "xs","sm","md","lg" and "xl". Getting larger from right side.
        "table" : {
            "allowPage" : false,
            "allowCheckBox" : false
        }
    };
    userLearningDetail = {
        "viewName" : dictionary.View_UserLearningDetail,
        "locations" : [GetText("menu_fire_service"),GetText("menu_hr_mgr")], //This is used for breadcrumbs.
        "viewType" : dictionary.DETAIL,
        "viewTitle" : {"normal":GetText("learning_achievement"), "new" : GetText("learning_achievement"),"update":GetText("learning_achievement")},
        "subTextOnDialogTitle" : "",
        "dialogWidthType" : {"maxWidth": "sm"}, //Pick one in "xs","sm","md","lg" and "xl". Getting larger from right side.
        "table" : {}
    };
    userFireTrainList = {
        "viewName" : dictionary.View_UserFireTrainList,
        "locations" : [GetText("menu_fire_service"),GetText("menu_hr_mgr")], //This is used for breadcrumbs.
        "viewType" : dictionary.LIST,
        "viewTitle" : {"normal":GetText("fire_train_achievement"), "new" : GetText("fire_train_achievement"),"update":GetText("fire_train_achievement")},
        "subTextOnDialogTitle" : "",
        "dialogWidthType" : {"maxWidth": "md"}, //Pick one in "xs","sm","md","lg" and "xl". Getting larger from right side.
        "table" : {
            "allowPage" : false,
            "allowCheckBox" : false
        }
    };
    userFireTrainDetail = {
        "viewName" : dictionary.View_UserFireTrainDetail,
        "locations" : [GetText("menu_fire_service"),GetText("menu_hr_mgr")], //This is used for breadcrumbs.
        "viewType" : dictionary.DETAIL,
        "viewTitle" : {"normal":GetText("fire_train_achievement"), "new" : GetText("fire_train_achievement"),"update":GetText("fire_train_achievement")},
        "subTextOnDialogTitle" : "",
        "dialogWidthType" : {"maxWidth": "md"}, //Pick one in "xs","sm","md","lg" and "xl". Getting larger from right side.
        "table" : {}
    };
    groupCodeTreeView = {
        "viewName" : dictionary.View_GroupCodeTreeView,
        "locations" : [GetText("menu_system_manager"),GetText("group_code_tree")], //This is used for breadcrumbs.
        "viewType" : dictionary.DETAIL,
        "viewTitle" : {"normal":GetText("select_code"), "new" : GetText("select_code"),"update":GetText("select_code")},
        "subTextOnDialogTitle" : GetText("subtext_groupcodetree"),
        "dialogWidthType" : {"maxWidth": "sl"}, //Pick one in "xs","sm","md","lg" and "xl". Getting larger from right side.
        "table" : {}
    };
}

export default ViewInfo;