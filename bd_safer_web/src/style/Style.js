import { makeStyles } from '@material-ui/core/styles';

export const FormStyle = makeStyles((theme) => ({
    root: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    textField10: {
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(0),
      width: '10ch',
      disabled: {
        color:'black'
      }
    },
    textField12: {
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(0),
      width: '12ch',
      disabled: {
        color:'black'
      }
    },
    textFieldS: {
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(1),
      width: '18ch',
      disabled: {
        color:'black'
      }
    },
    textField22: {
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(1),
      width: '22ch',
      disabled: {
        color:'black'
      }
    },
    textField: {
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(1),
      width: '25ch',
      disabled: {
        color:'black'
      }
    },
    textField2: {
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(1),
      width: '50ch',
    },
    textField3: {
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(1),
      width: '75ch',
    },
    textField4: {
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(1),
      width: '100ch',
    },
    textFieldFullLength: {
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(1),
      width: `calc(100% - ${theme.spacing(2)}px)`,
    },
    textFieldAddress: {
      marginLeft: theme.spacing(1),
      marginRight: theme.spacing(0),
      width: '50ch',
    },
    textFieldAddressEtc: {
      marginLeft: theme.spacing(0),
      marginRight: theme.spacing(1),
      width: '30ch',
    },
    paper: {
      display: 'flex',
      flexWrap: 'wrap',
      '& > *': {
        margin: theme.spacing(1)
      },
    },
    control: {
      padding: theme.spacing(1)
    },
    grow: {
      flexGrow: 1,
    },
    sectionSearch: {
      [theme.breakpoints.up('md')]: {
        display: 'flex',
      },
    },
    margin: {
      margin: theme.spacing(1),
    },
    gridPadding: {
      paddingBottom: '8px',
      paddingLeft: '8px',
      paddingRight: '8px',
    },
    centerLocate: {
      textAlign: 'center'
    },
    disabledTextField :{
      color:'black'
    },    
    select10 : {
      margin: theme.spacing(1),
      width: '10ch',
      disabled: {
        color:'black'
      }
    },
    select20 : {
      margin: theme.spacing(1),
      width: '12ch',
      disabled: {
        color:'black'
      }
    },
    selectS : {
      margin: theme.spacing(1),
      width: '18ch',
      disabled: {
        color:'black'
      }
    },
    select : {
      margin: theme.spacing(1),
      width: '25ch',
      disabled: {
        color:'black'
      }
    },
    select2 : {
      margin: theme.spacing(1),
      width: '50ch',
      disabled: {
        color:'black'
      }
    },
    media: {
      height: 140,
    },
  }));