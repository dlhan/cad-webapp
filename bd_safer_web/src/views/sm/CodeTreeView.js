import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TreeView from '@material-ui/lab/TreeView';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import TreeItem from '@material-ui/lab/TreeItem';
import { useUserContext } from '../../common/UserContext';
import serviceList from '../../service/ServiceList';
import { RunService } from '../../service/RestApi';
import { ResetForResponseData } from '../../common/Utility';
import Grid from '@material-ui/core/Grid';
import StickyHeadTable from '../../components/Table';
import Paper from '@material-ui/core/Paper';
import FormLabel from '@material-ui/core/FormLabel';
import FormControl from '@material-ui/core/FormControl';
import ActionPanel from '../../components/ActionPanel';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import Divider from '@material-ui/core/Divider';
import { GetText } from '../../common/BundleManager';

const columns = [
  { id: 'cd', numeric: false, disablePadding: false, label: '', align: 'left', minWidth: 0 },
  { id: 'cdName', numeric: false, disablePadding: false, label: 'code_name', align: 'left', minWidth: 150 },
];

const useStyles = makeStyles({
  treeRoot: {
    height: 400,
    flexGrow: 1,
    width: 300,
    margin: 10
  },
  formLabelmargin: {
    margin: 10
  },
  treePaper: {
    display: 'flex',
    overflow: 'auto'
  },
  tablePaper: {
    height: 400,
  },
  pageWidth:{
    width: '340px'
  }
});

let dataSelected = [];
export default function CodeTreeView(props) {
  const classes = useStyles();
  const [data, setData] = useState({});
  const [codeRows, setCodeRows] = useState([]);
  const userContext = useUserContext();
  const s = new serviceList();

  const onIconClick =(id)=> {
    if (id !== props.sendInfo.groupCode) getGroupCodes(id);
  }

  const getCodesForGroupCode =(id)=> {
    RunService(s.getCodesForGroupCode, {groupCode:id, locale:userContext.preference.language})
          .then(response => {
            setCodeRows(ResetForResponseData(s.getCodesForGroupCode.response, response.data,"cd"));
          })
          .catch(error => {
            console.log(error);
          })
  }

  const setDataStructure = (parentCode, title, rows)=> {
    let dataForm = {id:"",name:"", leaf:false};
    
    if (Object.keys(data).length !== 0) {
      appendChilden(parentCode, setChildren(rows));
    } else {
      dataForm.id = parentCode;
      dataForm.name = title;
      dataForm.children = setChildren(rows);
      setData(dataForm);
    }
  }

  const appendChilden = (code, children) => {
    var copyData = JSON.parse(JSON.stringify(data));

    Object.keys(copyData).forEach(d => {
      if (d === 'children' && Array.isArray(copyData[d])) {
        deepFind(code, children, copyData[d]);
      }
    });    

    setData(copyData);
  }

  const deepFind = (code, children, copyData) => {
    if (Array.isArray(copyData)) {
      copyData.map(function(c){
        if (c.id === code) {
          c.children = children;
        } else if (typeof c.children === 'object') {
          deepFind(code, children,c.children);
        }
      })
    } else {
      if (typeof copyData.id === 'string' && copyData.id === code) {
        copyData.children = children;
      }
    }
  }

  const setChildren =(rows)=> {
    let children = [];
    children = rows.map(function(row){
      if (row.leafYn) {
        return {id:row.cdGrp, name:row.cdGrpName, leaf:row.leafYn};
      } else {
        return {id:row.cdGrp, name:row.cdGrpName, leaf:row.leafYn, children:[{}]};
      }
    });
    return children;
  }

  const getGroupCodes = (code) => {
    RunService(s.getNextGroupCodes, {groupCode:code, locale:userContext.preference.language})
          .then(response => {
            setDataStructure(code, response.data.groupCodeTitle, ResetForResponseData(s.getNextGroupCodes.response.rows, response.data.rows));
          })
          .catch(error => {
            console.log(error);
          })
  }

  useEffect(() => {
    getGroupCodes(props.sendInfo.groupCode)
  }, []);
  
  const renderTree = (nodes) => (
    <TreeItem key={nodes.id} id={nodes.id} nodeId={nodes.id} label={nodes.name} onIconClick={()=>onIconClick(nodes.id)} onLabelClick={()=>getCodesForGroupCode(nodes.id)}>
      {Array.isArray(nodes.children) ? nodes.children.map((node) => renderTree(node)) : null}
    </TreeItem>
  );

  const checkBoxClickEvent = (data) => {
    dataSelected = data;
  }

  const onSelect =() => {
    let cdName="";
    for (let r in codeRows) {
      if (codeRows[r].cd === dataSelected[0]) {
        cdName = codeRows[r].cdName;    
        break;
      } 
    }
    props.callFromDialog({cd:dataSelected[0],cdName:cdName});
  }

  return (
    <React.Fragment>
      <Grid container className={classes.gridRoot} spacing={1}>
          <Grid item xs={12}>
            <Grid container justify="center" spacing={5}>
              <Grid key={1} item className={classes.pageWidth}>
                <FormControl >
                  <FormLabel className={classes.formLabelmargin}>{GetText("group_code_tree")}</FormLabel>
                  <Paper className={classes.treePaper}>
                    <TreeView
                      className={classes.treeRoot}
                      defaultCollapseIcon={<ExpandMoreIcon />}
                      defaultExpanded={['root']}
                      defaultExpandIcon={<ChevronRightIcon />}
                    >
                      {renderTree(data)}
                    </TreeView>  
                  </Paper>
                </FormControl>
              </Grid>
              <Grid key={2} item>
                <FormControl className={classes.pageWidth}>
                  <FormLabel className={classes.formLabelmargin}>{GetText("code_list")}</FormLabel>
                  <Paper className={classes.tablePaper}>
                    <StickyHeadTable 
                    tableId={"codeTable"} 
                    columns={columns} 
                    rows={codeRows}
                    SupportCheckBox={true} 
                    SupportPaging={false}
                    checkBoxClickEvent={checkBoxClickEvent}
                    heightGap={456}
                    allowOnlyOneCheck={true}
                  />
                </Paper>
                  </FormControl>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      <Box p={1}></Box>
      <Divider style={{'width':'100%','position': 'absolute', 'left': 0}} />
      <ActionPanel>
        <Button 
          color={userContext.preference.color}
          onClick={onSelect}
        >
          { GetText("select") }
        </Button>
      </ActionPanel>
    </React.Fragment>
  );
}
