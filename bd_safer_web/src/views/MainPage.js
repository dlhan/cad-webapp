import React, { useState, useEffect } from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import Badge from '@material-ui/core/Badge';
import Container from '@material-ui/core/Container';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import NotificationsIcon from '@material-ui/icons/Notifications';
import LeftMenu from '../components/LeftMenu';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MoreIcon from '@material-ui/icons/MoreVert';
import CustomizedDialog from '../components/Dialog';
import SelectView from './SelectView';
import ViewInfo from '../common/ViewInfo';
import Dictionary from '../common/Dictionary';
import Confirm from '../components/Confirm';
import { useUserContext } from '../common/UserContext';
import { CheckMenuAuth, ResetForResponseData } from '../common/Utility';
import serviceList from '../service/ServiceList';
import { RunService } from '../service/RestApi';
import { GetText } from '../common/BundleManager';

const drawerWidth = 300;
  
const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  toolbar: {
    paddingRight: 24, // keep right padding when drawer closed
  },
  toolbarIcon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
   }),
    },
    appBarShift: {
      marginLeft: drawerWidth,
      width: `calc(100% - ${drawerWidth}px)`,
      transition: theme.transitions.create(['width', 'margin'], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    menuButton: {
      marginRight: 36,
    },
    menuButtonHidden: {
      display: 'none',
    },
    title: {
      flexGrow: 1,
    },
    drawerPaper: {
      position: 'relative',
      whiteSpace: 'nowrap',
      width: drawerWidth,
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.enteringScreen,
      }),
    },
    drawerPaperClose: {
      overflowX: 'hidden',
      transition: theme.transitions.create('width', {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen,
      }),
      width: theme.spacing(5),
      [theme.breakpoints.up('sm')]: {
        width: theme.spacing(6),
      },
    },
    appBarSpacer: theme.mixins.toolbar,
    content: {
      flexGrow: 1,
      height: '100vh',
      overflow: 'hidden',
    },
    container: {
      paddingTop: theme.spacing(2),
      paddingBottom: theme.spacing(1),
      maxWidth: '100%',
      paddingLeft: theme.spacing(1),
      paddingRight: theme.spacing(1),
    },
    paper: {
      padding: theme.spacing(3),
      display: 'flex',
      overflow: 'auto',
      flexDirection: 'column',
    },
    grow: {
      flexGrow: 1,
    },
    sectionDesktop: {
      display: 'none',
      [theme.breakpoints.up('md')]: {
        display: 'flex',
      },
    },
    sectionMobile: {
      display: 'flex',
      [theme.breakpoints.up('md')]: {
        display: 'none',
      },
    },
}));
  
export default function MainPage(props) {
  var viewInfo = new ViewInfo();
  var dictionary = new Dictionary();
  const userContext = useUserContext();
  const classes = useStyles();
  const [open, setOpen] = useState(true);
  const [title, SetTitle] = useState("");
  const [notificationCount, setNotificationCount] = useState(0);
  const s = new serviceList();

  let notificationAuth = CheckMenuAuth("SM002") || userContext.userInfo.isAdmin;
    
  const [view, setView] = useState({viewInfo:viewInfo.home});
  const handleDrawerOpen = () => {
    setOpen(true);
  };
  const handleDrawerClose = () => {
    setOpen(false);
  };
    
  const [anchorEl, setAnchorEl] = useState(null);
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = useState(null);

  const isMenuOpen = Boolean(anchorEl);
  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);

  const handleProfileMenuOpen = (event) => {
    setIsUserPreferDialogOpen(true);
  };

  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null);
  };

  const handleMenuClose = () => {
    setAnchorEl(null);
    handleMobileMenuClose();
  };

  const handleMobileMenuOpen = (event) => {
    setMobileMoreAnchorEl(event.currentTarget);
  };

  //////Confrim event in closing the dialog///////////////
  const [confirmMessage, setConfirmMessage] = useState({
    isConfirmOpen : false,
    messageType : "",
    messageId : ""
  });
  let { isConfirmOpen,messageType,messageId } = confirmMessage;

  const handleConfirmDialog = (answer) => {
    setConfirmMessage({
      ...confirmMessage,
      isConfirmOpen:false
    });

    if (answer){
      setIsUserPreferDialogOpen(false);
    }
  }
////////////////////////////////////////////////////////

  //////Dialog event
  const [isUserPreferDialogOpen, setIsUserPreferDialogOpen] = useState(false);
  const [isNotificationDialogOpen, setIsNotificationDialogOpen] = useState(false);
  const [isModified, setIsModified] = useState(false);
  
  
  const callFromDialog = (p) => {
    setIsModified(p.modified);
  }

  const callFromNotification = (p) => {
    if (p.refresh) getNotificationList();
  }

  const handleUserPreferenceDialogClose = (forceNoMessage) => {
    let forceClose = false;

    if (typeof forceNoMessage === 'boolean') forceClose = forceNoMessage;

    if (isModified && !forceClose) {
      setConfirmMessage({
        isConfirmOpen: true,
        messageType: dictionary.WARNING,
        messageId: "msg_confirm_close_dialog" 
      });
    } else {
      setIsUserPreferDialogOpen(false);
    }
  };

  const handleNotificationDialogClose = () => {
    setIsNotificationDialogOpen(false);
  };

////////////////////////////////////////////////////////

  const handleNotificationMenuOpen = () => {
    setIsNotificationDialogOpen(true);
  }


  const menuId = 'primary-search-account-menu';
  const mobileMenuId = 'primary-search-account-menu-mobile';
  const renderMobileMenu = (
    <Menu
      anchorEl={mobileMoreAnchorEl}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      id={mobileMenuId}
      keepMounted
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={isMobileMenuOpen}
      onClose={handleMobileMenuClose}
    >
      { notificationAuth && <MenuItem onClick={handleNotificationMenuOpen}>
        <IconButton color="inherit" >
          <Badge badgeContent={notificationCount} color="secondary">
            <NotificationsIcon />
          </Badge>
        </IconButton>
        <p>{GetText("notification")}</p>
      </MenuItem>}
      <MenuItem onClick={handleProfileMenuOpen}>
        <IconButton
          aria-haspopup="true"
          color="inherit"
        >
          <AccountCircle />
        </IconButton>
        <p>{GetText("user_preference")}</p>
      </MenuItem>
    </Menu>
  );

  const onSelectMenu = (view) => {
    setView(view);
    SetTitle(view.viewTitle.normal);
  }

  const getNotificationList = () => {

    RunService(s.getNotificationList, {locale: userContext.preference.language})
        .then(response => {
          setNotificationCount(response.data.length);
        })
        .catch(error => {
          console.log(error);
        })
  }

  /////////////////////Get Notification list////////////////////////////////////////////
  useEffect(() => {
    getNotificationList();
  }, []);
  /////////////////////////////////////////////////////////////////

  return (
    <div className={classes.root}>
      <CssBaseline />
      <AppBar position="absolute" color={userContext.preference.color} className={clsx(classes.appBar, open && classes.appBarShift)}>
        <Toolbar className={classes.toolbar}>
          <IconButton
            edge="start"
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            className={clsx(classes.menuButton, open && classes.menuButtonHidden)}
          >
            <MenuIcon />
          </IconButton>
          <Typography className={classes.title} variant="h6" noWrap>
            {title}
          </Typography>
          <div className={classes.grow} />
          <div className={classes.sectionDesktop}>
            {notificationAuth && <IconButton color="inherit" onClick={handleNotificationMenuOpen}>
              <Badge badgeContent={notificationCount} color="secondary">
                <NotificationsIcon />
              </Badge>
            </IconButton>}
            <IconButton
              edge="end"
              aria-label="user preference"
              aria-controls={menuId}
              aria-haspopup="true"
              onClick={handleProfileMenuOpen}
              color="inherit"
            >
              <AccountCircle />
            </IconButton>
          </div>
          <div className={classes.sectionMobile}>
            <IconButton
              aria-label="show more"
              aria-controls={mobileMenuId}
              aria-haspopup="true"
              onClick={handleMobileMenuOpen}
              color="inherit"
            >
              <MoreIcon />
            </IconButton>
          </div>
        </Toolbar>
      </AppBar>
      {renderMobileMenu}
      <Drawer
        variant="permanent"
        classes={{
          paper: clsx(classes.drawerPaper, !open && classes.drawerPaperClose),
        }}
        open={open}
      >
        <div className={classes.toolbarIcon}>
          <IconButton onClick={handleDrawerClose}>
            <ChevronLeftIcon />
          </IconButton>
        </div>
        <Divider />
        <LeftMenu onSelectMenu={onSelectMenu}/>
      </Drawer>
      <main className={classes.content}>
        <div className={classes.appBarSpacer} />
        <Container className={classes.container}>
        {
          SelectView(view) 
        }
        </Container>
      </main>
      <Confirm open={isConfirmOpen} MessageType={messageType} MessageId={messageId} handleClose={handleConfirmDialog} />
      <CustomizedDialog isDialogOpen={isUserPreferDialogOpen} viewInfo={viewInfo.userPreference} handleDialogClose={handleUserPreferenceDialogClose} callFromDialog={callFromDialog} />
      <CustomizedDialog isDialogOpen={isNotificationDialogOpen} viewInfo={viewInfo.notificationList} handleDialogClose={handleNotificationDialogClose} callFromDialog={callFromNotification} />
    </div>
  );
}