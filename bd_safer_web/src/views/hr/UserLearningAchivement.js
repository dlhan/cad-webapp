import React, { useState, useEffect } from 'react';
import { GetText } from '../../common/BundleManager'
import serviceList from '../../service/ServiceList';
import CircularProgress from '@material-ui/core/CircularProgress';
import CustomizedSnackbar from '../../components/Snackbar';
import Dictionary from '../../common/Dictionary';
import { RunService } from '../../service/RestApi';
import { ResetForResponseData } from '../../common/Utility';
import StickyHeadTable from '../../components/Table';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import AddIcon from '@material-ui/icons/Add';
import Fab from '@material-ui/core/Fab';
import CustomizedDialog from '../../components/Dialog';
import { FormStyle } from '../../style/Style';
import ViewInfo from '../../common/ViewInfo';
import { useUserContext } from '../../common/UserContext';
import Confirm from '../../components/Confirm';

const useStyles = FormStyle;

const columns = [
  { id: 'educationPromotionSeq', numeric: false, disablePadding: false, label: '', align: 'center', minWidth: 0 },
  { id: 'courseName', numeric: false, disablePadding: false, label: 'course_name', align: 'center', minWidth: 200 },
  { id: 'institutionName', numeric: false, disablePadding: false, label: 'institution_name', align: 'center', minWidth: 150 },
  { id: 'startDate', numeric: false, disablePadding: false, label: 'start_date', align: 'center', minWidth: 100 },
  { id: 'endDate', numeric: false, disablePadding: false, label: 'end_date', align: 'center', minWidth: 100 },
  { id: 'educationClsCdName', numeric: false, disablePadding: false, label: 'education_classification', align: 'center', minWidth: 150 },
  { id: 'completeCdName', numeric: false, disablePadding: false, label: 'education_completed', align: 'center', minWidth: 100 },
  { id: 'score', numeric: false, disablePadding: false, label: 'education_score', align: 'center', minWidth: 100 }
];

export default function UserLearningAchivement(props) {
  const classes = useStyles();
  var s = new serviceList();
  var dictionary = new Dictionary();
  var viewInfo = new ViewInfo();
  var view = viewInfo.userLearningList;
  const userContext = useUserContext();

  let [loading, setLoading] = useState(false);

////////Setting for Table//////////////////////////////////////////
  const onSelectRow = (rowId) => {
    handleDialogOpen(dictionary.READ, rowId)
  }

  const [rows, setRows] = useState([]);
///////////////////////////////////////////////////////////////////

  const getData = () => {
    setLoading(true);
    let body = {userId : props.userId, locale:userContext.preference.language};
    
    RunService(s.getUserEduPromotionList, body, true)
        .then(response => {
          setLoading(false);
          setRows(ResetForResponseData(s.getUserEduPromotionList.response.rows,response.data,"educationPromotionSeq"));
        })
        .catch(error => {
          setLoading(false);
          setSnackbar(prev => ({
            ...prev,
            openSnackbar:true,
            severity:dictionary.MessageType.Error,
            message:GetText("msg_normal_error_message")
          }));
        })
  }

//////Dialog event
  const [isDialogOpen, setIsDialogOpen] = useState(false);
  const [isModified, setIsModified] = useState(false);
  
  const callFromDialog = (p) => {
    setIsModified(p.modified);

    if (p.refresh) {
      getData();
    }
  }

  const [sendInfo, setSendInfo] = useState({operateType:'', userId:props.userId, id:''});
  const { operateType,userId, id } = sendInfo;

  const handleDialogClose = (forceNoMessage) => {
    let forceClose = false;

    if (typeof forceNoMessage === 'boolean') forceClose = forceNoMessage;

    if (isModified && !forceClose) {
      setConfirmMessage({
        isConfirmOpen: true,
        messageType: dictionary.WARNING,
        messageId: "msg_confirm_close_dialog" 
      });
    } else {
      setIsDialogOpen(false);
    }
  };
  
  const handleDialogOpen = (operateType, id) => {
    setSendInfo({...sendInfo, operateType:operateType, id:id});
    setIsDialogOpen(true);
  };
////////////////////////////////////////////////////////

//////Confrim event in closing the dialog///////////////
  const [confirmMessage, setConfirmMessage] = useState({
    isConfirmOpen : false,
    messageType : "",
    messageId : ""
  });
  let { isConfirmOpen,messageType,messageId } = confirmMessage;

  const handleConfirmDialog = (answer) => {
    setConfirmMessage({
      ...confirmMessage,
      isConfirmOpen:false
    });

    if (answer){
      setIsDialogOpen(false);
    }
  }
////////////////////////////////////////////////////////

/////////////Setting for Snackbar////////////////////////////////
  let [snackbar, setSnackbar] = useState({openSnackbar:false, severity:'', message:''});
  let { openSnackbar, severity, message } = snackbar;
    
  const onCloseSnackbar = () => {
    setSnackbar(prev => ({
      ...prev,
      openSnackbar:false,
      severity:'',
      message:''
    }));
  }
////////////////////////////////////////////////////////

useEffect(() => {
  getData();
}, []);
 
  return (
    <React.Fragment>
      <Grid container spacing={1}>
        <Grid item xs={12}>
          <Paper className={classes.control}>
            <StickyHeadTable 
              tableId={"learningTable"} 
              onSelectRow={onSelectRow} 
              columns={columns} 
              rows={rows} 
              SupportCheckBox={view.table.allowCheckBox} 
              SupportPaging={view.table.allowPage}
              heightGap={250}
            />
          </Paper>
        </Grid>
        <Grid item xs={12}>
          <div className={classes.centerLocate}>
            <Fab color={userContext.preference.color} aria-label="add">
              <AddIcon onClick={() => handleDialogOpen(dictionary.NEW)} />
            </Fab>
          </div>
        </Grid>
      </Grid>
      {loading && <CircularProgress size={24} className={classes.buttonProgress} />}
      <CustomizedDialog isDialogOpen={isDialogOpen} sendInfo={sendInfo} viewInfo={viewInfo.userLearningDetail} handleDialogClose={handleDialogClose} callFromDialog={callFromDialog} />
      <Confirm open={isConfirmOpen} MessageType={messageType} MessageId={messageId} handleClose={handleConfirmDialog} />
      <CustomizedSnackbar isOpen={openSnackbar} severity={severity} message={message} closeHandler={onCloseSnackbar} />
     </React.Fragment>
  );
}