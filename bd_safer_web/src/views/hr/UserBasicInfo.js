import React, { useState, useEffect } from 'react';
import { GetText } from '../../common/BundleManager';
import serviceList from '../../service/ServiceList';
import { RunService } from '../../service/RestApi';
import Dictionary from '../../common/Dictionary';
import { CheckResultFromService, ResetForResponseData, CheckMenuAuth } from '../../common/Utility';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import FormLabel from '@material-ui/core/FormLabel';
import Address from '../../components/Address';
import ActionPanel from '../../components/ActionPanel';
import { FormStyle } from '../../style/Style';
import CustomizedSwitch from '../../components/Switch';
import { useUserContext } from '../../common/UserContext';
import CustomizedSnackbar from '../../components/Snackbar';
import CircularProgress from '@material-ui/core/CircularProgress';
import { useForm } from "react-hook-form";
import Confirm from '../../components/Confirm';
import UpwardAndWardCombo from '../../components/UpwardAndWardCombo';
import CustomizedTextField from '../../components/TextField';
import CodeComboBox from '../../components/CodeComboBox';
import YoutubeSearchedForIcon from '@material-ui/icons/YoutubeSearchedFor';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import CustomizedRadioGroup from '../../components/RadioGroup';
import TextFieldWithIcon from '../../components/TextFieldWithIcon';

const useStyles = FormStyle;
let originalInputs = {};
let isModified = false;

export default function UserBasicInfo(props) {
  const classes = useStyles();
  var dictionary = new Dictionary();
  const userContext = useUserContext();
  const s = new serviceList();
  const [operateType, setOperateType] = useState(props.sendInfo.operateType);
  const mode = props.sendInfo.mode;
  let [inputs, setInputs] = useState({userSeq:"",
                                      userId:"",
                                      firstName:"",
                                      lastName:"",
                                      citizenNo:"",
                                      upWardId:"",
                                      wardId:"",
                                      classCd:"",
                                      titleCd:"",
                                      workCd:"",
                                      workSectionCd:"",
                                      userPw:"",
                                      sysmgrYn:false,
                                      zipCode:"",
                                      address:"",
                                      homeTel:"",
                                      officeTel:"",
                                      cell:"",
                                      faxNo:"",
                                      extTel:"",
                                      email:"",
                                      birthDate:"",
                                      gender:"M"
                                    });
  let { userSeq,userId,firstName,lastName,citizenNo,upWardId,wardId,classCd,titleCd,workCd,workSectionCd,userPw,sysmgrYn,zipCode,address,homeTel,officeTel,cell,faxNo,extTel,email,birthDate,gender } = inputs;
  let [loading, setLoading] = useState(false);
  let [disabled, setDisabled] = useState(false);
  let [isUpdateMode, setIsUpdateMode] = useState(false);
  let [showPassword, setShowPassword] = useState(false);
  let [checkLoginId, setCheckLoginId] = useState({
    loginIdChecked: false,
    helpText: '',
    error: false
  });
  const hasUserCreateAuth = CheckMenuAuth(["HR001"]);
  
/////////Setting for validate the inputs in the form.
  const { handleSubmit } = useForm();
////////////////////////////////////////////////////

  const onChange = e => {
    let {name, value} = e.target;
    
    if (name === "sysmgrYn") { value = !inputs.sysmgrYn};
    if (name === 'userId' && !checkLoginId.loginIdChecked) {
      setCheckLoginId({...checkLoginId, helpText:GetText("msg_check_loginid")});
    }
    isModified = true;
    setInputs({
      ...inputs,
      [name]:value
    });
  }

/////////////Setting for Snackbar////////////////////////////////
  let [snackbar, setSnackbar] = useState({openSnackbar:false, severity:'', message:''});
  let { openSnackbar, severity, message } = snackbar;
    
  const onCloseSnackbar = () => {
    setSnackbar(prev => ({
      ...prev,
      openSnackbar:false,
      severity:'',
      message:''
    }));
  }
/////////////////////////////////////////////

//////Confrim event
  const [confirmMessage, setConfirmMessage] = React.useState({
                                                      isConfirmOpen : false,
                                                      messageType : "",
                                                      messageId : ""
                                                });
  let { isConfirmOpen,messageType,messageId } = confirmMessage;
  
  const handleConfirmDialog = (answer) => {
    setConfirmMessage({
      ...confirmMessage,
      isConfirmOpen:false
    });
  
    if (answer){
      onSubmit();
    }
  }
  /////////////////////////////////////////////
  
  ////Button operate
  
  const onChangeUpdateState = () => {
    setIsUpdateMode(!isUpdateMode);
    if (!isUpdateMode) {
      onUpdateMode();
    } else {
      onReadMode(userId);
    }
  }

  const onReadMode = (id) => {
    setDisabled(true);
    setOperateType("");
    setIsUpdateMode(false);
    setCheckLoginId({...checkLoginId, loginIdChecked:true});
    props.callFromDialog({id:id, operateType:dictionary.READ, modified: false});
  }

  const onNewMode = () => {
    setDisabled(false);
    setOperateType(dictionary.NEW);
    setInputs({ userSeq:"",
                userId:"",
                firstName:"",
                lastName:"",
                citizenNo:"",
                upWardId:"",
                wardId:"",
                classCd:"",
                titleCd:"",
                workCd:"",
                workSectionCd:"",
                userPw:"",
                sysmgrYn:false,
                zipCode:"",
                address:"",
                homeTel:"",
                officeTel:"",
                cell:"",
                faxNo:"",
                extTel:"",
                email:"",
                birthDate:"",
                gender:"M"
            });
    props.callFromDialog({id:'', operateType:dictionary.NEW, modified: false});
    originalInputs = {userSeq:"",
                      userId:"",
                      firstName:"",
                      lastName:"",
                      citizenNo:"",
                      upWardId:"",
                      wardId:"",
                      classCd:"",
                      titleCd:"",
                      workCd:"",
                      workSectionCd:"",
                      userPw:"",
                      sysmgrYn:false,
                      zipCode:"",
                      address:"",
                      homeTel:"",
                      officeTel:"",
                      cell:"",
                      faxNo:"",
                      extTel:"",
                      email:"",
                      birthDate:"",
                      gender:"M"
                  };
    isModified = false;
    setCheckLoginId({...checkLoginId, loginIdChecked:false});
  }

  const onUpdateMode = () => {
    setDisabled(false);
    setOperateType(dictionary.UPDATE);
    props.callFromDialog({id:userId, operateType:dictionary.UPDATE, modified: false});
  }

  const onConfirm = () => {
    if (operateType === dictionary.NEW || operateType === dictionary.UPDATE) {
      if (!checkLoginId.loginIdChecked) {
        setSnackbar(prev => ({
          ...prev,
          openSnackbar:true,
          severity:dictionary.MessageType.Error,
          message:GetText("msg_check_loginid")
        }));
  
        return;
      }
    }

    if (operateType === dictionary.NEW) {
      setConfirmMessage({ isConfirmOpen: true, messageType: dictionary.WARNING, messageId : "msg_will_you_insert" });
    } else if (operateType === dictionary.UPDATE) {
      setConfirmMessage({ isConfirmOpen: true, messageType: dictionary.WARNING, messageId : "msg_will_you_update" });
    } else if (hasUserCreateAuth) {
      onNewMode();
    }
  }

  const onDelete = () => {
    setOperateType(dictionary.DELETE);
    setConfirmMessage({ isConfirmOpen: true, messageType: dictionary.WARNING, messageId : "msg_will_you_delete" });
  }

/////////////////Check whether inputs are modified or not. If modified, it sends the state to parent//////////////////// 
  useEffect(() => {
    let Modified =  (JSON.stringify(inputs) === JSON.stringify(originalInputs)? false : true);
    
    if (operateType !== dictionary.READ) props.callFromDialog({id:userId, modified: Modified && isModified});
  }, [inputs]);
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////Get data////////////////////////////////////////////
  useEffect(() => {
    async function getInfo(id) {
      let body = {"userId": id};
      setLoading(true);
      RunService(s.getUserInfo, body)
          .then(response => {
            let reponseData = ResetForResponseData(s.getUserInfo.response, response.data);
            setInputs(reponseData);
            originalInputs = reponseData;
            onReadMode(id);
            setLoading(false);
          })
          .catch(error => {
            setSnackbar(prev => ({
              ...prev,
              openSnackbar:true,
              severity:dictionary.MessageType.Error,
              message:GetText("msg_normal_error_message")
            }));
          })
    }

    if (operateType === dictionary.READ && props.sendInfo.id) getInfo(props.sendInfo.id);
  }, []);
  /////////////////////////////////////////////////////////////////

  ///////////////////// Insert or Update///////////////////////////
  const onSubmit = () => {
    if (operateType === "") {
      onNewMode();
      return;
    }

    setLoading(true);
    let service;
    let body;

    switch(operateType) {
      case dictionary.NEW :
        service = s.insertUser;
        body = inputs;
        break;
      case dictionary.UPDATE :
        service = s.updateUser;
        body = inputs;
        break;
      case dictionary.DELETE :
        service = s.deleteUser;
        body = [inputs.userId]; 
        break;
      default :
        return;
    }
  
    RunService(service,{adminMode:userContext.userInfo.isAdmin ? "1":"0" } ,body)
        .then(response => {
          setLoading(false);
          var result = CheckResultFromService(response.data);
          if (result.isSuccess) {
            setSnackbar(prev => ({
              ...prev,
              openSnackbar:true,
              severity:dictionary.MessageType.Success,
              message:GetText(result.message)
            }));
                        
            onReadMode(result.id);
            
            if (operateType === dictionary.DELETE) {
              props.handleDialogClose(true);
            } else {
              props.callFromDialog({id:result.id, modified: false, refresh:true});
            }
          } else {
            setSnackbar(prev => ({
              ...prev,
              openSnackbar:true,
              severity:dictionary.MessageType.Error,
              message:GetText(result.message)
            }));
          }
        })
        .catch(error => {
          setLoading(false);
          setSnackbar(prev => ({
            ...prev,
            openSnackbar:true,
            severity:dictionary.MessageType.Error,
            message:GetText("msg_normal_error_message")
          }));
        })
  } 
  /////////////////////////////////////////////////////////////////

  const checkLoginDuplicated = () => {
    setLoading(true);
    var body = {loginId: userId};
    RunService(s.getCheckLoginIdDuplicated, body)
          .then(response => {
            if (response.data) {//true is duplicated, false:it is unique.
              setCheckLoginId({loginIdChecked:false, helpText:GetText("msg_id_duplicated"), error:true});
            } else {
              setCheckLoginId({loginIdChecked:true, helpText:GetText("msg_id_available"), error:false});
            }
            setLoading(false);
          })
          .catch(error => {
            setSnackbar(prev => ({
              ...prev,
              openSnackbar:true,
              severity:dictionary.MessageType.Error,
              message:GetText("msg_normal_error_message")
            }));
            setLoading(false);
          })
  }

  const onPasswordIconEvent = () => {
    setShowPassword(!showPassword);
  }

  return (
    <React.Fragment>
      <form onSubmit={handleSubmit(onConfirm)}>
      <div className={classes.paper}>
        <FormLabel component="legend">{GetText("basic_information")}</FormLabel>
      </div>
      <Paper className={classes.control}>
        <Grid container spacing={1}>
          <Grid item xs={12}>
            <div className={classes.paper}>
            <CustomizedTextField
                id="firstName"
                label={GetText("first_name")}
                onChange={onChange}
                disabled={disabled}
                value={firstName}
                required={true}
                className={classes.textFieldS}
              />     
              <CustomizedTextField
                id="lastName"
                label={GetText("last_name")}
                onChange={onChange}
                disabled={disabled}
                value={lastName}
                required={true}
                className={classes.textFieldS}
              />  
              <CustomizedTextField
                id="citizenNo"
                label={GetText("citizen_number")}
                onChange={onChange}
                disabled={disabled}
                value={citizenNo}
                required={true}
                className={classes.textFieldS}
              />  
              <CustomizedTextField
                id="birthDate"
                label={GetText("birth_day")}
                type="date"
                onChange={onChange}
                disabled={disabled}
                value={birthDate}
                className={classes.textFieldS}
              />
              <CustomizedRadioGroup 
                id="gender" 
                label={GetText("gender")}
                defaultValue={gender}
                value={gender}
                radios={[{label:GetText("female"), value:"F"},{label:GetText("male"), value:"M"}]} 
                onChange={onChange}
                disabled={disabled}
                className={classes.textFieldS}
              />
            </div>
          </Grid>
          <Grid item xs={12}>
            <div className={classes.paper}>
            <TextFieldWithIcon 
                id="userId"
                label={GetText("user_id")}
                onChange={onChange}
                disabled={disabled || operateType === dictionary.UPDATE}
                value={userId}
                iconEvent={checkLoginDuplicated}
                icon={operateType === dictionary.NEW ? <YoutubeSearchedForIcon /> : ""}
                helpText={checkLoginId.helpText}
                error={checkLoginId.error}
                required={true}
                className={classes.textFieldS}
              />
              {operateType === dictionary.NEW &&
                <TextFieldWithIcon 
                  id="userPw"
                  label={GetText("password")}
                  onChange={onChange}
                  type={showPassword ? 'text' : 'password'}
                  disabled={disabled}
                  value={userPw}
                  iconEvent={onPasswordIconEvent}
                  icon={showPassword ? <Visibility /> : <VisibilityOff />}
                  required={true}
                  className={classes.textFieldS}
                />
              }
              <CustomizedSwitch 
                id='sysmgrYn'
                name='sysmgrYn' 
                checked={sysmgrYn}
                onChange={onChange}
                value={sysmgrYn}
                label={GetText("is_ward_administrator")} 
                disabled={disabled}
              />  
            </div>
          </Grid>
        </Grid>
      </Paper>   
      <div className={classes.paper}>
        <FormLabel component="legend">{GetText("info_on_work")}</FormLabel>
      </div>
      <Paper className={classes.control}>
        <Grid container spacing={1}>
          <Grid item xs={12}>
            <div className={classes.paper}>
              <UpwardAndWardCombo 
                upperWardId="upWardId"
                className={classes.textFieldS} 
                callEvent={onChange} 
                selectedUpperWardValue={upWardId}
                selectedWardValue={wardId}
                disabled={disabled}
                typeClsCds={[2,3,4,5,6]}
              />
              <CodeComboBox
                  groupCode={'090'}
                  selectedCode={titleCd}
                  childLabel={GetText("position")}
                  childComboId={'titleCd'}
                  callEvent={onChange}
                  childOnly={true}
                  disabled={disabled}
                  className={classes.textFieldS}
              />
              <CodeComboBox
                  groupCode={'089001'}
                  selectedCode={classCd}
                  childLabel={GetText("job_class")}
                  childComboId={'classCd'}
                  callEvent={onChange}
                  childOnly={true}
                  disabled={disabled}
                  className={classes.textFieldS}
              /> 
              <CodeComboBox
                  groupCode={'091'}
                  selectedCode={workCd}
                  childLabel={GetText("job_type")}
                  childComboId={'workCd'}
                  callEvent={onChange}
                  childOnly={true}
                  disabled={disabled}
                  className={classes.textFieldS}
              /> 
              <CodeComboBox
                  groupCode={'092'}
                  selectedCode={workSectionCd}
                  childLabel={GetText("wark_cls")}
                  childComboId={'workSectionCd'}
                  callEvent={onChange}
                  childOnly={true}
                  disabled={disabled}
                  className={classes.textFieldS}
              /> 
            </div>
          </Grid>
        </Grid>
      </Paper>   
      <div className={classes.paper}>
        <FormLabel component="legend">{GetText("contact")}</FormLabel>
      </div>
      <Paper className={classes.control}>
        <Grid container spacing={1}>
        <Grid item xs={12}>
            <div className={classes.paper}>
              <Address 
                addressId={'zipCode'} 
                etcAddressid={'address'} 
                changeEvent={onChange}
                disabled={disabled}
                postalCodeValue={zipCode}
                etcAddressValue={address}
              />
              <CustomizedTextField
                label={GetText("home_tel")}
                id="homeTel"
                onChange={onChange}
                disabled={disabled}
                value={homeTel}
                className={classes.textFieldS}
              />
              <CustomizedTextField
                label={GetText("mobile_number")}
                id="cell"
                onChange={onChange}
                disabled={disabled}
                value={cell}
                className={classes.textFieldS}
              />
            </div>
          </Grid>
          <Grid item xs={12}>
            <div className={classes.paper}>
              <CustomizedTextField
                label={GetText("office_tel")}
                id="officeTel"
                onChange={onChange}
                disabled={disabled}
                value={officeTel}
                className={classes.textFieldS}
              />
              <CustomizedTextField
                label={GetText("ext_num")}
                id="extTel"
                onChange={onChange}
                disabled={disabled}
                value={extTel}
                className={classes.textFieldS}
              />
              <CustomizedTextField
                label={GetText("fax")}
                id="faxNo"
                onChange={onChange}
                disabled={disabled}
                value={faxNo}
                className={classes.textFieldS}
              />
              <CustomizedTextField
                label={GetText("email")}
                id="email"
                onChange={onChange}
                disabled={disabled}
                value={email}
                className={classes.textFieldS}
              />
            </div>
          </Grid>
        </Grid>
      </Paper> 
      <Box p={1}></Box>
      <Divider style={{'width':'100%','position': 'absolute', 'left': 0}} />
      <ActionPanel>
        <Button 
          disabled={loading}
          color={userContext.preference.color}
          type="submit"
        >
          { operateType !== "" ? GetText("save") : hasUserCreateAuth ? GetText("new") : "" }
        </Button>
        { 
          inputs.userId && operateType !== dictionary.NEW && hasUserCreateAuth &&
            <Button 
              disabled={loading}
              color={userContext.preference.color}
              name="update"
              onClick={onChangeUpdateState}
            >
              { isUpdateMode ? GetText("cancel") : GetText("update") }
            </Button>
        }
        
        {
          inputs.userId && hasUserCreateAuth &&
            <Button 
              disabled={loading}
              color={userContext.preference.color}
              name="delete"
              onClick={onDelete}
          >
              {GetText("delete")}
            </Button> 
        }
        
        {loading && <CircularProgress size={24} className={classes.buttonProgress} />}
      </ActionPanel>
      </form>
      <CustomizedSnackbar isOpen={openSnackbar} severity={severity} message={message} closeHandler={onCloseSnackbar} />
      <Confirm open={isConfirmOpen} MessageType={messageType} MessageId={messageId} handleClose={handleConfirmDialog} />
    </React.Fragment>
  );
}