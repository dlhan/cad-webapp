import React from 'react';
import Home from './Home';
import WardList from './is/WardList';
import WardDetail from './is/WardDetail';
import FireObjectList from './is/FireObjectList';
import FireObjectInfo from './is/FireObjectInfo';
import FireObjectDetail from './is/FireObjectDetail';
import BuildingInfoList from './is/BuildingInfoList';
import BuildingInfoDetail from './is/BuildingInfoDetail';
import StoryInfoList from './is/StoryInfoList';
import StoryInfoDetail from './is/StoryInfoDetail';
import StorySpecUsedDetail from './is/StorySpecUsedDetail';
import FireEquipmentList from './is/FireEquipmentList';
import FireEquipmentDetail from './is/FireEquipmentDetail';
import EmergencyPlan from './is/EmergencyPlan';
import EmegencyPicture from './is/EmegencyPicture';
import EmegencyPictureDetail from './is/EmegencyPicture';
import BuildingHistoryList from './is/BuildingHistoryList';
import BuildingHistoryDetail from './is/BuildingHistoryDetail';
import ArsonManagerList from './is/ArsonManagerList';
import ArsonManagerDetail from './is/ArsonManagerDetail';
import SafetyManagerList from './is/SafetyManagerList';
import SafetyManagerDetail from './is/SafetyManagerDetail';
import BuildingManagerList from './is/BuildingManagerList';
import BuildingManagerDetail from './is/BuildingManagerDetail';
import UserPreference from './UserPreference';
import UserList from './hr/UserList';
import UserInfo from './hr/UserInfo';
import UserBasicInfo from './hr/UserBasicInfo';
import UserEducationHistory from './hr/UserEducationHistory';
import UserEducationDetail from './hr/UserEducationDetail';
import UserLicencePossessed from './hr/UserLicencePossessed';
import UserLicencePossessedDetail from './hr/UserLicencePossessedDetail';
import UserLearningAchivement from './hr/UserLearningAchivement';
import UserLearningAchivementDetail from './hr/UserLearningAchivementDetail';
import UserFireTrainAchivement from './hr/UserFireTrainAchivement';
import UserFireTrainAchivementDetail from './hr/UserFireTrainAchivementDetail';
import CodeTreeView from './sm/CodeTreeView';
import Notification from './Notification';
import CreateAccount from './CreateAccount';
import Dictionary from '../common/Dictionary';

export default function SelectView(viewInfo, callFromDialog=null, sendInfo=null, handleDialogClose) {
  let dictionary = new Dictionary();

  switch (viewInfo.viewName) {
      case dictionary.View_WardList:
        return <WardList callFromDialog={callFromDialog} sendInfo={sendInfo} handleDialogClose={handleDialogClose} />;
      case dictionary.View_WardDetail:
        return <WardDetail callFromDialog={callFromDialog} sendInfo={sendInfo} handleDialogClose={handleDialogClose} />;
      case dictionary.View_FireObjectList:
        return <FireObjectList callFromDialog={callFromDialog} sendInfo={sendInfo} handleDialogClose={handleDialogClose} />;
      case dictionary.View_FireObjectInfo:
        return <FireObjectInfo callFromDialog={callFromDialog} sendInfo={sendInfo} handleDialogClose={handleDialogClose} />;
      case dictionary.View_FireObjectDetail:
        return <FireObjectDetail callFromDialog={callFromDialog} sendInfo={sendInfo} handleDialogClose={handleDialogClose} />;
      case dictionary.View_BuildingInfoList:
        return <BuildingInfoList callFromDialog={callFromDialog} sendInfo={sendInfo} handleDialogClose={handleDialogClose} />;
      case dictionary.View_BuildingInfoDetail:
        return <BuildingInfoDetail callFromDialog={callFromDialog} sendInfo={sendInfo} handleDialogClose={handleDialogClose} />;
      case dictionary.View_BuildingStoryInfoList:
        return <StoryInfoList callFromDialog={callFromDialog} sendInfo={sendInfo} handleDialogClose={handleDialogClose} />;
      case dictionary.View_BuildingStoryInfoDetail:
        return <StoryInfoDetail callFromDialog={callFromDialog} sendInfo={sendInfo} handleDialogClose={handleDialogClose} />;    
      case dictionary.View_BuildingStorySpecUsedInfoDetail:
        return <StorySpecUsedDetail callFromDialog={callFromDialog} sendInfo={sendInfo} handleDialogClose={handleDialogClose} />;    
      case dictionary.View_FireEquipmentStateList:
        return <FireEquipmentList callFromDialog={callFromDialog} sendInfo={sendInfo} handleDialogClose={handleDialogClose} />;
      case dictionary.View_FireEquipmentStateDetail:
        return <FireEquipmentDetail callFromDialog={callFromDialog} sendInfo={sendInfo} handleDialogClose={handleDialogClose} />;   
      case dictionary.View_EmergencyRescuePlan:
        return <EmergencyPlan callFromDialog={callFromDialog} sendInfo={sendInfo} handleDialogClose={handleDialogClose} />;
      case dictionary.View_EmergencyRescuePictureList:
        return <EmegencyPicture callFromDialog={callFromDialog} sendInfo={sendInfo} handleDialogClose={handleDialogClose} />;   
      case dictionary.View_EmergencyRescuePictureDetail:
        return <EmegencyPictureDetail callFromDialog={callFromDialog} sendInfo={sendInfo} handleDialogClose={handleDialogClose} />;   
      case dictionary.View_BuildingHistoryList:
        return <BuildingHistoryList callFromDialog={callFromDialog} sendInfo={sendInfo} handleDialogClose={handleDialogClose} />;   
      case dictionary.View_BuildingHistoryDetail:
        return <BuildingHistoryDetail callFromDialog={callFromDialog} sendInfo={sendInfo} handleDialogClose={handleDialogClose} />;  
      case dictionary.View_ArsonManagerList:
        return <ArsonManagerList callFromDialog={callFromDialog} sendInfo={sendInfo} handleDialogClose={handleDialogClose} />;   
      case dictionary.View_ArsonManagerDetail:
        return <ArsonManagerDetail callFromDialog={callFromDialog} sendInfo={sendInfo} handleDialogClose={handleDialogClose} />;  
      case dictionary.View_SafetyManagerList:
        return <SafetyManagerList callFromDialog={callFromDialog} sendInfo={sendInfo} handleDialogClose={handleDialogClose} />;   
      case dictionary.View_SafetyManagerDetail:
        return <SafetyManagerDetail callFromDialog={callFromDialog} sendInfo={sendInfo} handleDialogClose={handleDialogClose} />;  
      case dictionary.View_BuildingManagerList:
        return <BuildingManagerList callFromDialog={callFromDialog} sendInfo={sendInfo} handleDialogClose={handleDialogClose} />;   
      case dictionary.View_BuildingManagerDetail:
        return <BuildingManagerDetail callFromDialog={callFromDialog} sendInfo={sendInfo} handleDialogClose={handleDialogClose} />;  
      case dictionary.View_UserPreference:
        return <UserPreference callFromDialog={callFromDialog} sendInfo={sendInfo} handleDialogClose={handleDialogClose} />;
      case dictionary.View_UserList:
        return <UserList callFromDialog={callFromDialog} sendInfo={sendInfo} handleDialogClose={handleDialogClose} />;
      case dictionary.View_UserInfo:
        return <UserInfo callFromDialog={callFromDialog} sendInfo={sendInfo} handleDialogClose={handleDialogClose} />;
      case dictionary.View_UserBasicInfo:
        return <UserBasicInfo callFromDialog={callFromDialog} sendInfo={sendInfo} handleDialogClose={handleDialogClose} />;
      case dictionary.View_UserEducationHistory:
        return <UserEducationHistory callFromDialog={callFromDialog} sendInfo={sendInfo} handleDialogClose={handleDialogClose} />;
      case dictionary.View_UserEducationDetail:
        return <UserEducationDetail callFromDialog={callFromDialog} sendInfo={sendInfo} handleDialogClose={handleDialogClose} />;
      case dictionary.View_UserLicenseList:
        return <UserLicencePossessed callFromDialog={callFromDialog} sendInfo={sendInfo} handleDialogClose={handleDialogClose} />;
      case dictionary.View_UserLicenseDetail:
        return <UserLicencePossessedDetail callFromDialog={callFromDialog} sendInfo={sendInfo} handleDialogClose={handleDialogClose} />;
      case dictionary.View_UserLearningList:
        return <UserLearningAchivement callFromDialog={callFromDialog} handleDialogClose={handleDialogClose} />;
      case dictionary.View_UserLearningDetail:
        return <UserLearningAchivementDetail callFromDialog={callFromDialog} sendInfo={sendInfo} handleDialogClose={handleDialogClose} />;
      case dictionary.View_UserFireTrainList:
        return <UserFireTrainAchivement callFromDialog={callFromDialog} sendInfo={sendInfo} handleDialogClose={handleDialogClose} />;
      case dictionary.View_UserFireTrainDetail:
        return <UserFireTrainAchivementDetail callFromDialog={callFromDialog} sendInfo={sendInfo} handleDialogClose={handleDialogClose} />;
      case dictionary.View_CreateAccount:
        return <CreateAccount callFromDialog={callFromDialog} handleDialogClose={handleDialogClose} />;
      case dictionary.View_Notification:
        return <Notification callFromDialog={callFromDialog} handleDialogClose={handleDialogClose} />;
      case dictionary.View_GroupCodeTreeView:
        return <CodeTreeView callFromDialog={callFromDialog} sendInfo={sendInfo} />;
      default:
        return <Home />;
  }
}