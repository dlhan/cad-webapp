import React, { useState, useEffect } from 'react';
import { GetText } from '../common/BundleManager';
import serviceList from '../service/ServiceList';
import { RunService } from '../service/RestApi';
import Dictionary from '../common/Dictionary';
import { CheckResultFromService, ResetForResponseData } from '../common/Utility';
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import ActionPanel from '../components/ActionPanel';
import { FormStyle } from '../style/Style';
import Grid from '@material-ui/core/Grid';
import CustomizedSwitch from '../components/Switch';
import { useUserContext, useUserDispatch } from '../common/UserContext';
import CustomizedSnackbar from '../components/Snackbar';
import CircularProgress from '@material-ui/core/CircularProgress';
import { useForm } from "react-hook-form";
import Confirm from '../components/Confirm';
import NativeSelect from '@material-ui/core/NativeSelect';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import CustomizedTextField from '../components/TextField';

const useStyles = FormStyle;
let originalInputs = {};
let isModified = false;

export default function UserPreference(props) {
  const classes = useStyles();
  var dictionary = new Dictionary();
  const userContext = useUserContext();
  const s = new serviceList();

  let [inputs, setInputs] = useState({
         userId:userContext.userInfo.userId
        ,language:userContext.preference.language
        ,color:userContext.preference.color
        ,theme:userContext.preference.theme
        ,tableRows:userContext.preference.tableRowsCount
        ,tableDense:userContext.preference.tableIsDense
  });
  let { language, color, theme, tableRows, tableDense } = inputs;
  let [loading, setLoading] = useState(false);
/////////Setting for validate the inputs in the form.
  const { handleSubmit } = useForm();
////////////////////////////////////////////////////

  const userContextDispatch = useUserDispatch();

  const onChange = e => {
    let {name, value} = e.target;
    if (name === "tableDense") { value = !inputs.tableDense};
    if (name === "tableRows" && value < 10) { 
        setHelperText(GetText("msg_minimum_number_10")); 
        value = 10;
    } else {
        setHelperText(GetText(""));
    };

    isModified = true;
    setInputs({
      ...inputs,
      [name]:value
    });
  }

/////////////Setting for Snackbar////////////////////////////////
  let [snackbar, setSnackbar] = useState({openSnackbar:false, severity:'', message:''});
  let [helperText, setHelperText] = useState("");
  let { openSnackbar, severity, message } = snackbar;
    
  const onCloseSnackbar = () => {
    setSnackbar(prev => ({
      ...prev,
      openSnackbar:false,
      severity:'',
      message:''
    }));
  }
/////////////////////////////////////////////

//////Confrim event
  const [confirmMessage, setConfirmMessage] = React.useState({
                                                      isConfirmOpen : false,
                                                      messageType : dictionary.MessageType.Info,
                                                      messageId : ""
                                                });
  let { isConfirmOpen,messageType,messageId } = confirmMessage;
  
  const handleConfirmDialog = (answer) => {
    setConfirmMessage({
      ...confirmMessage,
      isConfirmOpen:false
    });
  
    if (answer){
      onSubmit();
    }
  }
  /////////////////////////////////////////////
  
  ////Button operate
  const onConfirm = () => {
    setConfirmMessage({ isConfirmOpen: true, messageType: dictionary.Info, messageId : "msg_will_you_save" });
  }

/////////////////Check whether inputs are modified or not. If modified, it sends the state to parent//////////////////// 
  useEffect(() => {
    let Modified =  (JSON.stringify(inputs) === JSON.stringify(originalInputs)? false : true);
    props.callFromDialog({modified: Modified && isModified});
  }, [inputs]);
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////Get data////////////////////////////////////////////
  useEffect(() => {
    let completed = false; 
    
    async function getInfo() {
      let body = {"userId": userContext.userInfo.userId};
      setLoading(true);
      RunService(s.getUserPreference, body)
          .then(response => {
            if (!completed) { 
              let reponseData = ResetForResponseData(s.getUserPreference.response, response.data);
              if (reponseData.userPreferenceId) {
                setInputs(reponseData);
                originalInputs = reponseData;
              }
            }
            completed= true;
            setLoading(false);
          })
          .catch(error => {
            setSnackbar(prev => ({
              ...prev,
              openSnackbar:true,
              severity:dictionary.MessageType.Error,
              message:GetText("msg_normal_error_message")
            }));
          })
    }

    getInfo();
    
    return () => {
      completed = true;
    };
  }, []);
  /////////////////////////////////////////////////////////////////

  ///////////////////// Insert or Update///////////////////////////
  const onSubmit = () => {
    setLoading(true);
    let service;
    let body;

    service = s.upsertUserPreference;
    body = inputs;
    RunService(service,"",body)
        .then(response => {
          setLoading(false);
          var result = CheckResultFromService(response.data);
          if (result.isSuccess) {
            setSnackbar(prev => ({
              ...prev,
              openSnackbar:true,
              severity:dictionary.MessageType.Success,
              message:GetText(result.message)
            }));
            
            originalInputs = inputs;
            
            userContextDispatch({type: dictionary.CHANGE_USER_PREFERENCE,data: inputs});
            props.callFromDialog({modified: false});
            isModified = false;
          } else {
            setSnackbar(prev => ({
              ...prev,
              openSnackbar:true,
              severity:dictionary.MessageType.Error,
              message:GetText(result.message)
            }));
          }
        })
        .catch(error => {
          setLoading(false);
          setSnackbar(prev => ({
            ...prev,
            openSnackbar:true,
            severity:dictionary.MessageType.Error,
            message:GetText("msg_normal_error_message")
          }));
        })
  } 
  /////////////////////////////////////////////////////////////////

  return (
    <React.Fragment>
      <form onSubmit={handleSubmit(onConfirm)}>
      <Paper className={classes.control}>
        <Grid container spacing={1}>
          <Grid item xs={12}>
            <div className={classes.paper}>
              <FormControl className={classes.textField2} >
                <InputLabel shrink htmlFor="age-native-label-placeholder">
                  {GetText("language")}
                </InputLabel>
                <NativeSelect
                    value={language}
                    onChange={onChange}
                    inputProps={{
                        name: 'language',
                        id: 'language',
                    }}
                >
                  <option value="en">English</option>
                  <option value="kr">한글</option>
                </NativeSelect>
              </FormControl>
            </div>
          </Grid>
          <Grid item xs={12}>
            <div className={classes.paper}>
              <FormControl className={classes.textField2} >
                <InputLabel shrink htmlFor="age-native-label-placeholder">
                  {GetText("theme")}
                </InputLabel>
                <NativeSelect
                    value={theme}
                    onChange={onChange}
                    inputProps={{
                        name: 'theme',
                        id: 'theme',
                    }}
                >
                  <option value="theme1">theme1</option>
                  <option value="theme2">theme2</option>
                </NativeSelect>
              </FormControl>
            </div>
          </Grid>
          <Grid item xs={12}>
            <div className={classes.paper}>
              <FormControl className={classes.textField2} >
                <InputLabel shrink htmlFor="age-native-label-placeholder">
                  {GetText("color")}
                </InputLabel>
                <NativeSelect
                    value={color}
                    onChange={onChange}
                    inputProps={{
                        name: 'color',
                        id: 'color',
                    }}
                >
                  <option value="primary">primary</option>
                  <option value="secondary">secondary</option>
                </NativeSelect>
              </FormControl>
            </div>
          </Grid>
          <Grid item xs={12}>
            <CustomizedTextField
              className={classes.textField25}
              label={GetText("table_rows")}
              id="tableRows"
              name="tableRows"
              onChange={onChange}
              value={tableRows}
              type="number"
              min={15}
              helperText={helperText}
            />       
          </Grid>
          <Grid item xs={12}>
            <div className={classes.paper}>
              <CustomizedSwitch 
                className={classes.textField25}
                id='tableDense'
                name='tableDense' 
                checked={tableDense}
                onChange={onChange}
                value={tableDense}
                label={GetText("table_dense_padding")} 
              />
            </div>
          </Grid>
        </Grid>
      </Paper>   
      <Box p={1}></Box>
      <Divider style={{'width':'100%','position': 'absolute', 'left': 0}} />
      <ActionPanel>
        <Button 
          color={userContext.preference.color}
          type="submit"
        >
          { GetText("save") }
        </Button>
        {loading && <CircularProgress size={24} className={classes.buttonProgress} />}
      </ActionPanel>
      </form>
      <CustomizedSnackbar isOpen={openSnackbar} severity={severity} message={message} closeHandler={onCloseSnackbar} />
      <Confirm open={isConfirmOpen} MessageType={messageType} MessageId={messageId} handleClose={handleConfirmDialog} />
    </React.Fragment>
  );
}