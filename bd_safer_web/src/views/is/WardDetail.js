import React, { useState, useEffect } from 'react';
import { GetText } from '../../common/BundleManager';
import serviceList from '../../service/ServiceList';
import { RunService } from '../../service/RestApi';
import Dictionary from '../../common/Dictionary';
import { CheckResultFromService, ResetForResponseData } from '../../common/Utility';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import Address from '../../components/Address';
import ActionPanel from '../../components/ActionPanel';
import { FormStyle } from '../../style/Style';
import CustomizedSwitch from '../../components/Switch';
import { useUserContext } from '../../common/UserContext';
import CustomizedSnackbar from '../../components/Snackbar';
import CircularProgress from '@material-ui/core/CircularProgress';
import ViewInfo from '../../common/ViewInfo';
import { useForm } from "react-hook-form";
import Confirm from '../../components/Confirm';
import UpperAndOfficeSelectByTypeClsCd from '../../components/UpperAndOfficeSelectByTypeClsCd';
import CustomizedTextField from '../../components/TextField';
import TextFieldWithIcon from '../../components/TextFieldWithIcon';
import FindInPageIcon from '@material-ui/icons/FindInPage';
import CustomizedDialog from '../../components/Dialog';

const useStyles = FormStyle;
let originalInputs = {};
let isModified = false;

export default function WardDetail(props) {
  const classes = useStyles();
  var dictionary = new Dictionary();
  const userContext = useUserContext();
  const s = new serviceList();
  var viewInfo = new ViewInfo();

  const [operateType, setOperateType] = useState(props.sendInfo.operateType);
  let [inputs, setInputs] = useState({wardId:''
                                    ,upwardId:''
                                    ,fireOfficeId:''
                                    ,wardName:''
                                    ,typeClsCd:''
                                    ,typeClsName:''
                                    ,nickName:''
                                    ,zipCode:''
                                    ,address:''
                                    ,dspGroupYn:false
                                    ,autoListYn:false
                                    ,dayTel:''
                                    ,nightTel:''
                                    ,extTel:''
                                    ,dayFax:''
                                    ,nightFax:''
                                    ,wardopenDate:''
                                    ,wardcloseDate:''
                                    ,wardopenYn:true
                                    ,brdUsingYn:false
                                    ,remark:''
                                    ,wardOrder:''
                                    ,x:''
                                    ,y:''
                                    ,useYn:''
                                    });
  let { wardId, upwardId, wardName,typeClsCd,typeClsName,nickName,zipCode,address,dspGroupYn,autoListYn,dayTel,nightTel,extTel,dayFax,nightFax,wardopenDate,wardcloseDate,wardopenYn,brdUsingYn,remark,wardOrder,x,y } = inputs;
  let [loading, setLoading] = useState(false);
  let [disabled, setDisabled] = useState(false);
  let [isUpdateMode, setIsUpdateMode] = useState(false);
  
//////////Upward select event/////////////////////////////////////////
  const callEvent = (e) => {
    setInputs({
      ...inputs,
      upperWardId:e[Object.keys(e)[0]]
    });
  }
///////////////////////////////////////////////////
  
/////////Setting for validate the inputs in the form.
  const { handleSubmit } = useForm();
////////////////////////////////////////////////////

  const onChange = e => {
    let {name, value} = e.target;
    
    if (name === "dspGroupYn") { value = !inputs.dspGroupYn};
    if (name === "autoListYn") { value = !inputs.autoListYn};
    if (name === "wardopenYn") { value = !inputs.wardopenYn};
    
    isModified = true;
    setInputs({
      ...inputs,
      [name]:value
    });
  }

/////////////Setting for Snackbar////////////////////////////////
  let [snackbar, setSnackbar] = useState({openSnackbar:false, severity:'', message:''});
  let { openSnackbar, severity, message } = snackbar;
    
  const onCloseSnackbar = () => {
    setSnackbar(prev => ({
      ...prev,
      openSnackbar:false,
      severity:'',
      message:''
    }));
  }
/////////////////////////////////////////////

//////Confrim event
  const [confirmMessage, setConfirmMessage] = React.useState({
                                                      isConfirmOpen : false,
                                                      messageType : "",
                                                      messageId : ""
                                                });
  let { isConfirmOpen,messageType,messageId } = confirmMessage;
  
  const handleConfirmDialog = (answer) => {
    setConfirmMessage({
      ...confirmMessage,
      isConfirmOpen:false
    });
  
    if (answer){
      onSubmit();
    }
  }
  /////////////////////////////////////////////
  
  ////Button operate
  
  const onChangeUpdateState = () => {
    setIsUpdateMode(!isUpdateMode);
    if (!isUpdateMode) {
      onUpdateMode();
    } else {
      onReadMode(wardId);
    }
  }

  const onReadMode = () => {
    setDisabled(true);
    setOperateType("");
    setIsUpdateMode(false);
    props.callFromDialog({modified: false});
  }

  const onNewMode = () => {
    setDisabled(false);
    setOperateType(dictionary.NEW);
    setInputs({ wardId:''
                ,upwardId:''
                ,fireOfficeId:''
                ,wardName:''
                ,typeClsCd:''
                ,typeClsName:''
                ,nickName:''
                ,zipCode:''
                ,address:''
                ,dspGroupYn:false
                ,autoListYn:false
                ,dayTel:''
                ,nightTel:''
                ,extTel:''
                ,dayFax:''
                ,nightFax:''
                ,wardopenDate:''
                ,wardcloseDate:''
                ,wardopenYn:true
                ,brdUsingYn:false
                ,remark:''
                ,wardOrder:''
                ,x:''
                ,y:''
                ,useYn:''
            });
    props.callFromDialog({modified: false});
    originalInputs = {
                wardId:''
                ,upwardId:''
                ,fireOfficeId:''
                ,wardName:''
                ,typeClsCd:''
                ,typeClsName:''
                ,nickName:''
                ,zipCode:''
                ,address:''
                ,dspGroupYn:false
                ,autoListYn:false
                ,dayTel:''
                ,nightTel:''
                ,extTel:''
                ,dayFax:''
                ,nightFax:''
                ,wardopenDate:''
                ,wardcloseDate:''
                ,wardopenYn:true
                ,brdUsingYn:false
                ,remark:''
                ,wardOrder:''
                ,x:''
                ,y:''
                ,useYn:''
                  };
    isModified = false;
  }

  const onUpdateMode = () => {
    setDisabled(false);
    setOperateType(dictionary.UPDATE);
    props.callFromDialog({modified: false});
  }

  const onConfirm = () => {
    if (operateType === dictionary.NEW) {
      setConfirmMessage({ isConfirmOpen: true, messageType: dictionary.WARNING, messageId : "msg_will_you_insert" });
    } else if (operateType === dictionary.UPDATE) {
      setConfirmMessage({ isConfirmOpen: true, messageType: dictionary.WARNING, messageId : "msg_will_you_update" });
    } else {
      onNewMode();
    }
  }

  const onDelete = () => {
    setOperateType(dictionary.DELETE);
    setConfirmMessage({ isConfirmOpen: true, messageType: dictionary.WARNING, messageId : "msg_will_you_delete" });
  }

  const openGroupCodeTreeView = () => {
    setIsDialogOpen(true);
  }

/////////////////Check whether inputs are modified or not. If modified, it sends the state to parent//////////////////// 
  useEffect(() => {
    let Modified =  (JSON.stringify(inputs) === JSON.stringify(originalInputs)? false : true);
    
    if (operateType !== dictionary.READ) props.callFromDialog({modified: Modified && isModified});
  }, [inputs]);
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//////Dialog event
const [isDialogOpen, setIsDialogOpen] = useState(false);

const handleDialogClose = () => {
  setIsDialogOpen(false);
};

const callFromDialog = (p) => {
  setInputs({
    ...inputs,
    typeClsCd:p.cd,
    typeClsName:p.cdName
  });
  setIsDialogOpen(false);
}

const sendInfo = {groupCode:'211'};
////////////////////////////////////////////////////////

/////////////////////Get data////////////////////////////////////////////
  useEffect(() => {
    let completed = false; 
    
    async function getInfo(id) {
      let body = {id: id,locale:userContext.preference.language};
      setLoading(true);
      RunService(s.getWardInfo, body)
          .then(response => {
            if (!completed) { 
              let reponseData = ResetForResponseData(s.getWardInfo.response, response.data);
              setInputs(reponseData);
              originalInputs = reponseData;
            }
            completed= true;
            onReadMode();
            setLoading(false);
          })
          .catch(error => {
            setSnackbar(prev => ({
              ...prev,
              openSnackbar:true,
              severity:dictionary.MessageType.Error,
              message:GetText("msg_normal_error_message")
            }));
          })
    }

    if (operateType === dictionary.READ && props.sendInfo.id) getInfo(props.sendInfo.id);
    
    return () => {
      completed = true;
    };
  }, []);
  /////////////////////////////////////////////////////////////////

  ///////////////////// Insert or Update///////////////////////////
  const onSubmit = () => {
    if (operateType === "") {
      onNewMode();
      return;
    }

    setLoading(true);
    let service;
    let body;

    switch(operateType) {
      case dictionary.NEW :
        service = s.insertWard;
        body = inputs;
        break;
      case dictionary.UPDATE :
        service = s.updateWard;
        body = inputs;
        break;
      case dictionary.DELETE :
        service = s.deleteWard;
        body = [inputs.wardId]; 
        break;
      default :
        return;
    }
  
    RunService(service,{userId: userContext.userInfo.userId} ,body)
        .then(response => {
          setLoading(false);
          var result = CheckResultFromService(response.data);
          if (result.isSuccess) {
            setSnackbar(prev => ({
              ...prev,
              openSnackbar:true,
              severity:dictionary.MessageType.Success,
              message:GetText(result.message)
            }));
            
            setInputs({
              ...inputs,
              wardId:result.id
            });
            onReadMode();
            
            if (operateType === dictionary.DELETE) {
              props.callFromDialog({modified: false, refresh:true});
              props.handleDialogClose(true);
            } else {
              props.callFromDialog({modified: false, refresh:true});
            }

          } else {
            setSnackbar(prev => ({
              ...prev,
              openSnackbar:true,
              severity:dictionary.MessageType.Error,
              message:GetText(result.message)
            }));
          }
        })
        .catch(error => {
          setLoading(false);
          setSnackbar(prev => ({
            ...prev,
            openSnackbar:true,
            severity:dictionary.MessageType.Error,
            message:GetText("msg_normal_error_message")
          }));
        })
  } 
  /////////////////////////////////////////////////////////////////

  return (
    <React.Fragment>
      <form onSubmit={handleSubmit(onConfirm)}>
      <Paper className={classes.control}>
        <Grid container spacing={1}>
          <Grid item xs={12}>
            <div className={classes.paper}>
            <TextFieldWithIcon 
                id="typeClsName"
                label={GetText("type_class_cd")}
                onChange={onChange}
                disabled={disabled}
                value={typeClsName}
                iconEvent={openGroupCodeTreeView}
                icon={<FindInPageIcon />}
                required={true}
              />
              <UpperAndOfficeSelectByTypeClsCd 
                upperWardId="upwardId"
                upperWardOnly={true}
                typeClsCd={typeClsCd}
                callEvent={onChange} 
                selectedUpperWardValue={upwardId}
                disabled={disabled}
              /> 
              <CustomizedTextField
                label={GetText("ward_name")}
                id="wardName"
                name="wardName"
                onChange={onChange}
                disabled={disabled}
                value={wardName}
              />    
              <CustomizedTextField
                label={GetText("abbreviation")}
                id="nickName"
                name="nickName"
                onChange={onChange}
                disabled={disabled}
                value={nickName}
              />          
            </div>
          </Grid>
          <Grid item xs={12}>
            <div className={classes.paper}>
              <CustomizedTextField
                label={GetText("day_tel_number")}
                id="dayTel"
                onChange={onChange}
                disabled={disabled}
                value={dayTel}
              />
              <CustomizedTextField
                label={GetText("night_tel_number")}
                id="nightTel"
                onChange={onChange}
                disabled={disabled}
                value={nightTel}
              />
              <CustomizedTextField
                label={GetText("ext_num")}
                id="extTel"
                type="number"
                length="4"
                onChange={onChange}
                disabled={disabled}
                value={extTel}
              />
              <CustomizedTextField
                label={GetText("day_fax")}
                id="dayFax"
                onChange={onChange}
                disabled={disabled}
                value={dayFax}
              />          
              <CustomizedTextField
                label={GetText("night_fax")}
                id="nightFax"
                onChange={onChange}
                disabled={disabled}
                value={nightFax}
              />   
              
            </div>
          </Grid>
          <Grid item xs={12}>
            <div className={classes.paper}>
              <Address 
                addressId={'zipCode'} 
                etcAddressid={'address'} 
                changeEvent={onChange}
                disabled={disabled}
                postalCodeValue={zipCode}
                etcAddressValue={address}
              />
            </div>
          </Grid>
          <Grid item xs={12}>
            <div className={classes.paper}>
              <CustomizedSwitch 
                id='brdUsingYn'
                name='brdUsingYn' 
                checked={brdUsingYn}
                onChange={onChange}
                disabled={disabled}
                value={brdUsingYn}
                label={GetText("is_use_broadcast")} 
              />
              <CustomizedSwitch 
                id='dspGroupYn'
                name='dspGroupYn' 
                checked={dspGroupYn}
                onChange={onChange}
                disabled={disabled}
                value={dspGroupYn}
                label={GetText("is_mobilized_team_organized")} 
              />
              <CustomizedSwitch 
                id='autoListYn'
                name='autoListYn' 
                checked={autoListYn}
                onChange={onChange}
                disabled={disabled}
                value={autoListYn}
                label={GetText("team_organized_automatically")} 
              />
              <CustomizedSwitch 
                id='wardopenYn'
                name='wardopenYn' 
                checked={wardopenYn}
                onChange={onChange}
                disabled={disabled}
                value={wardopenYn}
                label={GetText("ward_opened")} 
              />
              <CustomizedTextField
                label={GetText("ward_order")}
                id="wardOrder"
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={wardOrder}
              />
            </div>
          </Grid>
          <Grid item xs={12}>
            <div className={classes.paper}>
              <CustomizedTextField
                id="wardopenDate"
                label={GetText("ward_open_date")}
                type="date"
                onChange={onChange}
                disabled={disabled}
                value={wardopenDate}
              />
              <CustomizedTextField
                id="wardcloseDate"
                label={GetText("ward_close_date")}
                type="date"
                onChange={onChange}
                disabled={disabled}
                value={wardcloseDate}
              />
              <CustomizedTextField
                label={GetText("latitude")}
                id="x"
                onChange={onChange}
                disabled={disabled}
                value={x}
              />  
              <CustomizedTextField
                label={GetText("longitude")}
                id="y"
                onChange={onChange}
                disabled={disabled}
                value={y}
              />  
            </div>
          </Grid>
          <Grid item xs={12}>
            <CustomizedTextField
              id="remark"
              label={GetText("remark")}
              className={classes.textFieldFullLength}
              multiline={true}
              rows={4}
              variant="outlined"
              onChange={onChange}
              disabled={disabled}
              value={remark}
            />
          </Grid>
        </Grid>
      </Paper>   
      
      <Box p={1}></Box>
      <Divider style={{'width':'100%','position': 'absolute', 'left': 0}} />
      <ActionPanel>
        <Button 
          disabled={loading}
          color={userContext.preference.color}
          type="submit"
        >
          { operateType !== "" ? GetText("save") : GetText("new") }
        </Button>
        { 
          inputs.wardId && operateType !== dictionary.NEW &&
            <Button 
              disabled={loading}
              color={userContext.preference.color}
              name="update"
              onClick={onChangeUpdateState}
            >
              { isUpdateMode ? GetText("cancel") : GetText("update") }
            </Button>
        }
        
        {
          inputs.wardId && 
            <Button 
              disabled={loading}
              color={userContext.preference.color}
              name="delete"
              onClick={onDelete}
          >
              {GetText("delete")}
            </Button> 
        }
        
        {loading && <CircularProgress size={24} className={classes.buttonProgress} />}
      </ActionPanel>
      </form>
      <CustomizedSnackbar isOpen={openSnackbar} severity={severity} message={message} closeHandler={onCloseSnackbar} />
      <Confirm open={isConfirmOpen} MessageType={messageType} MessageId={messageId} handleClose={handleConfirmDialog} />
      <CustomizedDialog id={'groupCodeViewDialog'} isDialogOpen={isDialogOpen} sendInfo={sendInfo} viewInfo={viewInfo.groupCodeTreeView} callFromDialog={callFromDialog} handleDialogClose={handleDialogClose} />
    </React.Fragment>
  );
}