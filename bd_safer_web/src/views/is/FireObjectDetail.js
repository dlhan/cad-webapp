import React, { useState, useEffect } from 'react';
import { GetText } from '../../common/BundleManager';
import serviceList from '../../service/ServiceList';
import { RunService } from '../../service/RestApi';
import Dictionary from '../../common/Dictionary';
import { CheckResultFromService, ResetForResponseData } from '../../common/Utility';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import Address from '../../components/Address';
import ActionPanel from '../../components/ActionPanel';
import { FormStyle } from '../../style/Style';
import CustomizedSwitch from '../../components/Switch';
import { useUserContext } from '../../common/UserContext';
import CustomizedSnackbar from '../../components/Snackbar';
import CircularProgress from '@material-ui/core/CircularProgress';
import FormLabel from '@material-ui/core/FormLabel';
import CodeComboBox from '../../components/CodeComboBox';
import ViewInfo from '../../common/ViewInfo';
import { useForm } from "react-hook-form";
import Confirm from '../../components/Confirm';
import UpwardAndWardCombo from '../../components/UpwardAndWardCombo';
import CustomizedTextField from '../../components/TextField';

const useStyles = FormStyle;
let originalInputs = {};
let isModified = false;

export default function FireObjectDetail(props) {
  const classes = useStyles();
  var dictionary = new Dictionary();
  const userContext = useUserContext();
  const s = new serviceList();
  var viewInfo = new ViewInfo();

  const [operateType, setOperateType] = useState(props.sendInfo.operateType);
  let [inputs, setInputs] = useState({objId:''
                                    ,upwardId:''
                                    ,useYn:true
                                    ,wardId:''
                                    ,objManNum:''
                                    ,usedCd:''
                                    ,objCd:''
                                    ,constName:''
                                    ,rpsnFirmName:''
                                    ,zipCode:''
                                    ,address:''
                                    ,objStdCd:''
                                    ,bldgNm:''
                                    ,x:''
                                    ,y:''
                                    ,weakobjYn:false
                                    ,selfobjYn:false
                                    ,adgInfo:''
                                    ,dayTel:''
                                    ,nightTel:''
                                    ,prevdstTel:''
                                    ,useConfmDate:''
                                    ,institYn:false
                                    ,institClsCd:''
                                    ,pubfaclYn:false
                                    ,weakobjCd:''
                                    ,subUseDesc:''
                                    ,tunnelClsCd:''
                                    ,tunnelLength:''
                                    });
  let { objId,useYn,upwardId,wardId,objManNum,usedCd,objCd,constName,rpsnFirmName,zipCode,address,objStdCd,bldgNm,x,y,weakobjYn,selfobjYn,adgInfo,dayTel,nightTel,prevdstTel,useConfmDate,institYn,institClsCd,pubfaclYn,weakobjCd,subUseDesc,tunnelClsCd,tunnelLength} = inputs;
  let [loading, setLoading] = useState(false);
  let [disabled, setDisabled] = useState(false);
  let [isUpdateMode, setIsUpdateMode] = useState(false);
  
//////////Upward select event/////////////////////////////////////////
  const callEvent = (e) => {
    setInputs({
      ...inputs,
      upperWardId:e[Object.keys(e)[0]]
    });
  }
///////////////////////////////////////////////////
  
/////////Setting for validate the inputs in the form.
  const { handleSubmit } = useForm();
////////////////////////////////////////////////////

  const onChange = e => {
    let {name, value} = e.target;
    
    if (name === "weakobjYn") { value = !inputs.weakobjYn};
    if (name === "selfobjYn") { value = !inputs.selfobjYn};
    if (name === "institYn") { value = !inputs.institYn};
    if (name === "pubfaclYn") { value = !inputs.pubfaclYn};
    
    isModified = true;
    setInputs({
      ...inputs,
      [name]:value
    });
  }

/////////////Setting for Snackbar////////////////////////////////
  let [snackbar, setSnackbar] = useState({openSnackbar:false, severity:'', message:''});
  let { openSnackbar, severity, message } = snackbar;
    
  const onCloseSnackbar = () => {
    setSnackbar(prev => ({
      ...prev,
      openSnackbar:false,
      severity:'',
      message:''
    }));
  }
/////////////////////////////////////////////

//////Confrim event
  const [confirmMessage, setConfirmMessage] = React.useState({
                                                      isConfirmOpen : false,
                                                      messageType : "",
                                                      messageId : ""
                                                });
  let { isConfirmOpen,messageType,messageId } = confirmMessage;
  
  const handleConfirmDialog = (answer) => {
    setConfirmMessage({
      ...confirmMessage,
      isConfirmOpen:false
    });
  
    if (answer){
      onSubmit();
    }
  }
  /////////////////////////////////////////////
  
  ////Button operate
  
  const onChangeUpdateState = () => {
    setIsUpdateMode(!isUpdateMode);
    if (!isUpdateMode) {
      onUpdateMode();
    } else {
      onReadMode();
    }
  }

  const onReadMode = () => {
    setDisabled(true);
    setOperateType("");
    setIsUpdateMode(false);
    props.callFromDialog({modified: false});
  }

  const onNewMode = () => {
    setDisabled(false);
    setOperateType(dictionary.NEW);
    setInputs({ objId:''
              ,upwardId:''
              ,useYn:true
              ,wardId:''
              ,objManNum:''
              ,usedCd:''
              ,objCd:''
              ,constName:''
              ,rpsnFirmName:''
              ,zipCode:''
              ,address:''
              ,objStdCd:''
              ,bldgNm:''
              ,x:''
              ,y:''
              ,weakobjYn:false
              ,selfobjYn:false
              ,adgInfo:''
              ,dayTel:''
              ,nightTel:''
              ,prevdstTel:''
              ,useConfmDate:''
              ,institYn:false
              ,institClsCd:''
              ,pubfaclYn:false
              ,weakobjCd:''
              ,subUseDesc:''
              ,tunnelClsCd:''
              ,tunnelLength:''
            });
    props.callFromDialog({operateType:dictionary.NEW, modified: false});
    originalInputs = {
                objId:''
                ,upwardId:''
                ,useYn:true
                ,wardId:''
                ,objManNum:''
                ,usedCd:''
                ,objCd:''
                ,constName:''
                ,rpsnFirmName:''
                ,zipCode:''
                ,address:''
                ,objStdCd:''
                ,bldgNm:''
                ,x:''
                ,y:''
                ,weakobjYn:false
                ,selfobjYn:false
                ,adgInfo:''
                ,dayTel:''
                ,nightTel:''
                ,prevdstTel:''
                ,useConfmDate:''
                ,institYn:false
                ,institClsCd:''
                ,pubfaclYn:false
                ,weakobjCd:''
                ,subUseDesc:''
                ,tunnelClsCd:''
                ,tunnelLength:''
                  };
    isModified = false;
  }

  const onUpdateMode = () => {
    setDisabled(false);
    setOperateType(dictionary.UPDATE);
    props.callFromDialog({operateType:dictionary.UPDATE, modified: false});
  }

  const onConfirm = () => {
    if (operateType === dictionary.NEW) {
      setConfirmMessage({ isConfirmOpen: true, messageType: dictionary.WARNING, messageId : "msg_will_you_insert" });
    } else if (operateType === dictionary.UPDATE) {
      setConfirmMessage({ isConfirmOpen: true, messageType: dictionary.WARNING, messageId : "msg_will_you_update" });
    } else {
      onNewMode();
    }
  }

  const onDelete = () => {
    setOperateType(dictionary.DELETE);
    setConfirmMessage({ isConfirmOpen: true, messageType: dictionary.WARNING, messageId : "msg_will_you_delete" });
  }
/////////////////Check whether inputs are modified or not. If modified, it sends the state to parent//////////////////// 
  useEffect(() => {
    let Modified =  (JSON.stringify(inputs) === JSON.stringify(originalInputs)? false : true);
    
    if (operateType !== dictionary.READ) props.callFromDialog({modified: Modified && isModified});
  }, [inputs]);
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////Get data////////////////////////////////////////////
  useEffect(() => {
    let completed = false; 
    
    async function getInfo(id) {
      let body = {id: id};
      setLoading(true);
      RunService(s.getObjectInfo, body)
          .then(response => {
            if (!completed) { 
              let reponseData = ResetForResponseData(s.getObjectInfo.response, response.data);
              setInputs(reponseData);
              originalInputs = reponseData;
            }
            completed= true;
            onReadMode();
            setLoading(false);
          })
          .catch(error => {
            setSnackbar(prev => ({
              ...prev,
              openSnackbar:true,
              severity:dictionary.MessageType.Error,
              message:GetText("msg_normal_error_message")
            }));
          })
    }

    if (props.objId) getInfo(props.objId);
    
    return () => {
      completed = true;
    };
  }, []);
  /////////////////////////////////////////////////////////////////

  ///////////////////// Insert or Update///////////////////////////
  const onSubmit = () => {
    if (operateType === "") {
      onNewMode();
      return;
    }

    setLoading(true);
    let service;
    let body;

    switch(operateType) {
      case dictionary.NEW :
        service = s.insertObject;
        body = inputs;
        break;
      case dictionary.UPDATE :
        service = s.updateObject;
        body = inputs;
        break;
      case dictionary.DELETE :
        service = s.deleteObject;
        body = [inputs.objId]; 
        break;
      default :
        return;
    }
  
    RunService(service,{userId: userContext.userInfo.userId} ,body)
        .then(response => {
          setLoading(false);
          var result = CheckResultFromService(response.data);
          if (result.isSuccess) {
            setSnackbar(prev => ({
              ...prev,
              openSnackbar:true,
              severity:dictionary.MessageType.Success,
              message:GetText(result.message)
            }));
            
            setInputs({
              ...inputs,
              objId:result.id
            });
            onReadMode();
            
            if (operateType === dictionary.DELETE) {
              props.callFromDialog({operateType:dictionary.DELETE, modified: false, refresh:true});
              props.handleDialogClose(true);
            } else {
              props.callFromDialog({operateType:dictionary.READ, id:result.id, modified: false, refresh:true});
            }
          } else {
            setSnackbar(prev => ({
              ...prev,
              openSnackbar:true,
              severity:dictionary.MessageType.Error,
              message:GetText(result.message)
            }));
          }
        })
        .catch(error => {
          setLoading(false);
          setSnackbar(prev => ({
            ...prev,
            openSnackbar:true,
            severity:dictionary.MessageType.Error,
            message:GetText("msg_normal_error_message")
          }));
        })
  } 
  /////////////////////////////////////////////////////////////////

  return (
    <React.Fragment>
      <form onSubmit={handleSubmit(onConfirm)}>
      <div className={classes.paper}>
        <FormLabel component="legend">{GetText("basic_information")}</FormLabel>
      </div>
      <Paper className={classes.control}>
        <Grid container spacing={1}>
          <Grid item xs={12}>
            <div className={classes.paper}>
              <UpwardAndWardCombo 
                className={classes.textField25} 
                callEvent={onChange} 
                disabled={disabled}
                upperWardId="upwardId"
                wardid="wardId"
                typeClsCds={[2,3,4,5,6]}
                selectedUpperWardValue={upwardId}
                selectedWardValue={wardId}
              />
              <CustomizedTextField
                label={GetText("object_name")}
                id="constName"
                name="constName"
                onChange={onChange}
                disabled={disabled}
                value={constName}
              />    
              <CustomizedTextField
                label={GetText("object_manage_number")}
                id="objManNum"
                onChange={onChange}
                disabled={disabled}
                value={objManNum}
              />          
              <CustomizedTextField
                label={GetText("representative_name")}
                id="rpsnFirmName"
                onChange={onChange}
                disabled={disabled}
                value={rpsnFirmName}
              />
              <CustomizedTextField
                label={GetText("building_name")}
                id="bldgNm"
                onChange={onChange}
                disabled={disabled}
                value={bldgNm}
              />
            </div>
          </Grid>
          <Grid item xs={12}>
            <div className={classes.paper}>
              <CodeComboBox
                groupCode={'020070000'}
                disabled={disabled}
                className={classes.textField25}
                childLabel={GetText("object_classification")}
                childComboId={'objCd'}
                callEvent={onChange}
                childOnly={true}
                selectedCode={objCd}
              />
              <CodeComboBox
                groupCode={'020090000'}
                disabled={disabled}
                className={classes.textField25}
                childLabel={GetText("object_standard_type")}
                childComboId={'objStdCd'}
                callEvent={onChange}
                childOnly={true}
                selectedCode={objStdCd}
              />
              <CodeComboBox
                groupCode={'020010000'}
                disabled={disabled}
                className={classes.textField25}
                childLabel={GetText("building_main_used")}
                childComboId={'usedCd'}
                callEvent={onChange}
                childOnly={true}
                selectedCode={usedCd}
              />
              <CustomizedTextField
                label={GetText("subuse_detail")}
                id="subUseDesc"
                onChange={onChange}
                disabled={disabled}
                value={subUseDesc}
              />
              <CustomizedTextField
                label={GetText("day_tel_number")}
                id="dayTel"
                onChange={onChange}
                disabled={disabled}
                value={dayTel}
              />
              <CustomizedTextField
                label={GetText("night_tel_number")}
                id="nightTel"
                onChange={onChange}
                disabled={disabled}
                value={nightTel}
              />
            </div>
          </Grid>
          <Grid item xs={12}>
            <div className={classes.paper}>
              <CustomizedTextField
                label={GetText("disaster_prevention_tel")}
                id="prevdstTel"
                type="tel"
                onChange={onChange}
                disabled={disabled}
                value={prevdstTel}
              />
              <CustomizedTextField
                label={GetText("approval_use_date")}
                id="useConfmDate"
                type="date"
                onChange={onChange}
                disabled={disabled}
                value={useConfmDate}
              />
              <CodeComboBox
                groupCode={'020030000'}
                disabled={disabled}
                childLabel={GetText("tunnel_type")}
                className={classes.textField25}
                childComboId={'tunnelClsCd'}
                callEvent={onChange}
                childOnly={true}
                selectedCode={tunnelClsCd}
              />
              <CustomizedTextField
                label={GetText("tunnel_length")}
                id="tunnelLength"
                onChange={onChange}
                disabled={disabled}
                value={tunnelLength}
              />
            </div>
          </Grid>
          <Grid item xs={12}>
            <div className={classes.paper}>
              <CustomizedSwitch 
                id='selfobjYn'
                name='selfobjYn' 
                checked={selfobjYn}
                onChange={onChange}
                disabled={disabled}
                value={selfobjYn}
                label={GetText("is_self_check")} 
              />
              <CustomizedSwitch 
                id='pubfaclYn'
                name='pubfaclYn' 
                checked={pubfaclYn}
                onChange={onChange}
                disabled={disabled}
                value={pubfaclYn}
                label={GetText("is_public_institution")} 
              />
              <CustomizedSwitch 
                id='weakobjYn'
                name='weakobjYn' 
                className={classes.textField10}
                checked={weakobjYn}
                onChange={onChange}
                disabled={disabled}
                value={weakobjYn}
                label={GetText("is_vulnerability_of_large_fire")} 
              />
              <CodeComboBox
                groupCode={'020030000'}
                disabled={disabled}
                childComboId={'weakobjCd'}
                callEvent={onChange}
                childOnly={true}
                selectedCode={weakobjCd}
              />
              <CustomizedSwitch 
                id='institYn'
                name='institYn' 
                className={classes.textField10}
                checked={institYn}
                onChange={onChange}
                disabled={disabled}
                value={institYn}
                label={GetText("is_public_institution")} 
              />
              <CodeComboBox
                groupCode={'020030000'}
                disabled={disabled}
                className={classes.textField25}
                childComboId={'institClsCd'}
                callEvent={onChange}
                childOnly={true}
                selectedCode={institClsCd}
              />
            </div>
          </Grid>
        </Grid>
      </Paper>   
      <div className={classes.paper}>
        <FormLabel component="legend">{GetText("location_info")}</FormLabel>
      </div>
      <Paper className={classes.control}>
        <Grid container spacing={1}>
          <Grid item xs={12}>
            <div className={classes.paper}>
              <Address 
                addressId={'zipCode'} 
                etcAddressid={'address'} 
                changeEvent={onChange}
                disabled={disabled}
                postalCodeValue={zipCode}
                etcAddressValue={address}
              />
              <CustomizedTextField
                label={GetText("latitude")}
                id="x"
                onChange={onChange}
                disabled={disabled}
                value={x}
              />
              <CustomizedTextField
                label={GetText("longitude")}
                id="y"
                onChange={onChange}
                disabled={disabled}
                value={y}
              />
            </div>
          </Grid>
          <Grid item xs={12}>
            <div className={classes.paper}>
              <CustomizedTextField
                id="adgInfo"
                label={GetText("nearby_information")}
                className={classes.textFieldFullLength}
                multiline={true}
                rows={2}
                variant="outlined"
                onChange={onChange}
                disabled={disabled}
                value={adgInfo}
              />
            </div>
          </Grid>
        </Grid>
      </Paper>
      
      <Box p={1}></Box>
      <Divider style={{'width':'100%','position': 'absolute', 'left': 0}} />
      <ActionPanel>
        <Button 
          disabled={loading}
          color={userContext.preference.color}
          type="submit"
        >
          { operateType !== "" ? GetText("save") : GetText("new") }
        </Button>
        { 
          inputs.objId && operateType !== dictionary.NEW &&
            <Button 
              disabled={loading}
              color={userContext.preference.color}
              name="update"
              onClick={onChangeUpdateState}
            >
              { isUpdateMode ? GetText("cancel") : GetText("update") }
            </Button>
        }
        
        {
          inputs.objId && 
            <Button 
              disabled={loading}
              color={userContext.preference.color}
              name="delete"
              onClick={onDelete}
          >
              {GetText("delete")}
            </Button> 
        }
        
        {loading && <CircularProgress size={24} className={classes.buttonProgress} />}
      </ActionPanel>
      </form>
      <CustomizedSnackbar isOpen={openSnackbar} severity={severity} message={message} closeHandler={onCloseSnackbar} />
      <Confirm open={isConfirmOpen} MessageType={messageType} MessageId={messageId} handleClose={handleConfirmDialog} />
    </React.Fragment>
  );
}