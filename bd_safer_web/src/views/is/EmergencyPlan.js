import React, { useState, useEffect } from 'react';
import { GetText } from '../../common/BundleManager';
import serviceList from '../../service/ServiceList';
import { RunService } from '../../service/RestApi';
import Dictionary from '../../common/Dictionary';
import { CheckResultFromService, ResetForResponseData } from '../../common/Utility';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import ActionPanel from '../../components/ActionPanel';
import { FormStyle } from '../../style/Style';
import { useUserContext } from '../../common/UserContext';
import CustomizedSnackbar from '../../components/Snackbar';
import CircularProgress from '@material-ui/core/CircularProgress';
import ViewInfo from '../../common/ViewInfo';
import { useForm } from "react-hook-form";
import Confirm from '../../components/Confirm';
import CustomizedTextField from '../../components/TextField';
import FormLabel from '@material-ui/core/FormLabel';

const useStyles = FormStyle;
let originalInputs = {};
let isModified = false;

export default function EmergencyPlan(props) {
  const classes = useStyles();
  var dictionary = new Dictionary();
  const userContext = useUserContext();
  const s = new serviceList();
  var viewInfo = new ViewInfo();

  const [operateType, setOperateType] = useState(props.sendInfo.operateType);
  let [inputs, setInputs] = useState({objId:'',
                                      objPlanId:'',
                                      envDesc:'',
                                      expandRsn:'',
                                      rescEscCplan:'',
                                      fmngIssue:'',
                                      selfvolunOrg:'',
                                      realestExptAmt:'',
                                      mvestExptAmt:'',
                                      totAmt:'',
                                      ward1Dist:'',
                                      ward2Dist:'',
                                      escPlace:'',
                                      trainingGuideDesc:'',
                                      etcRemark:'',
                                      useYn:''
                                    });
  let { envDesc,expandRsn,rescEscCplan,fmngIssue,selfvolunOrg,realestExptAmt,mvestExptAmt,totAmt,ward1Dist,ward2Dist,escPlace,trainingGuideDesc,etcRemark } = inputs;
  let [loading, setLoading] = useState(false);
  let [disabled, setDisabled] = useState(false);
  let [isUpdateMode, setIsUpdateMode] = useState(false);
  
/////////Setting for validate the inputs in the form.
  const { handleSubmit } = useForm();
////////////////////////////////////////////////////

  const onChange = e => {
    let {name, value} = e.target;
       
    isModified = true;
    setInputs({
      ...inputs,
      [name]:value
    });
  }

/////////////Setting for Snackbar////////////////////////////////
  let [snackbar, setSnackbar] = useState({openSnackbar:false, severity:'', message:''});
  let { openSnackbar, severity, message } = snackbar;
    
  const onCloseSnackbar = () => {
    setSnackbar(prev => ({
      ...prev,
      openSnackbar:false,
      severity:'',
      message:''
    }));
  }
/////////////////////////////////////////////

//////Confrim event
  const [confirmMessage, setConfirmMessage] = React.useState({
                                                      isConfirmOpen : false,
                                                      messageType : "",
                                                      messageId : ""
                                                });
  let { isConfirmOpen,messageType,messageId } = confirmMessage;
  
  const handleConfirmDialog = (answer) => {
    setConfirmMessage({
      ...confirmMessage,
      isConfirmOpen:false
    });
  
    if (answer){
      onSubmit();
    }
  }
  /////////////////////////////////////////////
  
  ////Button operate
  
  const onChangeUpdateState = () => {
    setIsUpdateMode(!isUpdateMode);
    if (!isUpdateMode) {
      onUpdateMode();
    } else {
      onReadMode();
    }
  }

  const onReadMode = () => {
    setDisabled(true);
    setOperateType("");
    setIsUpdateMode(false);
    props.callFromDialog({modified: false});
  }

  const onNewMode = () => {
    setDisabled(false);
    setOperateType(dictionary.NEW);
    setInputs({ objId:'',
                objPlanId:'',
                envDesc:'',
                expandRsn:'',
                rescEscCplan:'',
                fmngIssue:'',
                selfvolunOrg:'',
                realestExptAmt:'',
                mvestExptAmt:'',
                totAmt:'',
                ward1Dist:'',
                ward2Dist:'',
                escPlace:'',
                trainingGuideDesc:'',
                etcRemark:'',
                useYn:''
            });
    props.callFromDialog({modified: false});
    originalInputs = {
                objId:'',
                objPlanId:'',
                envDesc:'',
                expandRsn:'',
                rescEscCplan:'',
                fmngIssue:'',
                selfvolunOrg:'',
                realestExptAmt:'',
                mvestExptAmt:'',
                totAmt:'',
                ward1Dist:'',
                ward2Dist:'',
                escPlace:'',
                trainingGuideDesc:'',
                etcRemark:'',
                useYn:''
            };
    isModified = false;
  }

  const onUpdateMode = () => {
    setDisabled(false);
    setOperateType(dictionary.UPDATE);
    props.callFromDialog({modified: false});
  }

  const onConfirm = () => {
    if (operateType === dictionary.NEW) {
      setConfirmMessage({ isConfirmOpen: true, messageType: dictionary.WARNING, messageId : "msg_will_you_insert" });
    } else if (operateType === dictionary.UPDATE) {
      setConfirmMessage({ isConfirmOpen: true, messageType: dictionary.WARNING, messageId : "msg_will_you_update" });
    } else {
      onNewMode();
    }
  }

  const onDelete = () => {
    setOperateType(dictionary.DELETE);
    setConfirmMessage({ isConfirmOpen: true, messageType: dictionary.WARNING, messageId : "msg_will_you_delete" });
  }

/////////////////Check whether inputs are modified or not. If modified, it sends the state to parent//////////////////// 
  useEffect(() => {
    let Modified =  (JSON.stringify(inputs) === JSON.stringify(originalInputs)? false : true);
    
    if (operateType !== dictionary.READ) props.callFromDialog({modified: Modified && isModified});
  }, [inputs]);
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////Get data////////////////////////////////////////////
  useEffect(() => {
    let completed = false; 
    
    async function getInfo(id) {
      let body = {objId: id};
      setLoading(true);
      RunService(s.getObjPlanInfo, body)
          .then(response => {
            if (!completed) { 
              if (response.data) {
                let reponseData = ResetForResponseData(s.getObjPlanInfo.response, response.data);
                setInputs(reponseData);
                originalInputs = reponseData;
                onReadMode();
              } else {
                onNewMode();
              }
            }
            completed= true;
            setLoading(false);
          })
          .catch(error => {
            setSnackbar(prev => ({
              ...prev,
              openSnackbar:true,
              severity:dictionary.MessageType.Error,
              message:GetText("msg_normal_error_message")
            }));
          })
    }

    if (operateType === dictionary.READ && props.objId) getInfo(props.objId);
    
    return () => {
      completed = true;
    };
  }, []);
  /////////////////////////////////////////////////////////////////

  ///////////////////// Insert or Update///////////////////////////
  const onSubmit = () => {
    if (operateType === "") {
      onNewMode();
      return;
    }

    setLoading(true);
    let service;
    let body;

    switch(operateType) {
      case dictionary.NEW :
        service = s.insertObjPlan;
        body = inputs;
        body.objId = props.objId;
        break;
      case dictionary.UPDATE :
        service = s.updateObjPlan;
        body = inputs;
        break;
      case dictionary.DELETE :
        service = s.deleteObjPlan;
        body = [inputs.objId]; 
        break;
      default :
        return;
    }
  
    RunService(service,{userId: userContext.userInfo.userId} ,body)
        .then(response => {
          setLoading(false);
          var result = CheckResultFromService(response.data);
          if (result.isSuccess) {
            setSnackbar(prev => ({
              ...prev,
              openSnackbar:true,
              severity:dictionary.MessageType.Success,
              message:GetText(result.message)
            }));
            
            setInputs({
              ...inputs
            });
            onReadMode();
          } else {
            setSnackbar(prev => ({
              ...prev,
              openSnackbar:true,
              severity:dictionary.MessageType.Error,
              message:GetText(result.message)
            }));
          }
        })
        .catch(error => {
          setLoading(false);
          setSnackbar(prev => ({
            ...prev,
            openSnackbar:true,
            severity:dictionary.MessageType.Error,
            message:GetText("msg_normal_error_message")
          }));
        })
  } 
  /////////////////////////////////////////////////////////////////

  return (
    <React.Fragment>
      <form onSubmit={handleSubmit(onConfirm)}>
      <div className={classes.paper}>
        <FormLabel component="legend">{GetText("emg_rescue_plan_basic_info")}</FormLabel>
      </div>
      <Paper className={classes.control}>
        <Grid container spacing={1}>
          <Grid item xs={12}>
            <div className={classes.paper}>
              <CustomizedTextField
                id="envDesc"
                label={GetText("surrounding_environment")}
                className={classes.textField3}
                multiline={true}
                rows={2}
                variant="outlined"
                onChange={onChange}
                disabled={disabled}
                value={envDesc}
              />
              <CustomizedTextField
                id="expandRsn"
                label={GetText("vulnerability_and_combust_factors")}
                className={classes.textField3}
                multiline={true}
                rows={2}
                variant="outlined"
                onChange={onChange}
                disabled={disabled}
                value={expandRsn}
              />         
            </div>
          </Grid>
          <Grid item xs={12}>
            <div className={classes.paper}>
            <CustomizedTextField
                id="rescEscCplan"
                label={GetText("lifesaving_or_evacuation_measure")}
                className={classes.textField3}
                multiline={true}
                rows={2}
                variant="outlined"
                onChange={onChange}
                disabled={disabled}
                value={rescEscCplan}
              />
              <CustomizedTextField
                id="fmngIssue"
                label={GetText("fire_control_problem")}
                className={classes.textField3}
                multiline={true}
                rows={2}
                variant="outlined"
                onChange={onChange}
                disabled={disabled}
                value={fmngIssue}
              />  
            </div>
          </Grid>
          <Grid item xs={12}>
            <div className={classes.paper}>
              <CustomizedTextField
                id="selfvolunOrg"
                label={GetText("self_defense_reorg_status")}
                className={classes.textFieldFullLength}
                multiline={true}
                rows={2}
                variant="outlined"
                onChange={onChange}
                disabled={disabled}
                value={selfvolunOrg}
              />  
            </div>
          </Grid>
        </Grid>
      </Paper>   
      <Grid container spacing={2} className={classes.root}>
          <Grid item style={{width:"50%"}}>
            <Grid container direction="column" alignItems="center">
              <Grid item style={{width:"100%"}}>
                <div className={classes.paper}>
                  <FormLabel component="legend">{GetText("demage_amount")}</FormLabel>
                </div>
                <Paper className={classes.control}>
                <Grid container spacing={1}>
                  <Grid item xs={12}>
                    <div className={classes.paper}>
                      <CustomizedTextField
                        label={GetText("estimated_real_estate_damage")}
                        id="realestExptAmt"
                        type="number"
                        length="12"
                        onChange={onChange}
                        disabled={disabled}
                        value={realestExptAmt}
                      />     
                      <CustomizedTextField
                        label={GetText("estimated_acount_damage_to_person_property")}
                        id="mvestExptAmt"
                        type="number"
                        length="12"
                        onChange={onChange}
                        disabled={disabled}
                        value={mvestExptAmt}
                      />
                      <CustomizedTextField
                        label={GetText("total_amount")}
                        id="totAmt"
                        type="number"
                        length="12"
                        onChange={onChange}
                        disabled={disabled}
                        value={totAmt}
                      />   
                    </div>
                  </Grid>
                </Grid>
                </Paper> 
              </Grid>
            </Grid>
          </Grid>
          <Grid item style={{width:"50%"}}>
            <Grid container direction="column" alignItems="center">
              <Grid item style={{width:"100%"}}>
                <div className={classes.paper}>
                  <FormLabel component="legend">{GetText("distance_shelter_info")}</FormLabel>
                </div>
                <Paper className={classes.control}>
                  <Grid container spacing={1}>
                    <Grid item xs={12}>
                      <div className={classes.paper}>
                        <CustomizedTextField
                          label={GetText("distance_to_fire_office")}
                          id="ward1Dist"
                          type="float"
                          length="8"
                          belowDotLength="1"
                          onChange={onChange}
                          disabled={disabled}
                          value={ward1Dist}
                        />     
                        <CustomizedTextField
                          label={GetText("distance_to_police_office")}
                          id="ward2Dist"
                          type="float"
                          length="8"
                          belowDotLength="1"
                          onChange={onChange}
                          disabled={disabled}
                          value={ward2Dist}
                        />
                        <CustomizedTextField
                          label={GetText("shelter")}
                          id="escPlace"
                          onChange={onChange}
                          disabled={disabled}
                          value={escPlace}
                        />   
                      </div>
                    </Grid>
                  </Grid>
                </Paper>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      <div className={classes.paper}>
        <FormLabel component="legend">{GetText("etc")}</FormLabel>
      </div>
      <Paper className={classes.control}>
        <Grid container spacing={1}>
          <Grid item xs={12}>
            <div className={classes.paper}>
              <CustomizedTextField
                id="etcRemark"
                label={GetText("other_specifics")}
                className={classes.textFieldFullLength}
                multiline={true}
                rows={2}
                variant="outlined"
                onChange={onChange}
                disabled={disabled}
                value={etcRemark}
              />    
            </div>
          </Grid>
        </Grid>
      </Paper>
      <Box p={1}></Box>
      <Divider style={{'width':'100%','position': 'absolute', 'left': 0}} />
      <ActionPanel>
        <Button 
          disabled={loading}
          color={userContext.preference.color}
          type="submit"
        >
          { operateType === dictionary.NEW || operateType === dictionary.UPDATE ? GetText("save") : "" }
        </Button>
        { 
          inputs.objId && operateType !== dictionary.NEW &&
            <Button 
              disabled={loading}
              color={userContext.preference.color}
              name="update"
              onClick={onChangeUpdateState}
            >
              { isUpdateMode ? GetText("cancel") : GetText("update") }
            </Button>
        }
        
        {
          inputs.objId && 
            <Button 
              disabled={loading}
              color={userContext.preference.color}
              name="delete"
              onClick={onDelete}
          >
              {GetText("delete")}
            </Button> 
        }
        
        {loading && <CircularProgress size={24} className={classes.buttonProgress} />}
      </ActionPanel>
      </form>
      <CustomizedSnackbar isOpen={openSnackbar} severity={severity} message={message} closeHandler={onCloseSnackbar} />
      <Confirm open={isConfirmOpen} MessageType={messageType} MessageId={messageId} handleClose={handleConfirmDialog} />
    </React.Fragment>
  );
}