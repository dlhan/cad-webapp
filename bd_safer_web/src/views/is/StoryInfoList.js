import React, { useState, useEffect } from 'react';
import { GetText } from '../../common/BundleManager'
import serviceList from '../../service/ServiceList';
import CircularProgress from '@material-ui/core/CircularProgress';
import CustomizedSnackbar from '../../components/Snackbar';
import Dictionary from '../../common/Dictionary';
import { RunService } from '../../service/RestApi';
import { SetSearchBody, ResetForResponseData, CheckMenuAuth, ConvertItemsToComboType } from '../../common/Utility';
import StickyHeadTable from '../../components/Table';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import AddIcon from '@material-ui/icons/Add';
import Fab from '@material-ui/core/Fab';
import CustomizedDialog from '../../components/Dialog';
import { FormStyle } from '../../style/Style';
import ViewInfo from '../../common/ViewInfo';
import { useUserContext } from '../../common/UserContext';
import Confirm from '../../components/Confirm';
import CustomizedSelect from '../../components/Select';
import FormLabel from '@material-ui/core/FormLabel';

const useStyles = FormStyle;

const storyColumns = [
  { id: 'objId', numeric: false, disablePadding: false, label: '', align: 'center', minWidth: 0 },
  { id: 'bldgSeq', numeric: false, disablePadding: false, label: '', align: 'center', minWidth: 0 },
  { id: 'storySeq', numeric: false, disablePadding: false, label: '', align: 'center', minWidth: 0 },
  { id: 'storyNo', numeric: false, disablePadding: false, label: 'floor', align: 'center', minWidth: 150 },
  { id: 'storyArea', numeric: false, disablePadding: false, label: 'total_area', align: 'center', minWidth: 150 },  
  { id: 'fcontCnt', numeric: false, disablePadding: false, label: 'fire_compartment_number', align: 'center', minWidth: 220 },
  { id: 'deco', numeric: false, disablePadding: false, label: 'interior_material', align: 'center', minWidth: 200 },
  { id: 'residentManCnt', numeric: false, disablePadding: false, label: 'resident_number', align: 'center', minWidth: 200 },
  { id: 'accomManCnt', numeric: false, disablePadding: false, label: 'people_accommodated_number', align: 'center', minWidth: 200 },
];

const specUsedColumns = [
  { id: 'objId', numeric: false, disablePadding: false, label: '', align: 'center', minWidth: 0 },
  { id: 'bldgSeq', numeric: false, disablePadding: false, label: '', align: 'center', minWidth: 0 },
  { id: 'storySeq', numeric: false, disablePadding: false, label: '', align: 'center', minWidth: 0 },
  { id: 'specUseSeq', numeric: false, disablePadding: false, label: '', align: 'center', minWidth: 0 },
  { id: 'storyNo', numeric: false, disablePadding: false, label: 'floor', align: 'center', minWidth: 150 },
  { id: 'specUseClsCdName', numeric: false, disablePadding: false, label: 'usage', align: 'center', minWidth: 150 },  
  { id: 'area', numeric: false, disablePadding: false, label: 'total_area', align: 'center', minWidth: 150 },
  { id: 'remark', numeric: false, disablePadding: false, label: 'remark', align: 'center', minWidth: 500 },  
];

export default function StoryInfoList(props) {
  const classes = useStyles();
  var s = new serviceList();
  var dictionary = new Dictionary();
  var viewInfo = new ViewInfo();
  
  const userContext = useUserContext();
  let [view, setView] = useState(viewInfo.storyDetail);
  let [loading, setLoading] = useState(false);
  let [buildingList, setBuildingList] = useState([]);
  
  let [inputs, setInputs] = useState({
    bldgSeq:"",
    storySeq:""
  });
  let { bldgSeq,storySeq } = inputs;

  const hasFireObjectCreateAuth = CheckMenuAuth(["IS002"]);
  
  ////////Setting for Table//////////////////////////////////////////
  const onSelectStoryRow = (rowId) => {
    handleDialogOpen("story", dictionary.READ, rowId);
  }

  const onSelectSpecUsedStoryRow = (rowId) => {
    handleDialogOpen("specUsedStory", dictionary.READ, rowId);
  }
  
  const [storyRows, setStoryRows] = useState([]);
  const [specUsedRows, setSpecUsedRows] = useState([]);
  
///////////////////////////////////////////////////////////////////
  useEffect(() => {
    RunService(s.getBuildingListForComboBox,{objId:props.objId, locale:userContext.preference.language},'', true)
        .then(response => {
          var comboItems = ConvertItemsToComboType("bldgSeq","bldgName",ResetForResponseData(s.getBuildingListForComboBox.response,response.data,""));
          setBuildingList(comboItems);
          if (Array.isArray(comboItems) && comboItems.length > 0) {
            setInputs({
              ...inputs,
              bldgSeq:comboItems[0].value
            });
            getData(comboItems[0].value);
          }
        })
        .catch(error => {
          setSnackbar(prev => ({
            ...prev,
            openSnackbar:true,
            severity:dictionary.MessageType.Error,
            message:GetText("msg_normal_error_message")
          }));
        })
  }, []);

  const getData = (bldgSeq) => {
    setLoading(true);
    
    RunService(s.getStoryList,{objId:props.objId, bldgSeq: bldgSeq},'', true)
        .then(response => {
          setLoading(false);
          setStoryRows(ResetForResponseData(s.getStoryList.response,response.data,"storySeq"));
        })
        .catch(error => {
          setLoading(false);
          setSnackbar(prev => ({
            ...prev,
            openSnackbar:true,
            severity:dictionary.MessageType.Error,
            message:GetText("msg_normal_error_message")
          }));
        })
  }

  const getSpecUsedStory=(bldgSeq, storySeq)=> {
    setLoading(true);
    
    RunService(s.getStorySpecUsedList,{objId:props.objId, bldgSeq: bldgSeq, storySeq:storySeq, locale:userContext.preference.language},'', true)
        .then(response => {
          setLoading(false);
          setSpecUsedRows(ResetForResponseData(s.getStorySpecUsedList.response,response.data,"specUseSeq"));
        })
        .catch(error => {
          setLoading(false);
          setSnackbar(prev => ({
            ...prev,
            openSnackbar:true,
            severity:dictionary.MessageType.Error,
            message:GetText("msg_normal_error_message")
          }));
        })
  }
//////Dialog event
  const [isDialogOpen, setIsDialogOpen] = useState(false);
  const [isModified, setIsModified] = useState(false);
  
  const callFromDialog = (p) => {
    setIsModified(p.modified);
    if (p.refresh) {
      if (p.tableType === 'story') {
        getData(bldgSeq);
      } else {
        getSpecUsedStory(bldgSeq, storySeq);
      }
      
    }
  }

  const [sendInfo, setSendInfo] = useState({operateType:'', objId:props.objId});
  const { operateType, objId } = sendInfo;

  const handleDialogClose = (forceNoMessage) => {
    let forceClose = false;

    if (typeof forceNoMessage === 'boolean') forceClose = forceNoMessage;

    if (isModified && !forceClose) {
      setConfirmMessage({
        isConfirmOpen: true,
        messageType: dictionary.WARNING,
        messageId: "msg_confirm_close_dialog" 
      });
    } else {
      setIsDialogOpen(false);
    }
  };
  
  const handleDialogOpen = (type, operateType, id) => {
    if (type === "story") {
      setView(viewInfo.storyDetail);
      setSendInfo({operateType:operateType, objId:props.objId, bldgSeq:bldgSeq,id:id,tableType:'story'});
    } else {
      setView(viewInfo.storySpecUsedDetail);
      setSendInfo({operateType:operateType, objId:props.objId, bldgSeq:bldgSeq,storySeq:storySeq,id:id,tableType:'specUsedStory'});
    }
    
    setIsDialogOpen(true);
  };
////////////////////////////////////////////////////////

//////Confrim event in closing the dialog///////////////
  const [confirmMessage, setConfirmMessage] = useState({
    isConfirmOpen : false,
    messageType : "",
    messageId : ""
  });
  let { isConfirmOpen,messageType,messageId } = confirmMessage;

  const handleConfirmDialog = (answer) => {
    setConfirmMessage({
      ...confirmMessage,
      isConfirmOpen:false
    });

    if (answer){
      setIsDialogOpen(false);
    }
  }
////////////////////////////////////////////////////////

/////////////Setting for Snackbar////////////////////////////////
  let [snackbar, setSnackbar] = useState({openSnackbar:false, severity:'', message:''});
  let { openSnackbar, severity, message } = snackbar;
    
  const onCloseSnackbar = () => {
    setSnackbar(prev => ({
      ...prev,
      openSnackbar:false,
      severity:'',
      message:''
    }));
  }
////////////////////////////////////////////////////////

const bldgComboChangeHandler =(e)=>{
  let {name, value} = e.target;
  setInputs({
    ...inputs,
    bldgSeq:value
  });
  getData(value);
}

const checkBoxClickEvent = (data) => {
  setInputs({
    ...inputs,
    storySeq:data[0]
  });

  getSpecUsedStory(bldgSeq, data[0]);
}
 
  return (
    <React.Fragment>
      <Grid container spacing={1}>
        <Grid item xs={12}>
          <CustomizedSelect 
            id="bldgSeq" 
            label={GetText("building_name")} 
            onChange={bldgComboChangeHandler}
            visibleFirstOption={false} 
            items={buildingList} 
          />
        </Grid>
      </Grid>
      <Paper className={classes.control}>
        <Grid container spacing={2} className={classes.root}>
          <Grid item style={{width:"60%"}}>
            <Grid container direction="column" alignItems="center">
              <Grid item style={{width:"100%"}}>
                <Paper className={classes.control}>
                  <div className={classes.paper}>
                    <FormLabel component="legend">{GetText("story_info")}</FormLabel>
                  </div>
                  <StickyHeadTable 
                    tableId={"story"} 
                    onSelectRow={onSelectStoryRow} 
                    checkBoxClickEvent={checkBoxClickEvent}
                    allowOnlyOneCheck={true}
                    columns={storyColumns} 
                    rows={storyRows} 
                    SupportCheckBox={true} 
                    SupportPaging={false}
                    heightGap={200}
                  />
                </Paper>
              </Grid>
              {hasFireObjectCreateAuth && <Grid item>
                  <div className={classes.centerLocate}>
                    <Fab color={userContext.preference.color} aria-label="add" style={{marginTop:"10px"}}>
                      <AddIcon onClick={() => handleDialogOpen('story',dictionary.NEW)} />
                    </Fab>
                  </div>
                </Grid>
              }
            </Grid>
          </Grid>
          <Grid item style={{width:"40%"}}>
            <Grid container direction="column" alignItems="center">
              <Grid item style={{width:"100%"}}>
                <Paper className={classes.control}>
                  <div className={classes.paper}>
                    <FormLabel component="legend">{GetText("story_special_use_status")}</FormLabel>
                  </div>
                  <StickyHeadTable 
                    tableId={"storySpecUsed"} 
                    onSelectRow={onSelectSpecUsedStoryRow} 
                    columns={specUsedColumns} 
                    rows={specUsedRows} 
                    SupportCheckBox={false} 
                    SupportPaging={false}
                    heightGap={200}
                  />
                </Paper>
              </Grid>
              {hasFireObjectCreateAuth && <Grid item>
                  <div className={classes.centerLocate}>
                    <Fab color={userContext.preference.color} aria-label="add" style={{marginTop:"10px"}}>
                      <AddIcon onClick={() => handleDialogOpen('storySpecUsed',dictionary.NEW)} />
                    </Fab>
                  </div>
                </Grid>
              }
            </Grid>
          </Grid>
        </Grid>
      </Paper>
      {loading && <CircularProgress size={24} className={classes.buttonProgress} />}
      <CustomizedDialog isDialogOpen={isDialogOpen} sendInfo={sendInfo} viewInfo={view} handleDialogClose={handleDialogClose} callFromDialog={callFromDialog} />
      <Confirm open={isConfirmOpen} MessageType={messageType} MessageId={messageId} handleClose={handleConfirmDialog} />
      <CustomizedSnackbar isOpen={openSnackbar} severity={severity} message={message} closeHandler={onCloseSnackbar} />
     </React.Fragment>
  );
}