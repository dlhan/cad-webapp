import React, { useState, useEffect } from 'react';
import { GetText } from '../../common/BundleManager';
import serviceList from '../../service/ServiceList';
import { RunService } from '../../service/RestApi';
import Dictionary from '../../common/Dictionary';
import { CheckResultFromService, ResetForResponseData } from '../../common/Utility';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import Address from '../../components/Address';
import ActionPanel from '../../components/ActionPanel';
import { FormStyle } from '../../style/Style';
import CustomizedSwitch from '../../components/Switch';
import { useUserContext } from '../../common/UserContext';
import CustomizedSnackbar from '../../components/Snackbar';
import CircularProgress from '@material-ui/core/CircularProgress';
import FormLabel from '@material-ui/core/FormLabel';
import ViewInfo from '../../common/ViewInfo';
import { useForm } from "react-hook-form";
import Confirm from '../../components/Confirm';
import CustomizedTextField from '../../components/TextField';

const useStyles = FormStyle;
let originalInputs = {};
let isModified = false;

export default function ArsonManagerDetail(props) {
  const classes = useStyles();
  var dictionary = new Dictionary();
  const userContext = useUserContext();
  const s = new serviceList();
  var viewInfo = new ViewInfo();

  const [operateType, setOperateType] = useState(props.sendInfo.operateType);
  let [inputs, setInputs] = useState({objId:props.sendInfo.objId
                                    ,fmgrSeq:''
                                    ,firstName:''
                                    ,lastName:''
                                    ,birthday:''
                                    ,zipCode:''
                                    ,address:''
                                    ,telNum:''
                                    ,assignDate:''
                                    ,dismissDate:''
                                    ,citizenNo:''
                                    ,pubFmngYn:''
                                    ,dismissRemark:''
                                    ,title:''
                                    ,mgeAgencyYn:''
                                    ,useYn:''
                                    });
  let { objId,fmgrSeq,firstName,lastName,birthday,zipCode,address,telNum,assignDate,dismissDate,citizenNo,pubFmngYn,dismissRemark,title,mgeAgencyYn } = inputs;
  let [loading, setLoading] = useState(false);
  let [disabled, setDisabled] = useState(false);
  let [isUpdateMode, setIsUpdateMode] = useState(false);
    
/////////Setting for validate the inputs in the form.
  const { handleSubmit } = useForm();
////////////////////////////////////////////////////

  const onChange = e => {
    let {name, value} = e.target;
    
    if (name === "pubFmngYn") { value = !inputs.pubFmngYn};
    if (name === "mgeAgencyYn") { value = !inputs.mgeAgencyYn};
            
    isModified = true;
    setInputs({
      ...inputs,
      [name]:value
    });
  }

/////////////Setting for Snackbar////////////////////////////////
  let [snackbar, setSnackbar] = useState({openSnackbar:false, severity:'', message:''});
  let { openSnackbar, severity, message } = snackbar;
    
  const onCloseSnackbar = () => {
    setSnackbar(prev => ({
      ...prev,
      openSnackbar:false,
      severity:'',
      message:''
    }));
  }
/////////////////////////////////////////////

//////Confrim event
  const [confirmMessage, setConfirmMessage] = React.useState({
                                                      isConfirmOpen : false,
                                                      messageType : "",
                                                      messageId : ""
                                                });
  let { isConfirmOpen,messageType,messageId } = confirmMessage;
  
  const handleConfirmDialog = (answer) => {
    setConfirmMessage({
      ...confirmMessage,
      isConfirmOpen:false
    });
  
    if (answer){
      onSubmit();
    }
  }
  /////////////////////////////////////////////
  
  ////Button operate
  
  const onChangeUpdateState = () => {
    setIsUpdateMode(!isUpdateMode);
    if (!isUpdateMode) {
      onUpdateMode();
    } else {
      onReadMode();
    }
  }

  const onReadMode = () => {
    setDisabled(true);
    setOperateType("");
    setIsUpdateMode(false);
    props.callFromDialog({modified: false});
  }

  const onNewMode = () => {
    setDisabled(false);
    setOperateType(dictionary.NEW);
    setInputs({objId:props.sendInfo.objId
            ,fmgrSeq:''
            ,firstName:''
            ,lastName:''
            ,birthday:''
            ,zipCode:''
            ,address:''
            ,telNum:''
            ,assignDate:''
            ,dismissDate:''
            ,citizenNo:''
            ,pubFmngYn:''
            ,dismissRemark:''
            ,title:''
            ,mgeAgencyYn:''
            ,useYn:''
    });
    props.callFromDialog({operateType:dictionary.NEW, modified: false});
    originalInputs = {objId:props.sendInfo.objId
                     ,fmgrSeq:''
                     ,firstName:''
                     ,lastName:''
                     ,birthday:''
                     ,zipCode:''
                     ,address:''
                     ,telNum:''
                     ,assignDate:''
                     ,dismissDate:''
                     ,citizenNo:''
                     ,pubFmngYn:''
                     ,dismissRemark:''
                     ,title:''
                     ,mgeAgencyYn:''
                     ,useYn:''
                    };
    isModified = false;
  }

  const onUpdateMode = () => {
    setDisabled(false);
    setOperateType(dictionary.UPDATE);
    props.callFromDialog({operateType:dictionary.UPDATE, modified: false});
  }

  const onConfirm = () => {
    if (operateType === dictionary.NEW) {
      setConfirmMessage({ isConfirmOpen: true, messageType: dictionary.WARNING, messageId : "msg_will_you_insert" });
    } else if (operateType === dictionary.UPDATE) {
      setConfirmMessage({ isConfirmOpen: true, messageType: dictionary.WARNING, messageId : "msg_will_you_update" });
    } else {
      onNewMode();
    }
  }

  const onDelete = () => {
    setOperateType(dictionary.DELETE);
    setConfirmMessage({ isConfirmOpen: true, messageType: dictionary.WARNING, messageId : "msg_will_you_delete" });
  }
/////////////////Check whether inputs are modified or not. If modified, it sends the state to parent//////////////////// 
  useEffect(() => {
    let Modified =  (JSON.stringify(inputs) === JSON.stringify(originalInputs)? false : true);
    
    if (operateType !== dictionary.READ) props.callFromDialog({modified: Modified && isModified});
  }, [inputs]);
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////Get data////////////////////////////////////////////
  useEffect(() => {
    let completed = false; 
    
    async function getInfo(id) {
      let body = {objId: props.sendInfo.objId,fmgrSeq:id};
      setLoading(true);
      RunService(s.getArsonManagerInfo, body)
          .then(response => {
            let reponseData = ResetForResponseData(s.getArsonManagerInfo.response, response.data);
            setInputs(reponseData);
            originalInputs = reponseData;
            
            onReadMode();
            setLoading(false);
          })
          .catch(error => {
            setSnackbar(prev => ({
              ...prev,
              openSnackbar:true,
              severity:dictionary.MessageType.Error,
              message:GetText("msg_normal_error_message")
            }));
          })
    }

    if (props.sendInfo.fmgrSeq) getInfo(props.sendInfo.fmgrSeq);
  }, []);
  /////////////////////////////////////////////////////////////////

  ///////////////////// Insert or Update///////////////////////////
  const onSubmit = () => {
    if (operateType === "") {
      onNewMode();
      return;
    }

    setLoading(true);
    let service;
    let body;

    switch(operateType) {
      case dictionary.NEW :
        service = s.insertArsonManager;
        body = inputs;
        break;
      case dictionary.UPDATE :
        service = s.updateArsonManager;
        body = inputs;
        break;
      case dictionary.DELETE :
        service = s.deleteArsonManager;
        body = [{objId:inputs.objId,fmgrSeq:inputs.fmgrSeq}];
        break;
      default :
        return;
    }
  
    RunService(service,'' ,body)
        .then(response => {
          setLoading(false);
          var result = CheckResultFromService(response.data);
          if (result.isSuccess) {
            setSnackbar(prev => ({
              ...prev,
              openSnackbar:true,
              severity:dictionary.MessageType.Success,
              message:GetText(result.message)
            }));
            
            setInputs({
              ...inputs,
              fmgrSeq:result.id
            });
            onReadMode();
            
            if (operateType === dictionary.DELETE) {
              props.callFromDialog({operateType:dictionary.DELETE, modified: false, refresh:true});
              props.handleDialogClose(true);
            } else {
              props.callFromDialog({operateType:dictionary.READ, modified: false, refresh:true});
            }
          } else {
            setSnackbar(prev => ({
              ...prev,
              openSnackbar:true,
              severity:dictionary.MessageType.Error,
              message:GetText(result.message)
            }));
          }
        })
        .catch(error => {
          setLoading(false);
          setSnackbar(prev => ({
            ...prev,
            openSnackbar:true,
            severity:dictionary.MessageType.Error,
            message:GetText("msg_normal_error_message")
          }));
        })
  } 
  /////////////////////////////////////////////////////////////////

  return (
    <React.Fragment>
      <form onSubmit={handleSubmit(onConfirm)}>
      <div className={classes.paper}>
        <FormLabel component="legend">{GetText("basic_information")}</FormLabel>
      </div>
      <Paper className={classes.control}>
        <Grid container spacing={1}>
          <Grid item xs={12}>
            <div className={classes.paper}>
              <CustomizedTextField
                label={GetText("first_name")}
                id="firstName"
                onChange={onChange}
                disabled={disabled}
                value={firstName}
              />
              <CustomizedTextField
                label={GetText("last_name")}
                id="lastName"
                onChange={onChange}
                disabled={disabled}
                value={lastName}
              />
              <CustomizedTextField
                label={GetText("birth_day")}
                id="birthday"
                type="date"
                onChange={onChange}
                disabled={disabled}
                value={birthday}
              />
              <CustomizedTextField
                label={GetText("citizen_number")}
                id="citizenNo"
                onChange={onChange}
                disabled={disabled}
                value={citizenNo}
              />
            </div>
          </Grid>
          <Grid item xs={12}>
            <div className={classes.paper}>
              <CustomizedTextField
                label={GetText("tel_num")}
                id="telNum"
                onChange={onChange}
                disabled={disabled}
                value={telNum}
              />
              <CustomizedTextField
                label={GetText("job_position")}
                id="title"
                onChange={onChange}
                disabled={disabled}
                value={title}
              />
              <CustomizedTextField
                label={GetText("appointment_date")}
                id="assignDate"
                type="date"
                onChange={onChange}
                disabled={disabled}
                value={assignDate}
              />
              <CustomizedSwitch 
                id='mgeAgencyYn'
                name='mgeAgencyYn' 
                checked={mgeAgencyYn}
                onChange={onChange}
                disabled={disabled}
                value={mgeAgencyYn}
                label={GetText("is_agency_for_management")} 
              />
              <CustomizedSwitch 
                id='pubFmngYn'
                name='pubFmngYn' 
                checked={pubFmngYn}
                onChange={onChange}
                disabled={disabled}
                value={pubFmngYn}
                label={GetText("is_joint_fire_management")} 
              />
            </div>
          </Grid>
          <Grid item xs={12}>
            <div className={classes.paper}>
              <CustomizedTextField
                label={GetText("dismiss_date")}
                id="dismissDate"
                type="date"
                onChange={onChange}
                disabled={disabled}
                value={dismissDate}
              />
              <CustomizedTextField
                label={GetText("dismiss_reason")}
                id="dismissRemark"
                className={classes.textField3} 
                onChange={onChange}
                disabled={disabled}
                value={dismissRemark}
              />
            </div>
          </Grid>
          <Grid item xs={12}>
            <div className={classes.paper}>
              <Address 
                addressId={'zipCode'} 
                etcAddressid={'address'} 
                changeEvent={onChange}
                disabled={disabled}
                postalCodeValue={zipCode}
                etcAddressValue={address}
              />
            </div>
          </Grid>
        </Grid>
      </Paper>   
      <Box p={1}></Box>
      <Divider style={{'width':'100%','position': 'absolute', 'left': 0}} />
      <ActionPanel>
        <Button 
          disabled={loading}
          color={userContext.preference.color}
          type="submit"
        >
          { operateType !== "" ? GetText("save") : GetText("new") }
        </Button>
        { 
          inputs.objId && inputs.fmgrSeq && operateType !== dictionary.NEW &&
            <Button 
              disabled={loading}
              color={userContext.preference.color}
              name="update"
              onClick={onChangeUpdateState}
            >
              { isUpdateMode ? GetText("cancel") : GetText("update") }
            </Button>
        }
        
        {
          inputs.objId && inputs.fmgrSeq && 
            <Button 
              disabled={loading}
              color={userContext.preference.color}
              name="delete"
              onClick={onDelete}
          >
              {GetText("delete")}
            </Button> 
        }
        
        {loading && <CircularProgress size={24} className={classes.buttonProgress} />}
      </ActionPanel>
      </form>
      <CustomizedSnackbar isOpen={openSnackbar} severity={severity} message={message} closeHandler={onCloseSnackbar} />
      <Confirm open={isConfirmOpen} MessageType={messageType} MessageId={messageId} handleClose={handleConfirmDialog} />
    </React.Fragment>
  );
}