import React, { useState, useEffect } from 'react';
import { GetText } from '../../common/BundleManager';
import serviceList from '../../service/ServiceList';
import { RunService } from '../../service/RestApi';
import Dictionary from '../../common/Dictionary';
import { CheckResultFromService, ResetForResponseData, ConvertItemsToComboType } from '../../common/Utility';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import ActionPanel from '../../components/ActionPanel';
import { FormStyle } from '../../style/Style';
import CustomizedSelect from '../../components/Select';
import { useUserContext } from '../../common/UserContext';
import CustomizedSnackbar from '../../components/Snackbar';
import CircularProgress from '@material-ui/core/CircularProgress';
import FormLabel from '@material-ui/core/FormLabel';
import ViewInfo from '../../common/ViewInfo';
import { useForm } from "react-hook-form";
import Confirm from '../../components/Confirm';
import CustomizedTextField from '../../components/TextField';

const useStyles = FormStyle;
let originalInputs = {};
let isModified = false;

export default function FireEquipmentDetail(props) {
  const classes = useStyles();
  var dictionary = new Dictionary();
  const userContext = useUserContext();
  const s = new serviceList();
  let [buildingList, setBuildingList] = useState([]);
  let [storyList, setStoryList] = useState([]);
  let [bldgSeq, setBldgSeq] = useState(props.sendInfo.bldgSeq);
  let [storySeq, setStorySeq] = useState(props.sendInfo.storySeq);
  
  const [operateType, setOperateType] = useState(props.sendInfo.operateType);
  let [inputs, setInputs] = useState({objId:props.sendInfo.objId
                                    ,inExtingCnt:'' 
                                    ,outExtingCnt:'' 
                                    ,extingPumpCnt:'' 
                                    ,sprinklerHCnt:'' 
                                    ,sprinklerAvCnt:'' 
                                    ,sprayExtingHCnt :'' 
                                    ,sprayExtingAvCnt :'' 
                                    ,poExtingHCnt :'' 
                                    ,poExtingAvCnt :'' 
                                    ,carbonDioxHCnt :'' 
                                    ,carbonDioxAvCnt :'' 
                                    ,halogenCompHCnt :'' 
                                    ,harlogenCompAvCnt :'' 
                                    ,powderExtingHCnt :'' 
                                    ,powderExtingAvCnt :'' 
                                    ,slideCnt :'' 
                                    ,ladderCnt :'' 
                                    ,rescueCnt :'' 
                                    ,descSlowDeviceCnt :'' 
                                    ,measureEquipCnt :'' 
                                    ,measureRopeCnt :'' 
                                    ,safeMatCnt :'' 
                                    ,rescueEquipCnt :'' 
                                    ,emgLightCnt :'' 
                                    ,waterExtingCnt :'' 
                                    ,extingWaterCnt :'' 
                                    ,lwtrCnt :'' 
                                    ,wtrpipeCnt :'' 
                                    ,waterSpringklingCnt :'' 
                                    ,emgPlugCnt :'' 
                                    ,wirelessCommCnt:''  
                                    ,extingCnt :'' 
                                    ,simplctyExtingCnt :'' 
                                    ,emgWaringCnt :'' 
                                    ,emgBrodcCnt :'' 
                                    ,lkgeWaringCnt :'' 
                                    ,autoFireFindSensCnt:''  
                                    ,autoFireFindCircuitCnt :'' 
                                    ,autoFireNewsfCnt :'' 
                                    ,gasLkgeWaringCnt :'' 
                                    ,induceLightCnt :'' 
                                    ,induceSignpostCnt :'' 
                                    ,hydEquipEtcCnt :'' 
                                    ,resmokeCnt :'' 
                                    ,curtainCnt :'' 
                                    ,carssetteCnt :'' 
                                    ,resistEtcCnt :'' 
                                    ,useYn:''
                                    });
  let { objId,inExtingCnt,outExtingCnt,extingPumpCnt,sprinklerHCnt,sprinklerAvCnt,sprayExtingHCnt,sprayExtingAvCnt,poExtingHCnt,poExtingAvCnt,carbonDioxHCnt,carbonDioxAvCnt,halogenCompHCnt,harlogenCompAvCnt,powderExtingHCnt,powderExtingAvCnt,slideCnt,ladderCnt,rescueCnt ,descSlowDeviceCnt ,measureEquipCnt ,measureRopeCnt ,safeMatCnt ,rescueEquipCnt ,emgLightCnt ,waterExtingCnt ,extingWaterCnt ,lwtrCnt ,wtrpipeCnt ,waterSpringklingCnt ,emgPlugCnt ,wirelessCommCnt,extingCnt ,simplctyExtingCnt ,emgWaringCnt ,emgBrodcCnt ,lkgeWaringCnt ,autoFireFindSensCnt,autoFireFindCircuitCnt,autoFireNewsfCnt,gasLkgeWaringCnt,induceLightCnt,induceSignpostCnt,hydEquipEtcCnt,resmokeCnt,curtainCnt,carssetteCnt,resistEtcCnt } = inputs;
  let [loading, setLoading] = useState(false);
  let [disabled, setDisabled] = useState(false);
  let [comboDisabled, setComboDisabled] = useState(false);
  let [isUpdateMode, setIsUpdateMode] = useState(false);
    
/////////Setting for validate the inputs in the form.
  const { handleSubmit } = useForm();
////////////////////////////////////////////////////

  const onChange = e => {
    let {name, value} = e.target;
    
    if (name === "rpsnBldgYn") { value = !inputs.rpsnBldgYn};
    if (name === "removeYn") { value = !inputs.removeYn};
    if (name === "rootYn") { value = !inputs.rootYn};
        
    isModified = true;
    setInputs({
      ...inputs,
      [name]:value
    });
  }

/////////////Setting for Snackbar////////////////////////////////
  let [snackbar, setSnackbar] = useState({openSnackbar:false, severity:'', message:''});
  let { openSnackbar, severity, message } = snackbar;
    
  const onCloseSnackbar = () => {
    setSnackbar(prev => ({
      ...prev,
      openSnackbar:false,
      severity:'',
      message:''
    }));
  }
/////////////////////////////////////////////

//////Confrim event
  const [confirmMessage, setConfirmMessage] = React.useState({
                                                      isConfirmOpen : false,
                                                      messageType : "",
                                                      messageId : ""
                                                });
  let { isConfirmOpen,messageType,messageId } = confirmMessage;
  
  const handleConfirmDialog = (answer) => {
    setConfirmMessage({
      ...confirmMessage,
      isConfirmOpen:false
    });
  
    if (answer){
      onSubmit();
    }
  }
  /////////////////////////////////////////////

  useEffect(() => {
    RunService(s.getBuildingListForComboBox,{objId:props.sendInfo.objId, locale:userContext.preference.language},'', true)
        .then(response => {
          var comboItems = ConvertItemsToComboType("bldgSeq","bldgName",ResetForResponseData(s.getBuildingListForComboBox.response,response.data,""));
          setBuildingList(comboItems);
          setBldgSeq(comboItems[0].value);
          getStoryData(comboItems[0].value);
        })
        .catch(error => {
          setSnackbar(prev => ({
            ...prev,
            openSnackbar:true,
            severity:dictionary.MessageType.Error,
            message:GetText("msg_normal_error_message")
          }));
        })
  }, []);

  const getStoryData = (id) => {
    var body = inputs;
    RunService(s.getStoryListForComboBox,{objId:props.sendInfo.objId, bldgSeq: id},body, true)
        .then(response => {
          var comboItems = ConvertItemsToComboType("storySeq","storyNo",ResetForResponseData(s.getStoryListForComboBox.response,response.data,""));
          setStoryList(comboItems);
          setStorySeq(comboItems[0].value);
        })
        .catch(error => {
          setLoading(false);
          setSnackbar(prev => ({
            ...prev,
            openSnackbar:true,
            severity:dictionary.MessageType.Error,
            message:GetText("msg_normal_error_message")
          }));
        })
  }

  const bldgComboChangeHandler =(e)=>{
    let {name, value} = e.target;
    setBldgSeq(value);
    getStoryData(value);
  }

  const storyComboChangeHandler =(e)=>{
    let {name, value} = e.target;
    setStorySeq(value);
  }
  
  ////Button operate
  
  const onChangeUpdateState = () => {
    setIsUpdateMode(!isUpdateMode);
    if (!isUpdateMode) {
      onUpdateMode();
    } else {
      onReadMode();
    }
  }

  const onReadMode = () => {
    setDisabled(true);
    setComboDisabled(true);
    setOperateType("");
    setIsUpdateMode(false);
    props.callFromDialog({modified: false});
  }

  const onNewMode = () => {
    setDisabled(false);
    setComboDisabled(false);
    setOperateType(dictionary.NEW);
    setInputs({ objId:props.sendInfo.objId
                ,bldgSeq:''
                ,storySeq:''
                ,inExtingCnt:'' 
                ,outExtingCnt:'' 
                ,extingPumpCnt:'' 
                ,sprinklerHCnt:'' 
                ,sprinklerAvCnt:'' 
                ,sprayExtingHCnt :'' 
                ,sprayExtingAvCnt :'' 
                ,poExtingHCnt :'' 
                ,poExtingAvCnt :'' 
                ,carbonDioxHCnt :'' 
                ,carbonDioxAvCnt :'' 
                ,halogenCompHCnt :'' 
                ,harlogenCompAvCnt :'' 
                ,powderExtingHCnt :'' 
                ,powderExtingAvCnt :'' 
                ,slideCnt :'' 
                ,ladderCnt :'' 
                ,rescueCnt :'' 
                ,descSlowDeviceCnt :'' 
                ,measureEquipCnt :'' 
                ,measureRopeCnt :'' 
                ,safeMatCnt :'' 
                ,rescueEquipCnt :'' 
                ,emgLightCnt :'' 
                ,waterExtingCnt :'' 
                ,extingWaterCnt :'' 
                ,lwtrCnt :'' 
                ,wtrpipeCnt :'' 
                ,waterSpringklingCnt :'' 
                ,emgPlugCnt :'' 
                ,wirelessCommCnt:''  
                ,extingCnt :'' 
                ,simplctyExtingCnt :'' 
                ,emgWaringCnt :'' 
                ,emgBrodcCnt :'' 
                ,lkgeWaringCnt :'' 
                ,autoFireFindSensCnt:''  
                ,autoFireFindCircuitCnt :'' 
                ,autoFireNewsfCnt :'' 
                ,gasLkgeWaringCnt :'' 
                ,induceLightCnt :'' 
                ,induceSignpostCnt :'' 
                ,hydEquipEtcCnt :'' 
                ,resmokeCnt :'' 
                ,curtainCnt :'' 
                ,carssetteCnt :'' 
                ,resistEtcCnt :'' 
                ,useYn:''
            });
    props.callFromDialog({operateType:dictionary.NEW, modified: false});
    originalInputs = {
                objId:props.sendInfo.objId
                ,bldgSeq:''
                ,storySeq:''
                ,inExtingCnt:'' 
                ,outExtingCnt:'' 
                ,extingPumpCnt:'' 
                ,sprinklerHCnt:'' 
                ,sprinklerAvCnt:'' 
                ,sprayExtingHCnt :'' 
                ,sprayExtingAvCnt :'' 
                ,poExtingHCnt :'' 
                ,poExtingAvCnt :'' 
                ,carbonDioxHCnt :'' 
                ,carbonDioxAvCnt :'' 
                ,halogenCompHCnt :'' 
                ,harlogenCompAvCnt :'' 
                ,powderExtingHCnt :'' 
                ,powderExtingAvCnt :'' 
                ,slideCnt :'' 
                ,ladderCnt :'' 
                ,rescueCnt :'' 
                ,descSlowDeviceCnt :'' 
                ,measureEquipCnt :'' 
                ,measureRopeCnt :'' 
                ,safeMatCnt :'' 
                ,rescueEquipCnt :'' 
                ,emgLightCnt :'' 
                ,waterExtingCnt :'' 
                ,extingWaterCnt :'' 
                ,lwtrCnt :'' 
                ,wtrpipeCnt :'' 
                ,waterSpringklingCnt :'' 
                ,emgPlugCnt :'' 
                ,wirelessCommCnt:''  
                ,extingCnt :'' 
                ,simplctyExtingCnt :'' 
                ,emgWaringCnt :'' 
                ,emgBrodcCnt :'' 
                ,lkgeWaringCnt :'' 
                ,autoFireFindSensCnt:''  
                ,autoFireFindCircuitCnt :'' 
                ,autoFireNewsfCnt :'' 
                ,gasLkgeWaringCnt :'' 
                ,induceLightCnt :'' 
                ,induceSignpostCnt :'' 
                ,hydEquipEtcCnt :'' 
                ,resmokeCnt :'' 
                ,curtainCnt :'' 
                ,carssetteCnt :'' 
                ,resistEtcCnt :'' 
                ,useYn:''
                  };
    isModified = false;
  }

  const onUpdateMode = () => {
    setDisabled(false);
    setOperateType(dictionary.UPDATE);
    props.callFromDialog({operateType:dictionary.UPDATE, modified: false});
  }

  const onConfirm = () => {
    if (operateType === dictionary.NEW) {
      setConfirmMessage({ isConfirmOpen: true, messageType: dictionary.WARNING, messageId : "msg_will_you_insert" });
    } else if (operateType === dictionary.UPDATE) {
      setConfirmMessage({ isConfirmOpen: true, messageType: dictionary.WARNING, messageId : "msg_will_you_update" });
    } else {
      onNewMode();
    }
  }

  const onDelete = () => {
    setOperateType(dictionary.DELETE);
    setConfirmMessage({ isConfirmOpen: true, messageType: dictionary.WARNING, messageId : "msg_will_you_delete" });
  }
/////////////////Check whether inputs are modified or not. If modified, it sends the state to parent//////////////////// 
  useEffect(() => {
    let Modified =  (JSON.stringify(inputs) === JSON.stringify(originalInputs)? false : true);
    
    if (operateType !== dictionary.READ) props.callFromDialog({modified: Modified && isModified});
  }, [inputs]);
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////Get data////////////////////////////////////////////
  useEffect(() => {
    let completed = false; 
    
    async function getInfo() {
      let body = {"objId": props.sendInfo.objId,"bldgSeq":props.sendInfo.bldgSeq,"storySeq":props.sendInfo.storySeq};
      setLoading(true);
      RunService(s.getFireEuipInfo, body)
          .then(response => {
            let reponseData = ResetForResponseData(s.getFireEuipInfo.response, response.data);
            setInputs(reponseData);
            originalInputs = reponseData;
            
            onReadMode();
            setLoading(false);
          })
          .catch(error => {
            setSnackbar(prev => ({
              ...prev,
              openSnackbar:true,
              severity:dictionary.MessageType.Error,
              message:GetText("msg_normal_error_message")
            }));
          })
    }

    if (props.sendInfo.bldgSeq && props.sendInfo.storySeq) getInfo();
  }, []);
  /////////////////////////////////////////////////////////////////

  ///////////////////// Insert or Update///////////////////////////
  const onSubmit = () => {
    if (operateType === "") {
      onNewMode();
      return;
    }

    setLoading(true);
    let service;
    let body;

    switch(operateType) {
      case dictionary.NEW :
        service = s.insertFireEuip;
        body = inputs;
        body.bldgSeq = bldgSeq;
        body.storySeq = storySeq;
        break;
      case dictionary.UPDATE :
        service = s.updateFireEuip;
        body = inputs;
        break;
      case dictionary.DELETE :
        service = s.deleteFireEuip;
        body = [{objId:inputs.objId,bldgSeq:inputs.bldgSeq,storySeq:inputs.storySeq}]; 
        break;
      default :
        return;
    }
  
    RunService(service,{userId: userContext.userInfo.userId} ,body)
        .then(response => {
          setLoading(false);
          var result = CheckResultFromService(response.data);
          if (result.isSuccess) {
            setSnackbar(prev => ({
              ...prev,
              openSnackbar:true,
              severity:dictionary.MessageType.Success,
              message:GetText(result.message)
            }));
            
            setInputs({
              ...inputs,
              storySeq:result.id
            });
            onReadMode();
            
            if (operateType === dictionary.DELETE) {
              props.callFromDialog({operateType:dictionary.DELETE, modified: false, refresh:true});
              props.handleDialogClose(true);
            } else {
              props.callFromDialog({operateType:dictionary.READ, modified: false, refresh:true});
            }
          } else {
            setSnackbar(prev => ({
              ...prev,
              openSnackbar:true,
              severity:dictionary.MessageType.Error,
              message:GetText(result.message)
            }));
          }
        })
        .catch(error => {
          setLoading(false);
          setSnackbar(prev => ({
            ...prev,
            openSnackbar:true,
            severity:dictionary.MessageType.Error,
            message:GetText("msg_normal_error_message")
          }));
        })
  } 
  /////////////////////////////////////////////////////////////////

  return (
    <React.Fragment>
      <form onSubmit={handleSubmit(onConfirm)}>
        <Grid container spacing={1}>
          <Grid item xs={12}>
            <div className={classes.paper}>
              <CustomizedSelect 
                id="bldgSeq" 
                label={GetText("building_name")} 
                onChange={bldgComboChangeHandler}
                selectedItemId={bldgSeq}
                visibleFirstOption={false}
                items={buildingList} 
                required={true}
                disabled={comboDisabled}
              />
              <CustomizedSelect 
                id="storySeq" 
                label={GetText("floor")} 
                onChange={storyComboChangeHandler}
                selectedItemId={storySeq}
                visibleFirstOption={false}
                items={storyList} 
                required={true}
                disabled={comboDisabled}
              />
            </div>
          </Grid>
        </Grid>
      <div className={classes.paper}>
        <FormLabel component="legend">{GetText("fire_extinguishing_equip_and_water")}</FormLabel>
      </div>
      <Paper className={classes.control}>
        <Grid container spacing={1}>
          <Grid item xs={12}>
            <div className={classes.paper}>
              <CustomizedTextField
                label={GetText("in_exting")}
                id="inExtingCnt"
                name="inExtingCnt"
                className={classes.textField22}
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={inExtingCnt}
              />    
              <CustomizedTextField
                label={GetText("out_exting")}
                id="outExtingCnt"
                name="outExtingCnt"
                className={classes.textField22}
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={outExtingCnt}
              />   
              <CustomizedTextField
                label={GetText("carbon_diox_h")}
                id="carbonDioxHCnt"
                name="carbonDioxHCnt"
                className={classes.textField22}
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={carbonDioxHCnt}
              />   
              <CustomizedTextField
                label={GetText("carbon_diox_av")}
                id="carbonDioxAvCnt"
                name="carbonDioxAvCnt"
                className={classes.textField22}
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={carbonDioxAvCnt}
              />   
              <CustomizedTextField
                label={GetText("sprinkler_h")}
                id="sprinklerHCnt"
                name="sprinklerHCnt"
                className={classes.textField22}
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={sprinklerHCnt}
              />   
              <CustomizedTextField
                label={GetText("sprinkler_av")}
                id="sprinklerAvCnt"
                name="sprinklerAvCnt"
                className={classes.textField22}
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={sprinklerAvCnt}
              />   
              <CustomizedTextField
                label={GetText("powder_exting_h")}
                id="powderExtingHCnt"
                name="powderExtingHCnt"
                className={classes.textField22}
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={powderExtingHCnt}
              />   
              <CustomizedTextField
                label={GetText("powder_exting_av")}
                id="powderExtingAvCnt"
                name="powderExtingAvCnt"
                className={classes.textField22}
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={powderExtingAvCnt}
              /> 
              <CustomizedTextField
                label={GetText("spray_exting_h")}
                id="sprayExtingHCnt"
                name="sprayExtingHCnt"
                className={classes.textField22}
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={sprayExtingHCnt}
              />   
              <CustomizedTextField
                label={GetText("spray_exting_av")}
                id="sprayExtingAvCnt"
                name="sprayExtingAvCnt"
                className={classes.textField22}
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={sprayExtingAvCnt}
              />   
              <CustomizedTextField
                label={GetText("po_exting_h")}
                id="poExtingHCnt"
                name="poExtingHCnt"
                className={classes.textField22}
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={poExtingHCnt}
              />   
              <CustomizedTextField
                label={GetText("po_exting_av")}
                id="poExtingAvCnt"
                name="poExtingAvCnt"
                className={classes.textField22}
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={poExtingAvCnt}
              />   
              <CustomizedTextField
                label={GetText("halogen_comp_h")}
                id="halogenCompHCnt"
                name="halogenCompHCnt"
                className={classes.textField22}
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={halogenCompHCnt}
              />   
              <CustomizedTextField
                label={GetText("halogen_comp_av")}
                id="harlogenCompAvCnt"
                name="harlogenCompAvCnt"
                className={classes.textField22}
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={harlogenCompAvCnt}
              />   
              <CustomizedTextField
                label={GetText("exting_pump")}
                id="extingPumpCnt"
                name="extingPumpCnt"
                className={classes.textField22}
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={extingPumpCnt}
              />  
              <CustomizedTextField
                label={GetText("exting")}
                id="extingCnt"
                name="extingCnt"
                className={classes.textField22}
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={extingCnt}
              />  
              <CustomizedTextField
                label={GetText("simplcty_exting")}
                id="simplctyExtingCnt"
                name="simplctyExtingCnt"
                className={classes.textField22}
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={simplctyExtingCnt}
              />  
              <CustomizedTextField
                label={GetText("water_exting")}
                id="waterExtingCnt"
                name="waterExtingCnt"
                className={classes.textField22}
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={waterExtingCnt}
              />
              <CustomizedTextField
                label={GetText("exting_water")}
                id="extingWaterCnt"
                name="extingWaterCnt"
                className={classes.textField22}
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={extingWaterCnt}
              />
              <CustomizedTextField
                label={GetText("lwtr")}
                id="lwtrCnt"
                name="lwtrCnt"
                className={classes.textField22}
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={lwtrCnt}
              />
              <CustomizedTextField
                label={GetText("hyd_equip_etc")}
                id="hydEquipEtcCnt"
                name="hydEquipEtcCnt"
                className={classes.textField22}
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={hydEquipEtcCnt}
              />
              <CustomizedTextField
                label={GetText("lkge_waring")}
                id="lkgeWaringCnt"
                name="lkgeWaringCnt"
                className={classes.textField22}
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={lkgeWaringCnt}
              />
              <CustomizedTextField
                label={GetText("auto_fire_news_f")}
                id="autoFireNewsfCnt"
                name="autoFireNewsfCnt"
                className={classes.textField22}
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={autoFireNewsfCnt}
              />
              <CustomizedTextField
                label={GetText("gas_lkge_waring")}
                id="gasLkgeWaringCnt"
                name="gasLkgeWaringCnt"
                className={classes.textField22}
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={gasLkgeWaringCnt}
              />
              <CustomizedTextField
                label={GetText("curtain")}
                id="curtainCnt"
                name="curtainCnt"
                className={classes.textField22}
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={curtainCnt}
              />
              <CustomizedTextField
                label={GetText("carssette")}
                id="carssetteCnt"
                name="carssetteCnt"
                className={classes.textField22}
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={carssetteCnt}
              />
              <CustomizedTextField
                label={GetText("resist_etc")}
                id="resistEtcCnt"
                name="resistEtcCnt"
                className={classes.textField22}
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={resistEtcCnt}
              />
            </div>
          </Grid>
        </Grid>
      </Paper>   
      <div className={classes.paper}>
        <FormLabel component="legend">{GetText("evacuation_facility")}</FormLabel>
      </div>
      <Paper className={classes.control}>
        <Grid container spacing={1}>
          <Grid item xs={12}>
            <div className={classes.paper}>
              <CustomizedTextField
                label={GetText("slide")}
                id="slideCnt"
                name="slideCnt"
                className={classes.textField22}
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={slideCnt}
              />
              <CustomizedTextField
                label={GetText("ladder")}
                id="ladderCnt"
                name="ladderCnt"
                className={classes.textField22}
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={ladderCnt}
              />
              <CustomizedTextField
                label={GetText("rescue_equipment")}
                id="rescueCnt"
                name="rescueCnt"
                className={classes.textField22}
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={rescueCnt}
              />
              <CustomizedTextField
                label={GetText("desc_slow_device")}
                id="descSlowDeviceCnt"
                name="descSlowDeviceCnt"
                className={classes.textField22}
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={descSlowDeviceCnt}
              />
              <CustomizedTextField
                label={GetText("measure_equip")}
                id="measureEquipCnt"
                name="measureEquipCnt"
                className={classes.textField22}
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={measureEquipCnt}
              />
              <CustomizedTextField
                label={GetText("measure_rope")}
                id="measureRopeCnt"
                name="measureRopeCnt"
                className={classes.textField22}
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={measureRopeCnt}
              />
              <CustomizedTextField
                label={GetText("safe_mat")}
                id="safeMatCnt"
                name="safeMatCnt"
                className={classes.textField22}
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={safeMatCnt}
              />
              <CustomizedTextField
                label={GetText("rescue_equip")}
                id="rescueEquipCnt"
                name="rescueEquipCnt"
                className={classes.textField22}
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={rescueEquipCnt}
              />
              <CustomizedTextField
                label={GetText("emg_light")}
                id="emgLightCnt"
                name="emgLightCnt"
                className={classes.textField22}
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={emgLightCnt}
              />
            </div>
          </Grid>
        </Grid>
      </Paper>
      <div className={classes.paper}>
        <FormLabel component="legend">{GetText("equip_for_fire_fighting")}</FormLabel>
      </div>
      <Paper className={classes.control}>
        <Grid container spacing={1}>
          <Grid item xs={12}>
            <div className={classes.paper}>
              <CustomizedTextField
                label={GetText("resmoke")}
                id="resmokeCnt"
                name="resmokeCnt"
                className={classes.textField22}
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={resmokeCnt}
              />
              <CustomizedTextField
                label={GetText("wtr_pipe")}
                id="wtrpipeCnt"
                name="wtrpipeCnt"
                className={classes.textField22}
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={wtrpipeCnt}
              />
              <CustomizedTextField
                label={GetText("emg_brodc")}
                id="emgBrodcCnt"
                name="emgBrodcCnt"
                className={classes.textField22}
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={emgBrodcCnt}
              />
              <CustomizedTextField
                label={GetText("auto_fire_find_sens")}
                id="autoFireFindSensCnt"
                name="autoFireFindSensCnt"
                className={classes.textField22}
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={autoFireFindSensCnt}
              />
              <CustomizedTextField
                label={GetText("auto_fire_find_circuit")}
                id="autoFireFindCircuitCnt"
                name="autoFireFindCircuitCnt"
                className={classes.textField22}
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={autoFireFindCircuitCnt}
              />
              <CustomizedTextField
                label={GetText("water_springkling")}
                id="waterSpringklingCnt"
                name="waterSpringklingCnt"
                className={classes.textField22}
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={waterSpringklingCnt}
              />
              <CustomizedTextField
                label={GetText("emg_waring")}
                id="emgWaringCnt"
                name="emgWaringCnt"
                className={classes.textField22}
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={emgWaringCnt}
              />
              <CustomizedTextField
                label={GetText("emg_plug")}
                id="emgPlugCnt"
                name="emgPlugCnt"
                className={classes.textField22}
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={emgPlugCnt}
              />
              <CustomizedTextField
                label={GetText("wireless_comm")}
                id="wirelessCommCnt"
                name="wirelessCommCnt"
                className={classes.textField22}
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={wirelessCommCnt}
              />
              <CustomizedTextField
                label={GetText("induce_light")}
                id="induceLightCnt"
                name="induceLightCnt"
                className={classes.textField22}
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={induceLightCnt}
              />
              <CustomizedTextField
                label={GetText("induce_singpost")}
                id="induceSignpostCnt"
                name="induceSignpostCnt"
                className={classes.textField22}
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={induceSignpostCnt}
              />
            </div>
          </Grid>
        </Grid>
      </Paper>
      
      <Box p={1}></Box>
      <Divider style={{'width':'100%','position': 'absolute', 'left': 0}} />
      <ActionPanel>
        <Button 
          disabled={loading}
          color={userContext.preference.color}
          type="submit"
        >
          { operateType !== "" ? GetText("save") : GetText("new") }
        </Button>
        { 
          inputs.objId && bldgSeq && storySeq && operateType !== dictionary.NEW &&
            <Button 
              disabled={loading}
              color={userContext.preference.color}
              name="update"
              onClick={onChangeUpdateState}
            >
              { isUpdateMode ? GetText("cancel") : GetText("update") }
            </Button>
        }
        
        {
          inputs.objId && bldgSeq && storySeq &&
            <Button 
              disabled={loading}
              color={userContext.preference.color}
              name="delete"
              onClick={onDelete}
          >
              {GetText("delete")}
            </Button> 
        }
        
        {loading && <CircularProgress size={24} className={classes.buttonProgress} />}
      </ActionPanel>
      </form>
      <CustomizedSnackbar isOpen={openSnackbar} severity={severity} message={message} closeHandler={onCloseSnackbar} />
      <Confirm open={isConfirmOpen} MessageType={messageType} MessageId={messageId} handleClose={handleConfirmDialog} />
    </React.Fragment>
  );
}