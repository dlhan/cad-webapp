import React, { useState, useEffect } from 'react';
import { GetText } from '../../common/BundleManager';
import serviceList from '../../service/ServiceList';
import { RunService } from '../../service/RestApi';
import Dictionary from '../../common/Dictionary';
import { CheckResultFromService, ResetForResponseData, ConvertItemsToComboType } from '../../common/Utility';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import ActionPanel from '../../components/ActionPanel';
import { FormStyle } from '../../style/Style';
import CustomizedSwitch from '../../components/Switch';
import { useUserContext } from '../../common/UserContext';
import CustomizedSnackbar from '../../components/Snackbar';
import CircularProgress from '@material-ui/core/CircularProgress';
import FormLabel from '@material-ui/core/FormLabel';
import CodeComboBox from '../../components/CodeComboBox';
import ViewInfo from '../../common/ViewInfo';
import { useForm } from "react-hook-form";
import Confirm from '../../components/Confirm';
import CustomizedTextField from '../../components/TextField';
import CustomizedSelect from '../../components/Select';

const useStyles = FormStyle;
let originalInputs = {};
let isModified = false;

export default function BuildingHistoryDetail(props) {
  const classes = useStyles();
  var dictionary = new Dictionary();
  const userContext = useUserContext();
  const s = new serviceList();
  var viewInfo = new ViewInfo();
  let [buildingList, setBuildingList] = useState([]);

  const [operateType, setOperateType] = useState(props.sendInfo.operateType);
  let [inputs, setInputs] = useState({
                                    objId:props.sendInfo.objId,
                                    objhistSeq:"", 
                                    bldgSeq:"",  
                                    histTypeCd:"",  
                                    constUse:"",  
                                    struct1:"",  
                                    struct2:"",  
                                    struct3:"",  
                                    floorArea:"",  
                                    totArea:"",  
                                    ustoryCnt:"",  
                                    bstoryCnt:"",  
                                    grantDate:"",  
                                    preuseYn:false,  
                                    finishDate:"",  
                                    etc:"",  
                                    chgDesc:"",  
                                    useYn:true
                                    });
  let { objhistSeq,objId,bldgSeq,histTypeCd,constUse,struct1,struct2,struct3,floorArea,totArea,ustoryCnt,bstoryCnt,grantDate,preuseYn,finishDate,etc,chgDesc,useYn  } = inputs;
  let [loading, setLoading] = useState(false);
  let [disabled, setDisabled] = useState(false);
  let [isUpdateMode, setIsUpdateMode] = useState(false);
  
/////////Setting for validate the inputs in the form.
  const { handleSubmit } = useForm();
////////////////////////////////////////////////////

  const onChange = e => {
    let {name, value} = e.target;
    
    if (name === "preuseYn") { value = !inputs.preuseYn};
        
    isModified = true;
    setInputs({
      ...inputs,
      [name]:value
    });
  }

/////////////Setting for Snackbar////////////////////////////////
  let [snackbar, setSnackbar] = useState({openSnackbar:false, severity:'', message:''});
  let { openSnackbar, severity, message } = snackbar;
    
  const onCloseSnackbar = () => {
    setSnackbar(prev => ({
      ...prev,
      openSnackbar:false,
      severity:'',
      message:''
    }));
  }
/////////////////////////////////////////////

//////Confrim event
  const [confirmMessage, setConfirmMessage] = React.useState({
                                                      isConfirmOpen : false,
                                                      messageType : "",
                                                      messageId : ""
                                                });
  let { isConfirmOpen,messageType,messageId } = confirmMessage;
  
  const handleConfirmDialog = (answer) => {
    setConfirmMessage({
      ...confirmMessage,
      isConfirmOpen:false
    });
  
    if (answer){
      onSubmit();
    }
  }
  /////////////////////////////////////////////
  
  ////Button operate
  
  const onChangeUpdateState = () => {
    setIsUpdateMode(!isUpdateMode);
    if (!isUpdateMode) {
      onUpdateMode();
    } else {
      onReadMode();
    }
  }

  const onReadMode = () => {
    setDisabled(true);
    setOperateType("");
    setIsUpdateMode(false);
    props.callFromDialog({modified: false});
  }

  const onNewMode = () => {
    setDisabled(false);
    setOperateType(dictionary.NEW);
    setInputs({ objId:props.sendInfo.objId,
                objhistSeq:"", 
                bldgSeq:"",  
                histTypeCd:"",  
                constUse:"",  
                struct1:"",  
                struct2:"",  
                struct3:"",  
                floorArea:"",  
                totArea:"",  
                ustoryCnt:"",  
                bstoryCnt:"",  
                grantDate:"",  
                preuseYn:false,  
                finishDate:"",  
                etc:"",  
                chgDesc:"",  
                useYn:true
            });
    props.callFromDialog({operateType:dictionary.NEW, modified: false});
    originalInputs = {
                objId:props.sendInfo.objId,
                objhistSeq:"", 
                bldgSeq:"",  
                histTypeCd:"",  
                constUse:"",  
                struct1:"",  
                struct2:"",  
                struct3:"",  
                floorArea:"",  
                totArea:"",  
                ustoryCnt:"",  
                bstoryCnt:"",  
                grantDate:"",  
                preuseYn:false,  
                finishDate:"",  
                etc:"",  
                chgDesc:"",  
                useYn:true
                  };
    isModified = false;
  }

  const onUpdateMode = () => {
    setDisabled(false);
    setOperateType(dictionary.UPDATE);
    props.callFromDialog({operateType:dictionary.UPDATE, modified: false});
  }

  const onConfirm = () => {
    if (operateType === dictionary.NEW) {
      setConfirmMessage({ isConfirmOpen: true, messageType: dictionary.WARNING, messageId : "msg_will_you_insert" });
    } else if (operateType === dictionary.UPDATE) {
      setConfirmMessage({ isConfirmOpen: true, messageType: dictionary.WARNING, messageId : "msg_will_you_update" });
    } else {
      onNewMode();
    }
  }

  const onDelete = () => {
    setOperateType(dictionary.DELETE);
    setConfirmMessage({ isConfirmOpen: true, messageType: dictionary.WARNING, messageId : "msg_will_you_delete" });
  }
/////////////////Check whether inputs are modified or not. If modified, it sends the state to parent//////////////////// 
  useEffect(() => {
    let Modified =  (JSON.stringify(inputs) === JSON.stringify(originalInputs)? false : true);
    
    if (operateType !== dictionary.READ) props.callFromDialog({modified: Modified && isModified});
  }, [inputs]);
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////Get data////////////////////////////////////////////
  function getInfo(id) {
    let body = {objId: props.sendInfo.objId, objhistSeq:id};
    setLoading(true);
    RunService(s.getBuildingHistoryInfo, body)
        .then(response => {
          let reponseData = ResetForResponseData(s.getBuildingHistoryInfo.response, response.data);
          setInputs(reponseData);
          originalInputs = reponseData;
          
          onReadMode();
          setLoading(false);
        })
        .catch(error => {
          setSnackbar(prev => ({
            ...prev,
            openSnackbar:true,
            severity:dictionary.MessageType.Error,
            message:GetText("msg_normal_error_message")
          }));
        })
  }
  /////////////////////////////////////////////////////////////////

  useEffect(() => {
    RunService(s.getBuildingListForComboBox,{objId:props.sendInfo.objId, locale:userContext.preference.language},'', true)
        .then(response => {
          var comboItems = ConvertItemsToComboType("bldgSeq","bldgName",ResetForResponseData(s.getBuildingListForComboBox.response,response.data,""));
          setBuildingList(comboItems);
          if (Array.isArray(comboItems) && comboItems.length > 0) {
            setInputs({
              ...inputs,
              bldgSeq:comboItems[0].value
            });
            if (props.sendInfo.objhistSeq) getInfo(props.sendInfo.objhistSeq);
          }
        })
        .catch(error => {
          setSnackbar(prev => ({
            ...prev,
            openSnackbar:true,
            severity:dictionary.MessageType.Error,
            message:GetText("msg_normal_error_message")
          }));
        })
  }, []);

  ///////////////////// Insert or Update///////////////////////////
  const onSubmit = () => {
    if (operateType === "") {
      onNewMode();
      return;
    }

    setLoading(true);
    let service;
    let body;

    switch(operateType) {
      case dictionary.NEW :
        service = s.insertBuildingHistory;
        body = inputs;
        break;
      case dictionary.UPDATE :
        service = s.updateBuildingHistory;
        body = inputs;
        break;
      case dictionary.DELETE :
        service = s.deleteBuildingHistory;
        body = [{objId:inputs.objId,objhistSeq:inputs.objhistSeq}]; 
        break;
      default :
        return;
    }
  
    RunService(service,{userId: userContext.userInfo.userId} ,body)
        .then(response => {
          setLoading(false);
          var result = CheckResultFromService(response.data);
          if (result.isSuccess) {
            setSnackbar(prev => ({
              ...prev,
              openSnackbar:true,
              severity:dictionary.MessageType.Success,
              message:GetText(result.message)
            }));
            
            setInputs({
              ...inputs,
              objhistSeq:result.id
            });
            onReadMode();
            
            if (operateType === dictionary.DELETE) {
              props.callFromDialog({operateType:dictionary.DELETE, modified: false, refresh:true});
              props.handleDialogClose(true);
            } else {
              props.callFromDialog({operateType:dictionary.READ, modified: false, refresh:true});
            }
          } else {
            setSnackbar(prev => ({
              ...prev,
              openSnackbar:true,
              severity:dictionary.MessageType.Error,
              message:GetText(result.message)
            }));
          }
        })
        .catch(error => {
          setLoading(false);
          setSnackbar(prev => ({
            ...prev,
            openSnackbar:true,
            severity:dictionary.MessageType.Error,
            message:GetText("msg_normal_error_message")
          }));
        })
  } 
  /////////////////////////////////////////////////////////////////

  const bldgComboChangeHandler =(e)=>{
    let {name, value} = e.target;
    setInputs({
      ...inputs,
      bldgSeq:value
    });
  }

  return (
    <React.Fragment>
      <form onSubmit={handleSubmit(onConfirm)}>
      <div className={classes.paper}>
        <FormLabel component="legend">{GetText("building_histor_detail_info")}</FormLabel>
      </div>
      <Paper className={classes.control}>
        <Grid container spacing={1}>
          <Grid item xs={12}>
            <div className={classes.paper}>
              <CustomizedSelect 
                id="bldgSeq" 
                disabled={disabled}
                label={GetText("building_name")} 
                onChange={bldgComboChangeHandler}
                selectedItemId={bldgSeq}
                visibleFirstOption={false} 
                items={buildingList} 
              />
              <CodeComboBox
                groupCode={'165'}
                disabled={disabled}
                className={classes.textField25}
                childLabel={GetText("building_history_type")}
                childComboId={'histTypeCd'}
                callEvent={onChange}
                childOnly={true}
                selectedCode={histTypeCd}
              />
              <CustomizedTextField
                label={GetText("usage")}
                id="constUse"
                name="constUse"
                onChange={onChange}
                disabled={disabled}
                value={constUse}
              />    
              <CustomizedSwitch 
                id='preuseYn'
                name='preuseYn' 
                checked={preuseYn}
                onChange={onChange}
                disabled={disabled}
                value={preuseYn}
                label={GetText("is_representative_building")} 
              />
            </div>
          </Grid>
          <Grid item xs={12}>
            <div className={classes.paper}>
              <CodeComboBox
                groupCode={'070'}
                disabled={disabled}
                className={classes.textField25}
                childLabel={GetText("building_structure_type")}
                childComboId={'struct1'}
                callEvent={onChange}
                childOnly={true}
                selectedCode={struct1}
              />
              <CodeComboBox
                groupCode={'071'}
                disabled={disabled}
                className={classes.textField25}
                childLabel={GetText("building_structure_material_type")}
                childComboId={'struct2'}
                callEvent={onChange}
                childOnly={true}
                selectedCode={struct2}
              />
              <CodeComboBox
                groupCode={'072'}
                disabled={disabled}
                className={classes.textField25}
                childLabel={GetText("building_roof_type")}
                childComboId={'struct3'}
                callEvent={onChange}
                childOnly={true}
                selectedCode={struct3}
              />
              <CustomizedTextField
                label={GetText("underground_floors_number")}
                id="bstoryCnt"
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={bstoryCnt}
                endAdornment={GetText("floor")}
              />
              <CustomizedTextField
                label={GetText("ground_floors_number")}
                id="ustoryCnt"
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={ustoryCnt}
                endAdornment={GetText("floor")}
              />
            </div>
          </Grid>
          <Grid item xs={12}>
            <div className={classes.paper}>
              <CustomizedTextField
                label={GetText("floor_area")}
                id="floorArea"
                type="float"
                onChange={onChange}
                disabled={disabled}
                value={floorArea}
                belowDotLength="2"
              />
              <CustomizedTextField
                label={GetText("total_area")}
                id="totArea"
                type="float"
                onChange={onChange}
                disabled={disabled}
                value={totArea}
                belowDotLength="2"
              />
              <CustomizedTextField
                label={GetText("approval_use_date")}
                id="grantDate"
                type="date"
                onChange={onChange}
                disabled={disabled}
                value={grantDate}
              />
              <CustomizedTextField
                label={GetText("approval_use_date")}
                id="finishDate"
                type="date"
                onChange={onChange}
                disabled={disabled}
                value={finishDate}
              />
            </div>
          </Grid>
          <Grid item xs={12}>
            <div className={classes.paper}>
              <CustomizedTextField
                id="chgDesc"
                label={GetText("modified_note")}
                className={classes.textField3}
                multiline={true}
                rows={2}
                variant="outlined"
                onChange={onChange}
                disabled={disabled}
                value={chgDesc}
              />
              <CustomizedTextField
                id="etc"
                label={GetText("remark")}
                className={classes.textField3}
                multiline={true}
                rows={2}
                variant="outlined"
                onChange={onChange}
                disabled={disabled}
                value={etc}
              />
            </div>
          </Grid>
        </Grid>
      </Paper>   
      <Box p={1}></Box>
      <Divider style={{'width':'100%','position': 'absolute', 'left': 0}} />
      <ActionPanel>
        <Button 
          disabled={loading}
          color={userContext.preference.color}
          type="submit"
        >
          { operateType !== "" ? GetText("save") : GetText("new") }
        </Button>
        { 
          inputs.objId && inputs.objhistSeq && operateType !== dictionary.NEW &&
            <Button 
              disabled={loading}
              color={userContext.preference.color}
              name="update"
              onClick={onChangeUpdateState}
            >
              { isUpdateMode ? GetText("cancel") : GetText("update") }
            </Button>
        }
        
        {
          inputs.objId && inputs.objhistSeq && 
            <Button 
              disabled={loading}
              color={userContext.preference.color}
              name="delete"
              onClick={onDelete}
          >
              {GetText("delete")}
            </Button> 
        }
        
        {loading && <CircularProgress size={24} className={classes.buttonProgress} />}
      </ActionPanel>
      </form>
      <CustomizedSnackbar isOpen={openSnackbar} severity={severity} message={message} closeHandler={onCloseSnackbar} />
      <Confirm open={isConfirmOpen} MessageType={messageType} MessageId={messageId} handleClose={handleConfirmDialog} />
    </React.Fragment>
  );
}