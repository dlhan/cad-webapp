import React, { useState, useEffect } from 'react';
import { GetText } from '../../common/BundleManager'
import serviceList from '../../service/ServiceList';
import CircularProgress from '@material-ui/core/CircularProgress';
import CustomizedSnackbar from '../../components/Snackbar';
import Dictionary from '../../common/Dictionary';
import { RunService } from '../../service/RestApi';
import { ResetForResponseData, CheckMenuAuth } from '../../common/Utility';
import StickyHeadTable from '../../components/Table';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import AddIcon from '@material-ui/icons/Add';
import Fab from '@material-ui/core/Fab';
import CustomizedDialog from '../../components/Dialog';
import { FormStyle } from '../../style/Style';
import ViewInfo from '../../common/ViewInfo';
import { useUserContext } from '../../common/UserContext';
import Confirm from '../../components/Confirm';

const useStyles = FormStyle;

const columns = [
  { id: 'objId', numeric: false, disablePadding: false, label: '', align: 'center', minWidth: 0 },
  { id: 'fmgrSeq', numeric: false, disablePadding: false, label: '', align: 'center', minWidth: 0 },
  { id: 'name', numeric: false, disablePadding: false, label: 'name', align: 'center', minWidth: 150 },
  { id: 'birthday', numeric: false, disablePadding: false, label: 'birth_day', align: 'center', minWidth: 150 },
  { id: 'title', numeric: false, disablePadding: false, label: 'job_position', align: 'center', minWidth: 150 },
  { id: 'address', numeric: false, disablePadding: false, label: 'address', align: 'center', minWidth: 150 },  
  { id: 'telNum', numeric: false, disablePadding: false, label: 'tel_num', align: 'center', minWidth: 220 },
  { id: 'pubFmngYn', numeric: false, disablePadding: false, label: 'is_agency_for_management', align: 'center', minWidth: 100 },
  { id: 'mgeAgencyYn', numeric: false, disablePadding: false, label: 'is_joint_fire_management', align: 'center', minWidth: 100 }
];

export default function ArsonManagerList(props) {
  const classes = useStyles();
  var s = new serviceList();
  var dictionary = new Dictionary();
  var viewInfo = new ViewInfo();
  var view = viewInfo.arsonManagerList;
  const userContext = useUserContext();

  let [loading, setLoading] = useState(false);
  const hasFireObjectCreateAuth = CheckMenuAuth(["IS002"]);

  ////////Setting for Table//////////////////////////////////////////
  const onSelectRow = (rowId) => {
    handleDialogOpen(dictionary.READ, rowId)
  }
  
  const [rows, setRows] = useState([]);
  
///////////////////////////////////////////////////////////////////

  const getData = () => {
    setLoading(true);
    
    RunService(s.getArsonManagerList,{objId:props.objId},'', true)
        .then(response => {
          setLoading(false);
          setRows(ResetForResponseData(s.getArsonManagerList.response,response.data,"fmgrSeq"));
        })
        .catch(error => {
          setLoading(false);
          setSnackbar(prev => ({
            ...prev,
            openSnackbar:true,
            severity:dictionary.MessageType.Error,
            message:GetText("msg_normal_error_message")
          }));
        })
  }

  useEffect(() => {
    getData();
  }, []);
  
//////Dialog event
  const [isDialogOpen, setIsDialogOpen] = useState(false);
  const [isModified, setIsModified] = useState(false);
  
  const callFromDialog = (p) => {
    setIsModified(p.modified);
    if (p.refresh) {
      getData();
    }
  }

  const [sendInfo, setSendInfo] = useState({operateType:'', objId:props.objId, fmgrSeq:''});
  const { operateType, objId, fmgrSeq } = sendInfo;

  const handleDialogClose = (forceNoMessage) => {
    let forceClose = false;

    if (typeof forceNoMessage === 'boolean') forceClose = forceNoMessage;

    if (isModified && !forceClose) {
      setConfirmMessage({
        isConfirmOpen: true,
        messageType: dictionary.WARNING,
        messageId: "msg_confirm_close_dialog" 
      });
    } else {
      setIsDialogOpen(false);
    }
  };
  
  const handleDialogOpen = (operateType, fmgrSeq) => {
    setSendInfo({...sendInfo, operateType:operateType, objId:props.objId, fmgrSeq:fmgrSeq});
    setIsDialogOpen(true);
  };
////////////////////////////////////////////////////////

//////Confrim event in closing the dialog///////////////
  const [confirmMessage, setConfirmMessage] = useState({
    isConfirmOpen : false,
    messageType : "",
    messageId : ""
  });
  let { isConfirmOpen,messageType,messageId } = confirmMessage;

  const handleConfirmDialog = (answer) => {
    setConfirmMessage({
      ...confirmMessage,
      isConfirmOpen:false
    });

    if (answer){
      setIsDialogOpen(false);
    }
  }
////////////////////////////////////////////////////////

/////////////Setting for Snackbar////////////////////////////////
  let [snackbar, setSnackbar] = useState({openSnackbar:false, severity:'', message:''});
  let { openSnackbar, severity, message } = snackbar;
    
  const onCloseSnackbar = () => {
    setSnackbar(prev => ({
      ...prev,
      openSnackbar:false,
      severity:'',
      message:''
    }));
  }
////////////////////////////////////////////////////////
 
  return (
    <React.Fragment>
      <Grid container spacing={1}>
        <Grid item xs={12}>
          <Paper className={classes.control}>
            <StickyHeadTable 
              tableId={"arsonManagerTable"} 
              onSelectRow={onSelectRow} 
              columns={columns} 
              rows={rows} 
              SupportCheckBox={view.table.allowCheckBox} 
              SupportPaging={view.table.allowPage}
              heightGap={200}
            />
          </Paper>
        </Grid>
        {hasFireObjectCreateAuth && <Grid item xs={12}>
            <div className={classes.centerLocate}>
              <Fab color={userContext.preference.color} aria-label="add">
                <AddIcon onClick={() => handleDialogOpen(dictionary.NEW)} />
              </Fab>
            </div>
          </Grid>
        }
        
      </Grid>
      {loading && <CircularProgress size={24} className={classes.buttonProgress} />}
      <CustomizedDialog isDialogOpen={isDialogOpen} sendInfo={sendInfo} viewInfo={viewInfo.arsonManagerDetail} handleDialogClose={handleDialogClose} callFromDialog={callFromDialog} />
      <Confirm open={isConfirmOpen} MessageType={messageType} MessageId={messageId} handleClose={handleConfirmDialog} />
      <CustomizedSnackbar isOpen={openSnackbar} severity={severity} message={message} closeHandler={onCloseSnackbar} />
     </React.Fragment>
  );
}