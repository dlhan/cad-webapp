import React, { useState, useEffect } from 'react';
import { GetText } from '../../common/BundleManager';
import serviceList from '../../service/ServiceList';
import { RunService } from '../../service/RestApi';
import Dictionary from '../../common/Dictionary';
import { CheckResultFromService, ResetForResponseData } from '../../common/Utility';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import ActionPanel from '../../components/ActionPanel';
import { FormStyle } from '../../style/Style';
import CustomizedSwitch from '../../components/Switch';
import { useUserContext } from '../../common/UserContext';
import CustomizedSnackbar from '../../components/Snackbar';
import CircularProgress from '@material-ui/core/CircularProgress';
import FormLabel from '@material-ui/core/FormLabel';
import CodeComboBox from '../../components/CodeComboBox';
import ViewInfo from '../../common/ViewInfo';
import { useForm } from "react-hook-form";
import Confirm from '../../components/Confirm';
import CustomizedTextField from '../../components/TextField';

const useStyles = FormStyle;
let originalInputs = {};
let isModified = false;

export default function BuildingInfoDetail(props) {
  const classes = useStyles();
  var dictionary = new Dictionary();
  const userContext = useUserContext();
  const s = new serviceList();
  var viewInfo = new ViewInfo();

  const [operateType, setOperateType] = useState(props.sendInfo.operateType);
  let [inputs, setInputs] = useState({objId:props.sendInfo.objId
                                    ,bldgSeq:''
                                    ,bldgName:''
                                    ,struct1:''
                                    ,struct2:''
                                    ,struct3:''
                                    ,ustoryCnt:''
                                    ,bstoryCnt:''
                                    ,floorArea:''
                                    ,totArea:''
                                    ,sescStairCnt:''
                                    ,lotArea:''
                                    ,escStairCnt:''
                                    ,comStairCnt:''
                                    ,outStairCnt:''
                                    ,inclineCnt:''
                                    ,emgliftCnt:''
                                    ,ecalCnt:''
                                    ,exitCnt:''
                                    ,rootYn:''
                                    ,homeCnt:''
                                    ,useYn:true
                                    ,rpsnBldgYn:true
                                    ,houseCnt:''
                                    ,entrpsCnt:''
                                    ,inhbtntCnt:''
                                    ,bldgHeight:''
                                    ,elvtrCnt:''
                                    ,removeDate:''
                                    ,removeYn:false
                                    ,bldgMainUseCd:''
                                    ,bldgSubUseCd:''
                                    ,useConfmDate:''
                                    ,useYn:''
                                    });
  let { objId,bldgSeq,bldgName,struct1,struct2,struct3,ustoryCnt,bstoryCnt,floorArea,totArea,sescStairCnt,lotArea,escStairCnt,comStairCnt,outStairCnt,inclineCnt,emgliftCnt,ecalCnt,exitCnt,rootYn,homeCnt,useYn,rpsnBldgYn,houseCnt,entrpsCnt,inhbtntCnt,bldgHeight,elvtrCnt,removeDate,removeYn,bldgMainUseCd,bldgSubUseCd,useConfmDate } = inputs;
  let [loading, setLoading] = useState(false);
  let [disabled, setDisabled] = useState(false);
  let [isUpdateMode, setIsUpdateMode] = useState(false);
    
/////////Setting for validate the inputs in the form.
  const { handleSubmit } = useForm();
////////////////////////////////////////////////////

  const onChange = e => {
    let {name, value} = e.target;
    
    if (name === "rpsnBldgYn") { value = !inputs.rpsnBldgYn};
    if (name === "removeYn") { value = !inputs.removeYn};
    if (name === "rootYn") { value = !inputs.rootYn};
        
    isModified = true;
    setInputs({
      ...inputs,
      [name]:value
    });
  }

/////////////Setting for Snackbar////////////////////////////////
  let [snackbar, setSnackbar] = useState({openSnackbar:false, severity:'', message:''});
  let { openSnackbar, severity, message } = snackbar;
    
  const onCloseSnackbar = () => {
    setSnackbar(prev => ({
      ...prev,
      openSnackbar:false,
      severity:'',
      message:''
    }));
  }
/////////////////////////////////////////////

//////Confrim event
  const [confirmMessage, setConfirmMessage] = React.useState({
                                                      isConfirmOpen : false,
                                                      messageType : "",
                                                      messageId : ""
                                                });
  let { isConfirmOpen,messageType,messageId } = confirmMessage;
  
  const handleConfirmDialog = (answer) => {
    setConfirmMessage({
      ...confirmMessage,
      isConfirmOpen:false
    });
  
    if (answer){
      onSubmit();
    }
  }
  /////////////////////////////////////////////
  
  ////Button operate
  
  const onChangeUpdateState = () => {
    setIsUpdateMode(!isUpdateMode);
    if (!isUpdateMode) {
      onUpdateMode();
    } else {
      onReadMode();
    }
  }

  const onReadMode = () => {
    setDisabled(true);
    setOperateType("");
    setIsUpdateMode(false);
    props.callFromDialog({modified: false});
  }

  const onNewMode = () => {
    setDisabled(false);
    setOperateType(dictionary.NEW);
    setInputs({ objId:props.sendInfo.objId
                    ,bldgSeq:''
                    ,bldgName:''
                    ,struct1:''
                    ,struct2:''
                    ,struct3:''
                    ,ustoryCnt:''
                    ,bstoryCnt:''
                    ,floorArea:''
                    ,totArea:''
                    ,sescStairCnt:''
                    ,lotArea:''
                    ,escStairCnt:''
                    ,comStairCnt:''
                    ,outStairCnt:''
                    ,inclineCnt:''
                    ,emgliftCnt:''
                    ,ecalCnt:''
                    ,exitCnt:''
                    ,rootYn:''
                    ,homeCnt:''
                    ,useYn:true
                    ,rpsnBldgYn:true
                    ,houseCnt:''
                    ,entrpsCnt:''
                    ,inhbtntCnt:''
                    ,bldgHeight:''
                    ,elvtrCnt:''
                    ,removeDate:''
                    ,removeYn:false
                    ,bldgMainUseCd:''
                    ,bldgSubUseCd:''
                    ,useConfmDate:''
                    ,useYn:''
            });
    props.callFromDialog({operateType:dictionary.NEW, modified: false});
    originalInputs = {
                    objId:props.sendInfo.objId
                    ,bldgSeq:''
                    ,bldgName:''
                    ,struct1:''
                    ,struct2:''
                    ,struct3:''
                    ,ustoryCnt:''
                    ,bstoryCnt:''
                    ,floorArea:''
                    ,totArea:''
                    ,sescStairCnt:''
                    ,lotArea:''
                    ,escStairCnt:''
                    ,comStairCnt:''
                    ,outStairCnt:''
                    ,inclineCnt:''
                    ,emgliftCnt:''
                    ,ecalCnt:''
                    ,exitCnt:''
                    ,rootYn:''
                    ,homeCnt:''
                    ,useYn:true
                    ,rpsnBldgYn:true
                    ,houseCnt:''
                    ,entrpsCnt:''
                    ,inhbtntCnt:''
                    ,bldgHeight:''
                    ,elvtrCnt:''
                    ,removeDate:''
                    ,removeYn:false
                    ,bldgMainUseCd:''
                    ,bldgSubUseCd:''
                    ,useConfmDate:''
                    ,useYn:''
                  };
    isModified = false;
  }

  const onUpdateMode = () => {
    setDisabled(false);
    setOperateType(dictionary.UPDATE);
    props.callFromDialog({operateType:dictionary.UPDATE, modified: false});
  }

  const onConfirm = () => {
    if (operateType === dictionary.NEW) {
      setConfirmMessage({ isConfirmOpen: true, messageType: dictionary.WARNING, messageId : "msg_will_you_insert" });
    } else if (operateType === dictionary.UPDATE) {
      setConfirmMessage({ isConfirmOpen: true, messageType: dictionary.WARNING, messageId : "msg_will_you_update" });
    } else {
      onNewMode();
    }
  }

  const onDelete = () => {
    setOperateType(dictionary.DELETE);
    setConfirmMessage({ isConfirmOpen: true, messageType: dictionary.WARNING, messageId : "msg_will_you_delete" });
  }
/////////////////Check whether inputs are modified or not. If modified, it sends the state to parent//////////////////// 
  useEffect(() => {
    let Modified =  (JSON.stringify(inputs) === JSON.stringify(originalInputs)? false : true);
    
    if (operateType !== dictionary.READ) props.callFromDialog({modified: Modified && isModified});
  }, [inputs]);
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////Get data////////////////////////////////////////////
  useEffect(() => {
    let completed = false; 
    
    async function getInfo(id) {
      let body = {objId: props.sendInfo.objId,bldgSeq:id};
      setLoading(true);
      RunService(s.getBuildingInfo, body)
          .then(response => {
            let reponseData = ResetForResponseData(s.getBuildingInfo.response, response.data);
            setInputs(reponseData);
            originalInputs = reponseData;
            
            onReadMode();
            setLoading(false);
          })
          .catch(error => {
            setSnackbar(prev => ({
              ...prev,
              openSnackbar:true,
              severity:dictionary.MessageType.Error,
              message:GetText("msg_normal_error_message")
            }));
          })
    }

    if (props.sendInfo.bldgSeq) getInfo(props.sendInfo.bldgSeq);
  }, []);
  /////////////////////////////////////////////////////////////////

  ///////////////////// Insert or Update///////////////////////////
  const onSubmit = () => {
    if (operateType === "") {
      onNewMode();
      return;
    }

    setLoading(true);
    let service;
    let body;

    switch(operateType) {
      case dictionary.NEW :
        service = s.insertBuilding;
        body = inputs;
        break;
      case dictionary.UPDATE :
        service = s.updateBuilding;
        body = inputs;
        break;
      case dictionary.DELETE :
        service = s.deleteBuilding;
        body = [inputs.objId]; 
        break;
      default :
        return;
    }
  
    RunService(service,{userId: userContext.userInfo.userId} ,body)
        .then(response => {
          setLoading(false);
          var result = CheckResultFromService(response.data);
          if (result.isSuccess) {
            setSnackbar(prev => ({
              ...prev,
              openSnackbar:true,
              severity:dictionary.MessageType.Success,
              message:GetText(result.message)
            }));
            
            setInputs({
              ...inputs,
              bldgSeq:result.id
            });
            onReadMode();
            
            if (operateType === dictionary.DELETE) {
              props.callFromDialog({operateType:dictionary.DELETE, modified: false, refresh:true});
              props.handleDialogClose(true);
            } else {
              props.callFromDialog({operateType:dictionary.READ, modified: false, refresh:true});
            }
          } else {
            setSnackbar(prev => ({
              ...prev,
              openSnackbar:true,
              severity:dictionary.MessageType.Error,
              message:GetText(result.message)
            }));
          }
        })
        .catch(error => {
          setLoading(false);
          setSnackbar(prev => ({
            ...prev,
            openSnackbar:true,
            severity:dictionary.MessageType.Error,
            message:GetText("msg_normal_error_message")
          }));
        })
  } 
  /////////////////////////////////////////////////////////////////

  return (
    <React.Fragment>
      <form onSubmit={handleSubmit(onConfirm)}>
      <div className={classes.paper}>
        <FormLabel component="legend">{GetText("basic_information")}</FormLabel>
      </div>
      <Paper className={classes.control}>
        <Grid container spacing={1}>
          <Grid item xs={12}>
            <div className={classes.paper}>
              <CustomizedTextField
                label={GetText("bldgName")}
                id="bldgName"
                name="bldgName"
                onChange={onChange}
                disabled={disabled}
                value={bldgName}
                required
              />    
            </div>
          </Grid>
          <Grid item xs={12}>
            <div className={classes.paper}>
              <CodeComboBox
                groupCode={'070'}
                disabled={disabled}
                className={classes.textField25}
                childLabel={GetText("building_structure_type")}
                childComboId={'struct1'}
                callEvent={onChange}
                childOnly={true}
                selectedCode={struct1}
              />
              <CodeComboBox
                groupCode={'071'}
                disabled={disabled}
                className={classes.textField25}
                childLabel={GetText("building_structure_material_type")}
                childComboId={'struct2'}
                callEvent={onChange}
                childOnly={true}
                selectedCode={struct2}
              />
              <CodeComboBox
                groupCode={'072'}
                disabled={disabled}
                className={classes.textField25}
                childLabel={GetText("building_roof_type")}
                childComboId={'struct3'}
                callEvent={onChange}
                childOnly={true}
                selectedCode={struct3}
              />
              <CustomizedTextField
                label={GetText("underground_floors_number")}
                id="bstoryCnt"
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={bstoryCnt}
                endAdornment={GetText("floor")}
              />
              <CustomizedTextField
                label={GetText("ground_floors_number")}
                id="ustoryCnt"
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={ustoryCnt}
                endAdornment={GetText("floor")}
              />
            </div>
          </Grid>
          <Grid item xs={12}>
            <div className={classes.paper}>
              <CustomizedTextField
                label={GetText("site_area")}
                id="lotArea"
                type="float"
                onChange={onChange}
                disabled={disabled}
                value={lotArea}
                belowDotLength="2"
              />
              <CustomizedTextField
                label={GetText("floor_area")}
                id="floorArea"
                type="float"
                onChange={onChange}
                disabled={disabled}
                value={floorArea}
                belowDotLength="2"
              />
              <CustomizedTextField
                label={GetText("total_area")}
                id="totArea"
                type="float"
                onChange={onChange}
                disabled={disabled}
                value={totArea}
                belowDotLength="2"
              />
              <CustomizedTextField
                label={GetText("building_height")}
                id="bldgHeight"
                type="float"
                onChange={onChange}
                disabled={disabled}
                value={bldgHeight}
                belowDotLength="2"
              />
            </div>
          </Grid>
          <Grid item xs={12}>
            <div className={classes.paper}>
              <CustomizedTextField
                label={GetText("household_number")}
                id="homeCnt"
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={homeCnt}
              />
              <CustomizedTextField
                label={GetText("home_number")}
                id="houseCnt"
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={houseCnt}
              />
              <CustomizedTextField
                label={GetText("company_number")}
                id="entrpsCnt"
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={entrpsCnt}
              />
              <CustomizedTextField
                label={GetText("inhabitants_number")}
                id="inhbtntCnt"
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={inhbtntCnt}
              />
            </div>
          </Grid>
          <Grid item xs={12}>
            <div className={classes.paper}>
              <CustomizedTextField
                label={GetText("approval_use_date")}
                id="useConfmDate"
                type="date"
                onChange={onChange}
                disabled={disabled}
                value={useConfmDate}
              />
              <CodeComboBox
                parentGroupCode={'020010000'}
                disabled={disabled}
                parentLabel={GetText("building_main_used")}
                childLabel={GetText("sub_used")}
                className={classes.textField25}
                callEvent={onChange}
                selectedParentCode={bldgMainUseCd}
                selectedCode={bldgSubUseCd}
                parentComboId='bldgMainUseCd'
                childComboId='bldgSubUseCd'
              />
              <CustomizedSwitch 
                id='rpsnBldgYn'
                name='rpsnBldgYn' 
                checked={rpsnBldgYn}
                onChange={onChange}
                disabled={disabled}
                value={rpsnBldgYn}
                label={GetText("is_representative_building")} 
              />
            </div>
          </Grid>
        </Grid>
      </Paper>   
      <div className={classes.paper}>
        <FormLabel component="legend">{GetText("building_facility_information")}</FormLabel>
      </div>
      <Paper className={classes.control}>
        <Grid container spacing={1}>
          <Grid item xs={12}>
            <div className={classes.paper}>
              <CustomizedTextField
                label={GetText("stair_number")}
                id="comStairCnt"
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={comStairCnt}
              />
              <CustomizedTextField
                label={GetText("outdoor_stair_number")}
                id="outStairCnt"
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={outStairCnt}
              />
              <CustomizedTextField
                label={GetText("ramp_number")}
                id="inclineCnt"
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={inclineCnt}
              />
              <CustomizedTextField
                label={GetText("evacuation_step_number")}
                id="escStairCnt"
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={escStairCnt}
              />
              <CustomizedTextField
                label={GetText("special_evacuation_step_number")}
                id="sescStairCnt"
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={sescStairCnt}
              />
              
            </div>
          </Grid>
          <Grid item xs={12}>
            <div className={classes.paper}>
              <CustomizedTextField
                label={GetText("emergency_exit_number")}
                id="exitCnt"
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={exitCnt}
              />
              <CustomizedTextField
                label={GetText("elevator_number")}
                id="elvtrCnt"
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={elvtrCnt}
              />
              <CustomizedTextField
                label={GetText("emergency_elevator_number")}
                id="emgliftCnt"
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={emgliftCnt}
              />
              <CustomizedTextField
                label={GetText("escalator_number")}
                id="ecalCnt"
                type="number"
                onChange={onChange}
                disabled={disabled}
                value={ecalCnt}
              />
              <CustomizedSwitch 
                id='rootYn'
                name='rpsnBldgYn' 
                checked={rootYn}
                onChange={onChange}
                disabled={disabled}
                value={rootYn}
                label={GetText("is_rooftop")} 
              />
            </div>
          </Grid>
          <Grid item xs={12}>
            <div className={classes.paper}>
              <CustomizedSwitch 
                id='removeYn'
                name='removeYn' 
                className={classes.textField12}
                checked={removeYn}
                onChange={onChange}
                disabled={disabled}
                value={removeYn}
                label={GetText("is_demolition")} 
              />
              <CustomizedTextField
                id="removeDate"
                type="date"
                onChange={onChange}
                disabled={disabled}
                value={removeDate}
              />
            </div>
          </Grid>
        </Grid>
      </Paper>
      
      <Box p={1}></Box>
      <Divider style={{'width':'100%','position': 'absolute', 'left': 0}} />
      <ActionPanel>
        <Button 
          disabled={loading}
          color={userContext.preference.color}
          type="submit"
        >
          { operateType !== "" ? GetText("save") : GetText("new") }
        </Button>
        { 
          inputs.objId && inputs.bldgSeq && operateType !== dictionary.NEW &&
            <Button 
              disabled={loading}
              color={userContext.preference.color}
              name="update"
              onClick={onChangeUpdateState}
            >
              { isUpdateMode ? GetText("cancel") : GetText("update") }
            </Button>
        }
        
        {
          inputs.objId && inputs.bldgSeq && 
            <Button 
              disabled={loading}
              color={userContext.preference.color}
              name="delete"
              onClick={onDelete}
          >
              {GetText("delete")}
            </Button> 
        }
        
        {loading && <CircularProgress size={24} className={classes.buttonProgress} />}
      </ActionPanel>
      </form>
      <CustomizedSnackbar isOpen={openSnackbar} severity={severity} message={message} closeHandler={onCloseSnackbar} />
      <Confirm open={isConfirmOpen} MessageType={messageType} MessageId={messageId} handleClose={handleConfirmDialog} />
    </React.Fragment>
  );
}