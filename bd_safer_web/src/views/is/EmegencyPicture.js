import React, { useState, useEffect } from 'react';
import { GetText } from '../../common/BundleManager';
import serviceList from '../../service/ServiceList';
import { RunService } from '../../service/RestApi';
import Dictionary from '../../common/Dictionary';
import { CheckResultFromService, ResetForResponseData } from '../../common/Utility';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import { FormStyle } from '../../style/Style';
import { useUserContext } from '../../common/UserContext';
import CustomizedSnackbar from '../../components/Snackbar';
import CircularProgress from '@material-ui/core/CircularProgress';
import ViewInfo from '../../common/ViewInfo';
import { useForm } from "react-hook-form";
import Confirm from '../../components/Confirm';
import CustomizedTextField from '../../components/TextField';
import FormLabel from '@material-ui/core/FormLabel';
import ImageUploading from "react-images-uploading";

const useStyles = FormStyle;
let originalInputs = {};
let isModified = false;

export default function EmergencyPlan(props) {
  const classes = useStyles();
  var dictionary = new Dictionary();
  const userContext = useUserContext();
  const s = new serviceList();
  var viewInfo = new ViewInfo();

  const [operateType, setOperateType] = useState(props.sendInfo.operateType);
  let [loading, setLoading] = useState(false);
  let [disabled, setDisabled] = useState(false);
  let [isUpdateMode, setIsUpdateMode] = useState(false);

  const [locationImages, setLocationImages] = React.useState([]);
  const [buildingImages, setBuildingImages] = React.useState([]);
  const [suppressionImages, setSuppressionImages] = React.useState([]);
  const maxNumber = 30;
  
/////////Setting for validate the inputs in the form.
  const { handleSubmit } = useForm();
////////////////////////////////////////////////////

/////////////Setting for Snackbar////////////////////////////////
  let [snackbar, setSnackbar] = useState({openSnackbar:false, severity:'', message:''});
  let { openSnackbar, severity, message } = snackbar;
    
  const onCloseSnackbar = () => {
    setSnackbar(prev => ({
      ...prev,
      openSnackbar:false,
      severity:'',
      message:''
    }));
  }
/////////////////////////////////////////////

//////Confrim event
  const [confirmMessage, setConfirmMessage] = React.useState({
                                                      isConfirmOpen : false,
                                                      messageType : "",
                                                      messageId : ""
                                                });
  let { isConfirmOpen,messageType,messageId } = confirmMessage;
  
  const handleConfirmDialog = (answer) => {
    setConfirmMessage({
      ...confirmMessage,
      isConfirmOpen:false
    });
  
    if (answer){
      onSubmit();
    }
  }
  /////////////////////////////////////////////
  
  ////Button operate
  
  const onChangeUpdateState = () => {
    setIsUpdateMode(!isUpdateMode);
    if (!isUpdateMode) {
      onUpdateMode();
    } else {
      onReadMode();
    }
  }

  const onReadMode = () => {
    setDisabled(true);
    setOperateType("");
    setIsUpdateMode(false);
    props.callFromDialog({modified: false});
  }

  const onNewMode = () => {
    setDisabled(false);
    setOperateType(dictionary.NEW);
    
    props.callFromDialog({modified: false});
    isModified = false;
  }

  const onUpdateMode = () => {
    setDisabled(false);
    setOperateType(dictionary.UPDATE);
    props.callFromDialog({modified: false});
  }

  const onConfirm = () => {
    if (operateType === dictionary.NEW) {
      setConfirmMessage({ isConfirmOpen: true, messageType: dictionary.WARNING, messageId : "msg_will_you_insert" });
    } else if (operateType === dictionary.UPDATE) {
      setConfirmMessage({ isConfirmOpen: true, messageType: dictionary.WARNING, messageId : "msg_will_you_update" });
    } else {
      onNewMode();
    }
  }

  const onDelete = () => {
    setOperateType(dictionary.DELETE);
    setConfirmMessage({ isConfirmOpen: true, messageType: dictionary.WARNING, messageId : "msg_will_you_delete" });
  }

  /////////////////////Get data////////////////////////////////////////////
  useEffect(() => {
    let completed = false; 
    
    async function getInfo(id) {
      let body = {objId: id};
      setLoading(true);
      RunService(s.getObjPlanInfo, body)
          .then(response => {
            if (!completed) { 
              if (response.data) {
                let reponseData = ResetForResponseData(s.getObjPlanInfo.response, response.data);
                onReadMode();
              } else {
                onNewMode();
              }
            }
            completed= true;
            setLoading(false);
          })
          .catch(error => {
            setSnackbar(prev => ({
              ...prev,
              openSnackbar:true,
              severity:dictionary.MessageType.Error,
              message:GetText("msg_normal_error_message")
            }));
          })
    }

    if (operateType === dictionary.READ && props.objId) getInfo(props.objId);
    
    return () => {
      completed = true;
    };
  }, []);
  /////////////////////////////////////////////////////////////////

  ///////////////////// Insert or Update///////////////////////////
  const onSubmit = () => {
    if (operateType === "") {
      onNewMode();
      return;
    }

    setLoading(true);
    let service;
    let body;

    switch(operateType) {
      case dictionary.NEW :
        service = s.insertObjPlan;
        body.objId = props.objId;
        break;
      case dictionary.UPDATE :
        service = s.updateObjPlan;
        break;
      case dictionary.DELETE :
        service = s.deleteObjPlan;
        break;
      default :
        return;
    }
  
    RunService(service,{userId: userContext.userInfo.userId} ,body)
        .then(response => {
          setLoading(false);
          var result = CheckResultFromService(response.data);
          if (result.isSuccess) {
            setSnackbar(prev => ({
              ...prev,
              openSnackbar:true,
              severity:dictionary.MessageType.Success,
              message:GetText(result.message)
            }));
            
            onReadMode();
          } else {
            setSnackbar(prev => ({
              ...prev,
              openSnackbar:true,
              severity:dictionary.MessageType.Error,
              message:GetText(result.message)
            }));
          }
        })
        .catch(error => {
          setLoading(false);
          setSnackbar(prev => ({
            ...prev,
            openSnackbar:true,
            severity:dictionary.MessageType.Error,
            message:GetText("msg_normal_error_message")
          }));
        })
  } 
  /////////////////////////////////////////////////////////////////

  const onLocationImageChange = (imageList, addUpdateIndex) => {
    setLocationImages(imageList);
  };

  const onBuildingImageChange = (imageList, addUpdateIndex) => {
    setBuildingImages(imageList);
  };

  const onSuppressionImageChange = (imageList, addUpdateIndex) => {
    setSuppressionImages(imageList);
  };

  return (
    <React.Fragment>
      <form onSubmit={handleSubmit(onConfirm)}>
      <div className={classes.paper}>
        <FormLabel component="legend">{GetText("building_location_picture")}</FormLabel>
      </div>
      <ImageUploading
        multiple
        value={locationImages}
        onChange={onLocationImageChange}
        maxNumber={maxNumber}
        dataURLKey="data_url"
      >
        {({
            imageList,
            onImageUpload,
            onImageRemoveAll,
            onImageRemove,
            isDragging,
            dragProps
        }) => (
                <Paper className={classes.control}>
                  <Grid container spacing={2} className={classes.root}>
                    <Grid item style={{width:"80%"}}>
                      <div>
                        {imageList.map((image, index) => (
                          <div key={index} className="image-item">
                            <img src={image.data_url} alt="" height="100" />
                          </div>
                        ))}
                      </div>
                      <div>
                        <CustomizedTextField
                          label={GetText("remark")}
                          id="comStairCnt"
                          className={classes.textFieldFullLength}
                        />
                      </div>        
                    </Grid>
                    <Grid item style={{width:"20%"}}>
                      <div>
                        <Button 
                          color={userContext.preference.color}
                          type="file"
                          name="save"
                          onClick={onImageUpload}
                          {...dragProps}
                        >
                          {GetText("add_pictures")}
                        </Button>
                      </div>
                      <div>
                        <Button 
                          color={userContext.preference.color}
                          type="file"
                          name="save"
                        >
                          {GetText("save")}
                        </Button>
                      </div>
                    </Grid>
                  </Grid>
                </Paper>
              )}
      </ImageUploading>
      
      <div className={classes.paper}>
        <FormLabel component="legend">{GetText("bulding_picture")}</FormLabel>
      </div>
      <ImageUploading
        multiple
        value={locationImages}
        onChange={onBuildingImageChange}
        maxNumber={maxNumber}
        dataURLKey="data_url"
      >
        {({
            imageList,
            onImageUpload,
            onImageRemoveAll,
            onImageRemove,
            isDragging,
            dragProps
        }) => (
                <Paper className={classes.control}>
                  <Grid container spacing={2} className={classes.root}>
                    <Grid item style={{width:"80%"}}>
                      <div>
                        {imageList.map((image, index) => (
                          <div key={index} className="image-item">
                            <img src={image.data_url} alt="" height="100" />
                          </div>
                        ))}
                      </div>
                      <div>
                        <CustomizedTextField
                          label={GetText("remark")}
                          id="comStairCnt"
                          className={classes.textFieldFullLength}
                        />
                      </div>        
                    </Grid>
                    <Grid item style={{width:"20%"}}>
                      <div>
                        <Button 
                          color={userContext.preference.color}
                          type="file"
                          name="save"
                          onClick={onImageUpload}
                          {...dragProps}
                        >
                          {GetText("add_pictures")}
                        </Button>
                      </div>
                      <div>
                        <Button 
                          color={userContext.preference.color}
                          type="file"
                          name="save"
                        >
                          {GetText("save")}
                        </Button>
                      </div>
                    </Grid>
                  </Grid>
                </Paper>
              )}
      </ImageUploading>
      <div className={classes.paper}>
        <FormLabel component="legend">{GetText("fire_suppression_operation")}</FormLabel>
      </div>
      <ImageUploading
        multiple
        value={locationImages}
        onChange={onSuppressionImageChange}
        maxNumber={maxNumber}
        dataURLKey="data_url"
      >
        {({
            imageList,
            onImageUpload,
            onImageRemoveAll,
            onImageRemove,
            isDragging,
            dragProps
        }) => (
                <Paper className={classes.control}>
                  <Grid container spacing={2} className={classes.root}>
                    <Grid item style={{width:"80%"}}>
                      <div>
                        {imageList.map((image, index) => (
                          <div key={index} className="image-item">
                            <img src={image.data_url} alt="" height="100" />
                          </div>
                        ))}
                      </div>
                      <div>
                        <CustomizedTextField
                          label={GetText("remark")}
                          id="comStairCnt"
                          className={classes.textFieldFullLength}
                        />
                      </div>        
                    </Grid>
                    <Grid item style={{width:"20%"}}>
                      <div>
                        <Button 
                          color={userContext.preference.color}
                          type="file"
                          name="save"
                          onClick={onImageUpload}
                          {...dragProps}
                        >
                          {GetText("add_pictures")}
                        </Button>
                      </div>
                      <div>
                        <Button 
                          color={userContext.preference.color}
                          type="file"
                          name="save"
                        >
                          {GetText("save")}
                        </Button>
                      </div>
                    </Grid>
                  </Grid>
                </Paper>
              )}
      </ImageUploading>
      </form>
      <CustomizedSnackbar isOpen={openSnackbar} severity={severity} message={message} closeHandler={onCloseSnackbar} />
      <Confirm open={isConfirmOpen} MessageType={messageType} MessageId={messageId} handleClose={handleConfirmDialog} />
    </React.Fragment>
  );
}