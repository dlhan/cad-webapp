import React, { useState, useEffect } from 'react';
import { GetText } from '../../common/BundleManager'
import serviceList from '../../service/ServiceList';
import CircularProgress from '@material-ui/core/CircularProgress';
import CustomizedSnackbar from '../../components/Snackbar';
import Dictionary from '../../common/Dictionary';
import { RunService } from '../../service/RestApi';
import { SetSearchBody, ResetForResponseData, CheckMenuAuth, ConvertItemsToComboType } from '../../common/Utility';
import StickyHeadTable from '../../components/Table';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import AddIcon from '@material-ui/icons/Add';
import Fab from '@material-ui/core/Fab';
import CustomizedDialog from '../../components/Dialog';
import { FormStyle } from '../../style/Style';
import ViewInfo from '../../common/ViewInfo';
import { useUserContext } from '../../common/UserContext';
import Confirm from '../../components/Confirm';
import CustomizedSelect from '../../components/Select';

const useStyles = FormStyle;

const columns = [
  { id: 'objId', numeric: false, disablePadding: false, label: '', align: 'center', minWidth: 0 },
  { id: 'bldgSeq', numeric: false, disablePadding: false, label: '', align: 'center', minWidth: 0 },
  { id: 'storySeq', numeric: false, disablePadding: false, label: '', align: 'center', minWidth: 0 },
  { id: 'bldgName', numeric: false, disablePadding: false, label: 'building_name', align: 'center', minWidth: 100 },
  { id: 'storyNo', numeric: false, disablePadding: false, label: 'floor', align: 'center', minWidth: 100 },
  { id: 'inExtingCnt', numeric: false, disablePadding: false, label: 'in_exting', align: 'center', minWidth: 10 },
  { id: 'outExtingCnt', numeric: false, disablePadding: false, label: 'out_exting', align: 'center', minWidth: 10 },
  { id: 'extingPumpCnt', numeric: false, disablePadding: false, label: 'exting_pump', align: 'center', minWidth: 10 },
  { id: 'sprinklerHCnt', numeric: false, disablePadding: false, label: 'sprinkler_h', align: 'center', minWidth: 10 },
  { id: 'sprinklerAvCnt', numeric: false, disablePadding: false, label: 'sprinkler_av', align: 'center', minWidth: 10 },
  { id: 'sprayExtingHCnt', numeric: false, disablePadding: false, label: 'spray_exting_h', align: 'center', minWidth: 10 },
  { id: 'sprayExtingAvCnt', numeric: false, disablePadding: false, label: 'spray_exting_av', align: 'center', minWidth: 10 },
  { id: 'poExtingHCnt', numeric: false, disablePadding: false, label: 'po_exting_h', align: 'center', minWidth: 10 },
  { id: 'poExtingAvCnt', numeric: false, disablePadding: false, label: 'po_exting_av', align: 'center', minWidth: 10 },
  { id: 'carbonDioxHCnt', numeric: false, disablePadding: false, label: 'carbon_diox_h', align: 'center', minWidth: 10 },
  { id: 'carbonDioxAvCnt', numeric: false, disablePadding: false, label: 'carbon_diox_av', align: 'center', minWidth: 10 },
  { id: 'halogenCompHCnt', numeric: false, disablePadding: false, label: 'halogen_comp_h', align: 'center', minWidth: 10 },
  { id: 'harlogenCompAvCnt', numeric: false, disablePadding: false, label: 'halogen_comp_av', align: 'center', minWidth: 10 },
  { id: 'powderExtingHCnt', numeric: false, disablePadding: false, label: 'powder_exting_h', align: 'center', minWidth: 10 },
  { id: 'powderExtingAvCnt', numeric: false, disablePadding: false, label: 'powder_exting_av', align: 'center', minWidth: 10 },
  { id: 'slideCnt', numeric: false, disablePadding: false, label: 'slide', align: 'center', minWidth: 10 },
  { id: 'ladderCnt', numeric: false, disablePadding: false, label: 'ladder', align: 'center', minWidth: 10 },
  { id: 'rescueCnt', numeric: false, disablePadding: false, label: 'rescue_equipment', align: 'center', minWidth: 10 },
  { id: 'descSlowDeviceCnt', numeric: false, disablePadding: false, label: 'desc_slow_device', align: 'center', minWidth: 10 },
  { id: 'measureEquipCnt', numeric: false, disablePadding: false, label: 'measure_equip', align: 'center', minWidth: 10 },
  { id: 'measureRopeCnt', numeric: false, disablePadding: false, label: 'measure_rope', align: 'center', minWidth: 10 },
  { id: 'safeMatCnt', numeric: false, disablePadding: false, label: 'safe_mat', align: 'center', minWidth: 10 },
  { id: 'rescueEquipCnt', numeric: false, disablePadding: false, label: 'rescue_equip', align: 'center', minWidth: 10 },
  { id: 'emgLightCnt', numeric: false, disablePadding: false, label: 'emg_light', align: 'center', minWidth: 10 },
  { id: 'waterExtingCnt', numeric: false, disablePadding: false, label: 'water_exting', align: 'center', minWidth: 10 },
  { id: 'extingWaterCnt', numeric: false, disablePadding: false, label: 'exting_water', align: 'center', minWidth: 10 },
  { id: 'lwtrCnt', numeric: false, disablePadding: false, label: 'lwtr', align: 'center', minWidth: 10 },
  { id: 'wtrpipeCnt', numeric: false, disablePadding: false, label: 'wtr_pipe', align: 'center', minWidth: 10 },
  { id: 'waterSpringklingCnt', numeric: false, disablePadding: false, label: 'water_springkling', align: 'center', minWidth: 10 },
  { id: 'emgPlugCnt', numeric: false, disablePadding: false, label: 'emg_plug', align: 'center', minWidth: 10 },
  { id: 'wirelessCommCnt', numeric: false, disablePadding: false, label: 'wireless_comm', align: 'center', minWidth: 10 },
  { id: 'extingCnt', numeric: false, disablePadding: false, label: 'exting', align: 'center', minWidth: 10 },
  { id: 'simplctyExtingCnt', numeric: false, disablePadding: false, label: 'simplcty_exting', align: 'center', minWidth: 10 },
  { id: 'emgWaringCnt', numeric: false, disablePadding: false, label: 'emg_waring', align: 'center', minWidth: 10 },
  { id: 'emgBrodcCnt', numeric: false, disablePadding: false, label: 'emg_brodc', align: 'center', minWidth: 10 },
  { id: 'lkgeWaringCnt', numeric: false, disablePadding: false, label: 'lkge_waring', align: 'center', minWidth: 10 },
  { id: 'autoFireFindSensCnt', numeric: false, disablePadding: false, label: 'auto_fire_find_sens', align: 'center', minWidth: 10 },
  { id: 'autoFireFindCircuitCnt', numeric: false, disablePadding: false, label: 'auto_fire_find_circuit', align: 'center', minWidth: 10 },
  { id: 'autoFireNewsfCnt', numeric: false, disablePadding: false, label: 'auto_fire_news_f', align: 'center', minWidth: 10 },
  { id: 'gasLkgeWaringCnt', numeric: false, disablePadding: false, label: 'gas_lkge_waring', align: 'center', minWidth: 10 },
  { id: 'induceLightCnt', numeric: false, disablePadding: false, label: 'induce_light', align: 'center', minWidth: 10 },
  { id: 'induceSignpostCnt', numeric: false, disablePadding: false, label: 'induce_singpost', align: 'center', minWidth: 10 },
  { id: 'hydEquipEtcCnt', numeric: false, disablePadding: false, label: 'hyd_equip_etc', align: 'center', minWidth: 10 },
  { id: 'resmokeCnt', numeric: false, disablePadding: false, label: 'resmoke', align: 'center', minWidth: 10 },
  { id: 'curtainCnt', numeric: false, disablePadding: false, label: 'curtain', align: 'center', minWidth: 10 },
  { id: 'carssetteCnt', numeric: false, disablePadding: false, label: 'carssette', align: 'center', minWidth: 10 },
  { id: 'resistEtcCnt', numeric: false, disablePadding: false, label: 'resist_etc', align: 'center', minWidth: 10 },
];

export default function FireEquipmentList(props) {
  const classes = useStyles();
  var s = new serviceList();
  var dictionary = new Dictionary();
  var viewInfo = new ViewInfo();
  var view = viewInfo.buildingList;
  const userContext = useUserContext();
  let [buildingList, setBuildingList] = useState([]);
  let [storyList, setStoryList] = useState([]);

  let [inputs, setInputs] = useState({
    objId:props.objId,
    bldgSeq:"",
    storySeq:""
  });
  let { bldgSeq,storySeq } = inputs;

  let [loading, setLoading] = useState(false);
  const hasFireObjectCreateAuth = CheckMenuAuth(["IS002"]);

  ////////Setting for Table//////////////////////////////////////////
  const onSelectRow = (rowId) => {
    handleDialogOpen(dictionary.READ, rowId)
  }
  
  const [rows, setRows] = useState([]);

  useEffect(() => {
    RunService(s.getBuildingListForComboBox,{objId:props.objId, locale:userContext.preference.language})
        .then(response => {
          var comboItems = ConvertItemsToComboType("bldgSeq","bldgName",ResetForResponseData(s.getBuildingListForComboBox.response,response.data,""));
          setBuildingList(comboItems);
        })
        .catch(error => {
          setSnackbar(prev => ({
            ...prev,
            openSnackbar:true,
            severity:dictionary.MessageType.Error,
            message:GetText("msg_normal_error_message")
          }));
        })
  }, []);

  const getStoryData = (id) => {
    RunService(s.getStoryListForComboBox,{objId:props.objId, bldgSeq: id})
        .then(response => {
          var comboItems = ConvertItemsToComboType("storySeq","storyNo",ResetForResponseData(s.getStoryListForComboBox.response,response.data,""));
          setStoryList(comboItems);
        })
        .catch(error => {
          setLoading(false);
          setSnackbar(prev => ({
            ...prev,
            openSnackbar:true,
            severity:dictionary.MessageType.Error,
            message:GetText("msg_normal_error_message")
          }));
        })
  }
  
///////////////////////////////////////////////////////////////////

  const getData = (bldgSeq, storySeq) => {
    setLoading(true);
    var body = {objId:props.objId,bldgSeq:bldgSeq,storySeq:storySeq};
    RunService(s.getFireEquipList,'',body)
        .then(response => {
          setLoading(false);
          setRows(ResetForResponseData(s.getFireEquipList.response,response.data,["bldgSeq","storySeq"]));
        })
        .catch(error => {
          setLoading(false);
          setSnackbar(prev => ({
            ...prev,
            openSnackbar:true,
            severity:dictionary.MessageType.Error,
            message:GetText("msg_normal_error_message")
          }));
        })
  }

  useEffect(() => {
    getData();
  }, []);
  
//////Dialog event
  const [isDialogOpen, setIsDialogOpen] = useState(false);
  const [isModified, setIsModified] = useState(false);
  
  const callFromDialog = (p) => {
    setIsModified(p.modified);
    if (p.refresh) {
      getData();
    }
  }

  const [sendInfo, setSendInfo] = useState({operateType:'', objId:props.objId, bldgSeq:''});
  
  const handleDialogClose = (forceNoMessage) => {
    let forceClose = false;

    if (typeof forceNoMessage === 'boolean') forceClose = forceNoMessage;

    if (isModified && !forceClose) {
      setConfirmMessage({
        isConfirmOpen: true,
        messageType: dictionary.WARNING,
        messageId: "msg_confirm_close_dialog" 
      });
    } else {
      setIsDialogOpen(false);
    }
  };
  
  const handleDialogOpen = (operateType, rowId) => {
    var ids;
    
    if (rowId) {
      ids = rowId.split('_');
    } else {
      ids=["",""];
    }
    
    setSendInfo({...sendInfo, operateType:operateType, objId:props.objId, bldgSeq:ids[0],storySeq:ids[1]});
    setIsDialogOpen(true);
  };
////////////////////////////////////////////////////////

//////Confrim event in closing the dialog///////////////
  const [confirmMessage, setConfirmMessage] = useState({
    isConfirmOpen : false,
    messageType : "",
    messageId : ""
  });
  let { isConfirmOpen,messageType,messageId } = confirmMessage;

  const handleConfirmDialog = (answer) => {
    setConfirmMessage({
      ...confirmMessage,
      isConfirmOpen:false
    });

    if (answer){
      setIsDialogOpen(false);
    }
  }
////////////////////////////////////////////////////////

/////////////Setting for Snackbar////////////////////////////////
  let [snackbar, setSnackbar] = useState({openSnackbar:false, severity:'', message:''});
  let { openSnackbar, severity, message } = snackbar;
    
  const onCloseSnackbar = () => {
    setSnackbar(prev => ({
      ...prev,
      openSnackbar:false,
      severity:'',
      message:''
    }));
  }
////////////////////////////////////////////////////////
  const bldgComboChangeHandler =(e)=>{
    let {name, value} = e.target;
    setInputs({
      bldgSeq:value,
      storySeq:""
    });
    if (value != "") {
      getStoryData(value);
    }
    
    getData(value);
  }

  const storyComboChangeHandler =(e)=>{
    let {name, value} = e.target;
    setInputs({
      ...inputs,
      storySeq:value
    });
    getData(bldgSeq,value);
  }
 
  return (
    <React.Fragment>
      <Grid container spacing={1}>
        <Grid item xs={12}>
          <div className={classes.paper}>
            <CustomizedSelect 
              id="bldgSeq" 
              label={GetText("building_name")} 
              onChange={bldgComboChangeHandler}
              selectedItemId={bldgSeq}
              visibleFirstOption={true}
              firstOptionItem={{value:"",label:GetText("all")}}
              items={buildingList} 
            />
            <CustomizedSelect 
              id="storySeq" 
              label={GetText("floor")} 
              onChange={storyComboChangeHandler}
              selectedItemId={storySeq}
              visibleFirstOption={true}
              firstOptionItem={{value:"",label:GetText("all")}}
              items={storyList} 
            />
          </div>
        </Grid>
      </Grid>
      <Grid container spacing={1}>
        <Grid item xs={12}>
          <Paper className={classes.control}>
            <StickyHeadTable 
              tableId={"buildingTable"} 
              onSelectRow={onSelectRow} 
              columns={columns} 
              rows={rows} 
              SupportCheckBox={view.table.allowCheckBox} 
              SupportPaging={view.table.allowPage}
              heightGap={200}
            />
          </Paper>
        </Grid>
        {hasFireObjectCreateAuth && <Grid item xs={12}>
            <div className={classes.centerLocate}>
              <Fab color={userContext.preference.color} aria-label="add">
                <AddIcon onClick={() => handleDialogOpen(dictionary.NEW)} />
              </Fab>
            </div>
          </Grid>
        }
        
      </Grid>
      {loading && <CircularProgress size={24} className={classes.buttonProgress} />}
      <CustomizedDialog isDialogOpen={isDialogOpen} sendInfo={sendInfo} viewInfo={viewInfo.fireEquipmentDetail} handleDialogClose={handleDialogClose} callFromDialog={callFromDialog} />
      <Confirm open={isConfirmOpen} MessageType={messageType} MessageId={messageId} handleClose={handleConfirmDialog} />
      <CustomizedSnackbar isOpen={openSnackbar} severity={severity} message={message} closeHandler={onCloseSnackbar} />
     </React.Fragment>
  );
}