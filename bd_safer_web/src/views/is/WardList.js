import React, { useState } from 'react';
import { GetText } from '../../common/BundleManager';
import serviceList from '../../service/ServiceList';
import CircularProgress from '@material-ui/core/CircularProgress';
import CustomizedSnackbar from '../../components/Snackbar';
import Dictionary from '../../common/Dictionary';
import { RunService } from '../../service/RestApi';
import { SetSearchBody, ResetForResponseData } from '../../common/Utility';
import StickyHeadTable from '../../components/Table';
import CustomizedBreadcrumb from '../../components/BreadCrumb';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import SearchIcon from '@material-ui/icons/Search';
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';
import Fab from '@material-ui/core/Fab';
import CustomizedDialog from '../../components/Dialog';
import { FormStyle } from '../../style/Style';
import ViewInfo from '../../common/ViewInfo';
import { useUserContext } from '../../common/UserContext';
import Confirm from '../../components/Confirm';
import UpwardAndWardCombo from '../../components/UpwardAndWardCombo';
import FindInPageIcon from '@material-ui/icons/FindInPage';
import AccordionSearchBox from '../../components/AccordionSearchBox';
import TextFieldWithIcon from '../../components/TextFieldWithIcon';

const useStyles = FormStyle;

const columns = [
  { id: 'wardId', numeric: false, disablePadding: false, label: '', align: 'center', minWidth: 0 },
  { id: 'upwardName', numeric: false, disablePadding: false, label: 'upper_ward_name', align: 'center', minWidth: 200 },
  { id: 'wardName', numeric: false, disablePadding: false, label: 'ward_name', align: 'center', minWidth: 200 },
  { id: 'typeCode', numeric: false, disablePadding: false, label: 'type', align: 'center', minWidth: 200 },
  { id: 'nickName', numeric: false, disablePadding: false, label: 'abbreviation', align: 'center', minWidth: 150 },
  { id: 'dayTel', numeric: false, disablePadding: false, label: 'tel_num', align: 'center', minWidth: 150 },
  { id: 'nightTel', numeric: false, disablePadding: false, label: 'tel_num', align: 'center', minWidth: 150 },
  { id: 'address', numeric: false, disablePadding: false, label: 'address', align: 'center', minWidth: 300 },
  { id: 'brdUsingYn', numeric: false, disablePadding: false, label: 'is_use_broadcast', align: 'center', minWidth: 200 }  
];

const SearchPanel = (props) => {
  const classes = useStyles();
  const userContext = useUserContext();
  const { openGroupCodeTreeView, typeCodeName, onChange, onSearch, typeClsCd, handleTypeClsCdDelete} = props;

  return (
    <div className={classes.paper} id="search_panel">
      <UpwardAndWardCombo 
        upperWardId="upwardId"
        className={classes.textField25} 
        callEvent={onChange} 
        typeClsCds={[2,3,4,5,6]}
      />
      <TextFieldWithIcon 
        id="typeCodeName"
        label={GetText("type_class_cd")}
        onChange={onChange}
        value={typeCodeName}
        typeClsCd={typeClsCd}
        iconEvent={openGroupCodeTreeView}
        icon={<FindInPageIcon />}
        readOnly={true}
        allowDeleteIcon={true}
        handleDelete={handleTypeClsCdDelete}
      />
      <div className={classes.grow} />
      <div className={classes.sectionSearch}>
        <Button
          variant="contained"
          color={userContext.preference.color}
          onClick={onSearch}
          className={classes.button}
          startIcon={<SearchIcon />}
        >
          {GetText("search")}
        </Button>
      </div>
    </div>
  );
};

export default function WardList(props) {
  const classes = useStyles();
  var s = new serviceList();
  var dictionary = new Dictionary();
  var viewInfo = new ViewInfo();
  var view = viewInfo.wardList;
  const userContext = useUserContext();

  let [loading, setLoading] = useState(false);

////////Setting for Table//////////////////////////////////////////
  const onSelectRow = (rowId) => {
    handleDialogOpen(dictionary.READ, rowId)
  }

  const [page, setPage] = React.useState(1);
  const [totalDataCount, setTotalDataCount] = React.useState(0);
  const [rows, setRows] = useState([]);
  const [expanded, setExpanded] = useState(true);
  const [tableResize, setTableResize] = useState();
  const onPageChange = (event, newPage) => {
    setPage(newPage);
    getData(newPage, false);
  }
///////////////////////////////////////////////////////////////////

//////The parf for search ////////////////////////////////
  let [searchItems, setSearchItems] = useState({
                                       upwardId:''
                                     , wardId:''
                                     , typeClsCd:''
                                     , typeCodeName:''
                                     });
  let { upwardId, wardId, typeClsCd, typeCodeName  } = searchItems;

  const onChange = e => {
    let {name, value} = e.target;

    if (name === 'upwardId') {
      setSearchItems({
        ...searchItems,
        [name]:value,
        wardId:''
      });
    } else {
      setSearchItems({
        ...searchItems,
        [name]:value
      });
    }
  }
///////////////////////////////////////////////////////////////////

  const getData = (pageNumber, isSearchMode) => {
    setLoading(true);
    let body = SetSearchBody(s.getWardList, searchItems, pageNumber + 1, userContext);

    RunService(s.getWardList, {locale: userContext.preference.language}, body, true)
        .then(response => {
          setLoading(false);
          setTotalDataCount(response.data.page.totalCount);

          setRows(ResetForResponseData(s.getWardList.response.rows,response.data.rows,"wardId"));

          if (isSearchMode) {
            setSnackbar(prev => ({
              ...prev,
              openSnackbar:true,
              severity:dictionary.MessageType.Success,
              message:GetText("msg_data_refreshed")
            }));
          }
        })
        .catch(error => {
          setLoading(false);
          setSnackbar(prev => ({
            ...prev,
            openSnackbar:true,
            severity:dictionary.MessageType.Error,
            message:GetText("msg_normal_error_message")
          }));
        })
  }

  const onSearch = () => {
    let pageNumber = 0;
    setPage(pageNumber);
    getData(pageNumber, true);
  }

//Callback from the combo for upward and ward.
  const onChangeWardCombo = (e) => {
    setSearchItems({
      ...searchItems,
      [Object.keys(e)[0]]:e[Object.keys(e)[0]]
    });
  }

//////Dialog event
  const [isDialogOpen, setIsDialogOpen] = useState(false);
  const [isModified, setIsModified] = useState(false);
  
  const callFromDialog = (p) => {
    setIsModified(p.modified);

    if (p.refresh) {
      onSearch();
    }
  }

  const [sendInfo, setSendInfo] = useState({operateType:'', id:''});
  const { operateType, id } = sendInfo;

  const handleDialogClose = (forceNoMessage) => {
    let forceClose = false;

    if (typeof forceNoMessage === 'boolean') forceClose = forceNoMessage;

    if (isModified && !forceClose) {
      setConfirmMessage({
        isConfirmOpen: true,
        messageType: dictionary.WARNING,
        messageId: "msg_confirm_close_dialog" 
      });
    } else {
      setIsDialogOpen(false);
    }
  };
  
  const handleDialogOpen = (operateType, id) => {
    setSendInfo({operateType:operateType, id:id});
    setIsDialogOpen(true);
  };
////////////////////////////////////////////////////////

//////Confrim event in closing the dialog///////////////
  const [confirmMessage, setConfirmMessage] = useState({
    isConfirmOpen : false,
    messageType : "",
    messageId : ""
  });
  let { isConfirmOpen,messageType,messageId } = confirmMessage;

  const handleConfirmDialog = (answer) => {
    setConfirmMessage({
      ...confirmMessage,
      isConfirmOpen:false
    });

    if (answer){
      setIsDialogOpen(false);
    }
  }
////////////////////////////////////////////////////////

/////////////Setting for Snackbar////////////////////////////////
  let [snackbar, setSnackbar] = useState({openSnackbar:false, severity:'', message:''});
  let { openSnackbar, severity, message } = snackbar;
    
  const onCloseSnackbar = () => {
    setSnackbar(prev => ({
      ...prev,
      openSnackbar:false,
      severity:'',
      message:''
    }));
  }
////////////////////////////////////////////////////////

////Accordion //////////////////////////////////////////
  const accordionId = "accordionId";
  const onChangeAccordion = () => {
    setExpanded(!expanded);
    setTimeout(function() {
      onTableResize(true);
    }, 300);
  }

//Resize the height of table whenever acoordioon is resized.
  const onTableResize = (e) => {
    setTableResize(document.getElementById(accordionId).clientHeight)
  }
//Dialog event for type classifcation code
const [isTypeClsCdDialogOpen, setIsTypeClsCdDialogOpen] = useState(false);
const typeClsCdInfo = {groupCode:'211'};

const openGroupCodeTreeView = () => {
  setIsTypeClsCdDialogOpen(true);
}

const callFromTypeClsCdDialog =(p)=> {
  setSearchItems({
    ...searchItems,
    typeClsCd:p.cd,
    typeCodeName:p.cdName
  });
  setIsTypeClsCdDialogOpen(false);
}

const handleTypeClsCdDialogClose = (forceNoMessage) => {
  setIsTypeClsCdDialogOpen(false);
};

const handleTypeClsCdDelete =() => {
  setSearchItems({
    ...searchItems,
    typeClsCd:'',
    typeCodeName:''
  });
}

//////////////////////////////////////
 
  return (
    <React.Fragment>
      <CustomizedBreadcrumb viewInfo={view} />
      <Grid container spacing={1}>
        <Grid item xs={12}>
          <Paper className={classes.control}>
            <AccordionSearchBox  
              expanded={expanded}  
              searchPanel={<SearchPanel 
                              onChangeWardCombo={onChangeWardCombo} 
                              openGroupCodeTreeView={openGroupCodeTreeView}
                              rows={rows} 
                              onChange={onChange} 
                              onSearch={onSearch} 
                              typeCodeName={typeCodeName}
                              typeClsCd={typeClsCd}
                              handleTypeClsCdDelete={handleTypeClsCdDelete}
                          />}
              onChange={onChangeAccordion}
              id={accordionId}
            />
          </Paper>
        </Grid>
        <Grid item xs={12}>
          <Paper className={classes.control}>
            <StickyHeadTable 
              tableId={"mainTable"} 
              onSelectRow={onSelectRow} 
              columns={columns} 
              rows={rows} 
              SupportCheckBox={view.table.allowCheckBox} 
              SupportPaging={view.table.allowPage}
              page={page} 
              totalDataCount={totalDataCount}
              onPageChange={onPageChange} 
              resize={tableResize}
            />
          </Paper>
        </Grid>
        <Grid item xs={12}>
          <div className={classes.centerLocate}>
            <Fab color={userContext.preference.color} aria-label="add">
              <AddIcon onClick={() => handleDialogOpen(dictionary.NEW)} />
            </Fab>
          </div>
        </Grid>
      </Grid>
      {loading && <CircularProgress size={24} className={classes.buttonProgress} />}
      <CustomizedDialog isDialogOpen={isDialogOpen} sendInfo={sendInfo} viewInfo={viewInfo.wardDetail} callFromDialog={callFromDialog} handleDialogClose={handleDialogClose} />
      <CustomizedDialog id='groupCodeViewDialog' isDialogOpen={isTypeClsCdDialogOpen} sendInfo={typeClsCdInfo} viewInfo={viewInfo.groupCodeTreeView} callFromDialog={callFromTypeClsCdDialog} handleDialogClose={handleTypeClsCdDialogClose} />
      <Confirm open={isConfirmOpen} MessageType={messageType} MessageId={messageId} handleClose={handleConfirmDialog} />
      <CustomizedSnackbar isOpen={openSnackbar} severity={severity} message={message} closeHandler={onCloseSnackbar} />
     </React.Fragment>
  );
}