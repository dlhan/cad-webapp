import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';
import { GetText } from '../../common/BundleManager';
import Divider from '@material-ui/core/Divider';
import Dictionary from '../../common/Dictionary';
import FireObjectDetail from '../../views/is/FireObjectDetail';
import BuildingInfoList from '../../views/is/BuildingInfoList';
import StoryInfoList from '../../views/is/StoryInfoList';
import FireEquipmentList from '../../views/is/FireEquipmentList';
import EmergencyPlan from '../../views/is/EmergencyPlan';
import EmegencyPicture from '../../views/is/EmegencyPicture';
import DrawingManagementList from '../../views/is/DrawingManagementList';
import BuildingHistoryList from '../../views/is/BuildingHistoryList';
import ArsonManagerList from '../../views/is/ArsonManagerList';
import SafetyManagerList from '../../views/is/SafetyManagerList';
import BuildingManagerList from '../../views/is/BuildingManagerList';

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`scrollable-auto-tabpanel-${index}`}
      aria-labelledby={`scrollable-auto-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired,
};

function a11yProps(index) {
  return {
    id: `scrollable-auto-tab-${index}`,
    'aria-controls': `scrollable-auto-tabpanel-${index}`,
  };
}

const useStyles = makeStyles((theme) => ({
  root: {
    flexGrow: 1,
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
}));

export default function FireObjectInfo(props) {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);
  const [objId, setObjId] = useState(props.sendInfo.id);
  var dictionary = new Dictionary();
    
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  const callFromDialog = (e) => {
    props.callFromDialog(e);

    if (e.operateType === dictionary.NEW) {
      setObjId(""); 
    } else if (e.id) {
      setObjId(e.id); 
    }
  }

  return (
    <div className={classes.root}>
      <AppBar position="static" color="default">
        <Tabs
          value={value}
          onChange={handleChange}
          indicatorColor="primary"
          textColor="primary"
          variant="scrollable"
          scrollButtons="auto"
        >
          <Tab label={GetText("menu_fire_object_info")} {...a11yProps(0)} />
          <Tab label={GetText("menu_building_info")} {...a11yProps(1)} disabled={objId ? false : true } />
          <Tab label={GetText("menu_story_info")} {...a11yProps(2)} disabled={objId ? false : true} />
          <Tab label={GetText("menu_fire_equipment_info")} {...a11yProps(3)} disabled={objId ? false : true} />
          <Tab label={GetText("menu_emergency_plan_info")} {...a11yProps(4)} disabled={objId ? false : true} />
          <Tab label={GetText("menu_emergency_picture")} {...a11yProps(5)} disabled={objId ? false : true} />
          <Tab label={GetText("menu_drawing_management")} {...a11yProps(6)} disabled={objId ? false : true} />
          <Tab label={GetText("menu_building_history")} {...a11yProps(7)} disabled={objId ? false : true} />
          <Tab label={GetText("menu_arson_manager")} {...a11yProps(8)} disabled={objId ? false : true} />
          <Tab label={GetText("menu_safety_manager")} {...a11yProps(9)} disabled={objId ? false : true} />
          <Tab label={GetText("menu_building_manager")} {...a11yProps(10)} disabled={objId ? false : true} />
        </Tabs>
      </AppBar>
      <TabPanel value={value} index={0}>
        <Divider />
        <FireObjectDetail callFromDialog={callFromDialog} sendInfo={props.sendInfo} objId={objId} handleDialogClose={props.handleDialogClose} />
      </TabPanel>
      <TabPanel value={value} index={1}>
        <Divider />
        <BuildingInfoList callFromDialog={callFromDialog} sendInfo={props.sendInfo} objId={objId} handleDialogClose={props.handleDialogClose} />
      </TabPanel>
      <TabPanel value={value} index={2}>
        <Divider />
        <StoryInfoList callFromDialog={callFromDialog} sendInfo={props.sendInfo} objId={objId} handleDialogClose={props.handleDialogClose} />
      </TabPanel>
      <TabPanel value={value} index={3}>
        <Divider />
        <FireEquipmentList callFromDialog={callFromDialog} sendInfo={props.sendInfo} objId={objId} handleDialogClose={props.handleDialogClose} />
      </TabPanel>
      <TabPanel value={value} index={4}>
        <Divider />
        <EmergencyPlan callFromDialog={callFromDialog} sendInfo={props.sendInfo} objId={objId} handleDialogClose={props.handleDialogClose} />
      </TabPanel>
      <TabPanel value={value} index={5}>
        <Divider />
        <EmegencyPicture callFromDialog={callFromDialog} sendInfo={props.sendInfo} objId={objId} handleDialogClose={props.handleDialogClose} />
      </TabPanel>
      <TabPanel value={value} index={6}>
        <Divider />
        <DrawingManagementList callFromDialog={callFromDialog} sendInfo={props.sendInfo} objId={objId} handleDialogClose={props.handleDialogClose} />
      </TabPanel>
      <TabPanel value={value} index={7}>
        <Divider />
        <BuildingHistoryList callFromDialog={callFromDialog} sendInfo={props.sendInfo} objId={objId} handleDialogClose={props.handleDialogClose} />
      </TabPanel>
      <TabPanel value={value} index={8}>
        <Divider />
        <ArsonManagerList callFromDialog={callFromDialog} sendInfo={props.sendInfo} objId={objId} handleDialogClose={props.handleDialogClose} />
      </TabPanel>
      <TabPanel value={value} index={9}>
        <Divider />
        <SafetyManagerList callFromDialog={callFromDialog} sendInfo={props.sendInfo} objId={objId} handleDialogClose={props.handleDialogClose} />
      </TabPanel>
      <TabPanel value={value} index={10}>
        <Divider />
        <BuildingManagerList callFromDialog={callFromDialog} sendInfo={props.sendInfo} objId={objId} handleDialogClose={props.handleDialogClose} />
      </TabPanel>
    </div>
  );
}
