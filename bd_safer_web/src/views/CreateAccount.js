import React, { useState, useEffect } from 'react';
import { GetText } from '../common/BundleManager';
import serviceList from '../service/ServiceList';
import { RunService } from '../service/RestApi';
import Dictionary from '../common/Dictionary';
import { CheckResultFromService } from '../common/Utility';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Divider from '@material-ui/core/Divider';
import Button from '@material-ui/core/Button';
import Box from '@material-ui/core/Box';
import FormLabel from '@material-ui/core/FormLabel';
import Address from '../components/Address';
import ActionPanel from '../components/ActionPanel';
import { FormStyle } from '../style/Style';
import CustomizedSwitch from '../components/Switch';
import { useUserContext } from '../common/UserContext';
import CustomizedSnackbar from '../components/Snackbar';
import CircularProgress from '@material-ui/core/CircularProgress';
import { useForm } from "react-hook-form";
import Confirm from '../components/Confirm';
import UpwardAndWardCombo from '../components/UpwardAndWardCombo';
import CustomizedTextField from '../components/TextField';
import CodeComboBox from '../components/CodeComboBox';
import TextFieldWithIcon from '../components/TextFieldWithIcon';
import YoutubeSearchedForIcon from '@material-ui/icons/YoutubeSearchedFor';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import CustomizedRadioGroup from '../components/RadioGroup';

const useStyles = FormStyle;
let isModified = false;

export default function CreateAccount(props) {
  const classes = useStyles();
  var dictionary = new Dictionary();
  const userContext = useUserContext();
  const s = new serviceList();

  let [inputs, setInputs] = useState({
                                      userId:''
                                    , firstName:''
                                    , lastName:''
                                    , citizenNo:''
                                    , wardId:''
                                    , classCd:''
                                    , titleCd:''
                                    , workCd:''
                                    , workSectionCd:''
                                    , userPw:''
                                    , zipCode:''
                                    , address:''
                                    , homeTel:''
                                    , officeTel:''
                                    , cell:''
                                    , faxNo:''
                                    , extTel:''
                                    , email:''
                                    , birthDate:''
                                    , gender:'F'
                                    });
  let { userId,firstName,lastName,citizenNo,classCd,titleCd,workCd,workSectionCd,userPw,zipCode,address,homeTel,officeTel,cell,faxNo,extTel,email,birthDate,gender } = inputs;
  let [loading, setLoading] = useState(false);
  let [disabled, setDisabled] = useState(false);
  let [showPassword, setShowPassword] = useState(false);
  let [checkLoginId, setCheckLoginId] = useState({
    loginIdChecked: false,
    helpText: '',
    error: false
  });
      
//////////Upward select event/////////////////////////////////////////
  const callEvent = (e) => {

    setInputs({
      ...inputs,
      wardId:e[Object.keys(e)[0]]
    });
  }
///////////////////////////////////////////////////
  
/////////Setting for validate the inputs in the form.
  const { handleSubmit } = useForm();
////////////////////////////////////////////////////

  const onChange = e => {
    let {name, value} = e.target;
    
    if (name === 'userId') {
      setCheckLoginId({...checkLoginId, helpText:GetText("msg_check_loginid")});
    }

    if (name === 'upwardId') name = 'wardId';
    isModified = true;
    setInputs({
      ...inputs,
      [name]:value
    });
  }

/////////////Setting for Snackbar////////////////////////////////
  let [snackbar, setSnackbar] = useState({openSnackbar:false, severity:'', message:''});
  let { openSnackbar, severity, message } = snackbar;
    
  const onCloseSnackbar = () => {
    setSnackbar(prev => ({
      ...prev,
      openSnackbar:false,
      severity:'',
      message:''
    }));
  }
/////////////////////////////////////////////

//////Confrim event
  const [confirmMessage, setConfirmMessage] = React.useState({
                                                      isConfirmOpen : false,
                                                      messageType : "",
                                                      messageId : ""
                                                });
  let { isConfirmOpen,messageType,messageId } = confirmMessage;
  
  const handleConfirmDialog = (answer) => {
    setConfirmMessage({
      ...confirmMessage,
      isConfirmOpen:false
    });
  
    if (answer){
      onSubmit();
    }
  }
  /////////////////////////////////////////////
  
  ////Button operate
  const onConfirm = () => {
    if (!checkLoginId.loginIdChecked) {
      setSnackbar(prev => ({
        ...prev,
        openSnackbar:true,
        severity:dictionary.MessageType.Error,
        message:GetText("msg_check_loginid")
      }));

      return;
    }
    setConfirmMessage({ isConfirmOpen: true, messageType: dictionary.WARNING, messageId : "msg_will_you_create_account" });
  }

  const checkLoginDuplicated = () => {
    setLoading(true);
    var body = {loginId: userId};
    RunService(s.getCheckLoginIdDuplicated, body)
          .then(response => {
            if (response.data) {//true is duplicated, false:it is unique.
              setCheckLoginId({loginIdChecked:false, helpText:GetText("msg_id_duplicated"), error:true});
            } else {
              setCheckLoginId({loginIdChecked:true, helpText:GetText("msg_id_available"), error:false});
            }
            setLoading(false);
          })
          .catch(error => {
            setSnackbar(prev => ({
              ...prev,
              openSnackbar:true,
              severity:dictionary.MessageType.Error,
              message:GetText("msg_normal_error_message")
            }));
            setLoading(false);
          })
  }

  const onPasswordIconEvent = () => {
    setShowPassword(!showPassword);
  }

  /////////////////Check whether inputs are modified or not. If modified, it sends the state to parent//////////////////// 
  useEffect(() => {
    props.callFromDialog({modified: isModified});
  }, [inputs]);
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  ///////////////////// Insert ///////////////////////////
  const onSubmit = () => {
    setLoading(true);
    let service;
    let body = inputs;
  
    RunService(s.insertUser,{loginId:" ",adminMode:0},body)
        .then(response => {
          setLoading(false);
          var result = CheckResultFromService(response.data);
          if (result.isSuccess) {
            props.callFromDialog({modified: false});
            setDisabled(true);
            setSnackbar(prev => ({
              ...prev,
              openSnackbar:true,
              severity:dictionary.MessageType.Success,
              message:GetText(result.message)
            }));
          } else {
            setSnackbar(prev => ({
              ...prev,
              openSnackbar:true,
              severity:dictionary.MessageType.Error,
              message:GetText(result.message)
            }));
          }
        })
        .catch(error => {
          setLoading(false);
          setSnackbar(prev => ({
            ...prev,
            openSnackbar:true,
            severity:dictionary.MessageType.Error,
            message:GetText("msg_normal_error_message")
          }));
        })
  } 
  /////////////////////////////////////////////////////////////////

  return (
    <React.Fragment>
      <CssBaseline />
      <form onSubmit={handleSubmit(onConfirm)}>
      <div className={classes.paper}>
        <FormLabel component="legend">{GetText("basic_information")}</FormLabel>
      </div>
      <Paper className={classes.control}>
        <Grid container spacing={1}>
          <Grid item xs={12}>
            <div className={classes.paper}>
              <CustomizedTextField
                id="firstName"
                label={GetText("first_name")}
                onChange={onChange}
                disabled={disabled}
                value={firstName}
                required={true}
              />     
              <CustomizedTextField
                id="lastName"
                label={GetText("last_name")}
                onChange={onChange}
                disabled={disabled}
                value={lastName}
                required={true}
              />  
              <CustomizedTextField
                id="birthDate"
                label={GetText("birth_day")}
                type="date"
                onChange={onChange}
                disabled={disabled}
                value={birthDate}
              />
              <CustomizedRadioGroup 
                id="gender" 
                label={GetText("gender")}
                defaultValue="F"
                value={gender}
                radios={[{label:GetText("female"), value:"F"},{label:GetText("male"), value:"M"}]} 
                onChange={onChange}
                disabled={disabled}
              />
              <CustomizedTextField
                label={GetText("citizen_number")}
                id="citizenNo"
                onChange={onChange}
                disabled={disabled}
                value={citizenNo}
              />
            </div>
          </Grid>
          <Grid item xs={12}>
            <div className={classes.paper}>
              <UpwardAndWardCombo 
                upperWardId="upwardId"
                wardid="wardId"
                className={classes.textField25} 
                callEvent={onChange} 
                typeClsCds={[2,3,4,5,6]}
              />
              <TextFieldWithIcon 
                id="userId"
                label={GetText("user_id")}
                onChange={onChange}
                disabled={disabled}
                value={userId}
                iconEvent={checkLoginDuplicated}
                icon={<YoutubeSearchedForIcon />}
                helpText={checkLoginId.helpText}
                error={checkLoginId.error}
                required={true}
              />
              <TextFieldWithIcon 
                id="userPw"
                label={GetText("password")}
                onChange={onChange}
                type={showPassword ? 'text' : 'password'}
                disabled={disabled}
                value={userPw}
                iconEvent={onPasswordIconEvent}
                icon={showPassword ? <Visibility /> : <VisibilityOff />}
                required={true}
              />
            </div>
          </Grid>
        </Grid>
      </Paper>   
      <div className={classes.paper}>
        <FormLabel component="legend">{GetText("job_info")}</FormLabel>
      </div>
      <Paper className={classes.control}>
        <Grid container spacing={1}>
          <Grid item xs={12}>
            <div className={classes.paper}>
              <CodeComboBox
                  groupCode={'090'}
                  selectedCode={classCd}
                  childLabel={GetText("job_position")}
                  childComboId={'classCd'}
                  callEvent={onChange}
                  childOnly={true}
                  disabled={disabled}
              />
              <CodeComboBox
                  groupCode={'089'}
                  selectedCode={titleCd}
                  childLabel={GetText("job_class")}
                  childComboId={'titleCd'}
                  callEvent={onChange}
                  childOnly={true}
                  disabled={disabled}
              /> 
              <CodeComboBox
                  groupCode={'091'}
                  selectedCode={workCd}
                  childLabel={GetText("job_type")}
                  childComboId={'workCd'}
                  callEvent={onChange}
                  childOnly={true}
                  disabled={disabled}
              /> 
              <CodeComboBox
                  groupCode={'092'}
                  selectedCode={workSectionCd}
                  childLabel={GetText("wark_cls")}
                  childComboId={'workSectionCd'}
                  callEvent={onChange}
                  childOnly={true}
                  disabled={disabled}
              /> 
            </div>
          </Grid>
        </Grid>
      </Paper>   
      <div className={classes.paper}>
        <FormLabel component="legend">{GetText("contact")}</FormLabel>
      </div>
      <Paper className={classes.control}>
        <Grid container spacing={1}>
          <Grid item xs={12}>
            <div className={classes.paper}>
              <CustomizedTextField
                label={GetText("home_tel")}
                id="homeTel"
                onChange={onChange}
                disabled={disabled}
                value={homeTel}
              />
              <CustomizedTextField
                label={GetText("mobile_number")}
                id="cell"
                onChange={onChange}
                disabled={disabled}
                value={cell}
              />
              <CustomizedTextField
                label={GetText("office_tel")}
                id="officeTel"
                onChange={onChange}
                disabled={disabled}
                value={officeTel}
              />
              <CustomizedTextField
                label={GetText("ext_num")}
                id="extTel"
                onChange={onChange}
                disabled={disabled}
                value={extTel}
              />
              <CustomizedTextField
                label={GetText("fax")}
                id="faxNo"
                onChange={onChange}
                disabled={disabled}
                value={faxNo}
              />
            </div>
          </Grid>
          <Grid item xs={12}>
            <div className={classes.paper}>
            <CustomizedTextField
                label={GetText("email")}
                id="email"
                onChange={onChange}
                disabled={disabled}
                value={email}
              />
              <Address 
                addressId={'zipCode'} 
                etcAddressid={'address'} 
                changeEvent={onChange}
                disabled={disabled}
                postalCodeValue={zipCode}
                etcAddressValue={address}
              />
            </div>
          </Grid>
        </Grid>
      </Paper> 
      <Box p={1}></Box>
      <Divider style={{'width':'100%','position': 'absolute', 'left': 0}} />
      <ActionPanel>
        <Button 
          disabled={disabled}
          color={userContext.preference.color}
          type="submit"
        >
          { GetText("save") }
        </Button>
        
        {loading && <CircularProgress size={24} className={classes.buttonProgress} />}
      </ActionPanel>
      </form>
      <CustomizedSnackbar isOpen={openSnackbar} severity={severity} message={message} closeHandler={onCloseSnackbar} />
      <Confirm open={isConfirmOpen} MessageType={messageType} MessageId={messageId} handleClose={handleConfirmDialog} />
    </React.Fragment>
  );
}