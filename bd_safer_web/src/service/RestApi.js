import axios from 'axios';
import Dictionary from '../common/Dictionary';
import { SetBody, StringUrlFormatFromJson } from '../common/Utility';

const dictionary = new Dictionary();

function RunService(service, uri, body, skipCheck = false) {
    var url = service.url;
    if (uri) {
      url = StringUrlFormatFromJson(service.url, uri);
    }
    
    switch (service.method) {
        case dictionary.POST :
            var requestBody = body;
            if (!skipCheck) {
                requestBody = SetBody(service.body, body);
            }
            return axios.post(url, requestBody);
        case dictionary.GET :
            return axios.get(url);
        case dictionary.DELETE :
            return axios.delete(url, body);
        default :
            return;
    }
}

export { RunService };

