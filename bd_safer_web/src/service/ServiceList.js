import Dictionary from '../common/Dictionary';

const dictionary = new Dictionary();
class ServiceList {
    login = {
                "url" : "/systemmanage/login",
                "method" : dictionary.POST,
                "body" : {
                            "userId":dictionary.STRING,
                            "userPw":dictionary.STRING
                        },
                "response" : {
                    "result": {
                        "isSuccess": dictionary.BOOLEAN,
                        "message": dictionary.STRING
                    },
                    "userInfo": {
                        "userSeq": dictionary.NUMERIC,
                        "userId": dictionary.STRING,
                        "wardId": dictionary.NUMERIC,
                        "email": dictionary.STRING,
                        "sysmgrYn": dictionary.BOOLEAN,
                    },
                    "userPreference": {
                        "language": dictionary.STRING,
                        "color": dictionary.STRING,
                        "theme": dictionary.STRING,
                        "tableRows": dictionary.NUMERIC,
                        "tableDense": dictionary.BOOLEAN
                    },
                    "userAuth": []
                }
            };
    getWardList = {
                "url" : "/informationsupport/wardlist/{locale}",
                "method" : dictionary.POST,
                "body" : {
                            "page" : {
                                        "rowsPerPage" : dictionary.NUMERIC,
                                        "pageNumber" : dictionary.NUMERIC
                                    },
                            "typeClsCd" : dictionary.NUMERIC,
                            "upwardId" : dictionary.STRING,
                            "wardId" : dictionary.STRING,
                            "locale" : dictionary.STRING
                        },
                "response" : {
                            "page" : {
                                        "rowsPerPage" : dictionary.NUMERIC,
                                        "pageNumber" : dictionary.NUMERIC,
                                        "totalCount" : dictionary.NUMERIC
                                    },
                            "rows" : {
                                "wardId": dictionary.STRING,
                                "upwardName": dictionary.STRING,
                                "wardName": dictionary.STRING,
                                "typeCode": dictionary.STRING,
                                "nickName": dictionary.STRING,
                                "dayTel": dictionary.STRING,
                                "nightTel": dictionary.STRING,
                                "address": dictionary.STRING,
                                "brdUsingYn":dictionary.BOOLEANUseOrNot
                            }
                }
            };
    getUpperwardListByTypeClsCd = {
                "url" : "/informationsupport/getupperwardlistbytypeclscd/{typeClsCd}",
                "method" : dictionary.GET,
                "body" : [], 
                "response" : {
                    "wardId": dictionary.STRING,
                    "wardName": dictionary.STRING
                }
            };
    getUpperwardList = {
                "url" : "/informationsupport/getUpperwardList",
                "method" : dictionary.POST,
                "body" : [], //Type Classification cds
                "response" : {
                    "wardId": dictionary.STRING,
                    "wardName": dictionary.STRING
                }
            };
    getWardListForUpperOffice = {
                "url" : "/informationsupport/getwardlistforupperoffice/{id}",
                "method" : dictionary.GET,
                "body" : [], 
                "response" : {
                    "wardId": dictionary.STRING,
                    "wardName": dictionary.STRING
                }
            };
    getWardListForTypeClsCd = {
                "url" : "/informationsupport/getwardlistfortypeclscd/{typeClsCd}",
                "method" : dictionary.GET,
                "body" : [],
                "response" : {
                    "wardId": dictionary.STRING,
                    "wardName": dictionary.STRING
                }
            };
    getWardInfo = {
                "url" : "/informationsupport/getwardinfo/{id}/{locale}",
                "method" : dictionary.GET,
                "body" : {},
                "response" : {
                                "wardId": dictionary.STRING,
                                "upwardId": dictionary.STRING,
                                "fireOfficeId": dictionary.STRING,
                                "wardName": dictionary.STRING,
                                "typeClsCd": dictionary.NUMERIC,
                                "typeClsName": dictionary.STRING,
                                "nickName": dictionary.STRING,
                                "zipCode": dictionary.STRING,
                                "address": dictionary.STRING,
                                "dspGroupYn": dictionary.BOOLEAN,
                                "autoListYn": dictionary.BOOLEAN,
                                "dayTel": dictionary.STRING,
                                "nightTel": dictionary.STRING,
                                "extTel": dictionary.STRING,
                                "dayFax": dictionary.STRING,
                                "nightFax": dictionary.STRING,
                                "wardopenDate": dictionary.DATE,
                                "wardcloseDate": dictionary.DATE,
                                "wardopenYn": dictionary.BOOLEAN,
                                "brdUsingYn": dictionary.BOOLEAN,
                                "remark": dictionary.STRING,
                                "wardOrder": dictionary.NUMERIC,
                                "x": dictionary.STRING,
                                "y": dictionary.STRING,
                                "useYn": dictionary.BOOLEAN
                            }
            };
    insertWard = {
                "url" : "/informationsupport/insertward/{userId}",
                "method" : dictionary.POST,
                "body" : {
                            "upwardId": dictionary.STRING,
                            "wardName": dictionary.STRING,
                            "typeClsCd": dictionary.NUMERIC,
                            "nickName": dictionary.STRING,
                            "zipCode": dictionary.STRING,
                            "address": dictionary.STRING,
                            "dspGroupYn": dictionary.BOOLEAN,
                            "autoListYn": dictionary.BOOLEAN,
                            "dayTel": dictionary.STRING,
                            "useYn": dictionary.BOOLEAN,
                            "nightTel": dictionary.STRING,
                            "extTel": dictionary.STRING,
                            "dayFax": dictionary.STRING,
                            "nightFax": dictionary.STRING,
                            "wardopenDate": dictionary.DATE,
                            "wardcloseDate": dictionary.DATE,
                            "wardopenYn": dictionary.BOOLEAN,
                            "brdUsingYn": dictionary.BOOLEAN,
                            "remark": dictionary.STRING,
                            "wardOrder": dictionary.NUMERIC,
                            "x": dictionary.STRING,
                            "y": dictionary.STRING,
                        },
                "response" : {
                    "result": {
                        "isSuccess": dictionary.BOOLEAN,
                        "message": dictionary.STRING,
                        "ID":dictionary.STRING
                    }
                }
            };
    updateWard = {
                "url" : "/informationsupport/updateward/{userId}",
                "method" : dictionary.POST,
                "body" : {
                            "wardId": dictionary.STRING,
                            "upwardId": dictionary.STRING,
                            "fireOfficeId": dictionary.STRING,
                            "wardName": dictionary.STRING,
                            "typeClsCd": dictionary.NUMERIC,
                            "nickName": dictionary.STRING,
                            "zipCode": dictionary.STRING,
                            "address": dictionary.STRING,
                            "dspGroupYn": dictionary.BOOLEAN,
                            "autoListYn": dictionary.BOOLEAN,
                            "dayTel": dictionary.STRING,
                            "nightTel": dictionary.STRING,
                            "extTel": dictionary.STRING,
                            "dayFax": dictionary.STRING,
                            "nightFax": dictionary.STRING,
                            "wardopenDate": dictionary.DATE,
                            "wardcloseDate": dictionary.DATE,
                            "wardopenYn": dictionary.BOOLEAN,
                            "brdUsingYn": dictionary.BOOLEAN,
                            "remark": dictionary.STRING,
                            "wardOrder": dictionary.NUMERIC,
                            "x": dictionary.STRING,
                            "y": dictionary.STRING,
                            "useYn": dictionary.BOOLEAN
                        },
                "response" : {
                    "result": {
                        "isSuccess": dictionary.BOOLEAN,
                        "message": dictionary.STRING,
                        "ID":dictionary.NUMERIC
                    }
                }
            };
    deleteWard = {
                "url" : "/informationsupport/deleteward",
                "method" : dictionary.POST,
                "body" : [], //Ward ids
                "response" : {
                    "result": {
                        "isSuccess": dictionary.BOOLEAN,
                        "message": dictionary.STRING
                    }
                }
            };
    getGroupCodes = {
                "url" : "/systemmanage/getgroupcode/{parentGroupCode}/{locale}",
                "method" : dictionary.GET,
                "body" : [], 
                "response" : {
                    "groupCode": dictionary.STRING,
                    "level": dictionary.NUMERIC,
                    "name": dictionary.STRING
                }
            };
    getCodesForGroupCode = {
                "url" : "/systemmanage/getcodesforgroupcode/{groupCode}/{locale}",
                "method" : dictionary.GET,
                "body" : [], 
                "response" : {
                    "code1": dictionary.STRING,
                    "name": dictionary.STRING,
                    "description": dictionary.STRING
                }
            };
    getUserPreference = {
                "url" : "/userpreference/getuserpreference/{userId}",
                "method" : dictionary.GET,
                "body" : [], 
                "response" : {
                    "userId": dictionary.STRING,
                    "language": dictionary.STRING,
                    "theme": dictionary.STRING,
                    "tableRows": dictionary.NUMERIC,
                    "tableDense": dictionary.BOOLEAN,
                    "color": dictionary.STRING
                }
            };
    upsertUserPreference = {
                "url" : "/userpreference/upsertuserpreference",
                "method" : dictionary.POST,
                "body" : {
                            "userId": dictionary.STRING,
                            "language": dictionary.STRING,
                            "color": dictionary.STRING,
                            "theme": dictionary.STRING,
                            "tableRows": dictionary.INT,
                            "tableDense": dictionary.BOOLEAN
                        },
                "response" : {
                            "result": {
                                "isSuccess": dictionary.BOOLEAN,
                                "message": dictionary.STRING,
                                "ID":dictionary.NUMERIC
                            }
                }
            };
    getUserList = {
                "url" : "/humanresource/getusers/{locale}",
                "method" : dictionary.POST,
                "body" : {
                            "page" : {
                                        "rowsPerPage" : dictionary.NUMERIC,
                                        "pageNumber" : dictionary.NUMERIC
                                    },
                            "wardId" : dictionary.STRING,
                            "firstName" : dictionary.STRING,
                            "lastName" : dictionary.STRING,
                            "userId" : dictionary.STRING,
                            "titleCd" : dictionary.NUMERIC,
                            "classCd" : dictionary.NUMERIC,
                            "workCd" : dictionary.NUMERIC,
                            "workSectionCd" : dictionary.NUMERIC
                        },
                "response" : {
                            "page" : {
                                        "rowsPerPage" : dictionary.NUMERIC,
                                        "pageNumber" : dictionary.NUMERIC,
                                        "totalCount" : dictionary.NUMERIC
                                    },
                            "rows" : {
                                "userId": dictionary.STRING,
                                "name": dictionary.STRING,
                                "upwardName": dictionary.STRING,
                                "wardName": dictionary.STRING,
                                "classCdName": dictionary.STRING,
                                "titleCdName": dictionary.STRING,
                                "workCdName": dictionary.STRING,
                                "workSectionCdName": dictionary.STRING,
                                "homeTel": dictionary.STRING,
                                "officeTel": dictionary.STRING,
                                "cell": dictionary.STRING,
                                "faxNo": dictionary.STRING,
                                "extTel": dictionary.STRING,
                                "email": dictionary.STRING
                            }
                }
            };
    getUserInfo = {
        "url" : "/humanresource/getuserinfo/{userId}",
        "method" : dictionary.GET,
        "body" : [], 
        "response" : {
            "userSeq": dictionary.NUMERIC,
            "userId": dictionary.STRING,
            "firstName": dictionary.STRING,
            "lastName": dictionary.STRING,
            "citizenNo": dictionary.STRING,
            "upWardId": dictionary.STRING,
            "wardId": dictionary.STRING,
            "classCd": dictionary.NUMERIC,
            "titleCd": dictionary.NUMERIC,
            "workCd": dictionary.NUMERIC,
            "workSectionCd": dictionary.NUMERIC,
            "sysmgrYn": dictionary.BOOLEAN,
            "zipCode": dictionary.STRING,
            "address": dictionary.STRING,
            "homeTel": dictionary.STRING,
            "officeTel": dictionary.STRING,
            "cell": dictionary.STRING,
            "faxNo": dictionary.STRING,
            "extTel": dictionary.STRING,
            "email": dictionary.STRING,
            "birthDate": dictionary.DATE,
            "gender": dictionary.STRING
            }
        };
    insertUser = {
            "url" : "/humanresource/insertuser/{loginId}/{adminMode}",
            "method" : dictionary.POST,
            "body" : {
                "userId": dictionary.STRING,
                "firstName": dictionary.STRING,
                "lastName": dictionary.STRING,
                "citizenNo": dictionary.STRING,
                "wardId": dictionary.STRING,
                "classCd":dictionary.NUMERIC,
                "titleCd":dictionary.NUMERIC,
                "workCd": dictionary.NUMERIC,
                "workSectionCd": dictionary.NUMERIC,
                "userPw":dictionary.STRING,
                "zipCode":dictionary.STRING,
                "address":dictionary.STRING,
                "homeTel":dictionary.STRING,
                "officeTel":dictionary.STRING,
                "cell":dictionary.STRING,
                "faxNo":dictionary.STRING,
                "extTel":dictionary.STRING,
                "email":dictionary.STRING,
                "birthDate":dictionary.DATE,
                "gender":dictionary.STRING,
                "sysmgrYn":dictionary.BOOLEAN
            },
            "response" : {
                "result": {
                    "isSuccess": dictionary.BOOLEAN,
                    "message": dictionary.STRING,
                    "ID":dictionary.NUMERIC
                }
            }
        };
    updateUser = {
            "url" : "/humanresource/updateuser/{loginId}",
            "method" : dictionary.POST,
            "body" : {
                    "userSeq": dictionary.NUMERIC,
                    "userId": dictionary.STRING,
                    "firstName": dictionary.STRING,
                    "lastName": dictionary.STRING,
                    "citizenNo": dictionary.STRING,
                    "wardId": dictionary.STRING,
                    "classCd":dictionary.NUMERIC,
                    "titleCd":dictionary.NUMERIC,
                    "workCd": dictionary.NUMERIC,
                    "workSectionCd": dictionary.NUMERIC,
                    "userPw":dictionary.STRING,
                    "zipCode":dictionary.STRING,
                    "address":dictionary.STRING,
                    "homeTel":dictionary.STRING,
                    "officeTel":dictionary.STRING,
                    "cell":dictionary.STRING,
                    "faxNo":dictionary.STRING,
                    "extTel":dictionary.STRING,
                    "email":dictionary.STRING,
                    "birthDate":dictionary.DATE,
                    "gender":dictionary.STRING,
                    "sysmgrYn":dictionary.BOOLEAN
                    },
            "response" : {
                "result": {
                    "isSuccess": dictionary.BOOLEAN,
                    "message": dictionary.STRING,
                    "ID":dictionary.NUMERIC
                }
            }
        };
    deleteUser = {
            "url" : "/humanresource/deleteuser/{userId}",
            "method" : dictionary.POST,
            "body" : [], //Array of userId
            "response" : {
                "result": {
                    "isSuccess": dictionary.BOOLEAN,
                    "message": dictionary.STRING
                }
            }
        };
    
    getCheckLoginIdDuplicated = {
            "url" : "/humanresource/checkloginidduplicated/{loginId}",
            "method" : dictionary.GET,
            "body" : [], 
            "response" : dictionary.BOOLEAN
        };
            
    getNotificationList = {
            "url" : "/systemmanage/getnotification/{locale}",
            "method" : dictionary.GET,
            "body" : [], 
            "response" : {
                "notificationId": dictionary.NUMERIC,
                "notificationType": dictionary.STRING,
                "state": dictionary.STRING,
                "createdDate": dictionary.STRING,
                "userId": dictionary.STRING,
                "firstName": dictionary.STRING,
                "lastName": dictionary.STRING,
                "wardName": dictionary.STRING
            }
        };
    approveNotification = {
            "url" : "/systemmanage/approvenotification",
            "method" : dictionary.POST,
            "body" : [], 
            "response" : {
                "result": {
                    "isSuccess": dictionary.BOOLEAN,
                    "message": dictionary.STRING
                }
            }
        };
    rejectNotification = {
            "url" : "/systemmanage/rejectnotification",
            "method" : dictionary.POST,
            "body" : [], 
            "response" : {
                "result": {
                    "isSuccess": dictionary.BOOLEAN,
                    "message": dictionary.STRING
                }
            }
        };
    getUserEducationList = {
            "url" : "/humanresource/getusereducationlist/{userId}/{locale}",
            "method" : dictionary.GET,
            "body" : {},
            "response" : {
                        "rows" : {
                            "userEducationSeq": dictionary.NUMERIC,
                            "academyName": dictionary.STRING,
                            "major": dictionary.STRING,
                            "degreeCdName":dictionary.STRING,
                            "academyTypeCdName": dictionary.STRING,
                            "entranceDate": dictionary.DATE,
                            "graducationDate": dictionary.DATE,
                        }
            }
        };
    getUserEducationInfo = {
            "url" : "/humanresource/geteducationinfo/{userEducationSeq}",
            "method" : dictionary.GET,
            "body" : [], 
            "response" : {
                "userEducationSeq": dictionary.NUMERIC,
                "userId": dictionary.STRING,
                "entranceDate": dictionary.DATE,
                "graducationDate": dictionary.DATE,
                "academyName": dictionary.STRING,
                "major": dictionary.STRING,
                "degreeCd":dictionary.STRING,
                "academyTypeCd": dictionary.NUMERIC
                }
            };
    insertUserEducation = {
            "url" : "/humanresource/inserteducationinfo",
            "method" : dictionary.POST,
            "body" : {
                "userId": dictionary.STRING,
                "entranceDate": dictionary.DATE,
                "graducationDate": dictionary.DATE,
                "academyName": dictionary.STRING,
                "major": dictionary.STRING,
                "degreeCd":dictionary.NUMERIC,
                "academyTypeCd": dictionary.STRING
                    },
            "response" : {
                "result": {
                    "isSuccess": dictionary.BOOLEAN,
                    "message": dictionary.STRING,
                    "ID":dictionary.STRING
                }
            }
    };
    updateUserEducation = {
        "url" : "/humanresource/updateeducationinfo",
        "method" : dictionary.POST,
        "body" : {
            "userEducationSeq": dictionary.NUMERIC,
            "userId": dictionary.NUMERIC,
            "entranceDate": dictionary.DATE,
            "graducationDate": dictionary.DATE,
            "academyName": dictionary.STRING,
            "major": dictionary.STRING,
            "degreeCd":dictionary.STRING,
            "academyTypeCd": dictionary.STRING
                },
        "response" : {
            "result": {
                "isSuccess": dictionary.BOOLEAN,
                "message": dictionary.STRING,
                "ID":dictionary.NUMERIC
            }
        }
    };
    deleteUserEducation = {
        "url" : "/humanresource/deleteeducationinfo",
        "method" : dictionary.POST,
        "body" : [], //UserEducation seq
        "response" : {
            "result": {
                "isSuccess": dictionary.BOOLEAN,
                "message": dictionary.STRING
            }
        }
    };
    getUserLicenseList = {
        "url" : "/humanresource/getlicenselist/{userId}",
        "method" : dictionary.GET,
        "body" : {},
        "response" : {
                    "rows" : {
                        "licenseSeq": dictionary.NUMERIC,
                        "licenseNum": dictionary.STRING,
                        "licenseName": dictionary.STRING,
                        "acqDate": dictionary.DATE
                    }
        }
    };
    getUserLicenseInfo = {
        "url" : "/humanresource/getlicenseinfo/{licenseSeq}",
        "method" : dictionary.GET,
        "body" : [], 
        "response" : {
                "licenseSeq": dictionary.NUMERIC,
                "userId": dictionary.STRING,
                "licenseNum": dictionary.STRING,
                "licenseName": dictionary.STRING,
                "acqDate": dictionary.DATE
            }
        };
    insertUserLicense = {
        "url" : "/humanresource/insertlicenseinfo",
        "method" : dictionary.POST,
        "body" : {
            "userId": dictionary.STRING,
            "licenseNum": dictionary.STRING,
            "licenseName": dictionary.STRING,
            "acqDate": dictionary.DATE
        },
        "response" : {
            "result": {
                "isSuccess": dictionary.BOOLEAN,
                "message": dictionary.STRING,
                "ID":dictionary.NUMERIC
            }
        }
    };
    updateUserLicense = {
        "url" : "/humanresource/updatelicenseinfo",
        "method" : dictionary.POST,
        "body" : {
            "licenseSeq": dictionary.NUMERIC,
            "userId": dictionary.STRING,
            "licenseNum": dictionary.STRING,
            "licenseName": dictionary.STRING,
            "acqDate": dictionary.DATE
        },
        "response" : {
            "result": {
                "isSuccess": dictionary.BOOLEAN,
                "message": dictionary.STRING,
                "ID":dictionary.NUMERIC
            }
        }
    };
    deleteUserLicense = {
        "url" : "/humanresource/deletelicenseinfo",
        "method" : dictionary.POST,
        "body" : [], //personLicense ids
        "response" : {
            "result": {
                "isSuccess": dictionary.BOOLEAN,
                "message": dictionary.STRING
            }
        }
    };
    getUserEduPromotionList = {
        "url" : "/humanresource/Geteducationpromotionlist/{userId}/{locale}",
        "method" : dictionary.GET,
        "body" : {},
        "response" : {
                    "rows" : {
                        "educationPromotionSeq": dictionary.NUMERIC,
						"courseName": dictionary.NUMERIC,
						"institutionName": dictionary.STRING,
						"startDate": dictionary.DATE,
						"endDate": dictionary.DATE,
						"educationClsCdName": dictionary.STRING,
						"completeCdName": dictionary.STRING,
						"score": dictionary.STRING,
                    }
        }
    };
    getUserEduPromotionInfo = {
        "url" : "/humanresource/geteducationpromotioninfo/{educationPromotionSeq}",
        "method" : dictionary.GET,
        "body" : [], 
        "response" : {
            "educationPromotionSeq": dictionary.NUMERIC,
            "userId": dictionary.STRING,
            "courseName": dictionary.STRING,
            "institutionName": dictionary.STRING,
            "startDate": dictionary.DATE,
            "endDate": dictionary.DATE,
            "educationClsCd": dictionary.NUMERIC,
            "completeCd": dictionary.NUMERIC,
            "score": dictionary.STRING
            }
        };
    insertUserEduPromotion = {
        "url" : "/humanresource/Inserteducationpromotioninfo",
        "method" : dictionary.POST,
        "body" : {
            "userId": dictionary.STRING,
            "courseName": dictionary.STRING,
            "institutionName": dictionary.STRING,
            "startDate": dictionary.DATE,
            "endDate": dictionary.DATE,
            "educationClsCd": dictionary.NUMERIC,
            "completeCd": dictionary.NUMERIC,
            "score": dictionary.STRING
        },
        "response" : {
            "result": {
                "isSuccess": dictionary.BOOLEAN,
                "message": dictionary.STRING,
                "ID":dictionary.NUMERIC
            }
        }
    };
    updateUserEduPromotion = {
        "url" : "/humanresource/updateeducationpromotioninfo",
        "method" : dictionary.POST,
        "body" : {
            "educationPromotionSeq": dictionary.NUMERIC,
            "userId": dictionary.STRING,
            "courseName": dictionary.STRING,
            "institutionName": dictionary.STRING,
            "startDate": dictionary.DATE,
            "endDate": dictionary.DATE,
            "educationClsCd": dictionary.NUMERIC,
            "completeCd": dictionary.NUMERIC,
            "score": dictionary.STRING
        },
        "response" : {
            "result": {
                "isSuccess": dictionary.BOOLEAN,
                "message": dictionary.STRING,
                "ID":dictionary.NUMERIC
            }
        }
    };
    deleteUserEduPromotion = {
        "url" : "/humanresource/deleteeducationpromotioninfo",
        "method" : dictionary.POST,
        "body" : [], //educationPromotionSeq
        "response" : {
            "result": {
                "isSuccess": dictionary.BOOLEAN,
                "message": dictionary.STRING
            }
        }
    };
    getUserFireTrainList = {
        "url" : "/humanresource/getfiretrainlist/{userId}",
        "method" : dictionary.GET,
        "body" : {},
        "response" : {
                    "rows" : {
                        "fireTrainSeq": dictionary.NUMERIC,
						"trainName": dictionary.STRING,
						"startDate": dictionary.DATE,
						"endDate": dictionary.DATE,
						"host": dictionary.STRING,
                        "perf": dictionary.STRING,
						"orgName": dictionary.STRING
                    }
        }
    };
    getUserFireTrainInfo = {
        "url" : "/humanresource/getfiretraininfo/{fireTrainSeq}",
        "method" : dictionary.GET,
        "body" : [], 
        "response" : {
            "fireTrainSeq": dictionary.NUMERIC,
            "userId": dictionary.STRING,
			"trainName": dictionary.STRING,
			"startDate": dictionary.DATE,
			"endDate": dictionary.DATE,
			"host": dictionary.STRING,
            "perf": dictionary.STRING,
			"orgName": dictionary.STRING
            }
        };
    insertUserFireTrain = {
        "url" : "/humanresource/insertfiretrainInfo",
        "method" : dictionary.POST,
        "body" : {
            "userId": dictionary.STRING,
            "trainName": dictionary.STRING,
			"startDate": dictionary.DATE,
			"endDate": dictionary.DATE,
			"host": dictionary.STRING,
            "perf": dictionary.STRING,
			"orgName": dictionary.STRING
        },
        "response" : {
            "result": {
                "isSuccess": dictionary.BOOLEAN,
                "message": dictionary.STRING,
                "ID":dictionary.NUMERIC
            }
        }
    };
    updateUserFireTrain = {
        "url" : "/humanresource/updatefiretraininfo",
        "method" : dictionary.POST,
        "body" : {
            "fireTrainSeq": dictionary.NUMERIC,
            "userId": dictionary.STRING,
			"trainName": dictionary.STRING,
			"startDate": dictionary.DATE,
			"endDate": dictionary.DATE,
			"host": dictionary.STRING,
            "perf": dictionary.STRING,
			"orgName": dictionary.STRING
        },
        "response" : {
            "result": {
                "isSuccess": dictionary.BOOLEAN,
                "message": dictionary.STRING,
                "ID":dictionary.NUMERIC
            }
        }
    };
    deleteUserFireTrain = {
        "url" : "/humanresource/deletefiretraininfo",
        "method" : dictionary.POST,
        "body" : [], //fireTrainSeq
        "response" : {
            "result": {
                "isSuccess": dictionary.BOOLEAN,
                "message": dictionary.STRING
            }
        }
    };
    getNextGroupCodes = {
        "url" : "/systemmanage/getnextgroupcode/{groupCode}/{locale}",
        "method" : dictionary.GET,
        "body" : {},
        "response" : {
            "groupCodeTitle":dictionary.STRING,
            "rows" : {
                "cdGrp": dictionary.STRING,
			    "cdGrpName": dictionary.STRING,
			    "cdGrpDesc": dictionary.STRING,
			    "leafYn": dictionary.BOOLEAN
            }
            
        }
    };
    getCodesForGroupCode = {
        "url" : "/systemmanage/getcodesforgroupcode/{groupCode}/{locale}",
        "method" : dictionary.GET,
        "body" : {},
        "response" : {
            "cd": dictionary.NUMERIC,
			"cdName": dictionary.STRING,
			"cdDesc": dictionary.STRING
        }
    };
    getObjectList = {
        "url" : "/object/objectlist/{locale}",
        "method" : dictionary.POST,
        "body" : {
                    "page" : {
                                "rowsPerPage" : dictionary.NUMERIC,
                                "pageNumber" : dictionary.NUMERIC
                            },
                    "wardId" : dictionary.STRING,
                    "constName" : dictionary.STRING,
                    "objManNum" : dictionary.STRING,
                    "objCd" : dictionary.NUMERIC,
                    "usedCd" : dictionary.NUMERIC,
                    "address" : dictionary.STRING
                },
        "response" : {
                    "page" : {
                                "rowsPerPage" : dictionary.NUMERIC,
                                "pageNumber" : dictionary.NUMERIC,
                                "totalCount" : dictionary.NUMERIC
                            },
                    "rows" : {
                        "objId": dictionary.NUMERIC,
                        "upwardName": dictionary.STRING,
                        "wardName": dictionary.STRING,
                        "objManNum": dictionary.STRING,
                        "constName": dictionary.STRING,
                        "bldgNm": dictionary.STRING,
                        "address": dictionary.STRING,
                        "objCdName": dictionary.STRING,
                        "usedCdName": dictionary.STRING
                    }
        }
    };
    getObjectInfo = {
        "url" : "/object/getobjectinfo/{id}",
        "method" : dictionary.GET,
        "body" : {},
        "response" : {
                        "objId": dictionary.NUMERIC,
                        "upwardId": dictionary.STRING,
                        "wardId": dictionary.STRING,
                        "objManNum": dictionary.STRING,
                        "usedCd": dictionary.NUMERIC,
                        "objCd": dictionary.NUMERIC,
                        "constName": dictionary.STRING,
                        "rpsnFirmName": dictionary.STRING,
                        "zipCode": dictionary.STRING,
                        "address": dictionary.STRING,
                        "objStdCd": dictionary.NUMERIC,
                        "bldgNm": dictionary.STRING,
                        "x": dictionary.STRING,
                        "y": dictionary.STRING,
                        "weakobjYn": dictionary.BOOLEAN,
                        "selfobjYn": dictionary.BOOLEAN,
                        "adgInfo": dictionary.STRING,
                        "dayTel": dictionary.STRING,
                        "nightTel": dictionary.STRING,
                        "prevdstTel": dictionary.STRING,
                        "useConfmDate": dictionary.STRING,
                        "institYn": dictionary.BOOLEAN,
                        "institClsCd": dictionary.NUMERIC,
                        "pubfaclYn": dictionary.BOOLEAN,
                        "weakobjCd": dictionary.NUMERIC,
                        "subUseDesc": dictionary.STRING,
                        "tunnelClsCd": dictionary.NUMERIC,
                        "tunnelLength": dictionary.NUMERIC
                    }
    };
    insertObject = {
        "url" : "/object/insertobject/{userId}",
        "method" : dictionary.POST,
        "body" : {
                    "wardId": dictionary.STRING,
                    "objManNum": dictionary.STRING,
                    "usedCd":dictionary.NUMERIC,
                    "objCd":dictionary.NUMERIC,
                    "useYn": dictionary.BOOLEAN,
                    "constName":dictionary.STRING,
                    "rpsnFirmName":dictionary.STRING,
                    "zipCode":dictionary.STRING,
                    "address":dictionary.STRING,
                    "objStdCd":dictionary.NUMERIC,
                    "bldgNm":dictionary.STRING,
                    "x":dictionary.STRING,
                    "y":dictionary.STRING,
                    "weakobjYn":dictionary.BOOLEAN,
                    "selfobjYn":dictionary.BOOLEAN,
                    "adgInfo":dictionary.STRING,
                    "dayTel":dictionary.STRING,
                    "nightTel":dictionary.STRING,
                    "prevdstTel":dictionary.STRING,
                    "useConfmDate":dictionary.DATE,
                    "institYn":dictionary.BOOLEAN,
                    "institClsCd":dictionary.NUMERIC,
                    "pubfaclYn":dictionary.BOOLEAN,
                    "weakobjCd":dictionary.NUMERIC,
                    "subUseDesc":dictionary.STRING,
                    "tunnelClsCd":dictionary.STRING,
                    "tunnelLength":dictionary.NUMERIC
                },
        "response" : {
            "result": {
                "isSuccess": dictionary.BOOLEAN,
                "message": dictionary.STRING,
                "ID":dictionary.STRING
            }
        }
    };
    updateObject = {
        "url" : "/object/updateobject/{userId}",
        "method" : dictionary.POST,
        "body" : {
                    "objId": dictionary.NUMERIC,
                    "wardId": dictionary.STRING,
                    "objManNum": dictionary.STRING,
                    "usedCd":dictionary.NUMERIC,
                    "objCd":dictionary.NUMERIC,
                    "constName":dictionary.STRING,
                    "rpsnFirmName":dictionary.STRING,
                    "zipCode":dictionary.STRING,
                    "address":dictionary.STRING,
                    "objStdCd":dictionary.NUMERIC,
                    "bldgNm":dictionary.STRING,
                    "x":dictionary.STRING,
                    "y":dictionary.STRING,
                    "weakobjYn":dictionary.BOOLEAN,
                    "selfobjYn":dictionary.BOOLEAN,
                    "adgInfo":dictionary.STRING,
                    "dayTel":dictionary.STRING,
                    "nightTel":dictionary.STRING,
                    "prevdstTel":dictionary.STRING,
                    "useConfmDate":dictionary.DATE,
                    "institYn":dictionary.BOOLEAN,
                    "institClsCd":dictionary.NUMERIC,
                    "pubfaclYn":dictionary.BOOLEAN,
                    "weakobjCd":dictionary.NUMERIC,
                    "subUseDesc":dictionary.STRING,
                    "tunnelClsCd":dictionary.STRING,
                    "tunnelLength":dictionary.NUMERIC
                },
        "response" : {
            "result": {
                "isSuccess": dictionary.BOOLEAN,
                "message": dictionary.STRING,
                "ID":dictionary.NUMERIC
            }
        }
    };
    deleteObject = {
        "url" : "/object/deleteobject",
        "method" : dictionary.POST,
        "body" : [], //Ward ids
        "response" : {
            "result": {
                "isSuccess": dictionary.BOOLEAN,
                "message": dictionary.STRING
            }
        }
    };

    getBuildingList = {
        "url" : "/object/buildinglist/{objId}/{locale}",
        "method" : dictionary.GET,
        "body" : {},
        "response" : {
            "bldgSeq": dictionary.NUMERIC,
            "objId" : dictionary.NUMERIC,
            "bldgName" :dictionary.STRING,
            "struct1" : dictionary.STRING,
            "struct2" : dictionary.STRING,
            "struct3" : dictionary.STRING,
            "mainUse" : dictionary.STRING,
            "subUse" : dictionary.STRING,
            "ustoryCnt" : dictionary.NUMERIC,
            "bstoryCnt" : dictionary.NUMERIC,
            "lotArea" : dictionary.NUMERIC,
            "floorArea" : dictionary.NUMERIC,
            "totArea" : dictionary.NUMERIC
        }
    };
    getBuildingListForComboBox = {
        "url" : "/object/buildinglist/{objId}/{locale}",
        "method" : dictionary.GET,
        "body" : {},
        "response" : {
            "bldgSeq": dictionary.NUMERIC,
            "bldgName" :dictionary.STRING
        }
    };
    getBuildingInfo = {
        "url" : "/object/getbuildinginfo/{objId}/{bldgSeq}",
        "method" : dictionary.GET,
        "body" : {},
        "response" : {
                    "objId":dictionary.NUMERIC
                    ,"bldgSeq":dictionary.NUMERIC
                    ,"bldgName":dictionary.STRING
                    ,"struct1":dictionary.NUMERIC
                    ,"struct2":dictionary.NUMERIC
                    ,"struct3":dictionary.NUMERIC
                    ,"ustoryCnt":dictionary.NUMERIC
                    ,"bstoryCnt":dictionary.NUMERIC
                    ,"floorArea":dictionary.NUMERIC
                    ,"totArea":dictionary.NUMERIC
                    ,"sescStairCnt":dictionary.NUMERIC
                    ,"lotArea":dictionary.NUMERIC
                    ,"escStairCnt":dictionary.NUMERIC
                    ,"comStairCnt":dictionary.NUMERIC
                    ,"outStairCnt":dictionary.NUMERIC
                    ,"inclineCnt":dictionary.NUMERIC
                    ,"emgliftCnt":dictionary.NUMERIC
                    ,"ecalCnt":dictionary.NUMERIC
                    ,"exitCnt":dictionary.NUMERIC
                    ,"rootYn":dictionary.BOOLEAN
                    ,"homeCnt":dictionary.NUMERIC
                    ,"rpsnBldgYn":dictionary.BOOLEAN
                    ,"houseCnt":dictionary.NUMERIC
                    ,"entrpsCnt":dictionary.NUMERIC
                    ,"inhbtntCnt":dictionary.NUMERIC
                    ,"bldgHeight":dictionary.NUMERIC
                    ,"elvtrCnt":dictionary.NUMERIC
                    ,"removeDate":dictionary.DATE
                    ,"removeYn":dictionary.BOOLEAN
                    ,"bldgMainUseCd":dictionary.NUMERIC
                    ,"bldgSubUseCd":dictionary.NUMERIC
                    ,"useConfmDate":dictionary.DATE
                    ,"useYn":dictionary.BOOLEAN
                    }
    };
    insertBuilding = {
        "url" : "/object/insertbuilding/{userId}",
        "method" : dictionary.POST,
        "body" : {
                 "bldgSeq":dictionary.NUMERIC
                ,"bldgName":dictionary.STRING
                ,"useYn": dictionary.BOOLEAN
                ,"struct1":dictionary.NUMERIC
                ,"struct2":dictionary.NUMERIC
                ,"struct3":dictionary.NUMERIC
                ,"ustoryCnt":dictionary.NUMERIC
                ,"bstoryCnt":dictionary.NUMERIC
                ,"floorArea":dictionary.NUMERIC
                ,"totArea":dictionary.NUMERIC
                ,"sescStairCnt":dictionary.NUMERIC
                ,"lotArea":dictionary.NUMERIC
                ,"escStairCnt":dictionary.NUMERIC
                ,"comStairCnt":dictionary.NUMERIC
                ,"outStairCnt":dictionary.NUMERIC
                ,"inclineCnt":dictionary.NUMERIC
                ,"emgliftCnt":dictionary.NUMERIC
                ,"ecalCnt":dictionary.NUMERIC
                ,"exitCnt":dictionary.NUMERIC
                ,"rootYn":dictionary.BOOLEAN
                ,"homeCnt":dictionary.NUMERIC
                ,"rpsnBldgYn":dictionary.BOOLEAN
                ,"houseCnt":dictionary.NUMERIC
                ,"entrpsCnt":dictionary.NUMERIC
                ,"inhbtntCnt":dictionary.NUMERIC
                ,"bldgHeight":dictionary.NUMERIC
                ,"elvtrCnt":dictionary.NUMERIC
                ,"removeDate":dictionary.DATE
                ,"removeYn":dictionary.BOOLEAN
                ,"bldgMainUseCd":dictionary.NUMERIC
                ,"bldgSubUseCd":dictionary.NUMERIC
                ,"useConfmDate":dictionary.DATE
                },
        "response" : {
            "result": {
                "isSuccess": dictionary.BOOLEAN,
                "message": dictionary.STRING,
                "ID":dictionary.STRING
            }
        }
    };
    updateBuilding = {
        "url" : "/object/updatebuilding/{userId}",
        "method" : dictionary.POST,
        "body" : {
                "objId":dictionary.NUMERIC
                ,"bldgSeq":dictionary.NUMERIC
                ,"bldgName":dictionary.STRING
                ,"struct1":dictionary.NUMERIC
                ,"struct2":dictionary.NUMERIC
                ,"struct3":dictionary.NUMERIC
                ,"ustoryCnt":dictionary.NUMERIC
                ,"bstoryCnt":dictionary.NUMERIC
                ,"floorArea":dictionary.NUMERIC
                ,"totArea":dictionary.NUMERIC
                ,"sescStairCnt":dictionary.NUMERIC
                ,"lotArea":dictionary.NUMERIC
                ,"escStairCnt":dictionary.NUMERIC
                ,"comStairCnt":dictionary.NUMERIC
                ,"outStairCnt":dictionary.NUMERIC
                ,"inclineCnt":dictionary.NUMERIC
                ,"emgliftCnt":dictionary.NUMERIC
                ,"ecalCnt":dictionary.NUMERIC
                ,"exitCnt":dictionary.NUMERIC
                ,"rootYn":dictionary.BOOLEAN
                ,"homeCnt":dictionary.NUMERIC
                ,"rpsnBldgYn":dictionary.BOOLEAN
                ,"houseCnt":dictionary.NUMERIC
                ,"entrpsCnt":dictionary.NUMERIC
                ,"inhbtntCnt":dictionary.NUMERIC
                ,"bldgHeight":dictionary.NUMERIC
                ,"elvtrCnt":dictionary.NUMERIC
                ,"removeDate":dictionary.DATE
                ,"removeYn":dictionary.BOOLEAN
                ,"bldgMainUseCd":dictionary.NUMERIC
                ,"bldgSubUseCd":dictionary.NUMERIC
                ,"useConfmDate":dictionary.DATE
                ,"useYn":dictionary.BOOLEAN
                },
        "response" : {
            "result": {
                "isSuccess": dictionary.BOOLEAN,
                "message": dictionary.STRING,
                "ID":dictionary.NUMERIC
            }
        }
    };
    deleteBuilding = {
        "url" : "/object/deletebuilding",
        "method" : dictionary.POST,
        "body" : [], //[{objId:"",bldgSeq:""}]
        "response" : {
            "result": {
                "isSuccess": dictionary.BOOLEAN,
                "message": dictionary.STRING
            }
        }
    };

    getStoryList = {
        "url" : "/object/getstorylist/{objId}/{bldgSeq}",
        "method" : dictionary.GET,
        "body" : {},
        "response" : {
            "objId" : dictionary.NUMERIC,
            "bldgSeq" : dictionary.NUMERIC,
            "storyNo" : dictionary.STRING,
            "storyArea" : dictionary.NUMERIC,
            "fcontCnt" : dictionary.NUMERIC,
            "deco" : dictionary.STRING,
            "residentManCnt" : dictionary.NUMERIC,
            "accomManCnt" : dictionary.NUMERIC,
            "etc" : dictionary.STRING,
            "storySeq" : dictionary.NUMERIC
        }
    };

    getStoryListForComboBox = {
        "url" : "/object/getstorylist/{objId}/{bldgSeq}",
        "method" : dictionary.GET,
        "body" : {},
        "response" : {
            "storySeq" : dictionary.NUMERIC,
            "storyNo" : dictionary.STRING
        }
    };

    getStoryInfo = {
        "url" : "/object/getstoryinfo/{objId}/{bldgSeq}/{storySeq}",
        "method" : dictionary.GET,
        "body" : {},
        "response" : {
            "objId" : dictionary.NUMERIC,
            "bldgSeq" : dictionary.NUMERIC,
            "storyNo" : dictionary.STRING,
            "storyArea" : dictionary.NUMERIC,
            "fcontCnt" : dictionary.NUMERIC,
            "deco" : dictionary.STRING,
            "residentManCnt" : dictionary.NUMERIC,
            "accomManCnt" : dictionary.NUMERIC,
            "etc" : dictionary.STRING,
            "storySeq" : dictionary.NUMERIC,
            "useYn":dictionary.BOOLEAN
        }
    };

    insertStory = {
        "url" : "/object/insertstory/{userId}",
        "method" : dictionary.POST,
        "body" : {
            "objId" : dictionary.NUMERIC,
            "bldgSeq" : dictionary.NUMERIC,
            "storyNo" : dictionary.STRING,
            "storyArea" : dictionary.NUMERIC,
            "fcontCnt" : dictionary.NUMERIC,
            "deco" : dictionary.STRING,
            "residentManCnt" : dictionary.NUMERIC,
            "accomManCnt" : dictionary.NUMERIC,
            "etc" : dictionary.STRING,
            "storySeq" : dictionary.NUMERIC
        },
        "response" : {
            "result": {
                "isSuccess": dictionary.BOOLEAN,
                "message": dictionary.STRING,
                "ID":dictionary.STRING //storySeq
            }
        }
    };

    updateStory = {
        "url" : "/object/updatestory/{userId}",
        "method" : dictionary.POST,
        "body" : {
                    "objId" : dictionary.NUMERIC,
                    "bldgSeq" : dictionary.NUMERIC,
                    "storyNo" : dictionary.STRING,
                    "storyArea" : dictionary.NUMERIC,
                    "fcontCnt" : dictionary.NUMERIC,
                    "deco" : dictionary.STRING,
                    "residentManCnt" : dictionary.NUMERIC,
                    "accomManCnt" : dictionary.NUMERIC,
                    "etc" : dictionary.STRING,
                    "storySeq" : dictionary.NUMERIC,
                    "useYn":dictionary.BOOLEAN
                },
        "response" : {
            "result": {
                "isSuccess": dictionary.BOOLEAN,
                "message": dictionary.STRING,
                "ID":dictionary.NUMERIC
            }
        }
    };

    deleteStory = {
        "url" : "/object/deletestory",
        "method" : dictionary.POST,
        "body" : [], //[{objId:"",bldgSeq:"",storySeq:""}]
        "response" : {
            "result": {
                "isSuccess": dictionary.BOOLEAN,
                "message": dictionary.STRING
            }
        }
    };

    getStorySpecUsedList = {
        "url" : "/object/getstoryspecusedlist/{objId}/{bldgSeq}/{storySeq}/{locale}",
        "method" : dictionary.GET,
        "body" : {},
        "response" : {
            "objId" : dictionary.NUMERIC,
            "bldgSeq" : dictionary.NUMERIC,
            "storySeq" : dictionary.NUMERIC,
            "specUseSeq" : dictionary.NUMERIC,
            "storyNo" : dictionary.STRING,
            "specUseClsCdName" : dictionary.STRING,
            "area" : dictionary.NUMERIC
        }
    };

    getStorySpecUsedInfo = {
        "url" : "/object/getstoryspecusedinfo/{objId}/{bldgSeq}/{storySeq}/{specUseSeq}/{locale}",
        "method" : dictionary.GET,
        "body" : {},
        "response" : {
            "objId" : dictionary.NUMERIC,
            "bldgSeq" : dictionary.NUMERIC,
            "storySeq" : dictionary.NUMERIC,
            "specUseSeq" : dictionary.NUMERIC,
            "specUseClsCd" : dictionary.NUMERIC,
            "specUseClsCdName" : dictionary.STRING,
            "area" : dictionary.NUMERIC,
            "remark" : dictionary.STRING,
            "useYn":dictionary.BOOLEAN
        }
    };

    insertStorySpecUsed = {
        "url" : "/object/insertstoryspecused/{userId}",
        "method" : dictionary.POST,
        "body" : {
                    "objId" : dictionary.NUMERIC,
                    "bldgSeq" : dictionary.NUMERIC,
                    "storySeq" : dictionary.NUMERIC,
                    "specUseSeq" : dictionary.NUMERIC,
                    "specUseClsCd" : dictionary.NUMERIC,
                    "area" : dictionary.NUMERIC,
                    "remark" : dictionary.STRING,
                    "useYn": dictionary.BOOLEAN
        },
        "response" : {
            "result": {
                "isSuccess": dictionary.BOOLEAN,
                "message": dictionary.STRING,
                "ID":dictionary.STRING //storySeq
            }
        }
    };

    updateStorySpecUsed = {
        "url" : "/object/updatestoryspecused/{userId}",
        "method" : dictionary.POST,
        "body" : {
                    "objId" : dictionary.NUMERIC,
                    "bldgSeq" : dictionary.NUMERIC,
                    "storySeq" : dictionary.NUMERIC,
                    "specUseSeq" : dictionary.NUMERIC,
                    "specUseClsCd" : dictionary.NUMERIC,
                    "area" : dictionary.NUMERIC,
                    "remark" : dictionary.STRING,
                    "useYn": dictionary.BOOLEAN
                },
        "response" : {
            "result": {
                "isSuccess": dictionary.BOOLEAN,
                "message": dictionary.STRING,
                "ID":dictionary.NUMERIC
            }
        }
    };

    deleteStorySpecUsed = {
        "url" : "/object/deletestoryspecused",
        "method" : dictionary.POST,
        "body" : [], //[{objId:"",bldgSeq:"",storySeq:"",specUseSeq:""}]
        "response" : {
            "result": {
                "isSuccess": dictionary.BOOLEAN,
                "message": dictionary.STRING
            }
        }
    };

    getFireEquipList = {
        "url" : "/object/getfireequiplist",
        "method" : dictionary.POST,
        "body" : {
            "objId" : dictionary.NUMERIC,
            "bldgSeq" : dictionary.NUMERIC,
            "storySeq" : dictionary.NUMERIC
        },
        "response" : {
            "objId" : dictionary.STRING,
            "bldgSeq" : dictionary.NUMERIC,
            "storySeq" : dictionary.NUMERIC,
            "bldgName" : dictionary.STRING,
            "storyNo" : dictionary.STRING,
            "inExtingCnt": dictionary.NUMERIC,
            "outExtingCnt": dictionary.NUMERIC,
            "extingPumpCnt": dictionary.NUMERIC,
            "sprinklerHCnt": dictionary.NUMERIC,
            "sprinklerAvCnt": dictionary.NUMERIC,
            "sprayExtingHCnt" : dictionary.NUMERIC,
            "sprayExtingAvCnt" : dictionary.NUMERIC,
            "poExtingHCnt" : dictionary.NUMERIC,
            "poExtingAvCnt" : dictionary.NUMERIC,
            "carbonDioxHCnt" : dictionary.NUMERIC,
            "carbonDioxAvCnt" : dictionary.NUMERIC,
            "halogenCompHCnt": dictionary.NUMERIC,
            "harlogenCompAvCnt" : dictionary.NUMERIC,
            "powderExtingHCnt" : dictionary.NUMERIC,
            "powderExtingAvCnt" : dictionary.NUMERIC,
            "slideCnt" : dictionary.NUMERIC,
            "ladderCnt" : dictionary.NUMERIC,
            "rescueCnt" : dictionary.NUMERIC,
            "descSlowDeviceCnt" : dictionary.NUMERIC,
            "measureEquipCnt" : dictionary.NUMERIC,
            "measureRopeCnt" : dictionary.NUMERIC,
            "safeMatCnt" : dictionary.NUMERIC,
            "rescueEquipCnt" : dictionary.NUMERIC,
            "emgLightCnt" : dictionary.NUMERIC,
            "waterExtingCnt" : dictionary.NUMERIC,
            "extingWaterCnt" : dictionary.NUMERIC,
            "lwtrCnt" : dictionary.NUMERIC,
            "wtrpipeCnt" : dictionary.NUMERIC,
            "waterSpringklingCnt" : dictionary.NUMERIC,
            "emgPlugCnt" : dictionary.NUMERIC,
            "wirelessCommCnt" : dictionary.NUMERIC,
            "extingCnt" : dictionary.NUMERIC,
            "simplctyExtingCnt" : dictionary.NUMERIC,
            "emgWaringCnt" : dictionary.NUMERIC,
            "emgBrodcCnt" : dictionary.NUMERIC,
            "lkgeWaringCnt" : dictionary.NUMERIC,
            "autoFireFindSensCnt" : dictionary.NUMERIC,
            "autoFireFindCircuitCnt" : dictionary.NUMERIC,
            "autoFireNewsfCnt" : dictionary.NUMERIC,
            "gasLkgeWaringCnt" : dictionary.NUMERIC,
            "induceLightCnt" : dictionary.NUMERIC,
            "induceSignpostCnt" : dictionary.NUMERIC,
            "hydEquipEtcCnt" : dictionary.NUMERIC,
            "resmokeCnt" : dictionary.NUMERIC,
            "curtainCnt" : dictionary.NUMERIC,
            "carssetteCnt" : dictionary.NUMERIC,
            "resistEtcCnt" : dictionary.NUMERIC
        }
    };

    getFireEuipInfo = {
        "url" : "/object/getfireequipinfo/{objId}/{bldgSeq}/{storySeq}",
        "method" : dictionary.GET,
        "body" : {},
        "response" : {
            "objId" : dictionary.STRING,
            "bldgSeq" : dictionary.NUMERIC,
            "storySeq" : dictionary.NUMERIC,
            "inExtingCnt": dictionary.NUMERIC,
            "outExtingCnt": dictionary.NUMERIC,
            "extingPumpCnt": dictionary.NUMERIC,
            "sprinklerHCnt": dictionary.NUMERIC,
            "sprinklerAvCnt": dictionary.NUMERIC,
            "sprayExtingHCnt" : dictionary.NUMERIC,
            "sprayExtingAvCnt" : dictionary.NUMERIC,
            "poExtingHCnt" : dictionary.NUMERIC,
            "poExtingAvCnt" : dictionary.NUMERIC,
            "carbonDioxHCnt" : dictionary.NUMERIC,
            "carbonDioxAvCnt" : dictionary.NUMERIC,
            "halogenCompHCnt": dictionary.NUMERIC,
            "harlogenCompAvCnt" : dictionary.NUMERIC,
            "powderExtingHCnt" : dictionary.NUMERIC,
            "powderExtingAvCnt" : dictionary.NUMERIC,
            "slideCnt" : dictionary.NUMERIC,
            "ladderCnt" : dictionary.NUMERIC,
            "rescueCnt" : dictionary.NUMERIC,
            "descSlowDeviceCnt" : dictionary.NUMERIC,
            "measureEquipCnt" : dictionary.NUMERIC,
            "measureRopeCnt" : dictionary.NUMERIC,
            "safeMatCnt" : dictionary.NUMERIC,
            "rescueEquipCnt" : dictionary.NUMERIC,
            "emgLightCnt" : dictionary.NUMERIC,
            "waterExtingCnt" : dictionary.NUMERIC,
            "extingWaterCnt" : dictionary.NUMERIC,
            "lwtrCnt" : dictionary.NUMERIC,
            "wtrpipeCnt" : dictionary.NUMERIC,
            "waterSpringklingCnt" : dictionary.NUMERIC,
            "emgPlugCnt" : dictionary.NUMERIC,
            "wirelessCommCnt" : dictionary.NUMERIC,
            "extingCnt" : dictionary.NUMERIC,
            "simplctyExtingCnt" : dictionary.NUMERIC,
            "emgWaringCnt" : dictionary.NUMERIC,
            "emgBrodcCnt" : dictionary.NUMERIC,
            "lkgeWaringCnt" : dictionary.NUMERIC,
            "autoFireFindSensCnt" : dictionary.NUMERIC,
            "autoFireFindCircuitCnt" : dictionary.NUMERIC,
            "autoFireNewsfCnt" : dictionary.NUMERIC,
            "gasLkgeWaringCnt" : dictionary.NUMERIC,
            "induceLightCnt" : dictionary.NUMERIC,
            "induceSignpostCnt" : dictionary.NUMERIC,
            "hydEquipEtcCnt" : dictionary.NUMERIC,
            "resmokeCnt" : dictionary.NUMERIC,
            "curtainCnt" : dictionary.NUMERIC,
            "carssetteCnt" : dictionary.NUMERIC,
            "resistEtcCnt" : dictionary.NUMERIC,
            "useYn" : dictionary.BOOLEAN
        }
    };

    insertFireEuip = {
        "url" : "/object/insertfireequip/{userId}",
        "method" : dictionary.POST,
        "body" : {
            "objId" : dictionary.STRING,
            "bldgSeq" : dictionary.NUMERIC,
            "storySeq" : dictionary.NUMERIC,
            "inExtingCnt": dictionary.NUMERIC,
            "outExtingCnt": dictionary.NUMERIC,
            "extingPumpCnt": dictionary.NUMERIC,
            "sprinklerHCnt": dictionary.NUMERIC,
            "sprinklerAvCnt": dictionary.NUMERIC,
            "sprayExtingHCnt" : dictionary.NUMERIC,
            "sprayExtingAvCnt" : dictionary.NUMERIC,
            "poExtingHCnt" : dictionary.NUMERIC,
            "poExtingAvCnt" : dictionary.NUMERIC,
            "carbonDioxHCnt" : dictionary.NUMERIC,
            "carbonDioxAvCnt" : dictionary.NUMERIC,
            "halogenCompHCnt": dictionary.NUMERIC,
            "harlogenCompAvCnt" : dictionary.NUMERIC,
            "powderExtingHCnt" : dictionary.NUMERIC,
            "powderExtingAvCnt" : dictionary.NUMERIC,
            "slideCnt" : dictionary.NUMERIC,
            "ladderCnt" : dictionary.NUMERIC,
            "rescueCnt" : dictionary.NUMERIC,
            "descSlowDeviceCnt" : dictionary.NUMERIC,
            "measureEquipCnt" : dictionary.NUMERIC,
            "measureRopeCnt" : dictionary.NUMERIC,
            "safeMatCnt" : dictionary.NUMERIC,
            "rescueEquipCnt" : dictionary.NUMERIC,
            "emgLightCnt" : dictionary.NUMERIC,
            "waterExtingCnt" : dictionary.NUMERIC,
            "extingWaterCnt" : dictionary.NUMERIC,
            "lwtrCnt" : dictionary.NUMERIC,
            "wtrpipeCnt" : dictionary.NUMERIC,
            "waterSpringklingCnt" : dictionary.NUMERIC,
            "emgPlugCnt" : dictionary.NUMERIC,
            "wirelessCommCnt" : dictionary.NUMERIC,
            "extingCnt" : dictionary.NUMERIC,
            "simplctyExtingCnt" : dictionary.NUMERIC,
            "emgWaringCnt" : dictionary.NUMERIC,
            "emgBrodcCnt" : dictionary.NUMERIC,
            "lkgeWaringCnt" : dictionary.NUMERIC,
            "autoFireFindSensCnt" : dictionary.NUMERIC,
            "autoFireFindCircuitCnt" : dictionary.NUMERIC,
            "autoFireNewsfCnt" : dictionary.NUMERIC,
            "gasLkgeWaringCnt" : dictionary.NUMERIC,
            "induceLightCnt" : dictionary.NUMERIC,
            "induceSignpostCnt" : dictionary.NUMERIC,
            "hydEquipEtcCnt" : dictionary.NUMERIC,
            "resmokeCnt" : dictionary.NUMERIC,
            "curtainCnt" : dictionary.NUMERIC,
            "carssetteCnt" : dictionary.NUMERIC,
            "resistEtcCnt" : dictionary.NUMERIC
        },
        "response" : {
            "result": {
                "isSuccess": dictionary.BOOLEAN,
                "message": dictionary.STRING,
                "ID":dictionary.STRING //storySeq
            }
        }
    };

    updateFireEuip = {
        "url" : "/object/updatefireequip/{userId}",
        "method" : dictionary.POST,
        "body" : {
            "objId" : dictionary.STRING,
            "bldgSeq" : dictionary.NUMERIC,
            "storySeq" : dictionary.NUMERIC,
            "inExtingCnt": dictionary.NUMERIC,
            "outExtingCnt": dictionary.NUMERIC,
            "extingPumpCnt": dictionary.NUMERIC,
            "sprinklerHCnt": dictionary.NUMERIC,
            "sprinklerAvCnt": dictionary.NUMERIC,
            "sprayExtingHCnt" : dictionary.NUMERIC,
            "sprayExtingAvCnt" : dictionary.NUMERIC,
            "poExtingHCnt" : dictionary.NUMERIC,
            "poExtingAvCnt" : dictionary.NUMERIC,
            "carbonDioxHCnt" : dictionary.NUMERIC,
            "carbonDioxAvCnt" : dictionary.NUMERIC,
            "halogenCompHCnt": dictionary.NUMERIC,
            "harlogenCompAvCnt" : dictionary.NUMERIC,
            "powderExtingHCnt" : dictionary.NUMERIC,
            "powderExtingAvCnt" : dictionary.NUMERIC,
            "slideCnt" : dictionary.NUMERIC,
            "ladderCnt" : dictionary.NUMERIC,
            "rescueCnt" : dictionary.NUMERIC,
            "descSlowDeviceCnt" : dictionary.NUMERIC,
            "measureEquipCnt" : dictionary.NUMERIC,
            "measureRopeCnt" : dictionary.NUMERIC,
            "safeMatCnt" : dictionary.NUMERIC,
            "rescueEquipCnt" : dictionary.NUMERIC,
            "emgLightCnt" : dictionary.NUMERIC,
            "waterExtingCnt" : dictionary.NUMERIC,
            "extingWaterCnt" : dictionary.NUMERIC,
            "lwtrCnt" : dictionary.NUMERIC,
            "wtrpipeCnt" : dictionary.NUMERIC,
            "waterSpringklingCnt" : dictionary.NUMERIC,
            "emgPlugCnt" : dictionary.NUMERIC,
            "wirelessCommCnt" : dictionary.NUMERIC,
            "extingCnt" : dictionary.NUMERIC,
            "simplctyExtingCnt" : dictionary.NUMERIC,
            "emgWaringCnt" : dictionary.NUMERIC,
            "emgBrodcCnt" : dictionary.NUMERIC,
            "lkgeWaringCnt" : dictionary.NUMERIC,
            "autoFireFindSensCnt" : dictionary.NUMERIC,
            "autoFireFindCircuitCnt" : dictionary.NUMERIC,
            "autoFireNewsfCnt" : dictionary.NUMERIC,
            "gasLkgeWaringCnt" : dictionary.NUMERIC,
            "induceLightCnt" : dictionary.NUMERIC,
            "induceSignpostCnt" : dictionary.NUMERIC,
            "hydEquipEtcCnt" : dictionary.NUMERIC,
            "resmokeCnt" : dictionary.NUMERIC,
            "curtainCnt" : dictionary.NUMERIC,
            "carssetteCnt" : dictionary.NUMERIC,
            "resistEtcCnt" : dictionary.NUMERIC,
            "useYn" : dictionary.BOOLEAN
                },
        "response" : {
            "result": {
                "isSuccess": dictionary.BOOLEAN,
                "message": dictionary.STRING,
                "ID":dictionary.NUMERIC
            }
        }
    };

    deleteFireEuip = {
        "url" : "/object/deletefireequip/{userId}",
        "method" : dictionary.POST,
        "body" : [], //[{objId:"",bldgSeq:"",storySeq:""}]
        "response" : {
            "result": {
                "isSuccess": dictionary.BOOLEAN,
                "message": dictionary.STRING
            }
        }
    };

    getObjPlanInfo = {
        "url" : "/object/getobjplan/{objId}",
        "method" : dictionary.GET,
        "body" : {},
        "response" : {
            "objId":dictionary.NUMERIC,
            "envDesc":dictionary.STRING,
            "expandRsn":dictionary.STRING,
            "rescEscCplan":dictionary.STRING,
            "fmngIssue":dictionary.STRING,
            "selfvolunOrg":dictionary.STRING,
            "realestExptAmt":dictionary.NUMERIC,
            "mvestExptAmt":dictionary.NUMERIC,
            "totAmt":dictionary.NUMERIC,
            "ward1Dist":dictionary.NUMERIC,
            "ward2Dist":dictionary.NUMERIC,
            "escPlace":dictionary.STRING,
            "trainingGuideDesc":dictionary.STRING,
            "etcRemark":dictionary.STRING,
            "useYn" : dictionary.BOOLEAN
        }
    };

    insertObjPlan = {
        "url" : "/object/insertobjplan/{userId}",
        "method" : dictionary.POST,
        "body" : {
            "objId":dictionary.NUMERIC,
            "envDesc":dictionary.STRING,
            "expandRsn":dictionary.STRING,
            "rescEscCplan":dictionary.STRING,
            "fmngIssue":dictionary.STRING,
            "selfvolunOrg":dictionary.STRING,
            "realestExptAmt":dictionary.NUMERIC,
            "mvestExptAmt":dictionary.NUMERIC,
            "totAmt":dictionary.NUMERIC,
            "ward1Dist":dictionary.NUMERIC,
            "ward2Dist":dictionary.NUMERIC,
            "escPlace":dictionary.STRING,
            "trainingGuideDesc":dictionary.STRING,
            "etcRemark":dictionary.STRING
        },
        "response" : {
            "result": {
                "isSuccess": dictionary.BOOLEAN,
                "message": dictionary.STRING,
                "ID":dictionary.STRING //storySeq
            }
        }
    };

    updateObjPlan = {
        "url" : "/object/updateobjplan/{userId}",
        "method" : dictionary.POST,
        "body" : {
            "objId":dictionary.NUMERIC,
            "envDesc":dictionary.STRING,
            "expandRsn":dictionary.STRING,
            "rescEscCplan":dictionary.STRING,
            "fmngIssue":dictionary.STRING,
            "selfvolunOrg":dictionary.STRING,
            "realestExptAmt":dictionary.NUMERIC,
            "mvestExptAmt":dictionary.NUMERIC,
            "totAmt":dictionary.NUMERIC,
            "ward1Dist":dictionary.NUMERIC,
            "ward2Dist":dictionary.NUMERIC,
            "escPlace":dictionary.STRING,
            "trainingGuideDesc":dictionary.STRING,
            "etcRemark":dictionary.STRING,
            "useYn" : dictionary.BOOLEAN
                },
        "response" : {
            "result": {
                "isSuccess": dictionary.BOOLEAN,
                "message": dictionary.STRING,
                "ID":dictionary.NUMERIC
            }
        }
    };

    deleteObjPlan = {
        "url" : "/object/deleteobjplan/{userId}",
        "method" : dictionary.POST,
        "body" : [], //[objIds]
        "response" : {
            "result": {
                "isSuccess": dictionary.BOOLEAN,
                "message": dictionary.STRING
            }
        }
    };

    getBuildingHistoryList = {
        "url" : "/object/getbuildinghistorylist/{objId}/{locale}",
        "method" : dictionary.GET,
        "body" : {},
        "response" : {
            "objId" : dictionary.NUMERIC,
            "objhistSeq" : dictionary.NUMERIC,
            "bldgName" : dictionary.STRING,
            "histTypeCdName" : dictionary.STRING,
            "constUse" : dictionary.STRING,
            "struct1Name" : dictionary.STRING,
            "struct2Name" : dictionary.STRING,
            "struct3Name" : dictionary.STRING
        }
    };

    getBuildingHistoryInfo = {
        "url" : "/object/getbuildinghistoryinfo/{objId}/{objhistSeq}",
        "method" : dictionary.GET,
        "body" : {},
        "response" : {
            "objhistSeq" : dictionary.NUMERIC, 
            "objId" : dictionary.NUMERIC, 
            "bldgSeq" : dictionary.NUMERIC, 
            "histTypeCd" : dictionary.NUMERIC, 
            "constUse" : dictionary.STRING, 
            "struct1" : dictionary.NUMERIC, 
            "struct2" : dictionary.NUMERIC, 
            "struct3" : dictionary.NUMERIC, 
            "floorArea" : dictionary.NUMERIC, 
            "totArea" : dictionary.NUMERIC, 
            "ustoryCnt" : dictionary.NUMERIC, 
            "bstoryCnt" : dictionary.NUMERIC, 
            "grantDate" : dictionary.DATE, 
            "preuseYn" : dictionary.BOOLEAN, 
            "finishDate" : dictionary.DATE, 
            "etc" : dictionary.STRING, 
            "chgDesc" : dictionary.STRING, 
            "useYn" : dictionary.BOOLEAN
        }
    };

    insertBuildingHistory = {
        "url" : "/object/insertbuildinghistory/{userId}",
        "method" : dictionary.POST,
        "body" : {
            "objId" : dictionary.NUMERIC, 
            "bldgSeq" : dictionary.NUMERIC, 
            "histTypeCd" : dictionary.NUMERIC, 
            "constUse" : dictionary.STRING, 
            "struct1" : dictionary.NUMERIC, 
            "struct2" : dictionary.NUMERIC, 
            "struct3" : dictionary.NUMERIC, 
            "floorArea" : dictionary.NUMERIC, 
            "totArea" : dictionary.NUMERIC, 
            "ustoryCnt" : dictionary.NUMERIC, 
            "bstoryCnt" : dictionary.NUMERIC, 
            "grantDate" : dictionary.DATE, 
            "preuseYn" : dictionary.BOOLEAN, 
            "finishDate" : dictionary.DATE, 
            "etc" : dictionary.STRING, 
            "chgDesc" : dictionary.STRING, 
            "useYn" : dictionary.BOOLEAN
        },
        "response" : {
            "result": {
                "isSuccess": dictionary.BOOLEAN,
                "message": dictionary.STRING,
                "ID":dictionary.STRING //objhistSeq
            }
        }
    };

    updateBuildingHistory = {
        "url" : "/object/Updatebuildinghistory/{userId}",
        "method" : dictionary.POST,
        "body" : {
            "objhistSeq" : dictionary.NUMERIC, 
            "objId" : dictionary.NUMERIC, 
            "bldgSeq" : dictionary.NUMERIC, 
            "histTypeCd" : dictionary.NUMERIC, 
            "constUse" : dictionary.STRING, 
            "struct1" : dictionary.NUMERIC, 
            "struct2" : dictionary.NUMERIC, 
            "struct3" : dictionary.NUMERIC, 
            "floorArea" : dictionary.NUMERIC, 
            "totArea" : dictionary.NUMERIC, 
            "ustoryCnt" : dictionary.NUMERIC, 
            "bstoryCnt" : dictionary.NUMERIC, 
            "grantDate" : dictionary.DATE, 
            "preuseYn" : dictionary.BOOLEAN, 
            "finishDate" : dictionary.DATE, 
            "etc" : dictionary.STRING, 
            "chgDesc" : dictionary.STRING, 
            "useYn" : dictionary.BOOLEAN
                },
        "response" : {
            "result": {
                "isSuccess": dictionary.BOOLEAN,
                "message": dictionary.STRING,
                "ID":dictionary.NUMERIC
            }
        }
    };

    deleteBuildingHistory = {
        "url" : "/object/deletebuildinghistory/{userId}",
        "method" : dictionary.POST,
        "body" : [], //[{objId:"",objhistSeq:""}]
        "response" : {
            "result": {
                "isSuccess": dictionary.BOOLEAN,
                "message": dictionary.STRING
            }
        }
    };

    getArsonManagerList = {
        "url" : "/object/getfiremanagerList/{objId}",
        "method" : dictionary.GET,
        "body" : {},
        "response" : {
            "objId" : dictionary.NUMERIC,
            "fmgrSeq" : dictionary.NUMERIC,
            "name" : dictionary.STRING,
            "birthday" : dictionary.DATE,
            "title" : dictionary.STRING,
            "address" : dictionary.STRING,
            "telNum" : dictionary.STRING,
            "pubFmngYn" : dictionary.BOOLEAN,
            "mgeAgencyYn" : dictionary.BOOLEAN
        }
    };

    getArsonManagerInfo = {
        "url" : "/object/getfiremanagerInfo/{objId}/{fmgrSeq}",
        "method" : dictionary.GET,
        "body" : {},
        "response" : {
            "objId" : dictionary.NUMERIC, 
            "fmgrSeq" : dictionary.NUMERIC, 
            "firstName" : dictionary.STRING, 
            "lastName" : dictionary.STRING, 
            "birthday" : dictionary.DATE, 
            "zipCode" : dictionary.STRING, 
            "address" : dictionary.STRING, 
            "telNum" : dictionary.STRING, 
            "assignDate" : dictionary.DATE, 
            "dismissDate" : dictionary.DATE, 
            "citizenNo" : dictionary.STRING, 
            "pubFmngYn" : dictionary.BOOLEAN, 
            "dismissRemark" : dictionary.STRING, 
            "title" : dictionary.STRING, 
            "mgeAgencyYn" : dictionary.BOOLEAN,
            "useYn" : dictionary.BOOLEAN
        }
    };

    insertArsonManager = {
        "url" : "/object/insertfiremanager",
        "method" : dictionary.POST,
        "body" : {
            "objId" : dictionary.NUMERIC, 
            "fmgrSeq" : dictionary.NUMERIC, 
            "firstName" : dictionary.STRING, 
            "lastName" : dictionary.STRING, 
            "birthday" : dictionary.DATE, 
            "zipCode" : dictionary.STRING, 
            "address" : dictionary.STRING, 
            "telNum" : dictionary.STRING, 
            "assignDate" : dictionary.DATE, 
            "dismissDate" : dictionary.DATE, 
            "citizenNo" : dictionary.STRING, 
            "pubFmngYn" : dictionary.BOOLEAN, 
            "dismissRemark" : dictionary.STRING, 
            "title" : dictionary.STRING, 
            "mgeAgencyYn" : dictionary.BOOLEAN
        },
        "response" : {
            "result": {
                "isSuccess": dictionary.BOOLEAN,
                "message": dictionary.STRING,
                "ID":dictionary.STRING //fmgrSeq
            }
        }
    };

    updateArsonManager = {
        "url" : "/object/updatefiremanager",
        "method" : dictionary.POST,
        "body" : {
            "objId" : dictionary.NUMERIC, 
            "fmgrSeq" : dictionary.NUMERIC, 
            "firstName" : dictionary.STRING, 
            "lastName" : dictionary.STRING, 
            "birthday" : dictionary.DATE, 
            "zipCode" : dictionary.STRING, 
            "address" : dictionary.STRING, 
            "telNum" : dictionary.STRING, 
            "assignDate" : dictionary.DATE, 
            "dismissDate" : dictionary.DATE, 
            "citizenNo" : dictionary.STRING, 
            "pubFmngYn" : dictionary.BOOLEAN, 
            "dismissRemark" : dictionary.STRING, 
            "title" : dictionary.STRING, 
            "mgeAgencyYn" : dictionary.BOOLEAN,
            "useYn" : dictionary.BOOLEAN
        },
        "response" : {
            "result": {
                "isSuccess": dictionary.BOOLEAN,
                "message": dictionary.STRING,
                "ID":dictionary.NUMERIC
            }
        }
    };

    deleteArsonManager = {
        "url" : "/object/deletefiremanager",
        "method" : dictionary.POST,
        "body" : [], //[{objId:"",fmgrSeq:""}]
        "response" : {
            "result": {
                "isSuccess": dictionary.BOOLEAN,
                "message": dictionary.STRING
            }
        }
    };

    getSafeManagerList = {
        "url" : "/object/getsafemanagerlist/{objId}",
        "method" : dictionary.GET,
        "body" : {},
        "response" : {
            "objId" : dictionary.NUMERIC,
            "dgrmgrSeq" : dictionary.NUMERIC,
            "name" : dictionary.STRING,
            "telNum" : dictionary.STRING,
            "address" : dictionary.STRING,
        }
    };

    getSafeManagerInfo = {
        "url" : "/object/getsafemanagerinfo/{objId}/{dgrmgrSeq}",
        "method" : dictionary.GET,
        "body" : {},
        "response" : {
            "objId" : dictionary.NUMERIC, 
            "dgrmgrSeq" : dictionary.NUMERIC, 
            "firstName" : dictionary.STRING, 
            "lastName" : dictionary.STRING, 
            "zipCode" : dictionary.STRING, 
            "address" : dictionary.STRING, 
            "telNum" : dictionary.STRING, 
            "useYn" : dictionary.BOOLEAN
        }
    };

    insertSafeManager = {
        "url" : "/object/insertsafemanager",
        "method" : dictionary.POST,
        "body" : {
            "objId" : dictionary.NUMERIC, 
            "dgrmgrSeq" : dictionary.NUMERIC, 
            "firstName" : dictionary.STRING, 
            "lastName" : dictionary.STRING, 
            "zipCode" : dictionary.STRING, 
            "address" : dictionary.STRING, 
            "telNum" : dictionary.STRING
        },
        "response" : {
            "result": {
                "isSuccess": dictionary.BOOLEAN,
                "message": dictionary.STRING,
                "ID":dictionary.STRING //fmgrSeq
            }
        }
    };

    updateSafeManager = {
        "url" : "/object/updatesafemanager",
        "method" : dictionary.POST,
        "body" : {
            "objId" : dictionary.NUMERIC, 
            "dgrmgrSeq" : dictionary.NUMERIC, 
            "firstName" : dictionary.STRING, 
            "lastName" : dictionary.STRING, 
            "zipCode" : dictionary.STRING, 
            "address" : dictionary.STRING, 
            "telNum" : dictionary.STRING, 
            "useYn" : dictionary.BOOLEAN
        },
        "response" : {
            "result": {
                "isSuccess": dictionary.BOOLEAN,
                "message": dictionary.STRING,
                "ID":dictionary.NUMERIC
            }
        }
    };

    deleteSafeManager = {
        "url" : "/object/deletesafemanager",
        "method" : dictionary.POST,
        "body" : [], //[{objId:"",dgrmgrSeq:""}]
        "response" : {
            "result": {
                "isSuccess": dictionary.BOOLEAN,
                "message": dictionary.STRING
            }
        }
    };

    getBuildingManagerList = {
        "url" : "/object/getmanagerlist/{objId}/{locale}",
        "method" : dictionary.GET,
        "body" : {},
        "response" : {
            "objId" : dictionary.NUMERIC,
            "relSeq" : dictionary.NUMERIC,
            "name" : dictionary.STRING,
            "birthday" : dictionary.DATE, 
            "relCdName" : dictionary.STRING, 
            "address" : dictionary.STRING,
            "telNum" : dictionary.STRING
        }
    };

    getBuildingManagerInfo = {
        "url" : "/object/getmanagerinfo/{objId}/{relSeq}",
        "method" : dictionary.GET,
        "body" : {},
        "response" : {
            "objId" : dictionary.NUMERIC, 
            "relSeq" : dictionary.NUMERIC, 
            "firstName" : dictionary.STRING, 
            "lastName" : dictionary.STRING, 
            "birthday" : dictionary.DATE, 
            "zipCode" : dictionary.STRING, 
            "address" : dictionary.STRING, 
            "telNum" : dictionary.STRING, 
            "relCd" : dictionary.NUMERIC, 
            "jobDesc" : dictionary.STRING,
            "citizenNo" : dictionary.STRING,
            "useYn" : dictionary.BOOLEAN
        }
    };

    insertBuildingManager = {
        "url" : "/object/insertmanager",
        "method" : dictionary.POST,
        "body" : {
            "objId" : dictionary.NUMERIC, 
            "relSeq" : dictionary.NUMERIC, 
            "firstName" : dictionary.STRING, 
            "lastName" : dictionary.STRING, 
            "birthday" : dictionary.DATE, 
            "zipCode" : dictionary.STRING, 
            "address" : dictionary.STRING, 
            "telNum" : dictionary.STRING, 
            "relCd" : dictionary.NUMERIC, 
            "jobDesc" : dictionary.STRING,
            "citizenNo" : dictionary.STRING
        },
        "response" : {
            "result": {
                "isSuccess": dictionary.BOOLEAN,
                "message": dictionary.STRING,
                "ID":dictionary.STRING //fmgrSeq
            }
        }
    };

    updateBuildingManager = {
        "url" : "/object/updatemanager",
        "method" : dictionary.POST,
        "body" : {
            "objId" : dictionary.NUMERIC, 
            "relSeq" : dictionary.NUMERIC, 
            "firstName" : dictionary.STRING, 
            "lastName" : dictionary.STRING, 
            "birthday" : dictionary.DATE, 
            "zipCode" : dictionary.STRING, 
            "address" : dictionary.STRING, 
            "telNum" : dictionary.STRING, 
            "relCd" : dictionary.NUMERIC, 
            "jobDesc" : dictionary.STRING,
            "citizenNo" : dictionary.STRING,
            "useYn" : dictionary.BOOLEAN
        },
        "response" : {
            "result": {
                "isSuccess": dictionary.BOOLEAN,
                "message": dictionary.STRING,
                "ID":dictionary.NUMERIC
            }
        }
    };

    deleteBuildingManager = {
        "url" : "/object/deletemanager",
        "method" : dictionary.POST,
        "body" : [], //[{objId:"",relSeq:""}]
        "response" : {
            "result": {
                "isSuccess": dictionary.BOOLEAN,
                "message": dictionary.STRING
            }
        }
    };    
}

export default ServiceList;