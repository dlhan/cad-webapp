import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import HomeIcon from '@material-ui/icons/Home';
import PermDeviceInformationIcon from '@material-ui/icons/PermDeviceInformation';
import AssessmentIcon from '@material-ui/icons/Assessment';
import PermDataSettingIcon from '@material-ui/icons/PermDataSetting';
import FireplaceIcon from '@material-ui/icons/Fireplace';
import { GetText } from '../common/BundleManager'
import { CheckMenuAuth } from '../common/Utility';
import ArrowRightIcon from '@material-ui/icons/ArrowRight';
import ViewInfo from '../common/ViewInfo';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
  nested: {
    paddingLeft: theme.spacing(4),
  },
  icon: {
    minWidth: "37px"
  }
}));

export default function LeftMenu({onSelectMenu}) {
  var viewInfo = new ViewInfo();
  const classes = useStyles();
  const ISAuth = ["IS009","IS001","IS007","IS006","IS008"];
  const FSAuth = ["IS005","IS003","IS004","FI001","HR001"];
  const FIAuth = ["FI001","FI002","FI003","FI004","FI005","FI006","FI007","FI008","FI009"];
  const SMAuth = ["SM001","SM002","SM003","SM004","SM005"];

  const [openIS, setOpenIS] = React.useState(false);
  const [openFS, setOpenFS] = React.useState(false);
  const [openSS, setOpenSS] = React.useState(false);
  const [openSM, setOpenSM] = React.useState(false);
  
  const handleClickIS = () => {
    setOpenIS(!openIS);
  };

  const handleClickFS = () => {
    setOpenFS(!openFS);
  };

  const handleClickSS = () => {
    setOpenSS(!openSS);
  };

  const handleClickSM = () => {
    setOpenSM(!openSM);
  };

  return (
    <List
      component="nav"
      aria-labelledby="nested-list-subheader"
      className={classes.root}
    >
      <ListItem button onClick={() => onSelectMenu(viewInfo.home)}>
        <ListItemIcon  className={classes.icon}>
          <HomeIcon />
        </ListItemIcon>
        <ListItemText primary={GetText("menu_home")} />
      </ListItem>

      { CheckMenuAuth(ISAuth) && 
        <ListItem button onClick={handleClickIS}>
          <ListItemIcon  className={classes.icon}>
            <PermDeviceInformationIcon />
          </ListItemIcon>
          <ListItemText primary={GetText("menu_information_support")} />
          {openIS ? <ExpandLess /> : <ExpandMore />}
        </ListItem>
      }
      { CheckMenuAuth(ISAuth) && 
        <Collapse in={openIS} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            { 
              CheckMenuAuth(["IS009"]) &&
              <ListItem button onClick={() => onSelectMenu(viewInfo.wardList)} className={classes.nested}>
                <ListItemIcon className={classes.icon}>
                  <ArrowRightIcon />
                </ListItemIcon>
                <ListItemText primary={GetText("menu_fire_dept_mgr")} />
              </ListItem>
            }
            { 
              CheckMenuAuth(["IS001"]) &&
              <ListItem button onClick={() => onSelectMenu(viewInfo.fireObjectList)} className={classes.nested}>
                <ListItemIcon className={classes.icon}>
                  <ArrowRightIcon />
                </ListItemIcon>
                <ListItemText primary={GetText("menu_building_info")} />
              </ListItem>
            }
            { 
              CheckMenuAuth(["IS007"]) &&
              <ListItem button className={classes.nested}>
                <ListItemIcon  className={classes.icon}>
                  <ArrowRightIcon />
                </ListItemIcon>
                <ListItemText primary={GetText("menu_org_mgr")} />
              </ListItem>
            }
            { 
              CheckMenuAuth(["IS006"]) &&
              <ListItem button className={classes.nested}>
                <ListItemIcon  className={classes.icon}>
                  <ArrowRightIcon />
                </ListItemIcon>
                <ListItemText primary={GetText("menu_host_mgr")} />
              </ListItem>
            }
            { 
              CheckMenuAuth(["IS008"]) &&
              <ListItem button className={classes.nested}>
                <ListItemIcon  className={classes.icon}>
                  <ArrowRightIcon />
                </ListItemIcon>
                <ListItemText primary={GetText("menu_fire_volun_mgr")} />
              </ListItem>
            }
          </List>
        </Collapse>
      }
      { CheckMenuAuth(FSAuth) && 
        <ListItem button onClick={handleClickFS}>
          <ListItemIcon  className={classes.icon}>
            <FireplaceIcon />
          </ListItemIcon>
          <ListItemText primary={GetText("menu_fire_service")} />
          {openFS ? <ExpandLess /> : <ExpandMore />}
        </ListItem>
      }
      { CheckMenuAuth(FSAuth) && 
        <Collapse in={openFS} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            { 
              CheckMenuAuth(["IS005"]) &&
              <ListItem button className={classes.nested}>
                <ListItemIcon  className={classes.icon}>
                  <ArrowRightIcon />
                </ListItemIcon>
                <ListItemText primary={GetText("menu_juris_mgr")} />
              </ListItem>
            }
            { 
              CheckMenuAuth(["IS003"]) &&
              <ListItem button className={classes.nested}>
                <ListItemIcon  className={classes.icon}>
                  <ArrowRightIcon />
                </ListItemIcon>
                <ListItemText primary={GetText("menu_vehicle_mgr")} />
              </ListItem>
            }
            { 
              CheckMenuAuth(["IS004"]) &&
              <ListItem button className={classes.nested}>
                <ListItemIcon  className={classes.icon}>
                  <ArrowRightIcon />
                </ListItemIcon>
                <ListItemText primary={GetText("menu_dispatch_team_org")} />
              </ListItem>
            }
            { 
              CheckMenuAuth(["FI001"]) &&
              <ListItem button className={classes.nested}>
                <ListItemIcon  className={classes.icon}>
                  <ArrowRightIcon />
                </ListItemIcon>
                <ListItemText primary={GetText("menu_work_log")} />
              </ListItem>
            }
            { 
              CheckMenuAuth(["HR001"]) &&
              <ListItem button onClick={() => onSelectMenu(viewInfo.userList)} className={classes.nested}>
                <ListItemIcon  className={classes.icon}>
                  <ArrowRightIcon />
                </ListItemIcon>
                <ListItemText primary={GetText("menu_hr_mgr")} />
              </ListItem>
            }
          </List>
        </Collapse>
      }
      { CheckMenuAuth(FIAuth) && 
        <ListItem button  onClick={handleClickSS}>
          <ListItemIcon  className={classes.icon}>
            <AssessmentIcon />
          </ListItemIcon>
          <ListItemText primary={GetText("menu_report")} />
          {openSS ? <ExpandLess /> : <ExpandMore />}
        </ListItem>
      }
      { CheckMenuAuth(FIAuth) && 
        <Collapse in={openSS} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            { 
              CheckMenuAuth(["FI002"]) &&
              <ListItem button className={classes.nested}>
                <ListItemIcon  className={classes.icon}>
                  <ArrowRightIcon />
                </ListItemIcon>
                <ListItemText primary={GetText("menu_accident_report")} />
              </ListItem>
            }
            { 
              CheckMenuAuth(["FI003"]) &&
              <ListItem button className={classes.nested}>
                <ListItemIcon  className={classes.icon}>
                  <ArrowRightIcon />
                </ListItemIcon>
                <ListItemText primary={GetText("menu_fire_report")} />
              </ListItem>
            }
            { 
              CheckMenuAuth(["FI004"]) &&
              <ListItem button className={classes.nested}>
                <ListItemIcon  className={classes.icon}>
                  <ArrowRightIcon />
                </ListItemIcon>
                <ListItemText primary={GetText("menu_fire_reason_report")} />
              </ListItem>
            }
            { 
              CheckMenuAuth(["FI005"]) &&
              <ListItem button className={classes.nested}>
                <ListItemIcon  className={classes.icon}>
                  <ArrowRightIcon />
                </ListItemIcon>
                <ListItemText primary={GetText("menu_rescue_per_type_report")} />
              </ListItem>
            }
            { 
              CheckMenuAuth(["FI006"]) &&
              <ListItem button className={classes.nested}>
                <ListItemIcon  className={classes.icon}>
                  <ArrowRightIcon />
                </ListItemIcon>
                <ListItemText primary={GetText("menu_etc_type_report")} />
              </ListItem>
            }
            { 
              CheckMenuAuth(["FI007"]) &&
              <ListItem button className={classes.nested}>
                <ListItemIcon  className={classes.icon}>
                  <ArrowRightIcon />
                </ListItemIcon>
                <ListItemText primary={GetText("menu_call_for_accident_per_type_report")} />
              </ListItem>
            }
            { 
              CheckMenuAuth(["FI008"]) &&
              <ListItem button className={classes.nested}>
                <ListItemIcon  className={classes.icon}>
                  <ArrowRightIcon />
                </ListItemIcon>
                <ListItemText primary={GetText("menu_weekday_call_report")} />
              </ListItem>
            }
            { 
              CheckMenuAuth(["FI009"]) &&
              <ListItem button className={classes.nested}>
                <ListItemIcon  className={classes.icon}>
                  <ArrowRightIcon />
                </ListItemIcon>
                <ListItemText primary={GetText("menu_call_result_per_type_report")} />
              </ListItem>
            }
          </List>
        </Collapse>
      }
      { CheckMenuAuth(SMAuth) && 
        <ListItem button onClick={handleClickSM}>
          <ListItemIcon  className={classes.icon}>
          <PermDataSettingIcon />
          </ListItemIcon>
          <ListItemText primary={GetText("menu_system_manager")} />
          {openSM ? <ExpandLess /> : <ExpandMore />}
        </ListItem>
      }
      { CheckMenuAuth(SMAuth) && 
        <Collapse in={openSM} timeout="auto" unmountOnExit>
          <List component="div" disablePadding>
            { 
              CheckMenuAuth(["SM003"]) &&
              <ListItem button className={classes.nested}>
                <ListItemIcon  className={classes.icon}>
                  <ArrowRightIcon />
                </ListItemIcon>
                <ListItemText primary={GetText("menu_user_auth")} />
              </ListItem>
            }
            { 
              CheckMenuAuth(["SM004"]) &&
              <ListItem button className={classes.nested}>
                <ListItemIcon  className={classes.icon}>
                  <ArrowRightIcon />
                </ListItemIcon>
                <ListItemText primary={GetText("menu_menu_mgr")} />
              </ListItem>
            }
            { 
              CheckMenuAuth(["SM005"]) &&
              <ListItem button className={classes.nested}>
                <ListItemIcon  className={classes.icon}>
                  <ArrowRightIcon />
                </ListItemIcon>
                <ListItemText primary={GetText("menu_code_mgr")} />
              </ListItem>
            }
            { 
              CheckMenuAuth(["SM002"]) &&
              <ListItem button className={classes.nested}>
                <ListItemIcon  className={classes.icon}>
                  <ArrowRightIcon />
                </ListItemIcon>
                <ListItemText primary={GetText("menu_code_mgr")} />
              </ListItem>
            }
          </List>
        </Collapse>
      }
    </List>
  );
}