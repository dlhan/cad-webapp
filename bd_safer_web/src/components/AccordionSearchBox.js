import React from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import MuiAccordion from '@material-ui/core/Accordion';
import MuiAccordionSummary from '@material-ui/core/AccordionSummary';
import MuiAccordionDetails from '@material-ui/core/AccordionDetails';
import Typography from '@material-ui/core/Typography';
import SearchIcon from '@material-ui/icons/Search';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { GetText } from '../common/BundleManager'

const Accordion = withStyles({
  root: {
    border: '1px solid rgba(0, 0, 0, .125)',
    boxShadow: 'none',
    '&:not(:last-child)': {
      borderBottom: 0,
    },
    '&:before': {
      display: 'none',
    },
    '&$expanded': {
      margin: 'auto',
    },
  },
  expanded: {},
})(MuiAccordion);

const AccordionSummary = withStyles({
  root: {
    backgroundColor: 'rgba(0, 0, 0, .03)',
    borderBottom: '1px solid rgba(0, 0, 0, .125)',
    marginBottom: -1,
    maxHeight: 30,
    '&$expanded': {
      minHeight: 30,
    },
    minHeight: 30
  },
  content: {
    '&$expanded': {
      margin: '12px 0',
    },
  },
  expanded: {},
})(MuiAccordionSummary);

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    
  },
  heading: {
    fontSize: theme.typography.pxToRem(13),
    flexBasis: '3%',
    flexShrink: 2,
    color:'#616161',
    marginTop:3
  },
  secondaryHeading: {
    fontSize: theme.typography.pxToRem(13),
    marginTop:5
  },
}));

const AccordionDetails = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
    display: 'block'
  },
}))(MuiAccordionDetails);

export default function AccordionSearchBox({searchPanel,expanded, onChange, id}) {
  const classes = useStyles();
  
  return (
    <div>
      <Accordion 
        expanded={expanded} 
        onChange={onChange}
        name={id}
        id={id}
      >
        <AccordionSummary 
          aria-controls="panel1d-content" 
          id="panel1d-header"
          expandIcon={<ExpandMoreIcon />}
        >
          <Typography className={classes.heading}><SearchIcon /></Typography>
          <Typography className={classes.secondaryHeading}> {GetText("filter_condition")}</Typography>
          
        </AccordionSummary>
        <AccordionDetails>
          {searchPanel}
        </AccordionDetails>
      </Accordion>
    </div>
  );
}
