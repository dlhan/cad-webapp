import React from 'react';
import clsx from 'clsx';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import InputLabel from '@material-ui/core/InputLabel';
import { useUserContext } from '../common/UserContext';

const useStyles = makeStyles({
  root: {
    '&:hover': {
      backgroundColor: 'transparent',
    },
  },
  icon: {
    borderRadius: '50%',
    width: 16,
    height: 16,
    boxShadow: 'inset 0 0 0 1px rgba(16,22,26,.2), inset 0 -1px 0 rgba(16,22,26,.1)',
    backgroundColor: '#f5f8fa',
    backgroundImage: 'linear-gradient(180deg,hsla(0,0%,100%,.8),hsla(0,0%,100%,0))',
    '$root.Mui-focusVisible &': {
      outline: '2px auto rgba(19,124,189,.6)',
      outlineOffset: 2,
    },
    'input:disabled ~ &': {
      boxShadow: 'none',
      background: 'rgba(206,217,224,.5)',
    },
  },
  checkedIcon: {
    backgroundColor: '#137cbd',
    backgroundImage: 'linear-gradient(180deg,hsla(0,0%,100%,.1),hsla(0,0%,100%,0))',
    '&:before': {
      display: 'block',
      width: 16,
      height: 16,
      backgroundImage: 'radial-gradient(#fff,#fff 28%,transparent 32%)',
      content: '""',
    }
  },
});

const RowRadioGoup = withStyles((theme) => ({
    root: {
      display: 'flex',
      flexDirection: 'row',
      flexWrap:'wrap',
      marginTop:12
    },
  }))(RadioGroup);

// Inspired by blueprintjs
function StyledRadio(props) {
  const classes = useStyles();
  const userContext = useUserContext();

  return (
    <Radio
      className={classes.root}
      disableRipple
      color={userContext.preference.color}
      checkedIcon={<span className={clsx(classes.icon, classes.checkedIcon)} />}
      icon={<span className={classes.icon} />}
      {...props}
    />
  );
}

export default function CustomizedRadioGroup({id, label, defaultValue, radios, value, onChange, disabled}) {
  return (
    <FormControl component="fieldset">
      <InputLabel shrink>{label}</InputLabel>
      <RowRadioGoup defaultValue={defaultValue} aria-label={id} name={id} onChange={onChange}>
        {radios.map((radio) => (
          <FormControlLabel value={radio.value} control={<StyledRadio checked={value === radio.value} disabled={disabled} />} label={radio.label} />
        ))}
      </RowRadioGoup>
    </FormControl>
  );
}
