import React from 'react';
import InputLabel from '@material-ui/core/InputLabel';
import { FormStyle } from '../style/Style';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputAdornment from '@material-ui/core/InputAdornment';
import IconButton from '@material-ui/core/IconButton';
import FormHelperText from '@material-ui/core/FormHelperText';
import DeleteIcon from '@material-ui/icons/Delete';

const useStyles = FormStyle;

export default function TextFieldWithIcon(props) {
  const { id, label, type, className ,onChange, disabled, value, required, readOnly, multiline, fullWidth, icon, iconEvent, helpText, error, allowDeleteIcon, handleDelete } = props;
  const classes = useStyles();
  let textFieldDisabled = readOnly ? false : disabled;
  let inputType = type ? type : 'text';
  
  return (
    <React.Fragment>
      <FormControl className={className ? className : classes.textField}>
        <InputLabel required={required ? true : false} shrink>{label}</InputLabel>
        <Input
          id={id}
          name={id}
          type={inputType}
          onChange={onChange}
          value={value}
          disabled={textFieldDisabled}
          required={required ? true : false}
          readOnly={readOnly}
          multiline={multiline}
          fullWidth={fullWidth}
          endAdornment={
            icon &&
            <InputAdornment position="end">
              <IconButton onClick={iconEvent} disabled={disabled} style={{padding:'0px'}}>
                {icon}
              </IconButton>
              {allowDeleteIcon && <IconButton onClick={handleDelete} style={{padding:'0px'}}>
                <DeleteIcon />  
              </IconButton>}
            </InputAdornment>
              }
            />
        <FormHelperText 
          error={error}
        >
          {helpText}
        </FormHelperText>
      </FormControl>
    </React.Fragment>
  );
}
