import React from 'react';
import { FormStyle } from '../style/Style';
import InputLabel from '@material-ui/core/InputLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import NativeSelect from '@material-ui/core/NativeSelect';

const useStyles = FormStyle;

export default function CustomizedSelect(props) {
    const {id, label, className, items, onChange, selectedItemId, disabled, visibleFirstOption, firstOptionItem, helpText, required} = props;
    const classes = useStyles();
    
    return (
      <FormControl className={ className ? className : classes.select }>
          <InputLabel htmlFor={id} shrink >{label} {required ? "*" : ""}</InputLabel>
          <NativeSelect
            id={id}
            value={selectedItemId}
            onChange={onChange}
            required={required}
            inputProps={{
                name:id,
                disabled:disabled
              }}
            InputLabelProps={{
              shrink: true,
            }}
          >
              {visibleFirstOption === true && firstOptionItem && <option value={firstOptionItem.value}>{firstOptionItem.label}</option>}
              {items.map((item) => {
                    return (
                      <option value={item.value}>{item.label}</option>
                    );
                  })
              
              }
          </NativeSelect>
          <FormHelperText>{helpText}</FormHelperText>
        </FormControl>
    );
  }
  