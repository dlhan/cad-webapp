import React from 'react';
import { withStyles, makeStyles } from '@material-ui/core/styles';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import FormLabel from '@material-ui/core/FormLabel';
import SelectView from '../views/SelectView';

const styles = (theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(2),
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  }
});

const useStyles = makeStyles((theme) => ({
  xl: {
    "& .MuiDialog-paperWidthXl": {
      width:'1920px',
      //height:'1000px'
    },
    "& .MuiDialog-paper": {
      margin: '32px',
      position: 'relative',
      overflow:'hidden'
      //height:'1000px'
    }
  },
  lg: {
    "& .MuiDialog-paperWidthLg": {
      width:'1280px',
      //height:'740px'
    },
    "& .MuiDialog-paper": {
      margin: '32px',
      position: 'relative',
      overflow:'hidden'
      //height:'1000px'
    }
  },
  md: {
    "& .MuiDialog-paperWidthMd": {
      width:'960px',
      //height:'600px'
    }
  },
  sl: {
    "& .MuiDialog-paperWidthSl": {
      width:'700px',
      //height:'520px'
    }
  },
  sm: {
    "& .MuiDialog-paperWidthSm": {
      width:'600px',
      //height:'520px'
    }
  },
  xs: {
    "& .MuiDialog-paperWidthXs": {
      width:'440px',
      //height:'500px'
    }
  },
  subText : {
    color: 'rgba(0, 0, 0, 0.54)',
    fontSize : '0.75rem'
  }
}));

const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose,subText, ...other } = props;
  return (
    <MuiDialogTitle disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      <FormLabel className={classes.subText}>{subText}</FormLabel>
      {onClose ? (
        <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme) => ({
  root: {
    padding: theme.spacing(2),
    overflow: 'auto'
  },
}))(MuiDialogContent);

export default function CustomizedDialog(props) {
  const classes = useStyles();
  let maxWidth = "md";
  let id = props.id ? props.id : "dialogId" ;

  if (props.viewInfo !== "") {
    maxWidth = props.viewInfo.dialogWidthType.maxWidth;
  }

  let className;
  switch (maxWidth) {
    case "xl" :
      className = classes.xl;
      break;
    case "lg" :
      className = classes.lg;
      break;
    case "md" :
      className = classes.md;
      break;
    case "sl" :
      className = classes.sl;
      break;
    case "sm" :
      className = classes.sm;
      break;
    case "xs" :
      className = classes.xs;
      break;
    default :
    className = classes.xl;
      break;
  }

  return (
    <React.Fragment>
      <Dialog name={id} className={className} maxWidth={maxWidth} open={props.isDialogOpen}>
        <DialogTitle onClose={props.handleDialogClose} classes={classes} subText={props.viewInfo.subTextOnDialogTitle}>
        {
          props.viewInfo.viewTitle.normal
        }
        </DialogTitle>
        <DialogContent dividers>
        {
          SelectView(props.viewInfo, props.callFromDialog, props.sendInfo, props.handleDialogClose) 
        }
        </DialogContent>
      </Dialog>
    </React.Fragment>
  );
}
