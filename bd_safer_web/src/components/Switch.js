import React from 'react';
import Switch from '@material-ui/core/Switch';
import FormControl from '@material-ui/core/FormControl';
import { InputLabel } from '@material-ui/core';
import Box from '@material-ui/core/Box';
import { withStyles } from '@material-ui/core/styles';
import { useUserContext } from '../common/UserContext';
import Grid from '@material-ui/core/Grid';
import { FormStyle } from '../style/Style';

const useStyles = FormStyle;

const AntSwitch = withStyles((theme) => ({
  root: {
    width: 28,
    height: 16,
    padding: 0,
    display: 'flex',
  },
  switchBase: {
    padding: 2,
    color: theme.palette.grey[500],
    '&$checked': {
      transform: 'translateX(12px)',
      color: theme.palette.common.white,
      '& + $track': {
        opacity: 1,
      },
    },
  },
  thumb: {
    width: 12,
    height: 12,
    boxShadow: 'none',
  },
  track: {
    border: `1px solid ${theme.palette.grey[500]}`,
    borderRadius: 16 / 2,
    opacity: 1,
    backgroundColor: theme.palette.common.white,
  },
  checked: {},
}))(Switch);

export default function CustomizedSwitch({id, label, className, onChange, disabled,value, checked}) {
  const userContext = useUserContext();
  const classes = useStyles();
  
  return (
    <React.Fragment>
      <FormControl className={className ? className : classes.textField}>
        <InputLabel shrink>
          {label}
        </InputLabel>
        <Box p={1.5}></Box>
        <Grid component="label" container alignItems="center" spacing={1}>
          <Grid item>Off</Grid>
          <Grid item>
            <AntSwitch 
              name={id}
              id={id}
              checked={checked}
              onChange={onChange}
              disabled={disabled}
              color={userContext.preference.color}
              value={value}
            />
          </Grid>
          <Grid item>On</Grid>
        </Grid>
      </FormControl>     
    </React.Fragment> 
  );
}
