import React, { useRef } from 'react';
import { FormStyle } from '../style/Style';
import TextField from '@material-ui/core/TextField';
import InputAdornment from '@material-ui/core/InputAdornment';
import { GetNumbers } from '../common/Utility';

const useStyles = FormStyle;

export default function CustomizedTextField(props) {
  const { id, label, className ,onChange, type,length,belowDotLength,disabled, value, required, multiline, fullWidth, rows, variant, defaultValue,max,min,helperText,error,startAdornment,endAdornment } = props;
  const classes = useStyles();
  const textField = useRef();

  let textType = type ? type === "float" || type === "number" ? "text" : type : "text";

  const onChangeHandler =(e)=>{
    var {name, value} = e.target;

    if (type === "number") {
      value = GetNumbers(value, false);
    } else if (type === "float") {
      value = GetNumbers(value, true, belowDotLength);
    }

    if (length && !isNaN(length)) {
      if (value.length > Number(length)) {
        value = value.substring(0, Number(length));
      }
    }

    textField.current.value = value;
    onChange(e);
  }

  return (
    <React.Fragment>
      <TextField
        id={id}
        name={id}
        type={textType}
        label={label ? label : " "}
        className={className ? className : classes.textField}
        multiline={multiline}
        required={required}
        rows={rows}
        variant={variant}
        onChange={onChangeHandler}
        defaultValue={defaultValue}
        disabled={disabled}
        value={value}
        fullWidth={fullWidth}
        max={max}
        min={min}
        inputRef={textField}
        InputLabelProps={{
          shrink: true,
        }}
        helperText={helperText}
        error={error}
        InputProps={{
          startAdornment: <InputAdornment position="start">{startAdornment}</InputAdornment>,
          endAdornment: <InputAdornment position="end">{endAdornment}</InputAdornment>,
        }}
      />
    </React.Fragment>
  );
}
