-- MySQL dump 10.13  Distrib 5.5.62, for Win64 (AMD64)
--
-- Host: 192.168.0.203    Database: bd_safer
-- ------------------------------------------------------
-- Server version	5.5.5-10.3.29-MariaDB-0ubuntu0.20.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `cm_districts`
--

DROP TABLE IF EXISTS `cm_districts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cm_districts` (
  `district_id` int(11) NOT NULL,
  `district_name` varchar(30) NOT NULL,
  `division_code` char(1) DEFAULT NULL,
  PRIMARY KEY (`district_id`),
  KEY `cm_districts_FK` (`division_code`),
  CONSTRAINT `cm_districts_FK` FOREIGN KEY (`division_code`) REFERENCES `cm_divisions` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cm_districts`
--

LOCK TABLES `cm_districts` WRITE;
/*!40000 ALTER TABLE `cm_districts` DISABLE KEYS */;
INSERT INTO `cm_districts` VALUES (1,'Bandarban','B'),(2,'Barguna','A'),(3,'Bogura','E'),(4,'Brahmanbaria','B'),(5,'Bagerhat','D'),(6,'Barishal','A'),(7,'Bhola','A'),(8,'Cumilla','B'),(9,'Chandpur','B'),(10,'Chattogram','B'),(11,'Cox\'s Bazar','B'),(12,'Chuadanga','D'),(13,'Dhaka','C'),(14,'Dinajpur','F'),(15,'Faridpur','C'),(16,'Feni','B'),(17,'Gopalganj','C'),(18,'Gazipur','C'),(19,'Gaibandha','F'),(20,'Habiganj','G'),(21,'Jamalpur','H'),(22,'Jashore','D'),(23,'Jhenaidah','D'),(24,'Joypurhat','E'),(25,'Jhalakathi','A'),(26,'Kishoreganj','C'),(27,'Khulna','D'),(28,'Kurigram','F'),(29,'Khagrachhari','B'),(30,'Kushtia','D'),(31,'Lakshmipur','B'),(32,'Lalmonirhat','F'),(33,'Manikganj','C'),(34,'Mymensingh','H'),(35,'Munshiganj','C'),(36,'Madaripur','C'),(37,'Magura','D'),(38,'Moulvibazar','G'),(39,'Meherpur','D'),(40,'Narayanganj','C'),(41,'Netrakona','H'),(42,'Narsingdi','C'),(43,'Narail','D'),(44,'Natore','E'),(45,'Chapai Nawabganj','E'),(46,'Nilphamari','F'),(47,'Noakhali','B'),(48,'Naogaon','E'),(49,'Pabna','E'),(50,'Pirojpur','A'),(51,'	Patuakhali','A'),(52,'	Panchagarh','F'),(53,'Rajbari','C'),(54,'Rajshahi','E'),(55,'Rangpur','F'),(56,'Rangamati','B'),(57,'Sherpur','H'),(58,'Satkhira','D'),(59,'Sirajganj','E'),(60,'Sylhet','G'),(61,'Sunamganj','G'),(62,'Shariatpur','C'),(63,'Tangail','C'),(64,'Thakurgaon','F');
/*!40000 ALTER TABLE `cm_districts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cm_divisions`
--

DROP TABLE IF EXISTS `cm_divisions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cm_divisions` (
  `code` char(1) NOT NULL,
  `division_name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cm_divisions`
--

LOCK TABLES `cm_divisions` WRITE;
/*!40000 ALTER TABLE `cm_divisions` DISABLE KEYS */;
INSERT INTO `cm_divisions` VALUES ('A','Barishal'),('B','Chattogram'),('C','Dhaka'),('D','Khulna'),('E','Rajshahi'),('F','Rangpur'),('G','Sylhet'),('H','Mymensingh');
/*!40000 ALTER TABLE `cm_divisions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cm_index`
--

DROP TABLE IF EXISTS `cm_index`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cm_index` (
  `id` int(11) NOT NULL,
  `organ_name` varchar(100) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cm_index`
--

LOCK TABLES `cm_index` WRITE;
/*!40000 ALTER TABLE `cm_index` DISABLE KEYS */;
INSERT INTO `cm_index` VALUES (100,'FSCD'),(201,'Dhaka HQ'),(202,'Barishal HQ'),(203,'Chattogram HQ'),(204,'Khulna HQ'),(205,'Rajshahi HQ'),(206,'Rangpur HQ'),(207,'Sylhet HQ'),(208,'Mymensingh HQ');
/*!40000 ALTER TABLE `cm_index` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `code`
--

DROP TABLE IF EXISTS `code`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `code` (
  `cd` int(11) DEFAULT NULL,
  `kr` varchar(100) DEFAULT NULL,
  `en` varchar(100) DEFAULT NULL,
  `code` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `code`
--

LOCK TABLES `code` WRITE;
/*!40000 ALTER TABLE `code` DISABLE KEYS */;
INSERT INTO `code` VALUES (724,'학력.경력인정소방기술자격','Firefighting technology qualification recognized for education and career',1),(978,'유선설비기사','Wired Equipment Engineer',4),(979,'유선설비산업기사','Wired Equipment Industry Engineer',5),(980,'일반기계산업기사','General Machinery Industry Engineer',6),(981,'위험물취급기능사','Dangerous Goods Handling Technician',7),(982,'기중기운전기능사','Crane driver',8),(983,'지게차운전기능사','Forklift driver',9),(984,'타워크레인운전기능사','Tower crane driver',10),(985,'가스용접기능사','Gas welding technician',11),(986,'용접기능장','Welding craftsman',12),(987,'용접기술사','Welding engineer',13),(988,'용접산업기사','Welding Industry Engineer',14),(989,'전기용접기능사','Electric welding technician',15),(990,'특수용접기능사','Special welding technician',16),(991,'굴삭기운전기능사','Excavator driver',17),(992,'로더운전기능사','Loader driver',18),(993,'모터그레이더운전기능사','Motor grader driver',19),(994,'불도저운전기능사','Bulldozer driver',20),(995,'가스기능사','Gas technician',21),(996,'가스기능장','Gas station',22),(997,'가스기사','Gas engineer',23),(998,'가스기술사','Gas engineer',24),(999,'가스산업기사','Gas Industry Engineer',25),(1000,'무선설비산업기사','Radio Equipment Industry Engineer',26),(1001,'소방설비기사(전기)','Firefighting Equipment Engineer (Electric)',27),(1002,'소방설비산업기사(전기)','Firefighting Equipment Industry Engineer (Electric)',28),(1003,'화공','Chemical engineer',29),(1004,'토목','Civil engineering',30),(1005,'잠수','Diving',31),(1006,'고압가스','High pressure gas',32),(1007,'기타관련자격','Other related qualifications',33),(1008,'건축기계설비기술사','Construction machine equipment engineer',34),(1009,'건축설비기사','Construction equipment engineer',35),(1010,'건축설비산업기사','Construction equipment industry engineer',35),(1011,'건축기사','Architectural engineer',37),(1012,'건축산업기사','Construction Industry Engineer',38),(1013,'일반기계기사','General mechanical engineer',39),(1014,'생산기계산업기사','Production machinery industry engineer',40),(1015,'공조냉동기계기술사','Air conditioning refrigeration machine engineer',41),(1016,'공조냉동기계기사','Air Conditioning Refrigeration Machine Engineer',42),(1017,'공조냉동기계산업기사','Air Conditioning Refrigeration Machine Industry Engineer',43),(1018,'보일러취급기능사','Boiler handling technician',44),(1019,'건설기계기사','Construction machine engineer',45),(1020,'건설기계산업기사','Construction machinery industry engineer',46),(1021,'중장비기사','Heavy Equipment Engineer',47),(1022,'금형기술사','Mold engineer',48),(1023,'열관리기사','Thermal Management Engineer',49),(1024,'산업안전기사','Industrial safety engineer',50),(1025,'소방설비기술사','Firefighting equipment engineer',51),(1026,'소방설비기사(기계)','Firefighting equipment engineer (machine)',52),(1027,'소방설비산업기사(기계)','Firefighting equipment industry engineer (machine)',53),(1028,'가스기사','Gas engineer',54),(1029,'가스산업기사','Gas Industry Engineer',55),(1030,'위험물관리산업기사','Dangerous Goods Management Industry Engineer',56),(1031,'위험물관리기능사','Dangerous Goods Management Technician',57),(1032,'전기기사','Electrician',58),(1033,'전기산업기사','Electrical Industry Engineer',59),(1034,'건축전기설비기술사','Architectural Electrical Equipment Engineer',60),(1035,'전기공사기사','Electrical construction engineer',61),(1036,'전기공사산업기사','Electrical construction industry engineer',62),(1037,'전기공사기능사','Electric construction technician',63),(1038,'전자산업기사','Electronics Industry Engineer',64),(1039,'건축사','Architect',65),(1040,'사무자동화산업기사','Office Automation Industry Engineer',66),(1041,'전파통신기사','Radio communication engineer',67),(1042,'전파통신산업기사','Radio Communication Industry Engineer',68),(1043,'소방시설관리사','Firefighting Facility Manager',69),(834,NULL,NULL,2),(977,NULL,NULL,3),(7,NULL,NULL,70);
/*!40000 ALTER TABLE `code` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ct_dsprule`
--

DROP TABLE IF EXISTS `ct_dsprule`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ct_dsprule` (
  `dsr_knd_cd` int(11) NOT NULL COMMENT '긴급구조종별코드',
  `dsr_cls_cd` int(11) NOT NULL COMMENT '긴급구조분류코드',
  `dsr_size_cd` int(11) NOT NULL COMMENT '긴급구조규모코드',
  `car_cd_grp` int(11) NOT NULL COMMENT '차종코드그룹',
  `car_cnt` int(4) DEFAULT NULL COMMENT '차량수',
  `lchg_dtime` varchar(20) DEFAULT NULL COMMENT '수정일시',
  `lchg_user_id` varchar(20) DEFAULT NULL COMMENT '수정자ID',
  PRIMARY KEY (`dsr_knd_cd`,`dsr_cls_cd`,`dsr_size_cd`,`car_cd_grp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='유형별출동대편성지침';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ct_dsprule`
--

LOCK TABLES `ct_dsprule` WRITE;
/*!40000 ALTER TABLE `ct_dsprule` DISABLE KEYS */;
/*!40000 ALTER TABLE `ct_dsprule` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr_edu_promotion`
--

DROP TABLE IF EXISTS `hr_edu_promotion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr_edu_promotion` (
  `education_promotion_seq` int(11) NOT NULL AUTO_INCREMENT COMMENT '일련번호',
  `user_id` varchar(20) NOT NULL COMMENT '사용자 id',
  `course_name` varchar(100) DEFAULT NULL COMMENT '과정명',
  `institution_name` varchar(100) DEFAULT NULL COMMENT '기관명',
  `start_date` date DEFAULT NULL COMMENT '시작일',
  `end_date` date DEFAULT NULL COMMENT '종료일',
  `education_cls_cd` int(11) DEFAULT NULL COMMENT '교육구분코드',
  `complete_cd` int(11) DEFAULT NULL COMMENT '교육이수코드',
  `score` varchar(10) DEFAULT NULL COMMENT '성적',
  PRIMARY KEY (`education_promotion_seq`,`user_id`),
  KEY `FK_hr_edu_promotion_user_id` (`user_id`),
  CONSTRAINT `FK_hr_edu_promotion_user_id` FOREIGN KEY (`user_id`) REFERENCES `hr_user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr_edu_promotion`
--

LOCK TABLES `hr_edu_promotion` WRITE;
/*!40000 ALTER TABLE `hr_edu_promotion` DISABLE KEYS */;
INSERT INTO `hr_edu_promotion` VALUES (2,'test3','Course1','Institute','2021-05-03','2021-05-31',924,58,'BB');
/*!40000 ALTER TABLE `hr_edu_promotion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr_personal_eval`
--

DROP TABLE IF EXISTS `hr_personal_eval`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr_personal_eval` (
  `eval_seq` int(11) NOT NULL AUTO_INCREMENT COMMENT '일련번호',
  `eval_year` int(4) NOT NULL COMMENT '평가년도',
  `user_id` varchar(20) NOT NULL COMMENT '사용자 id',
  `evaluator_user_id` varchar(20) NOT NULL COMMENT '평가자 id',
  `eval_date` varchar(20) DEFAULT NULL COMMENT '평가일시',
  `perf_ability_score` varchar(10) DEFAULT NULL COMMENT '직무수행능력 점수',
  `resp_score` varchar(10) DEFAULT NULL COMMENT '책임감 점수',
  `diligence_score` varchar(10) DEFAULT NULL COMMENT '성실성 점수',
  PRIMARY KEY (`eval_seq`,`eval_year`,`user_id`),
  KEY `FK_hr_personal_eval_user_id` (`user_id`),
  KEY `FK_hr_personal_eval_evaluator_user_id` (`evaluator_user_id`),
  CONSTRAINT `FK_hr_personal_eval_evaluator_user_id` FOREIGN KEY (`evaluator_user_id`) REFERENCES `hr_user` (`user_id`),
  CONSTRAINT `FK_hr_personal_eval_user_id` FOREIGN KEY (`user_id`) REFERENCES `hr_user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr_personal_eval`
--

LOCK TABLES `hr_personal_eval` WRITE;
/*!40000 ALTER TABLE `hr_personal_eval` DISABLE KEYS */;
/*!40000 ALTER TABLE `hr_personal_eval` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr_user`
--

DROP TABLE IF EXISTS `hr_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr_user` (
  `user_id` varchar(20) NOT NULL COMMENT '사용자ID',
  `first_name` varchar(50) DEFAULT NULL COMMENT 'First_name',
  `last_name` varchar(50) DEFAULT NULL COMMENT 'Last name',
  `citizen_no` varchar(20) DEFAULT NULL COMMENT '주민번호',
  `ward_id` varchar(9) DEFAULT NULL COMMENT '서·센터ID',
  `class_cd` int(11) DEFAULT NULL COMMENT '계급코드',
  `title_cd` int(11) DEFAULT NULL COMMENT '직위코드',
  `work_cd` int(11) DEFAULT NULL COMMENT '직류코드',
  `work_section_cd` int(11) DEFAULT NULL COMMENT '근무구분코드',
  `user_pw` varchar(40) DEFAULT NULL COMMENT '비밀번호',
  `pw_chg_date` date DEFAULT NULL COMMENT '비밀번호변경일자',
  `reg_date` varchar(20) DEFAULT NULL COMMENT '등록일자',
  `use_yn` char(1) DEFAULT '1' COMMENT '사용여부',
  `sysmgr_yn` char(1) DEFAULT NULL COMMENT '시스템관리자여부',
  `zip_code` varchar(10) DEFAULT NULL COMMENT '우편번호',
  `address` varchar(100) DEFAULT NULL COMMENT '주소',
  `home_tel` varchar(20) DEFAULT NULL COMMENT '집전화번호',
  `office_tel` varchar(20) DEFAULT NULL COMMENT '회사전화번호',
  `cell` varchar(20) DEFAULT NULL COMMENT '이동전화번호',
  `fax_no` varchar(20) DEFAULT NULL COMMENT '팩스번호',
  `ext_tel` varchar(10) DEFAULT NULL COMMENT '내선번호',
  `email` varchar(50) DEFAULT NULL COMMENT '이메일',
  `birth_date` date DEFAULT NULL COMMENT '생년월일',
  `gender` char(1) DEFAULT NULL COMMENT '성별',
  `lchg_dtime` varchar(20) DEFAULT NULL COMMENT '수정일시',
  `lchg_user_id` varchar(20) DEFAULT NULL COMMENT '수정자ID',
  `user_seq` int(11) NOT NULL COMMENT '사용자 일렬번호',
  PRIMARY KEY (`user_id`),
  KEY `FK_hr_user_ward_id` (`ward_id`),
  CONSTRAINT `FK_hr_user_ward_id` FOREIGN KEY (`ward_id`) REFERENCES `is_ward` (`ward_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr_user`
--

LOCK TABLES `hr_user` WRITE;
/*!40000 ALTER TABLE `hr_user` DISABLE KEYS */;
INSERT INTO `hr_user` VALUES ('test','FirstName','LastName','13131313','131001001',713,717,21,19,'1111',NULL,NULL,'1','1',NULL,NULL,'032-111-2222',NULL,'010-1112-2222',NULL,NULL,NULL,'2021-04-01','F','05/27/2021 09:44:58','{loginId}',130000001),('test10','FirstName','LastName',NULL,NULL,29,32,21,19,'1111',NULL,NULL,'0','0',NULL,NULL,'032-111-2222',NULL,'010-1112-2222',NULL,NULL,NULL,'2021-04-01','F','05/14/2021 17:27:57','test',130000008),('test11','FirstName','LastName','13131313','131004000',714,30,890,19,NULL,NULL,NULL,NULL,'0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2021-05-10','F','05/27/2021 09:39:38','{loginId}',130000009),('test119','FirstName','LastName',NULL,NULL,29,32,21,19,'1111',NULL,NULL,'1',NULL,NULL,NULL,'032-111-2222',NULL,'010-1112-2222',NULL,NULL,NULL,'2021-04-01','F',NULL,NULL,130000010),('test120','FirstName','LastName',NULL,NULL,29,32,21,19,'1111',NULL,NULL,'1',NULL,NULL,NULL,'032-111-2222',NULL,'010-1112-2222',NULL,NULL,NULL,'2021-04-01','F','05/14/2021 17:29:23',NULL,130000011),('test123','FirstName','LastName','13131313','131004000',713,28,21,908,'1111',NULL,'05/26/2021 18:04:05','1','1','1233','Etc address','032-111-2222','123-312','010-1112-2222','124-3121','1234','dlhan@incon.kr','2021-05-03','F','05/26/2021 18:04:05','{loginId}',130000013),('test2','FirstName','LastName',NULL,NULL,29,32,21,19,'1111',NULL,NULL,'1',NULL,NULL,NULL,'032-111-2222',NULL,'010-1112-2222',NULL,NULL,NULL,'2021-04-01','F',NULL,NULL,130000002),('test21','FirstName','LastName','13131313','131002000',712,29,23,910,NULL,NULL,NULL,NULL,'0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2021-05-03','F','05/27/2021 09:43:06','{loginId}',130000012),('test3','FirstName','LastName',NULL,'131000000',NULL,NULL,NULL,NULL,'1111',NULL,NULL,'1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2021-04-06','M',NULL,NULL,130000003),('test5','FirstName','LastName','13131313','132003000',709,724,891,908,NULL,NULL,NULL,NULL,'0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2021-05-03','M','05/27/2021 09:45:32','{loginId}',130000004),('test6','FirstName','LastName','13131313','132003000',714,30,21,908,NULL,NULL,NULL,NULL,'0',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2021-05-05','M','05/27/2021 09:43:56','{loginId}',130000005),('test8','FirstName','LastName','13131313','132004000',713,718,21,19,NULL,NULL,NULL,NULL,'1','1233','Etc address',NULL,NULL,NULL,NULL,NULL,NULL,'2021-05-04','F','05/27/2021 09:43:40','{loginId}',130000006),('test9','FirstName','LastName','13131313','131002001',706,29,21,19,NULL,NULL,NULL,NULL,'0',NULL,NULL,'032-111-2222',NULL,'010-1112-2222',NULL,NULL,NULL,'2021-04-01','F','05/27/2021 09:44:30','{loginId}',130000007);
/*!40000 ALTER TABLE `hr_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr_user_edu`
--

DROP TABLE IF EXISTS `hr_user_edu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr_user_edu` (
  `user_education_seq` int(11) NOT NULL AUTO_INCREMENT COMMENT '일련번호',
  `user_id` varchar(20) NOT NULL COMMENT '사용자 id',
  `entrance_date` date DEFAULT NULL COMMENT '입학일',
  `graducation_date` date DEFAULT NULL COMMENT '졸업일',
  `academy_name` varchar(50) DEFAULT NULL COMMENT '학교명',
  `major` varchar(50) DEFAULT NULL COMMENT '전공',
  `degree_cd` int(11) DEFAULT NULL COMMENT '학위',
  `academy_type_cd` int(11) DEFAULT NULL COMMENT '학교구분코드',
  PRIMARY KEY (`user_education_seq`,`user_id`),
  KEY `FK_hr_user_edu_user_id` (`user_id`),
  CONSTRAINT `FK_hr_user_edu_user_id` FOREIGN KEY (`user_id`) REFERENCES `hr_user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr_user_edu`
--

LOCK TABLES `hr_user_edu` WRITE;
/*!40000 ALTER TABLE `hr_user_edu` DISABLE KEYS */;
INSERT INTO `hr_user_edu` VALUES (1,'test3','2021-04-29','2021-05-12','ABC university','Major',913,47),(2,'test3','2021-05-04','2021-05-11','Test name',NULL,NULL,46),(3,'test3','2021-05-03','2021-05-04','Test name',NULL,NULL,45),(4,'test3','2021-05-02','2021-05-12','ABCCC','Major',913,47),(5,'test3','2021-05-05','2021-05-04','Test name',NULL,NULL,45),(6,'test3','2021-05-02','2021-05-26','Test name','Major',914,46);
/*!40000 ALTER TABLE `hr_user_edu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr_user_fire_train`
--

DROP TABLE IF EXISTS `hr_user_fire_train`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr_user_fire_train` (
  `fire_train_seq` int(11) NOT NULL AUTO_INCREMENT COMMENT '일련번호',
  `user_id` varchar(20) NOT NULL COMMENT '사용자 id',
  `train_name` varchar(100) DEFAULT NULL COMMENT '소방훈련명',
  `start_date` date DEFAULT NULL COMMENT '시작일',
  `end_date` date DEFAULT NULL COMMENT '종료일',
  `host` varchar(50) DEFAULT NULL COMMENT '주최',
  `perf` varchar(100) DEFAULT NULL COMMENT '실적',
  `org_name` varchar(50) DEFAULT NULL COMMENT '기관명',
  PRIMARY KEY (`fire_train_seq`,`user_id`),
  KEY `FK_hr_user_fire_train_user_id` (`user_id`),
  CONSTRAINT `FK_hr_user_fire_train_user_id` FOREIGN KEY (`user_id`) REFERENCES `hr_user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr_user_fire_train`
--

LOCK TABLES `hr_user_fire_train` WRITE;
/*!40000 ALTER TABLE `hr_user_fire_train` DISABLE KEYS */;
INSERT INTO `hr_user_fire_train` VALUES (2,'test3','Fire train 1','2021-05-11','2021-05-12','Host','Good','Institution 1');
/*!40000 ALTER TABLE `hr_user_fire_train` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `hr_user_license`
--

DROP TABLE IF EXISTS `hr_user_license`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `hr_user_license` (
  `license_seq` int(11) NOT NULL AUTO_INCREMENT COMMENT '일련번호',
  `user_id` varchar(20) NOT NULL COMMENT '사용자 id',
  `license_num` varchar(50) DEFAULT NULL COMMENT '자격증번호',
  `license_name` varchar(50) DEFAULT NULL COMMENT '자격증명',
  `acq_date` date DEFAULT NULL COMMENT '취득일',
  PRIMARY KEY (`license_seq`,`user_id`),
  KEY `FK_hr_user_license_user_id` (`user_id`),
  CONSTRAINT `FK_hr_user_license_user_id` FOREIGN KEY (`user_id`) REFERENCES `hr_user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `hr_user_license`
--

LOCK TABLES `hr_user_license` WRITE;
/*!40000 ALTER TABLE `hr_user_license` DISABLE KEYS */;
INSERT INTO `hr_user_license` VALUES (1,'test3','1213131','License 1','2021-05-04'),(3,'test3','34643456','License 12','2021-05-06'),(4,'test3','745745','License 22','2021-05-10'),(5,'test3','1213131','License 1','2021-05-04');
/*!40000 ALTER TABLE `hr_user_license` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `is_bldg`
--

DROP TABLE IF EXISTS `is_bldg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `is_bldg` (
  `bldg_seq` int(11) NOT NULL AUTO_INCREMENT COMMENT '동일련번호',
  `obj_id` int(20) NOT NULL COMMENT '대상물ID',
  `bldg_name` varchar(50) DEFAULT NULL COMMENT '동명',
  `struct_1` int(11) DEFAULT NULL COMMENT '건물구조식코드',
  `struct_2` int(11) DEFAULT NULL COMMENT '건물구조조코드',
  `struct_3` int(11) DEFAULT NULL COMMENT '건물구조즙코드',
  `ustory_cnt` int(3) DEFAULT NULL COMMENT '지상층수',
  `bstory_cnt` int(3) DEFAULT NULL COMMENT '지하층수',
  `floor_area` decimal(10,2) DEFAULT NULL COMMENT '바닥면적',
  `tot_area` decimal(10,2) DEFAULT NULL COMMENT '연면적',
  `sesc_stair_cnt` int(4) DEFAULT NULL COMMENT '특별피난계단수',
  `lot_area` decimal(10,2) DEFAULT NULL COMMENT '부지면적',
  `esc_stair_cnt` int(4) DEFAULT NULL COMMENT '피난계단수',
  `com_stair_cnt` int(4) DEFAULT NULL COMMENT '일반계단수',
  `out_stair_cnt` int(4) DEFAULT NULL COMMENT '옥외계단수',
  `incline_cnt` int(4) DEFAULT NULL COMMENT '경사로수',
  `emglift_cnt` int(4) DEFAULT NULL COMMENT '비상승강기수',
  `escal_cnt` int(4) DEFAULT NULL COMMENT '에스카레이터수',
  `exit_cnt` int(4) DEFAULT NULL COMMENT '비상구수',
  `root_yn` char(1) DEFAULT '0' COMMENT '옥상유무',
  `home_cnt` int(4) DEFAULT NULL COMMENT '세대수',
  `use_yn` char(1) DEFAULT '1' COMMENT '사용여부',
  `rpsn_bldg_yn` char(1) DEFAULT '1' COMMENT '대표건물동여부',
  `house_cnt` int(4) DEFAULT NULL COMMENT '가구수',
  `entrps_cnt` int(4) DEFAULT NULL COMMENT '업체수',
  `inhbtnt_cnt` int(4) DEFAULT NULL COMMENT '주민수',
  `bldg_height` decimal(7,2) DEFAULT NULL COMMENT '건물동높이',
  `elvtr_cnt` int(4) DEFAULT NULL COMMENT '승강기수',
  `remove_date` date DEFAULT NULL COMMENT '철거날짜',
  `remove_yn` char(1) DEFAULT NULL COMMENT '철거여부',
  `bldg_main_use_cd` int(11) DEFAULT NULL COMMENT '건물동주용도코드',
  `bldg_sub_use_cd` int(11) DEFAULT NULL COMMENT '건물동부용도코드',
  `use_confm_date` date DEFAULT NULL COMMENT '사용승인날짜',
  PRIMARY KEY (`bldg_seq`,`obj_id`),
  KEY `FK_is_bldg_obj_id` (`obj_id`),
  CONSTRAINT `FK_is_bldg_obj_id` FOREIGN KEY (`obj_id`) REFERENCES `is_obj` (`obj_id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COMMENT='대상물동별내역';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `is_bldg`
--

LOCK TABLES `is_bldg` WRITE;
/*!40000 ALTER TABLE `is_bldg` DISABLE KEYS */;
INSERT INTO `is_bldg` VALUES (1,130000002,'101',666,670,684,2,1,4.00,5.00,15,3.00,14,11,12,13,18,NULL,16,'1',7,'1','1',8,9,10,6.00,17,NULL,'0',490,489,'2021-05-04'),(2,130000002,'102',667,670,684,212,313,555.00,512.00,NULL,31.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0',555,'1','1',555,NULL,NULL,12.00,NULL,NULL,'0',NULL,NULL,NULL),(3,130000001,'103',666,670,685,31,2,7.00,9.00,NULL,54.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0',6,'1','1',NULL,NULL,NULL,96.00,NULL,NULL,'0',20010500,203,NULL),(4,130000001,'104',666,670,684,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0',NULL,'1','1',NULL,NULL,NULL,NULL,NULL,NULL,'0',NULL,NULL,NULL),(5,130000001,'105',667,671,684,31,888,14.00,7.00,NULL,13.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0',84,'1','1',86,678,96,74.00,NULL,NULL,'0',NULL,NULL,NULL),(6,130000008,'101',665,669,684,133,2,41.00,121.00,NULL,13.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0',41,'1','1',21,41,21,31.00,NULL,NULL,'0',NULL,NULL,NULL),(7,130000008,'102',665,670,683,2,3,NULL,NULL,NULL,3.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0',NULL,'1','1',NULL,NULL,NULL,NULL,NULL,NULL,'0',NULL,NULL,NULL),(8,130000008,'103',667,670,685,13,3,13.00,141.00,NULL,31.00,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0',41,'1','1',NULL,NULL,NULL,12.00,NULL,NULL,'0',NULL,NULL,NULL),(9,130000003,'101',665,669,684,20,30,20.10,30.30,412,30.20,31,123,312,312,121,NULL,123,'0',NULL,'1','1',NULL,NULL,NULL,NULL,141,NULL,'0',20010600,163,NULL);
/*!40000 ALTER TABLE `is_bldg` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `is_dgrmgr`
--

DROP TABLE IF EXISTS `is_dgrmgr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `is_dgrmgr` (
  `dgrmgr_seq` int(11) NOT NULL AUTO_INCREMENT COMMENT '안전관리자일련번호',
  `obj_id` int(20) NOT NULL COMMENT '대상물ID',
  `dgrmgr_name` varchar(50) DEFAULT NULL COMMENT '안전관리자명',
  `zip_code` varchar(10) DEFAULT NULL COMMENT '우편번호',
  `address` varchar(100) DEFAULT NULL COMMENT '주소',
  `tel_num` varchar(20) DEFAULT NULL COMMENT '전화번호',
  `use_yn` char(1) DEFAULT '1' COMMENT '사용여부',
  PRIMARY KEY (`dgrmgr_seq`,`obj_id`),
  KEY `FK_is_dgrmgr_obj_id` (`obj_id`),
  CONSTRAINT `FK_is_dgrmgr_obj_id` FOREIGN KEY (`obj_id`) REFERENCES `is_obj` (`obj_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='안전관리자';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `is_dgrmgr`
--

LOCK TABLES `is_dgrmgr` WRITE;
/*!40000 ALTER TABLE `is_dgrmgr` DISABLE KEYS */;
/*!40000 ALTER TABLE `is_dgrmgr` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `is_dsrstat`
--

DROP TABLE IF EXISTS `is_dsrstat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `is_dsrstat` (
  `work_dtime` varchar(20) NOT NULL COMMENT '근무일시',
  `situ_seq` int(11) NOT NULL COMMENT '상황일련번호',
  `make_user_id` varchar(20) NOT NULL COMMENT '작성자ID',
  `conf_user_id` varchar(20) DEFAULT NULL COMMENT '확인자ID',
  `juris_ward_id` varchar(9) DEFAULT NULL COMMENT '관할서소ID',
  `worksitu_desc` varchar(200) DEFAULT NULL COMMENT '근무상황내용',
  `worksitu_cd` int(11) DEFAULT NULL COMMENT '근무종별코드',
  `lchg_dtime` varchar(20) DEFAULT NULL COMMENT '수정일시',
  `lchg_user_id` varchar(20) DEFAULT NULL COMMENT '수정자ID',
  `created_date` varchar(20) DEFAULT NULL COMMENT '등록일자',
  PRIMARY KEY (`work_dtime`,`situ_seq`,`make_user_id`),
  KEY `FK_is_dsrstat_make_user_id` (`make_user_id`),
  KEY `FK_is_dsrstat_conf_user_id` (`conf_user_id`),
  KEY `FK_is_dsrstat_juris_ward_id` (`juris_ward_id`),
  CONSTRAINT `FK_is_dsrstat_conf_user_id` FOREIGN KEY (`conf_user_id`) REFERENCES `hr_user` (`user_id`),
  CONSTRAINT `FK_is_dsrstat_juris_ward_id` FOREIGN KEY (`juris_ward_id`) REFERENCES `is_ward` (`ward_id`),
  CONSTRAINT `FK_is_dsrstat_make_user_id` FOREIGN KEY (`make_user_id`) REFERENCES `hr_user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='근무상황';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `is_dsrstat`
--

LOCK TABLES `is_dsrstat` WRITE;
/*!40000 ALTER TABLE `is_dsrstat` DISABLE KEYS */;
/*!40000 ALTER TABLE `is_dsrstat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `is_fmgr`
--

DROP TABLE IF EXISTS `is_fmgr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `is_fmgr` (
  `fmgr_seq` int(11) NOT NULL AUTO_INCREMENT COMMENT '방화관리자일련번호',
  `obj_id` int(20) NOT NULL COMMENT '대상물ID',
  `fmgr_name` varchar(50) DEFAULT NULL COMMENT '방화관리자명',
  `fmgr_birthday` date DEFAULT NULL COMMENT '방화관리자생년월일',
  `zip_code` varchar(10) DEFAULT NULL COMMENT '우편번호',
  `address` varchar(100) DEFAULT NULL COMMENT '주소',
  `tel_num` varchar(20) DEFAULT NULL COMMENT '전화번호',
  `assign_date` date DEFAULT NULL COMMENT '선임일자',
  `dismiss_date` date DEFAULT NULL COMMENT '해임일자',
  `use_yn` char(1) DEFAULT '1' COMMENT '사용여부',
  `citizen_no` varchar(20) DEFAULT NULL COMMENT '주민번호',
  `pub_fmng_yn` char(1) DEFAULT NULL COMMENT '공동방화관리여부',
  `dismiss_remark` varchar(200) DEFAULT NULL COMMENT '해임사유',
  `title` varchar(50) DEFAULT NULL COMMENT '직위',
  PRIMARY KEY (`fmgr_seq`,`obj_id`),
  KEY `FK_is_fmgr_obj_id` (`obj_id`),
  CONSTRAINT `FK_is_fmgr_obj_id` FOREIGN KEY (`obj_id`) REFERENCES `is_obj` (`obj_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='방화관리자';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `is_fmgr`
--

LOCK TABLES `is_fmgr` WRITE;
/*!40000 ALTER TABLE `is_fmgr` DISABLE KEYS */;
/*!40000 ALTER TABLE `is_fmgr` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `is_hosp`
--

DROP TABLE IF EXISTS `is_hosp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `is_hosp` (
  `hosp_id` int(11) NOT NULL COMMENT '병원ID',
  `hosp_name` varchar(50) DEFAULT NULL COMMENT '병원명',
  `hosp_cls_cd` int(11) DEFAULT NULL COMMENT '병원구분코드',
  `er_medical_yn` char(1) DEFAULT NULL COMMENT '지정응급의료기관/센터여부',
  `main_treat` varchar(250) DEFAULT NULL COMMENT '주진료과목',
  `sickbed_cnt` int(4) DEFAULT NULL COMMENT '병상수',
  `doctor_cnt` int(4) DEFAULT NULL COMMENT '의사수',
  `ambul_cnt` int(4) DEFAULT NULL COMMENT '구급차수',
  `zip_code` varchar(10) DEFAULT NULL COMMENT '우편번호',
  `address` varchar(100) DEFAULT NULL COMMENT '주소',
  `day_tel` varchar(20) DEFAULT NULL COMMENT '주간전화번호',
  `night_tel` varchar(20) DEFAULT NULL COMMENT '야간전화번호',
  `ward_id` varchar(9) DEFAULT NULL COMMENT '서·센터ID',
  `remove_yn` char(1) DEFAULT NULL COMMENT '철거여부',
  `remark` varchar(300) DEFAULT NULL COMMENT '비고',
  `on_duty_yn` char(1) DEFAULT NULL COMMENT '당직근무여부',
  `lchg_dtime` varchar(20) DEFAULT NULL COMMENT '수정일시',
  `lchg_user_id` varchar(20) DEFAULT NULL COMMENT '수정자ID',
  `use_yn` char(1) DEFAULT NULL COMMENT '사용여부',
  `undgrd_yn` char(1) DEFAULT '0' COMMENT '지하 여부',
  PRIMARY KEY (`hosp_id`),
  KEY `is_hosp_FK` (`ward_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='병원';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `is_hosp`
--

LOCK TABLES `is_hosp` WRITE;
/*!40000 ALTER TABLE `is_hosp` DISABLE KEYS */;
/*!40000 ALTER TABLE `is_hosp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `is_hyd`
--

DROP TABLE IF EXISTS `is_hyd`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `is_hyd` (
  `hyd_id` int(11) NOT NULL COMMENT '소방용수ID',
  `ward_id` varchar(9) DEFAULT NULL COMMENT '서·센터ID',
  `form_cd` int(11) DEFAULT NULL COMMENT '형식코드',
  `hyd_wtr_no` varchar(100) DEFAULT NULL COMMENT '소방용수수리번호',
  `zip_code` varchar(10) DEFAULT NULL COMMENT '우편번호',
  `address` varchar(100) DEFAULT NULL COMMENT '주소',
  `hydknd_cd` int(11) DEFAULT NULL COMMENT '소방용수시설종별코드',
  `signpost_yn` char(1) DEFAULT NULL COMMENT '표지판설치여부',
  `remark` varchar(300) DEFAULT NULL COMMENT '비고',
  `adj_bldg` varchar(100) DEFAULT NULL COMMENT '인근건물',
  `remove_date` date DEFAULT NULL COMMENT '철거일자',
  `lchg_dtime` varchar(20) DEFAULT NULL COMMENT '수정일시',
  `lchg_user_id` varchar(20) DEFAULT NULL COMMENT '수정자ID',
  `use_yn` char(1) DEFAULT NULL COMMENT '사용여부',
  `x` varchar(20) DEFAULT NULL COMMENT '소방용수X좌표',
  `y` varchar(20) DEFAULT NULL COMMENT '소방용수Y좌표',
  PRIMARY KEY (`hyd_id`),
  KEY `FK_is_hyd_ward_id` (`ward_id`),
  CONSTRAINT `FK_is_hyd_ward_id` FOREIGN KEY (`ward_id`) REFERENCES `is_ward` (`ward_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='소방용수';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `is_hyd`
--

LOCK TABLES `is_hyd` WRITE;
/*!40000 ALTER TABLE `is_hyd` DISABLE KEYS */;
/*!40000 ALTER TABLE `is_hyd` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `is_jurisward`
--

DROP TABLE IF EXISTS `is_jurisward`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `is_jurisward` (
  `ward_id` varchar(9) NOT NULL COMMENT '서·센터ID',
  `zip_code` varchar(10) NOT NULL COMMENT '관할우편번호',
  `juris_divide_yn` char(1) DEFAULT NULL COMMENT '관할분할여부',
  `basic_juris_yn` char(1) DEFAULT NULL COMMENT '기본관할여부',
  PRIMARY KEY (`ward_id`,`zip_code`),
  CONSTRAINT `FK_juris_ward_id` FOREIGN KEY (`ward_id`) REFERENCES `is_ward` (`ward_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='서.센터 관할 정보';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `is_jurisward`
--

LOCK TABLES `is_jurisward` WRITE;
/*!40000 ALTER TABLE `is_jurisward` DISABLE KEYS */;
/*!40000 ALTER TABLE `is_jurisward` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `is_obj`
--

DROP TABLE IF EXISTS `is_obj`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `is_obj` (
  `obj_id` int(20) NOT NULL COMMENT '대상물ID',
  `ward_id` varchar(9) DEFAULT NULL COMMENT '서·센터ID',
  `obj_man_num` varchar(20) DEFAULT NULL COMMENT '대상물관리번호',
  `used_cd` int(11) DEFAULT NULL COMMENT '주용도코드',
  `obj_cd` int(11) DEFAULT NULL COMMENT '대상물구분코드',
  `const_name` varchar(50) DEFAULT NULL COMMENT '대상물명',
  `rpsn_firm_name` varchar(50) DEFAULT NULL COMMENT '대표상호명',
  `zip_code` varchar(10) DEFAULT NULL COMMENT 'zip code',
  `address` varchar(100) DEFAULT NULL COMMENT 'address',
  `use_yn` char(1) DEFAULT '1' COMMENT '사용여부',
  `obj_std_cd` int(11) DEFAULT NULL COMMENT '대상물기준코드',
  `bldg_nm` varchar(200) DEFAULT NULL COMMENT '건물명',
  `x` varchar(20) DEFAULT NULL COMMENT 'x',
  `y` varchar(20) DEFAULT NULL COMMENT 'y',
  `weakobj_yn` char(1) DEFAULT NULL COMMENT '대형화재취약대상여부',
  `selfobj_yn` char(1) DEFAULT NULL COMMENT '자체점검대상여부',
  `adg_info` varchar(1000) DEFAULT NULL COMMENT '인근정보',
  `day_tel` varchar(20) DEFAULT NULL COMMENT '주간전화번호',
  `night_tel` varchar(20) DEFAULT NULL COMMENT '야간전화번호',
  `prevdst_tel` varchar(20) DEFAULT NULL COMMENT '방재실전화번호',
  `use_confm_date` date DEFAULT NULL COMMENT '사용승인날짜',
  `instit_yn` char(1) DEFAULT NULL COMMENT '공공기관여부',
  `instit_cls_cd` int(11) DEFAULT NULL COMMENT '공공기관분류코드',
  `pubfacl_yn` char(1) DEFAULT NULL COMMENT '다중이용업여부',
  `weakobj_cd` int(11) DEFAULT NULL COMMENT '대행화재취약대상코드',
  `sub_use_desc` varchar(100) DEFAULT NULL COMMENT '부용도내역',
  `tunnel_cls_cd` int(11) DEFAULT NULL COMMENT '터널구분코드',
  `tunnel_length` decimal(5,1) DEFAULT NULL COMMENT '터널길이',
  PRIMARY KEY (`obj_id`),
  KEY `FK_is_obj_ward_id` (`ward_id`),
  CONSTRAINT `FK_is_obj_ward_id` FOREIGN KEY (`ward_id`) REFERENCES `is_ward` (`ward_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='대상물';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `is_obj`
--

LOCK TABLES `is_obj` WRITE;
/*!40000 ALTER TABLE `is_obj` DISABLE KEYS */;
INSERT INTO `is_obj` VALUES (130000001,'131001000','O120-3131',639,505,'Object Name','Representative name','123333','Etc address','0',618,'BD name','12\"22','66\"88','1','1','Test','02-1111-1111','02-2222-2222','02-333-3333','2021-05-05','1',486,'0',488,'Sub use',485,2.3),(130000002,'131001000','O120-3131',639,508,'Object Name','Representative name','123333','Etc address','0',624,'BD name','12\"22','66\"88','1','1','Test 12113131','02-1111-1111','02-2222-2222','02-3333-3333','2021-05-12','1',488,'1',488,'Sub use',488,2.3),(130000003,'131001000','O120-3131222',637,NULL,'Object Name22233','Representative name222','123333','Etc address','1',618,'BD name222','12\"22','66\"88','1','1','Test11231','02-1111-1111','02-2222-2222','02-3333-3333','2021-05-06','1',483,'1',487,'Sub use',488,2.3),(130000004,'132003000','O120-3131',637,507,'Object Name','Representative name',NULL,NULL,'0',618,'BD name',NULL,NULL,'0','0',NULL,NULL,NULL,NULL,NULL,'0',NULL,'0',NULL,NULL,NULL,NULL),(130000005,'132003000','O120-3131',NULL,NULL,'Object Name','Representative name',NULL,NULL,'0',NULL,NULL,NULL,NULL,'0','0',NULL,NULL,NULL,NULL,NULL,'0',NULL,'0',NULL,NULL,NULL,NULL),(130000006,'132003000','O120-3131',NULL,NULL,'Object Name',NULL,NULL,NULL,'0',NULL,NULL,NULL,NULL,'0','0',NULL,NULL,NULL,NULL,NULL,'0',NULL,'0',NULL,NULL,NULL,NULL),(130000007,'132003000','O120-3131',NULL,NULL,'Object Name','Representative name',NULL,NULL,'0',NULL,'BD name',NULL,NULL,'0','0',NULL,NULL,NULL,NULL,NULL,'0',NULL,'0',NULL,NULL,NULL,NULL),(130000008,'132003000','O120-3131',645,509,'Object Name111','Representative name','123333','Etc address','1',626,'BD name','12\"22','66\"88','1','1','TESET121313','02-1111-1111','02-2222-2222','02-3333-3333','2021-05-12','1',485,'1',488,'Sub use',488,2.3),(130000009,'131001000','O120-3131',NULL,NULL,'Object Name','Representative name',NULL,NULL,NULL,NULL,'BD name',NULL,NULL,'0','0',NULL,NULL,NULL,NULL,NULL,'0',NULL,'0',NULL,NULL,NULL,NULL),(130000010,'132003000','O120-3131',NULL,NULL,'Object Name',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0','0',NULL,NULL,NULL,NULL,NULL,'0',NULL,'0',NULL,NULL,NULL,NULL),(130000011,'132003000','O120-3131',NULL,NULL,'Object Name',NULL,NULL,NULL,'1',NULL,NULL,NULL,NULL,'0','0',NULL,NULL,NULL,NULL,NULL,'0',NULL,'0',NULL,NULL,NULL,NULL),(130000012,NULL,'O120-3131',636,507,'Object Name','Representative name',NULL,NULL,'1',618,'BD name',NULL,NULL,'0','0',NULL,NULL,NULL,NULL,NULL,'0',NULL,'0',NULL,NULL,NULL,NULL),(130000013,'131002000','O120-3131',NULL,NULL,'Object Name','Representative name',NULL,NULL,'1',NULL,NULL,NULL,NULL,'0','0',NULL,NULL,NULL,NULL,NULL,'0',NULL,'0',NULL,NULL,NULL,NULL),(130000014,'131001000','O120-3131',NULL,NULL,'Object Name22233','Representative name',NULL,NULL,'1',NULL,NULL,NULL,NULL,'0','0',NULL,NULL,NULL,NULL,NULL,'0',NULL,'0',NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `is_obj` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `is_objdrw`
--

DROP TABLE IF EXISTS `is_objdrw`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `is_objdrw` (
  `obj_id` int(20) NOT NULL COMMENT '대상물ID',
  `drw_seq` int(11) NOT NULL AUTO_INCREMENT COMMENT '도면일련번호',
  `file_name` varchar(100) DEFAULT NULL COMMENT '파일명',
  `file_path` varchar(100) DEFAULT NULL COMMENT '파일경로',
  `remark` varchar(1000) DEFAULT NULL COMMENT '비고',
  `use_yn` char(1) DEFAULT '1' COMMENT '사용여부',
  PRIMARY KEY (`drw_seq`,`obj_id`),
  KEY `FK_is_objdrw_obj_id` (`obj_id`),
  CONSTRAINT `FK_is_objdrw_obj_id` FOREIGN KEY (`obj_id`) REFERENCES `is_obj` (`obj_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='대상물도면정보';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `is_objdrw`
--

LOCK TABLES `is_objdrw` WRITE;
/*!40000 ALTER TABLE `is_objdrw` DISABLE KEYS */;
/*!40000 ALTER TABLE `is_objdrw` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `is_objfireequip`
--

DROP TABLE IF EXISTS `is_objfireequip`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `is_objfireequip` (
  `obj_id` int(20) NOT NULL COMMENT '대상물ID',
  `bldg_seq` int(11) NOT NULL COMMENT '동일련번호',
  `story_seq` int(11) NOT NULL COMMENT '층번호',
  `in_exting_cnt` int(4) DEFAULT NULL COMMENT '옥내소화전',
  `out_exting_cnt` binary(1) DEFAULT NULL COMMENT '옥외소화전',
  `exting_pump_cnt` int(4) DEFAULT NULL COMMENT '동력소방펌프',
  `sprinkler_h_cnt` int(5) DEFAULT NULL COMMENT '스프링클러H',
  `sprinkler_av_cnt` int(5) DEFAULT NULL COMMENT '스프링클러AV',
  `spray_exting_h_cnt` int(4) DEFAULT NULL COMMENT '물분무소화H',
  `spray_exting_av_cnt` int(4) DEFAULT NULL COMMENT '물분모소화AV',
  `po_exting_h_cnt` int(4) DEFAULT NULL COMMENT '포 소화_H',
  `po_exting_av_cnt` int(4) DEFAULT NULL COMMENT '포소화AV',
  `carbon_diox_h_cnt` int(4) DEFAULT NULL COMMENT '이산화탄소H',
  `carbon_diox_av_cnt` int(4) DEFAULT NULL COMMENT '이산화탄소AV',
  `halogen_comp_h_cnt` int(4) DEFAULT NULL COMMENT '할로겐화합물H',
  `harlogen_comp_av_cnt` int(4) DEFAULT NULL COMMENT '할로겐화합물AV',
  `powder_exting_h_cnt` int(4) DEFAULT NULL COMMENT '분말소화H',
  `powder_exting_av_cnt` int(4) DEFAULT NULL COMMENT '분말소화AV',
  `slide_cnt` int(4) DEFAULT NULL COMMENT '미끄림대',
  `ladder_cnt` int(4) DEFAULT NULL COMMENT '피난사다리',
  `rescue_cnt` int(4) DEFAULT NULL COMMENT '구조대',
  `desc_slow_device_cnt` int(4) DEFAULT NULL COMMENT '완강기',
  `measure_equip_cnt` int(4) DEFAULT NULL COMMENT '피난교',
  `measure_rope_cnt` int(4) DEFAULT NULL COMMENT '피난밧줄',
  `safe_mat_cnt` int(4) DEFAULT NULL COMMENT '공기안전매트',
  `rescue_equip_cnt` int(4) DEFAULT NULL COMMENT '인명구조기구',
  `emg_light_cnt` int(5) DEFAULT NULL COMMENT '비상조명등',
  `water_exting_cnt` int(4) DEFAULT NULL COMMENT '상수도소화용수',
  `exting_water_cnt` int(4) DEFAULT NULL COMMENT '소화수조',
  `lwtr_cnt` int(4) DEFAULT NULL COMMENT '저수조',
  `wtrpipe_cnt` int(4) DEFAULT NULL COMMENT '연결송수관',
  `water_springkling_cnt` int(4) DEFAULT NULL COMMENT '연결살수',
  `emg_plug_cnt` int(4) DEFAULT NULL COMMENT '비상콘센트',
  `wireless_comm_cnt` int(4) DEFAULT NULL COMMENT '무선통신보조',
  `use_yn` char(1) DEFAULT '1' COMMENT '사용 여부',
  `exting_cnt` int(4) DEFAULT NULL COMMENT '소화기',
  `simplcty_exting_cnt` int(4) DEFAULT NULL COMMENT '간이 소화 용구',
  `emg_waring_cnt` int(4) DEFAULT NULL COMMENT '비상 정보',
  `emg_brodc_cnt` int(4) DEFAULT NULL COMMENT '비상 방송',
  `lkge_waring_cnt` int(4) DEFAULT NULL COMMENT '누전 경보기',
  `auto_fire_find_sens_cnt` int(4) DEFAULT NULL COMMENT '자동 화재 탐지_감지기',
  `auto_fire_find_circuit_cnt` int(4) DEFAULT NULL COMMENT '자동 화재 탐지_회로',
  `auto_fire_newsf_cnt` int(4) DEFAULT NULL COMMENT '자동 화재 속보',
  `gas_lkge_waring_cnt` int(4) DEFAULT NULL COMMENT '가스 누설 경보기',
  `induce_light_cnt` int(4) DEFAULT NULL COMMENT '유도등',
  `induce_singpost_cnt` int(4) DEFAULT NULL COMMENT '유도 표지',
  `hyd_equip_etc_cnt` int(4) DEFAULT NULL COMMENT '소방 용수 설비 기타',
  `resmoke_cnt` int(4) DEFAULT NULL COMMENT '제연',
  `curtain_cnt` int(4) DEFAULT NULL COMMENT '커튼',
  `carssette_cnt` int(4) DEFAULT NULL COMMENT '카세트',
  `resist_etc_cnt` int(4) DEFAULT NULL COMMENT '방염 기타',
  PRIMARY KEY (`obj_id`,`bldg_seq`,`story_seq`),
  KEY `FK_is_objfireequip` (`story_seq`,`obj_id`,`bldg_seq`),
  CONSTRAINT `FK_is_objfireequip` FOREIGN KEY (`story_seq`, `obj_id`, `bldg_seq`) REFERENCES `is_story` (`story_seq`, `obj_id`, `bldg_seq`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='대상물소방시설';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `is_objfireequip`
--

LOCK TABLES `is_objfireequip` WRITE;
/*!40000 ALTER TABLE `is_objfireequip` DISABLE KEYS */;
/*!40000 ALTER TABLE `is_objfireequip` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `is_objhist`
--

DROP TABLE IF EXISTS `is_objhist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `is_objhist` (
  `objhist_seq` int(11) NOT NULL AUTO_INCREMENT COMMENT '건축연혁일련번호',
  `obj_id` int(20) NOT NULL COMMENT '대상물ID',
  `bldg_seq` int(11) NOT NULL COMMENT '동일련번호',
  `hist_type_cd` int(11) DEFAULT NULL COMMENT '연혁종별코드',
  `const_use` varchar(500) DEFAULT NULL COMMENT '건축물용도',
  `struct_1` int(11) DEFAULT NULL COMMENT '건물구조식코드',
  `struct_2` int(11) DEFAULT NULL COMMENT '건물구조조코드',
  `struct_3` int(11) DEFAULT NULL COMMENT '건물구조즙코드',
  `floor_area` decimal(10,2) DEFAULT NULL COMMENT '바닥면적',
  `tot_area` decimal(10,2) DEFAULT NULL COMMENT '연면적',
  `ustory_cnt` int(3) DEFAULT NULL COMMENT '지상층수',
  `bstory_cnt` int(3) DEFAULT NULL COMMENT '지하층수',
  `grant_date` date DEFAULT NULL COMMENT '허가일자',
  `preuse_yn` char(1) DEFAULT '0' COMMENT '가사용여부',
  `finish_date` date DEFAULT NULL COMMENT '준공일자',
  `etc` varchar(300) DEFAULT NULL COMMENT '기타',
  `use_yn` char(1) DEFAULT '1' COMMENT '사용여부',
  `chg_desc` varchar(1000) DEFAULT NULL COMMENT '변경내용',
  PRIMARY KEY (`objhist_seq`,`obj_id`,`bldg_seq`),
  KEY `FK_is_objhist` (`bldg_seq`,`obj_id`),
  CONSTRAINT `FK_is_objhist` FOREIGN KEY (`bldg_seq`, `obj_id`) REFERENCES `is_bldg` (`bldg_seq`, `obj_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='대상물건축연혁';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `is_objhist`
--

LOCK TABLES `is_objhist` WRITE;
/*!40000 ALTER TABLE `is_objhist` DISABLE KEYS */;
/*!40000 ALTER TABLE `is_objhist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `is_objpic`
--

DROP TABLE IF EXISTS `is_objpic`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `is_objpic` (
  `obj_photo_seq` int(11) NOT NULL AUTO_INCREMENT COMMENT '경방계획도사진일련번호',
  `obj_id` int(20) NOT NULL COMMENT '대상물ID',
  `obj_photo_type_cd` int(11) DEFAULT NULL COMMENT '경방계획도사진구분코드',
  `remark` varchar(1000) DEFAULT NULL COMMENT '비고',
  `file_name` varchar(100) DEFAULT NULL COMMENT '파일명',
  `file_path` varchar(100) DEFAULT NULL COMMENT '파일경로',
  `use_yn` char(1) DEFAULT '1' COMMENT '사용여부',
  PRIMARY KEY (`obj_photo_seq`,`obj_id`),
  KEY `FK_is_objpic_obj_id` (`obj_id`),
  CONSTRAINT `FK_is_objpic_obj_id` FOREIGN KEY (`obj_id`) REFERENCES `is_obj` (`obj_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='경방계획도사진';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `is_objpic`
--

LOCK TABLES `is_objpic` WRITE;
/*!40000 ALTER TABLE `is_objpic` DISABLE KEYS */;
/*!40000 ALTER TABLE `is_objpic` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `is_objplan`
--

DROP TABLE IF EXISTS `is_objplan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `is_objplan` (
  `obj_id` int(11) NOT NULL COMMENT '대상물ID',
  `env_desc` varchar(1000) DEFAULT NULL COMMENT '주위환경',
  `expand_rsn` varchar(1000) DEFAULT NULL COMMENT '화재취약및연소확대요인',
  `resc_esc_cplan` varchar(1000) DEFAULT NULL COMMENT '인명구조또는피난대책',
  `fmng_issue` varchar(1000) DEFAULT NULL COMMENT '방화관리문제점',
  `selfvolun_org` varchar(1000) DEFAULT NULL COMMENT '자위소방재조직상황',
  `realest_expt_amt` decimal(12,0) DEFAULT NULL COMMENT '부동상피해예상액(원)',
  `mvest_expt_amt` decimal(12,0) DEFAULT NULL COMMENT '동산피해예상액(원)',
  `tot_amt` decimal(12,0) DEFAULT NULL COMMENT '총액(원)',
  `ward1_dist` decimal(8,1) DEFAULT NULL COMMENT '본서와의거리(km)',
  `ward2_dist` decimal(8,2) DEFAULT NULL COMMENT '파출소와의거리(km)',
  `esc_place` varchar(1000) DEFAULT NULL COMMENT '대피장소',
  `training_guide_desc` varchar(1000) DEFAULT NULL COMMENT '소방훈련지도개요',
  `etc_remark` varchar(2000) DEFAULT NULL COMMENT '특기사항',
  `use_yn` char(1) DEFAULT '1' COMMENT '사용여부',
  PRIMARY KEY (`obj_id`),
  CONSTRAINT `is_objplan_FK` FOREIGN KEY (`obj_id`) REFERENCES `is_obj` (`obj_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='경방계획';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `is_objplan`
--

LOCK TABLES `is_objplan` WRITE;
/*!40000 ALTER TABLE `is_objplan` DISABLE KEYS */;
/*!40000 ALTER TABLE `is_objplan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `is_objrel`
--

DROP TABLE IF EXISTS `is_objrel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `is_objrel` (
  `obj_id` int(20) NOT NULL COMMENT '대상물ID',
  `rel_seq` int(11) NOT NULL COMMENT '관계자일련번호',
  `rel_cd` int(11) DEFAULT NULL COMMENT '관계자구분코드',
  `rel_name` varchar(50) DEFAULT NULL COMMENT '관계자명',
  `rel_birthday` date DEFAULT NULL COMMENT '관계자생년월일',
  `zip_code` varchar(10) DEFAULT NULL COMMENT '우편번호',
  `address` varchar(100) DEFAULT NULL COMMENT '주소',
  `tel_num` varchar(20) DEFAULT NULL COMMENT '전화번호',
  `use_yn` char(1) DEFAULT '1' COMMENT '사용여부',
  `job_desc` varchar(50) DEFAULT NULL COMMENT '직업',
  `citizen_no` varchar(20) DEFAULT NULL COMMENT '주민번호',
  PRIMARY KEY (`rel_seq`,`obj_id`),
  KEY `FK_is_objrel_obj_id` (`obj_id`),
  CONSTRAINT `FK_is_objrel_obj_id` FOREIGN KEY (`obj_id`) REFERENCES `is_obj` (`obj_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='대상물관계자내역';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `is_objrel`
--

LOCK TABLES `is_objrel` WRITE;
/*!40000 ALTER TABLE `is_objrel` DISABLE KEYS */;
/*!40000 ALTER TABLE `is_objrel` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `is_org`
--

DROP TABLE IF EXISTS `is_org`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `is_org` (
  `org_id` int(11) NOT NULL COMMENT '유관기관 ID',
  `ward_id` varchar(9) NOT NULL COMMENT '서센터ID',
  `org_name` varchar(50) DEFAULT NULL COMMENT '유관기관 명',
  `org_cls_cd` int(11) DEFAULT NULL COMMENT '조직분류코드',
  `zip_code` varchar(10) DEFAULT NULL COMMENT '우편번호',
  `address` varchar(100) DEFAULT NULL COMMENT '주소',
  `day_tel` varchar(20) DEFAULT NULL COMMENT '주간전화번호',
  `night_tel` varchar(20) DEFAULT NULL COMMENT '야간전화번호',
  `day_fax` varchar(20) DEFAULT NULL COMMENT '주간팩스번호',
  `night_fax` varchar(20) DEFAULT NULL COMMENT '야간팩스번호',
  `up_org_name` varchar(50) DEFAULT NULL COMMENT '상위조직명',
  `agree_yn` char(1) DEFAULT NULL COMMENT '승인여부',
  `dsr_sprt_org_yn` char(1) DEFAULT NULL COMMENT '재난지원여부',
  `dsr_org_yn` char(1) DEFAULT NULL COMMENT '재난조직여부',
  `touch_pad_yn` char(1) DEFAULT NULL COMMENT '터치패드여부',
  `remark` varchar(300) DEFAULT NULL COMMENT '비고',
  `etc` varchar(100) DEFAULT NULL COMMENT '기타내용',
  `lchg_dtime` varchar(20) DEFAULT NULL COMMENT '변경일시',
  `lchg_user_id` varchar(20) DEFAULT NULL COMMENT '변경자ID',
  `use_yn` char(1) DEFAULT '1' COMMENT '사용여부',
  `cell` varchar(20) DEFAULT NULL COMMENT '이동전화번호',
  `cell_owner_name` varchar(20) DEFAULT NULL COMMENT '이동전화소유자명',
  `undgrd_fg` char(1) DEFAULT NULL COMMENT '지하여부',
  PRIMARY KEY (`org_id`),
  KEY `FK_is_org_ward_id` (`ward_id`),
  CONSTRAINT `FK_is_org_ward_id` FOREIGN KEY (`ward_id`) REFERENCES `is_ward` (`ward_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='유관기관 테이블';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `is_org`
--

LOCK TABLES `is_org` WRITE;
/*!40000 ALTER TABLE `is_org` DISABLE KEYS */;
/*!40000 ALTER TABLE `is_org` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `is_orgfire`
--

DROP TABLE IF EXISTS `is_orgfire`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `is_orgfire` (
  `org_id` int(11) NOT NULL COMMENT '유관기관 ID',
  `fight_cls_cd` int(11) NOT NULL COMMENT '동원장비코드',
  `mob_fight_cnt` int(4) DEFAULT NULL COMMENT '동원소방력수',
  `lchg_dtime` varchar(20) DEFAULT NULL COMMENT '수정일시',
  `lchg_user_id` varchar(20) DEFAULT NULL COMMENT '수정자ID',
  `use_yn` char(1) DEFAULT NULL COMMENT '사용여부',
  PRIMARY KEY (`org_id`,`fight_cls_cd`),
  CONSTRAINT `FK_is_orgfire_org_id` FOREIGN KEY (`org_id`) REFERENCES `is_org` (`org_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='유관기관동원소방력';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `is_orgfire`
--

LOCK TABLES `is_orgfire` WRITE;
/*!40000 ALTER TABLE `is_orgfire` DISABLE KEYS */;
/*!40000 ALTER TABLE `is_orgfire` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `is_orgman`
--

DROP TABLE IF EXISTS `is_orgman`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `is_orgman` (
  `org_id` int(11) NOT NULL COMMENT '유관기관 ID',
  `skill_licn_cd` int(11) NOT NULL COMMENT '인력코드',
  `mob_man_cnt` int(5) DEFAULT NULL COMMENT '동원인원수',
  `lchg_dtime` varchar(20) DEFAULT NULL COMMENT '수정일시',
  `lchg_user_id` varchar(20) DEFAULT NULL COMMENT '수정자ID',
  `use_yn` char(1) DEFAULT NULL COMMENT '사용여부',
  PRIMARY KEY (`org_id`,`skill_licn_cd`),
  CONSTRAINT `FK_is_orgman_org_id` FOREIGN KEY (`org_id`) REFERENCES `is_org` (`org_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='유관기관인력';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `is_orgman`
--

LOCK TABLES `is_orgman` WRITE;
/*!40000 ALTER TABLE `is_orgman` DISABLE KEYS */;
/*!40000 ALTER TABLE `is_orgman` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `is_story`
--

DROP TABLE IF EXISTS `is_story`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `is_story` (
  `obj_id` int(20) NOT NULL COMMENT '대상물ID',
  `bldg_seq` int(11) NOT NULL COMMENT '동일련번호',
  `story_no` varchar(12) DEFAULT NULL COMMENT '층번호',
  `story_area` decimal(10,2) DEFAULT NULL COMMENT '층면적(m2)',
  `fcont_cnt` int(5) DEFAULT NULL COMMENT '방화구획수',
  `deco` varchar(100) DEFAULT NULL COMMENT '내장재',
  `resident_man_cnt` int(5) DEFAULT NULL COMMENT '상주인원수',
  `accom_man_cnt` int(5) DEFAULT NULL COMMENT '수용인원수',
  `etc` varchar(1000) DEFAULT NULL COMMENT '기타',
  `use_yn` char(1) DEFAULT '1' COMMENT '사용여부',
  `story_seq` int(11) NOT NULL AUTO_INCREMENT COMMENT '층일련번호',
  PRIMARY KEY (`story_seq`,`obj_id`,`bldg_seq`),
  KEY `FK_is_story` (`bldg_seq`,`obj_id`),
  CONSTRAINT `FK_is_story` FOREIGN KEY (`bldg_seq`, `obj_id`) REFERENCES `is_bldg` (`bldg_seq`, `obj_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='대상물층별내역';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `is_story`
--

LOCK TABLES `is_story` WRITE;
/*!40000 ALTER TABLE `is_story` DISABLE KEYS */;
/*!40000 ALTER TABLE `is_story` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `is_story_spec_used`
--

DROP TABLE IF EXISTS `is_story_spec_used`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `is_story_spec_used` (
  `obj_id` int(11) NOT NULL COMMENT '대상물ID',
  `bldg_seq` int(11) NOT NULL COMMENT '동일련번호',
  `story_seq` int(11) NOT NULL COMMENT '층일련번호',
  `spec_use_seq` int(11) NOT NULL AUTO_INCREMENT COMMENT '특수 용도 일련번호',
  `spec_use_cls_cd` int(11) DEFAULT NULL COMMENT '특수 용도 구분 코드',
  `area` decimal(10,2) DEFAULT NULL COMMENT '면적',
  `remark` varchar(1000) DEFAULT NULL COMMENT '비고',
  `use_yn` char(1) DEFAULT '1' COMMENT '사용 여부',
  PRIMARY KEY (`spec_use_seq`,`obj_id`,`bldg_seq`,`story_seq`),
  KEY `is_story_spec_used_FK` (`story_seq`,`obj_id`,`bldg_seq`),
  KEY `FK_is_story_spec_used` (`bldg_seq`,`obj_id`),
  CONSTRAINT `FK_is_story_spec_used` FOREIGN KEY (`bldg_seq`, `obj_id`) REFERENCES `is_bldg` (`bldg_seq`, `obj_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='층특수용도현황(특수용도)';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `is_story_spec_used`
--

LOCK TABLES `is_story_spec_used` WRITE;
/*!40000 ALTER TABLE `is_story_spec_used` DISABLE KEYS */;
/*!40000 ALTER TABLE `is_story_spec_used` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `is_typeorg`
--

DROP TABLE IF EXISTS `is_typeorg`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `is_typeorg` (
  `org_id` int(11) NOT NULL COMMENT '유관기관ID',
  `emg_type_cd` int(11) NOT NULL COMMENT '긴급구조유형코드',
  `lchg_dtime` varchar(20) DEFAULT NULL COMMENT '수정일시',
  `lchg_user_id` varchar(20) DEFAULT NULL COMMENT '수정자ID',
  `use_yn` char(1) DEFAULT '1' COMMENT '사용여부',
  PRIMARY KEY (`org_id`,`emg_type_cd`),
  CONSTRAINT `FK_is_typeorg_org_id` FOREIGN KEY (`org_id`) REFERENCES `is_org` (`org_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='긴급구조유형별유관기관';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `is_typeorg`
--

LOCK TABLES `is_typeorg` WRITE;
/*!40000 ALTER TABLE `is_typeorg` DISABLE KEYS */;
/*!40000 ALTER TABLE `is_typeorg` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `is_volun`
--

DROP TABLE IF EXISTS `is_volun`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `is_volun` (
  `volun_id` int(11) NOT NULL COMMENT '의용소방대원ID',
  `ward_id` varchar(9) NOT NULL COMMENT '서·센터ID',
  `volunman_name` varchar(100) DEFAULT NULL COMMENT '의용소방대원성명',
  `post_name` varchar(50) DEFAULT NULL COMMENT '소속대명',
  `postpart_name` varchar(50) DEFAULT NULL COMMENT '소속반명',
  `title_cd` int(11) DEFAULT NULL COMMENT '직위코드',
  `zip_code` varchar(10) DEFAULT NULL COMMENT '우편번호',
  `address` varchar(100) DEFAULT NULL COMMENT '주소',
  `enlist_date` date DEFAULT NULL COMMENT '입대일자',
  `birth_date` date DEFAULT NULL COMMENT '생년월일',
  `class_date` date DEFAULT NULL COMMENT '현계급임용일자',
  `contact_tel` varchar(20) DEFAULT NULL COMMENT '연락전화번호',
  `cell` varchar(20) DEFAULT NULL COMMENT '이동전화번호',
  `edu_comp_desc` varchar(200) DEFAULT NULL COMMENT '교육이수내역',
  `hold_licn` varchar(200) DEFAULT NULL COMMENT '보유자격증',
  `remark` varchar(300) DEFAULT NULL COMMENT '비고',
  `lchg_dtime` varchar(20) DEFAULT NULL COMMENT '수정일시',
  `lchg_user_id` varchar(20) DEFAULT NULL COMMENT '수정자ID',
  `use_yn` char(1) DEFAULT '1' COMMENT '사용여부',
  PRIMARY KEY (`volun_id`),
  KEY `FK_is_volun_ward_id` (`ward_id`),
  CONSTRAINT `FK_is_volun_ward_id` FOREIGN KEY (`ward_id`) REFERENCES `is_ward` (`ward_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='의용소방대원';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `is_volun`
--

LOCK TABLES `is_volun` WRITE;
/*!40000 ALTER TABLE `is_volun` DISABLE KEYS */;
/*!40000 ALTER TABLE `is_volun` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `is_volun_dsp`
--

DROP TABLE IF EXISTS `is_volun_dsp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `is_volun_dsp` (
  `volun_id` int(11) NOT NULL COMMENT '의용소방대원ID',
  `dsr_seq` varchar(12) NOT NULL COMMENT '긴급구조번호',
  `dsp_sdate` date DEFAULT NULL COMMENT '출동시간',
  `dsp_edate` date DEFAULT NULL COMMENT '완료시간',
  `remark` varchar(200) DEFAULT NULL COMMENT '비고',
  PRIMARY KEY (`volun_id`,`dsr_seq`),
  CONSTRAINT `FK_is_volun_dsp_volun_id` FOREIGN KEY (`volun_id`) REFERENCES `is_volun` (`volun_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='의용소방대 출동정보';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `is_volun_dsp`
--

LOCK TABLES `is_volun_dsp` WRITE;
/*!40000 ALTER TABLE `is_volun_dsp` DISABLE KEYS */;
/*!40000 ALTER TABLE `is_volun_dsp` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `is_volun_edu`
--

DROP TABLE IF EXISTS `is_volun_edu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `is_volun_edu` (
  `volun_id` int(11) NOT NULL COMMENT '의용소방대원ID',
  `volun_edu_seq` int(11) NOT NULL COMMENT 'Seq',
  `course_name` varchar(100) DEFAULT NULL COMMENT '과정명',
  `institution_name` varchar(100) DEFAULT NULL COMMENT '기관명',
  `start_date` date DEFAULT NULL COMMENT '과정 시작일',
  `end_date` date DEFAULT NULL COMMENT '과정 종료일',
  `complete_cd` int(11) DEFAULT NULL COMMENT '이수코드',
  `score` varchar(10) DEFAULT NULL COMMENT '성적',
  `remark` varchar(200) DEFAULT NULL COMMENT '비고',
  PRIMARY KEY (`volun_id`,`volun_edu_seq`),
  CONSTRAINT `FK_is_volun_edu_volun_id` FOREIGN KEY (`volun_id`) REFERENCES `is_volun` (`volun_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='의용소방대원 교육정보';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `is_volun_edu`
--

LOCK TABLES `is_volun_edu` WRITE;
/*!40000 ALTER TABLE `is_volun_edu` DISABLE KEYS */;
/*!40000 ALTER TABLE `is_volun_edu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `is_ward`
--

DROP TABLE IF EXISTS `is_ward`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `is_ward` (
  `ward_id` varchar(9) NOT NULL COMMENT '서·센터ID',
  `upward_id` varchar(9) DEFAULT NULL COMMENT '상위서·센터ID',
  `fire_office_id` varchar(9) DEFAULT NULL COMMENT '상위소방서ID',
  `ward_name` varchar(50) DEFAULT NULL COMMENT '서소명',
  `type_cls_cd` int(11) DEFAULT NULL COMMENT '유형구분코드',
  `nick_name` varchar(50) DEFAULT NULL COMMENT '약어명',
  `zip_code` varchar(10) DEFAULT NULL COMMENT '우편번호',
  `address` varchar(100) DEFAULT NULL COMMENT '주소',
  `dsp_group_yn` char(1) DEFAULT NULL COMMENT '출동대편성여부',
  `auto_list_yn` char(1) DEFAULT NULL COMMENT '자동편성여부',
  `day_tel` varchar(20) DEFAULT NULL COMMENT '주간전화번호',
  `night_tel` varchar(20) DEFAULT NULL COMMENT '야간전화번호',
  `ext_tel` varchar(10) DEFAULT NULL COMMENT '내선번호',
  `wardopen_date` date DEFAULT NULL COMMENT '개서(소)일자',
  `wardclose_date` date DEFAULT NULL COMMENT '폐서(소)일자',
  `wardopen_yn` char(1) DEFAULT NULL COMMENT '개소여부',
  `brd_using_yn` char(1) DEFAULT NULL COMMENT '방송사용중여부',
  `remark` varchar(300) DEFAULT NULL COMMENT '비고',
  `lchg_dtime` varchar(20) DEFAULT NULL COMMENT '수정일시',
  `lchg_user_id` varchar(20) DEFAULT NULL COMMENT '수정자ID',
  `ward_order` int(4) DEFAULT 9999 COMMENT '서센터정렬순서',
  `x` varchar(20) DEFAULT NULL COMMENT 'x',
  `y` varchar(20) DEFAULT NULL COMMENT 'y',
  `day_fax` varchar(20) DEFAULT NULL COMMENT '주간팩스번호',
  `night_fax` varchar(20) DEFAULT NULL COMMENT '야간팩스번호',
  `use_yn` char(1) DEFAULT '1' COMMENT '사용여부',
  PRIMARY KEY (`ward_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='서/센터';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `is_ward`
--

LOCK TABLES `is_ward` WRITE;
/*!40000 ALTER TABLE `is_ward` DISABLE KEYS */;
INSERT INTO `is_ward` VALUES ('131000000',NULL,'131000000','Dhaka-A HQ',2,'DHQ333','123333','Etc address','1','1','02-1111-1111','02-2222-2222','1233','2021-04-01','2021-04-16','1','1','Remark','05/21/2021 16:53:06','test',1,NULL,NULL,'02-4444-4444','02-5555-5555','0'),('131000001','131000000','131000000','DK Department 123',9,'DK-D1123333','123333','Etc address','1','1','02-1111-1111','02-2222-2222','1233','2021-04-01','2021-04-16','1','1','Remark','05/21/2021 17:23:32','test',20,NULL,NULL,'02-4444-4444','02-5555-5555','0'),('131001000','131000000','131001000','Dhaka-A Fire office3',3,'DK-F22222','123333','Etc address','1','1','02-1111-1111','02-2222-2222','1233','2021-04-01','2021-04-16','1','1','Remark','05/26/2021 15:56:29','test',7,NULL,NULL,'02-4444-4444','02-5555-5555','1'),('131001001','131001000','131001000','Department 3',10,'DK-D1234','123333','Etc address','1','1','02-1111-1111','02-2222-2222','1233','2021-04-01','2021-04-16','1','1','Remark','05/26/2021 15:56:53','test',22,NULL,NULL,'02-4444-4444','02-5555-5555','1'),('131001002','131001000','131001000','Department 2',10,'DK-D','123333','Etc address','1','1','02-1111-1111','02-2222-2222','1233','2021-04-01','2021-04-16','1','1','Remark','04/27/2021 16:18:47','test',23,NULL,NULL,'02-4444-4444','02-5555-5555','1'),('131001003','131001000','131001000','Department 9',10,'DK-D','123333','Etc address','1','1','02-1111-1111','02-2222-2222','1233','2021-04-01','2021-04-16','1','1','Remark','04/27/2021 17:43:12','test',24,NULL,NULL,'02-4444-4444','02-5555-5555','1'),('131002000','131000000','131002000','Dhaka-A Fire office2',3,'DK-F22','123333','Etc address','1','1','02-1111-1111','02-2222-2222','1233','2021-04-01','2021-04-16','1','1','Remark','05/21/2021 16:48:56','test',3,NULL,NULL,'02-4444-4444','02-5555-5555','1'),('131002001','131002000','131002000','HR',10,'ABB','1233','Etc address','0','1','02-1111-1111','02-2222-2222','0233',NULL,NULL,'1','1',NULL,'05/26/2021 15:57:37','test',NULL,'31\"22','66\"88','02-4444-4444','02-5555-5555','0'),('131003000','131000000','131003000','Dhaka Fire Office A',4,'ABB','123333','Etc address','0','0','02-1111-1111','02-2222-2222','1213',NULL,NULL,'1','1',NULL,'05/26/2021 15:50:05','test',NULL,NULL,NULL,'02-4444-4444','02-5555-5555','0'),('131004000','131000000','131004000','HQ Department',5,'ABB','1233','Etc address','0','0','02-1111-1111','02-2222-2222','2312',NULL,NULL,'1','0',NULL,'05/26/2021 15:58:43','test',2,NULL,NULL,'02-4444-4444','02-5555-5555','0'),('132000000',NULL,'132000000','Dhaka-B HQ',2,'DHQ333','123333','Etc address','1','1','02-1111-1111','02-2222-2222','1233','2021-04-01','2021-04-16','1','1','Remark','05/21/2021 16:59:41','test',2,NULL,NULL,'02-4444-4444','02-5555-5555','1'),('132003000','132000000','132003000','FireOffice 8',5,'ABB',NULL,NULL,'0','0','02-1111-1111','02-2222-2222',NULL,NULL,NULL,'1','0',NULL,'04/27/2021 17:44:59','test',5,NULL,NULL,NULL,NULL,'1'),('132004000','132000000','132004000','Dhaka B office',6,'ABB','1233','Etc address','0','0','02-1111-1111','02-2222-2222','0233',NULL,NULL,'1','0',NULL,'05/26/2021 16:03:54','test',NULL,NULL,NULL,'02-4444-4444','02-5555-5555','1');
/*!40000 ALTER TABLE `is_ward` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `is_wardcar`
--

DROP TABLE IF EXISTS `is_wardcar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `is_wardcar` (
  `car_id` int(11) NOT NULL COMMENT '차량호수',
  `ward_id` varchar(9) NOT NULL COMMENT '서·센터ID',
  `car_no` varchar(15) DEFAULT NULL COMMENT '차량번호',
  `car_cd` int(11) DEFAULT NULL COMMENT '차종코드',
  `carstat_cd` int(11) DEFAULT NULL COMMENT '차량상태코드',
  `dsp_auto_yn` char(1) DEFAULT '1' COMMENT '출동대편성여부',
  `car_name` varchar(50) DEFAULT NULL COMMENT '차량명',
  `car_grp_cd` int(11) DEFAULT NULL COMMENT '차량그룹코드',
  `manufacturer` varchar(50) DEFAULT NULL COMMENT '제작사',
  `car_kind` varchar(50) DEFAULT NULL COMMENT '기종',
  `ride_man_cnt` int(5) DEFAULT NULL COMMENT '탑승인원',
  `buy_date` date DEFAULT NULL COMMENT '구입일자',
  `ldsp_dtime` varchar(20) DEFAULT NULL COMMENT '최종출동시각',
  `lret_dtime` varchar(20) DEFAULT NULL COMMENT '최종귀소시각',
  `lchg_dtime` varchar(20) DEFAULT NULL COMMENT '수정일시',
  `lchg_user_id` varchar(20) DEFAULT NULL COMMENT '수정자ID',
  `use_yn` char(1) DEFAULT '1' COMMENT '사용여부',
  `etc_info` varchar(100) DEFAULT NULL COMMENT '기타정보',
  `car_stat_memo` varchar(200) DEFAULT NULL COMMENT '차량상태메모',
  `dsp_order` int(2) DEFAULT NULL COMMENT '출동우선순위',
  `radio_callsign` varchar(20) DEFAULT NULL COMMENT '무선호출명칭',
  `car_mobile` varchar(20) DEFAULT NULL COMMENT '차량휴대전화번호',
  `dsp_dsr_seq` varchar(12) DEFAULT NULL COMMENT '최종출동긴급구조번호',
  `platoon_cls` char(1) DEFAULT NULL COMMENT '소대구분',
  PRIMARY KEY (`car_id`,`ward_id`),
  KEY `FK_is_wardcar_ward_id` (`ward_id`),
  CONSTRAINT `FK_is_wardcar_ward_id` FOREIGN KEY (`ward_id`) REFERENCES `is_ward` (`ward_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='서·센터차량';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `is_wardcar`
--

LOCK TABLES `is_wardcar` WRITE;
/*!40000 ALTER TABLE `is_wardcar` DISABLE KEYS */;
/*!40000 ALTER TABLE `is_wardcar` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `is_wardcarhist`
--

DROP TABLE IF EXISTS `is_wardcarhist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `is_wardcarhist` (
  `car_id` int(11) NOT NULL COMMENT '차량호수',
  `ward_id` varchar(9) DEFAULT NULL COMMENT '서·센터ID',
  `car_no` varchar(15) DEFAULT NULL COMMENT '차량번호',
  `car_cd` int(11) DEFAULT NULL COMMENT '차종코드',
  `carstat_cd` int(11) DEFAULT NULL COMMENT '차량상태코드',
  `dsp_auto_yn` char(1) DEFAULT '1' COMMENT '출동대편성여부',
  `car_name` varchar(50) DEFAULT NULL COMMENT '차량명',
  `car_grp_cd` int(11) DEFAULT NULL COMMENT '차량그룹코드',
  `manufacturer` varchar(50) DEFAULT NULL COMMENT '제작사',
  `car_kind` varchar(50) DEFAULT NULL COMMENT '기종',
  `ride_man_cnt` int(5) DEFAULT NULL COMMENT '탑승인원',
  `buy_date` date DEFAULT NULL COMMENT '구입일자',
  `ldsp_dtime` varchar(20) DEFAULT NULL COMMENT '최종출동시각',
  `lret_dtime` varchar(20) DEFAULT NULL COMMENT '최종귀소시각',
  `lchg_dtime` varchar(20) NOT NULL COMMENT '수정일시',
  `lchg_user_id` varchar(20) DEFAULT NULL COMMENT '수정자ID',
  `use_yn` char(1) DEFAULT '1' COMMENT '사용여부',
  `etc_info` varchar(100) DEFAULT NULL COMMENT '기타정보',
  `car_stat_memo` varchar(200) DEFAULT NULL COMMENT '차량상태메모',
  `dsp_order` int(2) DEFAULT NULL COMMENT '출동우선순위',
  `radio_callsign` varchar(20) DEFAULT NULL COMMENT '무선호출명칭',
  `car_mobile` varchar(20) DEFAULT NULL COMMENT '차량휴대전화번호',
  `dsp_dsr_seq` varchar(12) DEFAULT NULL COMMENT '최종출동긴급구조번호',
  `platoon_cls` char(1) DEFAULT NULL COMMENT '소대구분',
  PRIMARY KEY (`car_id`,`lchg_dtime`),
  KEY `FK_is_wardcarhist_ward_id` (`ward_id`),
  CONSTRAINT `FK_is_wardcarhist_ward_id` FOREIGN KEY (`ward_id`) REFERENCES `is_ward` (`ward_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='서·센터차량 History';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `is_wardcarhist`
--

LOCK TABLES `is_wardcarhist` WRITE;
/*!40000 ALTER TABLE `is_wardcarhist` DISABLE KEYS */;
/*!40000 ALTER TABLE `is_wardcarhist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sm_cd_master`
--

DROP TABLE IF EXISTS `sm_cd_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sm_cd_master` (
  `cd` int(11) NOT NULL COMMENT '코드',
  `cd_desc` varchar(300) DEFAULT NULL COMMENT '코드설명',
  `use_yn` char(1) DEFAULT '1' COMMENT '사용여부',
  PRIMARY KEY (`cd`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='코드 Master';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sm_cd_master`
--

LOCK TABLES `sm_cd_master` WRITE;
/*!40000 ALTER TABLE `sm_cd_master` DISABLE KEYS */;
INSERT INTO `sm_cd_master` VALUES (1,'FSCD','1'),(2,'Divisional HQ','1'),(3,'Fire office(A)','1'),(4,'Fire office(B)','1'),(5,'Fire office(C)','1'),(6,'Fire office(R)','1'),(7,'Etc','1'),(8,'Department of FSCD','1'),(9,'Department of HQ','1'),(10,'Department of fire office','1'),(11,'Read / write','1'),(12,'Read only','1'),(13,'Completed','1'),(14,'Not completed','1'),(15,'Request User Account ','1'),(16,'Request User Auth','1'),(17,'Rejected','1'),(18,'Contract worker','1'),(19,'Part timer','1'),(20,'Fire worker','1'),(21,'Office worker','1'),(22,'System manager','1'),(23,'Financial','1'),(24,'Driver','1'),(25,'Staff','1'),(26,'General Manager','1'),(27,'Duputy General Manager','1'),(28,'Manager','1'),(29,'Assistant Manager','1'),(30,'Supervisor','1'),(31,'Executives','1'),(32,'Probationary Firefighter','1'),(33,'Paramedic','1'),(34,'Firefighter','1'),(35,'Lieutenant','1'),(36,'EMS Captain','1'),(37,'Battalion Chief','1'),(38,'Deputy Chief','1'),(39,'Division Chief','1'),(40,'Deputy Assistant Chief','1'),(41,'Assistant Chief','1'),(42,'Chief of Fire Operations','1'),(43,'Chief of Department','1'),(44,'Elementary school','1'),(45,'Middle school','1'),(46,'High school','1'),(47,'University','1'),(48,'Postgraduate school','1'),(49,'Associate','1'),(50,'Bachelor','1'),(51,'Master','1'),(52,'Doctor','1'),(53,'Medical practitioner','1'),(54,'Aid certificate','1'),(55,'Fire fighting education','1'),(56,'Etc','1'),(57,'Finished','1'),(58,'Not finished','1'),(59,'Police','1'),(60,'Electric company','1'),(61,'Gas','1'),(62,'Private rescue Org','1'),(63,'Mountain rescue','1'),(64,'Subway','1'),(65,'Hospital','1'),(66,'Central authority','1'),(67,'City/District/Upazila','1'),(68,'Army','1'),(69,'Environment','1'),(70,'Health institution','1'),(71,'Information center','1'),(72,'Local government','1'),(73,'Transfer center','1'),(74,'Etc(Embrance)','1'),(75,'public health','1'),(76,'Hospital','1'),(77,'Pharmacy','1'),(78,'Freight car','1'),(79,'Other equipment','1'),(80,'Water tank car','1'),(81,'Chemical vehicle','1'),(82,'Ladder vehicle (High)','1'),(83,'Refraction car','1'),(84,'Ambulance','1'),(85,'Patrol car','1'),(86,'Diagnosis car','1'),(87,'Bulldozer','1'),(88,'Peyrona','1'),(89,'Rack car','1'),(90,'Crane','1'),(91,'Dump truck','1'),(92,'Excavators','1'),(93,'Fork lift','1'),(94,'Water supply wagon','1'),(95,'Bus','1'),(96,'Helicopter','1'),(97,'Etc car','1'),(98,'Ladder car','1'),(99,'Fire vehicle','1'),(100,'Pump car(Middle)','1'),(101,'Pumb car(Small)','1'),(102,'Construction machine engineer','1'),(103,'Heavy Equipment Engineer','1'),(104,'Mold engineer','1'),(105,'Thermal Management Engineer','1'),(106,'Industrial safety engineer','1'),(107,'Firefighting equipment engineer','1'),(108,'Gas engineer','1'),(109,'Danger goods manager','1'),(110,'Electrician','1'),(111,'Radio communication engineer','1'),(112,'Firefighting Facility Manager','1'),(113,'1st mobilization','1'),(114,'2nd mobilization','1'),(115,'3rd mobilization','1'),(116,'Regular worker','1'),(117,'College','1'),(118,'Vocational training center','1'),(119,'Approved','1'),(120,'1st fire safety management','1'),(121,'2nd fire safety management','1'),(122,'Fire safety management for public organizations',NULL),(123,'Circus ground','1'),(124,'Others (performance facility)','1'),(125,'Theater','1'),(126,'Bandstand','1'),(127,'Performance hall (less than 300 square meters)','1'),(128,'Movie theater','1'),(129,'Entertainment hall','1'),(130,'Video small theater business (less than 300 square meters)','1'),(131,'Video surveillance loss (less than 300 square meters)','1'),(132,'Concert hall','1'),(133,'Others (Factory)','1'),(134,'Processing plant','1'),(135,'Repair shop','1'),(136,'Manufacturing plant','1'),(137,'Manufacturing establishment (less than 500 square meters)','1'),(138,'Loading dock','1'),(139,'Warehouse','1'),(140,'Others (warehouse facilities)','1'),(141,'Outdoor music hall','1'),(142,'Park, amusement park','1'),(143,'Military recreational facilities','1'),(144,'Park architecture','1'),(145,'Others (tourist rest facilities)','1'),(146,'Observation tower','1'),(147,'Outdoor theater','1'),(148,'Rest area','1'),(149,'Education Institution','1'),(150,'Special school','1'),(151,'Others (educational research facilities)','1'),(152,'University','1'),(153,'Academy (less than 500 square meters)','1'),(154,'High school','1'),(155,'Middle School','1'),(156,'Elementary School','1'),(157,'Library','1'),(158,'Laboratory','1'),(159,'Academy','1'),(160,'Vocational training center','1'),(161,'Others (Elderly People\'s Facility)','1'),(162,'Elderly Welfare Facility','1'),(163,'Rehabilitation facilities for the disabled','1'),(164,'Braille Library','1'),(165,'Infant Child Care Facility','1'),(166,'Kindergarden','1'),(167,'Nursing facility','1'),(168,'Child welfare facilities','1'),(169,'Animal hospital (less than 500 square meters)','1'),(170,'Others (facilities related to animals and plants)','1'),(171,'Plant related facilities','1'),(172,'Slaughterhouse','1'),(173,'Mushroom cultivation','1'),(174,'Seedling culture facility','1'),(175,'Greenhouse','1'),(176,'Barn','1'),(177,'Livestock exercise facility','1'),(178,'Artificial Insemination Center','1'),(179,'Warehouse for livestock','1'),(180,'Livestock market','1'),(181,'Animal Quarantine Station','1'),(182,'Experimental Animal Breeding Facility','1'),(183,'Animal and botanical garden','1'),(184,'Zoo','1'),(185,'Botanical garden','1'),(186,'Aquarium','1'),(187,'Power plant','1'),(188,'Substation','1'),(189,'Others (power generation facilities)','1'),(190,'Scenic spot','1'),(191,'Historic Site','1'),(192,'Traditional architecture','1'),(193,'Others (Cultural Property)','1'),(194,'Treasure (National Designated Cultural Property)','1'),(195,'National Treasure (National Designated Cultural Property)','1'),(196,'Designated Cultural Property','1'),(197,'Historic Site (National Designated Cultural Property)','1'),(198,'General hotel','1'),(199,'Motel','1'),(200,'Water Tourist Hotel','1'),(201,'Inn','1'),(202,'Others (accommodation facilities)','1'),(203,'Tourist Hotel','1'),(204,'Family Hotel','1'),(205,'Resort condominium','1'),(206,'National Insurance Corporation','1'),(207,'Other correction facilities','1'),(208,'Pumping station','1'),(209,'Shelter','1'),(210,'Public business facilities','1'),(211,'General business facilities','1'),(212,'Officetels','1'),(213,'Newspaper','1'),(214,'Prison','1'),(215,'Jail','1'),(216,'Reformatory','1'),(217,'Juvenile Classification Examiner','1'),(218,'Water purification plant','1'),(219,'Public toilet','1'),(220,'Village public hall','1'),(221,'Village joint work place','1'),(222,'Village joint market','1'),(223,'Police office','1'),(224,'Fire station','1'),(225,'Post office','1'),(226,'Public Health','1'),(227,'Public library','1'),(228,'Financial business (less than 500 square meters)','1'),(229,'Office (less than 500 square meters)','1'),(230,'Real estate brokerage (less than 500 square meters)','1'),(231,'Publisher (less than 500 square meters)','1'),(232,'Bookstore (less than 500 square meters)','1'),(233,'Marriage Consultation Center (less than 500 square meters)','1'),(234,'Town office (less than 1,000 square meters)','1'),(235,'Police station (less than 1,000 square meters)','1'),(236,'Fire department (less than 1,000 square meters)','1'),(237,'Post office (less than 1,000 square meters)','1'),(238,'Public health center (less than 1,000 square meters)','1'),(239,'Telegraphic and telephone offices (less than 1,000 square meters)','1'),(240,'Broadcasting station (less than 1,000 square meters)','1'),(241,'Public library (less than 1,000 square meters)','1'),(242,'National Insurance Corporation (less than 1,000 square meters)','1'),(243,'Local gorvement office','1'),(244,'Others (business facilities)','1'),(245,'Passenger Car Terminal','1'),(246,'Cargo terminal','1'),(247,'Railway history','1'),(248,'Airport facilities','1'),(249,'Port facilities','1'),(250,'Comprehensive Passenger Facility','1'),(251,'Others (history, terminal)','1'),(252,'Automobile maintenance plant','1'),(253,'Junkyard','1'),(254,'Car wash','1'),(255,'Parking lot','1'),(256,'Parking building','1'),(257,'Garage and parking lot','1'),(258,'Car driving school','1'),(259,'Car dealer','1'),(260,'Car shop','1'),(261,'Automobile inspection station','1'),(262,'Aircraft hangar','1'),(263,'Others (Transportation Vehicle-related Facilities)','1'),(264,'Dance club','1'),(265,'Bar','1'),(266,'Casino','1'),(267,'Amusement facilities','1'),(268,'Martial Arts Academy','1'),(269,'Others (entertainment facilities)','1'),(270,'Tujeon Enterprise','1'),(271,'Crematorium','1'),(272,'Others (related facilities such as sanitation)','1'),(273,'Funeral director (less than 500 square meters)','1'),(274,'Ossuary','1'),(275,'Waste treatment facility','1'),(276,'Waste recycling facility','1'),(277,'Cemetery','1'),(278,'Antique shop','1'),(279,'Oriental Medicine Hospital','1'),(280,'Funeral','1'),(281,'Infectious Hospital','1'),(282,'Drug clinic','1'),(283,'General Hospital','1'),(284,'Others (medical facilities)','1'),(285,'Osteopath','1'),(286,'Acupuncture','1'),(287,'Massage parlor','1'),(288,'Oriental medicine','1'),(289,'Dental clinic','1'),(290,'Doctor\'s office','1'),(291,'Midwife','1'),(292,'Sanitarium','1'),(293,'Mental health facility','1'),(294,'Dental hospital','1'),(295,'Postpartum care center','1'),(296,'General hospital','1'),(297,'House','1'),(298,'Wedding hall','1'),(299,'Conference hall','1'),(300,'Public hall','1'),(301,'Bookstore outside the market','1'),(302,'Viewing hall','1'),(303,'Racetrack','1'),(304,'Car racetrack','1'),(305,'Playground','1'),(306,'Showroom','1'),(307,'Museum','1'),(308,'Art gallery','1'),(309,'Science Museum','1'),(310,'Memorial','1'),(311,'Industrial exhibition','1'),(312,'Fairground','1'),(313,'Table tennis court','1'),(314,'Golf Driving Range','1'),(315,'Origin (less than 150 square meters)','1'),(316,'Tennis court (less than 500 square meters)','1'),(317,'Table tennis court (less than 500 square meters)','1'),(318,'Bowling alley (less than 500 square meters)','1'),(319,'Health Club Room (less than 500 square meters)','1'),(320,'Physical education painting (less than 500 square meters)','1'),(321,'Tennis court (less than 500 square meters)','1'),(322,'Physical fitness center (less than 500 square meters)','1'),(323,'Aerobics field (less than 500 square meters)','1'),(324,'Billiard room (less than 500 square meters)','1'),(325,'Indoor fishing ground (less than 500 square meters)','1'),(326,'Golf driving range (less than 500 square meters)','1'),(327,'Photo studio (less than 500 square meters)','1'),(328,'Game providing business (less than 500 square meters)','1'),(329,'Multimedia cultural contents facility provision business (less than 500 square meters)','1'),(330,'Offer business (less than 500 square meters)','1'),(331,'Betting phone voting booth','1'),(332,'Others (observation facilities, parks, sports facilities)','1'),(333,'Sauna','1'),(334,'Athletic field','1'),(335,'Ball stadium','1'),(336,'Swimming pool','1'),(337,'Skating rink','1'),(338,'Roller\'s Kate Ground','1'),(339,'Riding ground','1'),(340,'Range ground','1'),(341,'Archery ground','1'),(342,'Golf course','1'),(343,'Bowling club','1'),(344,'Gym','1'),(345,'General bath','1'),(346,'Physical education stamp','1'),(347,'Tennis court','1'),(348,'Fitness center','1'),(349,'Aerobics field','1'),(350,'Billiard room','1'),(351,'Indoor fishing ground','1'),(352,'Monastery','1'),(353,'Oratory','1'),(354,'Others (religious facilities)','1'),(355,'Religious assembly hall (less than 300 square meters)','1'),(356,'Convent','1'),(357,'Temple','1'),(358,'Production','1'),(359,'Religious Assembly Hall','1'),(360,'Church','1'),(361,'Cathedral','1'),(362,'Shrine','1'),(363,'Apartment','1'),(364,'Other (House)','1'),(365,'Dormitory','1'),(366,'For heating and cooling piping','1'),(367,'Communication','1'),(368,'For electric power','1'),(369,'Others (underground)','1'),(370,'Tunnel','1'),(371,'Underground shopping mall','1'),(372,'Youth Center','1'),(373,'Others (youth training facilities)','1'),(374,'Youth campsite','1'),(375,'Youth Culture House','1'),(376,'Youth Hostel','1'),(377,'Youth Training Center','1'),(378,'Telegraph office','1'),(379,'Broadcast stations','1'),(380,'Others (communication photography facilities)','1'),(381,'Communication facility','1'),(382,'Studio','1'),(383,'Wholesale market','1'),(384,'Retail market','1'),(385,'Shop','1'),(386,'Shopping center','1'),(387,'Department store','1'),(388,'Discount store','1'),(389,'Specialty store','1'),(390,'Supermarket (less than 1,000 square meters)','1'),(391,'Daily necessities retail store (less than 1,000 square meters)','1'),(392,'Bakery (less than 150 square meters)','1'),(393,'General restaurant (less than 150 square meters)','1'),(394,'Repair shop (less than 500 square meters)','1'),(395,'Laundry (less than 500 square meters)','1'),(396,'Standard point (less than 500 square meters)','1'),(397,'Gun shop (less than 500 square meters)','1'),(398,'Combined distribution (less than 500 square meters)','1'),(399,'Laundry (excluding those with a factory)','1'),(400,'Hair shop','1'),(401,'Pharmaceutical wholesale store (less than 1,000 square meters)','1'),(402,'Others (sales facilities)','1'),(403,'Automobile office (less than 1,000 square meters)','1'),(404,'Dangerous goods manufacturing plant, etc.','1'),(405,'Gas storage facility','1'),(406,'Others (dangerous material storage and treatment facilities)','1'),(407,'Gas handling facility','1'),(408,'Gas manufacturing facility','1'),(409,'Dangerous Goods Manufacturing','1'),(410,'Dangerous goods storage','1'),(411,'Dangerous goods emergency station','1'),(412,'Model house','1'),(413,'Others (complex buildings)','1'),(414,'Complex building','1'),(415,'Video painting industry','1'),(416,'Video water buffalo theater business','1'),(417,'Pharmaceutical wholesale business','1'),(418,'Clinic (including dentistry/oriental clinic)','1'),(419,'Daily necessities','1'),(420,'Car sales office','1'),(421,'Book publisher','1'),(422,'Gym','1'),(423,'Rest restaurant','1'),(424,'Others (Neighborhood Life)','1'),(425,'Water play-type facility','1'),(426,'Medical device sales office','1'),(427,'Super market','1'),(428,'Bakery','1'),(429,'Marriage Consultation Center','1'),(430,'Factory','1'),(431,'Financial business','1'),(432,'Etc(Neighborhood life facility)','1'),(433,'Animal hospital','1'),(434,'Complex distribution/providing business','1'),(435,'Real estate brokerage','1'),(436,'Office','1'),(437,'Photo studio','1'),(438,'Bookstore','1'),(439,'Laundry','1'),(440,'Retail','1'),(441,'Restaurant','1'),(442,'Gun shop','1'),(443,'Paper hanger store','1'),(444,'Entertainment bar','1'),(445,'Mall','1'),(446,'Market','1'),(447,'National government building','1'),(448,'Military hospital','1'),(449,'Embassy','1'),(450,'School / Training center','1'),(451,'Foreigner protection facility','1'),(452,'Treatment and supervision facility','1'),(453,'Highway National Highway Tunnel','1'),(454,'National Highway Tunnel','1'),(455,'Others (underground tunnel)','1'),(456,'Local road tunnel','1'),(457,'Underground shopping mall','1'),(458,'Railway tunnel','1'),(459,'Public  underground artificial structure','1'),(460,'Etc(Underground artificial structure)','1'),(461,'Single underground artificial structure','1'),(462,'Underground artificial structure for heating','1'),(463,'Underground artificial structure for power','1'),(464,'Underground artificial structure for telecom','1'),(465,'Thermal power plant','1'),(466,'Wind power plant','1'),(467,'Hydroelectric power plant','1'),(468,'Nuclear power plant','1'),(469,'Collective energy supply facility','1'),(470,'Solar power plant','1'),(471,'Others (power generation facilities)\n','1'),(472,'Cemetery','1'),(473,'Crematorium','1'),(474,'Others (cemetery related facilities)','1'),(475,'National administrative agency','1'),(476,'National and public high schools','1'),(477,'National and Public Public Schools','1'),(478,'National and public universities','1'),(479,'National and public kindergartens','1'),(480,'National and public middle schools','1'),(481,'National and public elementary schools','1'),(482,'Private high school','1'),(483,'Private civic school','1'),(484,'Private university','1'),(485,'Private kindergarten','1'),(486,'Private junior high school','1'),(487,'Private elementary school','1'),(488,'Government Investment Institution','1'),(489,'Local corporations and local industrial complexes','1'),(490,'Local government','1'),(491,'Special administrative agency','1'),(492,'Drying room','1'),(493,'Windowless floor','1'),(494,'Disaster Prevention Center','1'),(495,'Distribution/Substation Room','1'),(496,'Boiler room','1'),(497,'Kitchen/dining room','1'),(498,'Garage/parking lot','1'),(499,'Communication equipment room','1'),(500,'Others (for special tools)','1'),(501,'Gas handling','1'),(502,'danger goods','1'),(503,'Special combustibles','1'),(504,'Other (dangerous goods classification)','1'),(505,'Level 1','1'),(506,'Level 2','1'),(507,'General object','1'),(508,'Other objects (temporary buildings)','1'),(509,'Other objects (residential vinyl house)','1'),(510,'Simple fire extinguishing equipment','1'),(511,'Simple Spring Cooler','1'),(512,'Power fire pump','1'),(513,'Water spray fire extinguishing equipment AV','1'),(514,'Water spray fire extinguishing system H','1'),(515,'Powder fire extinguishing AV','1'),(516,'Powder fire extinguishing H','1'),(517,'Manual fire extinguisher','1'),(518,'Sprinkler AV','1'),(519,'Sprinkler H','1'),(520,'Indoor fire hydrant','1'),(521,'Outdoor fire hydrant','1'),(522,'Carbon dioxide AV','1'),(523,'Carbon dioxide H','1'),(524,'Automatic fire extinguisher','1'),(525,'Fossilized AV','1'),(526,'Fosification H','1'),(527,'Halogen compound AV','1'),(528,'Halogen compound H','1'),(529,'Gas leak alarm','1'),(530,'Earth leakage alarm','1'),(531,'Emergency alarm','1'),(532,'Emergency broadcast','1'),(533,'Automated Breaking News','1'),(534,'Automated Fire Detector (Sense)','1'),(535,'Automated Fire Detector (times)','1'),(536,'Liver Lean Strength','1'),(537,'Audience guidance, etc.','1'),(538,'Air safety mat','1'),(539,'Air respirator','1'),(540,'Rescue team','1'),(541,'Others (evacuation facilities)','1'),(542,'slide','1'),(543,'Heat shield','1'),(544,'Emergency lighting','1'),(545,'Stubbornness','1'),(546,'Guidance light','1'),(547,'Guidance signs','1'),(548,'Artificial resuscitation','1'),(549,'Aisle guide light','1'),(550,'Refuge','1'),(551,'Evacuation exit guidance, etc.','1'),(552,'Evacuation rope','1'),(553,'Evacuation ladder','1'),(554,'Evacuation trap','1'),(555,'Portable emergency lighting','1'),(556,'Water for fire extinguishing','1'),(557,'Digestion tank','1'),(558,'Others (fire water)','1'),(559,'Wireless communication assistant','1'),(560,'Emergency outlet','1'),(561,'Spray water connection ','1'),(562,'Connecting water pipe','1'),(563,'Combustion prevention facility','1'),(564,'Ventilation equipment','1'),(565,'carpet','1'),(566,'curtain','1'),(567,'Other (flame retardant)','1'),(568,'More than 30 floors (including basement)','1'),(569,'More than 120m in height','1'),(570,'Total floor area of ??200,000㎡ or more','1'),(571,'11th floor and above','1'),(572,'More than 1,000 tons of combustible gas','1'),(573,'Total floor area of ??15,000 or more','1'),(574,'Less than 1,000 tons of combustible gas','1'),(575,'Mobilization agency','1'),(576,'Honorary firefighter/medical fire brigade','1'),(577,'Civil defense','1'),(578,'firefighter','1'),(579,'Fire safety manager/self-defense fire brigade','1'),(580,'Installation target for sprinkler, water spray, etc.','1'),(581,'Indoor fire hydrant installation target','1'),(582,'Automatic re-detection and installation target','1'),(583,'citizen','1'),(584,'Underground tunel','1'),(585,'General object','1'),(586,'Merrymaking liquor store','1'),(587,'Residential Green House','1'),(588,'Side room','1'),(589,'Container House','1'),(590,'Scrapped car','1'),(591,'A waste plane','1'),(592,'Abandoned ship','1'),(593,'Others (classification of residential vinyl houses)','1'),(594,'Temporary Exhibition Hall','1'),(595,'Temporary store','1'),(596,'A temporary showroom','1'),(597,'Sample house (model house)','1'),(598,'Agricultural and industrial complex','1'),(599,'Agricultural fixed greenhouse','1'),(600,'Temporary Office, Warehouse, Accommodation','1'),(601,'Others (temporary building classification)','1'),(602,'Pensions with more than 7 rooms','1'),(603,'Pension with 7 rooms or less','1'),(604,'Guesthouse with 7 rooms or more','1'),(605,'Guesthouses with Less than 7 rooms','1'),(606,'Gas leak (explosion)','1'),(607,'Traffic Accident','1'),(608,'Mechanical factor','1'),(609,'Others (fire occurrence and ignition factor)','1'),(610,'carelessness','1'),(611,'Natural factors','1'),(612,'Electrical factor','1'),(613,'Chemical factor','1'),(614,'Unknown','1'),(615,'Cultural Heritage','1'),(616,'Express grade target','1'),(618,'Level 1 target','1'),(620,'Level 2 target','1'),(622,'General target','1'),(624,'Others-Residential Vinyl House','1'),(626,'Other-temporary building','1'),(628,'Other-Pension','1'),(630,'Others-Guest House','1'),(632,'Cultural Heritage','1'),(634,'Public institution','1'),(635,'Neighborhood life','1'),(636,'Recreational facility','1'),(637,'Cultural assembly and sports facility','1'),(638,'Sales and sales facility','1'),(639,'Accommodation','1'),(640,'Elderly people facility','1'),(641,'Medical facility','1'),(642,'Apartment house (apartment/dormitory)','1'),(643,'Business facility','1'),(644,'Communication filming facilty','1'),(645,'Educational Research Facility','1'),(646,'Factory','1'),(647,'Warehouse facility','1'),(648,'Transportation vehicle related facility','1'),(649,'Tourist rest facility','1'),(650,'Animal and plant related facility','1'),(651,'Sanitation related facilities','1'),(652,'Correctional facility','1'),(653,'Dangerous goods storage and treatment facility','1'),(654,'Underground tunnel','1'),(655,'Underground artificial structure','1'),(656,'Cultural Heritage','1'),(657,'Complex building','1'),(658,'Religious facility','1'),(659,'Training facility','1'),(660,'Exercise facility','1'),(661,'Aircraft and automobile related facility','1'),(662,'Power generation facility','1'),(663,'Cemetery related facility','1'),(664,'Funeral','1'),(665,'Framed Structure','1'),(666,'Masonry Structure','1'),(667,'Monolithic Structure','1'),(668,'Special Structure','1'),(669,'Simple woodwork','1'),(670,'Simple steel frame pipe','1'),(671,'woodcarving','1'),(672,'Brickwork','1'),(673,'Block','1'),(674,'Stonework','1'),(675,'Cement brick','1'),(676,'Cement block','1'),(677,'Steel frame','1'),(678,'Steel frame reinforced concrete construction','1'),(679,'Steel concrete structure','1'),(680,'Reinforced concrete','1'),(681,'Barbed wire','1'),(682,'Stucco brickwork','1'),(683,'Sandwich panel','1'),(684,'Threthga','1'),(685,'Slavs','1'),(686,'Cement tile','1'),(687,'Color coated iron plate','1'),(688,'Renovation','1'),(689,'Major repair','1'),(690,'New construction','1'),(691,'Change of use','1'),(692,'Rebuild','1'),(693,'Extension','1'),(694,'Others (Architecture History Code)','1'),(695,'Manager','1'),(696,'User','1'),(697,'Fire safety manager','1'),(698,'Owner','1'),(699,'Dangerous Goods Safety Manager','1'),(700,'Occupant','1'),(701,'Building photo','1'),(702,'Location map','1'),(703,'view map','1'),(704,'Fire suppression degree','1'),(705,'Probationary Firefighter','1'),(706,'Paramedic','1'),(707,'Firefighter','1'),(708,'Lieutenant','1'),(709,'EMS Captain','1'),(710,'Battalion Chief','1'),(711,'Deputy Chief','1'),(712,'Division Chief','1'),(713,'Deputy Assistant Chief','1'),(714,'Assistant Chief','1'),(715,'Chief of Fire Operations','1'),(716,'Chief of Department','1'),(717,'Chief of fire office','1'),(718,'Nurse','1'),(719,'Nursing assistant','1'),(720,'Consensus','1'),(721,'Inspection','1'),(722,'Inspector','1'),(723,'deck','1'),(724,'Construct','1'),(725,'Construction electricity','1'),(726,'Inspection map','1'),(727,'Accounting Manager','1'),(728,'Account Manager','1'),(729,'Chief of police officers','1'),(730,'Head of Security','1'),(731,'Management','1'),(732,'Instructor','1'),(733,'Academic Affairs Officer','1'),(734,'Professor','1'),(735,'Education and security','1'),(736,'Education plan','1'),(737,'Education management','1'),(738,'Trainee Management','1'),(739,'Education screening','1'),(740,'Education progress','1'),(741,'Education training','1'),(742,'Textbook Management','1'),(743,'Head of the department','1'),(744,'Exchange','1'),(745,'Emergency plan','1'),(746,'First aid','1'),(747,'Paramedic Management','1'),(748,'Emergency operation','1'),(749,'First aid guidance','1'),(750,'Rescue','1'),(751,'Rescue management','1'),(752,'Rescue First Aid Section Chief','1'),(753,'Head of Rescue First Aid Training Center','1'),(754,'Rescue Manager','1'),(755,'Rescue captain','1'),(756,'Rescue Team Leader','1'),(757,'Rescue operation','1'),(758,'Rescue guider','1'),(759,'Salary computation','1'),(760,'Machine','1'),(761,'Agency','1'),(762,'Planning and budget','1'),(763,'Planning Manager','1'),(764,'Planning Greetings','1'),(765,'Heating','1'),(766,'Purchase of goods','1'),(767,'Protection (guard)','1'),(768,'Protection Manager','1'),(769,'Director of Protection Structure','1'),(770,'Protection Officer','1'),(771,'Protection team leader','1'),(772,'Protection chief','1'),(773,'Press photo','1'),(774,'Press promotion','1'),(775,'Volunteer room','1'),(776,'Commander','1'),(777,'Subsidiary','1'),(778,'Deputy chief','1'),(779,'Analysis system','1'),(780,'breed','1'),(781,'boy','1'),(782,'Situation management','1'),(783,'Situation Chief','1'),(784,'Life guidance','1'),(785,'General manager','1'),(786,'General Affairs Officer','1'),(787,'Fire inspection','1'),(788,'Fire department chief','1'),(789,'Firefighting Planning Officer','1'),(790,'Fire Department','1'),(791,'Fire Department Head','1'),(792,'Fire chief','1'),(793,'Firefighting facilities','1'),(794,'Fire fighting water','1'),(795,'Fire brigade chief','1'),(796,'Fire Chief','1'),(797,'Fire Principal','1'),(798,'Fire Department Administration Manager','1'),(799,'Fire Administration Officer','1'),(800,'Water structure','1'),(801,'Water rescue leader','1'),(802,'Facility, flame retardant','1'),(803,'Facility manager','1'),(804,'Facility guide','1'),(805,'Facility promotion','1'),(806,'Safety','1'),(807,'Nutritionist','1'),(808,'prevention','1'),(809,'Prevention Manager','1'),(810,'Preventive management','1'),(811,'Prevention','1'),(812,'Prevention General','1'),(813,'Budget, accounting','1'),(814,'Budget operation','1'),(815,'Budget execution and settlement','1'),(816,'Water platoon management','1'),(817,'operation','1'),(818,'driving','1'),(819,'Driver','1'),(820,'Horticulture','1'),(821,'Cause investigation','1'),(822,'Hygiene ministry','1'),(823,'Danger goods','1'),(824,'Dangerous Goods Safety Officer','1'),(825,'Doctor','1'),(826,'Operation of medical contest','1'),(827,'Council operation','1'),(828,'General equipment','1'),(829,'Daily expenses','1'),(830,'Equipment management','1'),(831,'Equipment purchase','1'),(832,'Equipment in charge','1'),(833,'Property management','1'),(834,'Electricity','1'),(835,'Computational','1'),(836,'Computer development','1'),(837,'Computer education','1'),(838,'Computerized benefit processing','1'),(839,'Computational Planning','1'),(840,'Computing Manager','1'),(841,'Computerized operation','1'),(842,'Full-time instructor','1'),(843,'Mechanic','1'),(844,'The first officer','1'),(845,'The second chief','1'),(846,'3rd chieftain','1'),(847,'Manufacturing facility business','1'),(848,'Assistant','1'),(849,'Research','1'),(850,'Survey guide','1'),(851,'Survey statistics','1'),(852,'Pilot','1'),(853,'Organization','1'),(854,'Parking control','1'),(855,'In charge of guidance','1'),(856,'Command Officer','1'),(857,'Salary expenditure','1'),(858,'In charge of suppression 1','1'),(859,'In charge of suppression 2','1'),(860,'Operation of suppression exhibition','1'),(861,'Pick-up','1'),(862,'Private police','1'),(863,'Branch manager','1'),(864,'Typer','1'),(865,'Statistics','1'),(866,'Statistical analysis','1'),(867,'Communication','1'),(868,'Communication planning','1'),(869,'Commando','1'),(870,'Special Rescue Commander','1'),(871,'Commander of Special Rescue Unit','1'),(872,'Special equipment','1'),(873,'Damage investigation','1'),(874,'Handwriting shorthand','1'),(875,'Aircraft technology','1'),(876,'Flight ledger','1'),(877,'Sail','1'),(878,'Administration','1'),(879,'Executive Director','1'),(880,'Administrative information','1'),(881,'On-site operation','1'),(882,'Promotion','1'),(883,'Public relations enlightenment','1'),(884,'Public Relations Officer','1'),(885,'Fire prevention','1'),(886,'Fire investigation','1'),(887,'Fire investigation chief','1'),(888,'Welfare','1'),(889,'Cadre','1'),(890,'Nurse','1'),(891,'First aid','1'),(892,'Rescue','1'),(893,'Engineer','1'),(894,'Technology','1'),(895,'Leader','1'),(896,'Nutritionist','1'),(897,'Prevention','1'),(898,'driving','1'),(899,'Emergency Medical Technician','1'),(900,'Doctor','1'),(901,'Computational','1'),(902,'Mechanic','1'),(903,'Adjuster','1'),(904,'Private police','1'),(905,'Communication','1'),(906,'Mate','1'),(907,'Administration','1'),(908,'Daily work','1'),(909,'Emergency work','1'),(910,'Commander','1'),(911,'Completion','1'),(912,'Attending','1'),(913,'Graduated','1'),(914,'Withdraw','1'),(915,'Doctoral Program Completion','1'),(916,'Doctoral Program','1'),(917,'Doctoral Program Graduation','1'),(918,'Doctoral Program Dropout','1'),(919,'Master\'s Course Completion','1'),(920,'Master\'s course enrollment','1'),(921,'Master\'s Course Graduation','1'),(922,'Withdrawal from Master\'s Program','1'),(923,'Medical workers','1'),(924,'First-aid','1'),(925,'Firefighting Agency Training','1'),(926,'Other education','1'),(927,'On goinging','1'),(928,'Fire','1'),(929,'Rescue','1'),(930,'Forest fire','1'),(931,'Water accident','1'),(932,'Traffic Accident','1'),(933,'Railroad accident','1'),(934,'Aircraft accident','1'),(935,'Explosion','1'),(936,'Tanker Accident','1'),(937,'Shipwreck accident','1'),(938,'Marine accident','1'),(939,'Mine accident','1'),(940,'Environmental pollution accident','1'),(941,'Public Facility Accident','1'),(942,'Elevator accident','1'),(943,'Downpour','1'),(944,'Typhoon','1'),(945,'Heavy snow','1'),(946,'Drought','1'),(947,'Earthquake','1'),(948,'Gas leak (radioactive) accident','1'),(949,'Mountain climbing accident','1'),(950,'Building and bridge collapse accident','1'),(951,'Electric accident','1'),(952,'Other (emergency rescue mobilization type)','1'),(953,'Freight car','1'),(954,'Other equipment','1'),(955,'Water tank car','1'),(956,'Chemical car','1'),(957,'High heighted car','1'),(958,'Oyster procedure','1'),(959,'Ambulance','1'),(960,'Patrol car','1'),(961,'Diagnostic car','1'),(962,'Bulldozer','1'),(963,'Peyrodah','1'),(964,'Rack car','1'),(965,'Crane','1'),(966,'Dump truck','1'),(967,'Excavators','1'),(968,'Fork lift','1'),(969,'Watering, sprinkler','1'),(970,'Bus','1'),(971,'Aircraft (helicopter)','1'),(972,'Other cars','1'),(973,'Ladder car','1'),(974,'Fire engine','1'),(975,'Medium-sized pump car','1'),(976,'Small pump car','1'),(977,'Firefighting technology qualification recognized for education and career','1'),(978,'Wired Equipment Engineer','1'),(979,'Wired Equipment Industry Engineer','1'),(980,'General Machinery Industry Engineer','1'),(981,'Dangerous Goods Handling Technician','1'),(982,'Crane driver','1'),(983,'Forklift driver','1'),(984,'Tower crane driver','1'),(985,'Gas welding technician','1'),(986,'Welding craftsman','1'),(987,'Welding engineer','1'),(988,'Welding Industry Engineer','1'),(989,'Electric welding technician','1'),(990,'Special welding technician','1'),(991,'Excavator driver','1'),(992,'Loader driver','1'),(993,'Motor grader driver','1'),(994,'Bulldozer driver','1'),(995,'Gas technician','1'),(996,'Gas station','1'),(997,'Gas engineer','1'),(998,'Gas engineer','1'),(999,'Gas Industry Engineer','1'),(1000,'Radio Equipment Industry Engineer','1'),(1001,'Firefighting Equipment Engineer (Electric)','1'),(1002,'Firefighting Equipment Industry Engineer (Electric)','1'),(1003,'Chemical engineer','1'),(1004,'Civil engineering','1'),(1005,'Diving','1'),(1006,'High pressure gas','1'),(1007,'Other related qualifications','1'),(1008,'Construction machine equipment engineer','1'),(1009,'Construction equipment engineer','1'),(1010,'Construction equipment industry engineer','1'),(1011,'Architectural engineer','1'),(1012,'Construction Industry Engineer','1'),(1013,'General mechanical engineer','1'),(1014,'Production machinery industry engineer','1'),(1015,'Air conditioning refrigeration machine engineer','1'),(1016,'Air Conditioning Refrigeration Machine Engineer','1'),(1017,'Air Conditioning Refrigeration Machine Industry Engineer','1'),(1018,'Boiler handling technician','1'),(1019,'Construction machine engineer','1'),(1020,'Construction machinery industry engineer','1'),(1021,'Heavy Equipment Engineer','1'),(1022,'Mold engineer','1'),(1023,'Thermal Management Engineer','1'),(1024,'Industrial safety engineer','1'),(1025,'Firefighting equipment engineer','1'),(1026,'Firefighting equipment engineer (machine)','1'),(1027,'Firefighting equipment industry engineer (machine)','1'),(1028,'Gas engineer','1'),(1029,'Gas Industry Engineer','1'),(1030,'Dangerous Goods Management Industry Engineer','1'),(1031,'Dangerous Goods Management Technician','1'),(1032,'Electrician','1'),(1033,'Electrical Industry Engineer','1'),(1034,'Architectural Electrical Equipment Engineer','1'),(1035,'Electrical construction engineer','1'),(1036,'Electrical construction industry engineer','1'),(1037,'Electric construction technician','1'),(1038,'Electronics Industry Engineer','1'),(1039,'Architect','1'),(1040,'Office Automation Industry Engineer','1'),(1041,'Radio communication engineer','1'),(1042,'Radio Communication Industry Engineer','1'),(1043,'Firefighting Facility Manager','1'),(1044,'Emergency Medical Center','1'),(1045,'Other facilities','1'),(1046,'No. 1 dispatch','1'),(1047,'No. 2 dispatch','1'),(1048,'No. 3 dispatch','1'),(1049,'Emergency dispatch','1'),(1050,'Rescue No. 1','1'),(1051,'Rescue No. 2','1'),(1052,'Rescue emergency','1'),(1053,'No.1 First aid','1'),(1054,'No.2 First aid','1'),(1055,'Emergency first aid','1');
/*!40000 ALTER TABLE `sm_cd_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sm_cd_resource`
--

DROP TABLE IF EXISTS `sm_cd_resource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sm_cd_resource` (
  `cd` int(11) NOT NULL COMMENT '코드',
  `cd_name` varchar(100) DEFAULT NULL COMMENT '코드명',
  `cd_desc` varchar(300) DEFAULT NULL COMMENT '코드설명',
  `locale` varchar(2) NOT NULL COMMENT '국가코드',
  PRIMARY KEY (`cd`,`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sm_cd_resource`
--

LOCK TABLES `sm_cd_resource` WRITE;
/*!40000 ALTER TABLE `sm_cd_resource` DISABLE KEYS */;
INSERT INTO `sm_cd_resource` VALUES (1,'FSCD',NULL,'en'),(1,'소방민방위청',NULL,'kr'),(2,'Divisional HQ',NULL,'en'),(2,'지방본부',NULL,'kr'),(3,'Fire office(A)',NULL,'en'),(3,'소방서(A) ',NULL,'kr'),(4,'Fire office(B)',NULL,'en'),(4,'소방서(B)',NULL,'kr'),(5,'Fire office(C)',NULL,'en'),(5,'소방서(C)',NULL,'kr'),(6,'Fire office(R)',NULL,'en'),(6,'소방서(R) ',NULL,'kr'),(7,'Etc',NULL,'en'),(7,'기타',NULL,'kr'),(8,'Department of FSCD',NULL,'en'),(8,'소방민방위청 부서',NULL,'kr'),(9,'Department of HQ',NULL,'en'),(9,'HQ 부서',NULL,'kr'),(10,'Department of fire office',NULL,'en'),(10,'소방서 부서',NULL,'kr'),(11,'Read / write',NULL,'en'),(11,'읽기쓰기',NULL,'kr'),(12,'Read only',NULL,'en'),(12,'읽기전용',NULL,'kr'),(13,'Completed',NULL,'en'),(13,'완료',NULL,'kr'),(14,'Not completed',NULL,'en'),(14,'미처리',NULL,'kr'),(15,'Request User Account ',NULL,'en'),(15,'계정승인요청',NULL,'kr'),(16,'Request User Auth',NULL,'en'),(16,'권한요청',NULL,'kr'),(17,'Rejected',NULL,'en'),(17,'거절됨','','kr'),(18,'Contract worker',NULL,'en'),(18,'계약직',NULL,'kr'),(19,'Part timer',NULL,'en'),(19,'아르바이트',NULL,'kr'),(20,'Fire worker',NULL,'en'),(20,'소방대원',NULL,'kr'),(21,'Office worker',NULL,'en'),(21,'사무직',NULL,'kr'),(22,'System manager',NULL,'en'),(22,'시설관리',NULL,'kr'),(23,'Financial',NULL,'en'),(23,'재무',NULL,'kr'),(24,'Driver',NULL,'en'),(24,'운전',NULL,'kr'),(25,'Staff',NULL,'en'),(25,'사원',NULL,'kr'),(26,'General Manager',NULL,'en'),(26,'부장',NULL,'kr'),(27,'Duputy General Manager',NULL,'en'),(27,'차장',NULL,'kr'),(28,'Manager',NULL,'en'),(28,'과장',NULL,'kr'),(29,'Assistant Manager',NULL,'en'),(29,'대리',NULL,'kr'),(30,'Supervisor',NULL,'en'),(30,'주임',NULL,'kr'),(31,'Executives',NULL,'en'),(31,'임원',NULL,'kr'),(32,'Probationary Firefighter',NULL,'en'),(32,'소방사시보',NULL,'kr'),(33,'Paramedic',NULL,'en'),(33,'소방사',NULL,'kr'),(34,'Firefighter',NULL,'en'),(34,'소방교',NULL,'kr'),(35,'Lieutenant',NULL,'en'),(35,'소방장',NULL,'kr'),(36,'EMS Captain',NULL,'en'),(36,'소방위',NULL,'kr'),(37,'Battalion Chief',NULL,'en'),(37,'소방경',NULL,'kr'),(38,'Deputy Chief',NULL,'en'),(38,'소방령',NULL,'kr'),(39,'Division Chief',NULL,'en'),(39,'소방정',NULL,'kr'),(40,'Deputy Assistant Chief',NULL,'en'),(40,'소방준감',NULL,'kr'),(41,'Assistant Chief',NULL,'en'),(41,'소방감',NULL,'kr'),(42,'Chief of Fire Operations',NULL,'en'),(42,'소방정감',NULL,'kr'),(43,'Chief of Department',NULL,'en'),(43,'소방총감',NULL,'kr'),(44,'Elementary school',NULL,'en'),(44,'초등학교',NULL,'kr'),(45,'Middle school',NULL,'en'),(45,'중학교',NULL,'kr'),(46,'High school',NULL,'en'),(46,'고등학교',NULL,'kr'),(47,'University',NULL,'en'),(47,'대학교',NULL,'kr'),(48,'Postgraduate school',NULL,'en'),(48,'대학원',NULL,'kr'),(49,'Associate',NULL,'en'),(49,'준학사',NULL,'kr'),(50,'Bachelor',NULL,'en'),(50,'학사',NULL,'kr'),(51,'Master',NULL,'en'),(51,'석사',NULL,'kr'),(52,'Doctor',NULL,'en'),(52,'박사',NULL,'kr'),(53,'Medical practitioner',NULL,'en'),(53,'의료종사자',NULL,'kr'),(54,'Aid certificate',NULL,'en'),(54,'구급자격자',NULL,'kr'),(55,'Fire fighting education',NULL,'en'),(55,'소방기관교육',NULL,'kr'),(56,'Etc',NULL,'en'),(56,'그밖의 교육기관',NULL,'kr'),(57,'Finished',NULL,'en'),(57,'수료',NULL,'kr'),(58,'Not finished',NULL,'en'),(58,'미수료',NULL,'kr'),(59,'Police',NULL,'en'),(59,'경찰',NULL,'kr'),(60,'Electric company',NULL,'en'),(60,'전기안전공사',NULL,'kr'),(61,'Gas',NULL,'en'),(61,'가스',NULL,'kr'),(62,'Private rescue Org',NULL,'en'),(62,'민간구조단체',NULL,'kr'),(63,'Mountain rescue',NULL,'en'),(63,'산악',NULL,'kr'),(64,'Subway',NULL,'en'),(64,'지하철',NULL,'kr'),(65,'Hospital',NULL,'en'),(65,'병원',NULL,'kr'),(66,'Central authority',NULL,'en'),(66,'중앙기관',NULL,'kr'),(67,'City/District/Upazila',NULL,'en'),(67,'시/군/구청',NULL,'kr'),(68,'Army',NULL,'en'),(68,'군부대',NULL,'kr'),(69,'Environment',NULL,'en'),(69,'환경',NULL,'kr'),(70,'Health institution',NULL,'en'),(70,'보건기관',NULL,'kr'),(71,'Information center',NULL,'en'),(71,'정보센터',NULL,'kr'),(72,'Local government',NULL,'en'),(72,'지방자치단체',NULL,'kr'),(73,'Transfer center',NULL,'en'),(73,'이송단체',NULL,'kr'),(74,'Etc(Embrance)',NULL,'en'),(74,'기타(구급차)',NULL,'kr'),(75,'public health',NULL,'en'),(75,'보건소',NULL,'kr'),(76,'Hospital',NULL,'en'),(76,'병원',NULL,'kr'),(77,'Pharmacy',NULL,'en'),(77,'약국',NULL,'kr'),(78,'Freight car',NULL,'en'),(78,'화물차',NULL,'kr'),(79,'Other equipment',NULL,'en'),(79,'기타장비',NULL,'kr'),(80,'Water tank car',NULL,'en'),(80,'물탱크차',NULL,'kr'),(81,'Chemical vehicle',NULL,'en'),(81,'화학차',NULL,'kr'),(82,'Ladder vehicle (High)',NULL,'en'),(82,'고가차',NULL,'kr'),(83,'Refraction car',NULL,'en'),(83,'굴절차',NULL,'kr'),(84,'Ambulance',NULL,'en'),(84,'구급차',NULL,'kr'),(85,'Patrol car',NULL,'en'),(85,'순찰차',NULL,'kr'),(86,'Diagnosis car',NULL,'en'),(86,'진단차',NULL,'kr'),(87,'Bulldozer',NULL,'en'),(87,'불도저',NULL,'kr'),(88,'Peyrona',NULL,'en'),(88,'페이로다',NULL,'kr'),(89,'Rack car',NULL,'en'),(89,'래카차',NULL,'kr'),(90,'Crane',NULL,'en'),(90,'기중기',NULL,'kr'),(91,'Dump truck',NULL,'en'),(91,'덤프트럭',NULL,'kr'),(92,'Excavators',NULL,'en'),(92,'굴삭기',NULL,'kr'),(93,'Fork lift',NULL,'en'),(93,'지게차',NULL,'kr'),(94,'Water supply wagon',NULL,'en'),(94,'급수.살수차',NULL,'kr'),(95,'Bus',NULL,'en'),(95,'버스',NULL,'kr'),(96,'Helicopter',NULL,'en'),(96,'항공기(헬기)',NULL,'kr'),(97,'Etc car',NULL,'en'),(97,'기타차',NULL,'kr'),(98,'Ladder car',NULL,'en'),(98,'사다리차',NULL,'kr'),(99,'Fire vehicle',NULL,'en'),(99,'소방차',NULL,'kr'),(100,'Pump car(Middle)',NULL,'en'),(100,'중형펌프차',NULL,'kr'),(101,'Pumb car(Small)',NULL,'en'),(101,'소형펌프차',NULL,'kr'),(102,'Construction machine engineer',NULL,'en'),(102,'건설기계기사',NULL,'kr'),(103,'Heavy Equipment Engineer',NULL,'en'),(103,'중장비기사',NULL,'kr'),(104,'Mold engineer',NULL,'en'),(104,'금형기술사',NULL,'kr'),(105,'Thermal Management Engineer',NULL,'en'),(105,'열관리기사',NULL,'kr'),(106,'Industrial safety engineer',NULL,'en'),(106,'산업안전기사',NULL,'kr'),(107,'Firefighting equipment engineer',NULL,'en'),(107,'소방설비기술사',NULL,'kr'),(108,'Gas engineer',NULL,'en'),(108,'가스기사',NULL,'kr'),(109,'Danger goods manager',NULL,'en'),(109,'위험물관리기능사',NULL,'kr'),(110,'Electrician',NULL,'en'),(110,'전기기사',NULL,'kr'),(111,'Radio communication engineer',NULL,'en'),(111,'전파통신기사',NULL,'kr'),(112,'Firefighting Facility Manager',NULL,'en'),(112,'소방시설관리사',NULL,'kr'),(113,'1st mobilization',NULL,'en'),(113,'1차출동',NULL,'kr'),(114,'2nd mobilization',NULL,'en'),(114,'2차출동',NULL,'kr'),(115,'3rd mobilization',NULL,'en'),(115,'3차출동',NULL,'kr'),(116,'Regular worker',NULL,'en'),(116,'정규직',NULL,'kr'),(117,'College',NULL,'en'),(117,'전문대학',NULL,'kr'),(118,'Vocational training center',NULL,'en'),(118,'직업훈련소',NULL,'kr'),(119,'Approved',NULL,'en'),(119,'승인됨',NULL,'kr'),(120,'1st fire safety management',NULL,'en'),(120,'1급소방안전관리대상',NULL,'kr'),(121,'2nd fire safety management',NULL,'en'),(121,'2급소방안전관리대상',NULL,'kr'),(122,'Fire safety management for public organizations',NULL,'en'),(122,'공공기관소방안전관리대상',NULL,'kr'),(123,'Circus ground','','en'),(123,'서커스장','','kr'),(124,'Others (performance facility)','','en'),(124,'기타(공연시설)','','kr'),(125,'Theater','','en'),(125,'극장','','kr'),(126,'Bandstand','','en'),(126,'음악당','','kr'),(127,'Performance hall (less than 300 square meters)','','en'),(127,'공연장(300제곱미터 미만)','','kr'),(128,'Movie theater','','en'),(128,'영화상영관','','kr'),(129,'Entertainment hall','','en'),(129,'연예장','','kr'),(130,'Video small theater business (less than 300 square meters)','','en'),(130,'비디오소극장업(300제곱미터 미만)','','kr'),(131,'Video surveillance loss (less than 300 square meters)','','en'),(131,'비디오감물상실업(300제곱미터 미만)','','kr'),(132,'Concert hall','','en'),(132,'공연장','','kr'),(133,'Others (Factory)','','en'),(133,'기타(공장)','','kr'),(134,'Processing plant','','en'),(134,'가공공장','','kr'),(135,'Repair shop','','en'),(135,'수리공장','','kr'),(136,'Manufacturing plant','','en'),(136,'제조공장','','kr'),(137,'Manufacturing establishment (less than 500 square meters)','','en'),(137,'제조업소(500제곱미터 미만)','','kr'),(138,'Loading dock','','en'),(138,'하역장','','kr'),(139,'Warehouse','','en'),(139,'창고','','kr'),(140,'Others (warehouse facilities)','','en'),(140,'기타(창고시설)','','kr'),(141,'Outdoor music hall','','en'),(141,'야외음악당','','kr'),(142,'Park, amusement park','','en'),(142,'공원,유원지','','kr'),(143,'Military recreational facilities','','en'),(143,'군휴양시설','','kr'),(144,'Park architecture','','en'),(144,'공원건축물','','kr'),(145,'Others (tourist rest facilities)','','en'),(145,'기타(관광휴게시설)','','kr'),(146,'Observation tower','','en'),(146,'관망탑','','kr'),(147,'Outdoor theater','','en'),(147,'야외극장','','kr'),(148,'Rest area','','en'),(148,'휴게소','','kr'),(149,'Education Institution','','en'),(149,'교육원','','kr'),(150,'Special school','','en'),(150,'특수학교','','kr'),(151,'Others (educational research facilities)','','en'),(151,'기타(교육연구시설)','','kr'),(152,'University','','en'),(152,'대학','','kr'),(153,'Academy (less than 500 square meters)','','en'),(153,'학원(500제곱미터 미만)','','kr'),(154,'High school','','en'),(154,'고등학교','','kr'),(155,'Middle School','','en'),(155,'중학교','','kr'),(156,'Elementary School','','en'),(156,'초등학교','','kr'),(157,'Library','','en'),(157,'도서관','','kr'),(158,'Laboratory','','en'),(158,'연구소','','kr'),(159,'Academy','','en'),(159,'학원','','kr'),(160,'Vocational training center','','en'),(160,'직업훈련소','','kr'),(161,'Others (Elderly People\'s Facility)','','en'),(161,'기타(노유자시설)','','kr'),(162,'Elderly Welfare Facility','','en'),(162,'노인복지시설','','kr'),(163,'Rehabilitation facilities for the disabled','','en'),(163,'장애인재활시설','','kr'),(164,'Braille Library','','en'),(164,'점자도서관','','kr'),(165,'Infant Child Care Facility','','en'),(165,'영유아보육시설','','kr'),(166,'Kindergarden','','en'),(166,'유치원','','kr'),(167,'Nursing facility','','en'),(167,'요양시설','','kr'),(168,'Child welfare facilities','','en'),(168,'아동복지시설','','kr'),(169,'Animal hospital (less than 500 square meters)','','en'),(169,'동물병원(500제곱미터 미만)','','kr'),(170,'Others (facilities related to animals and plants)','','en'),(170,'기타(동식물관련시설)','','kr'),(171,'Plant related facilities','','en'),(171,'식물관련시설','','kr'),(172,'Slaughterhouse','','en'),(172,'도축장','','kr'),(173,'Mushroom cultivation','','en'),(173,'버섯재배사','','kr'),(174,'Seedling culture facility','','en'),(174,'종묘배양시설','','kr'),(175,'Greenhouse','','en'),(175,'온실','','kr'),(176,'Barn','','en'),(176,'축사','','kr'),(177,'Livestock exercise facility','','en'),(177,'가축용운동시설','','kr'),(178,'Artificial Insemination Center','','en'),(178,'인공수정센터','','kr'),(179,'Warehouse for livestock','','en'),(179,'가축용창고','','kr'),(180,'Livestock market','','en'),(180,'가축시장','','kr'),(181,'Animal Quarantine Station','','en'),(181,'동물검역소','','kr'),(182,'Experimental Animal Breeding Facility','','en'),(182,'실험동물사육시설','','kr'),(183,'Animal and botanical garden','','en'),(183,'동,식물원','','kr'),(184,'Zoo','','en'),(184,'동물원','','kr'),(185,'Botanical garden','','en'),(185,'식물원','','kr'),(186,'Aquarium','','en'),(186,'수족관','','kr'),(187,'Power plant','','en'),(187,'발전소','','kr'),(188,'Substation','','en'),(188,'변전소','','kr'),(189,'Others (power generation facilities)','','en'),(189,'기타(발전시설)','','kr'),(190,'Scenic spot','','en'),(190,'명승','','kr'),(191,'Historic Site','','en'),(191,'사적','','kr'),(192,'Traditional architecture','','en'),(192,'전통건축물','','kr'),(193,'Others (Cultural Property)','','en'),(193,'기타(문화재)','','kr'),(194,'Treasure (National Designated Cultural Property)','','en'),(194,'보물(국가지정문화재)','','kr'),(195,'National Treasure (National Designated Cultural Property)','','en'),(195,'국보(국가지정문화재)','','kr'),(196,'Designated Cultural Property','','en'),(196,'지정문화재','','kr'),(197,'Historic Site (National Designated Cultural Property)','','en'),(197,'사적(국가지정문화재)','','kr'),(198,'General hotel','','en'),(198,'일반호텔','','kr'),(199,'Motel','','en'),(199,'모텔','','kr'),(200,'Water Tourist Hotel','','en'),(200,'수상관광호텔','','kr'),(201,'Inn','','en'),(201,'여관','','kr'),(202,'Others (accommodation facilities)','','en'),(202,'기타(숙박시설)','','kr'),(203,'Tourist Hotel','','en'),(203,'관광호텔','','kr'),(204,'Family Hotel','','en'),(204,'가족호텔','','kr'),(205,'Resort condominium','','en'),(205,'휴양콘도미니엄','','kr'),(206,'National Insurance Corporation','','en'),(206,'국민보험공단','','kr'),(207,'Other correction facilities','','en'),(207,'기타교정시설','','kr'),(208,'Pumping station','','en'),(208,'양수장','','kr'),(209,'Shelter','','en'),(209,'대피소','','kr'),(210,'Public business facilities','','en'),(210,'공공업무시설','','kr'),(211,'General business facilities','','en'),(211,'일반업무시설','','kr'),(212,'Officetels','','en'),(212,'오피스텔','','kr'),(213,'Newspaper','','en'),(213,'신문사','','kr'),(214,'Prison','','en'),(214,'교도소','','kr'),(215,'Jail','','en'),(215,'구치소','','kr'),(216,'Reformatory','','en'),(216,'소년원','','kr'),(217,'Juvenile Classification Examiner','','en'),(217,'소년분류심사원','','kr'),(218,'Water purification plant','','en'),(218,'정수장','','kr'),(219,'Public toilet','','en'),(219,'공중화장실','','kr'),(220,'Village public hall','','en'),(220,'마을공회당','','kr'),(221,'Village joint work place','','en'),(221,'마을공동작업소','','kr'),(222,'Village joint market','','en'),(222,'마을공동구판장','','kr'),(223,'Police office','','en'),(223,'경찰서','','kr'),(224,'Fire station','','en'),(224,'소방서','','kr'),(225,'Post office','','en'),(225,'우체국','','kr'),(226,'Public Health','','en'),(226,'보건소','','kr'),(227,'Public library','','en'),(227,'공공도서관','','kr'),(228,'Financial business (less than 500 square meters)','','en'),(228,'금융업소(500제곱미터 미만)','','kr'),(229,'Office (less than 500 square meters)','','en'),(229,'사무소(500제곱미터 미만)','','kr'),(230,'Real estate brokerage (less than 500 square meters)','','en'),(230,'부동산중개업소(500제곱미터 미만)','','kr'),(231,'Publisher (less than 500 square meters)','','en'),(231,'출판사(500제곱미터 미만)','','kr'),(232,'Bookstore (less than 500 square meters)','','en'),(232,'서점(500제곱미터 미만)','','kr'),(233,'Marriage Consultation Center (less than 500 square meters)','','en'),(233,'결혼상담소(500제곱미터 미만)','','kr'),(234,'Town office (less than 1,000 square meters)','','en'),(234,'동사무소(1천제곱미터 미만)','','kr'),(235,'Police station (less than 1,000 square meters)','','en'),(235,'경찰서(1천제곱미터 미만)','','kr'),(236,'Fire department (less than 1,000 square meters)','','en'),(236,'소방서(1천제곱미터 미만)','','kr'),(237,'Post office (less than 1,000 square meters)','','en'),(237,'우체국(1천제곱미터 미만)','','kr'),(238,'Public health center (less than 1,000 square meters)','','en'),(238,'보건소(1천제곱미터 미만)','','kr'),(239,'Telegraphic and telephone offices (less than 1,000 square meters)','','en'),(239,'전신전화국(1천제곱미터 미만)','','kr'),(240,'Broadcasting station (less than 1,000 square meters)','','en'),(240,'방송국(1천제곱미터 미만)','','kr'),(241,'Public library (less than 1,000 square meters)','','en'),(241,'공공도서관(1천제곱미터 미만)','','kr'),(242,'National Insurance Corporation (less than 1,000 square meters)','','en'),(242,'국민보험공단(1천제곱미터 미만)','','kr'),(243,'Local gorvement office','','en'),(243,'동사무소','','kr'),(244,'Others (business facilities)','','en'),(244,'기타(업무시설)','','kr'),(245,'Passenger Car Terminal','','en'),(245,'여객자동차터미널','','kr'),(246,'Cargo terminal','','en'),(246,'화물터미널','','kr'),(247,'Railway history','','en'),(247,'철도역사','','kr'),(248,'Airport facilities','','en'),(248,'공항시설','','kr'),(249,'Port facilities','','en'),(249,'항만시설','','kr'),(250,'Comprehensive Passenger Facility','','en'),(250,'종합여객시설','','kr'),(251,'Others (history, terminal)','','en'),(251,'기타(역사,터미널)','','kr'),(252,'Automobile maintenance plant','','en'),(252,'자동차정비공장','','kr'),(253,'Junkyard','','en'),(253,'폐차장','','kr'),(254,'Car wash','','en'),(254,'세차장','','kr'),(255,'Parking lot','','en'),(255,'주차장','','kr'),(256,'Parking building','','en'),(256,'주차용건축물','','kr'),(257,'Garage and parking lot','','en'),(257,'차고및주기장','','kr'),(258,'Car driving school','','en'),(258,'자동차운전학원','','kr'),(259,'Car dealer','','en'),(259,'자동차부속상','','kr'),(260,'Car shop','','en'),(260,'자동차매매장','','kr'),(261,'Automobile inspection station','','en'),(261,'자동차검사장','','kr'),(262,'Aircraft hangar','','en'),(262,'항공기격납고','','kr'),(263,'Others (Transportation Vehicle-related Facilities)','','en'),(263,'기타(운수자동차관련시설)','','kr'),(264,'Dance club','','en'),(264,'무도장','','kr'),(265,'Bar','','en'),(265,'주점','','kr'),(266,'Casino','','en'),(266,'카지노업소','','kr'),(267,'Amusement facilities','','en'),(267,'유원시설업','','kr'),(268,'Martial Arts Academy','','en'),(268,'무도학원','','kr'),(269,'Others (entertainment facilities)','','en'),(269,'기타(위락시설)','','kr'),(270,'Tujeon Enterprise','','en'),(270,'투전기업소','','kr'),(271,'Crematorium','','en'),(271,'화장장','','kr'),(272,'Others (related facilities such as sanitation)','','en'),(272,'기타(위생 등 관련시설)','','kr'),(273,'Funeral director (less than 500 square meters)','','en'),(273,'장의사(500제곱미터 미만)','','kr'),(274,'Ossuary','','en'),(274,'납골당','','kr'),(275,'Waste treatment facility','','en'),(275,'폐기물처리시설','','kr'),(276,'Waste recycling facility','','en'),(276,'폐기물재활용시설','','kr'),(277,'Cemetery','','en'),(277,'묘지부수건축물','','kr'),(278,'Antique shop','','en'),(278,'고물상','','kr'),(279,'Oriental Medicine Hospital','','en'),(279,'한방병원','','kr'),(280,'Funeral','','en'),(280,'장례식장','','kr'),(281,'Infectious Hospital','','en'),(281,'전염병원','','kr'),(282,'Drug clinic','','en'),(282,'마약진료소','','kr'),(283,'General Hospital','','en'),(283,'종합병원','','kr'),(284,'Others (medical facilities)','','en'),(284,'기타(의료시설)','','kr'),(285,'Osteopath','','en'),(285,'접골원','','kr'),(286,'Acupuncture','','en'),(286,'침술원','','kr'),(287,'Massage parlor','','en'),(287,'안마시술소','','kr'),(288,'Oriental medicine','','en'),(288,'한의원','','kr'),(289,'Dental clinic','','en'),(289,'치과의원','','kr'),(290,'Lawmaker','','en'),(290,'의원','','kr'),(291,'Midwife','','en'),(291,'조산소','','kr'),(292,'Sanitarium','','en'),(292,'요양소','','kr'),(293,'Mental health facility','','en'),(293,'정신보건시설','','kr'),(294,'Dental hospital','','en'),(294,'치과병원','','kr'),(295,'Postpartum care center','','en'),(295,'산후조리원','','kr'),(296,'General hospital','','en'),(296,'일반병원','','kr'),(297,'House','','en'),(297,'집회장','','kr'),(298,'Wedding hall','','en'),(298,'예식장','','kr'),(299,'Conference hall','','en'),(299,'회의장','','kr'),(300,'Public hall','','en'),(300,'공회장','','kr'),(301,'Bookstore outside the market','','en'),(301,'마권장외발매소','','kr'),(302,'Viewing hall','','en'),(302,'관람장','','kr'),(303,'Racetrack','','en'),(303,'경마장','','kr'),(304,'Car racetrack','','en'),(304,'자동차경주장','','kr'),(305,'Playground','','en'),(305,'운동장','','kr'),(306,'Showroom','','en'),(306,'전시장','','kr'),(307,'Museum','','en'),(307,'박물관','','kr'),(308,'Art gallery','','en'),(308,'미술관','','kr'),(309,'Science Museum','','en'),(309,'과학관','','kr'),(310,'Memorial','','en'),(310,'기념관','','kr'),(311,'Industrial exhibition','','en'),(311,'산업전시장','','kr'),(312,'Fairground','','en'),(312,'박람회장','','kr'),(313,'Table tennis court','','en'),(313,'탁구장','','kr'),(314,'Golf Driving Range','','en'),(314,'골프연습장','','kr'),(316,'Tennis court (less than 500 square meters)','','en'),(316,'정구장(500제곱미터 미만)','','kr'),(317,'Table tennis court (less than 500 square meters)','','en'),(317,'탁구장(500제곱미터 미만)','','kr'),(318,'Bowling alley (less than 500 square meters)','','en'),(318,'볼링장(500제곱미터 미만)','','kr'),(319,'Health Club Room (less than 500 square meters)','','en'),(319,'헬스크럽장(500제곱미터 미만)','','kr'),(320,'Physical education painting (less than 500 square meters)','','en'),(320,'체육도장(500제곱미터 미만)','','kr'),(321,'Tennis court (less than 500 square meters)','','en'),(321,'테니스장(500제곱미터 미만)','','kr'),(322,'Physical fitness center (less than 500 square meters)','','en'),(322,'체력단련장(500제곱미터 미만)','','kr'),(323,'Aerobics field (less than 500 square meters)','','en'),(323,'에어로빅장(500제곱미터 미만)','','kr'),(324,'Billiard room (less than 500 square meters)','','en'),(324,'당구장(500제곱미터 미만)','','kr'),(325,'Indoor fishing ground (less than 500 square meters)','','en'),(325,'실내낚시터(500제곱미터 미만)','','kr'),(326,'Golf driving range (less than 500 square meters)','','en'),(326,'골프연습장(500제곱미터 미만)','','kr'),(327,'Photo studio (less than 500 square meters)','','en'),(327,'사진관(500제곱미터 미만)','','kr'),(328,'Game providing business (less than 500 square meters)','','en'),(328,'게임제공업(500제곱미터 미만)','','kr'),(329,'Multimedia cultural contents facility provision business (less than 500 square meters)','','en'),(329,'멀티미디어문화컨텐츠설비제공업(500제곱미터 미만)','','kr'),(330,'Offer business (less than 500 square meters)','','en'),(330,'제공업(500제곱미터 미만)','','kr'),(331,'Betting phone voting booth','','en'),(331,'마권전화투표소','','kr'),(332,'Others (observation facilities, parks, sports facilities)','','en'),(332,'기타(관람시설,공원,운동시설)','','kr'),(333,'Sauna','','en'),(333,'찜질방','','kr'),(334,'Athletic field','','en'),(334,'육상경기장','','kr'),(335,'Ball stadium','','en'),(335,'구기경기장','','kr'),(336,'Swimming pool','','en'),(336,'수영장','','kr'),(337,'Skating rink','','en'),(337,'스케이트장','','kr'),(338,'Roller\'s Kate Ground','','en'),(338,'로울러스케이트장','','kr'),(339,'Riding ground','','en'),(339,'승마장','','kr'),(340,'Range ground','','en'),(340,'사격장','','kr'),(341,'Archery ground','','en'),(341,'궁도장','','kr'),(342,'Golf course','','en'),(342,'골프장','','kr'),(343,'Bowling club','','en'),(343,'볼링장','','kr'),(344,'Gym','','en'),(344,'체육관','','kr'),(345,'General bath','','en'),(345,'일반목욕장','','kr'),(346,'Physical education stamp','','en'),(346,'체육도장','','kr'),(347,'Tennis court','','en'),(347,'테니스장','','kr'),(348,'Fitness center','','en'),(348,'체력단련장','','kr'),(349,'Aerobics field','','en'),(349,'에어로빅장','','kr'),(350,'Billiard room','','en'),(350,'당구장','','kr'),(351,'Indoor fishing ground','','en'),(351,'실내낚시터','','kr'),(352,'Monastery','','en'),(352,'수도원','','kr'),(353,'Oratory','','en'),(353,'기도원','','kr'),(354,'Others (religious facilities)','','en'),(354,'기타(종교시설)','','kr'),(355,'Religious assembly hall (less than 300 square meters)','','en'),(355,'종교집회장(300제곱미터 미만)','','kr'),(356,'Convent','','en'),(356,'수녀원','','kr'),(357,'Temple','','en'),(357,'사찰','','kr'),(358,'Production','','en'),(358,'제실','','kr'),(359,'Religious Assembly Hall','','en'),(359,'종교집회장','','kr'),(360,'Church','','en'),(360,'교회','','kr'),(361,'Cathedral','','en'),(361,'성당','','kr'),(362,'Shrine','','en'),(362,'사당','','kr'),(363,'Apartment','','en'),(363,'아파트','','kr'),(364,'Other (House)','','en'),(364,'기타(주택)','','kr'),(365,'Dormitory','','en'),(365,'기숙사','','kr'),(366,'For heating and cooling piping','','en'),(366,'냉난방배관용','','kr'),(367,'Communication','','en'),(367,'통신용','','kr'),(368,'For electric power','','en'),(368,'전력용','','kr'),(369,'Others (underground)','','en'),(369,'기타(지하가)','','kr'),(370,'Tunnel','','en'),(370,'터널','','kr'),(371,'Underground shopping mall','','en'),(371,'지하상가','','kr'),(372,'Youth Center','','en'),(372,'청소년수련관','','kr'),(373,'Others (youth training facilities)','','en'),(373,'기타(청소년수련시설)','','kr'),(374,'Youth campsite','','en'),(374,'청소년야영장','','kr'),(375,'Youth Culture House','','en'),(375,'청소년문화의집','','kr'),(376,'Youth Hostel','','en'),(376,'유스호스텔','','kr'),(377,'Youth Training Center','','en'),(377,'청소년수련원','','kr'),(378,'Telegraph office','','en'),(378,'전신전화국','','kr'),(379,'Broadcast stations','','en'),(379,'방송국','','kr'),(380,'Others (communication photography facilities)','','en'),(380,'기타(통신촬영시설)','','kr'),(381,'Communication facility','','en'),(381,'통신용시설','','kr'),(382,'Studio','','en'),(382,'촬영소','','kr'),(383,'Wholesale market','','en'),(383,'도매시장','','kr'),(384,'Retail market','','en'),(384,'소매시장','','kr'),(385,'Shop','','en'),(385,'상점','','kr'),(386,'Shopping center','','en'),(386,'쇼핑센타','','kr'),(387,'Department store','','en'),(387,'백화점','','kr'),(388,'Discount store','','en'),(388,'할인점','','kr'),(389,'Specialty store','','en'),(389,'전문점','','kr'),(390,'Supermarket (less than 1,000 square meters)','','en'),(390,'슈퍼마켓(1천제곱미터 미만)','','kr'),(391,'Daily necessities retail store (less than 1,000 square meters)','','en'),(391,'일용품소매점(1천제곱미터 미만)','','kr'),(392,'Bakery (less than 150 square meters)','','en'),(392,'재과점(150제곱미터 미만)','','kr'),(393,'General restaurant (less than 150 square meters)','','en'),(393,'일반음식점(150제곱미터 미만)','','kr'),(394,'Repair shop (less than 500 square meters)','','en'),(394,'수리점(500제곱미터 미만)','','kr'),(395,'Laundry (less than 500 square meters)','','en'),(395,'세탁소(500제곱미터 미만)','','kr'),(396,'Standard point (less than 500 square meters)','','en'),(396,'표구점(500제곱미터 미만)','','kr'),(397,'Gun shop (less than 500 square meters)','','en'),(397,'총포판매소(500제곱미터 미만)','','kr'),(398,'Combined distribution (less than 500 square meters)','','en'),(398,'복합유통(500제곱미터 미만)','','kr'),(399,'Laundry (excluding those with a factory)','','en'),(399,'세탁소(공장이 부설된 것 제외)','','kr'),(400,'Hair shop','','en'),(400,'미용원','','kr'),(401,'Pharmaceutical wholesale store (less than 1,000 square meters)','','en'),(401,'의약품도매점(1천제곱미터 미만)','','kr'),(402,'Others (sales facilities)','','en'),(402,'기타(판매시설)','','kr'),(403,'Automobile office (less than 1,000 square meters)','','en'),(403,'자동차영업소(1천제곱미터 미만)','','kr'),(404,'Dangerous goods manufacturing plant, etc.','','en'),(404,'위험물제조소등','','kr'),(405,'Gas storage facility','','en'),(405,'가스저장시설','','kr'),(406,'Others (dangerous material storage and treatment facilities)','','en'),(406,'기타(위험물저장 및 처리시설)','','kr'),(407,'Gas handling facility','','en'),(407,'가스취급시설','','kr'),(408,'Gas manufacturing facility','','en'),(408,'가스제조시설','','kr'),(409,'Dangerous Goods Manufacturing','','en'),(409,'위험물제조소','','kr'),(410,'Dangerous goods storage','','en'),(410,'위험물저장소','','kr'),(411,'Dangerous goods emergency station','','en'),(411,'위험물위급소','','kr'),(412,'Model house','','en'),(412,'모델하우스','','kr'),(413,'Others (complex buildings)','','en'),(413,'기타(복합건축물)','','kr'),(414,'Complex building','','en'),(414,'복합건축물','','kr'),(415,'Video painting industry','','en'),(415,'비디오물감상실업','','kr'),(416,'Video water buffalo theater business','','en'),(416,'비디오물소극장업','','kr'),(417,'Pharmaceutical wholesale business','','en'),(417,'의약품도매업','','kr'),(418,'Clinic (including dentistry/oriental clinic)','','en'),(418,'의원(치과/한의원포함)','','kr'),(419,'Daily necessities','','en'),(419,'일용품','','kr'),(420,'Car sales office','','en'),(420,'자동차영업소','','kr'),(421,'Book publisher','','en'),(421,'출판사','','kr'),(422,'Gym','','en'),(422,'헬스클럽장','','kr'),(423,'Rest restaurant','','en'),(423,'휴게음식점','','kr'),(424,'Others (Neighborhood Life)','','en'),(424,'기타(근린생활)','','kr'),(425,'Water play-type facility','','en'),(425,'물놀이형시설','','kr'),(426,'Medical device sales office','','en'),(426,'의료기기판매소','','kr'),(427,'Super market','','en'),(427,'슈퍼마켓','','kr'),(428,'Bakery','','en'),(428,'제과점','','kr'),(429,'Marriage Consultation Center',NULL,'en'),(429,'결혼상담소',NULL,'kr'),(430,'Factory',NULL,'en'),(430,'공장',NULL,'kr'),(431,'Financial business',NULL,'en'),(431,'금융업소',NULL,'kr'),(432,'Etc(Neighborhood life facility)',NULL,'en'),(432,'기타(근린생활)',NULL,'kr'),(433,'Animal hospital',NULL,'en'),(433,'동물병원',NULL,'kr'),(434,'Complex distribution/providing business',NULL,'en'),(434,'복합유통/제공업',NULL,'kr'),(435,'Real estate brokerage',NULL,'en'),(435,'부동산중개업소',NULL,'kr'),(436,'Office',NULL,'en'),(436,'사무소',NULL,'kr'),(437,'Photo studio',NULL,'en'),(437,'사진관',NULL,'kr'),(438,'Bookstore',NULL,'en'),(438,'서점',NULL,'kr'),(439,'Laundry',NULL,'en'),(439,'세탁소',NULL,'kr'),(440,'Retail',NULL,'en'),(440,'소매업',NULL,'kr'),(441,'Restaurant',NULL,'en'),(441,'일반음식점',NULL,'kr'),(442,'Gun shop',NULL,'en'),(442,'총포판매소',NULL,'kr'),(443,'Paper hanger store',NULL,'en'),(443,'표구점',NULL,'kr'),(444,'Entertainment bar',NULL,'en'),(444,'유흥주점',NULL,'kr'),(445,'Mall',NULL,'en'),(445,'대형마트',NULL,'kr'),(446,'Market',NULL,'en'),(446,'시장',NULL,'kr'),(447,'National government building',NULL,'en'),(447,'국가청사',NULL,'kr'),(448,'Military hospital',NULL,'en'),(448,'군병원',NULL,'kr'),(449,'Embassy',NULL,'en'),(449,'대사관',NULL,'kr'),(450,'School / Training center',NULL,'en'),(450,'학교및훈련소',NULL,'kr'),(451,'Foreigner protection facility',NULL,'en'),(451,'외국인보호시설',NULL,'kr'),(452,'Treatment and supervision facility',NULL,'en'),(452,'치료감호시설',NULL,'kr'),(453,'Highway National Highway Tunnel',NULL,'en'),(453,'고속국도터널',NULL,'kr'),(454,'National Highway Tunnel',NULL,'en'),(454,'국도터널',NULL,'kr'),(455,'Others (underground tunnel)',NULL,'en'),(455,'기타(지하가)',NULL,'kr'),(456,'Local road tunnel',NULL,'en'),(456,'지방도터널',NULL,'kr'),(457,'Underground shopping mall',NULL,'en'),(457,'지하상가',NULL,'kr'),(458,'Railway tunnel',NULL,'en'),(458,'철도터널',NULL,'kr'),(459,'Public  underground artificial structure',NULL,'en'),(459,'공동구',NULL,'kr'),(460,'Etc(Underground artificial structure)',NULL,'en'),(460,'기타(지하구)',NULL,'kr'),(461,'Single underground artificial structure',NULL,'en'),(461,'단독구 기타',NULL,'kr'),(462,'Underground artificial structure for heating',NULL,'en'),(462,'단독구 난방',NULL,'kr'),(463,'Underground artificial structure for power',NULL,'en'),(463,'단독구 전력',NULL,'kr'),(464,'Underground artificial structure for telecom',NULL,'en'),(464,'단독구 통신',NULL,'kr'),(465,'Thermal power plant',NULL,'en'),(465,'화력발전소',NULL,'kr'),(466,'Wind power plant',NULL,'en'),(466,'풍력발전소',NULL,'kr'),(467,'Hydroelectric power plant',NULL,'en'),(467,'수력발전소',NULL,'kr'),(468,'Nuclear power plant',NULL,'en'),(468,'원자력발전소',NULL,'kr'),(469,'Collective energy supply facility',NULL,'en'),(469,'집단에너지공급시설',NULL,'kr'),(470,'Solar power plant',NULL,'en'),(470,'태양광 발전소',NULL,'kr'),(471,'Others (power generation facilities)',NULL,'en'),(471,'기타(발전시설)',NULL,'kr'),(472,'Cemetery',NULL,'en'),(472,'묘지부수건축물',NULL,'kr'),(473,'Crematorium',NULL,'en'),(473,'화장장',NULL,'kr'),(474,'Others (cemetery related facilities)',NULL,'en'),(474,'기타(묘지관련시설)',NULL,'kr'),(475,'National administrative agency','','en'),(475,'국가행정기관','','kr'),(476,'National and public high schools','','en'),(476,'국공립 고등학교','','kr'),(477,'National and Public Public Schools','','en'),(477,'국공립 공민학교','','kr'),(478,'National and public universities','','en'),(478,'국공립 대학','','kr'),(479,'National and public kindergartens','','en'),(479,'국공립 유치원','','kr'),(480,'National and public middle schools','','en'),(480,'국공립 중학교','','kr'),(481,'National and public elementary schools','','en'),(481,'국공립 초등학교','','kr'),(482,'Private high school','','en'),(482,'사립 고등학교','','kr'),(483,'Private civic school','','en'),(483,'사립 공민학교','','kr'),(484,'Private university','','en'),(484,'사립 대학','','kr'),(485,'Private kindergarten','','en'),(485,'사립 유치원','','kr'),(486,'Private junior high school','','en'),(486,'사립 중학교','','kr'),(487,'Private elementary school','','en'),(487,'사립 초등학교','','kr'),(488,'Government Investment Institution','','en'),(488,'정부투자기관','','kr'),(489,'Local corporations and local industrial complexes','','en'),(489,'지방공사및지방공단','','kr'),(490,'Local government','','en'),(490,'지방자치단체','','kr'),(491,'Special administrative agency','','en'),(491,'특별행정기관','','kr'),(492,'Drying room','','en'),(492,'건조실','','kr'),(493,'Windowless floor','','en'),(493,'무창층','','kr'),(494,'Disaster Prevention Center','','en'),(494,'방재센타','','kr'),(495,'Distribution/Substation Room','','en'),(495,'배전/변전실','','kr'),(496,'Boiler room','','en'),(496,'보일러실','','kr'),(497,'Kitchen/dining room','','en'),(497,'주방/식당','','kr'),(498,'Garage/parking lot','','en'),(498,'차고/주차장','','kr'),(499,'Communication equipment room','','en'),(499,'통신기기실','','kr'),(500,'Others (for special tools)','','en'),(500,'기타(특수용도구분)','','kr'),(501,'Gas handling','','en'),(501,'가스취급','','kr'),(502,'danger goods','','en'),(502,'위험물','','kr'),(503,'Special combustibles','','en'),(503,'특수가연물','','kr'),(504,'Other (dangerous goods classification)','','en'),(504,'기타(위험물구분)','','kr'),(505,'Level 1','','en'),(505,'1급','','kr'),(506,'Level 2','','en'),(506,'2급','','kr'),(507,'General object','','en'),(507,'일반대상물','','kr'),(508,'Other objects (temporary buildings)','','en'),(508,'기타대상물(가설건축물)','','kr'),(509,'Other objects (residential vinyl house)','','en'),(509,'기타대상물(주거형비닐하우스)','','kr'),(510,'Simple fire extinguishing equipment','','en'),(510,'간이소화용구','','kr'),(511,'Simple Spring Cooler','','en'),(511,'간이스프링쿨러','','kr'),(512,'Power fire pump','','en'),(512,'동력소방펌프','','kr'),(513,'Water spray fire extinguishing equipment AV','','en'),(513,'물분무소화설비AV','','kr'),(514,'Water spray fire extinguishing system H','','en'),(514,'물분무소화설비H','','kr'),(515,'Powder fire extinguishing AV','','en'),(515,'분말소화AV','','kr'),(516,'Powder fire extinguishing H','','en'),(516,'분말소화H','','kr'),(517,'Manual fire extinguisher','','en'),(517,'수동식소화기','','kr'),(518,'Sprinkler AV','','en'),(518,'스프링쿨러AV','','kr'),(519,'Sprinkler H','','en'),(519,'스프링쿨러H','','kr'),(520,'Indoor fire hydrant','','en'),(520,'옥내소화전','','kr'),(521,'Outdoor fire hydrant','','en'),(521,'옥외소화전','','kr'),(522,'Carbon dioxide AV','','en'),(522,'이산화탄소AV','','kr'),(523,'Carbon dioxide H','','en'),(523,'이산화탄소H','','kr'),(524,'Automatic fire extinguisher','','en'),(524,'자동식소화기','','kr'),(525,'Fossilized AV','','en'),(525,'포소화AV','','kr'),(526,'Fosification H','','en'),(526,'포소화H','','kr'),(527,'Halogen compound AV','','en'),(527,'할로겐화합물AV','','kr'),(528,'Halogen compound H','','en'),(528,'할로겐화합물H','','kr'),(529,'Gas leak alarm','','en'),(529,'가스누설경보기','','kr'),(530,'Earth leakage alarm','','en'),(530,'누전경보기','','kr'),(531,'Emergency alarm','','en'),(531,'비상경보','','kr'),(532,'Emergency broadcast','','en'),(532,'비상방송','','kr'),(533,'Automated Breaking News','','en'),(533,'자동화재속보','','kr'),(534,'Automated Fire Detector (Sense)','','en'),(534,'자동화재탐지기(감)','','kr'),(535,'Automated Fire Detector (times)','','en'),(535,'자동화재탐지기(회)','','kr'),(536,'Liver Lean Strength','','en'),(536,'간이완강기','','kr'),(537,'Audience guidance, etc.','','en'),(537,'객석유도등','','kr'),(538,'Air safety mat','','en'),(538,'공기안전매트','','kr'),(539,'Air respirator','','en'),(539,'공기호흡기','','kr'),(540,'Rescue team','','en'),(540,'구조대','','kr'),(541,'Others (evacuation facilities)','','en'),(541,'기타(피난설비)','','kr'),(542,'slide','','en'),(542,'미끄럼대','','kr'),(543,'Heat shield','','en'),(543,'방열복','','kr'),(544,'Emergency lighting','','en'),(544,'비상조명등','','kr'),(545,'Stubbornness','','en'),(545,'완강기','','kr'),(546,'Guidance light','','en'),(546,'유도등','','kr'),(547,'Guidance signs','','en'),(547,'유도표지','','kr'),(548,'Artificial resuscitation','','en'),(548,'인공소생기','','kr'),(549,'Aisle guide light','','en'),(549,'통로유도등','','kr'),(550,'Refuge','','en'),(550,'피난교','','kr'),(551,'Evacuation exit guidance, etc.','','en'),(551,'피난구유도등','','kr'),(552,'Evacuation rope','','en'),(552,'피난밧줄','','kr'),(553,'Evacuation ladder','','en'),(553,'피난사다리','','kr'),(554,'Evacuation trap','','en'),(554,'피난용트랩','','kr'),(555,'Portable emergency lighting','','en'),(555,'휴대용비상조명등','','kr'),(556,'Water for fire extinguishing','','en'),(556,'상수도소화용수','','kr'),(557,'Digestion tank','','en'),(557,'소화수조','','kr'),(558,'Others (fire water)','','en'),(558,'기타(소화용수)','','kr'),(559,'Wireless communication assistant','','en'),(559,'무선통신보조','','kr'),(560,'Emergency outlet','','en'),(560,'비상콘센트','','kr'),(561,'Spray water connection ','','en'),(561,'연결살수','','kr'),(562,'Connecting water pipe','','en'),(562,'연결송수관','','kr'),(563,'Combustion prevention facility','','en'),(563,'연소방지설비','','kr'),(564,'Ventilation equipment','','en'),(564,'제연설비','','kr'),(565,'carpet','','en'),(565,'카페트','','kr'),(566,'curtain','','en'),(566,'커튼','','kr'),(567,'Other (flame retardant)','','en'),(567,'기타(방염)','','kr'),(568,'More than 30 floors (including basement)','','en'),(568,'30층 이상(지하포함)','','kr'),(569,'More than 120m in height','','en'),(569,'높이 120m 이상','','kr'),(570,'Total floor area of ??200,000㎡ or more','','en'),(570,'연면적 20만㎡ 이상','','kr'),(571,'11th floor and above','','en'),(571,'11층이상','','kr'),(572,'More than 1,000 tons of combustible gas','','en'),(572,'가연성가스 1천톤이상','','kr'),(573,'Total floor area of ??15,000 or more','','en'),(573,'연면적 15,000 이상','','kr'),(574,'Less than 1,000 tons of combustible gas','','en'),(574,'가연성가스1천톤미만','','kr'),(575,'Mobilization agency','','en'),(575,'동원기관인','','kr'),(576,'Honorary firefighter/medical fire brigade','','en'),(576,'명예소방관/의용소방대','','kr'),(577,'Civil defense','','en'),(577,'민방위','','kr'),(578,'firefighter','','en'),(578,'소방관','','kr'),(579,'Fire safety manager/self-defense fire brigade','','en'),(579,'소방안전관리자/자위소방대','','kr'),(580,'Installation target for sprinkler, water spray, etc.','','en'),(580,'스프링클러,물분무등설치대상','','kr'),(581,'Indoor fire hydrant installation target','','en'),(581,'옥내소화전설치대상','','kr'),(582,'Automatic re-detection and installation target','','en'),(582,'자동화재탐지설치대상','','kr'),(583,'citizen','','en'),(583,'주민','','kr'),(584,'Underground tunel','','en'),(584,'지하구','','kr'),(585,'General object','','en'),(585,'일반대상물','','kr'),(586,'Merrymaking liquor store','','en'),(586,'윤락업소','','kr'),(587,'Residential Green House','','en'),(587,'주거용비닐하우스','','kr'),(588,'Side room','','en'),(588,'쪽방','','kr'),(589,'Container House','','en'),(589,'콘테이너하우스','','kr'),(590,'Scrapped car','','en'),(590,'폐기차','','kr'),(591,'A waste plane','','en'),(591,'폐비행기','','kr'),(592,'Abandoned ship','','en'),(592,'폐선박','','kr'),(593,'Others (classification of residential vinyl houses)','','en'),(593,'기타(주거용비닐하우스분류)','','kr'),(594,'Temporary Exhibition Hall','','en'),(594,'가설전람회장','','kr'),(595,'Temporary store','','en'),(595,'가설점포','','kr'),(596,'A temporary showroom','','en'),(596,'가설흥행장','','kr'),(597,'Sample house (model house)','','en'),(597,'견본주택(모델하우스)','','kr'),(598,'Agricultural and industrial complex','','en'),(598,'농공단지','','kr'),(599,'Agricultural fixed greenhouse','','en'),(599,'농업용고정식온실','','kr'),(600,'Temporary Office, Warehouse, Accommodation','','en'),(600,'임시사무실,창고,숙소','','kr'),(601,'Others (temporary building classification)','','en'),(601,'기타(가설건축물분류)','','kr'),(602,'Pensions with more than 7 rooms','','en'),(602,'펜션7실이상','','kr'),(603,'Pension with 7 rooms or less','','en'),(603,'펜션7실이하','','kr'),(604,'Guesthouse with 7 rooms or more','','en'),(604,'민박7실이상','','kr'),(605,'Guesthouses with Less than 7 rooms','','en'),(605,'민박7실이하','','kr'),(606,'Gas leak (explosion)','','en'),(606,'가스누출(폭발)','','kr'),(607,'Traffic Accident','','en'),(607,'교통사고','','kr'),(608,'Mechanical factor','','en'),(608,'기계적요인','','kr'),(609,'Others (fire occurrence and ignition factor)','','en'),(609,'기타(화재발생발화요인)','','kr'),(610,'carelessness','','en'),(610,'부주의','','kr'),(611,'Natural factors','','en'),(611,'자연적요인','','kr'),(612,'Electrical factor','','en'),(612,'전기적요인','','kr'),(613,'Chemical factor','','en'),(613,'화학적요인','','kr'),(614,'Unknown','','en'),(614,'미상','','kr'),(615,'Cultural Heritage','','en'),(615,'문화재','','kr'),(616,'Express grade target','Express grade target','en'),(616,'특급대상','특급대상','kr'),(618,'Level 1 target','Level 1 target','en'),(618,'1급대상','1급대상','kr'),(620,'Level 2 target','Level 2 target','en'),(620,'2급대상','2급대상','kr'),(622,'General target','General target','en'),(622,'일반대상','일반대상','kr'),(624,'Others-Residential Vinyl House','Others-Residential Vinyl House','en'),(624,'기타-주거용비닐하우스','기타-주거용비닐하우스','kr'),(626,'Other-temporary building','Other-temporary building','en'),(626,'기타-가설건축물','기타-가설건축물','kr'),(628,'Other-Pension','Other-Pension','en'),(628,'기타-팬션','기타-팬션','kr'),(630,'Others-Guest House','Others-Guest House','en'),(630,'기타-민박','기타-민박','kr'),(632,'Cultural Heritage','Cultural Heritage','en'),(632,'문화재','문화재','kr'),(634,'Public institution','Public institution','en'),(634,'공공기관','공공기관','kr'),(635,'Neighborhood life','','en'),(635,'근린생활','','kr'),(636,'Recreational facility','','en'),(636,'위락시설','','kr'),(637,'Cultural assembly and sports facility','','en'),(637,'문화집회및 운동시설','','kr'),(638,'Sales and sales facility','','en'),(638,'판매시설및 영업시설','','kr'),(639,'Accommodation','','en'),(639,'숙박시설','','kr'),(640,'Elderly people facility','','en'),(640,'노유자시설','','kr'),(641,'Medical facility','','en'),(641,'의료시설','','kr'),(642,'Apartment house (apartment/dormitory)','','en'),(642,'공동주택(아파트/기숙사)','','kr'),(643,'Business facility','','en'),(643,'업무시설','','kr'),(644,'Communication filming facilty','','en'),(644,'통신촬영시설','','kr'),(645,'Educational Research Facility','','en'),(645,'교육연구시설','','kr'),(646,'Factory','','en'),(646,'공장','','kr'),(647,'Warehouse facility','','en'),(647,'창고시설','','kr'),(648,'Transportation vehicle related facility','','en'),(648,'운수자동차 관련시설','','kr'),(649,'Tourist rest facility','','en'),(649,'관광휴게시설','','kr'),(650,'Animal and plant related facility','','en'),(650,'동식물관련시설','','kr'),(651,'Sanitation related facilities','','en'),(651,'위생등관련시설','','kr'),(652,'Correctional facility','','en'),(652,'교정시설','','kr'),(653,'Dangerous goods storage and treatment facility','','en'),(653,'위험물저장및 처리시설','','kr'),(654,'Underground tunnel','','en'),(654,'지하가','','kr'),(655,'Underground artificial structure','','en'),(655,'지하구','','kr'),(656,'Cultural Heritage','','en'),(656,'문화재','','kr'),(657,'Complex building','','en'),(657,'복합건축물','','kr'),(658,'Religious facility','','en'),(658,'종교시설','','kr'),(659,'Training facility','','en'),(659,'수련시설','','kr'),(660,'Exercise facility','','en'),(660,'운동시설','','kr'),(661,'Aircraft and automobile related facility','','en'),(661,'항공기및자동차관련시설','','kr'),(662,'Power generation facility','','en'),(662,'발전시설','','kr'),(663,'Cemetery related facility','','en'),(663,'묘지관련시설','','kr'),(664,'Funeral','','en'),(664,'장례식장','','kr'),(665,'Framed Structure','','en'),(665,'가구식구조','','kr'),(666,'Masonry Structure','','en'),(666,'조적식구조','','kr'),(667,'Monolithic Structure','','en'),(667,'일체식구조','','kr'),(668,'Special Structure','','en'),(668,'특수구조','','kr'),(669,'Simple woodwork','','en'),(669,'간이목조','','kr'),(670,'Simple steel frame pipe','','en'),(670,'간이철골쇠파이프조','','kr'),(671,'woodcarving','','en'),(671,'목조','','kr'),(672,'Brickwork','','en'),(672,'벽돌조','','kr'),(673,'Block','','en'),(673,'블럭조','','kr'),(674,'Stonework','','en'),(674,'석조','','kr'),(675,'Cement brick','','en'),(675,'시멘트벽돌조','','kr'),(676,'Cement block','','en'),(676,'시멘트블럭조','','kr'),(677,'Steel frame','','en'),(677,'철골조','','kr'),(678,'Steel frame reinforced concrete construction','','en'),(678,'철골철근콘크리트조','','kr'),(679,'Steel concrete structure','','en'),(679,'철골콘크리트조','','kr'),(680,'Reinforced concrete','','en'),(680,'철근콘크리트조','','kr'),(681,'Barbed wire','','en'),(681,'철조','','kr'),(682,'Stucco brickwork','','en'),(682,'치장벽돌조','','kr'),(683,'Sandwich panel','','en'),(683,'샌드위치판넬','','kr'),(684,'Threthga','','en'),(684,'스레트가','','kr'),(685,'Slavs','','en'),(685,'슬라브가','','kr'),(686,'Cement tile','','en'),(686,'시멘트기와','','kr'),(687,'Color coated iron plate','','en'),(687,'칼라피복철판','','kr'),(688,'Renovation',NULL,'en'),(688,'개축',NULL,'kr'),(689,'Major repair',NULL,'en'),(689,'대수선',NULL,'kr'),(690,'New construction',NULL,'en'),(690,'신축',NULL,'kr'),(691,'Change of use',NULL,'en'),(691,'용도변경',NULL,'kr'),(692,'Rebuild',NULL,'en'),(692,'재축',NULL,'kr'),(693,'Extension',NULL,'en'),(693,'증축',NULL,'kr'),(694,'Others (Architecture History Code)',NULL,'en'),(694,'기타(건축연혁코드)',NULL,'kr'),(695,'Manager',NULL,'en'),(695,'관리자',NULL,'kr'),(696,'User',NULL,'en'),(696,'사용자',NULL,'kr'),(697,'Fire safety manager',NULL,'en'),(697,'소방안전관리자',NULL,'kr'),(698,'Owner',NULL,'en'),(698,'소유자',NULL,'kr'),(699,'Dangerous Goods Safety Manager',NULL,'en'),(699,'위험물안전관리자',NULL,'kr'),(700,'Occupant',NULL,'en'),(700,'점유자',NULL,'kr'),(701,'Building photo',NULL,'en'),(701,'건물사진',NULL,'kr'),(702,'Location map',NULL,'en'),(702,'위치도',NULL,'kr'),(703,'view map',NULL,'en'),(703,'조감도',NULL,'kr'),(704,'Fire suppression degree',NULL,'en'),(704,'화재진압도',NULL,'kr'),(705,'Probationary Firefighter','','en'),(705,'소방사시보','','kr'),(706,'Paramedic','','en'),(706,'소방사','','kr'),(707,'Firefighter','','en'),(707,'소방교','','kr'),(708,'Lieutenant','','en'),(708,'소방장','','kr'),(709,'EMS Captain','','en'),(709,'소방위','','kr'),(710,'Battalion Chief','','en'),(710,'소방경','','kr'),(711,'Deputy Chief','','en'),(711,'소방령','','kr'),(712,'Division Chief','','en'),(712,'소방정','','kr'),(713,'Deputy Assistant Chief','','en'),(713,'소방준감','','kr'),(714,'Assistant Chief','','en'),(714,'소방감','','kr'),(715,'Chief of Fire Operations','','en'),(715,'소방정감','','kr'),(716,'Chief of Department','','en'),(716,'소방총감','','kr'),(717,'Chief of fire office','','en'),(717,'소방서장','','kr'),(718,'Nurse','','en'),(718,'간호사','','kr'),(719,'Nursing assistant','','en'),(719,'간호조무','','kr'),(720,'Consensus','','en'),(720,'감식','','kr'),(721,'Inspection','','en'),(721,'감찰','','kr'),(722,'Inspector','','en'),(722,'감찰담당','','kr'),(723,'deck','','en'),(723,'갑판','','kr'),(724,'Construct','','en'),(724,'건축','','kr'),(725,'Construction electricity','','en'),(725,'건축전기','','kr'),(726,'Inspection map','','en'),(726,'검사지도','','kr'),(727,'Accounting Manager','','en'),(727,'경리담당','','kr'),(728,'Account Manager','','en'),(728,'경리주임','','kr'),(729,'Chief of police officers','','en'),(729,'경방반장','','kr'),(730,'Head of Security','','en'),(730,'경호주임','','kr'),(731,'Management','','en'),(731,'관리','','kr'),(732,'Instructor','','en'),(732,'교관','','kr'),(733,'Academic Affairs Officer','','en'),(733,'교무담당','','kr'),(734,'Professor','','en'),(734,'교수담당','','kr'),(735,'Education and security','','en'),(735,'교육.보안','','kr'),(736,'Education plan','','en'),(736,'교육계획','','kr'),(737,'Education management','','en'),(737,'교육관리','','kr'),(738,'Trainee Management','','en'),(738,'교육생관리','','kr'),(739,'Education screening','','en'),(739,'교육전형','','kr'),(740,'Education progress','','en'),(740,'교육진행','','kr'),(741,'Education training','','en'),(741,'교육훈련','','kr'),(742,'Textbook Management','','en'),(742,'교재관리','','kr'),(743,'Head of the department','','en'),(743,'교학과장','','kr'),(744,'Exchange','','en'),(744,'교환','','kr'),(745,'Emergency plan','','en'),(745,'구급계획','','kr'),(746,'First aid','','en'),(746,'구급담당','','kr'),(747,'Paramedic Management','','en'),(747,'구급대관리','','kr'),(748,'Emergency operation','','en'),(748,'구급운영','','kr'),(749,'First aid guidance','','en'),(749,'구급지도','','kr'),(750,'Rescue','','en'),(750,'구조','','kr'),(751,'Rescue management','','en'),(751,'구조관리','','kr'),(752,'Rescue First Aid Section Chief','','en'),(752,'구조구급과장','','kr'),(753,'Head of Rescue First Aid Training Center','','en'),(753,'구조구급훈련센타소장','','kr'),(754,'Rescue Manager','','en'),(754,'구조담당','','kr'),(755,'Rescue captain','','en'),(755,'구조대장','','kr'),(756,'Rescue Team Leader','','en'),(756,'구조반장','','kr'),(757,'Rescue operation','','en'),(757,'구조운영','','kr'),(758,'Rescue guider','','en'),(758,'구조지도','','kr'),(759,'Salary computation','','en'),(759,'급여전산','','kr'),(760,'Machine','','en'),(760,'기계','','kr'),(761,'Agency','','en'),(761,'기관','','kr'),(762,'Planning and budget','','en'),(762,'기획.예산','','kr'),(763,'Planning Manager','','en'),(763,'기획담당','','kr'),(764,'Planning Greetings','','en'),(764,'기획인사','','kr'),(765,'Heating','','en'),(765,'난방','','kr'),(766,'Purchase of goods','','en'),(766,'물품구매','','kr'),(767,'Protection (guard)','','en'),(767,'방호(경비)','','kr'),(768,'Protection Manager','','en'),(768,'방호과장','','kr'),(769,'Director of Protection Structure','','en'),(769,'방호구조과장','','kr'),(770,'Protection Officer','','en'),(770,'방호담당','','kr'),(771,'Protection team leader','','en'),(771,'방호반장','','kr'),(772,'Protection chief','','en'),(772,'방호주임','','kr'),(773,'Press photo','','en'),(773,'보도사진','','kr'),(774,'Press promotion','','en'),(774,'보도추진','','kr'),(775,'Volunteer room','','en'),(775,'봉사실','','kr'),(776,'Commander','','en'),(776,'부대장','','kr'),(777,'Subsidiary','','en'),(777,'부속담당','','kr'),(778,'Deputy chief','','en'),(778,'부제대장','','kr'),(779,'Analysis system','','en'),(779,'분석제도','','kr'),(780,'breed','','en'),(780,'사육','','kr'),(781,'boy','','en'),(781,'사환','','kr'),(782,'Situation management','','en'),(782,'상황관리','','kr'),(783,'Situation Chief','','en'),(783,'상황주임','','kr'),(784,'Life guidance','','en'),(784,'생활지도','','kr'),(785,'General manager','','en'),(785,'서무과장','','kr'),(786,'General Affairs Officer','','en'),(786,'서무담당','','kr'),(787,'Fire inspection','','en'),(787,'소방검사','','kr'),(788,'Fire department chief','','en'),(788,'소방과장','','kr'),(789,'Firefighting Planning Officer','','en'),(789,'소방기획담당','','kr'),(790,'Fire Department','','en'),(790,'소방담당','','kr'),(791,'Fire Department Head','','en'),(791,'소방본부장','','kr'),(792,'Fire chief','','en'),(792,'소방서장','','kr'),(793,'Firefighting facilities','','en'),(793,'소방시설','','kr'),(794,'Fire fighting water','','en'),(794,'소방용수','','kr'),(795,'Fire brigade chief','','en'),(795,'소방정대장','','kr'),(796,'Fire Chief','','en'),(796,'소방주임','','kr'),(797,'Fire Principal','','en'),(797,'소방학교장','','kr'),(798,'Fire Department Administration Manager','','en'),(798,'소방행정과장','','kr'),(799,'Fire Administration Officer','','en'),(799,'소방행정담당','','kr'),(800,'Water structure','','en'),(800,'수상구조','','kr'),(801,'Water rescue leader','','en'),(801,'수상구조대장','','kr'),(802,'Facility, flame retardant','','en'),(802,'시설.방염','','kr'),(803,'Facility manager','','en'),(803,'시설주임','','kr'),(804,'Facility guide','','en'),(804,'시설지도','','kr'),(805,'Facility promotion','','en'),(805,'시설촉진','','kr'),(806,'Safety','','en'),(806,'안전','','kr'),(807,'Nutritionist','','en'),(807,'영양사','','kr'),(808,'prevention','','en'),(808,'예방','','kr'),(809,'Prevention Manager','','en'),(809,'예방과장','','kr'),(810,'Preventive management','','en'),(810,'예방관리','','kr'),(811,'Prevention','','en'),(811,'예방담당','','kr'),(812,'Prevention General','','en'),(812,'예방일반','','kr'),(813,'Budget, accounting','','en'),(813,'예산.경리','','kr'),(814,'Budget operation','','en'),(814,'예산운영','','kr'),(815,'Budget execution and settlement','','en'),(815,'예산집행.결산','','kr'),(816,'Water platoon management','','en'),(816,'용수의소대관리','','kr'),(817,'operation','','en'),(817,'운영','','kr'),(818,'driving','','en'),(818,'운전','','kr'),(819,'Driver','','en'),(819,'운전반장','','kr'),(820,'Horticulture','','en'),(820,'원예','','kr'),(821,'Cause investigation','','en'),(821,'원인조사','','kr'),(822,'Hygiene ministry','','en'),(822,'위생사역','','kr'),(823,'Danger goods','','en'),(823,'위험물','','kr'),(824,'Dangerous Goods Safety Officer','','en'),(824,'위험물안전담당','','kr'),(825,'Doctor','','en'),(825,'의사','','kr'),(826,'Operation of medical contest','','en'),(826,'의소대전시운영','','kr'),(827,'Council operation','','en'),(827,'의회운영','','kr'),(828,'General equipment','','en'),(828,'일반장비','','kr'),(829,'Daily expenses','','en'),(829,'일상경비','','kr'),(830,'Equipment management','','en'),(830,'장비관리','','kr'),(831,'Equipment purchase','','en'),(831,'장비구매','','kr'),(832,'Equipment in charge','','en'),(832,'장비담당','','kr'),(833,'Property management','','en'),(833,'재산관리','','kr'),(834,'Electricity','','en'),(834,'전기','','kr'),(835,'Computational','','en'),(835,'전산','','kr'),(836,'Computer development','','en'),(836,'전산개발','','kr'),(837,'Computer education','','en'),(837,'전산교육','','kr'),(838,'Computerized benefit processing','','en'),(838,'전산급여처리','','kr'),(839,'Computational Planning','','en'),(839,'전산기획','','kr'),(840,'Computing Manager','','en'),(840,'전산담당','','kr'),(841,'Computerized operation','','en'),(841,'전산운용','','kr'),(842,'Full-time instructor','','en'),(842,'전임교관','','kr'),(843,'Mechanic','','en'),(843,'정비사','','kr'),(844,'The first officer','','en'),(844,'제1제대장','','kr'),(845,'The second chief','','en'),(845,'제2제대장','','kr'),(846,'3rd chieftain','','en'),(846,'제3제대장','','kr'),(847,'Manufacturing facility business','','en'),(847,'제조설비업','','kr'),(848,'Assistant','','en'),(848,'조교','','kr'),(849,'Research','','en'),(849,'조사','','kr'),(850,'Survey guide','','en'),(850,'조사지도','','kr'),(851,'Survey statistics','','en'),(851,'조사통계','','kr'),(852,'Pilot','','en'),(852,'조종사','','kr'),(853,'Organization','','en'),(853,'조직시정','','kr'),(854,'Parking control','','en'),(854,'주차단속','','kr'),(855,'In charge of guidance','','en'),(855,'지도담당','','kr'),(856,'Command Officer','','en'),(856,'지령실장','','kr'),(857,'Salary expenditure','','en'),(857,'지출봉급','','kr'),(858,'In charge of suppression 1','','en'),(858,'진압1담당','','kr'),(859,'In charge of suppression 2','','en'),(859,'진압2담당','','kr'),(860,'Operation of suppression exhibition','','en'),(860,'진압전시운영','','kr'),(861,'Pick-up','','en'),(861,'집배','','kr'),(862,'Private police','','en'),(862,'청원경찰','','kr'),(863,'Branch manager','','en'),(863,'출장소장','','kr'),(864,'Typer','','en'),(864,'타자','','kr'),(865,'Statistics','','en'),(865,'통계','','kr'),(866,'Statistical analysis','','en'),(866,'통계분석','','kr'),(867,'Communication','','en'),(867,'통신','','kr'),(868,'Communication planning','','en'),(868,'통신기획','','kr'),(869,'Commando','','en'),(869,'특공대','','kr'),(870,'Special Rescue Commander','','en'),(870,'특수구조대장','','kr'),(871,'Commander of Special Rescue Unit','','en'),(871,'특수구조부대장','','kr'),(872,'Special equipment','','en'),(872,'특수장비','','kr'),(873,'Damage investigation','','en'),(873,'피해조사','','kr'),(874,'Handwriting shorthand','','en'),(874,'필기속기','','kr'),(875,'Aircraft technology','','en'),(875,'항공기술','','kr'),(876,'Flight ledger','','en'),(876,'항공대장','','kr'),(877,'Sail','','en'),(877,'항해','','kr'),(878,'Administration','','en'),(878,'행정담당','','kr'),(879,'Executive Director','','en'),(879,'행정반장','','kr'),(880,'Administrative information','','en'),(880,'행정보조','','kr'),(881,'On-site operation','','en'),(881,'현장운영','','kr'),(882,'Promotion','','en'),(882,'홍보','','kr'),(883,'Public relations enlightenment','','en'),(883,'홍보계몽','','kr'),(884,'Public Relations Officer','','en'),(884,'홍보담당','','kr'),(885,'Fire prevention','','en'),(885,'화재예방','','kr'),(886,'Fire investigation','','en'),(886,'화재조사','','kr'),(887,'Fire investigation chief','','en'),(887,'화재조사계장','','kr'),(888,'Welfare','','en'),(888,'후생복지','','kr'),(889,'Cadre','','en'),(889,'간부','','kr'),(890,'Nurse','','en'),(890,'간호사','','kr'),(891,'First aid','','en'),(891,'구급','','kr'),(892,'Rescue','','en'),(892,'구조','','kr'),(893,'Engineer','','en'),(893,'기관사','','kr'),(894,'Technology','','en'),(894,'기술','','kr'),(895,'Leader','','en'),(895,'대장','','kr'),(896,'Nutritionist','','en'),(896,'영양사','','kr'),(897,'Prevention','','en'),(897,'예방','','kr'),(898,'driving','','en'),(898,'운전','','kr'),(899,'Emergency Medical Technician','','en'),(899,'응급구조사','','kr'),(900,'Doctor','','en'),(900,'의사','','kr'),(901,'Computational','','en'),(901,'전산','','kr'),(902,'Mechanic','','en'),(902,'정비사','','kr'),(903,'Adjuster','','en'),(903,'조정사','','kr'),(904,'Private police','','en'),(904,'청원경찰','','kr'),(905,'Communication','','en'),(905,'통신','','kr'),(906,'Mate','','en'),(906,'항해사','','kr'),(907,'Administration','','en'),(907,'행정','','kr'),(908,'Daily work',NULL,'en'),(908,'일근',NULL,'kr'),(909,'Emergency work',NULL,'en'),(909,'비상근무',NULL,'kr'),(910,'Commander',NULL,'en'),(910,'지휘관',NULL,'kr'),(911,'Completion','','en'),(911,'수료','','kr'),(912,'Attending','','en'),(912,'재학','','kr'),(913,'Graduated','','en'),(913,'졸업','','kr'),(914,'Withdraw','','en'),(914,'중퇴','','kr'),(915,'Doctoral Program Completion','','en'),(915,'박사과정수료','','kr'),(916,'Doctoral Program','','en'),(916,'박사과정재학','','kr'),(917,'Doctoral Program Graduation','','en'),(917,'박사과정졸업','','kr'),(918,'Doctoral Program Dropout','','en'),(918,'박사과정중퇴','','kr'),(919,'Master\'s Course Completion','','en'),(919,'석사과정수료','','kr'),(920,'Master\'s course enrollment','','en'),(920,'석사과정재학','','kr'),(921,'Master\'s Course Graduation','','en'),(921,'석사과정졸업','','kr'),(922,'Withdrawal from Master\'s Program','','en'),(922,'석사과정중퇴','','kr'),(923,'Medical workers',NULL,'en'),(923,'의료종사자',NULL,'kr'),(924,'First-aid',NULL,'en'),(924,'구급자격자',NULL,'kr'),(925,'Firefighting Agency Training',NULL,'en'),(925,'소방기관교육',NULL,'kr'),(926,'Other education',NULL,'en'),(926,'그밖의 교육',NULL,'kr'),(927,'On goinging',NULL,'en'),(927,'진행중',NULL,'kr'),(928,'Fire','','en'),(928,'화재','','kr'),(929,'Rescue','','en'),(929,'구조','','kr'),(930,'Forest fire','','en'),(930,'산불','','kr'),(931,'Water accident','','en'),(931,'수난사고','','kr'),(932,'Traffic Accident','','en'),(932,'교통사고','','kr'),(933,'Railroad accident','','en'),(933,'철도사고','','kr'),(934,'Aircraft accident','','en'),(934,'항공기사고','','kr'),(935,'Explosion','','en'),(935,'폭발사고','','kr'),(936,'Tanker Accident','','en'),(936,'유조선사고','','kr'),(937,'Shipwreck accident','','en'),(937,'해난사고','','kr'),(938,'Marine accident','','en'),(938,'해양사고','','kr'),(939,'Mine accident','','en'),(939,'광산사고','','kr'),(940,'Environmental pollution accident','','en'),(940,'환경오염사고','','kr'),(941,'Public Facility Accident','','en'),(941,'공공시설사고','','kr'),(942,'Elevator accident','','en'),(942,'승강기사고','','kr'),(943,'Downpour','','en'),(943,'호우','','kr'),(944,'Typhoon','','en'),(944,'태풍','','kr'),(945,'Heavy snow','','en'),(945,'폭설','','kr'),(946,'Drought','','en'),(946,'가뭄','','kr'),(947,'Earthquake','','en'),(947,'지진','','kr'),(948,'Gas leak (radioactive) accident','','en'),(948,'가스누출(방사성)사고','','kr'),(949,'Mountain climbing accident','','en'),(949,'산악.등반사고','','kr'),(950,'Building and bridge collapse accident','','en'),(950,'건축물.교량붕괴사고','','kr'),(951,'Electric accident','','en'),(951,'전기사고','','kr'),(952,'Other (emergency rescue mobilization type)','','en'),(952,'기타(긴급구조동원유형)','','kr'),(953,'Freight car','','en'),(953,'화물차','','kr'),(954,'Other equipment','','en'),(954,'기타장비','','kr'),(955,'Water tank car','','en'),(955,'물탱크차','','kr'),(956,'Chemical car','','en'),(956,'화학차','','kr'),(957,'High heighted car','','en'),(957,'고가차','','kr'),(958,'Oyster procedure','','en'),(958,'굴절차','','kr'),(959,'Ambulance','','en'),(959,'구급차','','kr'),(960,'Patrol car','','en'),(960,'순찰차','','kr'),(961,'Diagnostic car','','en'),(961,'진단차','','kr'),(962,'Bulldozer','','en'),(962,'불도저','','kr'),(963,'Peyrodah','','en'),(963,'페이로다','','kr'),(964,'Rack car','','en'),(964,'래카차','','kr'),(965,'Crane','','en'),(965,'기중기','','kr'),(966,'Dump truck','','en'),(966,'덤프트럭','','kr'),(967,'Excavators','','en'),(967,'굴삭기','','kr'),(968,'Fork lift','','en'),(968,'지게차','','kr'),(969,'Watering, sprinkler','','en'),(969,'급수.살수차','','kr'),(970,'Bus','','en'),(970,'버스','','kr'),(971,'Aircraft (helicopter)','','en'),(971,'항공기(헬기)','','kr'),(972,'Other cars','','en'),(972,'기타차','','kr'),(973,'Ladder car','','en'),(973,'사다리차','','kr'),(974,'Fire engine','','en'),(974,'소방차','','kr'),(975,'Medium-sized pump car','','en'),(975,'중형펌프차','','kr'),(976,'Small pump car','','en'),(976,'소형펌프차','','kr'),(977,'Firefighting technology qualification recognized for education and career','','en'),(977,'학력.경력인정소방기술자격','','kr'),(978,'Wired Equipment Engineer','','en'),(978,'유선설비기사','','kr'),(979,'Wired Equipment Industry Engineer','','en'),(979,'유선설비산업기사','','kr'),(980,'General Machinery Industry Engineer','','en'),(980,'일반기계산업기사','','kr'),(981,'Dangerous Goods Handling Technician','','en'),(981,'위험물취급기능사','','kr'),(982,'Crane driver','','en'),(982,'기중기운전기능사','','kr'),(983,'Forklift driver','','en'),(983,'지게차운전기능사','','kr'),(984,'Tower crane driver','','en'),(984,'타워크레인운전기능사','','kr'),(985,'Gas welding technician','','en'),(985,'가스용접기능사','','kr'),(986,'Welding craftsman','','en'),(986,'용접기능장','','kr'),(987,'Welding engineer','','en'),(987,'용접기술사','','kr'),(988,'Welding Industry Engineer','','en'),(988,'용접산업기사','','kr'),(989,'Electric welding technician','','en'),(989,'전기용접기능사','','kr'),(990,'Special welding technician','','en'),(990,'특수용접기능사','','kr'),(991,'Excavator driver','','en'),(991,'굴삭기운전기능사','','kr'),(992,'Loader driver','','en'),(992,'로더운전기능사','','kr'),(993,'Motor grader driver','','en'),(993,'모터그레이더운전기능사','','kr'),(994,'Bulldozer driver','','en'),(994,'불도저운전기능사','','kr'),(995,'Gas technician','','en'),(995,'가스기능사','','kr'),(996,'Gas station','','en'),(996,'가스기능장','','kr'),(997,'Gas engineer','','en'),(997,'가스기사','','kr'),(998,'Gas engineer','','en'),(998,'가스기술사','','kr'),(999,'Gas Industry Engineer','','en'),(999,'가스산업기사','','kr'),(1000,'Radio Equipment Industry Engineer','','en'),(1000,'무선설비산업기사','','kr'),(1001,'Firefighting Equipment Engineer (Electric)','','en'),(1001,'소방설비기사(전기)','','kr'),(1002,'Firefighting Equipment Industry Engineer (Electric)','','en'),(1002,'소방설비산업기사(전기)','','kr'),(1003,'Chemical engineer','','en'),(1003,'화공','','kr'),(1004,'Civil engineering','','en'),(1004,'토목','','kr'),(1005,'Diving','','en'),(1005,'잠수','','kr'),(1006,'High pressure gas','','en'),(1006,'고압가스','','kr'),(1007,'Other related qualifications','','en'),(1007,'기타관련자격','','kr'),(1008,'Construction machine equipment engineer','','en'),(1008,'건축기계설비기술사','','kr'),(1009,'Construction equipment engineer','','en'),(1009,'건축설비기사','','kr'),(1010,'Construction equipment industry engineer','','en'),(1010,'건축설비산업기사','','kr'),(1011,'Architectural engineer','','en'),(1011,'건축기사','','kr'),(1012,'Construction Industry Engineer','','en'),(1012,'건축산업기사','','kr'),(1013,'General mechanical engineer','','en'),(1013,'일반기계기사','','kr'),(1014,'Production machinery industry engineer','','en'),(1014,'생산기계산업기사','','kr'),(1015,'Air conditioning refrigeration machine engineer','','en'),(1015,'공조냉동기계기술사','','kr'),(1016,'Air Conditioning Refrigeration Machine Engineer','','en'),(1016,'공조냉동기계기사','','kr'),(1017,'Air Conditioning Refrigeration Machine Industry Engineer','','en'),(1017,'공조냉동기계산업기사','','kr'),(1018,'Boiler handling technician','','en'),(1018,'보일러취급기능사','','kr'),(1019,'Construction machine engineer','','en'),(1019,'건설기계기사','','kr'),(1020,'Construction machinery industry engineer','','en'),(1020,'건설기계산업기사','','kr'),(1021,'Heavy Equipment Engineer','','en'),(1021,'중장비기사','','kr'),(1022,'Mold engineer','','en'),(1022,'금형기술사','','kr'),(1023,'Thermal Management Engineer','','en'),(1023,'열관리기사','','kr'),(1024,'Industrial safety engineer','','en'),(1024,'산업안전기사','','kr'),(1025,'Firefighting equipment engineer','','en'),(1025,'소방설비기술사','','kr'),(1026,'Firefighting equipment engineer (machine)','','en'),(1026,'소방설비기사(기계)','','kr'),(1027,'Firefighting equipment industry engineer (machine)','','en'),(1027,'소방설비산업기사(기계)','','kr'),(1028,'Gas engineer','','en'),(1028,'가스기사','','kr'),(1029,'Gas Industry Engineer','','en'),(1029,'가스산업기사','','kr'),(1030,'Dangerous Goods Management Industry Engineer','','en'),(1030,'위험물관리산업기사','','kr'),(1031,'Dangerous Goods Management Technician','','en'),(1031,'위험물관리기능사','','kr'),(1032,'Electrician','','en'),(1032,'전기기사','','kr'),(1033,'Electrical Industry Engineer','','en'),(1033,'전기산업기사','','kr'),(1034,'Architectural Electrical Equipment Engineer','','en'),(1034,'건축전기설비기술사','','kr'),(1035,'Electrical construction engineer','','en'),(1035,'전기공사기사','','kr'),(1036,'Electrical construction industry engineer','','en'),(1036,'전기공사산업기사','','kr'),(1037,'Electric construction technician','','en'),(1037,'전기공사기능사','','kr'),(1038,'Electronics Industry Engineer','','en'),(1038,'전자산업기사','','kr'),(1039,'Architect','','en'),(1039,'건축사','','kr'),(1040,'Office Automation Industry Engineer','','en'),(1040,'사무자동화산업기사','','kr'),(1041,'Radio communication engineer','','en'),(1041,'전파통신기사','','kr'),(1042,'Radio Communication Industry Engineer','','en'),(1042,'전파통신산업기사','','kr'),(1043,'Firefighting Facility Manager','','en'),(1043,'소방시설관리사','','kr'),(1044,'Emergency Medical Center',NULL,'en'),(1044,'응급의료센터',NULL,'kr'),(1045,'Other facilities',NULL,'en'),(1045,'기타시설',NULL,'kr'),(1046,'No. 1 dispatch',NULL,'en'),(1046,'1호출동',NULL,'kr'),(1047,'No. 2 dispatch',NULL,'en'),(1047,'2호출동',NULL,'kr'),(1048,'No. 3 dispatch',NULL,'en'),(1048,'3호출동',NULL,'kr'),(1049,'Emergency dispatch',NULL,'en'),(1049,'비상출동',NULL,'kr'),(1050,'Rescue No. 1',NULL,'en'),(1050,'구조1호',NULL,'kr'),(1051,'Rescue No. 2',NULL,'en'),(1051,'구조2호',NULL,'kr'),(1052,'Rescue emergency',NULL,'en'),(1052,'구조비상',NULL,'kr'),(1053,'No.1 First aid',NULL,'en'),(1053,'구급1호',NULL,'kr'),(1054,'No.2 First aid',NULL,'en'),(1054,'구급2호\n',NULL,'kr'),(1055,'Emergency first aid\n',NULL,'en'),(1055,'구급비상',NULL,'kr');
/*!40000 ALTER TABLE `sm_cd_resource` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sm_cdgroup_master`
--

DROP TABLE IF EXISTS `sm_cdgroup_master`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sm_cdgroup_master` (
  `cd_grp` varchar(20) NOT NULL COMMENT '코드그룹',
  `upper_cd_grp` varchar(20) DEFAULT NULL COMMENT '상위코드그룹',
  `cd_grp_step` int(11) DEFAULT NULL COMMENT '코드그룹단계',
  `cd_grp_desc` varchar(100) DEFAULT NULL COMMENT '코드그룹설명',
  `use_yn` char(1) DEFAULT '1' COMMENT '사용여부',
  `sort_order` int(11) DEFAULT NULL COMMENT '정렬순서',
  `leaf_yn` char(1) DEFAULT NULL COMMENT 'leaf 여부',
  PRIMARY KEY (`cd_grp`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='그룹코드 master';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sm_cdgroup_master`
--

LOCK TABLES `sm_cdgroup_master` WRITE;
/*!40000 ALTER TABLE `sm_cdgroup_master` DISABLE KEYS */;
INSERT INTO `sm_cdgroup_master` VALUES ('001','G',1,'Emergency rescue type code','1',1,'1'),('002','G',1,'Emergency rescue scale code','1',1,'0'),('002001','002',1,'Fire Scale Code','1',1,'0'),('002001001','002001',2,'General fire scale code','1',1,'1'),('002001002','002001',2,'Emergency fire scale code','1',2,'1'),('002002','002',1,'Rescue Scale Code','1',1,'0'),('002002001','002002',2,'General rescue scale code','1',1,'1'),('002002002','002002',2,'Emergency rescue scale code','1',2,'1'),('002003','002',1,'First Aid Scale Code','1',1,'0'),('002003001','002003',2,'General first-aid scale code','1',1,'1'),('002003002','002003',2,'Emergency first-aid scale code','1',2,'1'),('002004','002',1,'Etc','1',1,'0'),('012','G',1,'Medical institution classification','1',1,'1'),('020000000','G',1,'Fire target object','1',1,'0'),('020010000','020000000',2,'Main use classification code','1',1,'0'),('020010100','020010000',3,'Neighborhood life','1',2,'1'),('020010200','020010000',3,'Recreational facility','1',3,'1'),('020010300','020010000',3,'Cultural assembly and sports facility','1',4,'1'),('020010400','020010000',3,'Sales and sales facility','1',5,'1'),('020010500','020010000',3,'Accommodation','1',6,'1'),('020010600','020010000',3,'Elderly people facility','1',7,'1'),('020010700','020010000',3,'Medical facility','1',8,'1'),('020010800','020010000',3,'Apartment house (apartment/dormitory)','1',9,'1'),('020010900','020010000',3,'Business facility','1',10,'1'),('020011000','020010000',3,'Communication filming facilty','1',11,'1'),('020011100','020010000',3,'Educational Research Facility','1',12,'1'),('020011200','020010000',3,'Factory','1',13,'1'),('020011300','020010000',3,'Warehouse facility','1',14,'1'),('020011400','020010000',3,'Transportation vehicle related facility','1',15,'1'),('020011500','020010000',3,'Tourist rest facility','1',16,'1'),('020011600','020010000',3,'Animal and plant related facility','1',17,'1'),('020011700','020010000',3,'Sanitation related facilities','1',18,'1'),('020011800','020010000',3,'Correctional facility','1',19,'1'),('020011900','020010000',3,'Dangerous goods storage and treatment facility','1',20,'1'),('020012000','020010000',3,'Underground tunnel','1',21,'1'),('020012100','020010000',3,'Underground artificial structure','1',22,'1'),('020012200','020010000',3,'Cultural Heritage','1',23,'1'),('020012300','020010000',3,'Complex building','1',24,'1'),('020012400','020010000',3,'Religious facility','1',25,'1'),('020012500','020010000',3,'Training facility','1',26,'1'),('020012600','020010000',3,'Exercise facility','1',27,'1'),('020012700','020010000',3,'Aircraft and automobile related facility','1',28,'1'),('020012800','020010000',3,'Power generation facility','1',29,'1'),('020012900','020010000',3,'Cemetery related facility','1',30,'1'),('020013000','020010000',3,'Funeral','1',31,'1'),('020030000','020000000',2,'Classification of public institution','1',2,'1'),('020040000','020000000',2,'Special use classifcation','1',3,'1'),('020050000','020000000',2,'Classification of dangerous goods','1',4,'1'),('020070000','020000000',2,'Object classification code','1',6,'1'),('020080000','020000000',2,'Classification of firefighting facility','1',7,'0'),('020080100','020080000',3,'Firefighting facility','1',1,'1'),('020080200','020080000',3,'Alarm equipment','1',2,'1'),('020080300','020080000',3,'Evacuation facility','1',3,'1'),('020080400','020080000',3,'Firefighting water facility','1',4,'1'),('020080500','020080000',3,'Fire extinguishing facility','1',5,'1'),('020080600','020080000',3,'Flame retardant','1',6,'1'),('020090000','020000000',2,'Object standard code','1',1,'0'),('020090050','020090000',3,'Express grade target','1',1,'1'),('020090100','020090000',3,'Level 1 target','1',2,'1'),('020090200','020090000',3,'Level 2 target','1',3,'1'),('020090300','020090000',3,'General target','1',4,'1'),('020090400','020090000',3,'Others-Residential Vinyl House','1',5,'1'),('020090500','020090000',3,'Other-temporary building','1',6,'1'),('020090600','020090000',3,'Other-Pension','1',7,'1'),('020090700','020090000',3,'Others-Guest House','1',8,'1'),('020090800','020090000',3,'Fire occurrence and ignition factor','1',9,'1'),('020090900','020090000',3,'Cultural Heritage','1',10,'1'),('020091000','020090000',3,'Public institution','1',11,'1'),('070','G',1,'Building type','1',1,'1'),('071','G',1,'Building Structure type','1',2,'1'),('072','G',1,'Building Roof type','1',3,'1'),('074','G',1,'Fire management classification code','1',1,'1'),('075','G',2,'Building classification','1',NULL,NULL),('075001','075',2,'Performance facility','1',NULL,NULL),('075002','075',2,'Factory','1',NULL,NULL),('075003','075',2,'Warehouse facility','1',NULL,NULL),('075004','075',2,'Tourist rest facility','1',NULL,NULL),('075005','075',2,'Educational Research Facility','1',NULL,NULL),('075006','075',2,'Elderly people facilities','1',NULL,NULL),('075007','075',2,'Animal and plant related facilities','1',NULL,NULL),('075008','075',2,'Power generation facilities','1',NULL,NULL),('075009','075',2,'Cultural Heritage','1',NULL,NULL),('075010','075',2,'Accommodation','1',NULL,NULL),('075011','075',2,'Business facility','1',NULL,NULL),('075012','075',2,'Station, terminal','1',NULL,NULL),('075013','075',2,'Transportation vehicle related facility','1',NULL,NULL),('075014','075',2,'Recreational facility','1',NULL,NULL),('075015','075',2,'Related facilities such as sanitation','1',NULL,NULL),('075016','075',2,'Medical facility','1',NULL,NULL),('075017','075',2,'Exhibition, park (including amusement facilities), sports facility','1',NULL,NULL),('075018','075',2,'Religious facility','1',NULL,NULL),('075019','075',2,'House','1',NULL,NULL),('075020','075',2,'Underground facility (underground buried facility)','1',NULL,NULL),('075021','075',2,'Youth Training Facility','1',NULL,NULL),('075022','075',2,'Communication filming facility','1',NULL,NULL),('075023','075',2,'Sales facility','1',NULL,NULL),('075024','075',2,'Dangerous goods storage and treatment facility','1',NULL,NULL),('075025','075',2,'Temporary building','1',NULL,NULL),('075026','075',2,'Complex building','1',NULL,NULL),('081','G',1,'Fire fighting power to mobilize','1',1,'1'),('084','G',1,'emergency rescue mobilization type','1',1,'1'),('089','G',1,'Job rank code','1',1,'0'),('089001','089',2,'Fire fighter classification code','1',1,'1'),('090','G',1,'Job title code','1',1,'1'),('091','G',1,'Task code','1',1,'1'),('092','G',1,'Work section code','1',1,'1'),('095','G',1,'Required manpower','1',1,'1'),('145','G',1,'Related organization type code','1',1,'1'),('163','G',1,'Building manager classification code','1',NULL,NULL),('165','G',1,'Building history code','1',1,'1'),('211','G',1,'Fire department type code','1',1,NULL),('211001','211',2,'Headquarter','1',1,'1'),('211002','211',2,'Fire office','1',2,'1'),('211003','211',2,'Department','1',3,'1'),('214','G',1,'Building picture classification code','1',1,'1'),('805','G',1,'Auth classification for using menus. ','1',1,'1'),('841','G',1,'Facility use code','1',1,'0'),('841001','841',1,'Residential facilities code','1',1,'0'),('841001001','841001',2,'House','1',1,'1'),('841001002','841001',2,'Apartment house','1',1,'1'),('841001003','841001',2,'Etc house','1',1,'1'),('841002','841',1,'Educational facilities','1',1,'0'),('841002001','841002',2,'School','1',1,'1'),('941','G',1,'Education Type','1',1,'1'),('956','G',1,'Notification state','1',1,'1'),('957','G',1,'Notification type','1',1,'1'),('958','G',1,'Educational degree','1',1,'1'),('959','G',1,'Education complete state','1',1,'1'),('G',NULL,0,'Group code','1',1,'1');
/*!40000 ALTER TABLE `sm_cdgroup_master` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sm_cdgroup_resource`
--

DROP TABLE IF EXISTS `sm_cdgroup_resource`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sm_cdgroup_resource` (
  `cd_grp` varchar(20) NOT NULL COMMENT '코드그룹',
  `cd_grp_name` varchar(100) DEFAULT NULL COMMENT '코드그룹명',
  `cd_grp_desc` varchar(1000) DEFAULT NULL COMMENT '코드그룹설명',
  `locale` varchar(2) NOT NULL COMMENT '국가코드',
  PRIMARY KEY (`cd_grp`,`locale`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sm_cdgroup_resource`
--

LOCK TABLES `sm_cdgroup_resource` WRITE;
/*!40000 ALTER TABLE `sm_cdgroup_resource` DISABLE KEYS */;
INSERT INTO `sm_cdgroup_resource` VALUES ('001','Emergency rescue type code',NULL,'en'),('001','긴급구조종별코드',NULL,'kr'),('002','Emergency rescue scale code',NULL,'en'),('002','긴급구조규모코드',NULL,'kr'),('002001','Fire Scale Code',NULL,'en'),('002001','화재규모코드',NULL,'kr'),('002001001','General fire scale code',NULL,'en'),('002001001','화재일반규모코드',NULL,'kr'),('002001002','Emergency fire scale code',NULL,'en'),('002001002','화재비상규모코드',NULL,'kr'),('002002','Structure Scale Code',NULL,'en'),('002002','구조규모코드',NULL,'kr'),('002002001','General rescue scale code',NULL,'en'),('002002001','구조일반규모코드',NULL,'kr'),('002002002','Emergency resuce scale code',NULL,'en'),('002002002','구조비상규모코드',NULL,'kr'),('002003','First Aid Scale Code',NULL,'en'),('002003','구급규모코드',NULL,'kr'),('002003001','General first-aid scale code',NULL,'en'),('002003001','구급일반규모코드',NULL,'kr'),('002003002','Eergency first-aid Scale Code',NULL,'en'),('002003002','구급비상규모코드',NULL,'kr'),('002004','Etc',NULL,'en'),('002004','기타',NULL,'kr'),('012','Medical institution classification',NULL,'en'),('012','의료기관구분코드',NULL,'kr'),('020000000','Fire target object','','en'),('020000000','소방대상물','','kr'),('020010000','Main use classification code','','en'),('020010000','주용도분류코드','','kr'),('020010100','Neighborhood life','','en'),('020010100','근린생활','','kr'),('020010200','Recreational facility','','en'),('020010200','위락시설','','kr'),('020010300','Cultural assembly and sports facility','','en'),('020010300','문화집회및 운동시설','','kr'),('020010400','Sales and sales facility','','en'),('020010400','판매시설및 영업시설','','kr'),('020010500','Accommodation','','en'),('020010500','숙박시설','','kr'),('020010600','Elderly people facility','','en'),('020010600','노유자시설','','kr'),('020010700','Medical facility','','en'),('020010700','의료시설','','kr'),('020010800','Apartment house (apartment/dormitory)','','en'),('020010800','공동주택(아파트/기숙사)','','kr'),('020010900','Business facility','','en'),('020010900','업무시설','','kr'),('020011000','Communication filming facilty','','en'),('020011000','통신촬영시설','','kr'),('020011100','Educational Research Facility','','en'),('020011100','교육연구시설','','kr'),('020011200','Factory','','en'),('020011200','공장','','kr'),('020011300','Warehouse facility','','en'),('020011300','창고시설','','kr'),('020011400','Transportation vehicle related facility','','en'),('020011400','운수자동차 관련시설','','kr'),('020011500','Tourist rest facility','','en'),('020011500','관광휴게시설','','kr'),('020011600','Animal and plant related facility','','en'),('020011600','동식물관련시설','','kr'),('020011700','Sanitation related facilities','','en'),('020011700','위생등관련시설','','kr'),('020011800','Correctional facility','','en'),('020011800','교정시설','','kr'),('020011900','Dangerous goods storage and treatment facility','','en'),('020011900','위험물저장및 처리시설','','kr'),('020012000','Underground tunnel','','en'),('020012000','지하가','','kr'),('020012100','Underground artificial structure','','en'),('020012100','지하구','','kr'),('020012200','Cultural Heritage','','en'),('020012200','문화재','','kr'),('020012300','Complex building','','en'),('020012300','복합건축물','','kr'),('020012400','Religious facility','','en'),('020012400','종교시설','','kr'),('020012500','Training facility','','en'),('020012500','수련시설','','kr'),('020012600','Exercise facility','','en'),('020012600','운동시설','','kr'),('020012700','Aircraft and automobile related facility','','en'),('020012700','항공기및자동차관련시설','','kr'),('020012800','Power generation facility','','en'),('020012800','발전시설','','kr'),('020012900','Cemetery related facility','','en'),('020012900','묘지관련시설','','kr'),('020013000','Funeral','','en'),('020013000','장례식장','','kr'),('020030000','Classification of public institution','','en'),('020030000','공공기관분류','','kr'),('020040000','Special use classifcation','','en'),('020040000','특수용도구분','','kr'),('020050000','Classification of dangerous goods','','en'),('020050000','위험물구분','','kr'),('020070000','Object classification code','','en'),('020070000','대상물구분코드','','kr'),('020080000','Classification of firefighting facility','','en'),('020080000','소방시설분류','','kr'),('020080100','Firefighting facility','','en'),('020080100','소방시설','','kr'),('020080200','Alarm equipment','','en'),('020080200','경보설비','','kr'),('020080300','Evacuation facility','','en'),('020080300','피난설비','','kr'),('020080400','Firefighting water facility','','en'),('020080400','소방용수설비','','kr'),('020080500','Fire extinguishing facility','','en'),('020080500','소화활동설비','','kr'),('020080600','Flame retardant','','en'),('020080600','방염','','kr'),('020090000','Object standard code','','en'),('020090000','대상물기준코드','','kr'),('020090050','Express grade target','','en'),('020090050','특급대상','','kr'),('020090100','Level 1 target','','en'),('020090100','1급대상','','kr'),('020090200','Level 2 target','','en'),('020090200','2급대상','','kr'),('020090300','General target','','en'),('020090300','일반대상','','kr'),('020090400','Others-Residential Vinyl House','','en'),('020090400','기타-주거용비닐하우스','','kr'),('020090500','Other-temporary building','','en'),('020090500','기타-가설건축물','','kr'),('020090600','Other-Pension','','en'),('020090600','기타-팬션','','kr'),('020090700','Others-Guest House','','en'),('020090700','기타-민박','','kr'),('020090800','Fire occurrence and ignition factor','','en'),('020090800','화재발생발화요인','','kr'),('020090900','Cultural Heritage','','en'),('020090900','문화재','','kr'),('020091000','Public institution','','en'),('020091000','공공기관','','kr'),('070','Building type',NULL,'en'),('070','건물구조식코드',NULL,'kr'),('071','Building Structure type',NULL,'en'),('071','건물구조조코드',NULL,'kr'),('072','Building Roof type',NULL,'en'),('072','건물구조즙코드',NULL,'kr'),('074','Fire management classification code',NULL,'en'),('074','방화관리대상물구분코드',NULL,'kr'),('075','Building classification',NULL,'en'),('075','대상물구분',NULL,'kr'),('075001','Performance facility',NULL,'en'),('075001','공연시설',NULL,'kr'),('075002','Factory',NULL,'en'),('075002','공장',NULL,'kr'),('075003','Warehouse facility',NULL,'en'),('075003','창고시설',NULL,'kr'),('075004','Tourist rest facility',NULL,'en'),('075004','관광휴게시설',NULL,'kr'),('075005','Educational Research Facility',NULL,'en'),('075005','교육연구시설',NULL,'kr'),('075006','Elderly people facility',NULL,'en'),('075006','노유자시설',NULL,'kr'),('075007','Animal and plant related facility',NULL,'en'),('075007','동식물관련시설',NULL,'kr'),('075008','Power generation facility',NULL,'en'),('075008','발전시설',NULL,'kr'),('075009','Cultural Heritage',NULL,'en'),('075009','문화재',NULL,'kr'),('075010','Accommodation',NULL,'en'),('075010','숙박시설',NULL,'kr'),('075011','Business facility',NULL,'en'),('075011','업무시설',NULL,'kr'),('075012','Station, terminal',NULL,'en'),('075012','역사,터미널',NULL,'kr'),('075013','Transportation vehicle related facility',NULL,'en'),('075013','운수자동차관련시설',NULL,'kr'),('075014','Recreational facility',NULL,'en'),('075014','위락시설',NULL,'kr'),('075015','Related facilities such as sanitation',NULL,'en'),('075015','위생 등 관련시설',NULL,'kr'),('075016','Medical facility',NULL,'en'),('075016','의료시설',NULL,'kr'),('075017','Exhibition, park (including amusement facilities), sports facility',NULL,'en'),('075017','관람전시, 공원(놀이시설 포함), 운동시설',NULL,'kr'),('075018','Religious facility',NULL,'en'),('075018','종교시설',NULL,'kr'),('075019','House',NULL,'en'),('075019','주택',NULL,'kr'),('075020','Underground facility (underground buried facility)',NULL,'en'),('075020','지중시설(지하매설시설)',NULL,'kr'),('075021','Youth Training Facility',NULL,'en'),('075021','청소년수련시설',NULL,'kr'),('075022','Communication filming facility',NULL,'en'),('075022','통신촬영시설',NULL,'kr'),('075023','Sales facility',NULL,'en'),('075023','판매시설',NULL,'kr'),('075024','Dangerous goods storage and treatment facility',NULL,'en'),('075024','위험물저장 및 처리시설',NULL,'kr'),('075025','Temporary building',NULL,'en'),('075025','가설건축물',NULL,'kr'),('075026','Complex building',NULL,'en'),('075026','복합건축물',NULL,'kr'),('081','Mobilization equipment',NULL,'en'),('081','동원장비',NULL,'kr'),('084','emergency rescue mobilization type',NULL,'en'),('084','긴급구조유형',NULL,'kr'),('089','Job rank code',NULL,'en'),('089','계급코드',NULL,'kr'),('089001','Fire fighter classification code',NULL,'en'),('089001','소방공무원 분류코드',NULL,'kr'),('090','Job title code',NULL,'en'),('090','직위코드',NULL,'kr'),('091','Task code',NULL,'en'),('091','직무코드',NULL,'kr'),('092','Work section code',NULL,'en'),('092','근무구분코드',NULL,'kr'),('095','Technical licenses',NULL,'en'),('095','기술자격코드',NULL,'kr'),('145','Related organization type code',NULL,'en'),('145','유관기관구분코드',NULL,'kr'),('163','Building manager classification code',NULL,'en'),('163','대상물관계자',NULL,'kr'),('165','Building history code',NULL,'en'),('165','건축연혁 코드',NULL,'kr'),('211','Fire department type code',NULL,'en'),('211','서소유형구분코드',NULL,'kr'),('211001','Headquarter',NULL,'en'),('211001','본부 및 직할',NULL,'kr'),('211002','Fire office',NULL,'en'),('211002','소방서',NULL,'kr'),('211003','Department',NULL,'en'),('211003','부서',NULL,'kr'),('214','Building picture classification code',NULL,'en'),('214','대상물사진구분코드',NULL,'kr'),('805','Auth classification for using menus. ',NULL,'en'),('805','메뉴사용권한분류',NULL,'kr'),('841','Facility use code',NULL,'en'),('841','시설용도코드',NULL,'kr'),('841001','Residential facilities code',NULL,'en'),('841001','주거시설',NULL,'kr'),('841001001','House',NULL,'en'),('841001001','단독주택',NULL,'kr'),('841001002','Apartment house',NULL,'en'),('841001002','공동주택',NULL,'kr'),('841001003','Etc house',NULL,'en'),('841001003','기타주택',NULL,'kr'),('841002','Educational facilities',NULL,'en'),('841002','교육시설',NULL,'kr'),('841002001','School',NULL,'en'),('841002001','학교',NULL,'kr'),('941','Education Type',NULL,'en'),('941','교육구분코드',NULL,'kr'),('956','Notification state',NULL,'en'),('956','승인요청상태코드',NULL,'kr'),('957','Notification type',NULL,'en'),('957','승인요청종류',NULL,'kr'),('959','Education complete state',NULL,'en'),('959','교육이수여부',NULL,'kr'),('G','Group code',NULL,'en'),('G','그룹코드',NULL,'kr');
/*!40000 ALTER TABLE `sm_cdgroup_resource` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sm_cdgroupcd`
--

DROP TABLE IF EXISTS `sm_cdgroupcd`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sm_cdgroupcd` (
  `cd_grp` varchar(20) NOT NULL COMMENT '코드그룹',
  `cd` int(11) NOT NULL COMMENT '코드',
  `sort_order` int(11) DEFAULT NULL COMMENT '정렬순서',
  `use_yn` char(1) DEFAULT '1' COMMENT '사용여부',
  PRIMARY KEY (`cd_grp`,`cd`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='코드그룹매핑';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sm_cdgroupcd`
--

LOCK TABLES `sm_cdgroupcd` WRITE;
/*!40000 ALTER TABLE `sm_cdgroupcd` DISABLE KEYS */;
INSERT INTO `sm_cdgroupcd` VALUES ('001',7,4,'1'),('001',891,3,'1'),('001',892,2,'1'),('001',928,1,'1'),('002001001',113,1,'1'),('002001001',114,2,'1'),('002001001',115,3,'1'),('002001002',1046,1,'1'),('002001002',1047,2,'1'),('002001002',1048,3,'1'),('002001002',1049,4,'1'),('002002001',113,1,'1'),('002002001',114,2,'1'),('002002002',1050,1,'1'),('002002002',1051,2,'1'),('002002002',1052,3,'1'),('002003001',113,1,'1'),('002003001',114,2,'1'),('002003002',1053,1,'1'),('002003002',1054,2,'1'),('002003002',1055,3,'1'),('002004',113,1,'1'),('002004',114,2,'1'),('012',65,2,'1'),('012',75,4,'1'),('012',283,1,'1'),('012',290,3,'1'),('012',291,5,'1'),('012',294,7,'1'),('012',1044,6,'1'),('012',1045,8,'1'),('020010000',635,1,'1'),('020010000',636,2,'1'),('020010000',637,3,'1'),('020010000',638,4,'1'),('020010000',639,5,'1'),('020010000',640,6,'1'),('020010000',641,7,'1'),('020010000',642,8,'1'),('020010000',643,9,'1'),('020010000',644,10,'1'),('020010000',645,11,'1'),('020010000',646,12,'1'),('020010000',647,13,'1'),('020010000',648,14,'1'),('020010000',649,15,'1'),('020010000',650,16,'1'),('020010000',651,17,'1'),('020010000',652,18,'1'),('020010000',653,19,'1'),('020010000',654,20,'1'),('020010000',655,21,'1'),('020010000',656,22,'1'),('020010000',657,23,'1'),('020010000',658,24,'1'),('020010000',659,25,'1'),('020010000',660,26,'1'),('020010000',661,27,'1'),('020010000',662,28,'1'),('020010000',663,29,'1'),('020010000',664,30,'1'),('020010100',132,NULL,'1'),('020010100',135,NULL,'1'),('020010100',136,NULL,'1'),('020010100',157,NULL,'1'),('020010100',159,NULL,'1'),('020010100',280,NULL,'1'),('020010100',285,NULL,'1'),('020010100',286,NULL,'1'),('020010100',287,NULL,'1'),('020010100',291,NULL,'1'),('020010100',295,NULL,'1'),('020010100',313,NULL,'1'),('020010100',314,NULL,'1'),('020010100',343,NULL,'1'),('020010100',345,NULL,'1'),('020010100',346,NULL,'1'),('020010100',347,NULL,'1'),('020010100',348,NULL,'1'),('020010100',349,NULL,'1'),('020010100',350,NULL,'1'),('020010100',351,NULL,'1'),('020010100',359,NULL,'1'),('020010100',400,NULL,'1'),('020010100',414,NULL,'1'),('020010100',415,NULL,'1'),('020010100',416,NULL,'1'),('020010100',417,NULL,'1'),('020010100',418,NULL,'1'),('020010100',419,NULL,'1'),('020010100',420,NULL,'1'),('020010100',421,NULL,'1'),('020010100',422,NULL,'1'),('020010100',423,NULL,'1'),('020010100',424,NULL,'1'),('020010100',425,NULL,'1'),('020010100',426,NULL,'1'),('020010100',427,NULL,'1'),('020010100',428,NULL,'1'),('020010100',429,NULL,'1'),('020010100',430,NULL,'1'),('020010100',431,NULL,'1'),('020010100',432,NULL,'1'),('020010100',433,NULL,'1'),('020010100',434,NULL,'1'),('020010100',435,NULL,'1'),('020010100',436,NULL,'1'),('020010100',437,NULL,'1'),('020010100',438,NULL,'1'),('020010100',439,NULL,'1'),('020010100',440,NULL,'1'),('020010100',441,NULL,'1'),('020010100',442,NULL,'1'),('020010100',443,NULL,'1'),('020010200',264,NULL,'1'),('020010200',266,NULL,'1'),('020010200',267,NULL,'1'),('020010200',268,NULL,'1'),('020010200',444,NULL,'1'),('020010300',25,NULL,'1'),('020010300',123,NULL,'1'),('020010300',125,NULL,'1'),('020010300',126,NULL,'1'),('020010300',128,NULL,'1'),('020010300',129,NULL,'1'),('020010300',183,NULL,'1'),('020010300',184,NULL,'1'),('020010300',185,NULL,'1'),('020010300',186,NULL,'1'),('020010300',298,NULL,'1'),('020010300',299,NULL,'1'),('020010300',300,NULL,'1'),('020010300',303,NULL,'1'),('020010300',304,NULL,'1'),('020010300',305,NULL,'1'),('020010300',307,NULL,'1'),('020010300',308,NULL,'1'),('020010300',309,NULL,'1'),('020010300',310,NULL,'1'),('020010300',311,NULL,'1'),('020010300',313,NULL,'1'),('020010300',314,NULL,'1'),('020010300',332,NULL,'1'),('020010300',334,NULL,'1'),('020010300',335,NULL,'1'),('020010300',336,NULL,'1'),('020010300',337,NULL,'1'),('020010300',338,NULL,'1'),('020010300',339,NULL,'1'),('020010300',340,NULL,'1'),('020010300',341,NULL,'1'),('020010300',343,NULL,'1'),('020010300',344,NULL,'1'),('020010300',346,NULL,'1'),('020010300',347,NULL,'1'),('020010300',348,NULL,'1'),('020010300',350,NULL,'1'),('020010300',351,NULL,'1'),('020010300',352,NULL,'1'),('020010300',353,NULL,'1'),('020010300',356,NULL,'1'),('020010300',357,NULL,'1'),('020010300',358,NULL,'1'),('020010300',360,NULL,'1'),('020010300',361,NULL,'1'),('020010300',362,NULL,'1'),('020010300',415,NULL,'1'),('020010300',416,NULL,'1'),('020010400',246,NULL,'1'),('020010400',247,NULL,'1'),('020010400',248,NULL,'1'),('020010400',249,NULL,'1'),('020010400',383,NULL,'1'),('020010400',385,NULL,'1'),('020010400',386,NULL,'1'),('020010400',387,NULL,'1'),('020010400',389,NULL,'1'),('020010400',402,1,'1'),('020010400',445,NULL,'1'),('020010400',446,NULL,'1'),('020010500',198,NULL,'1'),('020010500',199,NULL,'1'),('020010500',200,NULL,'1'),('020010500',201,NULL,'1'),('020010500',202,NULL,'1'),('020010500',203,NULL,'1'),('020010500',204,NULL,'1'),('020010500',205,NULL,'1'),('020010600',161,NULL,'1'),('020010600',162,NULL,'1'),('020010600',163,NULL,'1'),('020010600',164,NULL,'1'),('020010600',165,NULL,'1'),('020010600',166,NULL,'1'),('020010600',167,NULL,'1'),('020010600',168,NULL,'1'),('020010700',279,NULL,'1'),('020010700',280,NULL,'1'),('020010700',281,NULL,'1'),('020010700',282,NULL,'1'),('020010700',283,NULL,'1'),('020010700',284,1,'1'),('020010700',285,NULL,'1'),('020010700',286,NULL,'1'),('020010700',288,NULL,'1'),('020010700',289,NULL,'1'),('020010700',290,NULL,'1'),('020010700',291,NULL,'1'),('020010700',292,NULL,'1'),('020010700',293,NULL,'1'),('020010700',294,NULL,'1'),('020010700',295,NULL,'1'),('020010700',296,NULL,'1'),('020010800',363,1,'1'),('020010800',364,2,'1'),('020010800',365,3,'1'),('020010900',68,NULL,'1'),('020010900',187,NULL,'1'),('020010900',188,NULL,'1'),('020010900',206,NULL,'1'),('020010900',208,NULL,'1'),('020010900',209,NULL,'1'),('020010900',212,NULL,'1'),('020010900',213,NULL,'1'),('020010900',218,NULL,'1'),('020010900',219,NULL,'1'),('020010900',220,NULL,'1'),('020010900',221,NULL,'1'),('020010900',222,NULL,'1'),('020010900',223,NULL,'1'),('020010900',224,NULL,'1'),('020010900',225,NULL,'1'),('020010900',226,NULL,'1'),('020010900',227,NULL,'1'),('020010900',243,NULL,'1'),('020010900',244,NULL,'1'),('020010900',431,NULL,'1'),('020010900',436,NULL,'1'),('020010900',447,NULL,'1'),('020010900',448,NULL,'1'),('020010900',449,NULL,'1'),('020010900',450,NULL,'1'),('020011000',378,NULL,'1'),('020011000',379,NULL,'1'),('020011000',380,NULL,'1'),('020011000',381,NULL,'1'),('020011000',382,NULL,'1'),('020011100',149,NULL,'1'),('020011100',150,NULL,'1'),('020011100',151,NULL,'1'),('020011100',152,NULL,'1'),('020011100',154,NULL,'1'),('020011100',155,NULL,'1'),('020011100',156,NULL,'1'),('020011100',157,NULL,'1'),('020011100',158,NULL,'1'),('020011100',159,NULL,'1'),('020011100',160,NULL,'1'),('020011200',133,NULL,'1'),('020011200',134,NULL,'1'),('020011200',135,NULL,'1'),('020011200',136,NULL,'1'),('020011200',137,NULL,'1'),('020011300',138,NULL,'1'),('020011300',139,NULL,'1'),('020011300',140,NULL,'1'),('020011400',245,NULL,'1'),('020011400',246,NULL,'1'),('020011400',247,NULL,'1'),('020011400',248,NULL,'1'),('020011400',249,NULL,'1'),('020011400',250,NULL,'1'),('020011400',251,NULL,'1'),('020011400',252,NULL,'1'),('020011400',253,NULL,'1'),('020011400',254,NULL,'1'),('020011400',255,NULL,'1'),('020011400',256,NULL,'1'),('020011400',257,NULL,'1'),('020011400',258,NULL,'1'),('020011400',259,NULL,'1'),('020011400',260,NULL,'1'),('020011400',261,NULL,'1'),('020011400',262,NULL,'1'),('020011400',263,NULL,'1'),('020011500',141,NULL,'1'),('020011500',142,NULL,'1'),('020011500',143,NULL,'1'),('020011500',144,NULL,'1'),('020011500',145,NULL,'1'),('020011500',146,NULL,'1'),('020011500',147,NULL,'1'),('020011500',148,NULL,'1'),('020011600',170,NULL,'1'),('020011600',171,NULL,'1'),('020011600',172,NULL,'1'),('020011600',173,NULL,'1'),('020011600',174,NULL,'1'),('020011600',175,NULL,'1'),('020011600',176,NULL,'1'),('020011600',177,NULL,'1'),('020011600',178,NULL,'1'),('020011600',179,NULL,'1'),('020011600',180,NULL,'1'),('020011600',181,NULL,'1'),('020011600',182,NULL,'1'),('020011700',271,NULL,'1'),('020011700',272,NULL,'1'),('020011700',273,NULL,'1'),('020011700',274,NULL,'1'),('020011700',275,NULL,'1'),('020011700',276,NULL,'1'),('020011700',277,NULL,'1'),('020011700',278,NULL,'1'),('020011800',214,NULL,'1'),('020011800',215,NULL,'1'),('020011800',216,NULL,'1'),('020011800',217,NULL,'1'),('020011800',451,NULL,'1'),('020011800',452,NULL,'1'),('020011900',404,NULL,'1'),('020011900',405,NULL,'1'),('020011900',406,NULL,'1'),('020011900',407,NULL,'1'),('020011900',408,NULL,'1'),('020011900',409,NULL,'1'),('020011900',410,NULL,'1'),('020011900',411,NULL,'1'),('020012000',453,NULL,'1'),('020012000',454,NULL,'1'),('020012000',455,NULL,'1'),('020012000',456,NULL,'1'),('020012000',457,NULL,'1'),('020012000',458,NULL,'1'),('020012100',459,NULL,'1'),('020012100',460,NULL,'1'),('020012100',461,NULL,'1'),('020012100',462,NULL,'1'),('020012100',463,NULL,'1'),('020012100',464,NULL,'1'),('020012200',190,NULL,'1'),('020012200',191,NULL,'1'),('020012200',192,NULL,'1'),('020012200',193,NULL,'1'),('020012200',194,NULL,'1'),('020012200',195,NULL,'1'),('020012200',196,NULL,'1'),('020012200',197,NULL,'1'),('020012300',413,NULL,'1'),('020012300',414,NULL,'1'),('020012400',352,NULL,'1'),('020012400',353,NULL,'1'),('020012400',354,NULL,'1'),('020012400',356,NULL,'1'),('020012400',357,NULL,'1'),('020012400',358,NULL,'1'),('020012400',359,NULL,'1'),('020012400',360,NULL,'1'),('020012400',361,NULL,'1'),('020012400',362,NULL,'1'),('020012500',372,NULL,'1'),('020012500',373,NULL,'1'),('020012500',374,NULL,'1'),('020012500',375,NULL,'1'),('020012500',376,NULL,'1'),('020012500',377,NULL,'1'),('020012600',313,NULL,'1'),('020012600',314,NULL,'1'),('020012600',334,NULL,'1'),('020012600',335,NULL,'1'),('020012600',336,NULL,'1'),('020012600',337,NULL,'1'),('020012600',338,NULL,'1'),('020012600',339,NULL,'1'),('020012600',340,NULL,'1'),('020012600',341,NULL,'1'),('020012600',342,NULL,'1'),('020012600',343,NULL,'1'),('020012600',344,NULL,'1'),('020012600',346,NULL,'1'),('020012600',347,NULL,'1'),('020012600',348,NULL,'1'),('020012600',349,NULL,'1'),('020012600',350,NULL,'1'),('020012700',252,NULL,'1'),('020012700',253,NULL,'1'),('020012700',254,NULL,'1'),('020012700',255,NULL,'1'),('020012700',256,NULL,'1'),('020012700',257,NULL,'1'),('020012700',258,NULL,'1'),('020012700',259,NULL,'1'),('020012700',260,NULL,'1'),('020012700',261,NULL,'1'),('020012700',262,NULL,'1'),('020012700',263,NULL,'1'),('020012800',465,NULL,'1'),('020012800',466,NULL,'1'),('020012800',467,NULL,'1'),('020012800',468,NULL,'1'),('020012800',469,NULL,'1'),('020012800',470,NULL,'1'),('020012800',471,NULL,'1'),('020012900',472,NULL,'1'),('020012900',473,NULL,'1'),('020012900',474,NULL,'1'),('020013000',7,NULL,'1'),('020013000',280,NULL,'1'),('020030000',475,NULL,'1'),('020030000',476,NULL,'1'),('020030000',477,NULL,'1'),('020030000',478,NULL,'1'),('020030000',479,NULL,'1'),('020030000',480,NULL,'1'),('020030000',481,NULL,'1'),('020030000',482,NULL,'1'),('020030000',483,NULL,'1'),('020030000',484,NULL,'1'),('020030000',485,NULL,'1'),('020030000',486,NULL,'1'),('020030000',487,NULL,'1'),('020030000',488,NULL,'1'),('020030000',489,NULL,'1'),('020030000',490,NULL,'1'),('020030000',491,NULL,'1'),('020040000',7,9,'1'),('020040000',492,8,'1'),('020040000',493,7,'1'),('020040000',494,6,'1'),('020040000',495,5,'1'),('020040000',496,4,'1'),('020040000',497,3,'1'),('020040000',498,2,'1'),('020040000',499,1,'1'),('020050000',501,NULL,'1'),('020050000',502,NULL,'1'),('020050000',503,NULL,'1'),('020050000',504,NULL,'1'),('020070000',505,NULL,'1'),('020070000',506,NULL,'1'),('020070000',507,NULL,'1'),('020070000',508,NULL,'1'),('020070000',509,NULL,'1'),('020080100',510,NULL,'1'),('020080100',511,NULL,'1'),('020080100',512,NULL,'1'),('020080100',513,NULL,'1'),('020080100',514,NULL,'1'),('020080100',515,NULL,'1'),('020080100',516,NULL,'1'),('020080100',517,NULL,'1'),('020080100',518,NULL,'1'),('020080100',519,NULL,'1'),('020080100',520,NULL,'1'),('020080100',521,NULL,'1'),('020080100',522,NULL,'1'),('020080100',523,NULL,'1'),('020080100',524,NULL,'1'),('020080100',525,NULL,'1'),('020080100',526,NULL,'1'),('020080100',527,NULL,'1'),('020080100',528,NULL,'1'),('020080200',529,NULL,'1'),('020080200',530,NULL,'1'),('020080200',531,NULL,'1'),('020080200',532,NULL,'1'),('020080200',533,NULL,'1'),('020080200',534,NULL,'1'),('020080200',535,NULL,'1'),('020080300',536,NULL,'1'),('020080300',537,NULL,'1'),('020080300',538,NULL,'1'),('020080300',539,NULL,'1'),('020080300',540,NULL,'1'),('020080300',541,NULL,'1'),('020080300',542,NULL,'1'),('020080300',543,NULL,'1'),('020080300',544,NULL,'1'),('020080300',545,NULL,'1'),('020080300',546,NULL,'1'),('020080300',547,NULL,'1'),('020080300',548,NULL,'1'),('020080300',549,NULL,'1'),('020080300',550,NULL,'1'),('020080300',551,NULL,'1'),('020080300',552,NULL,'1'),('020080300',553,NULL,'1'),('020080300',554,NULL,'1'),('020080300',555,NULL,'1'),('020080400',556,NULL,'1'),('020080400',557,NULL,'1'),('020080400',558,NULL,'1'),('020080500',559,NULL,'1'),('020080500',560,NULL,'1'),('020080500',561,NULL,'1'),('020080500',562,NULL,'1'),('020080500',563,NULL,'1'),('020080500',564,NULL,'1'),('020080600',565,NULL,'1'),('020080600',566,NULL,'1'),('020080600',567,NULL,'1'),('020090000',616,1,'1'),('020090000',618,2,'1'),('020090000',620,3,'1'),('020090000',622,4,'1'),('020090000',624,5,'1'),('020090000',626,6,'1'),('020090000',628,7,'1'),('020090000',630,8,'1'),('020090000',632,9,'1'),('020090000',634,10,'1'),('020090050',568,NULL,'1'),('020090050',569,NULL,'1'),('020090050',570,NULL,'1'),('020090100',571,NULL,'1'),('020090100',572,NULL,'1'),('020090100',573,NULL,'1'),('020090200',574,NULL,'1'),('020090200',575,NULL,'1'),('020090200',576,NULL,'1'),('020090200',577,NULL,'1'),('020090200',578,NULL,'1'),('020090200',579,NULL,'1'),('020090200',580,NULL,'1'),('020090200',581,NULL,'1'),('020090200',582,NULL,'1'),('020090200',583,NULL,'1'),('020090200',584,NULL,'1'),('020090300',585,NULL,'1'),('020090400',586,NULL,'1'),('020090400',587,NULL,'1'),('020090400',588,NULL,'1'),('020090400',589,NULL,'1'),('020090400',590,NULL,'1'),('020090400',591,NULL,'1'),('020090400',592,NULL,'1'),('020090400',593,NULL,'1'),('020090500',594,NULL,'1'),('020090500',595,NULL,'1'),('020090500',596,NULL,'1'),('020090500',597,NULL,'1'),('020090500',598,NULL,'1'),('020090500',599,NULL,'1'),('020090500',600,NULL,'1'),('020090500',601,NULL,'1'),('020090600',602,NULL,'1'),('020090600',603,NULL,'1'),('020090700',604,NULL,'1'),('020090700',605,NULL,'1'),('020090800',606,NULL,'1'),('020090800',607,NULL,'1'),('020090800',608,NULL,'1'),('020090800',609,NULL,'1'),('020090800',610,NULL,'1'),('020090800',611,NULL,'1'),('020090800',612,NULL,'1'),('020090800',613,NULL,'1'),('020090800',614,NULL,'1'),('020090900',615,NULL,'1'),('070',7,5,'1'),('070',665,1,'1'),('070',666,2,'1'),('070',667,3,'1'),('070',668,4,'1'),('071',7,15,'1'),('071',669,1,'1'),('071',670,2,'1'),('071',671,3,'1'),('071',672,4,'1'),('071',673,5,'1'),('071',674,6,'1'),('071',675,7,'1'),('071',676,8,'1'),('071',677,9,'1'),('071',678,10,'1'),('071',679,11,'1'),('071',680,12,'1'),('071',681,13,'1'),('071',682,14,'1'),('072',7,6,'1'),('072',683,1,'1'),('072',684,2,'1'),('072',685,3,'1'),('072',686,4,'1'),('072',687,5,'1'),('074',7,4,'1'),('074',120,1,'1'),('074',121,2,'1'),('074',122,3,'1'),('075001',123,5,'1'),('075001',124,7,'1'),('075001',125,1,'1'),('075001',126,2,'1'),('075001',127,8,'1'),('075001',128,4,'1'),('075001',129,6,'1'),('075001',130,9,'1'),('075001',131,10,'1'),('075001',132,3,'1'),('075002',133,5,'1'),('075002',134,2,'1'),('075002',135,3,'1'),('075002',136,1,'1'),('075002',137,4,'1'),('075003',138,2,'1'),('075003',139,1,'1'),('075003',140,3,'1'),('075004',141,3,'1'),('075004',142,2,'1'),('075004',143,8,'1'),('075004',144,1,'1'),('075004',145,6,'1'),('075004',146,7,'1'),('075004',147,5,'1'),('075004',148,4,'1'),('075005',149,7,'1'),('075005',150,8,'1'),('075005',151,12,'1'),('075005',152,4,'1'),('075005',153,6,'1'),('075005',154,3,'1'),('075005',155,2,'1'),('075005',156,1,'1'),('075005',157,9,'1'),('075005',158,10,'1'),('075005',159,5,'1'),('075005',160,11,'1'),('075006',161,8,'1'),('075006',162,1,'1'),('075006',163,2,'1'),('075006',164,7,'1'),('075006',165,6,'1'),('075006',166,3,'1'),('075006',167,4,'1'),('075006',168,5,'1'),('075007',169,5,'1'),('075007',170,18,'1'),('075007',171,4,'1'),('075007',172,3,'1'),('075007',173,16,'1'),('075007',174,17,'1'),('075007',175,2,'1'),('075007',176,1,'1'),('075007',177,10,'1'),('075007',178,11,'1'),('075007',179,12,'1'),('075007',180,13,'1'),('075007',181,14,'1'),('075007',182,15,'1'),('075007',183,7,'1'),('075007',184,6,'1'),('075007',185,8,'1'),('075007',186,9,'1'),('075008',187,1,'1'),('075008',188,2,'1'),('075008',189,3,'1'),('075009',190,7,'1'),('075009',191,8,'1'),('075009',192,3,'1'),('075009',193,4,'1'),('075009',194,2,'1'),('075009',195,1,'1'),('075009',196,6,'1'),('075009',197,5,'1'),('075010',198,1,'1'),('075010',199,2,'1'),('075010',200,7,'1'),('075010',201,3,'1'),('075010',202,8,'1'),('075010',203,5,'1'),('075010',204,6,'1'),('075010',205,4,'1'),('075011',206,23,'1'),('075011',207,39,'1'),('075011',208,24,'1'),('075011',209,25,'1'),('075011',210,26,'1'),('075011',211,27,'1'),('075011',212,28,'1'),('075011',213,29,'1'),('075011',214,35,'1'),('075011',215,36,'1'),('075011',216,37,'1'),('075011',217,38,'1'),('075011',218,30,'1'),('075011',219,31,'1'),('075011',220,32,'1'),('075011',221,33,'1'),('075011',222,34,'1'),('075011',223,1,'1'),('075011',224,2,'1'),('075011',225,3,'1'),('075011',226,4,'1'),('075011',227,5,'1'),('075011',228,6,'1'),('075011',229,7,'1'),('075011',230,8,'1'),('075011',231,9,'1'),('075011',232,10,'1'),('075011',233,11,'1'),('075011',234,12,'1'),('075011',235,13,'1'),('075011',236,14,'1'),('075011',237,15,'1'),('075011',238,16,'1'),('075011',239,17,'1'),('075011',240,18,'1'),('075011',241,19,'1'),('075011',242,20,'1'),('075011',243,21,'1'),('075011',244,22,'1'),('075012',245,1,'1'),('075012',246,2,'1'),('075012',247,3,'1'),('075012',248,4,'1'),('075012',249,5,'1'),('075012',250,6,'1'),('075012',251,7,'1'),('075013',252,1,'1'),('075013',253,2,'1'),('075013',254,3,'1'),('075013',255,4,'1'),('075013',256,5,'1'),('075013',257,6,'1'),('075013',258,7,'1'),('075013',259,8,'1'),('075013',260,9,'1'),('075013',261,10,'1'),('075013',262,11,'1'),('075013',263,12,'1'),('075014',264,5,'1'),('075014',265,1,'1'),('075014',266,2,'1'),('075014',267,3,'1'),('075014',268,4,'1'),('075014',269,7,'1'),('075014',270,6,'1'),('075015',271,1,'1'),('075015',272,8,'1'),('075015',273,2,'1'),('075015',274,3,'1'),('075015',275,4,'1'),('075015',276,5,'1'),('075015',277,6,'1'),('075015',278,7,'1'),('075016',279,17,'1'),('075016',280,7,'1'),('075016',281,8,'1'),('075016',282,9,'1'),('075016',283,1,'1'),('075016',284,18,'1'),('075016',285,10,'1'),('075016',286,11,'1'),('075016',287,12,'1'),('075016',288,13,'1'),('075016',289,2,'1'),('075016',290,3,'1'),('075016',291,14,'1'),('075016',292,4,'1'),('075016',293,15,'1'),('075016',294,5,'1'),('075016',295,16,'1'),('075016',296,6,'1'),('075017',297,35,'1'),('075017',298,32,'1'),('075017',299,33,'1'),('075017',300,34,'1'),('075017',301,38,'1'),('075017',302,1,'1'),('075017',303,36,'1'),('075017',304,37,'1'),('075017',305,2,'1'),('075017',306,3,'1'),('075017',307,4,'1'),('075017',308,5,'1'),('075017',309,6,'1'),('075017',310,7,'1'),('075017',311,8,'1'),('075017',312,9,'1'),('075017',313,39,'1'),('075017',314,10,'1'),('075017',315,40,'1'),('075017',316,41,'1'),('075017',317,42,'1'),('075017',318,43,'1'),('075017',319,44,'1'),('075017',320,45,'1'),('075017',321,46,'1'),('075017',322,47,'1'),('075017',323,48,'1'),('075017',324,49,'1'),('075017',325,50,'1'),('075017',326,51,'1'),('075017',327,52,'1'),('075017',328,53,'1'),('075017',329,54,'1'),('075017',330,55,'1'),('075017',331,56,'1'),('075017',332,60,'1'),('075017',333,57,'1'),('075017',334,58,'1'),('075017',335,59,'1'),('075017',336,11,'1'),('075017',337,12,'1'),('075017',338,13,'1'),('075017',339,14,'1'),('075017',340,15,'1'),('075017',341,16,'1'),('075017',342,17,'1'),('075017',343,18,'1'),('075017',344,19,'1'),('075017',345,20,'1'),('075017',346,21,'1'),('075017',347,22,'1'),('075017',348,23,'1'),('075017',349,24,'1'),('075017',350,25,'1'),('075017',351,26,'1'),('075017',352,27,'1'),('075017',353,28,'1'),('075017',354,29,'1'),('075017',355,30,'1'),('075018',352,4,'1'),('075018',353,5,'1'),('075018',354,11,'1'),('075018',355,6,'1'),('075018',356,7,'1'),('075018',357,8,'1'),('075018',358,9,'1'),('075018',359,10,'1'),('075018',360,1,'1'),('075018',361,2,'1'),('075018',362,3,'1'),('075019',363,1,'1'),('075019',364,2,'1'),('075019',365,3,'1'),('075020',366,6,'1'),('075020',367,5,'1'),('075020',368,4,'1'),('075020',369,3,'1'),('075020',370,2,'1'),('075020',371,1,'1'),('075021',372,1,'1'),('075021',373,6,'1'),('075021',374,5,'1'),('075021',375,4,'1'),('075021',376,3,'1'),('075021',377,2,'1'),('075022',378,2,'1'),('075022',379,1,'1'),('075022',380,5,'1'),('075022',381,4,'1'),('075022',382,3,'1'),('075023',383,1,'1'),('075023',384,2,'1'),('075023',385,3,'1'),('075023',386,4,'1'),('075023',387,5,'1'),('075023',388,6,'1'),('075023',389,7,'1'),('075023',390,8,'1'),('075023',391,9,'1'),('075023',392,10,'1'),('075023',393,11,'1'),('075023',394,12,'1'),('075023',395,13,'1'),('075023',396,14,'1'),('075023',397,15,'1'),('075023',398,16,'1'),('075023',399,17,'1'),('075023',400,18,'1'),('075023',401,19,'1'),('075023',402,21,'1'),('075023',403,20,'1'),('075024',404,7,'1'),('075024',405,4,'1'),('075024',406,8,'1'),('075024',407,5,'1'),('075024',408,6,'1'),('075024',409,1,'1'),('075024',410,2,'1'),('075024',411,3,'1'),('075025',412,1,'1'),('075026',413,2,'1'),('075026',414,1,'1'),('075027',7,1,'1'),('081',953,1,'1'),('081',954,2,'1'),('081',955,3,'1'),('081',956,4,'1'),('081',957,5,'1'),('081',958,6,'1'),('081',959,7,'1'),('081',960,8,'1'),('081',961,9,'1'),('081',962,10,'1'),('081',963,11,'1'),('081',964,12,'1'),('081',965,13,'1'),('081',966,14,'1'),('081',967,15,'1'),('081',968,16,'1'),('081',969,17,'1'),('081',970,18,'1'),('081',971,19,'1'),('081',972,20,'1'),('081',973,21,'1'),('081',974,22,'1'),('081',975,23,'1'),('081',976,24,'1'),('084',928,1,'1'),('084',929,2,'1'),('084',930,3,'1'),('084',931,4,'1'),('084',932,5,'1'),('084',933,6,'1'),('084',934,7,'1'),('084',935,8,'1'),('084',936,9,'1'),('084',937,10,'1'),('084',938,11,'1'),('084',939,12,'1'),('084',940,13,'1'),('084',941,14,'1'),('084',942,15,'1'),('084',943,16,'1'),('084',944,17,'1'),('084',945,18,'1'),('084',946,19,'1'),('084',947,20,'1'),('084',948,21,'1'),('084',949,22,'1'),('084',950,23,'1'),('084',951,24,'1'),('084',952,25,'1'),('089',32,1,'1'),('089',33,2,'1'),('089',34,3,'1'),('089',35,4,'1'),('089',36,5,'1'),('089',37,6,'1'),('089',38,7,'1'),('089',39,8,'1'),('089',40,9,'1'),('089',41,10,'1'),('089',42,11,'1'),('089',43,12,'1'),('089001',705,12,'1'),('089001',706,11,'1'),('089001',707,10,'1'),('089001',708,9,'1'),('089001',709,8,'1'),('089001',710,7,'1'),('089001',711,6,'1'),('089001',712,5,'1'),('089001',713,4,'1'),('089001',714,3,'1'),('089001',715,2,'1'),('089001',716,1,'1'),('090',25,1,'1'),('090',26,6,'1'),('090',27,5,'1'),('090',28,4,'1'),('090',29,3,'1'),('090',30,2,'1'),('090',31,7,'1'),('090',717,1,'1'),('090',718,2,'1'),('090',719,3,'1'),('090',720,4,'1'),('090',721,5,'1'),('090',722,6,'1'),('090',723,7,'1'),('090',724,8,'1'),('090',725,9,'1'),('090',726,10,'1'),('090',727,11,'1'),('090',728,12,'1'),('090',729,13,'1'),('090',730,14,'1'),('090',731,15,'1'),('090',732,16,'1'),('090',733,17,'1'),('090',734,18,'1'),('090',735,19,'1'),('090',736,20,'1'),('090',737,21,'1'),('090',738,22,'1'),('090',739,23,'1'),('090',740,24,'1'),('090',741,25,'1'),('090',742,26,'1'),('090',743,27,'1'),('090',744,28,'1'),('090',745,29,'1'),('090',746,30,'1'),('090',747,31,'1'),('090',748,32,'1'),('090',749,33,'1'),('090',750,34,'1'),('090',751,35,'1'),('090',752,36,'1'),('090',753,37,'1'),('090',754,38,'1'),('090',755,39,'1'),('090',756,40,'1'),('090',757,41,'1'),('090',758,42,'1'),('090',759,43,'1'),('090',760,44,'1'),('090',761,45,'1'),('090',762,46,'1'),('090',763,47,'1'),('090',764,48,'1'),('090',765,49,'1'),('090',766,50,'1'),('090',767,51,'1'),('090',768,52,'1'),('090',769,53,'1'),('090',770,54,'1'),('090',771,55,'1'),('090',772,56,'1'),('090',773,57,'1'),('090',774,58,'1'),('090',775,59,'1'),('090',776,60,'1'),('090',777,61,'1'),('090',778,62,'1'),('090',779,63,'1'),('090',780,64,'1'),('090',781,65,'1'),('090',782,66,'1'),('090',783,67,'1'),('090',784,68,'1'),('090',785,69,'1'),('090',786,70,'1'),('090',787,71,'1'),('090',788,72,'1'),('090',789,73,'1'),('090',790,74,'1'),('090',791,75,'1'),('090',792,76,'1'),('090',793,77,'1'),('090',794,78,'1'),('090',795,79,'1'),('090',796,80,'1'),('090',797,81,'1'),('090',798,82,'1'),('090',799,83,'1'),('090',800,84,'1'),('090',801,85,'1'),('090',802,86,'1'),('090',803,87,'1'),('090',804,88,'1'),('090',805,89,'1'),('090',806,90,'1'),('090',807,91,'1'),('090',808,92,'1'),('090',809,93,'1'),('090',810,94,'1'),('090',811,95,'1'),('090',812,96,'1'),('090',813,97,'1'),('090',814,98,'1'),('090',815,99,'1'),('090',816,100,'1'),('090',817,101,'1'),('090',818,102,'1'),('090',819,103,'1'),('090',820,104,'1'),('090',821,105,'1'),('090',822,106,'1'),('090',823,107,'1'),('090',824,108,'1'),('090',825,109,'1'),('090',826,110,'1'),('090',827,111,'1'),('090',828,112,'1'),('090',829,113,'1'),('090',830,114,'1'),('090',831,115,'1'),('090',832,116,'1'),('090',833,117,'1'),('090',834,118,'1'),('090',835,119,'1'),('090',836,120,'1'),('090',837,121,'1'),('090',838,122,'1'),('090',839,123,'1'),('090',840,124,'1'),('090',841,125,'1'),('090',842,126,'1'),('090',843,127,'1'),('090',844,128,'1'),('090',845,129,'1'),('090',846,130,'1'),('090',847,131,'1'),('090',848,132,'1'),('090',849,133,'1'),('090',850,134,'1'),('090',851,135,'1'),('090',852,136,'1'),('090',853,137,'1'),('090',854,138,'1'),('090',855,139,'1'),('090',856,140,'1'),('090',857,141,'1'),('090',858,142,'1'),('090',859,143,'1'),('090',860,144,'1'),('090',861,145,'1'),('090',862,146,'1'),('090',863,147,'1'),('090',864,148,'1'),('090',865,149,'1'),('090',866,150,'1'),('090',867,151,'1'),('090',868,152,'1'),('090',869,153,'1'),('090',870,154,'1'),('090',871,155,'1'),('090',872,156,'1'),('090',873,157,'1'),('090',874,158,'1'),('090',875,159,'1'),('090',876,160,'1'),('090',877,161,'1'),('090',878,162,'1'),('090',879,163,'1'),('090',880,164,'1'),('090',881,165,'1'),('090',882,166,'1'),('090',883,167,'1'),('090',884,168,'1'),('090',885,169,'1'),('090',886,170,'1'),('090',887,171,'1'),('090',888,172,'1'),('091',20,1,'1'),('091',21,2,'1'),('091',22,3,'1'),('091',23,5,'1'),('091',24,6,'1'),('091',889,1,'1'),('091',890,2,'1'),('091',891,3,'1'),('091',892,4,'1'),('091',893,5,'1'),('091',894,6,'1'),('091',895,7,'1'),('091',896,8,'1'),('091',897,9,'1'),('091',898,10,'1'),('091',899,11,'1'),('091',900,12,'1'),('091',901,13,'1'),('091',902,14,'1'),('091',903,15,'1'),('091',904,16,'1'),('091',905,17,'1'),('091',906,18,'1'),('091',907,19,'1'),('092',7,4,'1'),('092',18,1,'1'),('092',19,2,'1'),('092',116,3,'1'),('092',908,1,'1'),('092',909,2,'1'),('092',910,3,'1'),('095',7,70,'1'),('095',724,1,'1'),('095',834,2,'1'),('095',977,3,'1'),('095',978,4,'1'),('095',979,5,'1'),('095',980,6,'1'),('095',981,7,'1'),('095',982,8,'1'),('095',983,9,'1'),('095',984,10,'1'),('095',985,11,'1'),('095',986,12,'1'),('095',987,13,'1'),('095',988,14,'1'),('095',989,15,'1'),('095',990,16,'1'),('095',991,17,'1'),('095',992,18,'1'),('095',993,19,'1'),('095',994,20,'1'),('095',995,21,'1'),('095',996,22,'1'),('095',997,23,'1'),('095',998,24,'1'),('095',999,25,'1'),('095',1000,26,'1'),('095',1001,27,'1'),('095',1002,28,'1'),('095',1003,29,'1'),('095',1004,30,'1'),('095',1005,31,'1'),('095',1006,32,'1'),('095',1007,33,'1'),('095',1008,34,'1'),('095',1009,35,'1'),('095',1010,35,'1'),('095',1011,37,'1'),('095',1012,38,'1'),('095',1013,39,'1'),('095',1014,40,'1'),('095',1015,41,'1'),('095',1016,42,'1'),('095',1017,43,'1'),('095',1018,44,'1'),('095',1019,45,'1'),('095',1020,46,'1'),('095',1021,47,'1'),('095',1022,48,'1'),('095',1023,49,'1'),('095',1024,50,'1'),('095',1025,51,'1'),('095',1026,52,'1'),('095',1027,53,'1'),('095',1028,54,'1'),('095',1029,55,'1'),('095',1030,56,'1'),('095',1031,57,'1'),('095',1032,58,'1'),('095',1033,59,'1'),('095',1034,60,'1'),('095',1035,61,'1'),('095',1036,62,'1'),('095',1037,63,'1'),('095',1038,64,'1'),('095',1039,65,'1'),('095',1040,66,'1'),('095',1041,67,'1'),('095',1042,68,'1'),('095',1043,69,'1'),('145',7,16,'1'),('145',59,1,'1'),('145',60,2,'1'),('145',61,3,'1'),('145',62,4,'1'),('145',63,5,'1'),('145',64,6,'1'),('145',65,7,'1'),('145',66,8,'1'),('145',67,9,'1'),('145',68,10,'1'),('145',69,11,'1'),('145',70,12,'1'),('145',71,13,'1'),('145',72,14,'1'),('145',73,15,'1'),('163',7,7,'1'),('163',695,1,'1'),('163',696,2,'1'),('163',697,3,'1'),('163',698,4,'1'),('163',699,5,'1'),('163',700,6,'1'),('165',7,8,'1'),('165',688,1,'1'),('165',689,2,'1'),('165',690,3,'1'),('165',691,4,'1'),('165',692,5,'1'),('165',693,6,'1'),('165',694,7,'1'),('211001',2,2,'1'),('211002',3,1,'1'),('211002',4,2,'1'),('211002',5,3,'1'),('211002',6,4,'1'),('211003',9,2,'1'),('211003',10,3,'1'),('214',7,5,'1'),('214',701,1,'1'),('214',702,2,'1'),('214',703,3,'1'),('214',704,4,'1'),('805',11,1,'1'),('805',12,2,'1'),('841002001',7,7,'1'),('841002001',44,1,'1'),('841002001',45,2,'1'),('841002001',46,3,'1'),('841002001',47,4,'1'),('841002001',48,5,'1'),('841002001',118,6,'1'),('941',923,1,'1'),('941',924,2,'1'),('941',925,3,'1'),('941',926,4,'1'),('956',13,1,'1'),('956',14,2,'1'),('957',15,1,'1'),('957',16,2,'1'),('958',911,1,'1'),('958',912,2,'1'),('958',913,3,'1'),('958',914,4,'1'),('958',915,5,'1'),('958',916,6,'1'),('958',917,7,'1'),('958',918,8,'1'),('958',919,9,'1'),('958',920,10,'1'),('958',921,11,'1'),('958',922,12,'1'),('959',57,1,'1'),('959',58,2,'1'),('959',927,3,'1');
/*!40000 ALTER TABLE `sm_cdgroupcd` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sm_menu`
--

DROP TABLE IF EXISTS `sm_menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sm_menu` (
  `menu_id` varchar(5) NOT NULL COMMENT '메뉴 id',
  `menu_name` varchar(50) DEFAULT NULL COMMENT '메뉴명',
  `use_yn` char(1) DEFAULT '1' COMMENT '사용여부',
  `nouse_rsn` varchar(100) DEFAULT NULL COMMENT '미사용이유',
  PRIMARY KEY (`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='메뉴';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sm_menu`
--

LOCK TABLES `sm_menu` WRITE;
/*!40000 ALTER TABLE `sm_menu` DISABLE KEYS */;
INSERT INTO `sm_menu` VALUES ('FI001','Work log','1',NULL),('FI002','Accident report','1',NULL),('FI003','Fire report','1',NULL),('FI004','Fire reason report','1',NULL),('FI005','Rescue report per type','1',NULL),('FI006','Etc report per type','1',NULL),('FI007','Accident report per type','1',NULL),('FI008','Weekday report for call','1',NULL),('FI009','Result for call report','1',NULL),('HR001','Manage human resource','1',NULL),('IS001','Search building','1',NULL),('IS002','Manage building','1',NULL),('IS003','Manage fire car','1',NULL),('IS004','Manage team to dispatch','1',NULL),('IS005','\'Manage jurisdiction area','1',NULL),('IS006','Manage hospital','1',NULL),('IS007','Manage agency','1',NULL),('IS008','Manage volunteer','1',NULL),('IS009','Manage ward','1',NULL),('SM001','Create User','1',NULL),('SM002','Notification','1',NULL),('SM003','Manage user auth','1',NULL),('SM004','Manage menu','1',NULL),('SM005','Manage code','1',NULL);
/*!40000 ALTER TABLE `sm_menu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sm_menupriv`
--

DROP TABLE IF EXISTS `sm_menupriv`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sm_menupriv` (
  `menu_priv_seq` int(11) NOT NULL COMMENT '일렬번호',
  `priv_id` varchar(5) NOT NULL COMMENT '권한 id',
  `menu_id` varchar(5) NOT NULL COMMENT '메뉴 id',
  `level` int(1) NOT NULL COMMENT '권한수준(1:Read only, 2:Read & Write)',
  `lchg_dtime` varchar(20) DEFAULT NULL COMMENT '수정일시',
  `lchg_user_id` varchar(20) DEFAULT NULL COMMENT '수정자ID',
  PRIMARY KEY (`menu_priv_seq`,`priv_id`,`menu_id`),
  KEY `sm_menu_priv_FK` (`menu_id`),
  KEY `sm_menu_priv_FK_1` (`priv_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='권한/메뉴 매핑 및 권한 수준 설정';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sm_menupriv`
--

LOCK TABLES `sm_menupriv` WRITE;
/*!40000 ALTER TABLE `sm_menupriv` DISABLE KEYS */;
INSERT INTO `sm_menupriv` VALUES (1100000001,'SA001','FI001',2,NULL,NULL),(1100000002,'SA001','FI002',2,NULL,NULL),(1100000003,'SA001','FI003',2,NULL,NULL),(1100000004,'SA001','FI004',2,NULL,NULL),(1100000005,'SA001','FI005',2,NULL,NULL),(1100000006,'SA001','FI006',2,NULL,NULL),(1100000007,'SA001','FI007',2,NULL,NULL),(1100000008,'SA001','FI008',2,NULL,NULL),(1100000009,'SA001','FI009',2,NULL,NULL),(1100000010,'SA001','HR001',2,NULL,NULL),(1100000011,'SA001','IS001',2,NULL,NULL),(1100000012,'SA001','IS002',2,NULL,NULL),(1100000013,'SA001','IS003',2,NULL,NULL),(1100000014,'SA001','IS004',2,NULL,NULL),(1100000015,'SA001','IS005',2,NULL,NULL),(1100000016,'SA001','IS006',2,NULL,NULL),(1100000017,'SA001','IS007',2,NULL,NULL),(1100000018,'SA001','IS008',2,NULL,NULL),(1100000019,'SA001','IS009',2,NULL,NULL),(1100000020,'SA001','SM001',2,NULL,NULL),(1100000022,'SA001','SM003',2,NULL,NULL),(1100000023,'SA001','SM004',2,NULL,NULL),(1100000024,'SA001','SM005',2,NULL,NULL);
/*!40000 ALTER TABLE `sm_menupriv` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sm_notification`
--

DROP TABLE IF EXISTS `sm_notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sm_notification` (
  `notification_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'ID',
  `notification_type_cd` int(11) DEFAULT NULL COMMENT '통보유형',
  `related_seq` int(11) DEFAULT NULL COMMENT '승인요청 항목 일렬번호',
  `state_cd` int(11) DEFAULT NULL COMMENT '상태',
  `created_date` varchar(20) DEFAULT NULL COMMENT '생성일',
  `completed_date` varchar(20) DEFAULT NULL COMMENT '완료일',
  `completed_yn` char(1) DEFAULT '0' COMMENT '완료여부',
  `user_id` varchar(20) NOT NULL COMMENT '사용자id',
  `note` varchar(200) DEFAULT NULL COMMENT '메모',
  PRIMARY KEY (`notification_id`),
  KEY `FK_sm_notification_user_id` (`user_id`),
  CONSTRAINT `FK_sm_notification_user_id` FOREIGN KEY (`user_id`) REFERENCES `hr_user` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COMMENT='승인요청';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sm_notification`
--

LOCK TABLES `sm_notification` WRITE;
/*!40000 ALTER TABLE `sm_notification` DISABLE KEYS */;
INSERT INTO `sm_notification` VALUES (1,15,130000001,17,'04/26/2021 17:14:58','04/26/2021 17:43:20','2','test',NULL),(2,15,130000002,17,'04/26/2021 17:26:40','04/26/2021 17:43:20','2','test2',NULL),(3,15,130000003,119,'04/30/2021 09:30:55','04/30/2021 09:33:55','1','test3',NULL),(4,15,130000004,119,'05/14/2021 15:12:59','05/14/2021 15:29:35','1','test5',NULL),(5,15,130000005,119,'05/14/2021 15:17:32','05/14/2021 15:29:35','1','test6',NULL),(6,15,130000006,119,'05/14/2021 15:31:14','05/14/2021 15:38:31','1','test8',NULL),(7,15,130000007,119,'05/14/2021 15:34:41','05/14/2021 15:38:31','1','test9',NULL),(8,15,130000008,119,'05/14/2021 15:36:53','05/14/2021 15:38:31','1','test10',NULL),(9,15,130000009,119,'05/14/2021 15:37:41','05/14/2021 15:38:31','1','test11',NULL),(10,15,130000010,119,'05/14/2021 17:07:20','05/14/2021 17:36:28','1','test119',NULL),(11,15,130000011,119,'05/14/2021 17:29:23','05/14/2021 17:36:28','1','test120',NULL),(12,15,130000012,119,'05/14/2021 17:35:38','05/14/2021 17:36:28','1','test21',NULL),(14,15,130000013,119,'05/26/2021 18:04:06','05/27/2021 17:43:22','1','test123',NULL);
/*!40000 ALTER TABLE `sm_notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sm_priv`
--

DROP TABLE IF EXISTS `sm_priv`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sm_priv` (
  `priv_id` varchar(5) NOT NULL COMMENT '권한 id',
  `priv_name` varchar(50) DEFAULT NULL COMMENT '권한명',
  `lchg_dtime` varchar(20) DEFAULT NULL COMMENT '수정일시',
  `lchg_user_id` varchar(20) DEFAULT NULL COMMENT '수정자ID',
  PRIMARY KEY (`priv_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='권한';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sm_priv`
--

LOCK TABLES `sm_priv` WRITE;
/*!40000 ALTER TABLE `sm_priv` DISABLE KEYS */;
INSERT INTO `sm_priv` VALUES ('CM001','Normal user',NULL,NULL),('SA001','Super admin',NULL,NULL);
/*!40000 ALTER TABLE `sm_priv` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sm_sysenv`
--

DROP TABLE IF EXISTS `sm_sysenv`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sm_sysenv` (
  `id` char(3) NOT NULL COMMENT 'ID',
  `name` varchar(30) DEFAULT NULL COMMENT '설정명',
  `desc` varchar(100) DEFAULT NULL COMMENT '설명',
  `values` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL COMMENT '설정값',
  `reg_date` varchar(20) DEFAULT NULL COMMENT '등록일',
  `lchg_user_id` varchar(20) DEFAULT NULL COMMENT '수정일시',
  `lchg_dtime` varchar(20) DEFAULT NULL COMMENT '수정자ID',
  `use_yn` char(1) DEFAULT '1' COMMENT '사용여부',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='시스템환경설정';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sm_sysenv`
--

LOCK TABLES `sm_sysenv` WRITE;
/*!40000 ALTER TABLE `sm_sysenv` DISABLE KEYS */;
INSERT INTO `sm_sysenv` VALUES ('E01','DIVISION_CODE','The iso of division','{\"code\" : 13}',NULL,NULL,NULL,'1'),('E02','SUPPORT_LANGUAGES','The languages to be supported.','{\"languages\":[\"en\",\"kr\"]}',NULL,NULL,NULL,'1');
/*!40000 ALTER TABLE `sm_sysenv` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sm_user_preference`
--

DROP TABLE IF EXISTS `sm_user_preference`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sm_user_preference` (
  `user_id` varchar(20) NOT NULL COMMENT '사용자 id',
  `language` char(2) DEFAULT NULL COMMENT '언어',
  `theme` varchar(20) DEFAULT NULL COMMENT '테마',
  `table_rows` int(11) DEFAULT NULL COMMENT '테이블 Row 수',
  `table_dense` char(1) DEFAULT NULL COMMENT '테이블 Row 간격 좁게 세팅',
  `color` varchar(20) DEFAULT NULL COMMENT 'Color',
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sm_user_preference`
--

LOCK TABLES `sm_user_preference` WRITE;
/*!40000 ALTER TABLE `sm_user_preference` DISABLE KEYS */;
INSERT INTO `sm_user_preference` VALUES ('test','en','hub22',25,'1','primary');
/*!40000 ALTER TABLE `sm_user_preference` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sm_userloghist`
--

DROP TABLE IF EXISTS `sm_userloghist`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sm_userloghist` (
  `log_seq` int(11) NOT NULL COMMENT '로그일련번호',
  `user_id` varchar(20) DEFAULT NULL COMMENT '사용자 id',
  `log_dtime` date DEFAULT NULL COMMENT '로그인/아웃 시간',
  `log_cls_cd` int(11) DEFAULT NULL COMMENT '로그인/아웃 코드',
  PRIMARY KEY (`log_seq`),
  KEY `FK_sm_userloghist_user_id` (`user_id`),
  CONSTRAINT `FK_sm_userloghist_user_id` FOREIGN KEY (`user_id`) REFERENCES `hr_user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='사용자로그인아웃이력';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sm_userloghist`
--

LOCK TABLES `sm_userloghist` WRITE;
/*!40000 ALTER TABLE `sm_userloghist` DISABLE KEYS */;
/*!40000 ALTER TABLE `sm_userloghist` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sm_userpriv`
--

DROP TABLE IF EXISTS `sm_userpriv`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sm_userpriv` (
  `userpriv_seq` int(11) NOT NULL COMMENT '일련번호',
  `user_id` varchar(20) NOT NULL COMMENT '사용자 id',
  `priv_id` varchar(5) NOT NULL COMMENT '권한 id',
  `approved_yn` char(1) DEFAULT '0' COMMENT '승인여부',
  PRIMARY KEY (`userpriv_seq`,`user_id`,`priv_id`),
  KEY `FK_sm_userpriv_user_id` (`user_id`),
  KEY `FK_sm_userpriv_priv_id` (`priv_id`),
  CONSTRAINT `FK_sm_userpriv_priv_id` FOREIGN KEY (`priv_id`) REFERENCES `sm_priv` (`priv_id`),
  CONSTRAINT `FK_sm_userpriv_user_id` FOREIGN KEY (`user_id`) REFERENCES `hr_user` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='사용자별 권한';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sm_userpriv`
--

LOCK TABLES `sm_userpriv` WRITE;
/*!40000 ALTER TABLE `sm_userpriv` DISABLE KEYS */;
INSERT INTO `sm_userpriv` VALUES (1,'test','SA001','1');
/*!40000 ALTER TABLE `sm_userpriv` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'bd_safer'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-05-28 13:30:42
